///////////
/// calcCellAvgs
/// AUTHOR: Amanda Gibson
/// NOTES: The events concatenation takes place within this function
/// using the "concatEventsForCell" subfunction
///////////
function calcCellAvgs(string cellName)
    DFREF cellDFR = getCellSummaryDF(cellname)

    if(dataFolderRefStatus(cellDFR)==0)
        return 0
    endif

    resetEventAvgVarsToNaN(cellDFR)

    // Reset passive property avgs to NaN
    Struct passiveProps cellAvgPass
    StructFill/SDFR=cellDFR/AC=1 cellAvgPass
    fillPassiveProps(cellAvgPass)
    variable iPass = 0, numPassProps = itemsinlist(cellAvgPass.propNames)
    // set all to NaN
    for(iPass = 0; iPass<numPassProps; iPass++)
        string passPropN = StringFromList(iPass, cellAvgPass.propNames)
        if(strlen(passPropN))
            NVAR/Z/SDFR=cellDFR passVar = $passPropN
            passVar = NaN
        endif
    endfor

    storeFreqDurTotalEventsCellAvg(cellDFR)

    Struct cellSummaryInfo cellSum
    StructFill/SDFR=cellDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)
    
    Struct eventAvgVars cellAvgVars
     if(stringmatch(getDFREFstring(cellAvgVars), "root:"))
        print "cell", cellName, "calcCellAvgs is making variables in root"
    endif
    StructFill/SDFR=cellDFR/AC=1 cellAvgVars

    // Calculate averages for passive properties 
    numPassProps = itemsinlist(cellSum.passiveSeriesWaves)
    for(iPass = 0; iPass<numPassProps; iPass++)
        string thisWaveName = StringFromList(iPass, cellSum.passiveSeriesWaves)
        string thisVarName = stringbykey(thisWaveName, cellSum.numericVarNames)
        Wave/SDFR=cellDFR sumWave = $thisWaveName
        NVAR/Z/SDFR=cellDFR sumVar = $thisVarName

        sumVar = calcWaveAvg(sumWave) // NaNs ignored when calculating average
    endfor


    DFREF cellAvgDFR = getCellAvgSubDF(cellName)
    variable cellAvgDFR_status = dataFolderRefStatus(cellAvgDFR)
    if(cellAvgDFR_status)
        resetEventAvgVarsToNaN(cellAvgDFR)
        storeFreqDurTotalEventsCellAvg(cellAvgDFR)
    else
        print "calcCellAvgs: cell average data folder doesn't exist", cellName
    endif

    DFREF cellAvgLimitedDFR = getCellAvgSubLimitedDF(cellName)
    variable cellAvgLimitedDFR_status = dataFolderRefStatus(cellAvgLimitedDFR)
    if(cellAvgLimitedDFR_status)
        resetEventAvgVarsToNaN(cellAvgLimitedDFR)
        storeFreqDurTotalEventsCellAvg(cellAvgLimitedDFR)
    else
        print "calcCellAvgs: cell average limited subset data folder doesn't exist", cellName
    endif

    DFREF cellIntDFR = getCellIntSubDF(cellName)
    variable cellIntDFR_status = dataFolderRefStatus(cellIntDFR)
    if(cellIntDFR_status)
        resetEventAvgVarsToNaN(cellIntDFR)
        storeFreqDurTotalEventsCellAvg(cellIntDFR)
    else
        print "calcCellAvgs: cell interval subset data folder doesn't exist", cellName
    endif

    concatEventsForCell(cellName, "all")
    concatEventsForCell(cellName, "avg")
    concatEventsForCell(cellName, "interval")
    concatEventsForCell(cellName, "limited")

    calcCellAvgEvent(cellName, "avg")
    calcCellAvgEvent(cellName, "limited")
end

///////////
/// resetEventAvgVarsToNaN
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-16
/// NOTES: These are the variables for the average overall value for the cell
///////////
function resetEventAvgVarsToNaN(DFREF cellDFR)
    Struct eventAvgVars cellAvgVars
    StructFill/SDFR=cellDFR/AC=1 cellAvgVars
    if(stringmatch(getDFREFstring(cellDFR), "root:"))
        print "resetEventAvgVarsToNaN is making variables in root"
    endif
    fillAvgVarNames(cellAvgVars)
    variable iVar, numVars = itemsInList(cellAvgVars.avgVarNames)
    // set all to NaN
    for(ivar = 0; ivar<numVars; ivar++)
        string propN = StringFromList(ivar, cellAvgVars.avgVarNames)
        if(strlen(propN))
            NVAR/Z/SDFR=cellDFR thisVar = $propN
            thisVar = NaN
        endif
    endfor
end

///////////
/// storeFreqDurTotalEventsCellAvg
/// AUTHOR: Amanda
/// ORIGINAL DATE: 2024-01-16
/// NOTES: 
///////////
function storeFreqDurTotalEventsCellAvg(DFREF cellDFR)
    Struct cellSummaryInfo cellSum
    StructFill/SDFR=cellDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    variable freq, dur, totalEvents
    [freq, dur, totalEvents] = calcCellAvgFreq(cellSum.numEventsW, cellSum.durationsW)
    
    Struct eventAvgVars cellAvgVars
    StructFill/SDFR=cellDFR/AC=1 cellAvgVars
    if(stringmatch(getDFREFstring(cellDFR), "root:"))
        print "resetEventAvgVarsToNaN is making variables in root"
    endif
    cellAvgVars.freq = freq
    cellAvgVars.duration = dur
    cellAvgVars.numEvents = totalEvents

    cellAvgVars.seriesTime = calcCellStartTime(cellSum.startTimesW)
end



/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-04
/// NOTES: Added a "total events" variable to the output
function [variable avgFreq, variable totalDuration, variable totalEvents] calcCellAvgFreq(Wave numEventsW, Wave durationsW)
    avgFreq = NaN
    totalDuration = NaN
    if(WaveExists(numEventsW) && waveExists(durationsW)) // AGG 2022-04-09, changed to &&
        totalEvents = sum(numEventsW)
        totalDuration = sum(durationsW)
        avgFreq = totalEvents/totalDuration
    endif
    return [avgFreq, totalDuration, totalEvents]
end

function calcCellStartTime(Wave startTimesW)
    variable startTime = NaN
    if(WaveExists(startTimesW))
        startTime = startTimesW[0]
        if(!numtype(startTime)==0) // if it isn't a number
            startTime = NaN
        endif
    endif
    return startTime
end

///////////
/// concatEventsForCell
/// AUTHOR: Amanda Gibson
/// NOTES: This is where the true concatenation of events info waves takes place for cells
/// The exception is the PTB wave concatenation, which has to take place separately to account
/// for the gaps between series
///////////
function concatEventsForCell(string cellName, string subType[, variable useAvgSubset, variable convertUnits])
    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0
        endif
    endif

    strswitch (subType)
        case "all":
            DFREF cellSumDFR = getCellSummaryDF(cellName)
            break
        case "avg":
            DFREF cellSumDFR = getCellAvgSubDF(cellName)
            break
        case "interval":
            DFREF cellSumDFR = getCellIntSubDF(cellName)
            break
        case "limited":
            DFREF cellSumDFR = getCellAvgSubLimitedDF(cellName)
            break
        default:
            DFREF cellSumDFR = getCellSummaryDF(cellName)
            break
    endswitch

    if(dataFolderRefStatus(cellSumDFR)==0)
        print "concatEventsForCell: no summary folder for cell", cellName 
        return 0
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellSumDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string wavesToConcat = cellSum.avgByEventWaves
    variable numWaves = itemsInList(wavesToConcat), iWave = 0
    
    Wave/SDFR=getCellSummaryDF(cellName) combinedSeriesW

    for(iWave = 0; iWave < numWaves; iWave++)
        string thisWaveName = stringfromlist(iWave, wavesToConcat)
        string concatWaveName = thisWaveName + "_concat"
        string distWaveName = thisWaveName + "_dist"

        string thisExt = stringbykey(thisWaveName, cellSum.avgByEventsExts)

        Wave/SDFR=cellSumDFR concatWave = $concatWaveName, distWave = $distWaveName
        
        if(!WaveExists(concatWave) || !WaveExists(distWave))
            print "concatEventsForCell", cellName, "analysis wave doesn't exist", concatWaveName, "and/or", distWaveName
            continue // bumps back to start of the for loop
        endif
        
        redimension/N=0 concatWave, distWave

        if(!WaveExists(combinedSeriesW))
            if(iWave == 0)
                print "concatEventsForCell: no selected series for cell", cellName
            endif
            continue
        endif

        variable numSeries = numpnts(combinedSeriesW), iSeries = 0

        if(numSeries <= 0)
            if(iWave == 0)
                print "concatEventsForCell: no selected series for cell", cellName
            endif
            continue
        endif

        for(iSeries = 0; iSeries < numSeries; iSeries++)
            variable seriesNum = combinedSeriesW[iSeries]
            string seriesName = buildSeriesWaveName(cellName, seriesNum)

            string seriesNameExt = seriesName + "_" + thisExt
            
            strswitch (subType)
                case "all":
                    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)
                    break
                case "avg":
                    DFREF evDetectDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                    break
                case "interval":
                    DFREF evDetectDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
                    break
                case "limited":
                    DFREF evDetectDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                    break
                default:
                    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)
                    break
            endswitch
            
            if(dataFolderRefStatus(evDetectDFR)==0)
                if(iSeries == 0 && iWave == 0)
                    print "concatEventsForCell: no data folder series events for cell", cellName, "; if using averages, and have no events, that could be why"
                endif
                continue
            endif

            Wave/SDFR=evDetectDFR dataWave = $seriesNameExt

            if(!WaveExists(dataWave))
                continue
            endif

            string dataUnits = getWaveUnits(dataWave)

            if(convertUnits)
                duplicate/O dataWave, $("d"+nameOfWave(dataWave))/Wave=dupWave
                Wave convertWave = convertWaveUnits(dupWave, "fromBase")
                Concatenate/NP {convertWave}, concatWave
                killwaves dupWave
            else
                Concatenate/NP {dataWave}, concatWave
            endif

            Concatenate/NP {dataWave}, distWave
        endfor

        STRUCT eventAvgVars varsStruct
        fillAvgVarNames(varsStruct)

        string varKey = varsStruct.varFromWave
        string varN = stringbykey(thisExt, varKey)
        NVAR/Z/SDFR=cellSumDFR thisVar = $varN
        thisVar = NaN
        if(numpnts(concatWave)>0)
            thisVar = calcWaveAvg(concatWave)
        endif

        duplicate/O distWave, cellSumDFR:$(thisWaveName+"_concatOrigUnits")/Wave=origUnits
        
        if(WaveExists(dataWave))
            copyDataScale(dataWave, distWave)
            copyDataScale(dataWave, origUnits)
            
            string probDistribution = StringByKey(thisWaveName, cellSum.probDistribution)

            strswitch (probDistribution)
                case "bySign":
                    struct analysisparameters ps
                    getSeriesDetectParams(cellName, num2str(seriesNum), paramStruct = ps) // read the panel parameters for this cell/series

                    probdistp(nameOfWave(distWave), ps.peaksign, distWave = distWave)
                    break
                case "pos":
                    probdistp(nameOfWave(distWave), 1, distWave = distWave)
                    break
                default:
                    
                    break
            endswitch
        endif

        // print "concatEventsForCell", thisWaveName, thisExt, varN, thisVar
    endfor
end

// Average wave form for events from this cell
function calcCellAvgEvent(string cellName, string subType)
    strswitch (subType)
        case "avg":
            DFREF cellSumDFR = getCellAvgSubDF(cellName)
            break
        case "limited":
            DFREF cellSumDFR = getCellAvgSubLimitedDF(cellName)
            break
        default:
            DFREF cellSumDFR = getCellAvgSubDF(cellName)
            break
    endswitch
    
    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)
    if(DataFolderRefStatus(cellSumDFR) != 0)
        make/O/N=0 cellSumDFR:cellAvgEvent/Wave=aveWave
        make/O/N=0 cellSumDFR:cellNAvgEvent/Wave=nAveWave
        Wave/SDFR=getCellSummaryDF(cellName) combinedSeriesW
        if(WaveExists(combinedSeriesW))
            variable numSeries = numpnts(combinedSeriesW), iSeries = 0
            if(numSeries>0)
                make/Wave/O/N=(numSeries) wavesToAvg, avgIndices
                for(iSeries = 0; iSeries < numSeries; iSeries++)
                    variable seriesNum = combinedSeriesW[iSeries]
                    string seriesName = buildSeriesWaveName(cellName, seriesNum)
                    Wave rawWave = evDetectDFR:$seriesName
                    wavesToAvg[iSeries] = rawWave
                    strswitch (subType)
                        case "avg":
                            DFREF avgSubDF = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                            break
                        case "limited":
                            DFREF avgSubDF = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                            break
                        default:
                            DFREF avgSubDF = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                            break
                    endswitch
                    if(dataFolderRefStatus(avgSubDF)!=0)
                        Wave/SDFR=avgSubDF avgIndex
                        if(WaveExists(avgIndex))
                            avgIndices[iSeries] = avgIndex
                        endif
                    endif
                endfor
                recalculateAverages5(wavesToAvg, aveWave = aveWave, nAveWave = nAveWave, avgIndexWaves = avgIndices)
            else
                print "calcCellAvgEvent: no selected series for cell", cellName
            endif
        else
            print "calcCellAvgEvent: no selected series for cell", cellName
        endif
    else
        print "calcCellAvgEvent: no summary folder for cell", cellName 
    endif
end


///////////
/// createCellEventNumDist
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-04
/// NOTES: Create a distribution wave and plot of the number of events in each cell
///////////
function createCellEventNumDist()
    DFREF currentDF = GetDataFolderDFR()

    DFREF cellsOutDFR = getAllCellsOutputDF()
    
    if(dataFolderRefStatus(cellsOutDFR)==0)
        print "createCellEventNumDist: cellsOutDFR doesn't exist. Quitting"
        abort
    endif
    
    Wave/SDFR=cellsOutDFR sumCount 
    if(!WaveExists(sumCount))
        print "createCellEventNumDist: sumCount wave doesn't exist. Quitting"
        abort
    endif

    SetDataFolder cellsOutDFR
    probdistP("sumCount", 1)
    SetDataFolder currentDF

    Wave/SDFR=cellsOutDFR sumCount_dist 

    if(!WaveExists(sumCount_dist))
        print "createCellEventNumDist: sumCount_dist wave doesn't exist. Quitting"
        abort
    endif
    
    killwindow/Z sumCount_dist
    Display/K=1/VERT sumCount_dist
    Label/Z bottom, "Total # of events"
    Label/Z left, "proportion of cells"

    string percentiles = ".10;.25;.50;.75;.90;"
    variable numPercentiles = itemsInList(percentiles), iPercentile
    for(iPercentile=0; iPercentile<numPercentiles; iPercentile++)
        string thisPercentile = StringFromList(iPercentile, percentiles)
        variable percentile = str2num(thisPercentile)
        variable numEvents = getPercentileFromDist(sumCount_dist, percentile)
        print 100-(percentile*100), "% of cells have at least", numEvents, "events"
    endfor
end


///////////
/// createOverallDistPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-14
/// NOTES: 
///////////
function createOverallDistPlots(string distType[, DFREF storageDF, string displayHost, string xMaxes])
    DFREF seriesOutDFR = getSeriesOutputDF()
    DFREF cellsOutDFR = getAllCellsOutputDF()

    if(paramIsDefault(storageDF) || dataFolderRefStatus(storageDF)==0)
        NewDataFolder/O cellsOutDFR:dist
        DFREF storageDF = cellsOutDFR:dist
    endif

    if(paramisdefault(displayHost))
        displayHost = "dist_" + distType
    endif
    
    if(WinType(displayHost)==0)
        Display/K=1/N=$displayHost
    endif

    [string hostN, string subN] = splitFullWindowName(displayHost)
    doWindow/F $hostN

    SetActiveSubWindow $displayHost

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    Wave/SDFR = cellsOutDFR/T cellNames = cellName

    variable numVals = numpnts(cellNames)
    variable iCell = 0, numCells = numVals

    variable iGroup = 0, numGroups = 1

    variable red, green, blue, plottingSomething = 0
    string colors, legendString =""

    for(iGroup = 0; iGroup < numGroups; iGroup++)
        variable updatedUnits = 0, peakSign = -1

        make/N=(numVals)/O/Wave storageDF:$("distWaves")/Wave=distWaves
        make/N=(numVals)/T/O/Wave storageDF:$("cellNames")/Wave=eventCellNameWaves
        for(iCell=0; iCell<numVals; iCell++)
            string cellName = cellNames[iCell]
            DFREF cellDFR = getCellAvgSubDF(cellName) // hard coded
            
            if(dataFolderRefStatus(cellDFR)==0)
                print "createGroupDistPlots: couldn't find cell summary df", cellName
                continue
            endif

            Wave/SDFR=cellDFR distWave = $(distType + "_dist")

            make/T/O/N=(numpnts(distWave)) storageDF:$cellName/Wave=eventCellNames
            eventCellnames = cellName
            if(!WaveExists(distWave))
                print "createGroupDistPlots: couldn't find dist wave", distType, cellName
                continue
            endif

            if(!updatedUnits)
                string unitsString = getWaveUnits(distWave)
                struct analysisparameters ps
                variable worked = readpanelparams2(ps)
                variable panelPeakSign = ps.peakSign
                if(panelPeakSign == 1 || panelPeakSign == -1)
                    peakSign = panelPeakSign
                endif
                if(strlen(unitsString)>0)
                    updatedUnits = 1
                endif
            endif

            distWaves[iCell] = distWave            
            eventCellNameWaves[iCell] = eventCellNames         
        endfor
        string groupDistN = distType + "_dist"
        string groupCellNames = distType + "_cellNames"

        concatenate/NP/O/DL {distWaves}, storageDF:$(groupDistN)
        concatenate/NP/O/DL {eventCellNameWaves}, storageDF:$(groupCellNames)
        Wave/SDFR=storageDF groupDist = $(groupDistN)

        for(wave dupWave : eventCellNameWaves)
            KillWaves/Z dupWave
        endfor
    
        // setScale d, 0, 1, unitsString, groupDist

        // variable numConcatPnts = numpnts(groupDist)

        // if(numConcatPnts < 1)
        //     continue 
        // endif

        // Note/K groupDist // get rid of existing note (concats all notes from each cell above)

        // plottingSomething = 1

        // string probDistribution = StringByKey(distType, cellSum.probDistribution)

        // strswitch (probDistribution)
        //     case "bySign":
        //         probdistp(nameOfWave(distWave), peaksign, distWave = groupDist)
        //         break
        //     case "pos":
        //         probdistp(nameOfWave(distWave), 1, distWave = groupDist)
        //         break
        //     default:
                
        //         break
        // endswitch

        // string traceString = TraceNameList(displayHost, ";", 1)
        // variable nTraces = itemsInList(traceString)
        // string traceN = "t" + num2str(nTraces)
        // variable sortDir = appendDistWave(groupDist, traceN = traceN, displayHost = displayHost)

        // if(iGroup != 0)
        //     legendString += "\n"
        // endif
                    
        // legendString += "\s('" + traceN + "')" + "all"
        // plottingSomething = 1
    endfor

    // if(plottingSomething)
    //     if(paramIsDefault(xMaxes))
    //         xMaxes = ""
    //     endif
    //     // string xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.03;t50r:0.0025;"
    //     variable xMax = NumberByKey(distType, xMaxes)
    //     if(numtype(xMax)==0) // if exists
    //         setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, xMax=xMax, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
    //     else
    //         setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
    //     endif    
    // endif

    // variable numEvents = numpnts(groupDist)
    // print numEvents, "events in total"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .50) * 1e3, "ms in 50% of events", "(", numEvents * .5, ")"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .75) * 1e3, "ms in 25% of events", "(", numEvents * .25, ")"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .90) * 1e3, "ms in 10% of events", "(", numEvents * .1, ")"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .95) * 1e3, "ms in 5% of events", "(", numEvents * .05, ")"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .99) * 1e3, "ms in 1% of events", "(", numEvents * .01, ")"
    // print "Decay is longer than", getPercentileFromDist(groupDist, .995) * 1e3, "ms in 0.5% of events", "(", numEvents * .005, ")"
    // print "Max decay", WaveMax(groupDist) * 1e3, "ms"
end

///////////
/// createGroupDistPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: 
///////////
function createGroupDistPlots(string distType[, DFREF storageDF, string displayHost, string xMaxes])
    DFREF seriesOutDFR = getSeriesOutputDF()
    DFREF cellsOutDFR = getAllCellsOutputDF()

    if(paramIsDefault(storageDF) || dataFolderRefStatus(storageDF)==0)
        NewDataFolder/O cellsOutDFR:dist
        DFREF storageDF = cellsOutDFR:dist
    endif

    if(paramisdefault(displayHost))
        displayHost = "groupDist_" + distType
    endif
    
    if(WinType(displayHost)==0)
        Display/K=1/N=$displayHost
    endif

    [string hostN, string subN] = splitFullWindowName(displayHost)
    doWindow/F $hostN

    SetActiveSubWindow $displayHost

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    Wave/SDFR = cellsOutDFR/T groupWave = groupName, cellNames = cellName

    variable numVals = numpnts(groupWave)

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup
    
    variable maxItemsInGroup = 0, itemsInGroup = 0

    variable red, green, blue, plottingSomething = 0
    string colors, legendString =""

    for(iGroup = 0; iGroup < numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        Extract/O/INDX groupWave, storageDF:groupIndex, stringmatch(groupWave, groupName)
        Wave/SDFR = storageDF groupIndex
        itemsInGroup = numpnts(groupIndex)
        
        make/N=(itemsInGroup)/O/T storageDF:$("cellNames_"+groupName)/Wave=cellNamesByGroup
        cellNamesByGroup[] = cellNames[groupIndex[p]]

        variable iCell
        variable updatedUnits = 0, peakSign = -1

        make/N=(itemsInGroup)/O/Wave storageDF:$("distWaves_"+groupName)/Wave=distWaves
        for(iCell=0; iCell<itemsInGroup; iCell++)
            string cellName = cellNamesByGroup[iCell][iGroup]
            DFREF cellDFR = getCellSummaryDF(cellName)
            
            if(dataFolderRefStatus(cellDFR)==0)
                print "createGroupDistPlots: couldn't find cell summary df", cellName
                continue
            endif

            Wave/SDFR=cellDFR distWave = $(distType + "_dist")

            if(!WaveExists(distWave))
                print "createGroupDistPlots: couldn't find dist wave", distType, cellName
                continue
            endif

            if(!updatedUnits)
                string unitsString = getWaveUnits(distWave)
                struct analysisparameters ps
                variable worked = readpanelparams2(ps)
                variable panelPeakSign = ps.peakSign
                if(panelPeakSign == 1 || panelPeakSign == -1)
                    peakSign = panelPeakSign
                endif
                if(strlen(unitsString)>0)
                    updatedUnits = 1
                endif
            endif

            distWaves[iCell] = distWave            
        endfor
        string groupDistN = distType + "_dist" + "_" + groupName
        concatenate/NP/O {distWaves}, storageDF:$(groupDistN)
        Wave/SDFR=storageDF groupDist = $(groupDistN)
    
        setScale d, 0, 1, unitsString, groupDist

        variable numConcatPnts = numpnts(groupDist)

        if(numConcatPnts < 1)
            continue 
        endif

        Note/K groupDist // get rid of existing note (concats all notes from each cell above)

        plottingSomething = 1

        string probDistribution = StringByKey(distType, cellSum.probDistribution)

        strswitch (probDistribution)
            case "bySign":
                probdistp(nameOfWave(distWave), peaksign, distWave = groupDist)
                break
            case "pos":
                probdistp(nameOfWave(distWave), 1, distWave = groupDist)
                break
            default:
                
                break
        endswitch

        string traceString = TraceNameList(displayHost, ";", 1)
        variable nTraces = itemsInList(traceString)
        string traceN = "t" + num2str(nTraces)
        variable sortDir = appendDistWave(groupDist, traceN = traceN, displayHost = displayHost)

        if(iGroup != 0)
            legendString += "\n"
        endif
                    
        legendString += "\s('" + traceN + "')" + groupName
        plottingSomething = 1
        setTraceColors(iGroup, numGroups, traceN, displayHost = displayHost)
    endfor

    if(plottingSomething)
        if(paramIsDefault(xMaxes))
            xMaxes = ""
        endif
        // string xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.03;t50r:0.0025;"
        variable xMax = NumberByKey(distType, xMaxes)
        if(numtype(xMax)==0) // if exists
            setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, xMax=xMax, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
        else
            setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
        endif    
    endif


end

///////////
/// setTraceColors
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-09
/// NOTES: 
///////////
function setTraceColors(variable iGroup, variable numGroups, string traceN[, string displayHost])
    if(paramisdefault(displayHost))
        displayHost = "" // top graph
    endif

    variable red, green, blue

    wave rgb_table = makeColorTableWave(numGroups)

    ModifyGraph/W=$displayHost rgb($traceN)=(rgb_table[iGroup][0], rgb_table[iGroup][1], rgb_table[iGroup][2])
end


///////////
/// appendDistWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-09
/// NOTES: Append a distribution wave to an existing plot
/// returns the sortDir for this wave to help with later setting the axis
///////////
function appendDistWave(wave distWave[, string traceN, string displayHost])
    if(!WaveExists(distWave))
        print "appendDistWave: distribution wave doesn't exist"
        return NaN
    endif

    if(paramIsDefault(traceN))
        traceN = nameOfWave(distWave)
    endif

    if(paramisdefault(displayHost))
        displayHost = "" // top graph
    endif

    string noteStr = note(distWave)
    variable sortDir = NumberByKey("sortDir", noteStr)
    if(!(sortDir == 1 || sortDir == -1))
        sortDir = 1 // default to normal direction
    endif

    AppendToGraph/W=$displayHost/VERT distWave/TN=$traceN
    return sortDir
end

///////////
/// setDistPlotAxes
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-09
/// NOTES: 
///////////
function setDistPlotAxes([string displayHost, variable sortDir, variable xMin, variable xMax, string bottomLabel, string leftLabel, string legendString, variable textSize])
    if(paramisdefault(displayHost))
        displayHost = "" // top graph
    endif

    string xAxisScaling
    if(paramIsDefault(xMin) && paramIsDefault(xMax))
        xAxisScaling = "auto"
    else // either 1 or both of them is not default
        if(paramIsDefault(xMin)) // must mean that xMax is not default
            xAxisScaling = "setMax"
        else
            if(paramIsDefault(xMax)) // must mean that xMin is not default
                xAxisScaling = "setMin"
            else // both are not default
                xAxisScaling = "setMinMax"
            endif
        endif
    endif

    if(paramIsDefault(textSize))
        textSize = 14
    endif
    
    ModifyGraph/W=$displayHost gfSize=textSize

    variable axisDir = 1
    if(!paramIsDefault(sortDir) && sortDir == -1)
        axisDir = -1
    endif

    // Autoscale first
    SetAxis/A/E=3/W=$displayHost bottom

    strswitch (xAxisScaling)
        case "auto":
            if(axisDir == -1)
                SetAxis/R/A/W=$displayHost bottom
            endif
            break
        case "setMax":
            if(axisDir == 1)
                SetAxis/W=$displayHost bottom, *, xMax
            else
                SetAxis/R/W=$displayHost bottom, *, xMax
            endif
            break
        case "setMin":
            if(axisDir == 1)
                SetAxis/W=$displayHost bottom, xMin, *
            else
                SetAxis/R/W=$displayHost bottom, xMin, *
            endif
            break
        default:
            if(axisDir == -1)
                SetAxis/R/A/W=$displayHost bottom
            endif
            break
            break
    endswitch

    // strswitch (xAxisScaling)
    //     case "auto":
    //         if(axisDir == 1)
    //             SetAxis/A/E=3/W=$displayHost bottom
    //         else
    //             SetAxis/A/E=3/R/W=$displayHost bottom
    //         endif
    //         break
    //     case "setMax":
    //         if(axisDir == 1)
    //             SetAxis/W=$displayHost bottom, *, xMax
    //         else
    //             SetAxis/R/W=$displayHost bottom, *, xMax
    //         endif
    //         break
    //     case "setMin":
    //         if(axisDir == 1)
    //             SetAxis/W=$displayHost bottom, xMin, *
    //         else
    //             SetAxis/R/W=$displayHost bottom, xMin, *
    //         endif
    //         break
    //     default:
    //         if(axisDir == 1)
    //             SetAxis/A/E=3/W=$displayHost bottom
    //         else
    //             SetAxis/A/E=3/R/W=$displayHost bottom
    //         endif
    //         break
    // endswitch


    // Still have the left set to show the full range, even if
    // showing only a subset of x by default
    SetAxis/A/E=3/W=$displayHost left
    
    if(paramIsDefault(leftLabel))
        leftLabel = "proportion of events"
    endif
    Label /W=$displayHost/Z left, leftLabel

    if(!paramIsDefault(bottomLabel))
        Label /W=$displayHost/Z bottom, bottomLabel
    endif

    if(!paramIsDefault(legendString))
        Legend/W=$displayHost /N=text0 /C/J/B=1/A=RB/G=(0,0,0) legendString
    endif
end




///////////
/// iterateDistWithLimitedEvents
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: 
///////////
function iterateDistWithLimitedEvents([string xMaxes])
    if(paramIsDefault(xMaxes))
        xMaxes = ""
    endif
    
    // string params = "peaks;der;t50r;fwhm;"
    string params = "decay9010;"

    variable iParam, nParams = itemsInList(params)

    string maxEvents = "20;50;100;150;"
    // string maxEvents = "10"
    variable iMax, nMax = itemsInList(maxEvents)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(!WaveExists(cellNames))
        return 0
    endif

    variable numCells = numpnts(cellNames)
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    NVAR/Z/SDFR = useAvgDFR limitEventsPerCell, maxEventsPerCell
    limitEventsPerCell = 1
    for(iMax=0; iMax<nMax; iMax++)
        maxEventsPerCell = str2num(StringFromList(iMax, maxEvents))

        variable nIterations = 10, iIt = 0
        for(iIt=0; iIt<nIterations; iIt++)
            DFREF cellSumDFR = getAllCellsOutputDF()
            string maxItStr = "max"+num2str(maxEventsPerCell)+"i"+num2str(iIt)
            NewDataFolder/O cellSumDFR:$(maxItStr)
            DFREF itDFR = cellSumDFR:$(maxItStr)
            variable iCell = 0
            for( iCell=0; iCell<numCells; iCell++ )
                string cellName = cellNames[iCell]
                DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
                variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
                if(dfrStatus != 0)
                    concatAndCalcAvgForCell(cellName)
                endif
            endfor
            
            for(iParam=0; iParam<nParams; iParam++)
                string distType = StringFromList(iParam, params)
                string displayHost = distType + "_" + "max" + num2str(maxEventsPerCell)
                createGroupDistPlots(distType, storageDF = itDFR, displayHost = displayHost, xMaxes = xMaxes)
            endfor
        endfor
    endfor
end

///////////
/// iterateIntDistWithLimitedEvents
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: 
///////////
function iterateIntDistWithLimitedEvents([string xMaxes])

    if(paramIsDefault(xMaxes))
        xMaxes = ""
    endif
    
    // string maxEvents = "10;20;25;30;50;75;100;"
    // string maxEvents = "50;75;100;"
    // string maxEvents = "75;"
    string maxEvents = "100;200;250;300;"
    variable iMax, nMax = itemsInList(maxEvents)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(!WaveExists(cellNames))
        return 0
    endif

    variable numCells = numpnts(cellNames)
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    NVAR/Z/SDFR = useAvgDFR limitEventsPerCell, maxEventsPerCell
    limitEventsPerCell = 1
    for(iMax=0; iMax<nMax; iMax++)
        maxEventsPerCell = str2num(StringFromList(iMax, maxEvents))

        variable nIterations = 20, iIt = 0
        for(iIt=0; iIt<nIterations; iIt++)
            DFREF cellSumDFR = getAllCellsOutputDF()
            string maxItStr = "max"+num2str(maxEventsPerCell)+"i"+num2str(iIt)
            NewDataFolder/O cellSumDFR:$(maxItStr)
            DFREF itDFR = cellSumDFR:$(maxItStr)
            variable iCell = 0
            for( iCell=0; iCell<numCells; iCell++ )
                string cellName = cellNames[iCell]
                DFREF indivCellSumDFR = getCellSummaryDF(cellName)
                if(dataFolderRefStatus(indivCellSumDFR)==0)
                    continue
                endif

                Wave/SDFR=indivCellSumDFR int_dist_all

                if(!WaveExists(int_dist_all))
                    continue
                endif

                if(DimSize(int_dist_all, 0)>=maxEventsPerCell)
                    randomSubsetOfWave(int_dist_all, maxEventsPerCell, subWN = "int_dist") 
                endif
            endfor

            string distType = "int"
            string displayHost = distType + "_" + "max" + num2str(maxEventsPerCell)
            createGroupDistPlots(distType, storageDF = itDFR, displayHost = displayHost, xMaxes = xMaxes)
        endfor
    endfor
end

function createIntDistAllWaves()
    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(!WaveExists(cellNames))
        return 0
    endif

    variable numCells = numpnts(cellNames)
    variable iCell = 0
    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            continue
        endif

        Wave/SDFR=cellSumDFR int_dist
        if(!WaveExists(int_dist))
            continue
        endif
        duplicate/O int_dist, cellSumDFR:int_dist_all
    endfor
end

///////////
/// iterateSaveDistPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: 
///////////
function iterateSaveDistPlots()
    // string params = "peaks;der;t50r;fwhm;"
    // string params = "decay9010;"
    string params = "int;"
    variable iParam, nParams = itemsInList(params)

    // string maxEvents = "20;50;100;150;"
    string maxEvents = "100;200;250;300;"
    variable iMax, nMax = itemsInList(maxEvents)

    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    NVAR/Z/SDFR = useAvgDFR limitEventsPerCell, maxEventsPerCell
    limitEventsPerCell = 1
    for(iMax=0; iMax<nMax; iMax++)
        maxEventsPerCell = str2num(StringFromList(iMax, maxEvents))

        for(iParam=0; iParam<nParams; iParam++)
            string distType = StringFromList(iParam, params)
            string displayHost = distType + "_" + "max" + num2str(maxEventsPerCell)
            string fileType = "png"
            variable width = 5.75
            variable height = 5.5
            // variable width = 5.75
            // variable height = 2.75

            savePlot(displayHost, fileName = "C:\\Users\\percs\\Documents\\ephysAnalysis\\dists\\a" + displayHost, units  = "in", width = width, height = height, fileType = "png")
        endfor
    endfor
end

///////////
/// iterateDistWithNoLimits
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: 
///////////
function iterateDistWithNoLimits([string xMaxes])
    if(paramIsDefault(xMaxes))
        xMaxes = ""
    endif
    
    // string params = "peaks;der;t50r;fwhm;"
    string params = "int"
    // string params = "decay9010;"
    variable iParam, nParams = itemsInList(params)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(!WaveExists(cellNames))
        return 0
    endif

    variable numCells = numpnts(cellNames)
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    NVAR/Z/SDFR = useAvgDFR limitEventsPerCell, maxEventsPerCell
    limitEventsPerCell = 0
    
    DFREF cellSumDFR = getAllCellsOutputDF()
    string maxItStr = "max0"
    NewDataFolder/O cellSumDFR:$(maxItStr)
    DFREF itDFR = cellSumDFR:$(maxItStr)
    variable iCell = 0
    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
        variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
        if(dfrStatus != 0)
            concatAndCalcAvgForCell(cellName)
        endif
    endfor
    
    for(iParam=0; iParam<nParams; iParam++)
        string distType = StringFromList(iParam, params)
        string displayHost = distType + "_" + "max0"
        createGroupDistPlots(distType, storageDF = itDFR, displayHost = displayHost, xMaxes = xMaxes)
        string fileType = "png"
        variable width = 5.75
        variable height = 5.5
        // variable width = 5.75
        // variable height = 2.75
        savePlot(displayHost, fileName = "C:\\Users\\percs\\Documents\\ephysAnalysis\\dists\\a" + displayHost, units  = "in", width = width, height = height, fileType = "png")
    endfor
end


///////////
/// iterateDistAsIs
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-14
/// NOTES: 
///////////
function iterateDistAsIs([string xMaxes])
    if(paramIsDefault(xMaxes))
        xMaxes = ""
    endif
    
    string params = "peaks;der;t50r;fwhm;decay9010;"

    variable iParam, nParams = itemsInList(params)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(!WaveExists(cellNames))
        return 0
    endif

    variable numCells = numpnts(cellNames)
    
    DFREF cellSumDFR = getAllCellsOutputDF()
    DFREF distDFR = cellSumDFR:dist
    if(dataFolderRefStatus(distDFR)==0)
        NewDataFolder/O cellSumDFR:dist
        DFREF distDFR = cellSumDFR:dist
    endif

    for(iParam=0; iParam<nParams; iParam++)
        string distType = StringFromList(iParam, params)
        string displayHost = distType
        createGroupDistPlots(distType, storageDF = distDFR, displayHost = displayHost, xMaxes = xMaxes)
    endfor
end
