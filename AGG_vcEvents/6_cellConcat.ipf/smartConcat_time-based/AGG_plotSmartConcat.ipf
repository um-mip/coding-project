///////////
/// plotPTBHisto
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-06
/// NOTES: 
///////////
function plotPTBHisto(string cellName[, DFREF cellSmartConcDF, string histoHost, DFREF cellSumDFR, variable showChopped, variable showHisto, variable colorHisto, variable showGaps, variable showCityPlot, variable startTime, variable endTime])
    if(paramIsDefault(histoHost))
        histoHost = "AGG_Events#cellHistoGraph"
    endif

    string hostN, subWin

    [hostN, subWin] = splitFullWindowName(histoHost)

    if(paramIsDefault(showGaps))
        showGaps = getShowConcGaps()
    endif

    if(paramIsDefault(showChopped))
        showChopped = getShowConcChopped()
    endif

    if(paramIsDefault(showHisto))
        showHisto = getShowConcHisto()
    endif
    
    if(paramIsDefault(colorHisto))
        colorHisto = getColorConcHisto()
    endif

    if(paramIsDefault(showCityPlot))
        // only show within burst panel
        if(stringmatch(hostN, "AGG_bursts"))
            showCityPlot = getShowCityPlot()
        else
            showCityPlot = 0
        endif
    endif
    
    clearDisplay(histoHost)

    if(paramIsDefault(cellSumDFR))
        DFREF cellSumDFR = getEventsCellSumOutDF()
    endif
    

    if(paramIsDefault(cellSmartConcDF))
        DFREF cellSmartConcDF = getCellSummaryDF(cellName)
    endif

    if(dataFolderRefStatus(cellSmartConcDF)==0) // no smart conc folder
        return 0
    endif
    
    Wave/SDFR=cellSmartConcDF sctWave = $(cellName + "_sct")
    if(!WaveExists(sctWave))
        return 0
    endif

    if(paramIsDefault(startTime) || numtype(startTime) == 1)
        startTime = 0
    endif

    if(paramIsDefault(endTime) || numtype(endTime) == 1)
        endTime = AGG_wnoteDuration(nameofWave(sctWave), cellSmartConcDF)
    endif


    Wave/SDFR=cellSumDFR eventHisto, eventHistoByFreq
    if(WaveExists(eventHisto))
        redimension/N=0 eventHisto
    endif

    if(WaveExists(eventHistoByFreq))
        redimension/N=0 eventHistoByFreq
    endif

    // AGG create the histo waves, even if not plotting, so that tables update
    [Wave eventHisto, Wave eventHistoByFreq] = createPTBHisto(sctWave, histoDFR = cellSumDFR, startTime = startTime, endTime = endTime)
    
    if(showHisto)
        appendPTBHistoToGraph(eventHistoByFreq, hostN = histoHost, histoAxis = "histoAxis")

        ModifyGraph/W=$histoHost rgb(eventHisto) = (13107,13107,13107), useBarStrokeRGB(eventHisto)=1 // make everything grey to start and add stroke
        if(colorHisto)
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            if(dataFolderRefStatus(cellInfoDFR)!=0)
                Wave/SDFR=cellInfoDFR/T regionsNames = regionsNames_stored
                if(!WaveExists(regionsNames))
                    DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
                    string regionsNames_str = getChildDFRList(regionDFR)
                    Wave/T regionsNames = listToTextWave(regionsNames_str, ";")
                endif
                if(WaveExists(regionsNames) && numpnts(regionsNames)>0)
                    Wave/SDFR=cellInfoDFR/D regionsStarts, regionsEnds
                    if(!WaveExists(regionsStarts))
                        make/D/FREE/N=(numpnts(regionsNames)) regionsStarts
                        regionsStarts = NaN
                    endif
                    if(!WaveExists(regionsEnds))
                        make/D/FREE/N=(numpnts(regionsNames)) regionsEnds
                        regionsEnds = NaN
                    endif
                    if(WaveExists(regionsStarts) && waveExists(regionsEnds))
                        colorHistogram(regionsStarts, regionsEnds, eventHistoByFreq, target = histoHost)
                    endif
                endif
            endif
        endif

        ModifyGraph/W=$histoHost lblPosMode($("histoAxis"))=1 // Sets the label to the outside
        ModifyGraph/W=$histoHost freePos($("histoAxis"))={0,kwFraction}
    endif


    Wave/SDFR=cellSmartConcDF choppedXTimeWave_cell = $(cellName + "_scx"), choppedEventsY_cell = $(cellName + "_scy"), gapPlotX_cell = $(cellName + "_gapPlotX"), gapPlotY_cell = $(cellName + "_gapPlotY")

    if(!WaveExists(choppedXTimeWave_cell) || !WaveExists(choppedEventsY_cell) || !WaveExists(gapPlotX_cell) || !WaveExists(gapPlotY_cell))
        print "plotPTBHisto: missing at least one wave to plot"
        return 0
    endif

    duplicate/O choppedXTimeWave_cell, cellSumDFR:choppedXTimeWave/Wave=choppedXTimeWave
    duplicate/O choppedEventsY_cell, cellSumDFR:choppedEventsY/Wave=choppedEventsY
    duplicate/O gapPlotX_cell, cellSumDFR:gapPlotX/Wave=gapPlotX
    duplicate/O gapPlotY_cell, cellSumDFR:gapPlotY/Wave=gapPlotY

    if(showChopped)
        appendChoppedEventsToGraph(choppedXTimeWave, choppedEventsY, hostN = histoHost)
        if(colorHisto)
            colorByRegions(cellName, "choppedEvents", target = histoHost, xWave = cellSmartConcDF:$(cellName + "_scx"), colorDFR = cellSumDFR)
        endif
    endif

    variable canPlotCity = 0
    if(showCityPlot)
        string vbwAxis = "newVbw"
        DFREF cellDetectDFR = getVBWCellDetectionDF(cellName) 
        if(dataFolderRefStatus(cellDetectDFR)!=0)
            Wave/SDFR=cellDetectDFR eventMinBurstWin
            if(WaveExists(eventMinBurstWin))
                canPlotCity = 1
            endif
        endif
        
        if(canPlotCity)
            appendCityPlot(cellName, target=histoHost, vbwAxis = vbwAxis, startTime = startTime, endTime = endTime)
            if(colorHisto)
                colorByRegions(cellName, "cityPlot", target = histoHost, xWave = cellSmartConcDF:$(cellName + "_sct"), colorDFR = cellSumDFR)
            endif
        else
            print "plotPTBHisto: Missing either the burst detection data folder and/or the evenMinBurstWin wave to make the city plot for", cellName
        endif
    endif

    if(showGaps)
        appendGapPlotsToGraph(gapPlotX, gapPlotY, hostN = histoHost)  
    endif

    if(showHisto && showChopped && showCityPlot && canPlotCity)
        ModifyGraph/W=$histoHost axisEnab($vbwAxis) = {0, 0.33}, axisEnab($("histoAxis")) = {0.35, 0.66}, axisEnab(left)={0.67, 1}
    elseif (showHisto && showChopped)
        ModifyGraph/W=$histoHost axisEnab($("histoAxis")) = {0, 0.32}, axisEnab(left)={0.38, 1}
    elseif (showHisto && showCityPlot && canPlotCity)
        ModifyGraph/W=$histoHost axisEnab($vbwAxis) = {0, 0.48}, axisEnab($("histoAxis")) = {0.52, 1}
    elseif (showChopped && showCityPlot && canPlotCity)
        ModifyGraph/W=$histoHost axisEnab($vbwAxis) = {0, 0.34}, axisEnab(left)={0.36, 1}
    endif
    if (showChopped || canPlotCity || showHisto || showGaps)
        Label/W=$histoHost bottom, "elapsed time (hh:mm:ss.ms)"
        ModifyGraph/W=$histoHost dateInfo(bottom)={1,2,0}
        SetAxis/W=$histoHost/Z/A/E=3 bottom
    endif

end

//\\//\\//\\//\\//\\  HISTOGRAM COMPONENTS \\//\\//\\//\\//\\//\\//\\//\\//

    ///////////
    /// createPTBHisto
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-06
    /// NOTES: binDur is in seconds
    /// assumes that ptbWave has already been subset for the appropriate time
    ///////////
    function [Wave/D eventHisto, Wave/D eventHistoByFreq] createPTBHisto(wave ptbWave[, variable binDur, DFREF histoDFR, variable startTime, variable endTime])
        DFREF cellSumDFR = getEventsCellSumOutDF()
        if(paramIsDefault(histoDFR))
            histoDFR = cellSumDFR
        endif

        if(dataFolderRefStatus(histoDFR)==0)
            print "createPTBHisto: Couldn't find histoDFR. Defaulting to root"
            DFREF histoDFR = root:
        endif
        
        if(paramIsDefault(binDur))
            NVAR/Z/SDFR=cellSumDFR globalBinDur = binDur 
            binDur = globalBinDur
        endif
        
        if(binDur == 0)
            print "createPTBHisto: binDur is 0. Setting to 60s"
            binDur = 60
        endif

        if(paramIsDefault(startTime))
            startTime = 0
        endif

        string ptbWaveN
        DFREF ptbDFR

        if(!WaveExists(ptbWave))
            print "createPTBHisto: Couldn't find ptbWave"
            return [$"", $""]
        endif

        [ptbWaveN, ptbDFR] = getWaveNameAndDFR(ptbWave)
        variable ptbDuration
        
        if(dataFolderRefStatus(ptbDFR)!=0)
            NVAR/Z/SDFR=ptbDFR totalDur
            if(NVAR_Exists(totalDur) && totalDur > 0)
                ptbDuration = totalDur
            else
                ptbDuration = AGG_wnoteduration( ptbWaveN , ptbDFR )
            endif
        else
            ptbDuration = AGG_wnoteduration( ptbWaveN , ptbDFR )
        endif
        
        if(paramIsDefault(endTime))
            endTime = ptbDuration
        endif

        variable binZero = startTime
        variable numBins = ceil((endTime - startTime)/binDur)

        make/N=(numBins)/D/O histoDFR:eventHisto/Wave=eventHisto = 0
        if(numbins > 1 && numpnts(ptbWave) > 0) // need 2 bins for histogram
            Histogram/B={binZero, binDur, numBins}/DP ptbWave, eventHisto
            duplicate/O/D eventHisto, histoDFR:eventHistoByFreq
            Wave/SDFR=histoDFR eventHistoByFreq
            eventHistoByFreq /= binDur
        else
            if(numbins == 1)
                print "createPTBHisto: WARNING need at least 2 bins to make a histogram"
            endif
        endif

        //Set the scale in time (dat)
        // x signifies the x (point) dimension
        SetScale/P x, startTime, binDur, "dat", eventHisto
        SetScale/P x, startTime, binDur, "dat", eventHistoByFreq

        return [eventHisto, eventHistoByFreq]
    end

    ///////////
    /// appendPTBHistoToGraph
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-06
    /// NOTES: 
    ///////////
    function appendPTBHistoToGraph(wave eventHisto[, string hostN, string histoAxis, variable binDur])
        if(paramIsDefault(hostN))
            hostN = ""
        endif

        if(paramIsDefault(histoAxis))
            histoAxis = "left"
        endif

        string binString = "bin"
        if(paramIsDefault(binDur))
            DFREF cellSumDFR = getEventsCellSumOutDF()
            if(dataFolderRefStatus(cellSumDFR)!=0)
                NVAR/Z/SDFR=cellSumDFR globalBinDur = binDur 
                if(NVAR_Exists(globalBinDur))
                    binDur = globalBinDur
                    binString = num2str(binDur) + "s"
                endif
            endif
        else
            binString = num2str(binDur) + "s"
        endif
        
        AppendToGraph/W=$hostN/L=$histoAxis eventHisto/TN=eventHisto
        SetAxis/A/E=1/W=$hostN $histoAxis
        Label/W=$hostN $histoAxis, "frequency (Hz)"
        ModifyGraph/W=$hostN mode(eventHisto) = 5, hbFill(eventHisto)=2, rgb(eventHisto) = (0, 0, 0)
    end

//\\//\\//\\//\\//\\//\\//\\// GAP COMPONENTS \\//\\//\\//\\//\\//\\//\\//
   
    ///////////
    /// appendGapPlotsToGraph
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-05
    /// NOTES: Adds blue vertical lines at locations of gaps to existing plot
    ///////////
    function appendGapPlotsToGraph(Wave gapPlotX, wave gapPlotY[, string hostN])
        if(!WaveExists(gapPlotX) || !waveExists(gapPlotY))
            return 0
        endif
        
        if(paramIsDefault(hostN))
            hostN = ""
        endif
        

        string gapAxis = "gap"
        appendtograph /W=$hostN /R=$gapAxis gapPlotY vs gapPlotX
        ModifyGraph/W=$hostN mode($nameOfWave(gapPlotY))=5
        ModifyGraph/W=$hostN rgb($nameOfWave(gapPlotY))=(0,65535,65535)
        ModifyGraph/W=$hostN hbFill($nameOfWave(gapPlotY))=2
        ModifyGraph/W=$hostN freePos($gapAxis)=0
        ModifyGraph/W=$hostN axisEnab($gapAxis)={0,1}
        ModifyGraph/W=$hostN axRGB($gapAxis)=(65535,65535,65535), freePos($gapAxis)=0
        ModifyGraph/W=$hostN tlblRGB($gapAxis)=(65535,65535,65535)
        SetScale d 0, 0, "dat", gapPlotX
    end

//\\//\\//\\//\\//\\//\\ CHOPPED EVENTS COMPONENTS //\\//\\//\\//\\//\\//\\//\\
    ///////////
    /// appendChoppedEventsToGraph
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-05
    /// NOTES: 
    ///////////
    function appendChoppedEventsToGraph(Wave choppedXTimeWave, wave choppedEventsY[, string hostN])
        if(!WaveExists(choppedXTimeWave) || !waveExists(choppedEventsY))
            return 0
        endif
        
        if(paramIsDefault(hostN))
            hostN = ""
        endif

        // string smartConcAxis = "smartConc"
        // appendtograph /W=$hostN /L=$smartConcAxis choppedEventsY vs choppedXTimeWave
        // ModifyGraph freePos($smartConcAxis)=0
        // ModifyGraph lblPosMode($smartConcAxis)=1
        // Label /W=$hostN $smartConcAxis "current"

        AppendToGraph/W=$hostN choppedEventsY vs choppedXTimeWave
        Label /W=$hostN bottom "elapsed time (hh:mm:ss.ms)"
        Label /W=$hostN left "current"

        ModifyGraph/W=$hostN rgb($nameOfWave(choppedEventsY))=(0, 0, 0)
        SetScale d 0, 0, "dat", choppedXTimeWave
    end

//\\//\\//\\//\\//\\//\\ CITY PLOT //\\//\\//\\//\\//\\//\\//\\




///////////
/// appendCityPlot
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-26
/// NOTES: 
///////////
function appendCityPlot(string cellName[, string target, string vbwAxis, variable startTime, variable endTime])
    if(strlen(cellName)==0)
        return NaN
    endif

    if(paramIsDefault(target))
        target = "AGG_bursts#burstCellPlot"
    endif

    string hostN, subN
    [hostN, subN] = splitFullWindowName(target)

    if(paramIsDefault(vbwAxis))
        vbwAxis = "newVBW"
    endif
    
    
    DFREF cellDetectDFR = getVBWCellDetectionDF(cellName)
    if(dataFolderRefStatus(cellDetectDFR)==0)
        return NaN
    endif

    Wave/SDFR=cellDetectDFR eventMinBurstWin, burstWindows

    if(!WaveExists(eventMinBurstWin) || !WaveExists(burstWindows))
        return NaN
    endif

    DFREF axisInfoDFR = getVBWCityPlotAxisDF()
    if(dataFolderRefStatus(axisInfoDFR)==0)
        createVBWCityPlotAxisDF()
        DFREF axisInfoDFR = getVBWCityPlotAxisDF()
    endif

    Wave sctWave = getCellSCTWave(cellName)
    if(paramIsDefault(startTime) || paramIsDefault(endTime))
        duplicate/O sctWave, axisInfoDFR:sctWave
        duplicate/O eventMinBurstWin, axisInfoDFR:flippedMin
    else
        wave sctWave_region = extractWaveWithinRegion(sctWave, startTime, endTime)
        Wave eventMinBurstWin_region = extractWaveWithinRegion(sctWave, startTime, endTime, infoWave = eventMinBurstWin)

        duplicate/O sctWave_region, axisInfoDFR:sctWave
        duplicate/O eventMinBurstWin_region, axisInfoDFR:flippedMin
    endif
    
    Wave/SDFR=axisInfoDFR sctWave
    Wave/SDFR=axisInfoDFR flippedMin

    variable/G axisInfoDFR:maxBW/N=maxBW
    maxBW = WaveMax(burstWindows)

    variable/G axisInfoDFR:lastMin/N=lastMin=NaN, axisInfoDFR:lastMax/N=lastMax=NaN
    
    flippedMin = maxBW - flippedMin

    if(!winExists(target))
        display/N=$target
    endif

    SetWindow $hostN hook(resizeHook) = burstWinHook

    SetScale d 0, 0, "dat", sctWave
    

    AppendToGraph/W=$target/L=$vbwAxis flippedMin vs sctWave

    ModifyGraph/W=$target mode($nameOfWave(flippedMin))=5, hbFill($nameOfWave(flippedMin))=2, toMode($nameOfWave(flippedMin))=1, rgb($nameOfWave(flippedMin))=(0, 0, 0)
    
    ModifyGraph /W=$target freePos($vbwAxis)={0, kwFraction}
    ModifyGraph /W=$target lblPosMode($vbwAxis)=1

    SetAxis/W=$target $vbwAxis 0, maxBW
    Label/W=$target $vbwAxis, "bin window (s)"


    flipCityPlotLabels(target, vbwAxis)

    Wave/SDFR=axisInfoDFR tickValuesWave
    Wave/SDFR=axisInfoDFR/T tickLabelsWave

    ModifyGraph/W=$target userticks($vbwAxis)={tickValuesWave,tickLabelsWave}
end

///////////
/// burstWinHook
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-26
/// NOTES: This hook is placed on the entire AGG_Bursts window, as subwindows can't take a window hook
/// This function checks if a graph (or notebook) has been modified, and then checks if it was the burstCellPlot display
/// If it was, it runs the flipCityPlotLabels function to see if the vbw axis has changed range, and if so, to flip the labels
///////////
function burstWinHook(s)
    STRUCT WMWinHookStruct &s

	Variable hookResult = 0

	switch(s.eventCode)
		case 8: // modified
            if(stringmatch(s.winName, "AGG_Bursts#burstCellPlot"))
                hookResult = flipCityPlotLabels(s.winName, "newVBW")
                return hookResult
            endif
            
            return hookResult
            break
	endswitch

	return hookResult		// 0 if nothing done, else 1
end


///////////
/// flipCityPlotLabels
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-26
/// NOTES: 
/// There is a function TickWavesFromAxis that gets you the waves of the Igor nicely spaced labels
/// for the graph. However, this draws the axis, and leads to a lot of visual strobbing as zooming that is
/// annoying. So i tried to come up with my own solution
///////////
function flipCityPlotLabels(string target, string vbwAxis)
    GetAxis/Q/W=$target $vbwAxis
    if(V_flag == 1) // axis doesn't exist
        return 0
    endif

    DFREF axisInfoDFR = getVBWCityPlotAxisDF()
    if(dataFolderRefStatus(axisInfoDFR)==0)
        createVBWCityPlotAxisDF()
        DFREF axisInfoDFR = getVBWCityPlotAxisDF()
    endif
    
    // Get the current min and max of the displayed axis, from the GetAxis call above
    variable/G axisInfoDFR:currentMin/N=currentMin
    variable/G axisInfoDFR:currentMax/N=currentMax
    currentMin = V_min; currentMax = V_Max

    // Determine what the last min and max were
    NVAR/Z/SDFR=axisInfoDFR lastMin, lastMax
    if(!NVar_Exists(lastMin) || numtype(lastMin) != 0)
        variable/g axisInfoDFR:lastMin/N=lastMin 
        lastMin = NaN
    endif

    if(!NVar_Exists(lastMax) || numtype(lastMax) != 0)
        variable/g axisInfoDFR:lastMax/N=lastMax 
        lastMax = NaN
    endif
    
    // If things match, we don't need to change anything. Stop the chain
    if(lastMin == currentMin && lastMax == currentMax)
        return 0
    endif

    // Get the maximum burst window
    NVAR/Z/SDFR=axisInfoDFR gMaxBW = maxBW
    variable maxBW
    if(!NVAR_Exists(gMaxBW) || gMaxBW == 0)
        maxBW = 1
    else
        maxBW = gMaxBW
    endif

    // Don't let the plot extend beyond zero or beyond the last burst window
    if(currentMin < 0 || currentMax > maxBW)
        if(currentMin < 0)
            currentMin = 0
        endif

        if(currentMax > maxBW)
            currentMax = maxBW
        endif

        SetAxis/W=$target $vbwAxis currentMin, currentMax
    endif

    variable fullRange = currentMax - currentMin

    variable roundMin, roundMax, roundDigits = 2, nTicks, roundRange
    // debugger
    if(fullRange > 0.12)
        nTicks = 6
    elseif(fullRange >0.06)
        // debugger
        roundRange = RoundToDecimal(fullRange, roundDigits, method = "floor")
        nTicks =  roundRange / 0.01
    elseif(fullRange >0.01)
        // debugger
        roundRange = RoundToDecimal(fullRange, roundDigits, method = "floor")
        nTicks =  roundRange / 0.01
    else
        // debugger
        roundDigits = 3
        nTicks = 1
    endif

    roundMin = RoundToDecimal(currentMin, roundDigits, method = "ceiling")
    roundMax = RoundToDecimal(currentMax, roundDigits, method = "floor")

    variable range = roundMax - roundMin, iTick = 0
    // within the append function, these are set as the user values for the y axis
    make/N=(nTicks)/D/O axisInfoDFR:tickValuesWave/Wave=tickValuesWave
    make/N=(nTicks)/T/O axisInfoDFR:tickLabelsWave/Wave=tickLabelsWave

    for(itick=0; itick<nTicks; itick++)
        variable increment, tickVal
        if(nTicks == 1)
            tickVal = currentMin
        else
            increment = RoundToDecimal(range/(nTicks - 1), roundDigits)
            tickVal = roundMin + increment * itick
        endif
        
        tickValuesWave[itick] = tickVal
        tickLabelsWave[itick] = num2str(maxBW - tickVal)
    endfor

    lastMin = currentMin; lastMax = currentMax
    return 1
end

