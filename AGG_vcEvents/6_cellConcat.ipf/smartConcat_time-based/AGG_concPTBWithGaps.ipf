///////////
/// smartConcForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-05
/// NOTES: Run AGG updated smart concatenation for the cell
/// Relies on previously concatenated series list, should be run before this function
/// If any of the series were stitched together from multiple sweeps, will find gap info from raw wave note
///////////
function smartConcForCell(string cellName[, string subType])
    if(paramIsDefault(subType))
        subType = "all"
    endif
    
    DFREF cellDFR = getCellSummaryDF(cellName)
    if(DataFolderRefStatus(cellDFR)!=0)
        Wave/SDFR=cellDFR selectedSeriesNums = combinedSeriesW

        Wave/SDFR=cellDFR startTimesW, durationsW 

        variable iSeries = 0, numSeries = numpnts(selectedSeriesNums)
        
        make/FREE=1/Wave/N=(numSeries) PTBs, rawWaves

        DFREF rawWavesDF = getEventsdetectWavesDF(cellName)

        for(iSeries = 0; iSeries<numSeries; iSeries++)
            variable seriesNum = selectedSeriesNums[iSeries]

            DFREF cellStoreDFR = returnCellEventsDFR_bySubType(cellName, subType)
            DFREF seriesEventsDFR = returnSeriesEventsDFR_bySubType(cellName, num2str(seriesNum), subType, eventsOrSum = "events")

            string thisWaveN = buildSeriesWaveName(cellName, seriesNum)
            Wave/SDFR=rawWavesDF rawWave = $thisWaveN
            if(!WaveExists(rawWave))
                continue
            endif

            string ptbWN = thisWaveN + "_ptb"
            Wave/SDFR=seriesEventsDFR ptbWave = $ptbWN

            if(!WaveExists(ptbWave))
                // the ptb wave doesn't exist. Don't add it to the wave
                continue
            endif

            string concatSweeps = AGG_wnoteSTRbyKey( thisWaveN, "concatSweeps", rawWavesDF, suppressWarning = 1 )
            if(strlen(concatSweeps)>0)
                string gapSTARTSstring = AGG_wnoteSTRbyKey( thisWaveN, "sweepGapStarts", rawWavesDF )
                string gapENDSstring = AGG_wnoteSTRbyKey( thisWaveN, "sweepGapEnds", rawWavesDF )

                // A bit risky to overwrite the note, but with current (2024-05-08) workflow,
                // nothing is added to the PTB wave note during detection
                // But if concatenate multiple times, will wind up with multiple versions of the concatSweeps strings
                // in the wave note if don't overwrite
                Note/NOCR/K ptbWave, "concatSweeps:" + concatSweeps + ";sweepGapStarts:" + gapStartsString + ";sweepGapEnds:" + gapEndsString + ";"
            endif

            PTBs[iSeries] = ptbWave
            rawWaves[iSeries] = rawWave
        endfor

        Sort startTimesW, startTimesW, PTBs, durationsW, rawWaves
        
        concatPTBsWithGaps(PTBs, startTimesW, durationsW, concatDFR = cellStoreDFR, rawWaves = rawWaves)
    endif
end

// TO-DO:
// Ideally, sort the three waves by the startTimes before feeding to concatPTBs
// Could be cases where loaded out of chronological order

///////////
/// concatPTBsWithGaps
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-05
/// NOTES: Provide a wave of wave of the PTBs along with a wave of their start times in seconds
/// and another wave of their durations
/// These three waves should all have the same number of points
/// Raw waves are optional - if provided, will create
///     - _scy: chopped events
///     - _scx: timing of chopped events
///     
///     - secPreEvent is amount of time to keep in chopped wave before event
///     - secPostEvent is amount of time to keep in chopped wave after event
/// ConcatWN_base shouldn't include the _sct extension
/// Going to store info about the gaps in wavenote, but have other functions to make waves
/// depending on the purpose and plotting
///////////
function/Wave concatPTBsWithGaps(Wave/Wave PTBs, wave startTimesW, wave durationsW[, string concatWN_base, DFREF concatDFR, wave/Wave rawWaves, variable secPreEvent, variable secPostEvent, variable baseOffset, variable baseDur])
    string sctExt = "_sct", scyExt = "_scy", scxExt = "_scx", gapStartsExt = "_gapStarts", gapEndsExt = "_gapEnds", gapStartsPlusEndExt = "_gapStartsPlusEnd", gapEndsPlusStartExt = "_gapEndsPlusStart"

    if(!WaveExists(PTBs) || ! waveExists(startTimesW) || !waveExists(durationsW))
        print "concatPTBsWithGaps: One of the provided waves doesn't exist"
        return $""
    endif

    variable nPTBs = numpnts(PTBs)

    if(nPTBs == 0)
        print "concatPTBsWithGaps: No PTBs provided"
        return $""
    endif

    if(nPTBs != numpnts(startTimesW) || nPTBs != numpnts(durationsW))
        print "concatPTBsWithGaps: The number of points doesn't match in all three waves"
        return $""
    endif

    Wave firstPTB = PTBs[0]

    if(!WaveExists(firstPTB))
        print "concatPTBsWithGaps: the first PTB wave doesn't exist"
        return $""
    endif

    string firstPTBwn
    DFREF firstPTBdfr

    [firstPTBwn, firstPTBdfr] = getWaveNameAndDFR(firstPTB)

    string firstPTB_cellID = getCellIDFromWaveN(firstPTBwn)

    if(paramIsDefault(concatWN_base))
        if(strlen(firstPTB_cellID)>0)
            concatWN_base = firstPTB_cellID
        else
            print "concatPTBsWithGaps: No concatenated wave name provided, and can't find cellID of first PTB wave. Quitting"
            return $""
        endif
    endif

    string concatWN
    concatWN = concatWN_base + sctExt
    
    if(paramIsDefault(concatDFR))
        if(dataFolderRefStatus(firstPTBdfr)!=0)
            concatDFR = firstPTBdfr
        else
            print "concatPTBsWithGaps: No concatenated data folder provided, and can't find data folder of first PTB wave. Quitting"
            return $""
        endif
    endif

    make/N=(0)/D/O concatDFR:$concatWN/Wave=concatWave
    make/N=(0)/D/O concatDFR:$(concatWN_base + gapStartsExt)/Wave=gapStartsWave
    make/N=(0)/D/O concatDFR:$(concatWN_base + gapEndsExt)/Wave=gapEndsWave

    variable storeChoppedEvents = 0
    DFREF cellSumPanelDFR = getEventsCellSumOutDF()
    if(dataFolderRefStatus(cellSumPanelDFR)!=0)
        NVAR/Z/SDFR=cellSumPanelDFR gSecPreEvent = secPreEvent, gSecPostEvent = secPostEvent, gBaseOffset = baseOffset, gBaseDur = baseDur
    endif
    if(!paramIsDefault(rawWaves))
        if(WaveExists(rawWaves) && numpnts(rawWaves) == nPTBs)
            storeChoppedEvents = 1
        endif

        if(paramIsDefault(secPreEvent))
            if(numType(gSecPreEvent) == 0)
                secPreEvent = gSecPreEvent
            else
                secPreEvent = 0.001 // default to 1ms
            endif
        endif
        
        if(paramIsDefault(secPostEvent))
            if(numType(gSecPostEvent) == 0)
                secPostEvent = gSecPostEvent
            else
                secPostEvent = 0.001 // default to 1ms
            endif
        endif
        
        if(paramIsDefault(baseOffset))
            if(numType(gbaseOffset) == 0)
                baseOffset = gbaseOffset
            else
                baseOffset = 0.002 // default to 2ms
            endif
        endif
        
        if(paramIsDefault(baseDur))
            if(numType(gbaseDur) == 0)
                baseDur = gbaseDur
            else
                baseDur = 0.005 // default to 2ms
            endif
        endif
        
    endif

    if(storeChoppedEvents)
        string scyWN = concatWN_base + scyExt
        string scxWN = concatWN_base + scxExt
        make/N=(0)/D/O concatDFR:$scyWN/Wave=choppedEventsY
        make/N=(0)/D/O concatDFR:$scxWN/Wave=timeChoppedEvents
    endif
    
    variable iPTB = 0, absStart, waveStart, waveEnd, waveDur, prevStart, prevEnd
    variable gapDur, totalGapDur = 0, totalRecDur = 0, totalDur = 0
    string gapStarts = "", gapEnds = ""
    variable/G concatDFR:totalDur/N=gTotalDur=NaN
    variable/G concatDFR:totalRecDur/N=gTotalRecDur=NaN
    variable/G concatDFR:totalGapDur/N=gtotalGapDur=NaN

    for(iPTB=0; iPTB<nPTBs; iPTB++)
        wave thisPTB = PTBs[iPTB]
        if(!WaveExists(thisPTB))
            print "concatPTBsWithGaps: Can't find one of the PTB waves"
            continue
        endif

        waveStart = startTimesW[iPTB]
        waveDur = durationsW[iPTB]

        string thisPTBN
        DFREF thisPTBdfr
        [thisPTBN, thisPTBdfr] = getWaveNameAndDFR(thisPTB)

        string concatSweeps = AGG_wnoteSTRbyKey( thisPTBN, "concatSweeps", thisPTBdfr, suppressWarning = 1 )
        variable hasConcatSweeps = 0
        if(strlen(concatSweeps)>0)
            hasConcatSweeps = 1
            string sweepGapStartsStr = AGG_wnoteSTRbyKey( thisPTBN, "sweepGapStarts", thisPTBdfr )
            string sweepGapEndsStr = AGG_wnoteSTRbyKey( thisPTBN, "sweepGapEnds", thisPTBdfr )

            Wave sweepGapStarts = makeFreeWaveFromStringList(sweepGapStartsStr)
            Wave sweepGapEnds = makeFreeWaveFromStringList(sweepGapEndsStr)
            make/Free/D/N=(numpnts(sweepGapStarts)) sweepGapDurs
            sweepGapDurs = sweepGapEnds - sweepGapStarts
        endif

        if(iPTB == 0)
            absStart = waveStart
            duplicate/O/FREE thisPTB, thisPTBToAdd
            gapDur = 0
        else
            gapDur = waveStart - prevEnd
            if(gapDur <0)
                print "concatPTBsWithGaps: Problem with chronological order.", nameOfWave(thisPTB), "starts before end of previous wave."
                continue
            endif

            // End of the previous series
            gapEndsWave[numpnts(gapEndsWave)] = {waveStart - absStart} // {} appends the point to the end
            // gapEnds += num2str(waveStart - absStart) + ","

            if(hasConcatSweeps)
                sweepGapStarts += waveStart - absStart
                sweepGapEnds += waveStart - absStart
            endif

            duplicate/O/FREE thisPTB, thisPTBToAdd
            // add the difference from start of this wave to the absolute start
            // to the PTB waves
            thisPTBToAdd += waveStart - absStart
        endif

        // Concatenate thisPTB into SCT wave
        Concatenate/NP {thisPTBToAdd}, concatWave

        // add to gap, recording, and total durations
        totalGapDur += gapDur
        totalRecDur += waveDur
        totalDur += waveDur + gapDur

        if(hasConcatSweeps)
            concatenate/NP {sweepGapStarts}, gapStartsWave
            concatenate/NP {sweepGapEnds}, gapEndsWave
            // gapStarts += convertNumWaveToStr(sweepGapStarts)
            // gapEnds += convertNumWaveToStr(sweepGapEnds)

            totalGapDur += sum(sweepGapDurs)
            totalRecDur -= sum(sweepGapDurs)
        endif
        

        if(iPTB != nPTBs - 1)
            prevStart = waveStart
            waveEnd = waveStart + waveDur
            prevEnd = waveEnd

            gapStartsWave[numpnts(gapStartsWave)] = {waveEnd - absStart} // {} appends the point to the end
        endif

        if(storeChoppedEvents)
            Wave thisRawWave = rawWaves[iPTB]
            if(!WaveExists(thisRawWave))
                continue
            endif

            [Wave choppedYWave, wave choppedXTimeWave] = getChoppedEvents(thisRawWave, thisPTB, offsetX = waveStart - absStart, secPreEvent = secPreEvent, secPostEvent = secPostEvent, baseOffset = baseOffset, baseDur = baseDur)
            if(!WaveExists(choppedYWave) || !WaveExists(choppedXTimeWave))
                continue
            endif
            
            concatenate/NP {choppedYWave}, choppedEventsY
            concatenate/NP {choppedXTimeWave}, timeChoppedEvents
            copyDataScale(thisRawWave, choppedEventsY) // will set to whatever scale the last raw wave has, should all be the same
        endif
    endfor

    if(storeChoppedEvents)
        [Wave gapPlotX, Wave gapPlotY] = makeGapPlotWaves(gapStartsWave, gapEndsWave, baseN = concatWN_base, dfrForPlotWaves = concatDFR, addStartEnd = 1, overallEndTime = totalDur)
    endif

    gTotalDur = totalDur
    gTotalRecDur = totalRecDur
    gtotalGapDur = totalGapDur

    string wnote = "DURATION:" + num2str( totalDur ) + ";" + "RECDURATION:" + num2str( totalRecDur ) + ";" + "GAPDURATION:" + num2str( totalGapDur ) + ";"

    duplicate/O gapStartsWave, concatDFR:$(concatWN_base + gapStartsPlusEndExt)
    Wave/SDFR=concatDFR gapStartsPlusEndWave = concatDFR:$(concatWN_base + gapStartsPlusEndExt)
    gapStartsPlusEndWave[numpnts(gapStartsPlusEndWave)] = {totalDur}
    
    duplicate/O gapEndsWave, concatDFR:$(concatWN_base + gapEndsPlusStartExt)
    Wave/SDFR=concatDFR gapEndsPlusStartWave = concatDFR:$(concatWN_base + gapEndsPlusStartExt)
    InsertPoints/V=0 0, 1, gapEndsPlusStartWave
    
    gapStarts = convertNumWaveToStr(gapStartsWave)
    gapEnds = convertNumWaveToStr(gapEndsWave)
    string gapStartsPlusEnd = convertNumWaveToStr(gapStartsPlusEndWave)
    string gapEndsPlusStart = convertNumWaveToStr(gapEndsPlusStartWave)

    // AGG - add gapStartsPlusEnd and gapEndsPlusStart
    wnote += "GAPSTARTS:" + gapStarts + ";" + "GAPENDS:" + gapEnds + ";" + "gapStartsPlusEnd:" + gapStartsPlusEnd + ";" + "gapEndsPlusStart:" + gapEndsPlusStart + ";" 

    Note/NOCR concatWave, wNote

    if(totalGapDur / totalDur > 0.01)
        print "concatPTBsWithGap: WARNING: Gap duration exceeds 1% of total duration", "gaps:", totalGapDur, "duration:", totalDur, "ratio:", totalGapDur / totalDur
    endif 
end

//\\//\\//\\//\\//\\//\\//\\// GAP COMPONENTS \\//\\//\\//\\//\\//\\//\\//
    ///////////
    /// makeGapPlotWaves
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-04
    /// NOTES: 
    ///////////
    function [Wave/D gapPlotX, Wave/D gapPlotY] makeGapPlotWaves(Wave gapStarts, Wave gapEnds[, string baseN, DFREF dfrForPlotWaves, variable addStartEnd, variable overallEndTime, variable overallStartTime])
        if(!WaveExists(gapStarts) || !WaveExists(gapEnds))
            return [$"", $""]
        endif

        string gapStartsWN
        DFREF gapStartsDFR
        [gapStartsWN, gapStartsDFR] = getWaveNameAndDFR(gapStarts)

        if(paramIsDefault(baseN))
            baseN = getWaveBasename(gapStartsWN)
        endif
        
        if(paramIsDefault(dfrForPlotWaves))
            DFREF dfrForPlotWaves = gapStartsDFR
        endif

        if(paramIsDefault(overallStartTime))
            overallStartTime = 0
        endif
        

        if(paramIsDefault(addStartEnd) || paramIsDefault(overallEndTime))
            addStartEnd = 0
        endif

        if(dataFolderRefStatus(dfrForPlotWaves)==0)
            print "Can't find the inteded folder to store the plot waves. Defaulting to root"
            DFREF dfrForPlotWaves = root:
        endif

        variable nGapStarts = numpnts(gapStarts), nGapEnds = numpnts(gapEnds)
        variable beginsWithStart = 0, finishesWithEnd = 0
        if((nGapStarts + nGapEnds) > 0) // at least one gap 
            variable firstGapStart = NaN, firstGapEnd = NaN, lastGapStart = NaN, lastGapEnd = NaN
            if(nGapStarts>0)
                firstGapStart = gapStarts[0]
                lastGapStart = gapStarts[nGapStarts-1]
            endif
            if(nGapEnds>0)
                firstGapEnd = gapEnds[0]
                lastGapEnd = gapEnds[nGapEnds-1]
            endif

            if((nGapStarts > 0 && nGapEnds == 0) || firstGapStart < firstGapEnd)
                beginsWithStart = 1
            endif
            if((nGapEnds > 0 && nGapStarts == 0) || lastGapEnd > lastGapStart)
                finishesWithEnd = 1
            endif
        endif

        variable numGaps = nGapStarts + nGapEnds
        
        variable iGap = 0, iGapPlot = 0

        variable maxPlotPoints = numGaps * 2 // two points each for gaps starts/end

        if(paramIsDefault(addStartEnd) || paramIsDefault(overallEndTime))
            addStartEnd = 0
        endif

        make/N=(maxPlotPoints)/D/O dfrForPlotWaves:$(baseN+"_gapPlotX")/Wave=gapPlotX
        make/N=(maxPlotPoints)/D/O dfrForPlotWaves:$(baseN+"_gapPlotY")/Wave=gapPlotY
        gapPlotX = 0
        gapPlotY = 0 // is it a gap (1) or not(0)
        
        if(numGaps == 0)
            // print "no gaps"
            return [gapPlotX, gapPlotY]
        endif

        variable thisGapStart, thisGapRelStart
        variable thisGapEnd, thisGapRelEnd


        if(!beginsWithStart)
            thisGapEnd = gapEnds[0]
            thisGapRelEnd = thisGapEnd - overallStartTime
            gapPlotX[iGapPlot] = thisGapEnd
            gapPlotY[iGapPlot] = 1
            iGapPlot ++

            gapPlotX[iGapPlot] = thisGapEnd
            gapPlotY[iGapPlot] = 0
            iGapPlot ++
        endif

        for(iGap = 0; iGap < nGapStarts; iGap ++ )
            thisGapStart = gapStarts[iGap]
            thisGapRelStart = thisGapStart - overallStartTime
            
            gapPlotX[iGapPlot] = thisGapStart
            gapPlotY[iGapPlot] = 0
            iGapPlot ++ 

            gapPlotX[iGapPlot] = thisGapStart
            gapPlotY[iGapPlot] = 1
            iGapPlot ++

            variable iGapEnd = iGap
            if(!beginsWithStart)
                iGapEnd = iGap + 1
            endif
            if(iGapEnd < nGapEnds)
                thisGapEnd = gapEnds[iGapEnd]
                thisGapRelEnd = thisGapEnd - overallStartTime
                gapPlotX[iGapPlot] = thisGapEnd
                gapPlotY[iGapPlot] = 1
                iGapPlot ++

                gapPlotX[iGapPlot] = thisGapEnd
                gapPlotY[iGapPlot] = 0
                iGapPlot ++    
            endif
        endfor

        if(addStartEnd)
            insertPoints 0, 1, gapPlotX, gapPlotY
            gapPlotX[0] = overallStartTime
            if(beginsWithStart)
                gapPlotY[0] = 0
            else
                gapPlotY[0] = 1
            endif

            gapPlotX[numpnts(gapPlotX)] = {overallEndTime}
            if(finishesWithEnd)
                gapPlotY[numpnts(gapPlotY)] = {0}
            else
                gapPlotY[numpnts(gapPlotY)] = {1}
            endif

            // if want to see vertical line at start and end
            // insertPoints 0, 2, gapPlotX, gapPlotY
            // gapPlotX[0] = overallStartTime
            // gapPlotY[0] = 1

            // gapPlotX[1] = overallStartTime
            // gapPlotY[1] = 0

            // gapPlotX[numpnts(gapPlotX)] = {overallEndTime}
            // gapPlotY[numpnts(gapPlotY)] = {0}
            // gapPlotX[numpnts(gapPlotX)] = {overallEndTime}
            // gapPlotY[numpnts(gapPlotY)] = {1}
            // gapPlotX[numpnts(gapPlotX)] = {overallEndTime}
            // gapPlotY[numpnts(gapPlotY)] = {0}
        endif
    end



//\\//\\//\\//\\//\\//\\ CHOPPED EVENTS COMPONENTS //\\//\\//\\//\\//\\//\\//\\

    ///////////
    /// getChoppedEvents
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2024-05-05
    ///////////
    function [Wave/D choppedYWave, Wave/D choppedXTimeWave] getChoppedEvents(Wave rawWave, Wave ptbWave[, variable offsetX, variable secPreEvent, variable secPostEvent, variable baseOffset, variable baseDur])
        if(!WaveExists(rawWave) || !WaveExists(ptbWave))
            return [$"", $""]
        endif

        variable nEvents = numpnts(ptbWave)

        make/FREE/D/N=0 choppedYWave
        make/FREE/D/N=0 choppedXTimeWave
        if(nEvents == 0)
            return [choppedYWave, choppedXTimeWave]
        endif

        DFREF cellSumPanelDFR = getEventsCellSumOutDF()
        if(dataFolderRefStatus(cellSumPanelDFR)!=0)
            NVAR/Z/SDFR=cellSumPanelDFR gSecPreEvent = secPreEvent, gSecPostEvent = secPostEvent, gBaseOffset = baseOffset, gBaseDur = baseDur
        endif
        
        if(paramIsDefault(secPreEvent))
            if(numType(gSecPreEvent) == 0)
                secPreEvent = gSecPreEvent
            else
                secPreEvent = 0.001 // default to 1ms
            endif
        endif
        
        if(paramIsDefault(secPostEvent))
            if(numType(gSecPostEvent) == 0)
                secPostEvent = gSecPostEvent
            else
                secPostEvent = 0.001 // default to 1ms
            endif
        endif
        
        if(paramIsDefault(baseOffset))
            if(numType(gbaseOffset) == 0)
                baseOffset = gbaseOffset
            else
                baseOffset = 0.002 // default to 2ms
            endif
        endif
        
        if(paramIsDefault(baseDur))
            if(numType(gbaseDur) == 0)
                baseDur = gbaseDur
            else
                baseDur = 0.005 // default to 2ms
            endif
        endif
        
        variable iEvent = 0
        for(variable peakTime : ptbWave)
            duplicate/FREE/R=(peakTime - secPreEvent - deltaX(rawWave), peakTime + secPostEvent) rawWave, eventYWave
            duplicate/FREE/R=(peakTime - secPreEvent - deltaX(rawWave), peakTime + secPostEvent) rawWave, eventXWave
            
            // Set x-wave equal to time (x value)
            eventXWave = x
            if(!paramIsDefault(offsetX))
                eventXWave += offsetX
            endif

            eventYWave[0] = nan // blank the first point to disconnect events in graph
            
            // Adjust for baseline average before event
            WaveStats/Q/R=(peakTime-baseOffset-baseDur, peakTime-baseOffset) rawWave
            eventYWave -= V_avg 

            concatenate/NP {eventYWave}, choppedYWave
            concatenate/NP {eventXWave}, choppedXTimeWave
        endfor

        return [choppedYWave, choppedXTimeWave]
    end