///////////
/// getCellSCTWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: Get the full duration SCT wave for a cell
///////////
function/Wave getCellSCTWave(string cellName)
    if(strlen(cellName)==0)
        return $""
    endif

    DFREF cellDFR = getCellSummaryDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        return $""
    endif

    Wave/SDFR=cellDFR sctWave = $(cellName + "_sct")
    if(!WaveExists(sctWave))
        // SCT wave doesn't exist, haven't yet concatenated
        return $""
    endif

    return sctWave
end