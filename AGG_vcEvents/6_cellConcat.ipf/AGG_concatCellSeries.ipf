// Get the information about the selected cell and the selected series (potentially multiple)
// In the detect tab to then add to the detection listbox
function concatSelectedCellSeries()
    string cellName = getSelectedItem("cellListBox_cellConcat", hostName = "AGG_events")
    Wave selectedSeriesNums = getSelectedSeries_cellSum() 

    if(strlen(cellName)>0)
        // print "selected", cellName
        DFREF cellDFR = getCellSummaryDF(cellName)
        if(DataFolderRefStatus(cellDFR)==0)
            createCellSummaryDF(cellName)
        else
            killDataFolder cellDFR // left off Z so it gives error if problem
            createCellSummaryDF(cellName)
        endif

        // Kill any existing stored region waves, prevents coloration when things aren't actually stored
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
        KillWaves/Z cellInfoDFR:regionsNames_stored, cellInfoDFR:regionsStarts, cellInfoDFR:regionsEnds

        DFREF cellDFR = getCellSummaryDF(cellName)
        duplicate/O selectedSeriesNums, cellDFR:combinedSeriesW/Wave=seriesNumsWave
        Sort seriesNumsWave, seriesNumsWave

        // check that detection has been done on the selected series
        if(numpnts(seriesNumsWave) > 0)
            variable row = numpnts(seriesNumsWave) - 1
            do // work from the end, b/c deleting point changes the index
                variable seriesNum = seriesNumsWave[row]
                variable deleteRow = 0
                if(numtype(seriesNum)!=0 || seriesNum <= 0)
                    deleteRow = 1
                else
                    DFREF eventsDFR = getEventsDetectWavesDF(cellName)
                    string seriesBN = buildSeriesWaveName(cellName, seriesNum)
                    if(!WaveExists(eventsDFR:$(seriesBN + "_ptb")))
                        print "concatSelectedCellSeries: No PTB wave for selected series", seriesNum, ". Have you detected?"
                        deleteRow = 1
                    endif
                endif
                if(deleteRow)
                    DeletePoints row,1, seriesNumsWave 
                endif

                row = row - 1
            while( row >= 0 )
        endif

        updateConcSeriesLB(cellName)
        if(numPnts(seriesNumsWave)>0)
            // LOTS of things happen within this function
            // The actual concatenation of events waves happens within the
            // concatEventsForCell function
            // The subset events waves are created within them 
            concatAndCalcAvgForCell(cellName)
        endif

    endif
end


///////////
/// concatAndCalcAvgForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// NOTES: This function will concatenate based on combinedSeriesW 
/// for a given cell, and also update the calculation for the cellAvgs
///////////
function concatAndCalcAvgForCell(string cellName)
    DFREF cellDFR = getCellSummaryDF(cellName)
    
    if(dataFolderRefStatus(cellDFR)==0)
        print "concatAndCalcAvgForCell: cell summary df doesn't exist yet for", cellName
        return 0
    endif

    Wave/SDFR=cellDFR selectedSeriesNums = combinedSeriesW
    variable iSeries = 0, numSeries = numpnts(selectedSeriesNums)

    makeCellSummaryDFandWaves(cellName, "all")
    makeCellSummaryDFandWaves(cellName, "avg")
    makeCellSummaryDFandWaves(cellName, "limited")
    makeCellSummaryDFandWaves(cellName, "interval")
    
    variable maxNumEvents, maxIntNumEvents
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    if(DataFolderRefStatus(useAvgDFR)!=0)
        NVAR/Z/SDFR = useAvgDFR globalMax = maxEventsPerCell, globalIntMax = intMaxEventsPerCell
        maxNumEvents = globalMax
        maxIntNumEvents = globalIntMax
    else
        maxNumEvents = NaN; maxIntNumEvents = NaN
    endif

    // This function makes the subset waves within each series folder
    calcSeriesSubsetWavesForCell(cellName, maxNumEvents, maxIntNumEvents, selectedSeriesNums)

    for(iSeries = 0; iSeries<numSeries; iSeries++)
        variable seriesNum = selectedSeriesNums[iSeries]
        
        calcSeriesEventAvgs(cellName, seriesNum) // recalc the average for the series

        // These functions are concatenating the series averages into output waves
        // which have a length equal to the number of series being concatenated
        concatCellSeries(cellName, seriesNum, "all")
        concatCellSeries(cellName, seriesNum, "avg")
        concatCellSeries(cellName, seriesNum, "interval")
        concatCellSeries(cellName, seriesNum, "limited")
    endfor

    // The actual concatenation of events happens within this function
    // in a subfunction called "concatEventsForCell"
    calcCellAvgs(cellName) // calculate the averages for the concatenated cell

    // Do PTB concatenations
    smartConcForCell(cellName, subType = "all")
    smartConcForCell(cellName, subType = "avg")
    smartConcForCell(cellName, subType = "interval")
    smartConcForCell(cellName, subType = "limited")

    makeCellRegionFolders(cellName)
    updateRegions(cellName)
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-16
/// NOTES: Added sumType to specify the folder for creation/storage of waves
function makeCellSummaryDFandWaves(string cellName, string sumType)
    strswitch (sumType)
        case "all":
            createCellSummaryDF(cellName)
            DFREF cellDFR = getCellSummaryDF(cellName)
            break
        case "avg":
            createCellAvgSubDF(cellName)
            DFREF cellDFR = getCellAvgSubDF(cellName)
            break
        case "limited":
            createCellAvgSubLimitedDF(cellName)
            DFREF cellDFR = getCellAvgSubLimitedDF(cellName)
            break
        case "interval":
            createCellIntSubDF(cellName)
            DFREF cellDFR = getCellIntSubDF(cellName)
            break
        default:
            createCellSummaryDF(cellName)
            DFREF cellDFR = getCellSummaryDF(cellName)
            break
    endswitch

    if(dataFolderRefStatus(cellDFR)==0)
        print "Cell summary folder does not exist for cell", cellName, "type", sumType
        return 0
    endif
    

    Struct cellSummaryInfo cellSum
    StructFill/SDFR=cellDFR/AC=3 cellSum
    fillStringsinCellSummaryInfo(cellSum) // updates the numericWaves string

    variable iWave = 0
    variable numWaves = itemsInList(cellSum.numericWaves)

    for(iWave = 0; iWave < numWaves; iWave ++)
        string thisWave = StringFromList(iWave, cellSum.numericWaves)
        if(!stringMatch(thisWave, "combinedSeriesW"))
            // print "analysis wave", thisWave
            redimension/N=0 cellDFR:$thisWave
        endif
    endfor
end

// this function adds the series's average values as rows in the
// corresponding waves for each cell
// This isn't the function that calculates the average value
// for parameters such as amplitude or FWHM for a cell.
// These are calculated by concatenating all of the events info waves (for indiv events) for the cell
// and then getting the average of these concatenated waves
function concatCellSeries(string cellName, variable seriesNum, string subType)
    DFREF panelDFR = getEventsPanelDF()
    DFREF panelCellDFR = getEventsCellSumOutDF()

    variable doPassive
    strswitch (subType)
        case "all":
            doPassive = 1
            DFREF cellDFR = getCellSummaryDF(cellName)
            DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
            break
        case "avg":
            DFREF cellDFR = getCellAvgSubDF(cellName)
            DFREF seriesDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
            break
        case "interval":
            DFREF cellDFR = getCellIntSubDF(cellName)
            DFREF seriesDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
            break
        case "limited":
            DFREF cellDFR = getCellAvgSubLimitedDF(cellName)
            DFREF seriesDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
            break
        default:
            doPassive = 1
            DFREF cellDFR = getCellSummaryDF(cellName)
            DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
            break
    endswitch

    if(dataFolderRefStatus(cellDFR)==0)
        print "cellDFR doesn't exist"
        return 0 
    endif

    Struct cellSummaryInfo cellSum
    StructFill/SDFR=cellDFR cellSum
    fillStringsinCellSummaryInfo(cellSum) // updates the numericWaves string
    


    if(dataFolderRefStatus(seriesDFR)==0)
        print "No series folder for cell", cellName, "series", seriesNum, "Have you detected?"
    endif

    addNumericEventsData(cellSum, cellDFR, seriesDFR)

    variable isExtracellular = checkIsExtracellular()
    if(doPassive & !isExtracellular)
        addRelatedPassiveSeriesData(cellSum, cellDFR, seriesDFR, cellName)

        // Check if series detections were confirmed
        // Print a warning, but allow to proceed
        variable didConfirm = 0
        
        variable/G seriesDFR:confirmedDetect/N=confirmedDetect
        if(numtype(confirmedDetect)==0)
            if(confirmedDetect)
                didConfirm = 1
            endif
        endif

        if(!didConfirm)
            print "WARNING: Detection for cell", cellName, "series", seriesNum, "was not confirmed"
        endif
    endif
end

function addRelatedPassiveSeriesData(cellSum, cellDFR, seriesDFR, cellName)
    STRUCT cellSummaryInfo &cellSum
    DFREF cellDFR, seriesDFR
    string cellName

    NVAR/Z/SDFR = seriesDFR passiveSeries1, passiveSeries2
    // print "pass1", passiveSeries1, "pass2", passiveSeries2
    
    checkAndAddPassiveSeriesData(cellSum, cellDFR, cellName, passiveSeries1)
    checkAndAddPassiveSeriesData(cellSum, cellDFR, cellName, passiveSeries2)
end

function checkAndAddPassiveSeriesData(cellSum, cellDFR, cellName, passSeriesNum)
    STRUCT cellSummaryInfo &cellSum
    DFREF cellDFR
    string cellName
    variable passSeriesNum

    if(numtype(passSeriesNum)!=0)
        return NaN
    endif

    FindValue/V=(passSeriesNum)/T=0.1 cellSum.passivesW
    if(V_Value == -1) // not found
        // Add this passive series number to the passive series wave
        variable numPassives = numpnts(cellSum.passivesW)
        redimension/N=(numPassives+1) cellSum.passivesW
        cellSum.passivesW[numPassives] = passSeriesNum
        
        // Add the values for each passive property to their respective waves
        variable iPass = 0, numPassProps = itemsinlist(cellSum.passiveSeriesWaves)
        for(iPass = 0; iPass<numPassProps; iPass++)
            string thisWaveName = StringFromList(iPass, cellSum.passiveSeriesWaves)
            // print thisWaveName, "in concatCellSeries"
            string thisVarName = stringbykey(thisWaveName, cellSum.numericVarNames)
            Wave/SDFR=cellDFR sumWave = $thisWaveName

            if(WaveExists(sumWave))
                variable numSeries = numpnts(sumWave)
                redimension/N=(numSeries + 1) sumWave
                
                DFREF seriesDFR = getSeriesDF(cellName, num2str(passSeriesNum))
                if(dataFolderRefStatus(seriesDFR)==0) // passive series data folder doesn't exist
                    continue
                endif
                NVAR/Z/SDFR=seriesDFR seriesVar = $thisVarName
                if(numtype(seriesVar)!=0)
                    continue
                endif
                sumWave[numSeries] = seriesVar
            else
                print thisWaveName, "does not exist"
            endif
        endfor
    endif
end

function addNumericEventsData(cellSum, cellDFR, seriesDFR)
    STRUCT cellSummaryInfo &cellSum
    DFREF cellDFR, seriesDFR

    variable iWave = 0
    variable numEventsWaves = itemsInList(cellSum.eventSeriesWaves)

    for(iWave = 0; iWave < numEventsWaves; iWave ++)
        string thisWaveName = StringFromList(iWave, cellSum.eventSeriesWaves)
        // print thisWaveName, "in concatCellSeries"
        string thisVarName = stringbykey(thisWaveName, cellSum.numericVarNames)
        Wave/SDFR=cellDFR sumWave = $thisWaveName

        if(WaveExists(sumWave))
            variable numSeries = numpnts(sumWave)
            redimension/N=(numSeries + 1) sumWave
            NVAR/Z/SDFR=seriesDFR seriesVar = $thisVarName
            sumWave[numSeries] = seriesVar
        else
            print thisWaveName, "does not exist"
        endif
    endfor
end