///////////
/// getRegionsStartsEnds
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: Get the regionsStarts and regionsEnds wave for a cell
/// always returns the waves in seconds, adjusting the _entry waves if necessary
///////////
function[Wave regionsStarts, wave regionsEnds] getRegionsStartsEnds(string cellName)
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)

    if(dataFolderRefStatus(cellInfoDFR)==0)
        return [$"", $""]
    endif

    Wave/SDFR=cellInfoDFR regionsStarts_entry, regionsEnds_entry

    if(!WaveExists(regionsStarts_entry) || !WaveExists(regionsEnds_entry))
        return [$"", $""]
    endif

    variable isOkay = checkEnteredRegionTimes(cellName = cellName)

    if(!isOkay)
        print "getRegionsStartsEnds: There was a problem with the entered region information. Please resolve and try again"
        return [$"", $""]
    endif

    variable regionTimeUnits = getRegionUnitsForCell(cellName)

    if(numType(regionTimeUnits) != 0)
        print "getRegionsStartsEnds: There is no value for the region time units for cell", cellName
        return [$"", $""]
    else
        if(!(regionTimeUnits == 1 || regionTimeUnits == 2 || regionTimeUnits == 3))
            print "getRegionsStartsEnds: Region time units for cell doesn't match seconds (1) minutes (2) or time (3)", cellName
            return [$"", $""]
        endif
    endif

    duplicate/O regionsStarts_entry, cellInfoDFR:regionsStarts/Wave=regionsStarts
    duplicate/O regionsEnds_entry, cellInfoDFR:regionsEnds/Wave=regionsEnds

    switch (regionTimeUnits)
        case 1: // seconds
            // don't need to change anything
            break
        case 2: // minutes
            regionsStarts *= 60                
            regionsEnds *= 60                
            break
        case 3:
            DFREF cellSumDFR = getCellSummaryDF(cellName)
            variable overallStart = 0
            if(dataFolderRefStatus(cellSumDFR)!=0)
                NVAR/Z/SDFR=cellSumDFR seriesTime
                if(NVAR_Exists(seriesTime) && numtype(seriesTime)==0)
                    overallStart = seriesTime
                endif
            endif
            regionsStarts -= overallStart
            regionsEnds -= overallStart
            break
        default:
            print "getRegionsStartsEnds: regionTimeUnits for cell doesn't match seconds (1) minutes (2) or time (3)" // should be caught above
            return [$"", $""] 
            break
    endswitch

    return [regionsStarts, regionsEnds]
end

///////////
/// getCellRegionStartEndTime
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: Return the start and end time in seconds for *a region* within a cell
/// based on the startTime/endTime variables that are saved within that region's folder
///////////
function[variable retStartTime, variable retEndTime] getCellRegionStartEndTime(string cellName, string regionName)
    DFREF cellRegionDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = regionName)
    if(dataFolderRefStatus(cellRegionDFR)==0)
        // print "getCellRegionStartEndTime: Couldn't find region folder"
        return [NaN, NaN]
    endif

    if(stringmatch(regionName, "Full duration"))
        NVAR/Z/SDFR=cellRegionDFR totalDur
        if(!NVAR_Exists(totalDur))
            return [NaN, NaN]
        endif

        return [0, totalDur]
    endif

    NVAR/Z/SDFR=cellRegionDFR startTime, endTime
    if(!NVAR_Exists(startTime) || !NVAR_Exists(endTime) )
        return [NaN, NaN]
    endif

    return [startTime, endTime]
end