///////////
/// makeCellRegionFolders
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Makes the region folder for all subtypes of data
/// if optional startTime, endTime are provided, they will be saved as global variables within the folders
///////////
function makeCellRegionFolders(string cellName[, string regionName, variable startTime, variable endTime])
    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1
    endif
    

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)
        if(!specRegion)
            DFREF regionDFR = makeCellRegionsDFR_bySubType(cellName, subType)
        else
            DFREF regionDFR = makeCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)
        endif

        if(dataFolderRefStatus(regionDFR)==0)
            continue 
        endif

        if(!paramIsDefault(startTime))
            variable/G regionDFR:startTime/N=gStartTime
            gStartTime = startTime
        endif
        
        if(!paramIsDefault(endTime))
            variable/G regionDFR:endTime/N=gEndTime
            gEndTime = endTime
        endif        
    endfor
end


///////////
/// deleteExistingRegions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Deletes the existing regions within each subset's region folder
///////////
function deleteExistingRegions(string cellName)
    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)
        DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, subType)
        if(dataFolderRefStatus(regionDFR)==0)
            continue
        endif

        Wave/DF childDFRsWave = getChildDFRWave(regionDFR)
        variable iChild = 0, nChildren = numpnts(childDFRsWave)
        if(nChildren == 0)
            continue
        endif

        for(DFREF childDFR : childDFRsWave)
            KillDataFolder/Z childDFR
        endfor
    endfor

    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellInfoDFR)==0)
        return NaN
    endif
    Wave/SDFR=cellInfoDFR regionsNames_stored, regionsStarts, regionsEnds
    KillWaves/Z regionsNames_stored, regionsStarts, regionsEnds
end

///////////
/// getAllRegionNames
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Finds all of the regions folders for all cells, removes duplicates, and provides a text wave of all region names
///////////
function/Wave getAllRegionNames()
    DFREF panelDFR = getEventsPanelDF()
    if(dataFolderRefStatus(panelDFR)==0)
        print "getAllRegionNames: couldn't find panelDFR. Quitting"
        abort
    endif

    Wave/SDFR=panelDFR/T cellNames = cellName
    if(!WaveExists(cellNames))
        print "getAllRegionNames: couldn't find cell names wave. Quitting"
        abort
    endif

    variable iCell = 0, nCells = numpnts(cellNames)
    string regionsNames = ""
    for(iCell=0; iCell<nCells; iCell++)
        string cellName = cellNames[iCell]

        DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
        if(dataFolderRefStatus(regionDFR)==0)
            continue
        endif

        // If the regions name stored version exists, prefer that, as it will be sorted by times for each cell
        Wave/SDFR=getEventsCellInfoDF(cellName)/T regionsNames_stored
        if(WaveExists(regionsNames_stored))
            for(string regionName : regionsNames_stored)
                regionsNames += regionName + ";"
            endfor
        else
            regionsNames += getChildDFRList(regionDFR) 
        endif

    endfor

    wave/T regionsNamesAsWave = listToTextWave(regionsNames, ";")
    if(numpnts(regionsNamesAsWave) <= 1)
        return regionsNamesAsWave
    else
        FindDuplicates/FREE/RT=regionsNames_noDups regionsNamesAsWave
        return regionsNames_noDups
    endif
end

///////////
/// storeAllRegionsNamesWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function/Wave storeAllRegionsNamesWave()
    Wave/T allRegions = getAllRegionNames()

    DFREF cellsOutDFR = getAllCellsOutputDF()

    duplicate/O/t allRegions, cellsOutDFR:allRegionNames/Wave=allRegionNames

    insertPoints 0, 1, allRegionNames
    allRegionNames[0] = "Full duration"
    return allRegionNames
end

///////////
/// storeAllRegionsSelectWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function/Wave storeAllRegionsSelectWave(Wave/T allRegionNames[, string whichSelWave])
    variable nAllRegions = numpnts(allRegionNames)

    if(paramIsDefault(whichSelWave))
        whichSelWave = "events"
    endif

    strswitch (whichSelWave)
        case "events":
            DFREF cellsOutDFR = getAllCellsOutputDF()
            make/D/O/N=(nAllRegions) cellsOutDFR:allRegionNames_sel/Wave=allRegionNames_sel
            
            break
        case "burstAnalysis":
            DFREF burstPanelDFR = getVBWPanelDF()
            make/D/O/N=(nAllRegions) burstPanelDFR:allRegionNames_sel/Wave=allRegionNames_sel
            break
        case "maxBW":
            DFREF burstPanelDFR = getVBWMaxBWPlotDF()
            make/D/O/N=(nAllRegions) burstPanelDFR:allRegionNames_sel/Wave=allRegionNames_sel
            break
        case "regionsOutput":
            DFREF burstPanelDFR = getVBWallCellsOutDF()
            make/D/O/N=(nAllRegions) burstPanelDFR:allRegionNames_sel/Wave=allRegionNames_sel
            break
        default:
            return $""
            break
    endswitch
    

    allRegionNames_sel = 0
    if(nAllRegions>0)
        allRegionNames_sel[0] = 1
    endif

    return allRegionNames_sel
end