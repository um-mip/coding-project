///////////
/// makeCellRegionWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: the _entry waves are what are displayed in the panel. These could be either in seconds or minutes
/// depending on user choice for units in the panel
/// The regionsStarts and regionsEnds waves will always be in seconds
///////////
function makeCellRegionWaves(string cellName)
    DFREF cellDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        print "makeCellRegionWaves:", cellName, "does not have an info folder. Quitting"
        abort 
    endif

    Wave/SDFR=cellDFR/D regionsStarts_entry, regionsEnds_entry, regionsStarts, regionsEnds
    Wave/SDFR=cellDFR/T regionsNames

    if(!WaveExists(regionsNames))
        make/O/T/N=(0) cellDFR:regionsNames/Wave=regionsNames
        regionsNames = ""
    endif

    if(!WaveExists(regionsStarts_entry))
        make/O/D/N=(0) cellDFR:regionsStarts_entry/Wave=regionsStarts_entry
        regionsStarts_entry = NaN
    endif
    if(!WaveExists(regionsEnds_entry))
        make/O/D/N=(0) cellDFR:regionsEnds_entry/Wave=regionsEnds_entry
        regionsEnds_entry = NaN
    endif
    if(!WaveExists(regionsStarts))
        make/O/D/N=(0) cellDFR:regionsStarts/Wave=regionsStarts
        regionsStarts = NaN
    endif
    if(!WaveExists(regionsEnds))
        make/O/D/N=(0) cellDFR:regionsEnds/Wave=regionsEnds
        regionsEnds = NaN
    endif
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-10
/// NOTES: This is a copy of the updateCellInfoEventsTable
/// could replace that function with this one, if desired
/// returns the number of points in the first wave
function updateCellInfoTable (string selectedCell[, string infoType, string tableName, string infoWaveNames, string tableTitles])
    if(paramIsDefault(infoType))
        infoType = "regions"
    endif

    if(stringMatch(infoType, "regions"))
        tableName = "AGG_Events#cellRegionInfoTable"
        infoWaveNames = "regionsNames;regionsStarts_entry;regionsEnds_entry;"
        tableTitles = "name;start;end;"
    else
        if(paramIsDefault(tableName))
            tableName = "AGG_Events#cellRegionInfoTable"
        endif
        if(paramIsDefault(infoWaveNames))
            infoWaveNames = "regionsNames;regionsStarts_entry;regionsEnds_entry;"
        endif
        if(paramIsDefault(tableTitles))
            tableTitles = "name;start;end;"
        endif
        
    endif
    
    acceptChangesInTable(tableName)
    RemoveWavesFromTable_AGG(tableName, "*")

    variable iWaves = 0
    variable numBParams = itemsInList(infoWaveNames)
    string nameWave = ""
    DFREF cellInfoDFR = getEventsCellInfoDF(selectedCell)
    NVAR/Z/SDFR=cellInfoDFR regionTimeUnits

    if(dataFolderRefStatus(cellInfoDFR)==0)
        print "updateCellInfoTable: can't find cell DFR for", selectedCell
    endif

    variable nPoints
    for( iWaves = 0; iWaves < numBParams; iWaves++ )
        nameWave = StringFromList( iWaves, infoWaveNames )

        Wave/SDFR=cellInfoDFR thisWave = $nameWave

        if(!WaveExists(thisWave))
            continue
        endif
        
        // Replace values for the display waves with the value of the selected cell/region
        AppendToTable /W=$tableName thisWave
        ModifyTable/W=$tableName title(thisWave)=StringFromList(iWaves, tableTitles)
        if(stringmatch(nameWave, "*_entry"))
            if(regionTimeUnits == 3)
                ModifyTable/W=$tableName format(thisWave) = 7  
            else
                ModifyTable/W=$tableName format(thisWave) = 0  
            endif
        endif
        if(iWaves == 0)
            ModifyTable/W=$tableName alignment(thisWave)=0
            nPoints = numpnts(thisWave)
        endif
    endfor
    ModifyTable/W=$tableName autosize={0, 0, -1, 0, 10}
    if(regionTimeUnits == 3)
        ModifyTable/W=$tableName showParts = 126
    else
        ModifyTable/W=$tableName showParts = 255
    endif
    return nPoints
end

///////////
/// updateRegionWavesDims
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Redimensions the region waves based on the number of regions desired
/// Asks first if the number of regions is being decreased
///////////
function updateRegionWavesDims(string cellName)
    DFREF cellDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        print "updateRegionWavesDims:", cellName, "does not have an info folder. Quitting"
        abort 
    endif

    Wave/SDFR=cellDFR/D regionsStarts_entry, regionsEnds_entry
    Wave/SDFR=cellDFR/T regionsNames

    if(!WaveExists(regionsNames) || !WaveExists(regionsStarts_entry) || !WaveExists(regionsEnds_entry))
        makeCellRegionWaves(cellName)
        Wave/SDFR=cellDFR/D regionsStarts_entry, regionsEnds_entry
        Wave/SDFR=cellDFR/T regionsNames
    endif

    variable origNregs = numpnts(regionsNames)

    DFREF panelDFR = getEventsCellSumOutDF()
    if(dataFolderRefStatus(panelDFR)==0)
        print "updateRegionWavesDims: Couldn't find the cell summary output panel dfr"
        abort
    endif

    NVAR/Z/SDFR=panelDFR gNumRegions = numRegions
    variable updatedNregs

    if(!NVAR_Exists(gNumRegions))
        updatedNregs = NaN
    else
        updatedNregs = gNumRegions
    endif

    variable proceedWithChange = 1
    if(numtype(updatedNregs)==0 && updatedNregs < origNregs)
        string warningMessage = "You've provided a new number of regions " + num2str(updatedNregs)
        warningMessage += " that is less than the current number of regions " + num2str(origNregs)
        warningMessage += ". How do you want to proceed?"
        
        variable userChoice
        Prompt userChoice, warningMessage, popup, "Reduce the number of regions;Keep the current number of regions"
        DoPrompt "Address interval", userChoice
        if (V_Flag)
            userChoice = 2 // If user cancels, keep the current number
        endif

        switch (userChoice)
            case 1:
                proceedWithChange = 1
                break
            case 2:
                proceedWithChange = 0
                break
            default:
                
                break
        endswitch
    endif

    if(!proceedWithChange)
        if(NVAR_Exists(gNumRegions))
            gNumRegions = origNregs
        endif
        return NaN
    endif

    redimension/N=(updatedNregs) regionsNames, regionsStarts_entry, regionsEnds_entry
    if(origNregs != updatedNregs)
        updateRegions(cellName)
    endif
end

///////////
/// checkEnteredRegionTimes
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Check the entered start/end times for the regions
/// based on the total duration of the concatenated cell
/// Also checks that the start time of the next region is after
/// the end time of the previous region
///////////
function checkEnteredRegionTimes([string cellName])
    if(paramIsDefault(cellName))
        cellName = getSelectedItem("cellListBox_cellConcat", hostName = "AGG_events")
    endif
    
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)

    if(dataFolderRefStatus(cellInfoDFR)==0)
        print "checkEnteredRegionTimes: Couldn't find info folder for cell", cellName
        return NaN
    endif

    string tableName = "AGG_Events#cellRegionInfoTable"
    acceptChangesInTable(tableName)

    Wave/SDFR=cellInfoDFR regionsStarts_entry, regionsEnds_entry
    Wave/SDFR=cellInfoDFR/T regionsNames
    if(!WaveExists(regionsNames) || !WaveExists(regionsStarts_entry) || !WaveExists(regionsEnds_entry))
        print "checkEnteredRegionTimes: Couldn't find waves for cell", cellName
        return NaN
    endif

    variable cellDur = updateCellConcDur(cellName)
    variable regionOkay = 1
    if(numtype(cellDur)==2)
        regionOkay = 0
        print "Cell duration for", cellName, "is NaN. This probably means you haven't concatenated"
        return regionOkay 
    endif
    
    variable localCellDur = cellDur
    DFREF panelDFR = getEventsCellSumOutDF()
    NVAR regionTimeUnits= panelDFR:regionTimeUnits
    variable overallStart = 0
    if(regionTimeUnits == 3) // time
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)!=0)
            NVAR/Z/SDFR=cellSumDFR seriesTime
            if(NVAR_Exists(seriesTime) && numtype(seriesTime)==0)
                overallStart = seriesTime
                localCellDur = cellDur + seriesTime
            endif
        endif
    endif

    variable nRegions = numpnts(regionsNames), iRegion = 0
    variable prevRegionEnd = 0
    
    for(iRegion=0; iRegion<nRegions; iRegion++)
        string regionName = regionsNames[iRegion]
        variable regionStart = regionsStarts_entry[iRegion]
        variable regionEnd = regionsEnds_entry[iRegion]

        if(strlen(regionName) == 0)
            regionOkay = 0
            print "Region", iRegion, "doesn't have a region name. Please enter one"
            prevRegionEnd = regionEnd
            continue
        endif

        if(regionStart != inf && regionStart < overallStart)
            regionOkay = 0
            if(regionTimeUnits == 3)
                print "Region", iRegion, regionName, "has a start time", Secs2Time(regionStart, 3), "before the start time of the cell", Secs2Time(overallStart, 3)
            else
                print "Region", iRegion, regionName, "has a start time", regionStart, "before the start time of the cell", overallStart
            endif
        endif

        if(regionEnd <= regionStart)
            regionOkay = 0
            if(regionTimeUnits == 3)
                print "Region", iRegion, regionName, "has an end time", Secs2Time(regionEnd, 3), "that is before or equal to its start time", Secs2Time(regionStart, 3)
            else
                print "Region", iRegion, regionName, "has an end time", regionEnd, "that is before or equal to its start time", regionStart
            endif
            prevRegionEnd = regionEnd
            continue
        endif

        if(regionStart < prevRegionEnd)
            regionOkay = 0
            if(regionTimeUnits == 3)
                print "Region", iRegion, regionName, "has a start time", Secs2Time(regionStart, 3), "that is before the previous region end time", Secs2Time(prevRegionEnd, 3)
            else
                print "Region", iRegion, regionName, "has a start time", regionStart, "that is before the previous region end time", prevRegionEnd
            endif
            prevRegionEnd = regionEnd
            continue
        endif

        if(regionEnd != inf && regionEnd > localCellDur)
            regionOkay = 0
            if(regionTimeUnits == 3)
                print "Region", iRegion, regionName, "has an end time", Secs2Time(regionEnd, 3), "that is longer than the concatenated duration of the cell", Secs2Time(localCellDur, 3)
            else
                print "Region", iRegion, regionName, "has an end time", regionEnd, "that is longer than the concatenated duration of the cell", localCellDur
            endif
            prevRegionEnd = regionEnd
            continue
        endif

        prevRegionEnd = regionEnd
    endfor

    return regionOkay
end