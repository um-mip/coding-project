///////////
/// regionsUpdateHook
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-04
/// NOTES: 
///////////
function regionsUpdateHook(s)
    STRUCT WMWinHookStruct & s

    switch(s.eventCode)
        
        case 24: // tableEntryAccepted
            print "Accepted entry"
            string tableName = "cellRegionInfoTable"
            [string hostN, string subN] = splitFullWindowName(s.winName)
            if( stringmatch( subN, tableName ) ) // 20171128
                string cellName = getSelectedItem("cellListBox_cellConcat", hostName = hostN)
                updateRegions(cellName)
                return 1
            endif
            break
        default:
            break
    endswitch
    return 0
end



///////////
/// updateRegions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: 
///////////
function updateRegions(string cellName)
    if(strlen(cellName) == 0)
        return NaN
    endif

    deleteExistingRegions(cellName)

    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)

    if(dataFolderRefStatus(cellInfoDFR)==0)
        return NaN
    endif

    Wave/SDFR=cellInfoDFR regionsStarts_entry, regionsEnds_entry
    Wave/T/SDFR=cellInfoDFR regionsNames

    if(!WaveExists(regionsNames) || !WaveExists(regionsStarts_entry) || !WaveExists(regionsEnds_entry))
        updateCellConcatDisplays()

        return NaN
    endif

    duplicate/O regionsNames, cellInfoDFR:regionsNames_stored

    variable isOkay = checkEnteredRegionTimes(cellName = cellName)

    if(!isOkay)
        print "updateRegions: There was a problem with the entered region information. Please resolve and try again"
        updateCellConcatDisplays()

        return NaN
    endif

    [Wave regionsStarts, wave regionsEnds] = getRegionsStartsEnds(cellName)

    variable iRegion = 0, nRegions = numpnts(regionsNames)

    for(iRegion=0; iRegion<nRegions; iRegion++)
        string regionName = regionsNames[iRegion] // strlen of regionName is checked in checkEnteredRegionTimes
        variable regionStart = regionsStarts[iRegion]
        variable regionEnd = regionsEnds[iRegion]

        // make the region folders and store the region start time and end time as variables within the folder
        // for each subtype of the data
        makeCellRegionFolders(cellName, regionName = regionName, startTime = regionStart, endTime = regionEnd)
    endfor

    storeRegionsSubsets(cellName)
    
    Wave/T allRegionNames = storeAllRegionsNamesWave()
    Wave allRegionNames_sel = storeAllRegionsSelectWave(allRegionNames)
    storeAllRegionsSelectWave(allRegionNames, whichSelWave = "burstAnalysis")
    storeAllRegionsSelectWave(allRegionNames, whichSelWave = "maxBW")
    storeAllRegionsSelectWave(allRegionNames, whichSelWave = "regionsOutput")

    updateCellConcatDisplays()
end


///////////
/// storeRegionsSubsets
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function storeRegionsSubsets(string cellName)
    DFREF cellDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        print "storeRegionsSubsets: Couldn't find cell data folder", cellName
        return 0
    endif

    DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
    if(dataFolderRefStatus(regionDFR)==0)
        print "storeRegionsSubsets: Couldn't find region DFR"
        return 0
    endif

    string regionsNamesString = getChildDFRList(regionDFR)
    wave/T regionsNames = listToTextWave(regionsNamesString, ";")
    if(!WaveExists(regionsNames))
        print "storeRegionsSubsets: Can't find regionsNames wave"
        return 0
    endif
    
    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    variable iRegion = 0, nRegions = numpnts(regionsNames)
    for(iRegion=0; iRegion<nRegions; iRegion++)
        string regionName = regionsNames[iRegion]
        iSubType = 0
        for(iSubType=0; iSubType<nSubTypes; iSubType++)
            string subType = StringFromList(iSubType, subTypes)
            storeRegionSubsets_forSubType(cellName, regionName, subType)
            [variable dur, variable numEvents, variable freq] = storeFreqDurTotalEvents_cellRegion(cellName, regionName, subType = subType)
            if(stringmatch(subType, "avg") || stringmatch(subType, "limited"))
                storeRegionAvgEvent_forSubType(cellName, regionName, subType)
            endif
        endfor
    endfor
end


///////////
/// storeRegionSubsets_forSubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function storeRegionSubsets_forSubType(string cellName, string regionName, string subType)
    DFREF cellSumDFR = returnCellEventsDFR_bySubType(cellName, subType)
    if(dataFolderRefStatus(cellSumDFR)==0)
        print "storeRegionSubsets_forSubType: no summary folder for cell", cellName 
        return 0
    endif

    DFREF cellRegDFR = returnCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)
    if(dataFolderRefStatus(cellRegDFR)==0)
        print "storeRegionSubsets_forSubType: no region folder for cell", cellName, "region", regionName
        return 0
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellSumDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string wavesToSubset = cellSum.avgByEventWaves
    variable numWaves = itemsInList(wavesToSubset), iWave = 0

    Wave/SDFR=cellSumDFR sctWave = $(cellName + "_sct")
    if(!WaveExists(sctWave))
        print "storeRegionSubsets_forSubtype: no SCT wave"
        return 0
    endif

    NVAR/Z/SDFR=cellRegDFR startTime, endTime
    if(numtype(startTime) == 2 || numtype(endTime) == 2)
        print "storeRegionSubsets_forSubtype: startTime or endTime is NaN"
        return 0
    endif

    Wave regionSCT = extractWaveWithinRegion(sctWave, startTime, endTime)
    duplicate/O regionSCT, cellRegDFR:$(nameOfWave(sctWave))
    Wave globalRegionSCT = cellRegDFR:$(nameOfWave(sctWave))

    // Make the cellAvgVars for the region
    Struct eventAvgVars cellAvgVars
    StructFill/SDFR=cellRegDFR/AC=1 cellAvgVars
    if(stringmatch(getDFREFstring(cellRegDFR), "root:"))
        print "storeRegionSubsets_forSubType is making variables in root"
    endif

    for(iWave = 0; iWave < numWaves; iWave++)
        string thisWaveName = stringfromlist(iWave, wavesToSubset)
        string concatWaveName = thisWaveName + "_concat"
        string distWaveName = thisWaveName + "_dist"

        string thisExt = stringbykey(thisWaveName, cellSum.avgByEventsExts)

        Wave/SDFR=cellSumDFR concatWave = $concatWaveName, origUnitsWave = $(thisWaveName+"_concatOrigUnits"), distWave = $distWaveName
        
        if(!WaveExists(concatWave))
            print "storeRegionSubsets_forSubType", cellName, "analysis wave doesn't exist", concatWaveName, "and/or", distWaveName
            continue // bumps back to start of the for loop
        endif

        // make the concatenated waves for region
        if(numpnts(concatWave) == 0)
            make/N=0/O/D cellRegDFR:$concatWaveName/Wave=regionSubWave
            make/N=0/D/O cellRegDFR:$distWaveName/Wave=distSubWave
        else
            Wave/D regionSubWave = extractWaveWithinRegion(sctWave, startTime, endTime, infoWave = concatWave)
            if(WaveExists(regionSubWave))
                // interval won't be found for some of the subsets
                // print thisWaveName, "can't find region subWave"
                duplicate/O/D regionSubWave, cellRegDFR:$concatWaveName/Wave=regionSubWave
            else
                make/N=0/O/D cellRegDFR:$concatWaveName/Wave=regionSubWave
            endif
            Wave/D regionSubWave_origUnits = extractWaveWithinRegion(sctWave, startTime, endTime, infoWave = origUnitsWave)
            if(WaveExists(regionSubWave_origUnits))
                duplicate/O/D regionSubWave_origUnits, cellRegDFR:$distWaveName/Wave=distSubWave 
            else
                make/N=0/D/O cellRegDFR:$distWaveName/Wave=distSubWave
            endif
        endif


        // Save the average for the region
        STRUCT eventAvgVars varsStruct
        fillAvgVarNames(varsStruct)

        string varKey = varsStruct.varFromWave
        string varN = stringbykey(thisExt, varKey)
        NVAR/Z/SDFR=cellRegDFR thisVar = $varN
        thisVar = NaN
        if(numpnts(regionSubWave)>0)
            thisVar = calcWaveAvg(regionSubWave)
        endif

        // Distribution waves
        copyDataScale(origUnitsWave, distSubWave)
            
        string probDistribution = StringByKey(thisWaveName, cellSum.probDistribution)

        strswitch (probDistribution)
            case "bySign":
                string sortDir = AGG_wnoteSTRbyKey(distWaveName, "sortDir", cellSumDFR)
                probdistP(nameOfWave(distSubWave), str2num(sortDir), distWave = distSubWave)
                break
            case "pos":
                probdistp(nameOfWave(distSubWave), 1, distWave = distSubWave)
                break
            default:
                
                break
        endswitch
    endfor

    // SCX and SCY waves
    Wave/SDFR=cellSumDFR scxWave = $(cellName + "_scx"), scyWave = $(cellName + "_scy")
    if(!WaveExists(scxWave) || !WaveExists(scyWave))
        return 0
    endif

    Wave regScxWave = extractWaveWithinRegion(scxWave, startTime, endTime)
    Wave regScyWave = extractWaveWithinRegion(scxWave, startTime, endTime, infoWave = scyWave)
    copyDataScale(scyWave, regScyWave)

    if(WaveExists(regScxWave))
        duplicate/O regScxWave, cellRegDFR:$(nameOfWave(scxWave))
    endif
    if(WaveExists(regScyWave))
        duplicate/O regScyWave, cellRegDFR:$(nameOfWave(scyWave))
    endif

    // Gap waves
    Wave/SDFR=cellSumDFR gapStarts = $(cellName + "_gapStarts"), gapEnds = $(cellName + "_gapEnds")

    if(!WaveExists(gapStarts) || !WaveExists(gapEnds))
        return 0
    endif

    Wave regGapStarts = extractWaveWithinRegion(gapStarts, startTime, endTime)
    Wave regGapEnds = extractWaveWithinRegion(gapEnds, startTime, endTime)

    if(WaveExists(regGapStarts))
        duplicate/O regGapStarts, cellRegDFR:$(nameOfWave(GapStarts))
        duplicate/O regGapStarts, cellRegDFR:$(cellName + "_gapStartsPlusEnd")
        Wave/SDFR=cellRegDFR regGapStartsPlusEnd = $(cellName + "_gapStartsPlusEnd")
    endif
    if(WaveExists(regGapEnds))
        duplicate/O regGapEnds, cellRegDFR:$(nameOfWave(GapEnds))
        duplicate/O regGapEnds, cellRegDFR:$(cellName + "_gapEndsPlusStart")
        Wave/SDFR=cellRegDFR regGapEndsPlusStart = $(cellName + "_gapEndsPlusStart")
    endif

    if(numType(startTime) == 1) // if inf
        startTime = 0
    endif

    if(WaveExists(regGapEndsPlusStart))
        InsertPoints/V=(startTime) 0, 1, regGapEndsPlusStart
    endif

    if(numType(endTime) == 1) // if inf
        NVAR/Z/SDFR=cellSumDFR totalDur
        if(NVAR_Exists(totalDur) && totalDur > 0)
            endTime = totalDur
        else
            endTime = str2num(AGG_wnoteSTRbyKey(cellName + "_sct", "duration", cellSumDFR ))
        endif
    endif

    if(WaveExists(regGapStartsPlusEnd))
        regGapStartsPlusEnd[numpnts(regGapStartsPlusEnd)] = {endTime}
    endif

    [Wave gapPlotX, Wave gapPlotY] = makeGapPlotWaves(regGapStarts, regGapEnds, baseN = cellName, dfrForPlotWaves = cellRegDFR, addStartEnd = 1, overallEndTime = endTime, overallStartTime = startTime)

    variable/G cellRegDFR:totalDur/N=regionDur
    regionDur = endTime - startTime
    string sctNote = "DURATION:" + num2str(regionDur) + ";"
    
    variable nGapStarts = numpnts(regGapStarts), nGapEnds = numpnts(regGapEnds)
    variable/G cellRegDFR:totalGapDur/N=gapDur
    gapDur = 0 
    if((nGapStarts + nGapEnds) > 0) // at least one gap 
        variable firstGapStart = NaN, firstGapEnd = NaN, lastGapStart = NaN, lastGapEnd = NaN
        if(nGapStarts>0)
            firstGapStart = regGapStarts[0]
            lastGapStart = regGapStarts[nGapStarts-1]
        endif
        if(nGapEnds>0)
            firstGapEnd = regGapEnds[0]
            lastGapEnd = regGapEnds[nGapEnds-1]
        endif

        duplicate/FREE regGapStarts, tempGapStarts
        duplicate/FREE regGapEnds, tempGapEnds 

        variable beginsWithStart = 0, finishesWithEnd = 0
        if((nGapStarts > 0 && nGapEnds == 0) || firstGapStart < firstGapEnd)
            beginsWithStart = 1
        else
            insertPoints/V=(startTime) 0, 1, tempGapStarts
        endif
        if((nGapEnds > 0 && nGapStarts == 0) || lastGapEnd > lastGapStart)
            finishesWithEnd = 1
        else
            tempGapEnds[numpnts(tempGapEnds)] = {endTime}
        endif

        if(!(numpnts(tempGapStarts)==numpnts(tempGapEnds)))
            print "storeRegionSubsets_forSubType: Problem with gaps, number of points not equal"
        else
            make/FREE/N=(numpnts(tempGapStarts)) indivGapDurations
            indivGapDurations = tempGapEnds - tempGapStarts
            gapDur = sum(indivGapDurations)
        endif
    endif

    variable/G cellRegDFR:totalRecDur/N=recDur
    recDur = regionDur - gapDur
    sctNote += "RECDURATION:" + num2str(recDur) + ";"
    sctNote += "GAPDURATION:" + num2str(gapDur) + ";"
    sctNote += "GAPSTARTS:" + convertNumWaveToStr(regGapStarts) + ";"
    sctNote += "GAPENDS:" + convertNumWaveToStr(regGapEnds) + ";"
    sctNote += "gapStartsPlusEnd:" + convertNumWaveToStr(regGapEnds) + num2str(endTime) + ";"
    sctNote += "gapEndsPlusStart:" + num2str(startTime) + "," + convertNumWaveToStr(regGapEnds) + ";"
    
    Note/NOCR globalRegionSCT, sctNote
end

///////////
/// storeRegionAvgEvent_forSubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function storeRegionAvgEvent_forSubType(string cellName, string regionName, string subType)
    string okaySubTypes = "avg;limited;"
    if(!itemsInList(ListMatch(okaySubTypes, subType))>0)
        return 0 // don't do anything for other subtypes
    endif
    
    DFREF cellSumDFR = returnCellEventsDFR_bySubType(cellName, subType)
    
    if(dataFolderRefStatus(cellSumDFR)==0)
        print "storeRegionAvgEvent_forSubType: no summary folder for cell", cellName 
        return 0
    endif

    DFREF cellRegDFR = returnCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)
    if(dataFolderRefStatus(cellRegDFR)==0)
        print "storeRegionAvgEvent_forSubType: no region folder for cell", cellName, "region", regionName
        return 0
    endif

    Wave/SDFR=cellSumDFR concatAvgIndices
    if(!WaveExists(concatAvgIndices))
        print "storeRegionAvgEvent_forSubType: no concatAvgIndices wave exists for cell", cellName, "subtype", subType 
    endif

    Wave/SDFR=cellSumDFR sctWave = $(cellName + "_sct")
    if(!WaveExists(sctWave))
        print "storeRegionAvgEvent_forSubtype: no SCT wave"
        return 0
    endif

    NVAR/Z/SDFR=cellRegDFR startTime, endTime
    if(numtype(startTime) == 2 || numtype(endTime) == 2)
        print "storeRegionAvgEvent_forSubtype: startTime or endTime is NaN"
        return 0
    endif

    Wave concatAvgIndices_reg = extractWaveWithinRegion(sctWave, startTime, endTime, infoWave = concatAvgIndices)
    if(!WaveExists(concatAvgIndices_reg))
        print "storeRegionAvgEvent_forSubType: the region subset of the avg event indices doesn't exist"
        return 0
    endif

    duplicate/O concatAvgIndices_reg, cellRegDFR:concatAvgIndices

    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)

    duplicate/FREE=1/RMD=[][0,0] concatAvgIndices_reg, indicesCol
    duplicate/FREE=1/RMD=[][1,1] concatAvgIndices_reg, seriesCol

    if(numpnts(seriesCol)>1)
        FindDuplicates/RN=cellRegDFR:combinedSeriesW seriesCol
        Wave uniqueSeries = cellRegDFR:combinedSeriesW
    else
        duplicate/O seriesCol, cellRegDFR:combinedSeriesW
        Wave uniqueSeries = cellRegDFR:combinedSeriesW
    endif
    variable iSeries = 0, nSeries = numpnts(uniqueSeries)
    make/FREE/N=(nSeries)/wave wavesToAvg, avgIndices
    for(iSeries=0; iSeries<nSeries; iSeries++)
        variable seriesNum = uniqueSeries[iSeries]
        string seriesName = buildSeriesWaveName(cellName, seriesNum)
        Wave rawWave = evDetectDFR:$seriesName

        if(!WaveExists(rawWave))
            print "storeRegionAvgEvent_forSubType: raw wave for cell", cellName, "series", seriesNum, "can't be found"
            continue
        endif

        // Store the series raw wave
        wavesToAvg[iSeries] = rawWave 
        
        // Find the indices (of the index wave) that match the current series
        Extract/O/FREE/INDX seriesCol, indicesMatchingSeries, seriesCol == seriesNum
        
        make/N=(numpnts(indicesMatchingSeries))/O cellRegDFR:$("s" + num2str(seriesNum) + "avgIndex")/Wave=avgIndex
        // make/N=(numpnts(indicesMatchingSeries))/FREE=1 avgIndex

        // Set the values of the avg index wave for this series equal to
        // the appropriate indices that we selected above
        avgIndex = indicesCol[indicesMatchingSeries[p]]

        if(!WaveExists(avgIndex))
            print "storeRegionAvgEvent_forSubType: avgIndex for cell", cellName, "series", seriesNum, "can't be found"
            continue
        endif

        avgIndices[iSeries] = avgIndex
    endfor

    make/O/N=0 cellRegDFR:cellAvgEvent/Wave=aveWave
    make/O/N=0 cellRegDFR:cellNAvgEvent/Wave=nAveWave
    recalculateAverages5(wavesToAvg, aveWave = aveWave, nAveWave = nAveWave, avgIndexWaves = avgIndices)
end