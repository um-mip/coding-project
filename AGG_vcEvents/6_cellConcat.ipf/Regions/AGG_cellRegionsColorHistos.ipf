///////////
/// colorHistogramForRegion
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function colorHistogramForRegion(variable iRegion, Wave rgb_table, Wave eventHisto, variable regionStartTime, variable regionEndTime, string target)
    string histoName = nameOfWave(eventHisto)
    variable iBin = 0, nBins = numpnts(eventHisto)

    ModifyGraph/W=$target rgb(eventHisto) = (48059,48059,48059), useBarStrokeRGB(eventHisto)=1 // make everything grey to start and add stroke
    for(iBin=0; iBin<nBins; iBin++)
        variable binStartTime = pnt2x(eventHisto, iBin)
        variable isInRange = isWithinRange(binStartTime, regionStartTime, regionEndTime)
        if(isInRange)
            ModifyGraph/W=$target rgb(eventHisto[iBin]) = (rgb_table[iRegion][0], rgb_table[iRegion][1], rgb_table[iRegion][2]), barStrokeRGB(eventHisto[iBin])=(0,0,0)
        endif
    endfor
end

///////////
/// colorHistogram
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: Color the histogram based on the regions
///////////
function colorHistogram(Wave regionsStarts, Wave regionsEnds, Wave eventHisto[, string target])
    if(paramIsDefault(target))
        target = "AGG_events#cellHistoGraph"
    endif
    
    if(!WaveExists(regionsStarts) || ! WaveExists(regionsEnds) || !WaveExists(eventHisto))
        print "colorHistogram: Couldn't find either regions waves or event histogram"
        return 0
    endif

    variable iRegion = 0, nRegions = numpnts(regionsStarts)

    if(nRegions <= 0)
        return 0
    endif

    Wave rgb_table = makeColorTableWave(nRegions) // default is to skip black

    for(iRegion=0; iRegion<nRegions; iRegion++)
        variable regionStart = regionsStarts[iRegion]
        variable regionEnd = regionsEnds[iRegion]

        colorHistogramForRegion(iRegion, rgb_table, eventHisto, regionStart, regionEnd, target)
    endfor
end

///////////
/// colorCityPlot
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-27
/// NOTES: 
///////////
function colorCityPlot(string cellName[, string target])
    Wave sctWave = getCellSCTWave(cellName)

    if(!WaveExists(sctWave))
        return NaN
    endif

    DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
    if(dataFolderRefStatus(regionDFR)==0)
        return NaN
    endif

    // If the regions name stored version exists, prefer that, as it will be sorted by times for each cell
    Wave/SDFR=getEventsCellInfoDF(cellName)/T regionsNames_stored
    if(!WaveExists(regionsNames_stored))
        string regionsNames = getChildDFRList(regionDFR)
        Wave/T regionsNames_stored = listToTextWave(regionsNames, ";")
    endif

    variable iRegion = 0, nRegions = numpnts(regionsNames_stored)
    DFREF axisInfoDFR = getVBWCityPlotAxisDF()

    duplicate/O sctWave, axisInfoDFR:zWave
    Wave/SDFR=axisInfoDFR zWave 
    zWave = 0 // everything to zero first

    for(iRegion=0; iRegion<nRegions; iRegion++)
        string regionName = regionsNames_stored[iRegion]
        DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = regionName)
        if(dataFolderRefStatus(regionDFR)==0)
            continue
        endif

        NVAR/Z/SDFR=regionDFR startTime, endTime

        zWave[] = isWithinRange(sctWave[p], startTime, endTime) ? iRegion + 1 : zWave[p]
    endfor

    Wave MyColorTableWave = makeColorTableWave(3) // default is to skip black
    duplicate/O MyColorTableWave, axisInfoDFR:MyColorTableWave
    Wave/SDFR=axisInfoDFR MyColorTableWave
    InsertPoints 0, 1, MyColorTableWave

    // make zero, first row grey
    MyColorTableWave[0][] = 48059

    if(paramIsDefault(target))
        target = "AGG_Bursts#burstCellPlot"
    endif
    
    
    // ModifyGraph/W=AGG_Bursts#burstCellPlot rgb($nameOfWave(flippedMin[0:100])=(0,0,0)
    ModifyGraph/W=$target zColor(flippedMin) = {zWave, *, *, cIndexRGB, 0, MyColorTableWave}
end

///////////
/// colorByRegions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-27
/// NOTES: 
///////////
function colorByRegions(string cellName, string plotType[, string target, string axisN, wave xWave, DFREF colorDFR])
    if(strlen(cellName)==0)
        return NaN
    endif

    if(paramIsDefault(target))
        target = "AGG_bursts#burstCellPlot"
    endif

    if(paramIsDefault(colorDFR))
        colorDFR = getVBWCityPlotAxisDF()
    endif
    
    // debugger
    // need to get appropriate x wave and y wave name
    string yName, zWaveName, colorWaveName
    strswitch (plotType)
        case "cityPlot":
            if(paramIsDefault(xWave))
                wave xWave = getCellSCTWave(cellName)
            endif
            if(paramIsDefault(axisN))
                axisN = "newVBW"
            endif
            
            yName = "flippedMin"
            zWaveName = "zWave_city"
            colorWaveName = "colorWave_city"
            break
        case "choppedEvents":
            if(paramIsDefault(xWave))
                wave/SDFR=getCellSummaryDF(cellName) xWave = $(cellName + "_scx")
            endif

            if(paramIsDefault(axisN))
                axisN = "left"
            endif
            
            yName = "choppedEventsY"
            zWaveName = "zWave_chopped"
            colorWaveName = "colorWave_chopped"
            break
        case "eventHisto":
            // let the colorHistogramForRegion function continue to do the work... but could be replaced by this
            return NaN
            break
        default:
            if(paramIsDefault(xWave))
                wave xWave = getCellSCTWave(cellName)
            endif

            if(paramIsDefault(axisN))
                axisN = "newVBW"
            endif

            yName = "flippedMin"
            zWaveName = "zWave_city"
            colorWaveName = "colorWave_city"
            break
    endswitch

    if(!WaveExists(xWave))
        return NaN
    endif
    
    DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
    if(dataFolderRefStatus(regionDFR)==0)
        return NaN
    endif

    // If the regions name stored version exists, prefer that, as it will be sorted by times for each cell
    Wave/SDFR=getEventsCellInfoDF(cellName)/T regionsNames_stored
    if(!WaveExists(regionsNames_stored))
        string regionsNames = getChildDFRList(regionDFR)
        Wave/T regionsNames_stored = listToTextWave(regionsNames, ";")
    endif

    variable iRegion = 0, nRegions = numpnts(regionsNames_stored)
    
    duplicate/O xWave, colorDFR:$(zWaveName)
    Wave/SDFR=colorDFR zWave = $(zWaveName)
    zWave = 0 // everything to zero first

    for(iRegion=0; iRegion<nRegions; iRegion++)
        string regionName = regionsNames_stored[iRegion]
        DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = regionName)
        if(dataFolderRefStatus(regionDFR)==0)
            continue
        endif

        NVAR/Z/SDFR=regionDFR startTime, endTime

        zWave[] = isWithinRange(xWave[p], startTime, endTime) ? iRegion + 1 : zWave[p]
    endfor

    Wave MyColorTableWave = makeColorTableWave(nRegions) // default is to skip black
    duplicate/O MyColorTableWave, colorDFR:$(colorWaveName)
    Wave/SDFR=colorDFR MyColorTableWave = $(colorWaveName)
    InsertPoints 0, 1, MyColorTableWave

    // make zero, first row grey
    MyColorTableWave[0][] = 48059
    
    ModifyGraph/W=$target zColor($yName) = {zWave, *, *, cIndexRGB, 0, MyColorTableWave}
end

