///////////
/// storeFreqDurTotalEvents_cellRegion
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: The SCT wave needs to be present in region folder before making these calculations
///////////
function[variable dur, variable numEvents, variable freq] storeFreqDurTotalEvents_cellRegion(string cellName, string regionName[, string subType])
    
    if(paramIsDefault(subType))
        subType = "all"
    endif
    
    DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)

    if(dataFolderRefStatus(cellDFR)==0)
        print "storeFreqDurTotalEvents_cellRegion: Can't find appropriate cell data folder"
        return [dur, numEvents, freq]
    endif

    if(stringmatch(getDFREFstring(cellDFR), "root:"))
        print "storeFreqDurTotalEvents_cellRegion is making variables in root"
    endif


    NVAR/Z/SDFR=cellDFR startTime, endTime
    if(!numType(startTime) == 2 && !numtype(endTime) == 2)
        print "storeFreqDurTotalEvents_cellRegion: startTime and/or endTime for region", regionName, "is NaN"
        return [dur, numEvents, freq]
    endif
    
    DFREF cellSumDFR = getCellSummaryDF(cellName)
    if(dataFolderRefStatus(cellSumDFR)==0)
        return [dur, numEvents, freq]
    endif

    if(numType(startTime) == 1) // if inf
        startTime = 0
    endif

    if(numType(endTime) == 1) // if inf
        NVAR/Z/SDFR=cellSumDFR totalDur
        if(NVAR_Exists(totalDur) && totalDur > 0)
            endTime = totalDur
        else
            Wave/SDFR=cellSumDFR fullSCTWave = $(cellName + "_sct")
            if(!WaveExists(fullSCTWave))
                return [dur, numEvents, freq]
            endif

            endTime = str2num(AGG_wnoteSTRbyKey(cellName + "_sct", "duration", cellSumDFR ))
        endif
    endif

    Struct eventAvgVars cellAvgVars
    StructFill/SDFR=cellDFR/AC=1 cellAvgVars
    if(stringmatch(getDFREFstring(cellDFR), "root:"))
        print "resetEventAvgVarsToNaN is making variables in root"
    endif

    dur = endTime - startTime
    cellAvgVars.duration = dur

    Wave/SDFR=cellDFR sctWave = $(cellName + "_sct")
    if(!WaveExists(sctWave))
        return [dur, numEvents, freq]
    endif

    numEvents = numpnts(sctWave)
    freq = numEvents / dur
    cellAvgVars.freq = freq
    cellAvgVars.numEvents = numEvents

    NVAR/Z/SDFR=cellSumDFR seriesTime
    variable regionClockStart = seriesTime + startTime

    cellAvgVars.seriesTime = regionClockStart

    return [dur, numEvents, freq]
end