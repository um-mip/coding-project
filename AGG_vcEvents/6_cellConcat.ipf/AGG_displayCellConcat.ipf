///////////
/// updateCellConcatDisplays
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: 
///////////
function updateCellConcatDisplays()
    string histoHost = "AGG_events#cellHistoGraph"
    clearDisplay(histoHost)

    string cellName = getSelectedItem("cellListBox_cellConcat", hostName = "AGG_events")

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    DFREF cellSumDFR = getEventsCellSumOutDF()

    // Cell histogram
    Wave/SDFR=cellSumDFR eventHisto, eventHistoByFreq
    redimension/N=0 eventHisto, eventHistoByFreq

    if(strlen(cellName) == 0) // a blank cell name, stop here
        return 0
    endif

    string subType = getSelectedDispType()
    DFREF cellDFR = returnCellEventsDFR_bySubType(cellName, subType)
    if(dataFolderRefStatus(cellDFR)!=0)
        plotPTBHisto(cellName, cellSmartConcDF = cellDFR, cellSumDFR = cellSumDFR)
    endif

    setDisplayedUnitsForCellUnits(cellName)

    updateCellStartTime(cellName)
    variable cellDur = updateCellConcDur(cellName)
    // if(numType(cellDur) == 2)
    //     return NaN
    // endif
    setRegionUnitsForCell(cellName)

    makeCellRegionWaves(cellName)
    variable nRegions = updateCellInfoTable(cellName, infoType = "regions")

    NVAR/Z/SDFR=cellSumDFR numRegions
    if(NVAR_Exists(numRegions))
        numRegions = nRegions
    endif
end

///////////
/// updateCellStartTime
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-04
/// NOTES: Update the string for start time for this cell
///////////
function/S updateCellStartTime(string cellName[, variable returnOnly])
    if(paramIsDefault(returnOnly)) // just return the cell duration, in sec, without updating panel
        returnOnly = 0
    endif
    
    DFREF cellSumDFR = getEventsCellSumOutDF()
    if(dataFolderRefStatus(cellSumDFR)==0)
        print "updateCellStartTime: Can't find events cell summary data folder"
        return ""
    endif

    SVAR/SDFR=cellSumDFR cellStartTime
    if(!SVAR_Exists(cellStartTime))
        print "updateCellStartTime: Can't find events start time variable"
        return ""
    endif

    if(!returnOnly)
        cellStartTime = ""
    endif

    DFREF cellDFR = returnCellEventsDFR_bySubType(cellName, "all") // duration should be the same for all subsets

    if(dataFolderRefStatus(cellDFR)==0)
        // couldn't find cellDFR, but don't warn. Means haven't concatenated yet
        return ""
    endif

    NVAR/Z/SDFR=cellDFR seriesTime
    if(!NVar_Exists(seriesTime) || numtype(seriesTime)!=0)
        return ""
    endif
    
    string startTime = Secs2Time(seriesTime, 3)
    if(!returnOnly)
        cellStartTime = startTime
    endif

    return startTime
end



///////////
/// updateCellConcDur
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-08
/// NOTES: Update the "total dur" set variable within the cell concat tab with the total concatenated
/// duration for the selected cell
///////////
function updateCellConcDur(string cellName[, variable returnOnly])

    if(paramIsDefault(returnOnly)) // just return the cell duration, in sec, without updating panel
        returnOnly = 0
    endif
    
    DFREF cellSumDFR = getEventsCellSumOutDF()
    if(dataFolderRefStatus(cellSumDFR)==0)
        print "updateCellConcDur: Can't find events cell summary data folder"
        return NaN
    endif

    NVAR/Z/SDFR=cellSumDFR cellDur
    if(!NVAR_Exists(cellDur))
        print "updateCellConcDur: Can't find events duration variable"
        return NaN
    endif

    cellDur = NaN

    DFREF cellDFR = returnCellEventsDFR_bySubType(cellName, "all") // duration should be the same for all subsets

    if(dataFolderRefStatus(cellDFR)==0)
        // couldn't find cellDFR, but don't warn. Means haven't concatenated yet
        return cellDur
    endif
    
    string sctWN = cellName + "_sct"
    Wave/SDFR=cellDFR sctWave = $sctWN
    if(!WaveExists(sctWave))
        // print "updateCellConcDur: Can't find SCT wave for cell", cellName
        return cellDur
    endif


    NVAR/Z/SDFR=cellDFR gTotalDur = totalDur

    variable totalDur
    if(NVAR_Exists(gTotalDur) && gTotalDur > 0)
        totalDur = gTotalDur
    else
        string totalDur_str = AGG_wnoteSTRbyKey( sctWN, "Duration", cellDFR )
        if(strlen(totalDur_str)==0)
            print "updateCellConcDur: Can't duration from SCT wave for cell", cellName
            return cellDur
        endif

        totalDur = str2num(totalDur_str)
    endif

    if(totalDur<=0)
        print "updateCellConcDur: Duration <=0 for cell", cellName
        return cellDur
    endif

    if(returnOnly)
        return totalDur
    endif
    
    variable regionTimeUnits = getRegionUnitsForCell(cellName)
    switch (regionTimeUnits)
        case 1: //sec
            cellDur = totalDur
            break
        case 2: //min
            cellDur = totalDur / 60
            break
        default:
            cellDur = totalDur
            break
    endswitch
    return cellDur
end