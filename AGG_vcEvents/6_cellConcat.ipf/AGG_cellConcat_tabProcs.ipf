function concatCellSeriesProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        // print "add to detect press"
        concatSelectedCellSeries()
        updateCellConcatDisplays()
    endif
end

// Run when a user interacts with the cell list wave
// Update the list wave for series based on the selected cell
function selectedCellListProc_cellConcat(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF panelDFR = getEventsPanelDF()

        string selectedCell = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)

        // print selectedCell
        updateSeriesWave_cell(selectedCell)
        clearDisplay("AGG_events#cellHistoGraph")
        RemoveWavesFromTable_AGG("AGG_events#cellRegionInfoTable", "*") // default is to accept any in-progress changes before deleting
        updateConcSeriesLB(selectedCell)
        updateCellConcatDisplays()
    endif
    return 0
end

function updateSeriesWave_cell (cellName)
    String cellName
    variable makeBlank = 0

    DFREF panelDFR = getEventsPanelDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()
    if(strlen(cellName)>0)
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)

        Wave cellEvents_series = cellInfoDFR:Events_series

        if(WaveExists(cellEvents_series))

            Duplicate/O cellEvents_series, cellSumDFR:selCellSeries

            Wave selSeries = cellSumDFR:selSeries

            redimension/N=(numpnts(cellEvents_series)) selSeries
            selSeries = 0
        else
            makeBlank = 1
        endif
    else 
        makeBlank = 1
    endif

    if(makeBlank)
        Wave selCellSeries = cellSumDFR:selCellSeries
        redimension/N=0 selCellSeries

        Wave selSeries = cellSumDFR:selSeries
        redimension/N=0 selSeries
        // selSeries = 0
    endif
end


function updateConcSeriesLB(string cellName)
    DFREF cellSumDFR = getCellSummaryDF(cellName)
    DFREF panelCellSum = getEventsCellSumOutDF()
    make/O/N=0/T panelCellSum:concSeries
    
    if(DataFolderRefStatus(cellSumDFR)!=0)
        // PrintAllWaveNames(cellSumDFR)
        Wave/SDFR=cellSumDFR combinedSeriesW
        if(WaveExists(combinedSeriesW))
            // print "combinedSeriesW wave exists"
            make/T/O/N=(numpnts(combinedSeriesW)) panelCellSum:concSeries = num2str(combinedSeriesW)
        endif
    endif
end


///////////
/// updateConcHistoProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-06
/// NOTES: 
///////////
function updateConcHistoProc(SV_Struct) :SetVariableControl
    STRUCT WMSetVariableAction & SV_Struct
    if(SV_Struct.eventcode == 8 || SV_Struct.eventcode == 6) // end edit
        string panelName = SV_Struct.win
        string cellName
        strswitch (panelName)
            case "AGG_events":
                cellName = getSelectedItem("cellListBox_cellConcat", hostName = SV_Struct.win)
                plotPTBHisto(cellName)
                displayCellParams(suppressWarning = 1)
                
                break
            case "AGG_bursts":
                displayBurstPlot(panelName = SV_Struct.win)
                break
            default:
                
                break
        endswitch
    endif
    return 0
end

///////////
/// updateConcHistoProc_check
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: 
///////////
function updateConcHistoProc_check(CB_Struct) : CheckBoxControl
    STRUCT WMCheckboxAction & CB_Struct
    if(CB_Struct.eventcode == 2) // Mouse up
        string panelName = CB_Struct.win
        string cellName
        strswitch (panelName)
            case "AGG_events":
                cellName = getSelectedItem("cellListBox_cellConcat", hostName = CB_Struct.win)
                plotPTBHisto(cellName)
                displayCellParams(suppressWarning = 1)
                
                break
            case "AGG_bursts":
                displayBurstPlot(panelName = CB_Struct.win)
            default:
                
                break
        endswitch
    endif
    return 0
end

///////////
/// getShowConcHisto
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: Get the state of the showHisto checkbox for cerebro
///////////
function getShowConcHisto()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable showHisto
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR gShowHisto = showHisto
        if(numtype(gShowHisto) ==0)
            showHisto = gShowHisto
        endif  
    endif

    if(numtype(showHisto) !=0)
        showHisto = 1
        string ctrlPanelN = "AGG_Events"
        ControlInfo/W=$ctrlPanelN showHistoCheck
        if(V_flag<0)
            print "no showHistoCheck control found, defaulting to show"
        else
            if(V_value == 0)
                showHisto = 0
            endif
        endif
    endif

	
	return showHisto
end

///////////
/// getShowConcGaps
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: Get the state of the showGaps checkbox for cerebro
///////////
function getShowConcGaps()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable showGaps
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR gShowGaps = showGaps
        if(numtype(gShowGaps) ==0)
            showGaps = gShowGaps
        endif  
    endif

    if(numtype(showGaps) !=0)
        showGaps = 1
        string ctrlPanelN = "AGG_Events"
        ControlInfo/W=$ctrlPanelN showGapsCheck
        if(V_flag<0)
            print "no showGapsCheck control found, defaulting to show"
        else
            if(V_value == 0)
                showGaps = 0
            endif
        endif
    endif

	
	return showGaps
end

///////////
/// getShowConcChopped
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: Get the state of the showChopped checkbox for cerebro
///////////
function getShowConcChopped()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable showChopped
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR gShowChopped = showChopped
        if(numtype(gShowChopped) ==0)
            showChopped = gShowChopped
        endif  
    endif

    if(numtype(showChopped) !=0)
        showChopped = 1
        string ctrlPanelN = "AGG_Events"
        ControlInfo/W=$ctrlPanelN showChoppedCheck
        if(V_flag<0)
            print "no showChoppedCheck control found, defaulting to show"
        else
            if(V_value == 0)
                showChopped = 0
            endif
        endif
    endif

	
	return showChopped
end

///////////
/// getShowCityPlot
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: Get the state of the showChopped checkbox for cerebro
///////////
function getShowCityPlot()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable showCityPlot
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR gshowCityPlot = showCityPlot
        if(numtype(gshowCityPlot) ==0)
            showCityPlot = gshowCityPlot
        endif  
    endif

    if(numtype(showCityPlot) !=0)
        showCityPlot = 1
        string ctrlPanelN = "AGG_Bursts"
        ControlInfo/W=$ctrlPanelN showCityPlotCheck
        if(V_flag<0)
            print "no showCityPlotCheck control found, defaulting to show"
        else
            if(V_value == 0)
                showCityPlot = 0
            endif
        endif
    endif

	
	return showCityPlot
end

///////////
/// getColorConcHisto
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: Get the state of the colorHisto checkbox for cerebro
///////////
function getColorConcHisto()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable colorHisto
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR gColorHisto = colorHisto
        if(numtype(gColorHisto) ==0)
            colorHisto = gColorHisto
        endif  
    endif

    if(numtype(colorHisto) !=0)
        colorHisto = 1
        string ctrlPanelN = "AGG_Events"
        ControlInfo/W=$ctrlPanelN colorHistoCheck
        if(V_flag<0)
            print "no colorHistoCheck control found, defaulting to color"
        else
            if(V_value == 0)
                colorHisto = 0
            endif
        endif
    endif

	
	return colorHisto
end

Function updateRegionUnitsProc_byCell(cb) : CheckBoxControl
    STRUCT WMCheckboxAction& cb
    
    
    // don't change things when the control is killed
    //Otherwise, it switches the value to 1 when macro closed
    if(cb.eventcode != -1) 
        DFREF panelDFR = getEventsCellSumOutDF()
        NVAR regionTimeUnits= panelDFR:regionTimeUnits
        variable previousUnits = regionTimeUnits

        strswitch (cb.ctrlName)
            case "regionsAsSec_check":
                regionTimeUnits = 1
                break
            case "regionsAsMin_check":
                regionTimeUnits = 2
                break
            case "regionsAsTime_check":
                regionTimeUnits = 3
                break
        endswitch
        CheckBox regionsAsSec_check, value= regionTimeUnits==1
        CheckBox regionsAsMin_check, value= regionTimeUnits==2
        CheckBox regionsAsTime_check, value= regionTimeUnits==3

        string cellName = getSelectedItem("cellListBox_cellConcat", hostName = cb.win)
        setRegionUnitsForCell(cellName, unitsType = regionTimeUnits)
        updateCellConcDur(cellName)
        changeCellRegionsEntryWavesForUnits(cellName, previousUnits, regionTimeUnits)
    endif


    return 0
End

///////////
/// setRegionUnitsForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-08
/// NOTES: unitsType 1 = sec, 2 = min
///////////
function setRegionUnitsForCell(string cellName[, variable unitsType])
    DFREF cellDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        return 0
    endif

    if(paramIsDefault(unitsType))
        DFREF panelDFR = getEventsCellSumOutDF()
        NVAR regionTimeUnits= panelDFR:regionTimeUnits
        unitsType = regionTimeUnits
    endif
    

    variable/G cellDFR:regionTimeUnits/N=regionTimeUnits
    regionTimeUnits = unitsType
end

///////////
/// changeCellRegionsEntryWavesForUnits
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: units: 1 = sec, 2 = min
///////////
function changeCellRegionsEntryWavesForUnits(string cellName, variable prevUnits, variable newUnits)
    if(strlen(cellName)==0)
        return NaN
    endif

    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellInfoDFR)==0)
        return NaN
    endif

    acceptChangesInTable("AGG_events#cellRegionInfoTable")

    Wave/SDFR=cellInfoDFR regionsStarts_entry, regionsEnds_entry
    if(!WaveExists(regionsStarts_entry) || !WaveExists(regionsEnds_entry))
        print "changeCellRegionsEntryWavesForUnits: Couldn't find waves for cell", cellName
        return NaN
    endif

    DFREF cellSumDFR = getCellSummaryDF(cellName)
    NVAR/Z/SDFR=cellSumDFR seriesTime
    variable startTime = 0
    if(NVar_Exists(seriesTime) && numtype(seriesTime)==0)
        startTime = seriesTime
    endif

    if(prevUnits == 1 && newUnits == 2) // seconds to minutes
        regionsStarts_entry /= 60
        regionsEnds_entry /= 60
    elseif (prevUnits == 2 && newUnits == 1) // minutes to seconds
        regionsStarts_entry *= 60
        regionsEnds_entry *= 60
    elseif (prevUnits == 1 && newUnits == 3) // seconds to time
        regionsStarts_entry += startTime
        regionsEnds_entry += startTime
    elseif (prevUnits == 2 && newUnits == 3) // min to time
        regionsStarts_entry *= 60
        regionsEnds_entry *= 60
        regionsStarts_entry += startTime
        regionsEnds_entry += startTime
    elseif (prevUnits == 3 && newUnits == 1) // time to sec
        regionsStarts_entry -= startTime
        regionsEnds_entry -= startTime
    elseif (prevUnits == 3 && newUnits == 2) // time to min
        regionsStarts_entry -= startTime
        regionsEnds_entry -= startTime
        regionsStarts_entry /= 60
        regionsEnds_entry /= 60
    else
            // this can happen when click around radio boxes but not on them
            // print "changeCellRegionsEntryWavesForUnits: prev and new units are the same. Not changing anything"
    endif

    string tableName = "AGG_Events#cellRegionInfoTable"
    SetActiveSubwindow $tableName
    if(newUnits == 1 || newUnits == 2)
        ModifyTable/W=$tableName format(regionsStarts_entry) = 0, format(regionsEnds_entry) = 0
        ModifyTable/W=$tableName showParts = 255
        ModifyTable/W=$tableName autosize={0, 0, -1, 0, 10}
    elseif(newUnits == 3)
        ModifyTable/W=$tableName format(regionsStarts_entry) = 7, format(regionsEnds_entry) = 7
        ModifyTable/W=$tableName showParts = 126
        ModifyTable/W=$tableName autosize={0, 0, -1, 0, 10}
    endif
end


///////////
/// getRegionUnitsForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-08
/// NOTES: unitsType 1 = sec, 2 = min
///////////
function getRegionUnitsForCell(string cellName)
    DFREF cellDFR = getEventsCellInfoDF(cellName)
    if(dataFolderRefStatus(cellDFR)==0)
        return NaN
    endif

    NVAR/Z/SDFR=cellDFR regionTimeUnits
    if(NVAR_Exists(regionTimeUnits))
        return regionTimeUnits
    else
        return NaN
    endif
end

///////////
/// setDisplayedUnitsForCellUnits
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: When setting up the panel for a cell, need to change the displayed time units to match the choice for that cell
///////////
function setDisplayedUnitsForCellUnits(string cellName)
    variable cellUnits = getRegionUnitsForCell(cellName)

    DFREF panelDFR = getEventsCellSumOutDF()
    NVAR regionTimeUnits= panelDFR:regionTimeUnits
    if(numtype(cellUnits)==0)
        regionTimeUnits = cellUnits
    endif

    CheckBox regionsAsSec_check, value= regionTimeUnits==1
    CheckBox regionsAsMin_check, value= regionTimeUnits==2
    CheckBox regionsAsTime_check, value= regionTimeUnits==3
end


///////////
/// updateRegWavesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Change the dimensions of the regions wave
///////////
function updateRegWavesProc(SV_Struct) :SetVariableControl
    STRUCT WMSetVariableAction & SV_Struct
    if(SV_Struct.eventcode == 8 || SV_Struct.eventcode == 6) // end edit
        string cellName = getSelectedItem("cellListBox_cellConcat", hostName = SV_Struct.win)
        updateRegionWavesDims(cellName)
    endif
    return 0
end


///////////
/// updateRegionsButtonProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: 
///////////
function updateRegionsButtonProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        // get selected cell
        string cellName = getSelectedItem("cellListBox_cellConcat", hostName = B_Struct.win)

        updateRegions(cellName)
    endif
    return 0
end