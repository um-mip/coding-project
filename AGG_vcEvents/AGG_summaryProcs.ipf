function AGG_summarizeEvents()
    // // set a flag to follow up at the end of the loop
    // //	variable summflag = 
    // //mmake waves to store info: time, series name, parameter
    // if(strlen(extension) != 0) // AGG - 2022-01-02 temporary fix to check if the extension is not empty
    //     // This check prevents problems with the "events" selection, as it is not summarized, and has no extension
    //     // Leads to a wave with the name of "summary" but this causes problems because there's already a macro by this name 
    //     string paramwn = "summary"+extension
    //     make/D/N=(nawaves)/O summarytime
    //     make/T/N=(nawaves) /O summaryname
    //     /////
    //     // AGG - troubleshooting what happens when "events" is selected for summary table
    //     // Error that "name already exists as a macro"
    //     // It's because there is no extension for events, so the paramwn is just "summary"
    //         print(paramwn)
    //     ////
    //         make/N=(nawaves) /O $paramwn
    //     WAVE pw = $paramwn
    //     //loop over file list wave
    //     iwave=0
    //     do
    //         waven=removequotes(iw[iwave])+extension
    //         summaryname[iwave]=iw[iwave]
    //         summarytime[iwave]=PMsecs2Igor(acqtime(removequotes(iw[iwave])))
    //         WAVE thiswave = $waven
    //         //appendtotable/W=summarytable thiswave
    //         wavestats/Q/Z thiswave
    //         pw[iwave]=V_avg
    //         iwave+=1
    //     while(iwave<nawaves)
    //     SetScale d 0,0,"dat", summarytime

    //     if(iwave>1)
    //         display/k=1 pw vs summarytime // make bonus time plots
    //         rename pw $paramwn
    //     endif
    //         //average parameter
    //         //get time
    //         //store
    //     break
    // endif
end

function AGG_makeSummaryTable()
end

function AGG_summarizeParameter(extension)
    string extension
end

function AGG_summarizeCountAndFreq(duration)
    variable duration
	// variable i=0,n=0,nevents=0
	// string intwave ="",intext="_int",freqwave=""
	// n=dimsize(summaryname,0)
	// if(numtype(n)==0)
	// 	make/O/N=(n) summarycount
	// 	make/O/N=(n) summaryfreq
	// 	WAVE/T sn=summaryname
	// 	summarycount=0
	// 	summaryfreq=0
	// 	for(i=0;i<n;i+=1)
	// 		intwave=sn[i]+intext
	// 		WAVE tempw = $intwave
	// 		summarycount[i]=dimsize(tempw,0)
	// 		// AGG - 2022-01-02: This is hard coded to assume a duration of 120 seconds, which is not true
	// 		summaryfreq[i]=summarycount[i]/120
	// 	endfor
	// endif
end

