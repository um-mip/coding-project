function createConfirmationTab(panelName, tabNum, panelSize)
    string panelName
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""
    string alwaysHidetabControls = ""

    tabControls += "cellNamesText_series;cellListBox_series;seriesNamesText_series;seriesListBox_series;"

    variable ypos = panelSize.yPosPnts + panelSize.listBoxHeight*2 + panelSize.buttonHeight*2
    Button showWaveIntrinsic, pos = {panelSize.xPosPnts, ypos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button showWaveIntrinsic, title = "Wave Intrinsic", proc = plotWaveIntrinsicProc
    tabControls += "showWaveIntrinsic;"

    ypos += panelSize.buttonHeight
    Button showCerebro, pos = {panelSize.xPosPnts, ypos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button showCerebro, title = "CEREBRO - for sel'd cell", proc = plotCerebroProc
    tabControls += "showCerebro;"
    ypos += panelSize.buttonHeight
    Button recheckAvgList, pos = {panelSize.xPosPnts, ypos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button recheckAvgList, title = "Rescan events for avg", proc = recheckAvgListProc
    tabControls += "recheckAvgList;"
    ypos += panelSize.buttonHeight
    Button showAvgList, pos = {panelSize.xPosPnts, ypos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button showAvgList, title = "Show avg event and list", proc = showAvgListAndAvgEventProc
    tabControls += "showAvgList;"

    // Detection controls
        tabControls += "deriv_Presmooth;"
        tabControls += "deriv_thresh;"
        tabControls += "deriv_min_dur;"
        tabControls += "deriv_smooth;"
        tabControls += "dp_window;"
        tabControls += "peak_sign;"
        tabControls += "peak_thresh;"
        tabControls += "peak_smth_pts;"
        tabControls += "peak_a_thresh;"
        tabControls += "base_offset;"
        tabControls += "base_dur;"
        tabControls += "trace_dur;"
        tabControls += "trace_base_dur;"
        tabControls += "trace_ave_cutoff;"
    // end detection controls

    DFREF confirmDF = getEvConfirmDF()
    if(DataFolderRefStatus(confirmDF) == 0)
        createEvConfirmDF()
        confirmDF = getEvConfirmDF()
    endif

    DFREF currentDF = GetDataFolderDFR()

    SetDataFolder confirmDF

    // Note that these are being created in the confirmDF
    variable/g gpredsmooth=11,gdsmooth=1,gdthresh=10,gdmin_dur=0.5,gdmax_dur=5,gdpwin=2
    variable/g gdetectsign=-1
    variable/g gboffset=2,gbdur=5,gpthresh=5,gpsmooth=3
    variable/g gathresh=1,gawin=10
    variable/g gtdur=200,gtoffset=10
    variable /g gavecut=1000
    variable/g gxmin1=0,gxmax1=120, gymin1=0,gymax1=1
    variable /g gxmin2=0,gxmax2=120, gymin2=0,gymax2=1
    variable /g g_autopass=0

    STRUCT analysisParamsFromNVARS confirmParams
    StructFill/AC=1/SDFR=confirmDF confirmParams

    variable p_xpos = panelSize.left, p_ypos = 60, p_dx = 710, p_dy = 650
    variable col0start=panelSize.xPosPnts + posRelPanel(0.35, "width", panelName = panelName)
    variable col1start=panelSize.xPosPnts + posRelPanel(0.55, "width", panelName = panelName) 
    variable col2start =panelSize.xPosPnts + posRelPanel(0.75, "width", panelName = panelName)
    variable col3start=panelSize.xPosPnts + posRelPanel(0.85, "width", panelName = panelName)

    variable paramWidth = panelSize.buttonWidth
    variable paramHeight = panelSize.buttonHeight - 3
    variable paramCol0start=panelSize.xPosPnts + posRelPanel(0.34, "width", panelName = panelName)
    variable paramCol1start= paramCol0start + (paramWidth + 4) * 1
    variable paramCol2start= paramCol0start + (paramWidth + 4) * 2
    variable paramCol3start= paramCol0start + (paramWidth + 4) * 3

    variable rowstart=posRelPanel(0.5, "height", panelName = panelName)

    variable rowinc=panelSize.buttonHeight,bw1=panelSize.buttonWidth,bw2=panelSize.buttonWidth

    variable  clearButtonX=col3start, clearButtonY= posRelPanel(0.8, "height", panelName = panelName), tcinc=panelSize.buttonHeight*0.05 //600,tcinc=32

    TitleBox confirmTitleBox pos = {paramCol0start, rowstart - rowinc}, title = "Parameters for Selected Cell:", frame=0, fstyle=1
    tabControls += "confirmTitleBox;"
    
    // TitleBox updateParamsInfo pos = {paramCol1start, rowstart - rowinc}, title = "U:", frame=0, fstyle=1, fsize = 12
    // tabControls += "updateParamsInfo;"

    Button updateDetectParams pos = {paramCol0start, rowstart - rowinc * 3.5}, size={bw2,panelSize.buttonHeight*2}, title = "Update detect params\nw/ cell vals", proc = updateDetectParams
    tabControls += "updateDetectParams;"
    
    Button changeTraceDurationButton pos = {paramCol3start, rowstart - rowinc * 3.5}, size={bw2,panelSize.buttonHeight}, title = "Change series trace duration", proc = changeTraceDurationProc
    tabControls += "changeTraceDurationButton;"

    variable/g updatedTraceDur = NaN
    SetVariable updatedTraceDurVar, pos = {paramCol3start, rowstart - rowinc * 3.5 + panelSize.buttonHeight}, size={bw2, panelSize.buttonHeight}, title = "New trace dur (ms):"
    SetVariable updatedTraceDurVar, value = updatedTraceDur
    tabControls += "updatedTraceDurVar;"

    // set up detection parameter handling
    String tabControlsNoEdit = ""
    
    variable xPos // ypos made above
    xPos = paramCol0start; yPos = rowstart

    // Deriv Pre-smooth
    SetVariable conf_deriv_Presmooth, pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Pre-smooth (points)"
    SetVariable conf_deriv_Presmooth,limits={0,1000,1},value= gpredsmooth
    tabControls += "conf_deriv_Presmooth;"
    tabControlsNoEdit += "conf_deriv_Presmooth;"
    yPos += rowInc

    // Deriv max. width
    // AGG 2024-05-22, this variable doesn't appear to be used in detection, so removing from interface
    SetVariable conf_deriv_max_dur,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Max. Width (ms)"
    SetVariable conf_deriv_max_dur,limits={0,1000,0.2},value= gdmax_dur	
    alwaysHidetabControls += "conf_deriv_max_dur;"
    
    // Deriv min width
    SetVariable conf_deriv_min_dur,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Min. Width (ms)"
    SetVariable conf_deriv_min_dur,limits={0,1000,0.2},value= gdmin_dur
    tabControls += "conf_deriv_min_dur;"
    tabControlsNoEdit += "conf_deriv_min_dur;"
    yPos += rowInc

    // Deriv smoothing
    SetVariable conf_deriv_smooth,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Smoothing (points) "
    SetVariable conf_deriv_smooth,limits={0,1000,1},value= gdsmooth
    tabControls += "conf_deriv_smooth;"
    tabControlsNoEdit += "conf_deriv_smooth;"
    yPos += rowInc

    // Deriv threshold
    SetVariable conf_deriv_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Threshold (pA/ms)"
    // deriv thresh label color
    SetVariable conf_deriv_thresh,limits={-inf,inf,0.1},value= gdthresh, proc=testingsetvar,labelBack=(65535,0,0)
    tabControls += "conf_deriv_thresh;"
    tabControlsNoEdit += "conf_deriv_thresh;"
    yPos += rowInc

    xPos = paramCol1start
    yPos = rowStart

    // Window search
    SetVariable conf_dp_window,pos={xPos,yPos},size={paramWidth, paramHeight},title="Window search (ms)"
    SetVariable conf_dp_window,limits={0,inf,1},value= gdpwin
    tabControls += "conf_dp_window;"
    tabControlsNoEdit += "conf_dp_window;"
    yPos += rowInc
    
    // Peak sign
    SetVariable conf_peak_sign,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak sign"
    SetVariable conf_peak_sign,limits={-1,1,1},value= gdetectsign
    tabControls += "conf_peak_sign;"
    tabControlsNoEdit += "conf_peak_sign;"
    yPos += rowInc
    
    // Peak smoothing
    SetVariable conf_peak_smth_pts,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak Smoothing (points)"
    SetVariable conf_peak_smth_pts,limits={0,1000,1},value= gpsmooth
    tabControls += "conf_peak_smth_pts;"
    tabControlsNoEdit += "conf_peak_smth_pts;"
    yPos += rowInc

    // Peak threshold
    SetVariable conf_peak_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak threshold (pA)"
    // peak thresh label color	
    SetVariable conf_peak_thresh,limits={0,inf,1},value= gpthresh, proc=testingsetvar,labelBack=(3,52428,1)
    tabControls += "conf_peak_thresh;"
    tabControlsNoEdit += "conf_peak_thresh;"
    yPos += rowInc

    xPos = paramCol2start
    yPos = rowStart

    // Area threshold
    SetVariable conf_peak_a_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Area threshold (pA*ms)"
    SetVariable conf_peak_a_thresh,limits={0,inf,1},value= gathresh
    tabControls += "conf_peak_a_thresh;"
    tabControlsNoEdit += "conf_peak_a_thresh;"
    yPos += rowInc

    // Area window
    // AGG 2024-05-22: Hiding because parameter isn't in use
    SetVariable conf_peak_a_win,pos={xPos,yPos},size={paramWidth, paramHeight},title="Area Window (ms)"
    SetVariable conf_peak_a_win limits={0,inf,1},value= gawin
    alwaysHidetabControls += "conf_peak_a_win"

    // Ave cutoff
    SetVariable conf_trace_ave_cutoff,pos={xPos,yPos},size={paramWidth,paramHeight},title="Ave cutoff (pA)"
    tabControls += "conf_trace_ave_cutoff;"
    tabControlsNoEdit += "conf_trace_ave_cutoff;"
    SetVariable conf_trace_ave_cutoff,limits={0,inf,1},value= gavecut
    yPos += rowInc

    // Baseline offset
    SetVariable conf_base_offset,pos={xPos,yPos},size={paramWidth,paramHeight},title="Baseline offset (ms)"
    // baseline offset label color	
    SetVariable conf_base_offset,limits={0,inf,1},value= gboffset,labelback=(16385,35088,50000, 30000)
    tabControls += "conf_base_offset;"
    tabControlsNoEdit += "conf_base_offset;"
    yPos += rowInc
    
    // Baseline duration
    SetVariable conf_base_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Baseline duration (ms)"
    SetVariable conf_base_dur,limits={0,inf,1},value= gbdur
    tabControls += "conf_base_dur;"
    tabControlsNoEdit += "conf_base_dur;"
    yPos += rowInc

    xPos = paramCol3start
    yPos = rowStart
    
    // Trace duration
    SetVariable conf_trace_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Trace duration (ms)"
    // Set background color
    SetVariable conf_trace_dur,limits={0,inf,10},value= gtdur,labelback=(65535,32768,58981)
    tabControls += "conf_trace_dur;"
    tabControlsNoEdit += "conf_trace_dur;"
    yPos += rowInc

    // Trace offset
    SetVariable conf_trace_base_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Trace offset (ms)"
    SetVariable conf_trace_base_dur,limits={0,inf,10},value= gtoffset
    tabControls += "conf_trace_base_dur;"
    tabControlsNoEdit += "conf_trace_base_dur;"
    yPos += rowInc
    
    SetDataFolder currentDF

    xPos = panelSize.xPosPnts
    xPos += panelSize.buttonWidth * 1.1

    ypos = posRelPanel(0.5, "height", panelName = panelName) - rowinc

    variable placeHolderCheck = 0
    CheckBox cellDetectConfirmCheck, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title="Confirmed detection", value = placeHolderCheck
    tabControls += "cellDetectConfirmCheck;"

    addTabUserData(tabControls, tabNum)
    addTabUserData_forNoEdit(tabControlsNoEdit, tabNum)
    addTabUserData(alwaysHidetabControls, NaN)
    // addDisplayUserData(tabDisplays, tabNum, panelName)
end

structure analysisParamsFromNVARS
    NVAR gpredsmooth, gdsmooth, gdthresh, gdmin_dur, gdmax_dur, gdpwin
    NVAR gdetectsign, gboffset, gbdur, gpthresh, gpsmooth
    NVAR gathresh, gawin, gtdur, gtoffset, gavecut
    NVAR gxmin1, gxmax1, gymin1, gymax1
    NVAR gxmin2, gxmax2, gymin2, gymax2
    NVAR g_autopass
endstructure