function createSeriesGraphTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""
    string tabControls = ""

    // createSeriesOutputDF()
    DFREF seriesOutDFR = getSeriesOutputDF()

    Display /W=(0.2, 0.07, 0.93, 0.95) /L/B /HOST=$panelName /HIDE=(1)
    RenameWindow #, groupSeriesPlot
    tabDisplays += "groupSeriesPlot;"

    string name_wn = "Parameter"
    TitleBox parameterText pos = {panelSize.xPosPnts, panelSize.yPosPnts}, title = "Select a parameter:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    tabControls += "parameterText;"

    STRUCT outputWaves outputWaves
    fillOutWavesStrings(outputWaves)
    // string analysis_types = "sumCount;sumFreq;pks;int;der;t50r;fwhm;Rinput;RseriesSub;capa;holdingc"
    // string analysis_types = outputWaves.analysis_types
    string analysis_types = outputWaves.niceNames
    
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    string nameVariable = ""

    make/O/T/N=(numAnalyses) analysisListW

    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameAnalysis = StringFromList( iAnalysis, analysis_types )
        analysisListW[iAnalysis] = nameAnalysis
    endfor

    Make /B /O /N=(numAnalyses) analysisSelW

    ListBox analysisLB, mode=1, listwave = analysisListW, selwave=analysisSelW, pos={panelSize.xPosPnts, panelSize.yPosPnts + panelSize.buttonHeight}, size={panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = updateGroupGraphProc
    tabControls += "analysisLB;"
    tabControls += "convertUnitsCheck;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    Button saveSeriesGroups, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="groupSeriesPlot", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveSeriesGroups;"

    
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end