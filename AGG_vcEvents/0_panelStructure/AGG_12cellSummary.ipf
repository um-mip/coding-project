function createCellSummaryTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""
    tabControls += "convertUnitsCheck;"
    // tabControls += "useAvgSubCheck;"
    // tabControls += "limitEventsPerCellCheck;"
    tabControls += "maxEventsPerCellVar;"
    tabControls += "intMaxEventsVar;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    tabControls += "histoInfoText_cell;"
    tabControls += "showGapsCheck;"
    tabControls += "showChoppedCheck;"
    tabControls += "showHistoCheck;"
    tabControls += "colorHistoCheck;"
    tabControls += "binDurVar;"

    string tabNumStr = num2str(tabNum)

    createEventsCellSumOutDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable yPos = panelSize.yPosPnts
    variable xPos = panelSize.xPosPnts
    variable buttonWidth = panelSize.buttonWidth
    variable buttonHeight = panelSize.buttonHeight

    tabControls += "cellNamesText_cell;"

    // Cell Select Listbox
    string name_wn = "cellName"
    Make /B /O cellSumDFR:cellSelWave = 0
    yPos += buttonHeight
    ListBox cellListBox_cell, mode=1, listwave = $name_wn, selwave=cellSumDFR:cellSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = selectedCellListProc_cell
    tabControls += "cellListBox_cell;"
    yPos += panelSize.listBoxHeight

    // Make region listbox
    TitleBox regionSelect_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select region:", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelect_text;"
    yPos += buttonHeight
    make/t/O/N=0 cellSumDFR:cellRegionNames/Wave=cellRegionNames
    Make/B/O/N=0 cellSumDFR:regionSelWave_cell = 0
    ListBox regionListBox_cell, mode=1, listwave = cellRegionNames, selwave=cellSumDFR:regionSelWave_cell, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight * 0.5}, proc = selectedRegionListProc_cell
    tabControls += "regionListBox_cell;"
    yPos += panelSize.listBoxHeight * 0.5


    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder cellSumDFR

    Struct outputWaves outputWaves
    StructFill/SDFR=cellSumDFR/AC=3 outputWaves
    fillOutWavesStrings(outputWaves)

    edit /N=cellSummaryTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.7, 0.95, 0.93) sumCount, sumTime, sumFreq, duration, Rinput, RseriesSub, capa, holdingc, pks, int, der, t50r, fwhm, decay9010, decayTimes, tauFits, y0Fits, AFits, x0Fits
    ModifyTable width(sumTime) = 60, format(sumTime)=7
    ModifyTable showParts=126
    tabDisplays += "cellSummaryTable;"

    SetDataFolder currentDF

    Wave/T listWave, titleWave
    Wave selWave
    [listWave, titleWave, selWave] = makeDisplayParamsWaves()
    xPos = posRelPanel(0.80, "width", panelName = panelName)
    yPos = panelSize.yPosPnts + panelSize.buttonHeight
    ListBox paramListBox_cell, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight*.5}, listWave = titleWave, selWave = selWave
    ListBox paramListBox_cell, proc=displayCellParamsProc
    tabControls += "paramListBox_cell;"

    tabControls += "paramTitle_series;"
    yPos += panelSize.buttonHeight
    yPos += panelSize.listBoxHeight * 0.5
    
    runMakeCellParams()
    make/O/N=0/T cellSumDFR:blankTitle/Wave=titleWave_params
    make/O/N=0 cellSumDFR:blankSel/Wave=selWave_params
    ListBox paramListBox_cellParams, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight*0.9}, listWave = titleWave_params, selWave = selWave_params
    ListBox paramListBox_cellParams, proc=displayCellParamsProc
    tabControls += "paramListBox_cellParams;"

    variable left = xPos / posRelPanel(1, "width", panelName = panelName)
    variable top = (yPos - buttonHeight) / posRelPanel(1, "height", panelName = panelName)
    variable right = 0.99
    variable bottom = 0.68
    edit/N=cellSumHistoTable/Hide=(1)/K=1/Host=$panelName/W=(left, top, right, bottom)

    Display/N=cellSumGraph/Hide=(1)/K=1/Host=$panelName/W=(0.2, 0.1, 0.78, 0.68)
    tabDisplays += "cellSumGraph;"

    yPos += panelSize.listBoxHeight*0.9


    xPos = posRelPanel(0.2, "width", panelName = panelName)
    yPos = posRelPanel(0.1, "height", panelName = panelName)-panelSize.buttonHeight
    Button saveCellSum, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="cellSumGraph", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveCellSum;"

    xPos += panelSize.buttonWidth
    Button openTauFit, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = openTauFitProc, title = "open tau fit panel"
    tabControls += "openTauFit;"
    
    xPos += panelSize.buttonWidth
    Button fitTauCell, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = fitTauCellProc, title = "fit tau for cell"
    tabControls += "fitTauCell;"
    
    xPos += 2*panelSize.buttonWidth
    Button fitTauAllCells, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = fitTauAllCellsEventsProc, title = "fit tau for ALL cells"
    tabControls += "fitTauAllCells;"

    
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-08
/// NOTES: Switched to use displayListNameWaves instead of listNameWaves structure field
/// separating out the type of graph from the parameter that is being plotted
function [wave/T listWave, wave/T titleWave, wave selWave] makeDisplayParamsWaves()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder cellSumDFR

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string listWaveNames = cellSum.displayListNameWaves
    string niceNames = cellSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/O/N=0/T listWave, titleWave
    make/O/N=0 selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave, titleWave, selWave]
end

///////////
/// makeCellParamWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// EDITOR: Amanda Gibson
/// NOTES: 
/// Create the list wave, title wave, and select wave for the Cell summary parameter waves
/// uses the CellSummaryInfo structure to get these waves and names
/// the dispType variable tells the function which field of the cellSummaryInfo structure
/// to get the liststring from
/// options are
/// - seriesByTime - parameters available to show the average series val over time
/// - dist - parameters available to show the distribution of events for the cell
///////////
function [wave/T listWave, wave/T titleWave, wave selWave] makeCellParamWaves([string dispType])
    if(paramIsDefault(dispType))
        dispType = "seriesByTime"
    endif
    
    DFREF cellSumDFR = getEventsCellSumOutDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder cellSumDFR

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    
    string listWaveNames = "", listWaveN, titleWaveN, selWaveN
    strswitch (dispType)
        case "seriesByTime":
            listWaveNames = cellSum.analysisTypes
            listWaven = "listWave_seriesParams"
            titleWaven = "titleWave_seriesParams"
            selWaven = "selWave_seriesParams"
            break
        case "dist":
            listWaveNames = cellSum.avgByEventWaves
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        default:
            listWaveNames = cellSum.analysisTypes
            listWaven = "listWave_seriesParams"
            titleWaven = "titleWave_seriesParams"
            selWaven = "selWave_seriesParams"
            break
    endswitch
    string niceNames = cellSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/N=0/T/O cellSumDFR:$listWaveN/Wave=listWave
    make/N=0/T/O cellSumDFR:$titleWaveN/Wave=titleWave
    make/N=0/O cellSumDFR:$selWaveN/Wave=selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            if(strlen(thisNiceName) == 0)
                print "missing nice name for", thisWaveName
            endif
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave, titleWave, selWave]
end

function runMakeCellParams()
    [wave/T listWave, wave/T titleWave, wave selWave] = makeCellParamWaves(dispType = "seriesByTime")
    [wave/T listWave, wave/T titleWave, wave selWave] = makeCellParamWaves(dispType = "dist")
end