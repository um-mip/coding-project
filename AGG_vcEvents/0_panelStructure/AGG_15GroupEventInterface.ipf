function createGroupEventTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""
    string tabControls = ""

    tabControls += "regionSelect_text_all;"
    tabControls += "regionListBox_all;"

    Display/W=(0.2, 0.07, 0.82, 0.95) /L/B /HOST=$panelName /HIDE=(1)
    RenameWindow #, groupEventPlot
    tabDisplays += "groupEventPlot;"
    edit/W=(0.2, 0.07, 0.82, 0.95)/HOST=$panelName /HIDE=(1)
    RenameWindow #, groupPropTable

    tabControls += "convertUnitsCheck;"
    
    tabControls += "parameterText;"

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts + panelSize.buttonHeight

    createAllGroupsOutputDF()
    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    
    Wave/T listWave, titleWave
    Wave selWave
    [listWave, titleWave, selWave] = makeGroupDisplayParamsWaves()

    ListBox paramListBox_group, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight*.5}, listWave = titleWave, selWave = selWave
    ListBox paramListBox_group, proc=displayGroupParamsProc
    tabControls += "paramListBox_group;"

    yPos += panelSize.buttonHeight
    yPos += panelSize.listBoxHeight * 0.5

    runMakeGroupsParams()
    Wave/SDFR=groupsGraphDFR titleWave_params = titleWave_groupAvgParams, selWave_params = selWave_groupAvgParams
    ListBox paramListBox_groupParams, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight * .5}, listWave = titleWave_params, selWave = selWave_params
    ListBox paramListBox_groupParams, proc=displayGroupParamsProc
    tabControls += "paramListBox_groupParams;"

    [listWave, titleWave, selWave] = makeGroupDisplayWaves()
    yPos += panelSize.buttonHeight + panelSize.listBoxHeight * .5
    ListBox paramListBox_whichGroup, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight*.5}, listWave = titleWave, selWave = selWave
    ListBox paramListBox_whichGroup, proc=displayGroupParamsProc
    tabControls += "paramListBox_whichGroup;"


    yPos += panelSize.buttonHeight + panelSize.listBoxHeight * .5

    tabControls += "updateGroupsButton;"

    xPos = panelSize.xPosPnts
    yPos = posRelPanel(0.8, "height", panelName = panelName)
    Button saveGroupTraceButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="groupEventPlot", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveGroupTraceButton;"

    yPos += panelSize.buttonHeight
    Button saveGroupEventOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveGroupEventOutButton, title = "Save table", proc = saveTableProc, userdata="tableName:groupPropTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveGroupEventOutButton;"

    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end


