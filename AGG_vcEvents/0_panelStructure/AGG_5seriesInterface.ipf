function createSeriesControls(panelName, panelDFR, panelSize)
    string panelName
    DFREF panelDFR
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string name_wn = "cellName"
    TitleBox cellNamesText_series pos = {panelSize.xPosPnts, panelSize.yPosPnts}, title = "Select a cell:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    
    Make /B /O cellSelWave_series = 0
    // Cell Select Listbox
    ListBox cellListBox_series, mode=1, listwave = $name_wn, selwave=cellSelWave_series, pos={panelSize.xPosPnts, panelSize.yPosPnts + panelSize.buttonHeight}, size={panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = selectedCellListProc_series

    TitleBox seriesNamesText_series pos = {panelSize.xPosPnts, panelSize.yPosPnts + panelSize.listBoxHeight + panelSize.buttonHeight}, title = "Select a series:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    // Series Select Listbox
    string selCellSeriesWN = "selCellSeries"
    if(! WaveExists($selCellSeriesWN))
        make/T/O $selCellSeriesWN
    endif

    string selSeriesWN = "selSeries"
    if(! WaveExists($selSeriesWN))
        make/O $selSeriesWN
    endif

    ListBox seriesListBox_series, mode=1, listwave = $selCellSeriesWN, selwave = $selSeriesWN, pos = {panelSize.xPosPnts, panelSize.yPosPnts + panelSize.listBoxHeight + panelSize.buttonHeight*2}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = selectedSeriesListProc_series
end