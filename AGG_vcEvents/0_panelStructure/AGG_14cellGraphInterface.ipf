function createCellsGraphTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""
    string tabControls = ""

    tabControls += "regionSelect_text_all;"
    tabControls += "regionListBox_all;"

    Display /W=(0.2, 0.07, 0.82, 0.95) /L/B /HOST=$panelName /HIDE=(1)
    RenameWindow #, groupCellsPlot
    tabDisplays += "groupCellsPlot;"

    tabControls += "parameterText;"
    tabControls += "analysisLB;"
    tabControls += "convertUnitsCheck;"

    Button saveCellsGroups, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="groupCellsPlot", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveCellsGroups;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    variable xPos, yPos
    xPos = panelSize.xPosPnts
    yPos = panelSize.yPosPnts + panelSize.buttonHeight
    yPos += panelSize.buttonHeight + panelSize.listBoxHeight * .5
    yPos += panelSize.buttonHeight + panelSize.listBoxHeight * .5
    yPos += panelSize.buttonHeight
    yPos += panelSize.listBoxHeight * 0.5
    Button updateGroupsButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = updateGroupWavesProc, title = "Update group waves"
    tabControls += "updateGroupsButton;"
    
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end