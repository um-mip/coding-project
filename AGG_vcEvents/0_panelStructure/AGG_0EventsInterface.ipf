#pragma rtGlobals = 3 // Use modern global access method and strict wave access
#include <Resize Controls Panel>

Structure AGG_EventsPanelSizeInfo
    variable xPos, yPos, xRightPos, xPosPnts, yPosPnts, xRightPosPnts, buttonWidth, buttonHeight, listBoxWidth, listBoxHeight, left, top, right, bottom, buttonWidthRel, buttonHeightRel, listBoxHeightRel
EndStructure

Structure AGG_Events_tabs
    NVAR infoTab, passiveViewTab, eventDetectTab, eventConfirmTab
    NVAR seriesEventsTab, seriesSumTab, cellSumTab, cellConcatTab, seriesOutTab
    NVAR cellOutTab, seriesGraphTab, cellsGraphTab, rawDataTab, groupEventTab
    SVAR tabLabels, tabVarNames//    NVAR optoTab
EndStructure

function fillEventsTabNums (tabStruct)
    STRUCT AGG_Events_tabs &tabStruct

    tabStruct.infoTab = 0
    tabStruct.passiveViewTab = 1
    tabStruct.rawDataTab = 2
    tabStruct.eventDetectTab = 3
    tabStruct.eventConfirmTab = 4
    tabStruct.seriesEventsTab = 5
    tabStruct.seriesSumTab = 6
    tabStruct.cellConcatTab = 7
    tabStruct.cellSumTab = 8
    tabStruct.seriesOutTab = 9
    tabStruct.cellOutTab = 10
    tabStruct.seriesGraphTab = 11
    tabStruct.cellsGraphTab = 12
    tabStruct.groupEventTab = 13
    // tabStruct.optoTab = 13

    tabStruct.tabLabels = "infoTab:Load Data;passiveViewTab:Passives;eventDetectTab:Detection;"
    tabStruct.tabLabels += "eventConfirmTab:Confirmation;seriesEventsTab:Series Events;seriesSumTab:Series Summary;"
    tabStruct.tabLabels += "cellConcatTab:Cell Concat;cellSumTab:Cell Summary;seriesOutTab:Series Output;cellOutTab:Cell Output;seriesGraphTab:By series plots;cellsGraphTab:By cell plots;"
    tabStruct.tabLabels += "rawDataTab:Raw Data;groupEventTab:Group traces;"
    tabStruct.tablabels += "optoTab:Opto;"
    tabStruct.tabVarNames = "infoTab;passiveViewTab;rawDataTab;eventDetectTab;eventConfirmTab;seriesEventsTab;seriesSumTab;cellConcatTab;cellSumTab;seriesOutTab;cellOutTab;seriesGraphTab;cellsGraphTab;groupEventTab"
end

function AGG_build_MultCellEvents_panel()
    DoWindow/F AGG_Events     // bring panel to the front if it exists
    if( V_Flag == 0 )       // panel doesn't exist
        createEventsDF()
        createEventsPanelDF()
        createEventsInfoDF()
        // createSCTwavesDF() // 2022-01-05 - Not relevant
        DFREF panelDFR = getEventsPanelDF()
        
        SetDataFolder panelDFR
        print "Current path:", GetDataFolder(1)

        string panelName = "AGG_Events"
        string udata = ""

        // define the screen parameter structure
        STRUCT ScreenSizeParameters ScrP
   
        // initialize it
        InitScreenParameters(ScrP)

        // Create the panel
        AGG_makePanel(5, 0, 95, 80, panelName)
        // NewPanel /K=1/W=(50, 50, 1250, 750)/N=$panelName
        modifypanel cbRGB=(50000,50000,50000)

        variable width, height
        [width, height] = getWindowDims(type = "point")

        // print width
        variable tabFSize = width/100
        variable panelFSize = width/88
        if(panelFSize>18)
            panelFSize = 18
        endif
        print "tab font size", tabFSize, "panel font size", panelFSize
        
        //the current publishing standard is 1 pt = 1/72"). In other words points are fixed in size independent of resolution. This is good since, all things properly configured, 12 point text on an monitor will be the same height when printed on paper.

        DefaultGUIFont/W=$panelName panel = {"Arial", panelFSize, 0}, all = {"Arial", panelFSize, 0}, TabControl = {"Arial", tabFSize, 0}
        DefaultGUIFont/W=$panelName graph = {"Arial", 16, 0}, table = {"Arial", panelFSize, 0}
        // DefaultFont

        // Use a structure for positioning to pass easily to subfunctions
        STRUCT AGG_EventsPanelSizeInfo panelSize
        
        // Variable and dimensions
        variable xPos = 0.02
        variable yPos = 0.06
        variable xRightPos = 0.8

        variable xPosPnts = posRelPanel(xPos, "width")
        variable yPosPnts = posRelPanel(yPos, "height")
        variable xRightPosPnts = posRelPanel(xRightPos, "width")

        variable buttonWidth, buttonHeight, listBoxWidth, listBoxHeight, buttonWidthRel = 0.15, buttonHeightRel = 0.04, listBoxHeightRel = 0.35
        [buttonWidth, buttonHeight] = xyRelPanel(buttonWidthRel, buttonHeightRel)
        [listBoxWidth, listBoxHeight] = xyRelPanel(buttonWidthRel, listBoxHeightRel)

        panelSize.xPos = xPos
        panelSize.yPos = yPos
        panelSize.xRightPos = xRightPos
        panelSize.xPosPnts = xPosPnts
        panelSize.yPosPnts = yPosPnts
        panelSize.xRightPosPnts = xRightPosPnts
        panelSize.buttonWidthRel = buttonWidthRel
        panelSize.buttonHeightRel = buttonHeightRel
        panelSize.buttonWidth = buttonWidth
        panelSize.buttonHeight = buttonHeight
        panelSize.listBoxWidth = listBoxWidth
        panelSize.listBoxHeight = listBoxHeight
        panelSize.listBoxHeightRel = listBoxHeightRel
        panelSize.left = 0.2
        panelSize.top = 0.1
        panelSize.right = 0.8
        panelSize.bottom = 0.95

        Struct AGG_Events_tabs tabStruct
        StructFill/SDFR=panelDFR/AC=1 tabStruct
        fillEventsTabNums(tabStruct) // Set values

        string tabs = tabStruct.tabVarNames
        string tablabels = tabStruct.tabLabels
        TabControl EventsTabs pos={0, 0}, size={posRelPanel(1, "width"),buttonHeight}, proc=changeDisplayForTab, userdata(panelDFR)=GetDataFolder(1, panelDFR), userdata(tabs)=tabs, userdata(tabLabels)=tabLabels
        makeTabControls("EventsTabs", tabs, tabLabels, panelDFR)

        createEventsUnitsDF()
        DFREF unitsDFR = getEventsUnitsDF()

        STRUCT unitConversions units
        StructFill/SDFR = unitsDFR/AC=1 units
        fillUnitConversions(units)
        units.convertUnits = 1 // use human-interpretable units
        
        CheckBox convertUnitsCheck, pos = {xPosPnts, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, variable=units.convertUnits, title="Convert units", proc = updatePanelCheckProc

        createEventsUseAvgSubsetDF()
        DFREF useAvgDFR = getEventsUseAvgSubsetDF()

        variable/G useAvgDFR:useAvgSubset/N=useAvgSubset // don't use average subset
        if(numtype(useAvgSubset)!=0)
            useAvgSubset = 0
        endif
        
        variable/G useAvgDFR:limitEventsPerCell/N=limitEventsPerCell
        if(numtype(limitEventsPerCell)!=0)
            limitEventsPerCell = 0
        endif
        
        variable/G useAvgDFR:maxEventsPerCell/N=maxEventsPerCell
        if(numtype(maxEventsPerCell)!=0)
            maxEventsPerCell = 0
        endif

        variable/G useAvgDFR:intMaxEventsPerCell/N=intMaxEventsPerCell
        if(numtype(intMaxEventsPerCell)!=0)
            intMaxEventsPerCell = 0
        endif
        
        xPos = xPosPnts + buttonWidth

        variable/G useAvgDFR:gSubsetSelectedRadioButton/N=radioButton
        radioButton = 1

        yPos = posRelPanel(0.95, "height")
        CheckBox allDispRadio, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, value = 1, mode = 1, title = "All events", proc=subsetSelectedDisplayProc
        xPos += buttonWidth/2
        CheckBox avgDispRadio, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, value = 0, mode = 1, title = "Avg subset", proc=subsetSelectedDisplayProc
        xPos += buttonWidth/2
        CheckBox limitedDispRadio, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, value = 0, mode = 1, title = "Limited avg subset", proc=subsetSelectedDisplayProc
        xPos += buttonWidth/1.5
        CheckBox intervalDispRadio, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, value = 0, mode = 1, title = "Interval subset", proc=subsetSelectedDisplayProc
        xPos += buttonWidth/1.75
        CheckBox specifyDispRadio, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, value = 0, mode = 1, title = "Specify by type", proc=subsetSelectedDisplayProc


        // CheckBox useAvgSubCheck, pos = {xPos, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, variable=useAvgSubset, title="Use Avg Subset", proc = updatePanelCheckProc
        // xPos += buttonWidth/1.5
        // CheckBox limitEventsPerCellCheck, pos = {xPos, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, variable=limitEventsPerCell, title="Limit events/cell"//, proc = updatePanelCheckProc
        xPos += buttonWidth/1.5
        SetVariable maxEventsPerCellVar, pos = {xPos, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, value=maxEventsPerCell, title="Avg max events/cell" // TO-DO: ADD PROC
        xPos += buttonWidth
        SetVariable intMaxEventsVar, pos = {xPos, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, value=intMaxEventsPerCell, title="Int max events/cell" // TO-DO: ADD PROC

        createCellInfoTab(panelName, tabStruct.infoTab, panelSize)
        createSeriesControls(panelName, panelDFR, panelSize)
        createPassiveViewTab(panelName, panelDFR, tabStruct.passiveViewTab, panelSize)
        createDetectionTab(panelName, tabStruct.eventDetectTab, panelSize)
        createConfirmationTab(panelName, tabStruct.eventConfirmTab, panelSize)
        createSeriesEventTab(panelName, panelDFR, tabStruct.seriesEventsTab, panelSize)
        createSeriesSummaryTab(panelName, panelDFR, tabStruct.seriesSumTab, panelSize)
        createSeriesOutTab(panelName, panelDFR, tabStruct.seriesOutTab, panelSize)
        createSeriesGraphTab(panelName, panelDFR, tabStruct.seriesGraphTab, panelSize)
        createCellConcatTab(panelName, panelDFR, tabStruct.cellConcatTab, panelSize) 
        createCellSummaryTab(panelName, panelDFR, tabStruct.cellSumTab, panelSize)
        createCellOutTab(panelName, panelDFR, tabStruct.cellOutTab, panelSize)
        createCellsGraphTab(panelName, panelDFR, tabStruct.cellsGraphTab, panelSize)
        createRawDataTab(panelName, panelDFR, tabStruct.rawDataTab, panelSize)
        createGroupEventTab(panelName, panelDFR, tabStruct.groupEventTab, panelSize)
        // createOptoTab(panelName, panelDFR, tabStruct.optoTab, panelSize)

        updateCellNameListBox()
        updatePanelForSelectedSeries(panelName = panelName)
    
        enableControlsByTabData("", 0)
        enableDisplaysByTabData("", 0, panelName)

        SetActiveSubWindow $panelName
        setDataFolder root:
    endif
end

