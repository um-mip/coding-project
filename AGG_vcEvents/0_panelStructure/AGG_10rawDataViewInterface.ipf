function createRawDataTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""

    string tabNumStr = num2str(tabNum)
    
    // created by the createSeriesControls box
    tabControls += "cellNamesText_series;"
    tabControls += "cellListBox_series;"
    tabControls += "seriesNamesText_series;"
    tabControls += "seriesListBox_series;"
    tabControls += "convertUnitsCheck;"

    Display/W=(0.2, 0.07, 0.8, 0.93)/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, rawData
    tabDisplays += "rawData;"

    variable xPos = posRelPanel(0.82, "width", panelName = panelName)
    variable yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight
    variable buttonWidth = panelSize.buttonWidth

    createEventsRawDataDF()
    DFREF rawDFR = getEventsRawDataDF()

    variable/G rawDFR:startTime/N=startTime=0, rawDFR:duration/N=duration=120, rawDFR:yMin/N=yMin=-100, rawDFR:yRange/N=yRange=100, rawDFR:showGaps/N=showGaps=1

    CheckBox scaleCheck title = "Scale axes", pos = {xPos, yPos}
    yPos += buttonHeight * 1.2
    tabControls += "scaleCheck;"

    CheckBox smoothWave title = "Box 9 smooth", pos = {xPos, yPos}
    yPos += buttonHeight * 1.2
    tabControls += "smoothWave;"

    SetVariable startTimePick value = startTime, title = "start time (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight * 1.2
    tabControls += "startTimePick;"

    SetVariable durationPick value = duration, title = "duration (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight * 1.2
    tabControls += "durationPick;"
    
    SetVariable yMinPick value = yMin, title = "min current (pA)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight * 1.2
    tabControls += "yMinPick;"
    
    SetVariable yRangePick value = yRange, title = "current range (pA)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight * 1.2
    tabControls += "yRangePick;"
    
    CheckBox showGapsPick variable = showGaps, title = "highlight gaps if present", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight * 1.2
    tabControls += "showGapsPick;"

    Button updateRawGraph title = "UPDATE GRAPH", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, proc = updateRawGraphProc
    yPos += buttonHeight * 1.2
    tabControls += "updateRawGraph;"

    Button saveRawGraph, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, userdata="rawData", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveRawGraph;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end