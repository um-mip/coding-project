function createCellOutTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""

    createAllCellsOutputDF()
    DFREF cellsOutDFR = getAllCellsOutputDF()

    string tabControls = ""
    variable xPos, yPos
    variable buttonWidth = panelSize.buttonWidth
    variable buttonHeight = panelSize.buttonHeight

    // Make region listbox
    xPos = posRelPanel(0.84, "width", panelName = panelName)
    yPos = panelSize.yPosPnts

    TitleBox regionSelect_text_all pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select region:", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelect_text_all;"
    yPos += buttonHeight
    Wave/T allRegionNames = storeAllRegionsNamesWave()
    Wave allRegionNames_sel = storeAllRegionsSelectWave(allRegionNames)
    
    ListBox regionListBox_all, mode=1, listwave = allRegionNames, selwave=allRegionNames_sel, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = selectedRegionListProc_all
    tabControls += "regionListBox_all;"
    yPos += panelSize.listBoxHeight

    SetDataFolder cellsOutDFR
    Struct outputWaves outputWaves
    StructFill/SDFR = cellsOutDFR/AC=3 outputWaves
    
    edit /N=cellOutputTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.02, 0.1, 0.82, 0.93) cellName, groupName, groupName2, groupName3, sumTime, duration, sumFreq, sumCount, pks, int, der, t50r, fwhm, decay9010, Rinput, RseriesSub, capa, holdingc, decayTimes, tauFits, y0Fits, AFits, x0Fits
    ModifyTable autosize={0, 0, -1, 0, 10}
    tabDisplays += "cellOutputTable;"

    ModifyTable width(cellName) = 90, width(sumTime) = 50, format(sumTime)=7 // changed to only time  // Not sure why date/time isn't working with SetScale above, trying this
    ModifyTable showParts=126


    [xPos, yPos] = xyRelPanel(0.02, 0.06, panelName = panelName)
    Button updateCellsOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button updateCellsOutButton, title = "Update table", proc = updateCellsOutProc
    tabControls += "updateCellsOutButton;"
    xPos += panelSize.buttonWidth * 1.5

    Button updateCellsOutButton_concat, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button updateCellsOutButton_concat, title = "Re-concat and update", proc = updateCellsOutProc, userdata="concat:1;"
    tabControls += "updateCellsOutButton_concat;"
    xPos += panelSize.buttonWidth * 1.5
    
    Button saveCellsOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveCellsOutButton, title = "Save table", proc = saveTableProc, userdata="tableName:cellOutputTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveCellsOutButton;"
    tabControls += "convertUnitsCheck;"
    // tabControls += "useAvgSubCheck;"
    // tabControls += "limitEventsPerCellCheck;"
    tabControls += "maxEventsPerCellVar;"
    tabControls += "intMaxEventsVar;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
    

    SetDataFolder panelDFR
end