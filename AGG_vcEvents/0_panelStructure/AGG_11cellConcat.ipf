function createCellConcatTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    tabControls += "maxEventsPerCellVar;"
    tabControls += "intMaxEventsVar;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    string tabNumStr = num2str(tabNum)

    createEventsCellSumOutDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable buttonWidth = panelSize.buttonWidth
    variable buttonHeight = panelSize.buttonHeight
    variable listBoxWidth = panelSize.listBoxWidth
    variable listBoxHeight = panelSize.listBoxHeight

    string name_wn = "cellName"
    variable yPos = panelSize.yPosPnts
    variable xPos = panelSize.xPosPnts
    TitleBox cellNamesText_cell pos = {xPos, yPos}, title = "Select a cell:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    tabControls += "cellNamesText_cell;"
    
    Make /B /O cellSumDFR:cellSelWave_concat = 0
    // Cell Select Listbox
    yPos += buttonHeight
    ListBox cellListBox_cellConcat, mode=1, listwave = $name_wn, selwave=cellSumDFR:cellSelWave_concat, pos={xPos, yPos}, size={listBoxWidth, listBoxHeight}, proc = selectedCellListProc_cellConcat
    tabControls += "cellListBox_cellConcat;"

    xPos += buttonWidth * 1.1
    yPos -= buttonHeight

    TitleBox seriesNamesText_cell pos = {xPos, yPos}, title = "Select series:", frame=0, fstyle=1, size = {buttonWidth * 0.73, buttonHeight}, anchor = LB
    // Series Select Listbox
    string selCellSeriesWN = "selCellSeries"
    if(! WaveExists(cellSumDFR:$selCellSeriesWN))
        make/T/O cellSumDFR:$selCellSeriesWN
    endif
    tabControls += "seriesNamesText_cell;"


    string selSeriesWN = "selSeries"
    if(! WaveExists(cellSumDFR:$selSeriesWN))
        make/O cellSumDFR:$selSeriesWN
    endif

    yPos += buttonHeight
    ListBox seriesListBox_cell, mode=4, listwave = cellSumDFR:$selCellSeriesWN, selwave = cellSumDFR:$selSeriesWN, pos = {xPos, yPos}, size = {listBoxWidth * 0.73, listBoxHeight*0.5}//, proc = selectedSeriesListProc_cell
    tabControls += "seriesListBox_cell;"

    xPos += listBoxWidth * 0.75
    yPos -= buttonHeight

    TitleBox concSeriesTitle pos = {xPos, yPos}, title = "Concatenated series:", frame=0, fstyle=1, size = {buttonWidth * 0.73, buttonHeight}, anchor = LB
    tabControls += "concSeriesTitle;"
    yPos += buttonHeight

    make/O/N=0/T cellSumDFR:concSeries/Wave=concSeries
    ListBox concSeriesListBox, pos = {xPos, yPos}, size = {listBoxWidth * 0.73, listBoxHeight*0.5}, listWave=concSeries
    tabControls += "concSeriesListBox;"

    xPos -= listBoxWidth * 0.75
    yPos += listBoxHeight * 0.52
    Button concSeriesButton, pos = {xPos, yPos}, size = {listBoxWidth * 1.48, buttonHeight}, proc = concatCellSeriesProc, title = "Concat Series"
    tabControls += "concSeriesButton;"
    
    // Concatenation parameters
    yPos = panelSize.yPosPnts + buttonHeight + listBoxHeight
    xPos = panelSize.xPosPnts

    variable/G cellSumDFR:binDur/N=binDur=60, cellSumDFR:secPreEvent/N=secPreEvent=0.001, cellSumDFR:secPostEvent/N=secPostEvent=0.001, cellSumDFR:baseDur/N=baseDur=0.005, cellSumDFR:baseOffset/N=baseOffset=0.002
    variable/G cellSumDFR:showHisto/N=showHisto=1, cellSumDFR:colorHisto/N=colorHisto=1, cellSumDFR:showChopped/N=showChopped=1,  cellSumDFR:showGaps/N=showGaps=1

    TitleBox concatParamsText_cell pos = {xPos, yPos}, title = "Specify concatenation params:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    tabControls += "concatParamsText_cell;"

    SetVariable secPreEventVar value = secPreEvent, title = "Keep pre event (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight
    tabControls += "secPreEventVar;"
    
    SetVariable secPostEventVar value = secPostEvent, title = "Keep post event (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight
    tabControls += "secPostEventVar;"
    
    SetVariable baseDurVar value = baseDur, title = "baseline dur (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight
    tabControls += "baseDurVar;"
    
    SetVariable baseOffsetVar value = baseOffset, title = "baseline offset (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}
    yPos += buttonHeight
    tabControls += "baseOffsetVar;"
    
    TitleBox histoInfoText_cell pos = {xPos, yPos}, title = "Specify histogram information:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    tabControls += "histoInfoText_cell;"

    CheckBox showGapsCheck pos = {xPos, yPos}, title = "show gaps", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showGaps
    yPos += buttonHeight
    tabControls += "showGapsCheck;"

    CheckBox showChoppedCheck pos = {xPos, yPos}, title = "show chopped events", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showChopped
    yPos += buttonHeight
    tabControls += "showChoppedCheck;"

    CheckBox showHistoCheck pos = {xPos, yPos}, title = "show histogram", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showHisto
    yPos += buttonHeight
    tabControls += "showHistoCheck;"
    
    CheckBox colorHistoCheck pos = {xPos, yPos}, title = "color by region", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = colorHisto
    yPos += buttonHeight
    tabControls += "colorHistoCheck;"
    
    SetVariable binDurVar value = binDur, title = "histo bin width (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc
    yPos += buttonHeight
    tabControls += "binDurVar;"

    make/N=7/D/O cellSumDFR:sliderTicks/Wave=sliderTicks
    make/N=7/T/O cellSumDFR:sliderTickLabels/Wave=sliderTickLabels
    sliderTicks = {0, 30, 60, 90, 120, 150, 180}
    sliderTickLabels = {"0", "30", "60", "90", "120", "150", "180"}
    Slider binDur_slider, pos = {xPos, yPos}, limits = {0, 180, 5}, vert=0, size = {buttonWidth, buttonHeight}, variable = binDur, ticks = 2, live = 0, userTicks = {sliderTicks, sliderTickLabels}
    tabControls += "binDur_slider;"
    yPos += buttonHeight*2
    
    xPos = xPos + buttonWidth *1.1
    yPos = panelSize.yPosPnts + buttonHeight *2.2 + listBoxHeight*0.5
    // TitleBox treatmentRegions_cell pos = {xPos, yPos}, title = "Enter treatment regions:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    // yPos += buttonHeight
    // tabControls += "treatmentRegions_cell;"

    variable/G cellSumDFR:regionTimeUnits/N=regionTimeUnits

    if(numtype(regionTimeUnits)!=0 || !regionTimeUnits)
        regionTimeUnits = 2 //if no value, default to 2 (min)
    endif

    TitleBox regionUnits_text pos = {xPos, yPos}, size={listBoxWidth * 1.48, buttonHeight}, title = "Select units of region start/end times:", frame=0, fstyle=1, anchor = LB
    yPos += buttonHeight
    tabControls += "regionUnits_text;"

    CheckBox regionsAsSec_check pos = {xPos, yPos}, proc = updateRegionUnitsProc_byCell, title = "Seconds", value = 0, mode =1
    tabControls += "regionsAsSec_check;"
    CheckBox regionsAsMin_check pos = {xPos + listBoxWidth * 0.5, yPos}, proc = updateRegionUnitsProc_byCell, title = "Minutes", value = 1, mode = 1
    tabControls += "regionsAsMin_check;"
    CheckBox regionsAsTime_check pos = {xPos + listBoxWidth, yPos}, proc = updateRegionUnitsProc_byCell, title = "Time (no edit)", value = 0, mode = 1
    tabControls += "regionsAsTime_check;"
    
    yPos += buttonHeight

    variable/G cellSumDFR:numRegions/N=numRegions=0
    variable/G cellSumDFR:cellDur/N=cellDur
    string/G cellSumDFR:cellStartTime/N=cellStartTime

    SetVariable cellStartTimeSet pos = {xPos, yPos}, value = cellStartTime, size = {listBoxWidth * 1.48, buttonHeight}, title = "Start time:"
    tabControls += "cellStartTimeSet;"
    tabControlsNoEdit += "cellStartTimeSet;"
    yPos += buttonHeight

    SetVariable cellDurSet pos = {xPos, yPos}, value = cellDur, size = {listBoxWidth * 0.73, buttonHeight}, title = "Total dur:", limits = {0, inf, 1}
    tabControls += "cellDurSet;"
    tabControlsNoEdit += "cellDurSet;"
    xPos += listBoxWidth *0.75
    
    SetVariable numRegionsSet pos = {xPos, yPos}, value = numRegions, size = {listBoxWidth * 0.73, buttonHeight}, title = "# regions", proc = updateRegWavesProc, limits = {0, inf, 1}
    tabControls += "numRegionsSet;"
    xPos -= listBoxWidth *0.75
    yPos += buttonHeight

    // AGG 2024-06-04, added window hook to watch for accepted updates to table, instead of user having to remember to press button
    // Button updateRegionsButton, pos = {xPos, yPos}, size = {listBoxWidth * 1.48, buttonHeight}, proc = updateRegionsButtonProc, title = "Update regions"
    // tabControls += "updateRegionsButton;"
    // yPos += buttonHeight
    
    [variable width, variable height] = getWindowDims(windowName = panelName)
    variable xRel = xPos / width
    variable yRel = yPos / height
    variable xEndRel = (xPos + listBoxWidth * 1.48) / width
    edit /N=cellRegionInfoTable /HIDE=(0) /K=1 /HOST=$panelName /W=(xRel, yRel, xEndRel, .95)
    tabDisplays += "cellRegionInfoTable;"

    SetWindow AGG_Events hook(regionsUpdate) = regionsUpdateHook

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder cellSumDFR
    Wave/D eventHisto, eventHistoByFreq
    if(!WaveExists(eventHisto))
        make/D/O/N=0 eventHisto
    endif

    if(!WaveExists(eventHistoByFreq))
        make/D/O/N=0 eventHistoByFreq
    endif

    edit /N=cellHistoTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.80, 0.1, 0.98, 0.95) eventHisto.x, eventHisto, eventHistoByFreq.d
    ModifyTable showParts=126
    ModifyTable title(Point)="bin#"
    ModifyTable title(eventHisto.x)="binStart"
    ModifyTable title(eventHistoByFreq.d)="freq(Hz)"
    ModifyTable title(eventHisto.d)="#events"
    // ModifyTable width(eventHisto.d)=40, width(eventHistoByFreq.d)=50, width(eventHisto.x)=55, width(Point)=25
    ModifyTable format(eventHistoByFreq.d)=3, digits(eventHistoByFreq.d)=2
    ModifyTable autosize={0, 0, 0, 0, 10}
    tabDisplays += "cellHistoTable;"

    SetDataFolder currentDF

    xPos += buttonWidth * 1.58
    xRel = xPos / width
    Display/N=cellHistoGraph/Hide=(1)/K=1/Host=$panelName/W=(xRel, 0.1, 0.79, 0.95)
    tabDisplays += "cellHistoGraph;"
    
    yPos = posRelPanel(0.1, "height", panelName = panelName)-panelSize.buttonHeight
    Button saveCellHisto, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="cellHistoGraph", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveCellHisto;"

    string selectedCell = getSelectedItem("cellListBox_cellConcat", hostName = panelName)
    updateSeriesWave_cell(selectedCell)

    updateConcSeriesLB(selectedCell)

    
    addTabUserData(tabControls, tabNum)
    addTabUserData_forNoEdit(tabControlsNoEdit, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end

