function AGG_selectAllPress_Events(B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

	if (B_Struct.eventcode == 2)
		string swavename = stringbykey("selwave", B_Struct.userdata)
		string whichDF = stringbykey("df", B_Struct.userdata)
        // print whichDF
        DFREF dfr
        strswitch (whichDF)
            case "panel":
                dfr = getEventsPanelDF()
                break
            case "sctw":
                dfr = getSCTWavesDF()
                break
            case "root":
                dfr = root:
                break
            default:
                dfr = getEventsPanelDF()
        endswitch

        WAVE /B /Z swave = dfr:$swavename
		variable n = numpnts(swave)
		variable i = 0

		for (i = 0; i < n; i += 1)
			swave[i] = 1
		endfor
	endif
end

///////////
/// updatePanelCheckProc
/// AUTHOR: Amanda
/// ORIGINAL DATE: 2022-04-25
/// NOTES: Update the panel for convert units and use average subset checks
///////////
function updatePanelCheckProc(CB_Struct) : CheckBoxControl
    STRUCT WMCheckboxAction &CB_Struct
    if(CB_Struct.eventcode == 2) // Mouse up
        updatePanelForSelectedSeries(panelName = CB_Struct.win)
    endif
    return 0
end

///////////
/// subsetSelectedDisplayProc
/// AUTHOR: Amanda
/// ORIGINAL DATE: 2024-01-17
/// NOTES: Update the radio buttons and global value for what subset to display
///////////
function subsetSelectedDisplayProc(CB_Struct) : CheckBoxControl
    STRUCT WMCheckboxAction &CB_Struct
    if(CB_Struct.eventcode == 2) // Mouse up
        handleSubsetSelectedRadioButtonClick(CB_Struct.ctrlName)

        updatePanelForSelectedSeries(panelName = CB_Struct.win)
    endif
    return 0
end

///////////
/// handleSubsetSelectedRadioButtonClick
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function handleSubsetSelectedRadioButtonClick(string controlName)
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    
    NVAR/Z/SDFR=useAvgDFR radioButton = gSubsetSelectedRadioButton

	strswitch(controlName)
		case "allDispRadio":
			radioButton = returnSubsetDispSetVal("all")
			break
		case "avgDispRadio":
			radioButton = returnSubsetDispSetVal("avg")
			break
		case "limitedDispRadio":
			radioButton = returnSubsetDispSetVal("limited")
			break
        case "intervalDispRadio":
            radioButton = returnSubsetDispSetVal("interval")
            break
        case "specifyDispRadio":
            radioButton = returnSubsetDispSetVal("specify")
            break
	endswitch

	CheckBox allDispRadio, value = radioButton==returnSubsetDispSetVal("all")
	CheckBox avgDispRadio, value = radioButton==returnSubsetDispSetVal("avg")
	CheckBox limitedDispRadio, value = radioButton==returnSubsetDispSetVal("limited")
	CheckBox intervalDispRadio, value = radioButton==returnSubsetDispSetVal("interval")
	CheckBox specifyDispRadio, value = radioButton==returnSubsetDispSetVal("specify")
end

///////////
/// returnSubsetDispSetVal
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: For the subtype returns the corresponding value
///////////
function returnSubsetDispSetVal(string subType)
    variable subVal
    strswitch (subType)
        case "all":
            subVal = 1
            break
        case "avg":
            subVal = 2
            break
        case "limited":
            subVal = 3
            break
        case "interval":
            subVal = 4
            break
        case "specify":
            subVal = 5
            break
        default:
            subVal = 1
            break
    endswitch

    return subVal
end

///////////
/// returnSubsetDispSetType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: For the value returns the corresponding subtype
///////////
function/S returnSubsetDispSetType(variable subVal)
    string subType
    switch (subVal)
        case 1:
            subType = "all"
            break
        case 2:
            subType = "avg"
            break
        case 3:
            subType = "limited"
            break
        case 4:
            subType = "interval"
            break
        case 5:
            subType = "specify"
            break
        default:
            subType = "all"

            break
    endswitch

    return subType
end

///////////
/// getSelectedDispType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function/S getSelectedDispType()
    DFREF useAvgDFR = getEventsUseAvgSubsetDF()
    NVAR/Z/SDFR=useAvgDFR radioButton = gSubsetSelectedRadioButton

    string subType = returnSubsetDispSetType(radioButton)
    return subType
end



