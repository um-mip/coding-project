function createCellInfoTab(hostName, tabNum, panelSize)
    string hostName
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string name_wn = "cellName"
    string seriesNames_wn = "Events_series"
    string group_wn = "groupName"
    string group_wn2 = "groupName2"
    string group_wn3 = "groupName3"
    string pass1_wn = "passive1"
    string pass2_wn = "passive2"

    string tabControls = ""
    string tabDisplays = ""

    // Create a cell info table
    edit /N=cellInfoTable /HIDE=(0) /K=1 /HOST=$hostName /W=(0.1, panelSize.top, 0.28, panelSize.bottom)
    tabDisplays += "cellInfoTable;"

    variable xPos = posRelPanel(0.1, "width", panelName = hostName)
    variable yPos = panelSize.yPosPnts
    variable endX = posRelPanel(0.28, "width", panelName = hostName)
    variable width = endX - xPos
    Button loadFromCollectorButton, pos = {xPos, yPos}, size = {width, panelSize.buttonHeight}, title = "Load cells from Collector", proc = loadFromCollectorProc
    tabControls += "loadFromCollectorButton;"

    // Make waves for cell info table
    string infoTableTextWaves = "cellName;groupName;groupName2;groupName3"

    variable iWaveName = 0
    variable numTextWaves = itemsInList(infoTableTextWaves)
    string nameWave = ""

    for(iWaveName = 0; iWaveName < numTextWaves; iWaveName++)
        nameWave = StringFromList( iWaveName, infoTableTextWaves )
        if(! WaveExists( $nameWave ))
            make/T/O $nameWave
        endif
        AppendToTable /W = $hostName#cellInfoTable $nameWave
    endfor

    ModifyTable autosize={0, 0, -1, 0, 10}

    xPos = posRelPanel(0.3, "width", panelName = hostName)
    variable buttonHeight = panelSize.buttonHeight
    TitleBox cellNamesText pos = {xPos, yPos}, title = "Cells:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    tabControls += "cellNamesText;"
    yPos += buttonHeight
    Make /B /O cellSelWave = 0
    
    ListBox cellListBox, mode=1, listwave=$name_wn, selwave=cellSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth,panelSize.listBoxHeight*1.8}, proc = getSeriesInfo_LBproc
    tabControls += "cellListBox;"

    DFREF panelDFR = getEventsPanelDF()
    string/G panelDFR:seriesLabelList/N=seriesLabelList
    if(strlen(seriesLabelList) == 0)
        seriesLabelList = "EC-10kHz;EC-20kHz;VC-10kHz;"
    endif

    xPos += panelSize.listBoxWidth * 1.1

    TitleBox labelTitleText, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight*2}, title = "Enter events series labels:\nseparate each label with ;", anchor = LT, frame=0, fstyle=1
    tabControls += "labelTitleText;"
    yPos += buttonHeight *2 

    SetVariable setSeriesLabelList, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight *2}, title = "labels:", live = 1, value = seriesLabelList
    tabControls += "setSeriesLabelList;"
    yPos += buttonHeight

    TitleBox fillSeriesButtonText, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight*4}, title = "For cells with a blank\nEvents_series wave,\nbutton below will fill wave\nwith series #s whose PGF label\nmatches any of above list", anchor = LB, frame=0, fstyle=1
    tabControls += "fillSeriesButtonText;"
    yPos += buttonHeight *4

    Button updateExpSeries, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Fill all empty Events_series", proc=updateExpSeriesProc
    tabControls += "updateExpSeries;"
    yPos -= buttonHeight *7

    xPos -= panelSize.buttonWidth * 1.1
    yPos = panelSize.yPosPnts + panelSize.listBoxHeight*1.85
    
    Button updateCells, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Update Cell Names", proc=removeBlankCellsProc
    tabControls += "updateCells;"
    yPos += buttonHeight

    CheckBox fillExpCardCheck, pos = {xPos, yPos}, size = {panelSize.buttonWidth, buttonHeight}, title = "Fill experiment card", value = 0
    tabControls += "fillExpCardCheck;"
    yPos += buttonHeight
    
    CheckBox showExpCardCheck, pos = {xPos, yPos}, size = {panelSize.buttonWidth, buttonHeight}, title = "Bring card to front", value = 0
    tabControls += "showExpCardCheck;"
    
    yPos += buttonHeight*1.2

    variable/G panelDFR:isExtracellular/N=isExtracellular
    if(numtype(isExtracellular)!=0)
        isExtracellular = 0
    endif

    CheckBox isExtracellularCheck, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, variable=isExtracellular, title="Extracellular Recording", proc=isExtracellularProc
    tabControls += "isExtracellularCheck;"

    variable/G panelDFR:defTraceNum/N=defTraceNum
    if(numtype(defTraceNum)!=0)
        defTraceNum = 1
    endif
    if(! defTraceNum >= 1 || ! isInteger(defTraceNum) )
        print "defTraceNum should be an integer greater than or equal to 1"
        defTraceNum = 1
    endif

    // xPos = posRelPanel(0.62, "width", panelName = hostName)
    xPos += panelSize.buttonWidth * 2.2
    yPos = posRelPanel(0.1, "height", panelName = hostName)
    yPos -= panelSize.buttonHeight
    SetVariable setDefTraceNum, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Set trace number", limits = {1, inf, 1}, live = 1, value = defTraceNum, proc = checkDefTraceNumProc
    tabControls += "setDefTraceNum;"

    variable xPosRel = xPos / posRelPanel(1, "width", panelName = hostName)

    edit /N=cellSeriesInfoTable /HIDE=(0) /K=1 /HOST=$hostName /W=(xPosRel, .1, .95, .97)
    tabDisplays += "cellSeriesInfoTable;"

    fillCellInfoTable(panelName = hostName)

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, hostName)
    // Series info table is re-created each time a cell is selected in the cellListBox_tabx
end