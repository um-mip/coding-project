function createDetectionTab(panelName, tabNum, panelSize)
    string panelName
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabNumStr = num2str(tabNum)
    string tabControls = "", alwaysHidetabControls = "", tabDisplays = ""

    createevDetectDF()
    DFREF evDetectDFR = getevDetectDF()
    DFREF panelDFR = getEventsPanelDF()

    variable yPos = panelSize.yPosPnts

    string name_wn = "cellName"
    TitleBox cellNamesTextDetect pos = {panelSize.xPosPnts, yPos}, title = "Select a cell:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB//, fixedSize = 1, anchor = LB
    tabControls += "cellNamesTextDetect;"
    Make /B /O evDetectDFR:cellSelWave = 0
    // Cell Select Listbox

    yPos += panelSize.buttonHeight
    ListBox cellListBoxDetect, mode=1, listwave = panelDFR:$name_wn, selwave=evDetectDFR:cellSelWave, pos={panelSize.xPosPnts, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight}, proc = updateDetectSeriesListProc
    tabControls += "cellListBoxDetect;"

    yPos += panelSize.listBoxHeight
    TitleBox seriesNamesTextDetect pos = {panelSize.xPosPnts, yPos}, title = "Select a series:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    tabControls += "seriesNamesTextDetect;"
    // Series Select Listbox
    string selCellSeriesWN = "selCellSeries"
    if(! WaveExists(evDetectDFR:$selCellSeriesWN))
        make/T/O evDetectDFR:$selCellSeriesWN
    else
        ControlInfo cellListBoxDetect
        variable selectedCellNum = V_Value
        WAVE/T cellNames = panelDFR:cellName
        if(selectedCellNum < numpnts(cellNames))
            string selectedCell = cellNames[selectedCellNum]
            // print selectedCell
            updateSeriesWave_detect(selectedCell)
        endif
    endif

    string selSeriesWN = "selSeries"
    if(! WaveExists(evDetectDFR:$selSeriesWN))
        make/O evDetectDFR:$selSeriesWN
    endif

    yPos += panelSize.buttonHeight
    ListBox seriesListBoxDetect, mode=4, listwave = evDetectDFR:$selCellSeriesWN, selwave = evDetectDFR:$selSeriesWN, pos = {panelSize.xPosPnts, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight}//, proc = updateDetectAnalysisListProc
    tabControls += "seriesListBoxDetect;"

    yPos += panelSize.listBoxHeight
    Button addSeriesToDetectButton, pos = {panelSize.xPosPnts, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = addCellSeriesToDetectProc, title = "Add to Detect Waves"
    tabControls += "addSeriesToDetectButton;"

    // check if blast panel exists
    // prompt to close blast panel

    DFREF currentDF = GetDataFolderDFR()

    SetDataFolder root:
    // SetDataFolder evDetectDFR // If do this and set explicitly before all of the analysis procs, it could work, but probably easier to leave it as root, since that's where things will search

    string thisSubPanel = "DetectPanel0"
    string/g detectpanel=panelName

    SVAR/Z bpname = blastpanel
    if( SVAR_Exists( bpname ) )
        bpname = detectpanel
    else
        string/g bpname = detectpanel 
        string/g blastpanel = detectpanel
    endif

    variable MAXABFFILES=256, MAXSWEEPS=999, MAXIMPORTWAVES=999

    // Check first if global variables have been created before re-assigning a default value
    // could do this programmatically with strings, but because use these named variables later
    // it wouldn't be much more efficient
        NVAR/Z gpredsmooth, gdsmooth, gdthresh, gdmin_dur, gdmax_dur, gdpwin, gdetectsign
        NVAR/Z gboffset, gbdur, gpthresh, gpsmooth, gathresh, gawin, gtdur, gtoffset, gavecut
        NVAR/Z gxmin1, gxmax1, gymin1, gymax1
        NVAR/Z gxmin2, gxmax2, gymin2, gymax2
        NVAR/Z g_autopass

        if(!NVar_Exists(gpredsmooth) || numtype(gpredsmooth) != 0)
            variable/g gpredsmooth = 11
        endif

        if(!NVar_Exists(gdsmooth) || numtype(gdsmooth) != 0)
            variable/g gdsmooth = 1
        endif
        
        if(!NVar_Exists(gdthresh) || numtype(gdthresh) != 0)
            variable/g gdthresh = 10
        endif
        
        if(!NVar_Exists(gdmin_dur) || numtype(gdmin_dur) != 0)
            variable/g gdmin_dur = 0.5
        endif
        
        if(!NVar_Exists(gdmax_dur) || numtype(gdmax_dur) != 0)
            variable/g gdmax_dur = 5
        endif
        
        if(!NVar_Exists(gdpwin) || numtype(gdpwin) != 0)
            variable/g gdpwin = 2
        endif
        
        if(!NVar_Exists(gdetectsign) || numtype(gdetectsign) != 0)
            variable/g gdetectsign = -1
        endif
        
        if(!NVar_Exists(gboffset) || numtype(gboffset) != 0)
            variable/g gboffset = 2
        endif
        
        if(!NVar_Exists(gbdur) || numtype(gbdur) != 0)
            variable/g gbdur = 5
        endif
        
        if(!NVar_Exists(gpthresh) || numtype(gpthresh) != 0)
            variable/g gpthresh = 5
        endif
        
        if(!NVar_Exists(gpsmooth) || numtype(gpsmooth) != 0)
            variable/g gpsmooth = 3
        endif
        
        if(!NVar_Exists(gathresh) || numtype(gathresh) != 0)
            variable/g gathresh = 1
        endif
        
        if(!NVar_Exists(gawin) || numtype(gawin) != 0)
            variable/g gawin = 10
        endif
        
        if(!NVar_Exists(gtdur) || numtype(gtdur) != 0)
            variable isExtracellular = checkIsExtracellular()
            if(isExtracellular)
                variable/g gtdur = 10
            else
                variable/g gtdur = 200
            endif
        endif

        if(!NVar_Exists(gtoffset) || numtype(gtoffset) != 0)
            variable/g gtoffset = 10
        endif
        
        if(!NVar_Exists(gavecut) || numtype(gavecut) != 0)
            variable/g gavecut = 1000
        endif
        
        if(!NVar_Exists(gxmin1) || numtype(gxmin1) != 0)
            variable/g gxmin1 = 0
        endif
        
        if(!NVar_Exists(gxmax1) || numtype(gxmax1) != 0)
            variable/g gxmax1 = 120
        endif
        
        if(!NVar_Exists(gymin1) || numtype(gymin1) != 0)
            variable/g gymin1 = 0
        endif
        
        if(!NVar_Exists(gymax1) || numtype(gymax1) != 0)
            variable/g gymax1 = 1
        endif

        if(!NVar_Exists(gxmin2) || numtype(gxmin2) != 0)
            variable/g gxmin2 = 0
        endif
        
        if(!NVar_Exists(gxmax2) || numtype(gxmax2) != 0)
            variable/g gxmax2 = 120
        endif
        
        if(!NVar_Exists(gymin2) || numtype(gymin2) != 0)
            variable/g gymin2 = 0
        endif
        
        if(!NVar_Exists(gymax2) || numtype(gymax2) != 0)
            variable/g gymax2 = 1
        endif

        if(!NVar_Exists(g_autopass) || numtype(g_autopass) != 0)
            variable/g g_autopass = 0
        endif

    // Set columns and sizes
        variable p_xpos = panelSize.left, p_ypos = 60, p_dx = 710, p_dy = 650
        
        variable col0start=panelSize.xPosPnts + posRelPanel(0.35, "width", panelName = panelName)
        variable col1start=panelSize.xPosPnts + posRelPanel(0.55, "width", panelName = panelName) 
        variable col2start =panelSize.xPosPnts + posRelPanel(0.75, "width", panelName = panelName)
        variable col3start=panelSize.xPosPnts + posRelPanel(0.85, "width", panelName = panelName)

        variable paramWidth = panelSize.buttonWidth, paramWidthRel = panelSize.buttonWidthRel
        variable reduceParamHeight = posRelPanel(panelSize.buttonHeightRel/5, "height", panelName = panelName)
        
        variable paramHeight = panelSize.buttonHeight - reduceParamHeight
        variable paramCol0start=panelSize.xPosPnts + posRelPanel(0.34, "width", panelName = panelName)
        variable paramCol0StartRel = panelSize.xPos + 0.34
        variable colBufferRel = paramWidthRel/25, colBuffer = posRelPanel(colBufferRel, "width", panelName = panelName)
        
        variable paramCol1start= paramCol0start + (paramWidth + colBuffer) * 1
        variable paramCol1startRel= paramCol0startRel + (paramWidthRel + colBufferRel) * 1
        
        variable paramCol2start= paramCol0start + (paramWidth + colBuffer) * 2
        variable paramCol2startRel= paramCol0startRel + (paramWidthRel + colBufferRel) * 2

        variable paramCol3start= paramCol0start + (paramWidth + colBuffer) * 3
        variable paramCol3startRel= paramCol0startRel + (paramWidthRel + colBufferRel) * 3

        variable rowstart=panelSize.yPosPnts, rowstartRel = panelSize.yPos

        variable rowinc=panelSize.buttonHeight, rowIncRel = panelSize.buttonHeightRel, bw1=panelSize.buttonWidth,bw2=panelSize.buttonWidth
        variable verytop = panelSize.yPosPnts

    // Make listboxes
        string detectWN = "ImportListWave"
        make/T/O/N=(MAXIMPORTWAVES,1) ImportListWave
        string detectSelWN = "ImportSelWave"
        make/O/B/U/N=(MAXIMPORTWAVES,1,2) ImportSelWave

        importselwave[][][1]=0
        importlistwave= ""

        variable colBoxStart = panelSize.xPosPnts + posRelPanel(0.17, "width", panelName = panelName)

        yPos = verytop
        TitleBox importTitle title="Detect Waves", pos={colBoxStart,yPos}, frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
        yPos += panelSize.buttonHeight
        tabControls += "importTitle;"

        ListBox importfileList, proc=importfilelistboxproc_AGG, mode=4, pos={colBoxStart,yPos},size={panelSize.listBoxWidth, panelSize.listBoxHeight * 1.5}, listwave=ImportListwave, selwave=importselwave
        ListBox importfilelist, userdata(listwn)="ImportListWave", userdata(selwn)="ImportSelWave"
        tabControls += "importFileList;"

        yPos += panelSize.listBoxHeight *1.5

        Button selAllDetectWaves, proc = AGG_selectAllPress_Events, pos = {colBoxStart, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Select All", userdata = "df:root;selwave:ImportSelWave"
        tabControls += "selAllDetectWaves;"

        yPos += panelSize.buttonHeight
        Button removeDetectWaves, proc = AGG_RemoveSelWavesProc, pos = {colBoxStart, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Remove Selected", userdata = "selwave:ImportSelWave;listwave:ImportListWave;dfSelWave:root;dfDelWave:root;clearAnal:1;"
        tabControls += "removeDetectWaves;"

        Button GUI,pos={col0start, verytop},size={bw1,rowinc},proc=InitializeGUIProc,title="GUI"
        alwaysHidetabControls += "GUI;"

    // Make set variable controls and buttons
        variable buttonrow=posRelPanel(0.8, "height", panelName = panelName)
        variable  clearButtonX=col3start, clearButtonY= posRelPanel(0.8, "height", panelName = panelName), tcinc=panelSize.buttonHeight*0.05 //600,tcinc=32

        variable crap=5
        variable xPos // ypos made above
        xPos = paramCol0start; yPos = rowstart
        // set up detection parameter handling
        // Deriv. pre-smooth
        SetVariable deriv_Presmooth, pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Pre-smooth (points)"
        tabControls += "deriv_Presmooth;"
        SetVariable deriv_Presmooth,limits={0,1000,1},value= gpredsmooth
        yPos += rowInc

        // Deriv max. width
        // AGG 2024-05-22, this variable doesn't appear to be used in detection, so removing from interface
        SetVariable deriv_max_dur,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Max. Width (ms)"
        SetVariable deriv_max_dur,limits={0,1000,0.2},value= gdmax_dur	
        // tabControls += "deriv_max_dur;"
        alwaysHidetabControls += "deriv_max_dur;"
        
        // Deriv min width
        SetVariable deriv_min_dur,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Min. Width (ms)"
        tabControls += "deriv_min_dur;"
        SetVariable deriv_min_dur,limits={0,1000,0.2},value= gdmin_dur
        yPos += rowInc

        // Deriv. smoothing
        SetVariable deriv_smooth,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Smoothing (points) "
        tabControls += "deriv_smooth;"
        SetVariable deriv_smooth,limits={0,1000,1},value= gdsmooth
        yPos += rowInc
        
        // Deriv. threshold
        SetVariable deriv_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Deriv. Threshold (pA/ms)"
        tabControls += "deriv_thresh;"
        // deriv thresh label color
        SetVariable deriv_thresh,limits={-inf,inf,0.1},value= gdthresh, proc=testingsetvar,labelBack=(65535,0,0)
        yPos += rowInc

        xPos = paramCol1start
        yPos = rowstart

        // Window search - find the raw peak within window after deriv peak
        SetVariable dp_window,pos={xPos,yPos},size={paramWidth, paramHeight},title="Window search (ms)"
        tabControls += "dp_window;"
        SetVariable dp_window,limits={0,inf,1},value= gdpwin
        yPos += rowInc
        
        // Peak sign
        SetVariable peak_sign,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak sign"
        tabControls += "peak_sign;"
        SetVariable peak_sign,limits={-1,1,1},value= gdetectsign
        yPos += rowInc
        
        // Peak Smoothing
        SetVariable peak_smth_pts,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak Smoothing (points)"
        tabControls += "peak_smth_pts;"
        SetVariable peak_smth_pts,limits={0,1000,1},value= gpsmooth
        yPos += rowInc

        // Peak threshold
        SetVariable peak_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Peak threshold (pA)"
        tabControls += "peak_thresh;"
        // peak thresh label color	
        SetVariable peak_thresh,limits={0,inf,1},value= gpthresh, proc=testingsetvar,labelBack=(3,52428,1)
        yPos += rowInc

        xPos = paramCol2start
        yPos = rowStart

        // Area Threshold
        SetVariable peak_a_thresh,pos={xPos,yPos},size={paramWidth, paramHeight},title="Area threshold (pA*ms)"
        tabControls += "peak_a_thresh;"
        SetVariable peak_a_thresh,limits={0,inf,1},value= gathresh
        yPos += rowInc

        // Area Window
        // AGG 2024-05-22: Don't use in detection, so remove from display
        SetVariable peak_a_win,pos={xPos,yPos},size={paramWidth, paramHeight},title="Area Window (ms)"
        alwaysHidetabControls += "peak_a_win;"
        SetVariable peak_a_win limits={0,inf,1},value= gawin

        // Ave cutoff 
        SetVariable trace_ave_cutoff,pos={xPos,yPos},size={paramWidth,paramHeight},title="Ave cutoff (pA)"
        tabControls += "trace_ave_cutoff;"
        SetVariable trace_ave_cutoff,limits={0,inf,1},value= gavecut
        yPos += rowInc

        // Baseline offset
        SetVariable base_offset,pos={xPos,yPos},size={paramWidth,paramHeight},title="Baseline offset (ms)"
        tabControls += "base_offset;"
        // baseline offset label color	
        SetVariable base_offset,limits={0,inf,1},value= gboffset,labelback=(16385,35088,50000, 30000)
        yPos += rowInc
        
        // Baseline Duration
        SetVariable base_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Baseline duration (ms)"
        tabControls += "base_dur;"
        SetVariable base_dur,limits={0,inf,1},value= gbdur, proc = updateDetectDisplayProc
        yPos += rowInc

        xPos = paramCol3start
        yPos = rowStart

        // Trace duration
        SetVariable trace_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Trace duration (ms)"
        tabControls += "trace_dur;"
        // trace duration label color
        SetVariable trace_dur,limits={0,inf,10},value= gtdur,labelback=(65535,32768,58981)
        yPos += rowInc

        // Trace offset
        SetVariable trace_base_dur,pos={xPos,yPos},size={paramWidth,paramHeight},title="Trace offset (ms)"
        tabControls += "trace_base_dur;"
        SetVariable trace_base_dur,limits={0,inf,10},value= gtoffset
        yPos += rowInc
        
        Button detect,pos={paramCol0start,rowstart+rowinc*4},size={bw2,panelSize.buttonHeight},proc=DetectButtonProc_AGG,title="detect"
        Button detect, userdata="df:root;detect:"+detectWN
        tabControls += "detect;"

        variable rangeRowY = rowstart + rowinc * 4
        Button alignPlots, pos={paramCol1start, rangeRowY},size={paramWidth,panelSize.buttonHeight}, proc=AlignPlotsProc,title="align by raw x-axis"
        tabControls += "alignPlots;"
        
    // Make analysis displays
        variable displayTop = rowstartRel + rowIncRel * 5
        Display/Host = $panelName/W=(paramCol0startRel, displayTop, paramCol2startRel - colBufferRel, .98) /N=AnalysisGraph1
        tabDisplays += "AnalysisGraph1;"
        
        Display/Host = $panelName/W=(paramCol2startRel, displayTop, paramCol3startRel + paramWidthRel, .98) /N=AnalysisGraph2
        tabDisplays += "AnalysisGraph2;"

        variable buttonStart=col1start,buttondx=panelSize.buttonHeight,setdx=panelSize.buttonHeight,sizex=panelSize.buttonHeight,sizey=panelSize.buttonHeight

    // make range entry
        variable rangestart=paramCol2start,rangedx= paramWidth / 2
        SetVariable scale1xmin, pos={rangestart, rangeRowY},size={rangedx,buttondx},title="xmin",limits={0,10000,1},value= gxmin1, proc = updateDetectDisplayProc
        tabControls += "scale1xmin;"
        SetVariable scale1xmax, pos={rangestart+rangedx, rangeRowY},size={rangedx,buttondx},title="xmax",limits={0,10000,1},value= gxmax1, proc = updateDetectDisplayProc
        tabControls += "scale1xmax;"
        button getrange, pos={paramCol3start, rangeRowY}, size={rangedx,buttondx}, proc=getrangeproc_AGG,title="Disp'd range"
        tabControls += "getrange;"
        button setRange, pos={paramCol3start+rangedx, rangeRowY}, size={rangedx,buttondx}, proc=setRangeproc_AGG,title="Full range"
        tabControls += "setRange;"

    addTabUserData(tabControls, tabNum)
    addTabUserData(alwaysHidetabControls, NaN)
    addDisplayUserData(tabDisplays, tabNum, panelName)

    SetDataFolder currentDF
end