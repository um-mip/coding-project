function createSeriesOutTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""

    createSeriesOutputDF()
    DFREF seriesOutDFR = getSeriesOutputDF()

    SetDataFolder seriesOutDFR
    // Make/O/N=0 sumCount, sumFreq, pks, int, der, t50r, fwhm, Rinput, RseriesSub, capa, holdingc
    // Make/O/D/N=0 sumTime
    // Make/O/T/N=0 seriesName, groupName
    Struct outputWaves outputWaves
    StructFill/SDFR = seriesOutDFR/AC=3 outputWaves
    
    edit /N=seriesOutputTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.07, 0.1, 0.95, 0.93) seriesName, groupName, confDetect, sumCount, sumTime, duration, sumFreq, pks, int, der, t50r, fwhm, decay9010, Rinput, RseriesSub, capa, holdingc, groupName2, groupName3
    ModifyTable autosize={0, 0, -1, 0, 10}
    tabDisplays += "seriesOutputTable;"

    ModifyTable showParts=126
    ModifyTable width(seriesName) = 130, width(sumTime) = 50, format(sumTime)=7 // changed to only time  // Not sure why date/time isn't working with SetScale above, trying this

    string tabControls = ""
    variable xPos, yPos
    [xPos, yPos] = xyRelPanel(0.07, 0.06, panelName = panelName)
    Button updateSeriesOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button updateSeriesOutButton, title = "Update table", proc = updateSeriesOutProc, userdata = "recalc:0;"
    tabControls += "updateSeriesOutButton;"
    xPos += panelSize.buttonWidth * 1.5
    Button recalcUpdateSeriesOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button recalcUpdateSeriesOutButton, title = "Re-calc + update table", proc = updateSeriesOutProc, userdata="recalc:1;"
    tabControls += "recalcUpdateSeriesOutButton;"
    xPos += panelSize.buttonWidth * 1.5
    
    Button saveSeriesOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveSeriesOutButton, title = "Save table", proc = saveTableProc, userdata="tableName:seriesOutputTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveSeriesOutButton;"
    tabControls += "convertUnitsCheck;"
    // tabControls += "useAvgSubCheck;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    runMakeParams()

    // TitleBox seriesWarning, pos = {panelSize.xPosPnts + panelSize.buttonWidth*1.2, posRelPanel(0.95, "height", panelName = panelName)}, title = "Update table if changing checks; may take a while"
    // tabControls += "seriesWarning;"
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)

    // updateSeriesOutTable()

    SetDataFolder panelDFR
end