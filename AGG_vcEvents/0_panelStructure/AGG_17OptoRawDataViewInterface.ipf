
function createOptoRawDataTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""

    string tabNumStr = num2str(tabNum)
    
    // created by the createSeriesControls box
    tabControls += "cellNamesText_series;"
    tabControls += "cellListBox_series;"
    tabControls += "seriesNamesText_series;"
    tabControls += "seriesListBox_series;"
    tabControls += "convertUnitsCheck;"

    Display/W=(0.2, 0.1, 0.8, 0.93)/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, rawOptoData
    tabDisplays += "rawOptoData;"

    createOptoOutputDF()
    DFREF optoOutDFR = getOptoOutputDF()

    variable/G optoOutDFR:dataTrace/N=dataTrace
    variable/G optoOutDFR:redStimTrace/N=redStimTrace
    variable/G optoOutDFR:blueStimTrace/N=blueStimTrace

	if(numType(dataTrace)!=0 || dataTrace == 0)
		dataTrace = 1
	endif
	if(numType(redStimTrace)!=0 || redStimTrace == 0)
		redStimTrace = 4
	endif
	if(numType(blueStimTrace)!=0 || blueStimTrace == 0)
		blueStimTrace = 3
	endif

    variable xPos, yPos
    [xPos, yPos] = xyRelPanel(0.3, 0.06, panelName = panelName)
    SetVariable dataTraceSet value = dataTrace, title = "Data Trace #", limits = {0, 6, 1}, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
	xPos += panelSize.buttonWidth * 1.1
    tabControls += "dataTraceSet;"

    SetVariable redStimTraceSet value = redStimTrace, title = "Red Stim Trace #", limits = {0, 6, 1}, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
	xPos += panelSize.buttonWidth * 1.1
    tabControls += "redStimTraceSet;"

    SetVariable blueStimTraceSet value = blueStimTrace, title = "Blue Stim Trace #", limits = {0, 6, 1}, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
	xPos += panelSize.buttonWidth * 1.1
    tabControls += "blueStimTraceSet;"

    createEventsRawDataDF()
    DFREF rawDFR = getEventsRawDataDF()

    xPos = posRelPanel(0.82, "width", panelName = panelName)
    yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight
    variable buttonWidth = panelSize.buttonWidth


    yPos += buttonHeight * 1.2
    tabControls += "scaleCheck;"

    yPos += buttonHeight * 1.2
    tabControls += "smoothWave;"

    yPos += buttonHeight * 1.2
    tabControls += "startTimePick;"

    yPos += buttonHeight * 1.2
    tabControls += "durationPick;"
    
    yPos += buttonHeight * 1.2
    tabControls += "yMinPick;"
    
    yPos += buttonHeight * 1.2
    tabControls += "yRangePick;"

    Button updateRawOptoGraph title = "UPDATE GRAPH", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, proc = updateRawOptoGraphProc
    yPos += buttonHeight * 1.2
    tabControls += "updateRawOptoGraph;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end

function updateRawOptoGraphProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    string panelName = B_Struct.win
    if(B_Struct.eventCode == 2)
        string selCell
        variable selSeries, validCell, prevPassives, prevDetection
        [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = panelName)
        updateRawOptoGraph(selCell, selSeries, panelName = panelName)
    endif
end


function updateRawOptoGraph(string cellName, variable seriesNum[, string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_opto"
    endif

    DFREF optoOutDFR = getOptoOutputDF()

    variable dataTraceNum = 1 // default to trace 1 for data
    if(dataFolderRefStatus(optoOutDFR)!=0)
        NVAR/Z/SDFR=optoOutDFR dataTrace
        if(numType(dataTrace) == 0)
            dataTraceNum = dataTrace // if there's something different in the panel, use that
            // print "using panel data stim trace"
        endif
    else
        print "opto out dfr doesn't exist"
    endif

    string seriesWaveName = buildSeriesTraceWaveName(cellName, seriesNum, dataTraceNum)

    variable redTrace = 4 // default to trace 4 for red
    if(dataFolderRefStatus(optoOutDFR)!=0)
        NVAR/Z/SDFR=optoOutDFR redStimTrace
        if(numType(redStimTrace) == 0)
            redTrace = redStimTrace // if there's something different in the panel, use that
            // print "using panel red stim trace"
        endif
    else
        print "opto out dfr doesn't exist"
    endif
    
    string redWaveName = buildSeriesTraceWaveName(cellName, seriesNum, redTrace)

    variable blueTrace = 3 // default to trace 3 for blue
    if(dataFolderRefStatus(optoOutDFR)!=0)
        NVAR/Z/SDFR=optoOutDFR blueStimTrace
        if(numType(blueStimTrace) == 0)
            blueTrace = blueStimTrace // if there's something different in the panel, use that
        endif
    endif
    
    string blueWaveName = buildSeriesTraceWaveName(cellName, seriesNum, blueTrace)

    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)

    if(DataFolderRefStatus(evDetectDFR)!=0)
        DFREF waveDFR = evDetectDFR
    else
        DFREF waveDFR = root:
    endif

    Wave/SDFR=waveDFR rawWave = $seriesWaveName
    Wave/SDFR=waveDFR redWave = $redWaveName
    Wave/SDFR=waveDFR blueWave = $blueWaveName

    DFREF rawDFR = getEventsRawDataDF()

    string rawDataGraph = panelName + "#rawOptoData"
    if(WinType(rawDataGraph)==1)
        clearDisplay(rawDataGraph)
        if(WaveExists(rawWave))
            ControlInfo/W=$panelName smoothWave
            variable smoothWave = V_value
            string traceN = seriesWaveName
            if(smoothWave)
                string smoothWN = seriesWaveName+"_smth"
                traceN = smoothWN
                Duplicate/O rawWave, evDetectDFR:$smoothWN
                Wave/SDFR=evDetectDFR rawWave = $smoothWN
                Smooth/B 9, rawWave
            endif

            string axisName = "data"
            AppendToGraph/W=$rawDataGraph/L=$axisName rawWave
            ModifyGraph/W=$rawDataGraph rgb($traceN)=(0,0,0)
            ModifyGraph/W=$rawDataGraph freePos($axisName) = 0
            ModifyGraph/W=$rawDataGraph lblPosMode($axisName) = 1
            ModifyGraph/W=$rawDataGraph axisEnab($axisName) = {0.3, 1.0}

            axisName = "red"

            AppendToGraph/W=$rawDataGraph/R=$axisName redWave
            ModifyGraph/W=$rawDataGraph rgb($redWaveName)=(65535,0,0)
            ModifyGraph/W=$rawDataGraph freePos($axisName) = 0
            ModifyGraph/W=$rawDataGraph lblPosMode($axisName) = 1
            ModifyGraph/W=$rawDataGraph axisEnab($axisName) = {0.16, 0.29}


            axisName = "blue"

            AppendToGraph/W=$rawDataGraph/R=$axisName blueWave
            ModifyGraph/W=$rawDataGraph rgb($blueWaveName)=(0,0,65535)
            ModifyGraph/W=$rawDataGraph freePos($axisName) = 0
            ModifyGraph/W=$rawDataGraph lblPosMode($axisName) = 1
            ModifyGraph/W=$rawDataGraph axisEnab($axisName) = {0.0, 0.13}

            ControlInfo/W=$panelName scaleCheck
            variable useScale = V_value

            if(useScale)
                NVAR/Z/SDFR=rawDFR startTime, duration, yMin, yRange
                if(NumType(startTime)==0 && numType(duration)==0)
                    variable endTime = startTime + duration
                    SetAxis/W=$rawDataGraph bottom startTime, endTime
                endif
                if(NumType(yMin)==0 && numType(yRange)==0)
                    variable yMinScale, yRangeScale
                    yMinScale = yMin * 1e-12
                    yRangeScale = yRange * 1e-12
                    SetAxis/W=$rawDataGraph data yMinScale, yMinScale+yRangeScale
                endif
            else
                SetAxis/W=$rawDataGraph/A/E=3 bottom
                SetAxis/W=$rawDataGraph/A/E=3 data
            endif

            // scale red and blue to match
            variable maxBlue = ceil(WaveMax(blueWave))
            // variable minBlue = floor(WaveMin(blueWave))

            variable maxRed = ceil(WaveMax(redWave))
            // variable minRed = floor(WaveMin(redWave))

            // variable optoMin = min(minBlue, minRed)
            variable optoMin = 0 // always include zero
            variable optoMax = max(maxBlue, maxRed)

            // print "minBlue", minBlue, "maxBlue", maxBlue, "minRed", minRed, "maxRed", maxRed, "optoMin", optoMin, "optoMax", optoMax

            SetAxis/W=$rawDataGraph blue optoMin, optoMax
            SetAxis/W=$rawDataGraph red optoMin, optoMax

        endif
    endif
end