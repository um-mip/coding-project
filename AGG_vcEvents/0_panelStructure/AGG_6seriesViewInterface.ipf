function createSeriesEventTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""

    string tabNumStr = num2str(tabNum)
    
    // created by the createSeriesControls box
    tabControls += "cellNamesText_series;"
    tabControls += "cellListBox_series;"
    tabControls += "seriesNamesText_series;"
    tabControls += "seriesListBox_series;"
    tabControls += "convertUnitsCheck;"
    // tabControls += "useAvgSubCheck;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    // Events Table
    createEventsIndivSeriesOutDF()
    DFREF indivSeriesOutDFR = getEventsIndivSeriesOutDF()

    SetDataFolder indivSeriesOutDFR

    make/O ptb, pk2, pks, int, der, area2, t50r, '1090d', fwhm

    WAVE/T/Z/SDFR = panelDFR cellNames = cellName, seriesNums = selCellSeries
    ControlInfo /W=AGG_Events cellListBox_series
    variable selCellRow = V_value
    ControlInfo /W = AGG_Events seriesListBox_series
    variable selSeriesRow = V_value

    variable makeBlankTable = 0
    string cellName
    if(WaveExists(cellNames) && WaveExists(seriesNums))
        variable numCells = numpnts(cellNames), numSeries = numpnts(seriesNums)

        if(selCellRow < numCells && selSeriesRow < numSeries)
            cellName = cellNames[selCellRow]
            updateSeriesWave(cellName)
        endif
    endif

    edit /N=seriesEventTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.07, 0.8, 0.93) ptb, pk2, pks, int, der, area2, t50r, '1090d', fwhm
    tabDisplays += "seriesEventTable;"
    ModifyTable showParts=126

    string paramsDefs0 = "ptb: Peak time (s)"
    string paramsDefs1 = "pk2: Abs. peak (pA)"
    string paramsDefs2 = "pks: Rel. peak (pA)"
    string paramsDefs3 = "int: Interval (s)"
    string paramsDefs4 = "der: Derivative (pA/ms)"
    string paramsDefs5 = "area: Area (pA*ms)"
    string paramsDefs6 = "t50r: Rise time (ms)"
    string paramsDefs7 = "1090d: 10to90decay (ms)"
    string paramsDefs8 = "fwhm: Full width half max (ms)"

    TitleBox paramsDefSeries0 title = paramsDefs0
    TitleBox paramsDefSeries1 title = paramsDefs1
    TitleBox paramsDefSeries2 title = paramsDefs2
    TitleBox paramsDefSeries3 title = paramsDefs3
    TitleBox paramsDefSeries4 title = paramsDefs4
    TitleBox paramsDefSeries5 title = paramsDefs5
    TitleBox paramsDefSeries6 title = paramsDefs6
    TitleBox paramsDefSeries7 title = paramsDefs7
    TitleBox paramsDefSeries8 title = paramsDefs8
    
    variable numParams = 9
    variable iParam = 0
    variable xPos = posRelPanel(0.82, "width", panelName = panelName)
    variable yPos = panelSize.yPosPnts
    for(iParam = 0; iParam < numParams; iParam ++)
        TitleBox $("paramsDefSeries" + num2str(iParam)) pos = {xPos, yPos + panelSize.buttonHeight *iParam}, frame = 0, fstyle = 0
        tabControls += "paramsDefSeries" + num2str(iParam)+";"
    endfor

    ModifyTable width(point) = 35
    
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)

    SetDataFolder panelDFR
end