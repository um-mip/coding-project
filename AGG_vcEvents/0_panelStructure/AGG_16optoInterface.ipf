Structure AGG_Opto_tabs
    NVAR infoTab, passiveViewTab, eventDetectTab, eventConfirmTab
    NVAR seriesEventsTab, seriesSumTab, cellSumTab, seriesOutTab
    NVAR cellOutTab, seriesGraphTab, cellsGraphTab, rawDataTab, groupEventTab
    NVAR optoTableTab, optoRawTab, evokedTab
    SVAR tabLabels, tabVarNames
EndStructure

function fillOptoTabNums (tabStruct)
    STRUCT AGG_Opto_tabs &tabStruct

    tabStruct.infoTab = 0
    tabStruct.passiveViewTab = 1
    tabStruct.rawDataTab = 2
    tabStruct.optoRawTab = 3
    tabStruct.optoTableTab = 4
    tabStruct.evokedTab = 5

    tabStruct.tabLabels = "infoTab:Load Data;passiveViewTab:Passives;eventDetectTab:Detection;"
    tabStruct.tabLabels += "eventConfirmTab:Confirmation;seriesEventsTab:Series Events;seriesSumTab:Series Summary;"
    tabStruct.tabLabels += "cellSumTab:Cell Summary;seriesOutTab:Series Output;cellOutTab:Cell Output;seriesGraphTab:Series Group Graphs;cellsGraphTab:Cell Group Graphs;"
    tabStruct.tabLabels += "rawDataTab:Raw Data;groupEventTab:Avg Event;"
    tabStruct.tablabels += "optoTableTab:Opto Table;optoRawTab:Opto Raw;evokedTab:Evoked;"
    tabStruct.tabVarNames = "infoTab;passiveViewTab;rawDataTab;optoRawTab;optoTableTab;evokedTab;"
end

function makeAGGOptoPanel()
    DoWindow/F AGG_opto     // bring panel to the front if it exists
    if( V_Flag == 0 )       // panel doesn't exist
        createEventsDF()
        createEventsPanelDF()
        createEventsInfoDF()
        // createSCTwavesDF() // 2022-01-05 - Not relevant
        DFREF panelDFR = getEventsPanelDF()
        
        SetDataFolder panelDFR
        // print "Current path:", GetDataFolder(1)

        string panelName = "AGG_opto"
        string udata = ""

        // define the screen parameter structure
        STRUCT ScreenSizeParameters ScrP
   
        // initialize it
        InitScreenParameters(ScrP)

        // Create the panel
        AGG_makePanel(5, 0, 95, 80, panelName)
        // NewPanel /K=1/W=(50, 50, 1250, 750)/N=$panelName
        modifypanel cbRGB=(50000,50000,50000)

        variable width, height
        [width, height] = getWindowDims(type = "point")

        // print width
        variable tabFSize = width/100
        variable panelFSize = width/88
        if(panelFSize>18)
            panelFSize = 18
        endif
        // print "tab font size", tabFSize, "panel font size", panelFSize
        
        //the current publishing standard is 1 pt = 1/72"). In other words points are fixed in size independent of resolution. This is good since, all things properly configured, 12 point text on an monitor will be the same height when printed on paper.

        DefaultGUIFont/W=$panelName panel = {"Arial", panelFSize, 0}, all = {"Arial", panelFSize, 0}, TabControl = {"Arial", tabFSize, 0}
        DefaultGUIFont/W=$panelName graph = {"Arial", 16, 0}, table = {"Arial", panelFSize, 0}
        // DefaultFont

        // Use a structure for positioning to pass easily to subfunctions
        STRUCT AGG_EventsPanelSizeInfo panelSize
        
        // Variable and dimensions
        variable xPos = 0.02
        variable yPos = 0.06
        variable xRightPos = 0.8

        variable xPosPnts = posRelPanel(xPos, "width")
        variable yPosPnts = posRelPanel(yPos, "height")
        variable xRightPosPnts = posRelPanel(xRightPos, "width")

        variable buttonWidth, buttonHeight, listBoxWidth, listBoxHeight, buttonWidthRel = 0.15, buttonHeightRel = 0.04, listBoxHeightRel = 0.35
        [buttonWidth, buttonHeight] = xyRelPanel(buttonWidthRel, buttonHeightRel)
        [listBoxWidth, listBoxHeight] = xyRelPanel(buttonWidthRel, listBoxHeightRel)

        panelSize.xPos = xPos
        panelSize.yPos = yPos
        panelSize.xRightPos = xRightPos
        panelSize.xPosPnts = xPosPnts
        panelSize.yPosPnts = yPosPnts
        panelSize.xRightPosPnts = xRightPosPnts
        panelSize.buttonWidthRel = buttonWidthRel
        panelSize.buttonHeightRel = buttonHeightRel
        panelSize.buttonWidth = buttonWidth
        panelSize.buttonHeight = buttonHeight
        panelSize.listBoxWidth = listBoxWidth
        panelSize.listBoxHeight = listBoxHeight
        panelSize.listBoxHeightRel = listBoxHeightRel
        panelSize.left = 0.2
        panelSize.top = 0.1
        panelSize.right = 0.8
        panelSize.bottom = 0.95

        Struct AGG_Opto_tabs tabStruct
        StructFill/SDFR=panelDFR/AC=1 tabStruct
        fillOptoTabNums(tabStruct) // Set values

        string tabs = tabStruct.tabVarNames
        string tablabels = tabStruct.tabLabels
        TabControl EventsTabs pos={0, 0}, size={posRelPanel(1, "width"),buttonHeight}, proc=changeDisplayForTab, userdata(panelDFR)=GetDataFolder(1, panelDFR), userdata(tabs)=tabs, userdata(tabLabels)=tabLabels
        makeTabControls("EventsTabs", tabs, tabLabels, panelDFR)

        createEventsUnitsDF()
        DFREF unitsDFR = getEventsUnitsDF()

        STRUCT unitConversions units
        StructFill/SDFR = unitsDFR/AC=1 units
        fillUnitConversions(units)
        units.convertUnits = 1 // use human-interpretable units
        
        CheckBox convertUnitsCheck, pos = {xPosPnts, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, variable=units.convertUnits, title="Convert units", proc = updatePanelCheckProc

        createEventsUseAvgSubsetDF()
        DFREF useAvgDFR = getEventsUseAvgSubsetDF()

        variable/G useAvgDFR:useAvgSubset/N=useAvgSubset // don't use average subset
        if(numtype(useAvgSubset)!=0)
            useAvgSubset = 0
        endif
        
        CheckBox useAvgSubCheck, pos = {xPosPnts + buttonWidth/2, posRelPanel(0.95, "height")}, size = {buttonWidth, buttonHeight}, variable=useAvgSubset, title="Use Avg Subset", proc = updatePanelCheckProc

        createCellInfoTab(panelName, tabStruct.infoTab, panelSize)
        createSeriesControls(panelName, panelDFR, panelSize)
        createPassiveViewTab(panelName, panelDFR, tabStruct.passiveViewTab, panelSize)
        // createDetectionTab(panelName, tabStruct.eventDetectTab, panelSize)
        // createConfirmationTab(panelName, tabStruct.eventConfirmTab, panelSize)
        // createSeriesEventTab(panelName, panelDFR, tabStruct.seriesEventsTab, panelSize)
        // createSeriesSummaryTab(panelName, panelDFR, tabStruct.seriesSumTab, panelSize)
        // createSeriesOutTab(panelName, panelDFR, tabStruct.seriesOutTab, panelSize)
        // createSeriesGraphTab(panelName, panelDFR, tabStruct.seriesGraphTab, panelSize)
        // createCellSummaryTab(panelName, panelDFR, tabStruct.cellSumTab, panelSize)
        // createCellOutTab(panelName, panelDFR, tabStruct.cellOutTab, panelSize)
        // createCellsGraphTab(panelName, panelDFR, tabStruct.cellsGraphTab, panelSize)
        createRawDataTab(panelName, panelDFR, tabStruct.rawDataTab, panelSize)
        // createGroupEventTab(panelName, panelDFR, tabStruct.groupEventTab, panelSize)
        createOptoRawDataTab(panelName, panelDFR, tabStruct.optoRawTab, panelSize)
        createOptoTableTab(panelName, panelDFR, tabStruct.optoTableTab, panelSize)
        createEvokedTab(panelName, panelDFR, tabStruct.evokedTab, panelSize)

        updateCellNameListBox()
        updatePanelForSelectedSeries(panelName = panelName)
    
        enableControlsByTabData("", 0)
        enableDisplaysByTabData("", 0, panelName)

        SetActiveSubWindow $panelName
        setDataFolder root:
    endif
end

function createOptoTableTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""

    createOptoOutputDF()
    DFREF optoOutDFR = getOptoOutputDF()

    SetDataFolder optoOutDFR
    // Make/O/N=0 sumCount, sumFreq, pks, int, der, t50r, fwhm, Rinput, RseriesSub, capa, holdingc
    // Make/O/D/N=0 sumTime
    // Make/O/T/N=0 seriesName, groupName
    Struct optoOutputWaves optoOutputWaves
    StructFill/SDFR = optoOutDFR/AC=3 optoOutputWaves
    
    edit /N=optoOutputTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.07, 0.1, 0.9, 0.93) seriesName, groupName, PGFlabel, confDetect, sumCount, sumTime, duration, redFreq, redAmp, redDur, redNumStims, redNumEvoked, redNumStimsWithEvoked, blueFreq, blueAmp, blueDur, blueNumStims, blueNumEvoked, blueNumStimsWithEvoked sumFreq, pks, int, der, t50r, fwhm, Rinput, RseriesSub, capa, holdingc
    tabDisplays += "optoOutputTable;"

    ModifyTable showParts=126
    ModifyTable width(seriesName) = 130, width(sumTime) = 50, format(sumTime)=7 // changed to only time  // Not sure why date/time isn't working with SetScale above, trying this

    string tabControls = ""
    variable xPos, yPos

    // Created by the opto raw tab
    tabControls += "redStimTraceSet;"
    tabControls += "blueStimTraceSet;"

    [xPos, yPos] = xyRelPanel(0.07, 0.06, panelName = panelName)
    Button updateOptoOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button updateOptoOutButton, title = "Update table", proc = updateOptoOutProc
    tabControls += "updateOptoOutButton;"
    xPos += panelSize.buttonWidth * 1.5
    
    Button saveOptoOutButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveOptoOutButton, title = "Save table", proc = saveTableProc, userdata="tableName:optoOutputTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveOptoOutButton;"
    tabControls += "convertUnitsCheck;"
    tabControls += "useAvgSubCheck;"


    TitleBox seriesWarning, pos = {panelSize.xPosPnts + panelSize.buttonWidth*1.2, posRelPanel(0.95, "height", panelName = panelName)}, title = "Update table if changing checks; may take a while"
    tabControls += "seriesWarning;"
    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)

    // updateSeriesOutTable()

    SetDataFolder panelDFR
end


function updateOptoOutProc(B_Struct): ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        updateOptoOutTable()
    endif
end

function updateOptoOutTable()

    clearUnnamedCellRows()

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    DFREF optoOutDFR = getOptoOutputDF()
    Struct optoOutputWaves outWaves
    StructFill/SDFR=optoOutDFR outWaves

    redimension/N=0 outWaves.seriesName, outWaves.groupName, outWaves.pgfLabel, outWaves.confDetect, outWaves.duration, outWaves.sumCount, outWaves.sumTime, outWaves.redFreq, outWaves.redAmp, outWaves.redDur, outWaves.redNumStims, outWaves.redNumEvoked, outWaves.redNumStimsWithEvoked, outWaves.blueFreq, outWaves.blueAmp, outWaves.blueDur, outWaves.blueNumStims, outWaves.blueNumEvoked, outWaves.blueNumStimsWithEvoked, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc

    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        variable numAddedSeries = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell]
            string groupName = groupNames[iCell]
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
            if(dfrStatus != 0)
                Wave/T/SDFR=cellInfoDFR Events_series
                if(WaveExists(Events_series))
                    variable numSeries = numpnts(Events_series)
                    variable iSeries = 0
                    for( iSeries = 0; iSeries < numSeries; iSeries++ )
                        string seriesName = Events_series[iSeries]
                        if(strlen(seriesName)>0)
                            variable seriesNum = str2num(seriesName)
                            redimension/N=(numAddedSeries+1) outWaves.seriesName, outWaves.groupName, outWaves.pgfLabel, outWaves.confDetect, outWaves.duration, outWaves.sumCount, outWaves.sumTime, outWaves.redFreq, outWaves.redAmp, outWaves.redDur, outWaves.redNumStims, outWaves.redNumEvoked, outWaves.redNumStimsWithEvoked, outWaves.blueFreq, outWaves.blueAmp, outWaves.blueDur, outWaves.blueNumStims, outWaves.blueNumEvoked, outWaves.blueNumStimsWithEvoked, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc
                            string seriesWaveN = buildSeriesWaveName(cellName, seriesNum)
                            outWaves.seriesName[numAddedSeries] = seriesWaveN
                            outWaves.groupName[numAddedSeries] = groupName

                            DFREF seriesDFR = getSeriesDF(cellName, seriesName)
                            fillPassivesForEventSeries(cellName, seriesNum)
                            if(DataFolderRefStatus(seriesDFR) != 0)

                                // Recalculate average subset
                                calcSeriesAvgSubsetWaves(cellName, seriesNum) // update the average subset

                                // Calculate the averages for this series
                                calcSeriesEventAvgs(cellName, seriesNum)

                                // Detect Opto
                                storeOptoInfoForSeries(cellName, seriesNum)

                                // Calc evoked events
                                calcEvokedEventsForSeries(cellName, seriesNum)
                            endif


                            // Add info to table
                            updateOptoOutWaves(seriesDFR, optoOutDFR, numAddedSeries)
                            outWaves.pgfLabel[numAddedSeries] = getWaveLabel($seriesWaveN)
                            numAddedSeries++
                        endif
                    endfor
                endif
            endif
        endfor
    else
        print "updateOptoOutTable: cellName wave doesn't exist"
    endif
end

///////////
/// storeOptoInfoForSeries
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-18
/// NOTES: Get the frequency, amplitude, duration, and number of stimulations for the red and blue stimulation for a series
///////////
function storeOptoInfoForSeries(string cellName, variable seriesNum)
    
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
    DFREF optoOutDFR = getOptoOutputDF()
    if(dfrStatus != 0)
        DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
        if(DataFolderRefStatus(seriesDFR) != 0)
            // Red - assumes is trace 4
            variable redTrace = 4
            if(dataFolderRefStatus(optoOutDFR)!=0)
                NVAR/Z/SDFR=optoOutDFR redStimTrace
                if(numType(redStimTrace) == 0)
                    redTrace = redStimTrace
                endif
            endif


            variable freq = NaN, amp = NaN, pulsedur = NaN, numStims = 0
            string pgfLabel = ""
            // [redFreq, redAmp, redPulseDur, redNumStims, redPGFLabel] = getStimInfo(cellName, seriesNum, redTrace) // I don't know why this isn't working for global variables
            [freq, amp, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, redTrace)

            variable/G seriesDFR:redFreq=freq
            variable/G seriesDFR:redDur=pulseDur
            variable/G seriesDFR:redAmp=amp
            variable/G seriesDFR:redNumStims=numStims
            string/G seriesDFR:redPGFLabel=PGFLabel

            // Blue - assumes is trace 3
            variable blueTrace = 3
            if(dataFolderRefStatus(optoOutDFR)!=0)
                NVAR/Z/SDFR=optoOutDFR blueStimTrace
                if(numType(blueStimTrace) == 0)
                    blueTrace = blueStimTrace
                endif
            endif
            
            freq = NaN
            amp = NaN
            pulseDur = NaN
            numStims = NaN
            pgfLabel = ""
            // [blueFreq, blueAmp, bluePulseDur, blueNumStims, bluePGFLabel] = getStimInfo(cellName, seriesNum, blueTrace)
            [freq, amp, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, blueTrace)

            variable/G seriesDFR:blueFreq=freq
            variable/G seriesDFR:blueDur=pulseDur
            variable/G seriesDFR:blueAmp=amp
            variable/G seriesDFR:blueNumStims=numStims
            string/G seriesDFR:bluePGFLabel=PGFLabel
        endif
    endif

end

///////////
/// calcEvokedEventsForSeries
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: 
///////////
function calcEvokedEventsForSeries(string cellName, variable seriesNum)
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
    DFREF optoOutDFR = getOptoOutputDF()
    if(dfrStatus != 0)
        DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
        if(DataFolderRefStatus(seriesDFR) != 0)
            // Red - assumes is trace 4
            variable redTrace = 4
            if(dataFolderRefStatus(optoOutDFR)!=0)
                NVAR/Z/SDFR=optoOutDFR redStimTrace
                if(numType(redStimTrace) == 0)
                    redTrace = redStimTrace
                endif
            endif

            wave redStimStarts, ptbWave
            variable maxLatency = 0.02 // TODO: add into panel
            [redStimStarts, ptbWave] = getStimStartsAndPTBWave(cellName, seriesNum, redStimTrace)
            variable numEvokedEvents =NaN, numStimsWithEvoked =NaN
            if(WaveExists(redStimStarts) && waveExists(ptbWave))
                [numEvokedEvents, numStimsWithEvoked]= summarizeEvokedEvents(redStimStarts, ptbWave, maxLatency)
            endif

            variable/G seriesDFR:redNumEvoked=numEvokedEvents
            variable/G seriesDFR:redNumStimsWithEvoked=numStimsWithEvoked

            // Blue - assumes is trace 3
            variable blueTrace = 3
            if(dataFolderRefStatus(optoOutDFR)!=0)
                NVAR/Z/SDFR=optoOutDFR blueStimTrace
                if(numType(blueStimTrace) == 0)
                    blueTrace = blueStimTrace
                endif
            endif
            
            wave blueStimStarts, ptbWave
            maxLatency = 0.02 // TODO: add into panel
            [blueStimStarts, ptbWave] = getStimStartsAndPTBWave(cellName, seriesNum, blueStimTrace)
            numEvokedEvents =NaN
            numStimsWithEvoked =NaN
            if(WaveExists(blueStimStarts) && waveExists(ptbWave))
                [numEvokedEvents, numStimsWithEvoked]= summarizeEvokedEvents(blueStimStarts, ptbWave, maxLatency)
            endif

            variable/G seriesDFR:blueNumEvoked=numEvokedEvents
            variable/G seriesDFR:blueNumStimsWithEvoked=numStimsWithEvoked

        endif
    endif

end

///////////
/// getStimStartsAndPTBWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Make best guess as to waves for stim start and PTB wave for series
///////////
function [wave stimStarts, wave ptbWave] getStimStartsAndPTBWave(string cellName, variable seriesNum, variable stimTraceNum)
    string waveN = cellName + "g1s" + num2str(seriesNum) + "sw1t" + num2str(stimTraceNum)

    wave stimStarts = $(waveN + "_stimStarts")
    if(WaveExists(stimStarts))
        string ptbWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t1_ptb"
        Wave ptbWave = $ptbWaveN
        if(! WaveExists(ptbWave))
            ptbWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t2_relTime"
            Wave ptbWave = $ptbWaveN
        endif
    endif
    return[stimStarts, ptbWave]
end

function updateOptoOutWaves(DFREF seriesDFR, DFREF outDFR, variable numAddedSeries)
    Struct optoOutputWaves outputWaves
    fillOptoOutWavesStrings(outputWaves)
    string analysis_types = outputWaves.analysis_types
    // string analysis_types = "sumCount;sumTime;sumFreq;pks;int;der;t50r;fwhm;Rinput;RseriesSub;capa;holdingc"
    // string variable_names = "numEvents;seriesTime;freq;relPeak;interval;derivative;riseTime;fwhm;Rinput;RseriesSub;capa;holdingc"
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    string nameVariable = ""

    // printDFREF(outDFR)

    // print "analysisTypes", analysis_types
    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameAnalysis = StringFromList( iAnalysis, analysis_types )
        // nameVariable = StringFromList( iAnalysis, variable_names )
        nameVariable = stringbykey(nameAnalysis, outputWaves.varFromWave)


        Wave/SDFR = outDFR thisAnalysis = $nameAnalysis
        // thisAnalysis[numAddedSeries] = NaN
        
        variable thisAvg = NaN
        if(DataFolderRefStatus(seriesDFR) != 0)
            NVAR/Z/SDFR = seriesDFR avgVar = $nameVariable
            if(numType(avgVar) == 0)
                thisAvg = avgVar
                // print "thisAvg is", thisAvg
            else
            endif
        else
        endif
        thisAnalysis[numAddedSeries] = thisAvg
    endfor
end

function createEvokedTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""

    string tabNumStr = num2str(tabNum)
    
    // created by the createSeriesControls box
    tabControls += "cellNamesText_series;"
    tabControls += "cellListBox_series;"
    tabControls += "seriesNamesText_series;"
    tabControls += "seriesListBox_series;"
    tabControls += "convertUnitsCheck;"

    variable buttonHeight = panelSize.buttonHeight
    variable buttonWidth = panelSize.buttonWidth

    variable xPos, yPos
    xPos = panelSize.xPosPnts
    yPos = posRelPanel(0.85, "height", panelName = panelName)

    Button showEvokedPanel title = "OPEN EVOKED PANEL", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, proc = showEvokedPanelProc
    yPos += buttonHeight * 1.2
    tabControls += "showEvokedPanel;"

    variable startX = 0.2, startY = 0.1, winWidth = 0.35, winheight = 0.4, winBuffer = 0.05
    Make/O/FREE coords = {startX, startY, startX + winWidth, startY + winheight} //Adjust graph window size/positioning here
    Make/O/FREE nextDown = {0,winheight+winbuffer,0,winheight+winbuffer}
    Make/O/FREE nextOver = {winWidth + winBuffer,0,winWidth + winBuffer,0}
    
    Display/W=(coords[0],coords[1],coords[2],coords[3])/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, selectedWave
    tabDisplays += "selectedWave;"
    coords = coords + nextDown

    Display/W=(coords[0],coords[1],coords[2],coords[3])/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, evoked
    tabDisplays += "evoked;"
    variable evokedxPos = coords[0], evokedYPos = coords[1]
    coords = coords - nextDown + nextOver

    Display/W=(coords[0],coords[1],coords[2],coords[3])/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, peristimHistGraph
    tabDisplays += "peristimHistGraph;"
    coords = coords + nextDown

    Display/W=(coords[0],coords[1],coords[2],coords[3])/L/B/Host=$panelName/Hide=(1)
    RenameWindow #, not_evoked
    variable notEvokedxPos = coords[0], notEvokedYPos = coords[1]
    tabDisplays += "not_evoked;"

    [xPos, yPos] = xyRelPanel(0.5, 0.06, panelName = panelName)
    xPos += panelSize.buttonWidth * 1.1
    tabControls += "redStimTraceSet;"

    xPos += panelSize.buttonWidth * 1.1
    tabControls += "blueStimTraceSet;"

    [xPos, yPos] = xyRelPanel(evokedxPos, evokedYPos, panelName = panelName)
    yPos -= buttonHeight
    TitleBox evokedTitle, pos = {xPos, yPos}, title = "Evoked", size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1
    tabControls += "evokedTitle;"

    [xPos, yPos] = xyRelPanel(notEvokedxPos, notEvokedYPos, panelName = panelName)
    yPos -= buttonHeight
    TitleBox notEvokedTitle, pos = {xPos, yPos}, title = "Not evoked", size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1
    tabControls += "notEvokedTitle;"


    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end

///////////
/// showEvokedPanelProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-18
/// NOTES: Open the analyzeOpto panel for the selected cell/series
///////////
function showEvokedPanelProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        string selCell
        variable selSeries, validCell, prevPassives, prevDetection
        [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = B_Struct.win)

        DFREF optoDFR = getOptoOutputDF()
        NVAR/Z/SDFR=optoDFR redStimTrace
        variable optoTrace = redStimTrace
        analyzeOpto_forSeries(selCell, selSeries, optoTrace, hostPanel = B_Struct.win)
    endif
    return 0
end
