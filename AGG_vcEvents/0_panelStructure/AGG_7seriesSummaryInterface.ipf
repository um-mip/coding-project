function createSeriesSummaryTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabDisplays = ""

    createEventsSeriesSumOutDF()
    DFREF seriesSumDFR = getEventsSeriesSumOutDF()

    SetDataFolder seriesSumDFR
    Make/O/N=1 sumCount, sumFreq, pks, int, der, t50r, fwhm, decay9010
    Make/O/D/N=1 sumTime
    SetScale d 0,0,"dat", sumTime
    Make/O seriesSumTrace

    edit /N=seriesSummaryTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.7, 0.95, 0.93) sumCount, sumTime, sumFreq, pks, int, der, t50r, fwhm, decay9010
    ModifyTable showParts=126
    tabDisplays += "seriesSummaryTable;"

    ModifyTable width(sumTime) = 100, format(sumTime)=8  // Not sure why date/time isn't working with SetScale above, trying this

    // Series Summary
    Display /W=(0.2, 0.1, 0.78, 0.68) /L/B /HOST=$panelName /HIDE=(1)
    RenameWindow #, seriesSumGraph
    tabDisplays += "seriesSumGraph"

    string tabControls = "cellNamesText_series;cellListBox_series;seriesNamesText_series;seriesListBox_series;"
    tabControls += "convertUnitsCheck;"
    // tabControls += "useAvgSubCheck;"
    tabControls += "allDispRadio;avgDispRadio;limitedDispRadio;intervalDispRadio;specifyDispRadio;"

    
    Button saveSeriesSum, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="seriesSumGraph", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveSeriesSum;"

    Wave/T listWave, titleWave
    Wave selWave
    [listWave, titleWave, selWave] = makeSeriesDisplayWaves()

    variable xPos = posRelPanel(0.80, "width", panelName = panelName)
    variable yPos = panelSize.yPosPnts + panelSize.buttonHeight
    ListBox displayTypeListBox_series, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight*.5}, listWave = titleWave, selWave = selWave
    ListBox displayTypeListBox_series, proc=displaySeriesGraphProc
    tabControls += "displayTypeListBox_series;"

    yPos += panelSize.listBoxHeight * 0.5
    TitleBox paramTitle_series pos = {xPos, yPos}, title = "Select a parameter:", frame=0, fstyle=1, size = {panelSize.buttonWidth, panelSize.buttonHeight}, anchor = LB
    tabControls += "paramTitle_series;"
    yPos += panelSize.buttonHeight
    
    make/O/N=0/T seriesSumDFR:blankTitle/Wave=titleWave_params
    make/O/N=0 seriesSumDFR:blankSel/Wave=selWave_params
    ListBox paramListBox_series, mode = 1, pos = {xPos, yPos}, size = {panelSize.listBoxWidth, panelSize.listBoxHeight}, listWave = titleWave_params, selWave = selWave_params
    ListBox paramListBox_series, proc=displaySeriesGraphProc
    tabControls += "paramListBox_series;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)

    SetDataFolder panelDFR
end

///////////
/// makeSeriesDisplayWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// EDITOR: Amanda Gibson
/// NOTES: 
/// Create the list wave, title wave, and select wave for the series summary displays
/// uses the seriesSummaryInfo structure to get these waves and names
///////////
function [wave/T listWave, wave/T titleWave, wave selWave] makeSeriesDisplayWaves()
    DFREF seriesSumDFR = getEventsSeriesSumOutDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder seriesSumDFR

    STRUCT seriesSummaryInfo seriesSum
    fillStringsinSeriesSummaryInfo(seriesSum)

    string listWaveNames = seriesSum.listWaveNames
    string niceNames = seriesSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/O/N=0/T listWave, titleWave
    make/O/N=0 selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave, titleWave, selWave]
end

///////////
/// makeSeriesParamWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// EDITOR: Amanda Gibson
/// NOTES: 
/// Create the list wave, title wave, and select wave for the series summary parameter waves
/// uses the seriesSummaryInfo structure to get these waves and names
///////////
function [wave/T listWave, wave/T titleWave, wave selWave] makeSeriesParamWaves()
    DFREF seriesSumDFR = getEventsSeriesSumOutDF()

    STRUCT seriesSummaryInfo seriesSum
    fillStringsinSeriesSummaryInfo(seriesSum)

    string listWaveNames = seriesSum.analysisTypes
    string niceNames = seriesSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/N=0/T/O seriesSumDFR:listWave_params/Wave=listWave
    make/N=0/T/O seriesSumDFR:titleWave_params/Wave=titleWave
    make/N=0/O seriesSumDFR:selWave_params/Wave=selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor

    return [listWave, titleWave, selWave]
end

function runMakeParams()
    [wave/T listWave, wave/T titleWave, wave selWave] = makeSeriesParamWaves()
end