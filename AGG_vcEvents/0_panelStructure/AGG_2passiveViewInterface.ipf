function createPassiveViewTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize
    string tabControls = ""
    string tabDisplays = ""

    string tabNumStr = num2str(tabNum)

    // Use the same cell and series list boxes already created for the series review tabs
    tabControls += "cellNamesText_series;"
    tabControls += "cellListBox_series;"
    tabControls += "seriesNamesText_series;"
    tabControls += "seriesListBox_series;"

    createPassivePanelDF()
    DFREF passiveDFR = getPassivePanelDF()

    SetDataFolder passiveDFR

    make/O/D Rinput, Rseries, RseriesSub, capa, holdingc, Tstart, Tstart_rel
    make/O/T passn
    
    edit /N=passivesTable /HIDE=(1) /K=1 /HOST=$panelName /W = (0.2, 0.07, 0.93, 0.7) passn, Rinput, Rseries, RseriesSub, capa, holdingc, Tstart_rel, Tstart
    ModifyTable format(Point)=1,format(Tstart)=8,width(passn)=164,width(tstart)=156
    ModifyTable showParts=126

    createPassivePanelSeriesDF()
    DFREF passiveSeriesDF = getPassivePanelSeriesDF()
    SetDataFolder passiveSeriesDF
    make/O/D/N=3 Rinput, Rseries, RseriesSub, capa, holdingc
    make/O/T/N=3 passn
    
    edit /N=passivesSeriesTable /HIDE=(1) /K=1 /HOST=$panelName /W = (0.2, 0.71, 0.93, 0.97) passn, Rinput, Rseries, RseriesSub, capa, holdingc
    ModifyTable format(Point)=1,width(passn)=164
    ModifyTable showParts=126
    tabDisplays += "passivesSeriesTable;"

    setDataFolder panelDFR

    Display/N=panelPassivesGraph/HIDE=(1)/K=1/Host=$panelName/W=(0.2, 0.07, 0.93, 0.7)
    tabDisplays += "panelPassivesGraph;" // default to this
    
    variable yPos = panelSize.yPosPnts + panelSize.listBoxHeight * 2 + panelSize.buttonHeight *2.5
    Button switchPassDisplay, pos = {panelSize.xPosPnts, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = switchPassDisplayProc, title = "Show Passive Table"
    Button switchPassDisplay, userdata(graph)="panelPassivesGraph", userdata(table)="passivesTable"
    tabControls += "switchPassDisplay;"
    
    yPos += panelSize.buttonHeight
    Button makeSepPassDisplay, pos = {panelSize.xPosPnts, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, proc = makePassDisplayProc, title = "Remake Passive Graph"
    tabControls += "makeSepPassDisplay;"

    yPos += panelSize.buttonHeight
    Button savePassGraph, pos = {panelSize.xPosPnts, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="panelPassiveGraph", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "savePassGraph;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end