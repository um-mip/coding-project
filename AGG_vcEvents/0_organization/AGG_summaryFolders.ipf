#pragma rtGlobals = 3 // Use modern global access method and strict wave access

////////////////////////////////////////////////////////////////////////
///////////////////////// DATA FOLDER FUNCTIONS \\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////////////////////////////////

// Outer Events folder
    ThreadSafe function createEventsDF()
        NewDataFolder/O root:Events
    end

    ThreadSafe function/DF getEventsDF()
        DFREF dfr = root:Events
        return dfr
    end

// Panel folder
    ThreadSafe function createEventsPanelDF()
        DFREF EventsDF = getEventsDF()
        NewDataFolder/O EventsDF:Panel
    end

    ThreadSafe function/DF getEventsPanelDF()
        DFREF EventsDF = getEventsDF()
        DFREF dfr = EventsDF:Panel
        return dfr
    end

// Units folder
    ThreadSafe function createEventsUnitsDF()
        DFREF panelDF = getEventsPanelDF()
        NewDataFolder/O panelDF:units
    end

    ThreadSafe function/DF getEventsUnitsDF()
        DFREF panelDF = getEventsPanelDF()
        DFREF dfr = panelDF:units
        return dfr
    end

// Use Subset folder
    ThreadSafe function createEventsUseAvgSubsetDF()
        DFREF panelDF = getEventsPanelDF()
        NewDataFolder/O panelDF:useAvgSubset
    end

    ThreadSafe function/DF getEventsUseAvgSubsetDF()
        DFREF panelDF = getEventsPanelDF()
        DFREF dfr = panelDF:useAvgSubset
        return dfr
    end

// Passives Folder
    ThreadSafe function createPassivePanelDF()
        DFREF panelDF = getEventsPanelDF()
        NewDataFolder/O panelDF:passives
    end

    ThreadSafe function/DF getPassivePanelDF()
        DFREF panelDF = getEventsPanelDF()
        DFREF dfr = panelDF:passives
        return dfr
    end

// Passive - Series Subfolder
    ThreadSafe function createPassivePanelSeriesDF()
        DFREF passiveDF = getPassivePanelDF()
        NewDataFolder/O passiveDF:series
    end

    ThreadSafe function/DF getPassivePanelSeriesDF()
        DFREF passiveDF = getPassivePanelDF()
        DFREF dfr = passiveDF:series
        return dfr
    end

// Event Detection Param Folder
    ThreadSafe function createEvDetectDF()
        DFREF panelDF = getEventsPanelDF()
        NewDataFolder/O panelDF:evDetect
    end

    ThreadSafe function/DF getEvDetectDF()
        DFREF panelDF = getEventsPanelDF()
        DFREF dfr = panelDF:evDetect
        return dfr
    end

// Event Confirmation Param Folder
    ThreadSafe function createEvConfirmDF()
        DFREF panelDF = getEventsPanelDF()
        NewDataFolder/O panelDF:evConfirm
    end

    ThreadSafe function/DF getEvConfirmDF()
        DFREF panelDF = getEventsPanelDF()
        DFREF dfr = panelDF:evConfirm
        return dfr
    end

// Cell Info Folder
    ThreadSafe function createEventsInfoDF()
        DFREF EventsDF = getEventsDF()
        NewDataFolder/O EventsDF:cellInfo
    end

    ThreadSafe function/DF getEventsInfoDF()
        DFREF EventsDF = getEventsDF()
        DFREF dfr = EventsDF:cellInfo
        return dfr
    end

    ThreadSafe function createEventscellInfoDF(cellName)
        string cellName
        string folderName = "c" + cellName
        DFREF infoDFR = getEventsInfoDF() // info folder should already have been created

        NewDataFolder/O infoDFR:$folderName
    end

    ThreadSafe function/DF getEventsCellInfoDF(cellName)
        string cellName
        string folderName = "c" + cellName
        DFREF infoDFR = getEventsInfoDF()
        // printDFREF(infoDFR)

        DFREF dfr = infoDFR:$folderName
        // print "in getEventsCellInfoDF", "folderName", folderName
        // printDFREF(dfr)
        return dfr
    end

    ThreadSafe function/DF getEventsCellInfoDF_byFolderName(folderName)
        string folderName
        DFREF infoDFR = getEventsInfoDF()

        DFREF dfr = infoDFR:$folderName
        return dfr
    end

    ThreadSafe function killEventsCellInfoDF(cellName)
        string cellName

        DFREF cellDFR = getEventsCellInfoDF(cellName)
        if(DataFolderRefStatus(cellDFR) != 0)
            KillDataFolder/Z cellDFR
        endif
    end

// Events Detection Waves Folder
// AGG - 2022-05-14 - Switched this to root instead of EventsWaves for each cell
// Kept the calling structure for this function within the code in case want to resurrect
// But switching back to root to avoid duplicating all of the data and save on experiment space
// Commented out the actions within copySeriesEventsWaves to avoid duplicating into same location
    ThreadSafe function createEventsdetectWavesDF(cellName)
        string cellName

        // DFREF cellDFR = getEventsCellInfoDF(cellName)
        // NewDataFolder/O cellDFR:Eventswaves
    end

    ThreadSafe function/DF getEventsdetectWavesDF(cellName)
        string cellName
        // DFREF cellDFR = getEventsCellInfoDF(cellName)

        // DFREF dfr = cellDFR:Eventswaves
        // return dfr

        DFREF rootRef = root:
        return rootRef
    end

function removeEventsWavesFolders()
    clearUnnamedCellRows()

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Struct outputWaves outWaves
    StructFill/SDFR=cellsOutDFR outWaves

    redimension/N=0 outWaves.cellName, outWaves.groupName, outWaves.sumCount, outWaves.sumTime, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.decay9010, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc

    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        variable numAddedCells = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell] // These could be just duplicated directly, but this could be safer, or if only want cells with event folders
            string groupName = groupNames[iCell]
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
            if(dfrStatus != 0)
                DFREF eventsDFR = cellInfoDFR:EventsWaves
                if(dataFolderRefStatus(eventsDFR)!=0)
                    KillDataFolder eventsDFR
                endif
            endif
        endfor
    endif
end


// Series Sub Folder
    ThreadSafe function createSeriesDF(cellName, seriesName)
        string cellName, seriesName

        DFREF cellDFR = getEventsCellInfoDF(cellName)
        if(DataFolderRefStatus(cellDFR) == 0)
            print "cell folder doesn't exist", strlen(cellName), cellName
            // printDFREF(cellDFR)
        else
            // print cellName, "in createSeriesDF"
            NewDataFolder/O cellDFR:$("s" + seriesName)
        endif
        
    end

    ThreadSafe function/DF getSeriesDF (cellName, seriesName)
        string cellName, seriesName

        DFREF cellDFR = getEventsCellInfoDF(cellName)

        DFREF dfr = cellDFR:$("s"+seriesName)
        return dfr
    end

// Event detection and avg calc for avg only
    ThreadSafe function createSeriesEventsForAvgDF(cellName, seriesName)
        string cellName, seriesName

        DFREF seriesDFR = getSeriesDF(cellName, seriesName)
        NewDataFolder/O seriesDFR:avgSubset
    end

    ThreadSafe function/DF getSeriesEventsForAvgDF(cellName, seriesName)
        string cellName, seriesName
        DFREF seriesDFR = getSeriesDF(cellName, seriesName)

        DFREF dfr = seriesDFR:avgSubset
        return dfr
    end

// Event detection and avg calc for limited events avg only subset
    ThreadSafe function createSeriesEventsForAvgLimitedDF(cellName, seriesName)
        string cellName, seriesName

        DFREF avgSubsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)
        NewDataFolder/O avgSubsetDFR:limited
    end

    ThreadSafe function/DF getSeriesEventsForAvgLimitedDF(cellName, seriesName)
        string cellName, seriesName
        DFREF avgSubsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)

        DFREF dfr = avgSubsetDFR:limited
        return dfr
    end

// Event detection and avg calc for interval subset
    ThreadSafe function createSeriesEventsForIntervalDF(cellName, seriesName)
        string cellName, seriesName

        DFREF seriesDFR = getSeriesDF(cellName, seriesName)
        NewDataFolder/O seriesDFR:intervalDF
    end

    ThreadSafe function/DF getSeriesEventsForIntervalDF(cellName, seriesName)
        string cellName, seriesName
        DFREF seriesDFR = getSeriesDF(cellName, seriesName)

        DFREF dfr = seriesDFR:intervalDF
        return dfr
    end

// Indiv Series Out Folder
    ThreadSafe function createEventsIndivSeriesOutDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:seriesOut
    end

    ThreadSafe function/DF getEventsIndivSeriesOutDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:seriesOut
        return dfr
    end

// Series Summary Out Folder
    ThreadSafe function createEventsSeriesSumOutDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:seriesSumOut
    end

    ThreadSafe function/DF getEventsSeriesSumOutDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:seriesSumOut
        return dfr
    end
    
// Raw series plot folder
    ThreadSafe function createEventsRawDataDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:rawData
    end

    ThreadSafe function/DF getEventsRawDataDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:rawData
        return dfr
    end

// Series Output Folder
    ThreadSafe function createSeriesOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:allSeriesTbl
    end

    ThreadSafe function/DF getSeriesOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:allSeriesTbl
        return dfr
    end


// Cell Summary Out Folder
    ThreadSafe function createEventsCellSumOutDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:cellSumOut
    end

    ThreadSafe function/DF getEventsCellSumOutDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:cellSumOut
        return dfr
    end

// Cell Summary Out Folder - Smart conc
    ThreadSafe function createEventsCellSumOutDF_smartConc()
        DFREF panelDFR = getEventsCellSumOutDF()
        NewDataFolder/O panelDFR:smartConc
    end

    ThreadSafe function/DF getEventsCellSumOutDF_smartConc()
        DFREF panelDFR = getEventsCellSumOutDF()
        DFREF dfr = panelDFR:smartConc
        return dfr
    end

// Cell Summary Indiv Folder
    ThreadSafe function createCellSummaryDF(cellName)
        string cellName

        DFREF cellDFR = getEventsCellInfoDF(cellName)
        if(DataFolderRefStatus(cellDFR) == 0)
            print "cell folder doesn't exist", strlen(cellName), cellName
            // printDFREF(cellDFR)
        else
            // print cellName, "in createCellSummaryDF"
            NewDataFolder/O cellDFR:summary
        endif
        
    end

    ThreadSafe function/DF getCellSummaryDF (cellName)
        string cellName

        DFREF cellDFR = getEventsCellInfoDF(cellName)
        if(dataFolderRefStatus(cellDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellDFR 
        endif

        DFREF dfr = cellDFR:summary
        return dfr
    end

// Cell Summary - Regions - Indiv Folder
    ThreadSafe function createCellRegionsDF(cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)

        NewDataFolder/O cellSumDFR:Regions        
    end

    ThreadSafe function/DF getCellRegionsDF (cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:Regions
        return dfr
    end

// Cell Summary - Regions - Indiv Folder
    ThreadSafe function createCellSpecRegionDF(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF(cellName)

        NewDataFolder/O cellRegionsDFR:$regionName        
    end

    ThreadSafe function/DF getCellSpecRegionDF (cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF(cellName)
        if(dataFolderRefStatus(cellRegionsDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellRegionsDFR 
        endif

        DFREF dfr = cellRegionsDFR:$regionName
        return dfr
    end

// Cell Summary - Smart Conc - Indiv Folder
    ThreadSafe function createCellSmartConcDF(cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)

        NewDataFolder/O cellSumDFR:SmartConc        
    end

    ThreadSafe function/DF getCellSmartConcDF (cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:SmartConc
        return dfr
    end

// Cell Summary - Avg subset - Indiv Folder
    ThreadSafe function createCellAvgSubDF(cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)

        NewDataFolder/O cellSumDFR:avgSubset        
    end

    ThreadSafe function/DF getCellAvgSubDF (cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:avgSubset
        return dfr
    end

// Cell Summary - Avg subset - Regions - Indiv Folder
    ThreadSafe function createCellRegionsDF_avg(cellName)
        string cellName
        DFREF cellSumDFR = getCellAvgSubDF(cellName)

        NewDataFolder/O cellSumDFR:Regions        
    end

    ThreadSafe function/DF getCellRegionsDF_avg (cellName)
        string cellName
        DFREF cellSumDFR = getCellAvgSubDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:Regions
        return dfr
    end

// Cell Summary - Avg subset - Regions - Indiv Folder
    ThreadSafe function createCellSpecRegionDF_avg(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_avg(cellName)

        NewDataFolder/O cellRegionsDFR:$regionName        
    end

    ThreadSafe function/DF getCellSpecRegionDF_avg(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_avg(cellName)
        if(dataFolderRefStatus(cellRegionsDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellRegionsDFR 
        endif

        DFREF dfr = cellRegionsDFR:$regionName
        return dfr
    end

// Cell Summary - Avg subset - Indiv Folder
    ThreadSafe function createCellAvgSubLimitedDF(cellName)
        string cellName
        DFREF avgSubsetDFR = getCellAvgSubDF(cellName)

        NewDataFolder/O avgSubsetDFR:limited        
    end

    ThreadSafe function/DF getCellAvgSubLimitedDF (cellName)
        string cellName
        DFREF avgSubsetDFR = getCellAvgSubDF(cellName)
        if(dataFolderRefStatus(avgSubsetDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return avgSubsetDFR 
        endif

        DFREF dfr = avgSubsetDFR:limited
        return dfr
    end

// Cell Summary - limited avg subset - Regions - Indiv Folder
    ThreadSafe function createCellRegionsDF_limitedAvg(cellName)
        string cellName
        DFREF cellSumDFR = getCellAvgSubLimitedDF(cellName)

        NewDataFolder/O cellSumDFR:Regions        
    end

    ThreadSafe function/DF getCellRegionsDF_limitedAvg (cellName)
        string cellName
        DFREF cellSumDFR = getCellAvgSubLimitedDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:Regions
        return dfr
    end

// Cell Summary - limited avg subset - Regions - Indiv Folder
    ThreadSafe function createCellSpecRegionDF_limitedAvg(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_limitedAvg(cellName)

        NewDataFolder/O cellRegionsDFR:$regionName        
    end

    ThreadSafe function/DF getCellSpecRegionDF_limitedAvg(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_limitedAvg(cellName)
        if(dataFolderRefStatus(cellRegionsDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellRegionsDFR 
        endif

        DFREF dfr = cellRegionsDFR:$regionName
        return dfr
    end

// Cell Summary - Interval subset - Indiv Folder
    ThreadSafe function createCellIntSubDF(cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)

        NewDataFolder/O cellSumDFR:intSubset        
    end

    ThreadSafe function/DF getCellIntSubDF (cellName)
        string cellName
        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:intSubset
        return dfr
    end

// Cell Summary - interval subset - Regions - Indiv Folder
    ThreadSafe function createCellRegionsDF_interval(cellName)
        string cellName
        DFREF cellSumDFR = getCellIntSubDF(cellName)

        NewDataFolder/O cellSumDFR:Regions        
    end

    ThreadSafe function/DF getCellRegionsDF_interval (cellName)
        string cellName
        DFREF cellSumDFR = getCellIntSubDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellSumDFR 
        endif

        DFREF dfr = cellSumDFR:Regions
        return dfr
    end

// Cell Summary - interval subset - Regions - Indiv Folder
    ThreadSafe function createCellSpecRegionDF_interval(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_interval(cellName)

        NewDataFolder/O cellRegionsDFR:$regionName        
    end

    ThreadSafe function/DF getCellSpecRegionDF_interval(cellName, regionName)
        string cellName, regionName
        DFREF cellRegionsDFR = getCellRegionsDF_interval(cellName)
        if(dataFolderRefStatus(cellRegionsDFR)==0)
            // if the higher datafolder doesn't exist, return the null DFR
            // rather than trying to add on to the null DFR, which defaults to root
            return cellRegionsDFR 
        endif

        DFREF dfr = cellRegionsDFR:$regionName
        return dfr
    end

// All Cells Output Folder
    ThreadSafe function createAllCellsOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:allCellsTbl
    end

    ThreadSafe function/DF getAllCellsOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:allCellsTbl
        return dfr
    end

// All Cells Output Folder - All events
    ThreadSafe function createAllCellsOutputDF_allEvents()
        DFREF panelDFR = getAllCellsOutputDF()
        NewDataFolder/O panelDFR:all_events
    end

    ThreadSafe function/DF getAllCellsOutputDF_allEvents()
        DFREF panelDFR = getAllCellsOutputDF()
        DFREF dfr = panelDFR:all_events
        return dfr
    end

// All Cells Output Folder - All events temp duplication folder
    ThreadSafe function createAllCellsOutputDF_allEvents_dup()
        DFREF panelDFR = getAllCellsOutputDF_allEvents()
        NewDataFolder/O panelDFR:dup
    end

    ThreadSafe function/DF getAllCellsOutputDF_allEvents_dup()
        DFREF panelDFR = getAllCellsOutputDF_allEvents()
        DFREF dfr = panelDFR:dup
        return dfr
    end

// All Groups Output Folder
    ThreadSafe function createAllGroupsOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:groupsGraph
    end

    ThreadSafe function/DF getAllGroupsOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:groupsGraph
        return dfr
    end

// Opto output folder
    ThreadSafe function createOptoOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:optoOutput
    end

    ThreadSafe function/DF getOptoOutputDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:optoOutput
        return dfr
    end


// // Temp Waves Folder
//     ThreadSafe function createTemEventsTwavesDF()
//         NewDataFolder/O root:SCIW
//     end

//     ThreadSafe function/DF getTemEventsTwavesDF()
//         DFREF dfr = root:SCIW
//         return dfr
//     end

//     ThreadSafe function killTemEventsTwavesDF()
//         if(DataFolderRefStatus(root:SCIW) != 0)
//             KillDataFolder/Z root:SCIW
//         endif
//     end
    

// // Waves folder
//     ThreadSafe function createSCTwavesDF()
//         DFREF EventsDF = getEventsDF()
//         NewDataFolder/O EventsDF:SCTW
//     end

//     ThreadSafe function/DF getSCTWavesDF()
//         DFREF EventsDF = getEventsDF()
//         DFREF dfr = EventsDF:SCTW
//         return dfr
//     end

// Detection folder
    ThreadSafe function createEventsdetectionDF()
        DFREF EventsDF = getEventsDF()
        NewDataFolder/O EventsDF:detect
    end

    ThreadSafe function/DF getEventsdetectionDF()
        DFREF EventsDF = getEventsDF()
        DFREF dfr = EventsDF:detect
        return dfr
    end

// Analysis folder
    ThreadSafe function createEventsanalysisDF()
        DFREF EventsDF = getEventsDF()
        NewDataFolder/O EventsDF:ansys
    end

    ThreadSafe function/DF getEventsanalysisDF()
        DFREF EventsDF = getEventsDF()
        DFREF dfr = EventsDF:ansys
        return dfr
    end

// Cell detection folder
    ThreadSafe function createEventsCellDetectionDF(cellName)
        string cellName
        string folderName = "c" + cellName
        DFREF detectDFR = getEventsdetectionDF() // detection folder should already have been created

        NewDataFolder/O detectDFR:$folderName
    end

    ThreadSafe function/DF getEventsCellDetectionDF(cellName)
        string cellName
        string folderName = "c" + cellName
        DFREF detectDFR = getEventsdetectionDF()

        DFREF dfr = detectDFR:$folderName
        return dfr
    end

    ThreadSafe function/DF getEventsCellDetectionDF_byFolderName(folderName)
        string folderName
        DFREF detectDFR = getEventsdetectionDF()

        DFREF dfr = detectDFR:$folderName
        return dfr
    end

    ThreadSafe function killEventsCellDetectionDF(cellName)
        string cellName

        DFREF cellDFR = getEventsCellDetectionDF(cellName)
        if(DataFolderRefStatus(cellDFR) != 0)
            KillDataFolder/Z cellDFR
        endif
    end

// Specific Analysis Folder
    ThreadSafe function createEventsSpecAnalysisDF(analysisName)
        string analysisName
        DFREF analysisDFR = getEventsanalysisDF() // analysis folder should already have been created
        NewDataFolder/O analysisDFR:$analysisName
        return 0
    end

    ThreadSafe function/DF getEventsSpecAnalysisDF(analysisName)
        string analysisName
        DFREF analysisDFR = getEventsanalysisDF() // analysis folder should already have been created

        DFREF dfr = analysisDFR:$analysisName
        return dfr
    end

    ThreadSafe function killEventsSpecAnalysisDF(analysisName)
        string analysisName

        DFREF analysisDFR = getEventsSpecAnalysisDF(analysisName)

        if(DataFolderRefStatus(analysisDFR) != 0)
            KillDataFolder /Z analysisDFR
        endif
    end

// Cell analysis folder
    ThreadSafe function createEventsCellAnalysisDF(analysisName, cellName)
        string analysisName, cellName
        string folderName = "c" + cellName
        DFREF analysisDFR = getEventsSpecAnalysisDF(analysisName)

        NewDataFolder/O analysisDFR:$folderName
    end

    ThreadSafe function/DF getEventsCellAnalysisDF(analysisName, cellName)
        string analysisName, cellName
        string folderName = "c" + cellName
        DFREF analysisDFR = getEventsSpecAnalysisDF(analysisName)

        DFREF dfr = analysisDFR:$folderName
        return dfr
    end

    ThreadSafe function/DF getEventsCellAnalysisDF_byFolderName(analysisName, cellFolderName)
        string analysisName, cellFolderName
        DFREF analysisDFR = getEventsSpecAnalysisDF(analysisName)

        DFREF dfr = analysisDFR:$cellFolderName
        return dfr
    end

// Individual Cell Output Tables
    ThreadSafe function createEventsIndivCellOutDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:cellOut
    end

    ThreadSafe function/DF getEventsIndivCellOutDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:cellOut
        return dfr
    end

// All Cells Output By BW Tables
    ThreadSafe function createEventsallCellsOutDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:outByBW
    end

    ThreadSafe function/DF getEventsallCellsOutDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:outByBW
        return dfr
    end

// Full Histo Folder
    ThreadSafe function createEventsViewHistoDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:viewHisto
    end

    ThreadSafe function/DF getEventsViewHistoDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:viewHisto
        Return dfr
    end

// All Histograms Folder
    ThreadSafe function createEventsAllHistoDF()
        DFREF panelDFR = getEventsPanelDF()
        NewDataFolder/O panelDFR:allHisto
    end

    ThreadSafe function/DF getEventsAllHistoDF()
        DFREF panelDFR = getEventsPanelDF()
        DFREF dfr = panelDFR:allHisto
        Return dfr
    end

// Groups folder
    ThreadSafe function createGroupsInfoDF()
        DFREF EventsDF = getEventsDF()
        NewDataFolder/O EventsDF:groupsInfo
    end

    ThreadSafe function/DF getGroupsInfoDF()
        DFREF EventsDF = getEventsDF()
        DFREF dfr = EventsDF:groupsInfo
        return dfr
    end

// Overall group region folder
    function createGroupInfoRegionDF()
        DFREF groupsInfoDF = getGroupsInfoDF()
        NewDataFolder/O groupsInfoDF:Region
    end

    function/DF getGroupInfoRegionDF()
        DFREF groupsInfoDF = getgroupsInfoDF()
        DFREF dfr = groupsInfoDF:Region
        return dfr
    end

// Overall group specific region folder
    function createGroupInfoSpecRegionDF(string regionName)
        DFREF groupsInfoDF = getGroupInfoRegionDF()
        NewDataFolder/O groupsInfoDF:$regionName
    end

    function/DF getGroupInfoSpecRegionDF(string regionName)
        DFREF groupsInfoDF = getGroupInfoRegionDF()
        if(dataFolderRefStatus(groupsInfoDF)==0)
            return groupsInfoDF
        endif
        DFREF dfr = groupsInfoDF:$regionName
        return dfr
    end

// Overall group average folder
    function createGroupInfoForAvgDF()
        DFREF groupsInfoDF = getGroupsInfoDF()
        NewDataFolder/O groupsInfoDF:avgSubset
    end

    function/DF getGroupInfoForAvgDF()
        DFREF groupsInfoDF = getgroupsInfoDF()
        DFREF dfr = groupsInfoDF:avgSubset
        return dfr
    end

// Overall groups folder - avg subset - Region
    function createGroupInfoForAvgRegionDF()
        DFREF groupDF = getGroupInfoForAvgDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgDF()
            DFREF groupDF = getGroupInfoForAvgDF()
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupInfoForAvgRegionDF()
        DFREF groupDF = getGroupInfoForAvgDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgDF()
            DFREF groupDF = getGroupInfoForAvgDF()
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Overall groups folder - avg subset - Specific region
    function createGroupInfoForAvgSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForAvgRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgDF()
            DFREF groupDF = getGroupInfoForAvgRegionDF()
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupInfoForAvgSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForAvgRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgRegionDF()
            DFREF groupDF = getGroupInfoForAvgRegionDF()
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end

// Overall groups folder - limited avg subset
    function createGroupInfoForAvgLimitedDF()
        DFREF groupDF = getGroupInfoForAvgDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgDF()
            DFREF groupDF = getGroupInfoForAvgDF()
        endif
        NewDataFolder/O groupDF:limited
    end

    function/DF getGroupInfoForAvgLimitedDF()
        DFREF groupDF = getGroupInfoForAvgDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgDF()
            DFREF groupDF = getGroupInfoForAvgDF()
        endif
        DFREF dfr = groupDF:limited
        return dfr
    end

// Overall groups folder - limited avg subset - Region
    function createGroupInfoForAvgLimitedRegionDF()
        DFREF groupDF = getGroupInfoForAvgLimitedDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgLimitedDF()
            DFREF groupDF = getGroupInfoForAvgLimitedDF()
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupInfoForAvgLimitedRegionDF()
        DFREF groupDF = getGroupInfoForAvgLimitedDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgLimitedDF()
            DFREF groupDF = getGroupInfoForAvgLimitedDF()
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Overall groups folder - limited avg subset - Region
    function createGroupInfoForAvgLimitedSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForAvgLimitedRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgLimitedRegionDF()
            DFREF groupDF = getGroupInfoForAvgLimitedRegionDF()
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupInfoForAvgLimitedSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForAvgLimitedRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForAvgLimitedRegionDF()
            DFREF groupDF = getGroupInfoForAvgLimitedRegionDF()
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end

// Overall groups folder - interval subset
    function createGroupInfoForIntervalDF()
        DFREF groupDF = getGroupsInfoDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupsInfoDF()
            DFREF groupDF = getGroupsInfoDF()
        endif
        NewDataFolder/O groupDF:intervalDF
    end

    function/DF getGroupInfoForIntervalDF()
        DFREF groupDF = getGroupsInfoDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupsInfoDF()
            DFREF groupDF = getGroupsInfoDF()
        endif
        DFREF dfr = groupDF:intervalDF
        return dfr
    end

// Overall groups folder - interval subset - Region
    function createGroupInfoForIntervalRegionDF()
        DFREF groupDF = getGroupInfoForIntervalDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForIntervalDF()
            DFREF groupDF = getGroupInfoForIntervalDF()
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupInfoForIntervalRegionDF()
        DFREF groupDF = getGroupInfoForIntervalDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForIntervalDF()
            DFREF groupDF = getGroupInfoForIntervalDF()
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Overall groups folder - interval subset - Specific region
    function createGroupInfoForIntervalSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForIntervalRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForIntervalRegionDF()
            DFREF groupDF = getGroupInfoForIntervalRegionDF()
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupInfoForIntervalSpecRegionDF(string regionName)
        DFREF groupDF = getGroupInfoForIntervalRegionDF()
        if(dataFolderRefStatus(groupDF)==0)
            createGroupInfoForIntervalRegionDF()
            DFREF groupDF = getGroupInfoForIntervalRegionDF()
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end

// Indiv groups folder
    function createGroupDF(string groupName)
        DFREF groupsInfoDF = getGroupsInfoDF()
        NewDataFolder/O groupsInfoDF:$groupName
    end

    function/DF getGroupDF(string groupName)
        DFREF groupsInfoDF = getgroupsInfoDF()
        DFREF dfr = groupsInfoDF:$groupName
        return dfr
    end

// Indiv groups folder - Region
    function createGroupRegionsDF(string groupName)
        DFREF groupsInfoDF = getGroupDF(groupName)
        NewDataFolder/O groupsInfoDF:Region
    end

    function/DF getGroupRegionsDF(string groupName)
        DFREF groupsInfoDF = getGroupDF(groupName)
        DFREF dfr = groupsInfoDF:Region
        return dfr
    end

// Indiv groups folder - Specific Region
    function createGroupSpecRegionDF(string groupName, string regionName)
        DFREF groupsInfoDF = getGroupRegionsDF(groupName)
        NewDataFolder/O groupsInfoDF:$regionName
    end

    function/DF getGroupSpecRegionDF(string groupName, string regionName)
        DFREF groupsInfoDF = getGroupRegionsDF(groupName)
        DFREF dfr = groupsInfoDF:$regionName
        return dfr
    end

// Indiv groups folder - avg subset
    function createGroupForAvgDF(string groupName)
        DFREF groupDF = getGroupDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupDF(groupName)
            DFREF groupDF = getGroupDF(groupName)
        endif
        NewDataFolder/O groupDF:avgSubset
    end

    function/DF getGroupForAvgDF(string groupName)
        DFREF groupDF = getGroupDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupDF(groupName)
            DFREF groupDF = getGroupDF(groupName)
        endif
        DFREF dfr = groupDF:avgSubset
        return dfr
    end

// Indiv groups folder - avg subset - Region
    function createGroupForAvgRegionDF(string groupName)
        DFREF groupDF = getGroupForAvgDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgDF(groupName)
            DFREF groupDF = getGroupForAvgDF(groupName)
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupForAvgRegionDF(string groupName)
        DFREF groupDF = getGroupForAvgDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgDF(groupName)
            DFREF groupDF = getGroupForAvgDF(groupName)
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Indiv groups folder - avg subset - Specific region
    function createGroupForAvgSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForAvgRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgDF(groupName)
            DFREF groupDF = getGroupForAvgRegionDF(groupName)
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupForAvgSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForAvgRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgRegionDF(groupName)
            DFREF groupDF = getGroupForAvgRegionDF(groupName)
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end

// Indiv groups folder - limited avg subset
    function createGroupForAvgLimitedDF(string groupName)
        DFREF groupDF = getGroupForAvgDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgDF(groupName)
            DFREF groupDF = getGroupForAvgDF(groupName)
        endif
        NewDataFolder/O groupDF:limited
    end

    function/DF getGroupForAvgLimitedDF(string groupName)
        DFREF groupDF = getGroupForAvgDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgDF(groupName)
            DFREF groupDF = getGroupForAvgDF(groupName)
        endif
        DFREF dfr = groupDF:limited
        return dfr
    end

// Indiv groups folder - limited avg subset - Region
    function createGroupForAvgLimitedRegionDF(string groupName)
        DFREF groupDF = getGroupForAvgLimitedDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgLimitedDF(groupName)
            DFREF groupDF = getGroupForAvgLimitedDF(groupName)
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupForAvgLimitedRegionDF(string groupName)
        DFREF groupDF = getGroupForAvgLimitedDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgLimitedDF(groupName)
            DFREF groupDF = getGroupForAvgLimitedDF(groupName)
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Indiv groups folder - limited avg subset - Region
    function createGroupForAvgLimitedSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForAvgLimitedRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgLimitedRegionDF(groupName)
            DFREF groupDF = getGroupForAvgLimitedRegionDF(groupName)
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupForAvgLimitedSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForAvgLimitedRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForAvgLimitedRegionDF(groupName)
            DFREF groupDF = getGroupForAvgLimitedRegionDF(groupName)
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end

// Indiv groups folder - interval subset
    function createGroupForIntervalDF(string groupName)
        DFREF groupDF = getGroupDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupDF(groupName)
            DFREF groupDF = getGroupDF(groupName)
        endif
        NewDataFolder/O groupDF:intervalDF
    end

    function/DF getGroupForIntervalDF(string groupName)
        DFREF groupDF = getGroupDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupDF(groupName)
            DFREF groupDF = getGroupDF(groupName)
        endif
        DFREF dfr = groupDF:intervalDF
        return dfr
    end

// Indiv groups folder - interval subset - Region
    function createGroupForIntervalRegionDF(string groupName)
        DFREF groupDF = getGroupForIntervalDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForIntervalDF(groupName)
            DFREF groupDF = getGroupForIntervalDF(groupName)
        endif
        NewDataFolder/O groupDF:Region
    end

    function/DF getGroupForIntervalRegionDF(string groupName)
        DFREF groupDF = getGroupForIntervalDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForIntervalDF(groupName)
            DFREF groupDF = getGroupForIntervalDF(groupName)
        endif
        DFREF dfr = groupDF:Region
        return dfr
    end

// Indiv groups folder - interval subset - Specific region
    function createGroupForIntervalSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForIntervalRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForIntervalRegionDF(groupName)
            DFREF groupDF = getGroupForIntervalRegionDF(groupName)
        endif
        NewDataFolder/O groupDF:$regionName
    end

    function/DF getGroupForIntervalSpecRegionDF(string groupName, string regionName)
        DFREF groupDF = getGroupForIntervalRegionDF(groupName)
        if(dataFolderRefStatus(groupDF)==0)
            createGroupForIntervalRegionDF(groupName)
            DFREF groupDF = getGroupForIntervalRegionDF(groupName)
        endif
        DFREF dfr = groupDF:$regionName
        return dfr
    end



///////////////// Derivative detection \\\\\\\\\\\\\\\\
// Detection variables folder
    ThreadSafe function createDerivDetectVarsDF()
        NewDataFolder/O root:DerivDetectVars
    end

    ThreadSafe function/DF getDerivDetectVarsDF()
        DFREF dfr = root:DerivDetectVars
        return dfr
    end

// Previous detection variables folder
    ThreadSafe function createPrevDerivDetectVarsDF()
        NewDataFolder/O root:PrevDerivDetectVars
    end

    ThreadSafe function/DF getPrevDerivDetectVarsDF()
        DFREF dfr = root:PrevDerivDetectVars
        return dfr
    end


//////////////////// experiment cards \\\\\\\\\\\\\\\\\\\\\\\

function createExpCardDF()
    NewDataFolder/O root:expCards
end

function/DF getExpCardDF()
    DFREF dfr = root:expCards
    return dfr
end

function createCellExpCardDF(string cellName)
    DFREF dfr = getExpCardDF()
    if(dataFolderRefStatus(dfr)==0)
        createExpCardDF()
        DFREF dfr = getExpCardDF()
    endif

    NewDataFolder/O dfr:$("c" + cellName)
end

function/DF getCellExpCardDF(string cellName)
    DFREF cardDFR = getExpCardDF()
    if(dataFolderRefStatus(cardDFR)==0)
        createExpCardDF()
        DFREF cardDFR = getExpCardDF()
    endif

    DFREF dfr = cardDFR:$("c" + cellName)

    return dfr
end