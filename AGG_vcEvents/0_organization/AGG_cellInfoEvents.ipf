#pragma rtGlobals=3		//Use modern global access method and strict wave access

///////////////////////////////////////////////////////
/// COPY SERIES WAVES INTO FOLDER FOR CELL
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: ~ Jan 2022
/// NOTES: 
///////////////////////////////////////////////////////

///////////
///  Function Name
/// AUTHOR: 
/// EDITOR: 
/// ORIGINAL DATE: 2022-04-14
/// UPDATED: 
/// NOTES: 
///////////
function copySeriesEventsWaves(cellName, Events_series)
    string cellName
    variable Events_series

    // AGG - 2022-05-14 - removed copy/storage of events waves in separate folders for each cell
    // Was just duplicating the data and making the experiment unnecessarily large
    // Still keeping structure in case want to later add infrastructure to do detection/confirmation
    // in specific subfolders, but that's going to be harder to change

    // string seriesName = buildSeriesWaveName(cellName, Events_series) + "*"
    // // string seriesWaves = WaveList(seriesName, ";", "")

    // DFREF cellDetectDFR = getEventsdetectWavesDF(cellName)
    // if(DataFolderRefStatus(cellDetectDFR) == 0)
    //     // print "folder doesn't exist"
    //     createEventsdetectWavesDF(cellName)
    //     cellDetectDFR = getEventsdetectWavesDF(cellName)
    // endif

    // // AGG - TO:DO - change this to a series-specific folder?

    // // variable nWaves = itemsInList(seriesWaves)
    // // variable iWave = 0

    // // // Copy the series waves into the cell's detection waves folder
    // // for(iWave = 0; iWave < nWaves; iWave ++)

    // // endfor

	// DFREF dfr = root:
    // string objName
    // #if (IgorVersion() < 9.00)
    // Variable index = 0
	// do
	// 	objName = GetIndexedObjNameDFR(dfr, 1, index)
    //     // print objName
	// 	if (strlen(objName) == 0)
	// 		break
	// 	endif
    //     if(stringmatch(objName, seriesName))
    //         // Print "Copying wave", objName, "series", seriesName
    //         if(WaveExists(root:$objName))
    //             Duplicate /O root:$objName, cellDetectDFR:$objName
    //         else
    //             // this was happening when initializing because were searching for objects in root
    //             // but were building the panel in different data folders
    //             print "Somehow, the wave that was found doesn't exist"
    //         endif
    //     endif
	// 	index += 1
	// while(1)
    // #endif

    // // This is WAY faster, as it searches for matching waves with wavelist
    // // instead of indexing through all objects
    // // Need Igor 9 to be able to specify the folder that should be searched in WaveList
    // #if (IgorVersion() >= 9.00)
    //     string matchingWaves = WaveList(seriesName, ";", "", dfr)

    //     variable iWave = 0, nWaves = itemsinlist(matchingWaves)
    //     for(iWave=0; iWave<nWaves; iWave++)
    //         objName = StringFromList(iWave, matchingWaves)
    //         if(WaveExists(root:$objName))
    //             // print "copying wave", objName
    //             Duplicate /O root:$objName, cellDetectDFR:$objName
    //         else
    //             // this was happening when initializing because were searching for objects in root
    //             // but were building the panel in different data folders
    //             print "Somehow, the wave that was found doesn't exist"
    //         endif
    //     endfor
    // #endif
end