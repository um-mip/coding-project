///////////
/// deleteWavesFromCellsNotInCellList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-07
/// NOTES: 
///////////
function deleteWavesFromCellsNotInCellList()
    string wavesMatchingCellRegEx = findWavesInDFRByRegEx("([[:digit:]]+)([[:alpha:]])g([[:digit:]]+)s([[:digit:]]+)sw([[:digit:]]+)t.*")

    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID = getCellIDFromWaveN(waveN)
        if(strlen(cellID)>0)
            if(!isStringInWave(cellID, cellNames))
                // print waveN
                KillWaves/Z $waveN
            endif
        endif
    endfor
end

///////////
/// deleteFoldersFromCellsNotInCellList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-07
/// NOTES: 
///////////
function deleteFoldersFromCellsNotInCellList()
    DFREF cellInfoDFR = getEventsInfoDF()
    string cellFoldersList = DataFolderList("*", ";", cellInfoDFR)

    variable iFolder = 0, nFolders = itemsInList(cellFoldersList)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    for(iFolder=0; iFolder<nFolders; iFolder++)
        string folderName = StringFromList(iFolder, cellFoldersList)
        string cellID = folderName[1, strlen(folderName)-1]
        if(strlen(cellID)>0)
            if(!isStringInWave(cellID, cellNames))
                DFREF thisFolder = cellInfoDFR:$(folderName)
                KillDataFolder/Z thisFolder
            endif
        endif
    endfor
end

///////////
/// deleteFoldersFromSeriesNotInList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-07
/// NOTES: 
///////////
function deleteFoldersFromSeriesNotInList()
    DFREF cellInfoDFR = getEventsInfoDF()
    string cellFoldersList = DataFolderList("*", ";", cellInfoDFR)

    variable iFolder = 0, nFolders = itemsInList(cellFoldersList)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    for(iFolder=0; iFolder<nFolders; iFolder++)
        string folderName = StringFromList(iFolder, cellFoldersList)
        string cellID = folderName[1, strlen(folderName)-1]
        if(strlen(cellID)>0)
            DFREF thisFolder = cellInfoDFR:$(folderName)
            Wave/SDFR=thisFolder Events_series

            string seriesFolderList = DataFolderList("!summary", ";", thisFolder)

            variable iSeriesDF, nSeriesDF = itemsInList(seriesFolderList)

            for(iSeriesDF=0; iSeriesDF<nSeriesDF; iSeriesDF++)
                string seriesDF = StringFromList(iSeriesDF, seriesFolderList)
                string seriesName = seriesDF[1, strlen(seriesDF)-1]
                if(!isStringInWave(seriesName, Events_series) && !isStringInWave(seriesName, passive1) && !isStringInWave(seriesName, passive2))
                    DFREF seriesDFR = thisFolder:$seriesDF
                    KillDataFolder/Z seriesDFR
                endif
            endfor
        endif
    endfor
end


///////////
/// deleteWavesFromSeriesNotInSeriesList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-07
/// NOTES: 
///////////
function deleteWavesFromSeriesNotInSeriesList()
    string wavesMatchingCellRegEx = findWavesInDFRByRegEx("([[:digit:]]+)([[:alpha:]])g([[:digit:]]+)s([[:digit:]]+)sw([[:digit:]]+)t.*")

    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID, seriesName
        [cellID, seriesName] = getCellIDAndSeriesFromWaveN(waveN)
        if(strlen(cellID)>0 && strlen(seriesName)>0)
            // if(StringMatch(cellID, "20220115e"))
            //     debugger
            // endif
            DFREF cellDFR = getEventsCellInfoDF(cellID)
            Wave/SDFR=cellDFR Events_series, passive1, passive2
            if(!isStringInWave(seriesName, Events_series) && !isStringInWave(seriesName, passive1) && !isStringInWave(seriesName, passive2))
                // print waveN
                KillWaves/Z $waveN
            endif
        endif
    endfor
end


///////////
/// deleteSDSWavesFromCellsNotInCellList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-07
/// NOTES: 
///////////
function deleteSDSWavesFromCellsNotInCellList()
    string wavesMatchingCellRegEx = findWavesInDFRByRegEx("([[:digit:]]+)([[:alpha:]])_SDS")

    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID = getWaveBasename(waveN)
        if(strlen(cellID)>0)
            if(!isStringInWave(cellID, cellNames))
                // print waveN
                KillWaves/Z $waveN
            endif
        endif
    endfor
end