Structure outputWaves
    Wave/D sumCount, sumFreq, pks, int, der, t50r, fwhm, decay9010, Rinput, RseriesSub, capa, holdingc, confDetect, duration, tauFits, y0Fits, AFits, x0Fits, decayTimes
    Wave/D sumTime
    Wave/T seriesName, groupName, cellName, groupName2, groupName3
    Wave/D eventHisto, eventHistoByFreq
    string analysis_types, variable_names, varFromWave, niceNamesFromWave, waveFromNiceNames, niceNames
    string avgSubsetAnalysisTypes, seriesEventsAnalysisTypes, passiveWaves, allEventsOutputs
endstructure

function fillOutWavesStrings(outputWaves)
    STRUCT outputWaves &outputWaves
    outputWaves.analysis_types = "confDetect;sumCount;sumTime;duration;sumFreq;pks;int;der;t50r;decay9010;fwhm;Rinput;RseriesSub;capa;holdingc;" // have to redimension by force within updateSeriesOutTable function
    outputWaves.analysis_types += "tauFits;y0Fits;AFits;x0Fits;decayTimes;"
    outputWaves.variable_names = "confirmedDetect;numEvents;seriesTime;duration;freq;relPeak;interval;derivative;riseTime;fwhm;Rinput;RseriesSub;capa;holdingc;"
    outputWaves.variable_names += "tau;y0;A;x0;decayTime;decay9010;"
    outputWaves.varFromWave = "confDetect:confirmedDetect;sumCount:numEvents;sumTime:seriesTime;duration:duration;sumFreq:freq;pks:relPeak;int:interval;der:derivative;t50r:riseTime;fwhm:fwhm;Rinput:Rinput;RseriesSub:RseriesSub;capa:capa;holdingc:holdingC;"
    outputWaves.varFromWave += "tauFits:tau;y0Fits:y0;AFits:A;x0Fits:x0;decayTimes:decayTime;decay9010:decay9010;"
    outputWaves.niceNamesFromWave = "sumCount:num of events;sumTime:time;sumFreq:frequency (Hz);pks:relative peak (A);int:interval (s);der:derivative;t50r:rise time (s);fwhm:full width half maximum (s);Rinput:input resistance (MOhm);RseriesSub:series resistance (MOhm);capa:capacitance (pF);holdingc:holdingC (pA);decay9010:decay time from 90% to 10% peak (ms);"
    outputWaves.waveFromNiceNames = "num of events:sumCount;time:sumTime;frequency (Hz):sumFreq;relative peak (A):pks;interval (s):int;derivative:der;rise time (s):t50r;full width half maximum (s):fwhm;input resistance (MOhm):Rinput;series resistance (MOhm):RseriesSub;capacitance (pF):capa;holdingC (pA):holdingc;decay time from 90% to 10% peak (ms):decay9010;"
    outputWaves.niceNames = "num of events;frequency (Hz);relative peak (A);interval (s);derivative;rise time (s);full width half maximum (s);decay time from 90% to 10% peak (ms);input resistance (MOhm);series resistance (MOhm);capacitance (pF);holdingC (pA)"

    outputWaves.avgSubsetAnalysisTypes = "area;der;dtb;fwhm;pks;pk2;ptb;t50r;1090d;" // don't include interval for average subset. Not meaningful
    outputWaves.seriesEventsAnalysisTypes = "ptb;pk2;pks;int;der;area;t50r;1090d;fwhm;"
    outputWaves.allEventsOutputs = "ptb;pk2;pks;int;ptb_backInt;ptb_forInt;avel;der;area;t50r;1090d;fwhm;"

    outputWaves.passiveWaves = "Rinput;RseriesSub;capa;holdingc;"
end

Structure optoOutputWaves
    Wave sumCount, sumFreq, pks, int, der, t50r, fwhm, Rinput, RseriesSub, capa, holdingc, confDetect, duration
    Wave/D sumTime
    Wave redFreq, redAmp, redDur, redNumStims, blueFreq, blueAmp, blueDur, blueNumStims
    Wave redNumEvoked, redNumStimsWithEvoked, blueNumEvoked, blueNumStimsWithEvoked
    Wave/T seriesName, groupName, cellName, pgfLabel
    string analysis_types, variable_names, varFromWave, niceNamesFromWave, waveFromNiceNames, niceNames
endstructure

function fillOptoOutWavesStrings(outputWaves)
    STRUCT optoOutputWaves &outputWaves
    outputWaves.analysis_types = "confDetect;sumCount;sumTime;duration;redFreq;redAmp;redDur;redNumStims;redNumEvoked;redNumStimsWithEvoked;blueFreq;blueAmp;blueDur;blueNumStims;blueNumEvoked;blueNumStimsWithEvoked;sumFreq;pks;int;der;t50r;fwhm;Rinput;RseriesSub;capa;holdingc;" // have to redimension by force within updateOptoOutputTable function
    outputWaves.variable_names = "confirmedDetect;numEvents;seriesTime;duration;redFreq;redAmp;redDur;redNumStims;redNumEvoked;redNumStimsWithEvoked;blueFreq;blueAmp;blueDur;blueNumStims;blueNumEvoked;blueNumStimsWithEvoked;freq;relPeak;interval;derivative;riseTime;fwhm;Rinput;RseriesSub;capa;holdingc;"
    outputWaves.varFromWave = "confDetect:confirmedDetect;sumCount:numEvents;sumTime:seriesTime;duration:duration;redFreq:redFreq;redAmp:redAmp;redDur:redDur;redNumStims:redNumStims;redNumEvoked:redNumEvoked;redNumStimsWithEvoked:redNumStimsWithEvoked;blueFreq:blueFreq;blueAmp:blueAmp;blueDur:blueDur;blueNumStims:blueNumStims;blueNumEvoked:blueNumEvoked;blueNumStimsWithEvoked:blueNumStimsWithEvoked;sumFreq:freq;pks:relPeak;int:interval;der:derivative;t50r:riseTime;fwhm:fwhm;Rinput:Rinput;RseriesSub:RseriesSub;capa:capa;holdingc:holdingC;"
    outputWaves.niceNamesFromWave = "sumCount:num of events;sumTime:time;redFreq:red frequency (Hz);redAmp:red amplitude (A);redDur:red pulse duration (s);redNumStims:red # stimulations;redNumEvoked:red # evoked events;redNumStimsWithEvoked:red # stims w/ evoked events;blueFreq:blue frequency (Hz);blueAmp:blue amplitude (A);blueDur:blue pulse duration (ms);blueNumStims:blue # stimulations;blueNumEvoked:blue # evoked events;blueNumStimsWithEvoked:blue # stims w/ evoked events;sumFreq:frequency (Hz);pks:relative peak (A);int:interval (s);der:derivative;t50r:rise time (s);fwhm:full width half maximum (s);Rinput:input resistance (MOhm);RseriesSub:series resistance (MOhm);capa:capacitance (pF);holdingc:holdingC (pA)"
    outputWaves.waveFromNiceNames = "num of events:sumCount;time:sumTime;red frequency (Hz):redFreq;red amplitude (A):redAmp;red pulse duration (s):redDur;red # stimulations:redNumStims;red # evoked events:redNumEvoked;red # stims w/ evoked events:redNumStimsWithEvoked;blue frequency (Hz):blueFreq;blue amplitude (A):blueAmp;blue pulse duration (ms):blueDur;blue # stimulations:blueNumStims;blue # evoked events:blueNumEvoked;blue # stims w/ evoked events:blueNumStimsWithEvoked;frequency (Hz):sumFreq;relative peak (A):pks;interval (s):int;derivative:der;rise time (s):t50r;full width half maximum (s):fwhm;input resistance (MOhm):Rinput;series resistance (MOhm):RseriesSub;capacitance (pF):capa;holdingC (pA):holdingc"
    outputWaves.niceNames = "num of events;frequency (Hz);red frequency (Hz);red amplitude (A);red pulse duration (s);red # stimulations;red # evoked events;red # stims w/ evoked events;blue frequency (Hz);blue amplitude (A);blue pulse duration (ms);blue # stimulations;blue # evoked events;blue # stims w/ evoked events;relative peak (A);interval (s);derivative;rise time (s);full width half maximum (s);input resistance (MOhm);series resistance (MOhm);capacitance (pF);holdingC (pA)"
end

Structure burstOutputWaves
    Wave/D bww, bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags
    Wave/T cellName, groupName, groupName2, groupName3
    string analysis_types, variable_names, varFromWave, niceNamesFromWave, waveFromNiceNames, niceNames
endstructure

function fillBurstOutWavesStrings(outputWaves)
    STRUCT burstOutputWaves &outputWaves
    outputWaves.analysis_types = "bww;bn;mbd;spb;bf;ssn;ssf;tf;inter;intra;bFlags;ssFlags;" // have to redimension by force within updateBurstOutTable function
    outputWaves.variable_names = "bn;mbd;spb;bf;ssn;ssf;tf;inter;intra;bFlags;ssFlags;"
    outputWaves.varFromWave = "bn:bn;mbd:mbd;spb:spb;bf:bf;ssn:ssn;ssf:ssf;tf:tf;inter:inter;intra:intra;bFlags:bFlags;ssFlags:ssFlags;"
    outputWaves.niceNamesFromWave = "bww:burst win (s);bn:# bursts;mbd:mean burst dur (s);spb:spikes per burst;bf:burst freq (Hz);"
    outputWaves.niceNamesFromWave += "ssn:# single spikes;ssf: single spike freq (Hz);tf:total freq (Hz);"
    outputWaves.niceNamesFromWave += "inter:interburst interval (s);intra:intraburst interval (s);bFlags:burst flags;ssFlags:single spike flags;"
    
    outputWaves.waveFromNiceNames = "burst win (s):bww;# bursts:bn;mean burst dur (s):mbd;spikes per burst:spb;burst freq (Hz):bf;"
    outputWaves.waveFromNiceNames += "# single spikes:ssn; single spike freq (Hz):ssf;total freq (Hz):tf;"
    outputWaves.waveFromNiceNames += "interburst interval (s):inter;intraburst interval (s):intra;burst flags:bFlags; single spike flags:ssFlags;"
    
    outputWaves.niceNames = "burst win (s);# bursts;mean burst dur (s);spikes per burst;burst freq (Hz);"
    outputWaves.niceNames += "# single spike; single spike freq (Hz);total freq (Hz);"
    outputWaves.niceNames += "interburst interval (s);intraburst interval (s);burst flags; single spike flags;"
end

Structure burstVals
    NVAR bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags
endstructure

Structure passiveProps
    NVAR Rinput, Rseries, RseriesSub, capa, holdingc, Tstart_rel, Tstart
    string propNames
EndStructure

function fillPassiveProps(passiveProps)
    STRUCT passiveProps &passiveProps
    passiveProps.propNames = "Rinput;Rseries;RseriesSub;capa;holdingc;Tstart_rel;Tstart"
end


STRUCTURE eventAvgVars
    NVAR numEvents, seriesTime, duration, freq, relPeak, interval, derivative, riseTime, fwhm, decay9010
    string avgVarNames, waveFromVar, varFromWave, analysis_types
EndStructure

function fillAvgVarNames(eventAvgsStruct)
    Struct eventAvgVars &eventAvgsStruct
    eventAvgsStruct.avgVarNames = "numEvents;seriesTime;duration;freq;relPeak;interval;derivative;riseTime;fwhm;decay9010;"
    eventAvgsStruct.waveFromVar = "relPeak:pks;interval:int;derivative:der;riseTime:t50r;fwhm:fwhm;decay9010:1090d;"
    eventAvgsStruct.varFromWave = "pks:relPeak;int:interval;der:derivative;t50r:riseTime;fwhm:fwhm;1090d:decay9010;"
    eventAvgsStruct.analysis_types = "relPeak;interval;derivative;riseTime;fwhm;decay9010;"
end

STRUCTURE cellSummaryInfo
    Wave/D combinedSeriesW, passivesW, numEventsW, freqsW, durationsW
    Wave/D RinputsW, RSeriesSubsW, capasW, holdingCsW
    Wave/D cellAvgEvent, cellNAvgEvent
    Wave/D pksW, intW, derW, t50rW, fwhmW, decay9010W // for the averages from each series
    Wave/D peaks_concat, int_concat, der_concat, t50r_concat, fwhm_concat, decay9010_concat // to calculate average for cell
    Wave/D peaks_dist, int_dist, der_dist, t50r_dist, fwhm_dist, decay9010_dist // to calculate distribution for cell 
    Wave/D startTimesW, passStartTimesW
    string numericWaves, doubleWaves, numericVarNames, eventSeriesWaves, passiveSeriesWaves, eventAvgWaves, avgByEventWaves, listWaveNames, niceNames, avgByEventsExts, tauFitDisplay
    string probDistribution
    string displayListNameWaves, analysisTypes, distTypes, passiveVarNames
    string groupDisplayListNameWaves, groupAvgWaves
EndStructure

function fillStringsinCellSummaryInfo(cellSummaryInfo)
    Struct cellSummaryInfo &cellSummaryInfo
    cellSummaryInfo.numericWaves = "combinedSeriesW;passivesW;numEventsW;freqsW;durationsW;RinputsW;RSeriesSubsW;capasW;holdingCsW;startTimesW;passStartTimesW;pksW;intW;derW;t50rW;fwhmW;decay9010W;"
    cellSummaryInfo.eventSeriesWaves = "freqsW;durationsW;numEventsW;startTimesW;pksW;intW;derW;t50rW;fwhmW;decay9010W;"
    cellSummaryInfo.passiveSeriesWaves = "RinputsW;RSeriesSubsW;capasW;holdingCsW;passStartTimesW;"
    
    cellSummaryInfo.passiveVarNames = "Rinput;RseriesSub;capa;holdingc;"
    cellSummaryInfo.numericVarNames = "numEventsW:numEvents;freqsW:freq;durationsW:duration;"
    cellSummaryInfo.numericVarNames += "RinputsW:Rinput;RSeriesSubsW:RseriesSub;capasW:capa;holdingCsW:holdingc;"
    cellSummaryInfo.numericVarNames += "startTimesW:seriesTime;passStartTimesW:tStart;"
    cellSummaryInfo.numericVarNames += "pksW:relPeak;intW:interval;derW:derivative;t50rW:riseTime;fwhmW:fwhm;decay9010W:decay9010;"
    
    cellSummaryInfo.eventAvgWaves = "cellAvgEvent;cellNAvgEvent;"

    cellSummaryInfo.tauFitDisplay = "tauFitDisplay;"
    
    cellSummaryInfo.listWaveNames = cellSummaryInfo.eventSeriesWaves + cellSummaryInfo.passiveSeriesWaves + cellSummaryInfo.eventAvgWaves + cellSummaryInfo.tauFitDisplay
    
    cellSummaryInfo.niceNames = "numEventsW:num events;"
    cellSummaryInfo.niceNames += "freqsW:frequency (Hz);"
    cellSummaryInfo.niceNames += "durationsW:duration (s);"
    cellSummaryInfo.niceNames += "startTimesW:start time;"
    cellSummaryInfo.niceNames += "RinputsW:input resistance (MOhm);"
    cellSummaryInfo.niceNames += "RSeriesSubsW:series resistance (MOhm);"
    cellSummaryInfo.niceNames += "capasW:capacitance (pF);"
    cellSummaryInfo.niceNames += "holdingCsW:holding current (pA);"
    cellSummaryInfo.niceNames += "cellAvgEvent:average event;"
    cellSummaryInfo.niceNames += "cellNAvgEvent:norm. average event;"
    cellSummaryInfo.niceNames += "tauFitDisplay:show tau fit;"
    cellSummaryInfo.niceNames += "pksW:relative peak (pA);"
    cellSummaryInfo.niceNames += "intW:interval (s);"
    cellSummaryInfo.niceNames += "derW:derivative (pA/ms);"
    cellSummaryInfo.niceNames += "t50rW:rise time (ms);"
    cellSummaryInfo.niceNames += "fwhmW:full width half max (ms);"
    cellSummaryInfo.niceNames += "decay9010W:decay time from 90% to 10% peak (ms);"
    cellSummaryInfo.niceNames += "dist:distribution plot;"
    cellSummaryInfo.niceNames += "dist_dec:decimated dist plot;"
    cellSummaryInfo.niceNames += "prop:event property table;"
    cellSummaryInfo.niceNames += "allProps:all parameters;"
    cellSummaryInfo.niceNames += "seriesByTime:plot series avg v time;"
    cellSummaryInfo.niceNames += "smartConcDisplay:plot smart concatenation;"
    cellSummaryInfo.niceNames += "peaks:relative peak (pA);"
    cellSummaryInfo.niceNames += "int:interval (s);"
    cellSummaryInfo.niceNames += "der:derivative (pA/ms);"
    cellSummaryInfo.niceNames += "t50r:rise time (ms);"
    cellSummaryInfo.niceNames += "fwhm:full width half max (ms);"
    cellSummaryInfo.niceNames += "decay9010:decay time from 90% to 10% peak (ms);"
    cellSummaryInfo.niceNames += "groupAvg:group avg events;"
    cellSummaryInfo.niceNames += "groupAvgEvent:avg by event;"
    cellSummaryInfo.niceNames += "groupNAvgEvent:norm avg by event;"
    cellSummaryInfo.niceNames += "groupAvgEvent_byCell:avg by cell;"
    cellSummaryInfo.niceNames += "groupNAvgEvent_byCell:norm avg by cell;"

    cellSummaryInfo.avgByEventWaves = "peaks;der;t50r;fwhm;int;decay9010;"
    cellSummaryInfo.avgByEventsExts = "peaks:pks;int:int;der:der;t50r:t50r;fwhm:fwhm;decay9010:1090d;"

    cellSummaryInfo.probDistribution = "peaks:bySign;"
    cellSummaryInfo.probDistribution += "int:pos;"
    cellSummaryInfo.probDistribution += "der:bySign;"
    cellSummaryInfo.probDistribution += "t50r:pos;"
    cellSummaryInfo.probDistribution += "fwhm:pos;"
    cellSummaryInfo.probDistribution += "decay9010:pos;"

    cellSummaryInfo.displayListNameWaves = "smartConcDisplay;cellAvgEvent;cellNAvgEvent;dist;seriesByTime;tauFitDisplay;"
    CellSummaryInfo.analysisTypes = cellSummaryInfo.eventSeriesWaves + cellSummaryInfo.passiveSeriesWaves
    
    CellSummaryInfo.groupDisplayListNameWaves = "groupAvg;dist;dist_dec;prop;"
    cellSummaryInfo.groupAvgWaves = "groupAvgEvent;groupNAvgEvent;groupAvgEvent_byCell;groupNAvgEvent_byCell;"
end

STRUCTURE seriesSummaryInfo
    string listWaveNames, niceNames, analysisTypes, appendixFromNiceName
EndStructure

function fillStringsinseriesSummaryInfo(seriesSummaryInfo)
    Struct seriesSummaryInfo &seriesSummaryInfo
    seriesSummaryInfo.listWaveNames = "ave;nave;dist"
    seriesSummaryInfo.analysisTypes = "pks;der;int;fwhm;t50r;area;1090d"
    seriesSummaryInfo.niceNames = "ave:average event;"
    seriesSummaryInfo.niceNames += "nave:norm average event;"
    seriesSummaryInfo.niceNames += "dist:distribution plot;"
    seriesSummaryInfo.niceNames += "pks:rel. peak (pA);"
    seriesSummaryInfo.niceNames += "int:interval (s);"
    seriesSummaryInfo.niceNames += "der:derivative (pA/ms);"
    seriesSummaryInfo.niceNames += "t50r:rise time (ms);"
    seriesSummaryInfo.niceNames += "fwhm:full width half max (ms);"
    seriesSummaryInfo.niceNames += "area:area (pA*ms);"
    seriesSummaryInfo.niceNames += "1090d:decay time from 90% to 10% peak (ms);"
    seriesSummaryInfo.appendixFromNiceName = "average event:ave;"
    seriesSummaryInfo.appendixFromNiceName += "norm average event:nave;"
    seriesSummaryInfo.appendixFromNiceName += "distribution plot:dist;"
    seriesSummaryInfo.appendixFromNiceName += "rel. peak (pA):pks;"
    seriesSummaryInfo.appendixFromNiceName += "interval (s):int;"
    seriesSummaryInfo.appendixFromNiceName += "derivative (pA/ms):der;"
    seriesSummaryInfo.appendixFromNiceName += "rise time (ms):t50r;"
    seriesSummaryInfo.appendixFromNiceName += "full width half max (ms):fwhm;"
    seriesSummaryInfo.appendixFromNiceName += "area (pA*ms):area;"
    seriesSummaryInfo.appendixFromNiceName += "decay time from 90% to 10% peak (ms):1090d;"
end


STRUCTURE derivDetectStruct
    string waveTypes, unitTypes, probDistribution, typeToVar, varsOnly, riseFallWaves

    NVAR peak, relPeak, peak_time, dPeak, dPeak_time, this_area, t50rise, t50rise_time
    NVAR diffRiseSmooth, decay1090, fwhm, baseline, base_start, base_end

    NVAR p50, p10, p90, fall50_time, rise50_time, fall10_time, fall90_time

    variable testVar
EndStructure

function fillDerivDetectStruct(derivDetectStruct)
    Struct derivDetectStruct &derivDetectStruct
    // don't have interval included. Want to make and control this elsewhere
    derivDetectStruct.waveTypes = "absolute peak;relative peak;peak time;derivative;derivative time;area;risetime;t50riseAtTime;10to90decay;fwhm;"
    derivDetectStruct.waveTypes += "baseline;baseline start;baseline end;difference rise smoothed;"
    
    derivDetectStruct.waveTypes += "p50;p10;p90;fall50_time;rise50_time;fall10_time;fall90_time;" // save these, too, so that we can plot them with cerebro/wave intrinsic
    // derivDetectStruct.varsOnly = "p50;p10;p90;fall50_time;rise50_time;fall10_time;fall90_time;"
    derivDetectStruct.riseFallWaves = "p50;p10;p90;fall50_time;rise50_time;fall10_time;fall90_time;"

    derivDetectStruct.unitTypes = "absolute peak:signal;"
    derivDetectStruct.unitTypes += "relative peak:signal;"
    derivDetectStruct.unitTypes += "peak time:time;"
    derivDetectStruct.unitTypes += "derivative:deriv;"
    derivDetectStruct.unitTypes += "derivative time:time;"
    derivDetectStruct.unitTypes += "area:area;"
    derivDetectStruct.unitTypes += "risetime:time;"
    derivDetectStruct.unitTypes += "t50riseAtTime:time;"
    derivDetectStruct.unitTypes += "10to90decay:time;"
    derivDetectStruct.unitTypes += "fwhm:time;"
    derivDetectStruct.unitTypes += "baseline:signal;"
    derivDetectStruct.unitTypes += "baseline start:time;"
    derivDetectStruct.unitTypes += "baseline end:time;"
    derivDetectStruct.unitTypes += "difference rise smoothed:time;"
    derivDetectStruct.unitTypes += "p50:signal;"
    derivDetectStruct.unitTypes += "p10:signal;"
    derivDetectStruct.unitTypes += "p90:signal;"
    derivDetectStruct.unitTypes += "fall50_time:time;"
    derivDetectStruct.unitTypes += "fall10_time:time;"
    derivDetectStruct.unitTypes += "fall90_time:time;"
    derivDetectStruct.unitTypes += "rise50_time:time;"

    derivDetectStruct.probDistribution = "absolute peak:NA;"
    derivDetectStruct.probDistribution += "relative peak:bySign;"
    derivDetectStruct.probDistribution += "peak time:NA;"
    derivDetectStruct.probDistribution += "derivative:bySign;"
    derivDetectStruct.probDistribution += "derivative time:NA;"
    derivDetectStruct.probDistribution += "area:bySign;"
    derivDetectStruct.probDistribution += "risetime:pos;" // always sort positive by time
    derivDetectStruct.probDistribution += "t50riseAtTime:NA;"
    derivDetectStruct.probDistribution += "10to90decay:pos;" // always sort positive by time
    derivDetectStruct.probDistribution += "fwhm:pos;" // always sort positive by time
    derivDetectStruct.probDistribution += "baseline:bySign;"
    derivDetectStruct.probDistribution += "baseline start:NA;"
    derivDetectStruct.probDistribution += "baseline end:NA;"
    derivDetectStruct.probDistribution += "difference rise smoothed:NA;"
    derivDetectStruct.probDistribution += "p50:NA;"
    derivDetectStruct.probDistribution += "p10:NA;"
    derivDetectStruct.probDistribution += "p90:NA;"
    derivDetectStruct.probDistribution += "fall50_time:NA;"
    derivDetectStruct.probDistribution += "fall10_time:NA;"
    derivDetectStruct.probDistribution += "fall90_time:NA;"
    derivDetectStruct.probDistribution += "rise50_time:NA;"
    derivDetectStruct.probDistribution += "interval:pos;"

    derivDetectStruct.typeToVar = "absolute peak:peak;"
    derivDetectStruct.typeToVar += "relative peak:relPeak;"
    derivDetectStruct.typeToVar += "peak time:peak_time;"
    derivDetectStruct.typeToVar += "derivative:dPeak;"
    derivDetectStruct.typeToVar += "derivative time:dPeak_time;"
    derivDetectStruct.typeToVar += "area:this_area;"
    derivDetectStruct.typeToVar += "risetime:t50rise;"
    derivDetectStruct.typeToVar += "t50riseAtTime:t50rise_time;"
    derivDetectStruct.typeToVar += "10to90decay:decay1090;"
    derivDetectStruct.typeToVar += "fwhm:fwhm;"
    derivDetectStruct.typeToVar += "baseline:baseline;"
    derivDetectStruct.typeToVar += "baseline start:base_start;"
    derivDetectStruct.typeToVar += "baseline end:base_end;"
    derivDetectStruct.typeToVar += "difference rise smoothed:diffRiseSmooth;"
    derivDetectStruct.typeToVar += "p50:p50;"
    derivDetectStruct.typeToVar += "p10:p10;"
    derivDetectStruct.typeToVar += "p90:p90;"
    derivDetectStruct.typeToVar += "fall50_time:fall50_time;"
    derivDetectStruct.typeToVar += "fall10_time:fall10_time;"
    derivDetectStruct.typeToVar += "fall90_time:fall90_time;"
    derivDetectStruct.typeToVar += "rise50_time:rise50_time;"
end

///////////
/// returnUnitType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: Returns the units string for a given unitType
///////////
function/S returnUnitType(wave rawData, string unitType)
    string unitString = ""

    if(!WaveExists(rawData))
        print "returnUnitType: rawData wave doesn't exit"
        return unitString
    endif

    string info = waveInfo(rawData, 0)
    string timeunits = stringbykey("XUNITS", info)
    string signalunits = stringbykey("DUNITS", info)

    
    strswitch (unitType)
        case "time":
            unitString = timeunits
            break
        case "signal":
            unitString = signalunits
            break
        case "area":
            unitString = signalunits + "*" + timeunits
            break
        case "deriv": 
            unitString = signalunits + "/" + timeunits
            break
        default:
            
            break
    endswitch

    return unitString
end

// note that StructPut can't put NVARs, they have to be variables... so
// to save the panel parameters at the point of analysis, need to 
// copy everything over. This is annoying and kind of pointless, but
// I think it's better than trying to save all of the panel parameters as individual variables
// for each cell
structure tauPanelStruct
    NVAR doSmoothing, doNorm, highPercPeak, lowPercPeak, allowY0vary, allowTauVary
    NVAR allowAvary, y0, tau, A, useX0constant, X0constant, useFitName, makeGraph, extrapolate, shiftPeakToZero
    SVAR fitWaveN

    variable v_doSmoothing, v_doNorm, v_highPercPeak, v_lowPercPeak, v_allowY0vary, v_allowTauVary
    variable v_allowAvary, v_y0, v_tau, v_A, v_useX0constant, v_X0constant, v_useFitName, v_makeGraph, v_extrapolate, v_shiftPeakToZero
endstructure

structure tauFitVarsStruct
    NVAR tau, y0, A, X0, decayTime
    string varNames
endstructure

///////////
/// fillTauFitVarsStruct
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: 
///////////
function fillTauFitVarsStruct(fitStruct)
    Struct tauFitVarsStruct &fitStruct

    fitStruct.varNames = "tau;y0;A;X0;decayTime;"
end

///////////
/// copyTauPanelParams
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: 
///////////
function copyTauPanelParams(tp[, tauDF])
    DFREF tauDF
    Struct tauPanelStruct &tp
    StructFill/AC=1/SDFR=tauDF tp

    if(paramIsDefault(tauDF))
        tauDF = root:tauPanel
    endif
    
    tp.v_doSmoothing = tp.doSmoothing
    tp.v_doNorm = tp.doNorm
    tp.v_highPercPeak = tp.highPercPeak
    tp.v_lowPercPeak = tp.lowPercPeak
    tp.v_allowY0vary = tp.allowY0vary
    tp.v_allowTauVary = tp.allowTauVary
    tp.v_allowAvary = tp.allowAvary
    tp.v_tau = tp.tau
    tp.v_A = tp.A
    tp.v_useX0constant = tp.useX0constant
    tp.v_x0constant = tp.x0constant
    tp.v_useFitName = tp.useFitName
    tp.v_makeGraph = tp.makeGraph
    tp.v_extrapolate = tp.extrapolate
    tp.v_shiftPeakToZero = tp.shiftPeakToZero
end




