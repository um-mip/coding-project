///////////
/// getTypeStorageDF
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: For the parameter indicated
/// use the panel information to decide on the appropriate type of
/// storage data folder
///     - "all"
///     - "avg"
///     - "interval"
///     - "limited"
/// avgType is whether or not the avgList should use all available ("avg")
/// for avg or the limited average subset
///////////
function/S getTypeStorageDF(string param[, string outType, string avgType])
    string dfrType

    string allList = "", avgList = "", intList = ""
    strswitch (outType)
        case "seriesSummaryTable":
            allList = "sumCount;sumTime;sumFreq;confDetect;duration;"
            intList = "int;"
            avgList = "pks;der;t50r;fwhm;decay9010;"
            avgList += "tauFits;y0Fits;AFits;x0Fits;decayTimes;"
            if(paramIsDefault(avgType))
                avgType = "avg"
            endif
            
            break
        case "seriesAvgDisplay":
            avgList = "ave;nave;"
            if(paramIsDefault(avgType))
                avgType = "avg"
            endif
            break
        case "seriesDistDisplay":
            avgList = "pks;der;fwhm;t50r;area;1090d;"
            intList = "int;"
            if(paramIsDefault(avgType))
                avgType = "avg"
            endif
            // print "param", param
            break
        case "cellAvgDisplay":
            avgList = "ave;nave;cellAvgEvent;cellNAvgEvent;groupAvgEvent;groupNAvgEvent;groupAvgEvent_byCell;groupNAvgEvent_byCell;"
            if(paramIsDefault(avgType))
                avgType = "avg"
            endif
            break
        case "cellDisplay":
            avgList = "peaks;der;t50r;fwhm;decay9010;"
            avgList += "peaksW;derW;t50rW;fwhmW;decay9010W;"
            avgList += "pks;decayTimes;"
            avgList += "tauFits;y0Fits;AFits;x0Fits;decayTimes;"
            avgList += "tauFitDisplay;"
            avgList += "pksW;"
            avgList += "aveWave;nAveWave;"
            intList = "int;interval;intW;" 
            STRUCT cellSummaryInfo cellSum
            fillStringsinCellSummaryInfo(cellSum)
            allList = cellSum.passiveSeriesWaves 
            allList += cellSum.passiveVarNames 
            allList += "sumCount;sumTime;sumFreq;confDetect;duration;"
            allList += "freqsW;durationsW;numEventsW;"
            if(paramIsDefault(avgType))
                avgType = "avg"
            endif
            break
        case "cellHistoGraph":
            allList = "eventHisto;"
            break
        default:
            allList = "sumCount;sumTime;sumFreq;int;"
            avgList = "pks;der;t50r;fwhm;decay9010;"
            break
    endswitch

    if(isInList(param, allList))
        dfrType = "all"
        return dfrType
    endif

    if(isInList(param, avgList))
        strswitch (avgType)
            case "avg":
                dfrType = "avg"
                break
            case "limited":
                dfrType = "limited"
                break
            default:
                dfrType = "limited"
                break
        endswitch
        return dfrType
    endif

    if(isInList(param, intList))
        dfrType = "interval"
        return dfrType
    endif
    
    print "getTypeStorageDF", outType, param, "nothing set yet, defaulting to avg"
    dfrType = "avg"
    return dfrType
end

///////////
/// isInList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function isInList(string stringVar, string myList)
    variable isInList = strlen(ListMatch(myList, stringVar)) > 0
    return isInList
end


///////////
/// returnSeriesEventsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: Returns the DFR for the location of waves/events based on the subset (subType) of the data
/// If subType is "specify" that means a different location is being used for different parameters
/// The series event waves for the "all" subset are stored in the root folder for historical reasons with detection
/// but the summary averages for these waves for the series are stored within the series specific folder
/// so if trying to get the summary information, eventsOrSum should be set to "sum"
/// outType specifies which type of output it is (likely the name of a graph or table), which might have different
/// decision making about the subType that should be used for each parameter
///////////
function/DF returnSeriesEventsDFR_bySubType(string cellName, string seriesName, string subType[, string param, string eventsOrSum, string outType])
    if(paramIsDefault(eventsOrSum))
        eventsOrSum = "events"
    endif

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    string passList = cellSum.passiveSeriesWaves + cellSum.passiveVarNames

    // If this is a passive parameter, always use all
    if(!paramIsDefault(param) && isInList(param, passList))
        subType = "all"
    endif
    
    DFREF seriesDFR
    strswitch (subType)
        case "all":
            if(stringMatch(eventsOrSum, "events"))
                seriesDFR = getEventsdetectWavesDF(cellName)
            else
                seriesDFR = getSeriesDF(cellName, seriesName)
            endif
            break
        case "avg":
            seriesDFR = getSeriesEventsForAvgDF(cellName, seriesName)
            break
        case "limited":
            seriesDFR = getSeriesEventsForAvgLimitedDF(cellName, seriesName)
            break
        case "interval":
            seriesDFR = getSeriesEventsForIntervalDF(cellName, seriesName)
            break
        case "specify":
            string specSubType
            if(paramIsDefault(param))
                print "returnSeriesEventsDFR_bySubType: Did not specify param, defaulting to average"
                specSubType = "avg"
            else
                if(paramIsDefault(outType))
                    specSubType = getTypeStorageDF(param)
                else
                    specSubType = getTypeStorageDF(param, outType = outType)
                endif
            endif
            seriesDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, specSubType, eventsOrSum = eventsOrSum)
            break
        default:
            seriesDFR = getEventsdetectWavesDF(cellName)
            
            break
    endswitch
    return seriesDFR
end

///////////
/// returnCellEventsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: Returns the DFR for the location of waves/events based on the subset (subType) of the data
/// If subType is "specify" that means a different location is being used for different parameters
/// The series event waves for the "all" subset are stored in the root folder for historical reasons with detection
/// but the summary averages for these waves for the series are stored within the series specific folder
/// so if trying to get the summary information, eventsOrSum should be set to "sum"
/// outType specifies which type of output it is (likely the name of a graph or table), which might have different
/// decision making about the subType that should be used for each parameter
///////////
function/DF returnCellEventsDFR_bySubType(string cellName, string subType[, string param, string eventsOrSum, string outType])
    if(paramIsDefault(eventsOrSum)) // probably don't even need this
        eventsOrSum = "sum"
    endif

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    string passList = cellSum.passiveSeriesWaves + cellSum.passiveVarNames

    // If this is a passive parameter, always use all
    if(!paramIsDefault(param) && isInList(param, passList))
        subType = "all"
    endif
    
    DFREF cellDFR
    strswitch (subType)
        case "all":
            cellDFR = getCellSummaryDF(cellName)
            break
        case "avg":
            cellDFR = getCellAvgSubDF(cellName)
            break
        case "limited":
            cellDFR = getCellAvgSubLimitedDF(cellName)
            break
        case "interval":
            cellDFR = getCellIntSubDF(cellName)
            break
        case "specify":
            string specSubType
            if(paramIsDefault(param))
                print "returnCellEventsDFR_bySubType: Did not specify param, defaulting to average"
                specSubType = "avg"
            else
                if(paramIsDefault(outType))
                    specSubType = getTypeStorageDF(param)
                else
                    specSubType = getTypeStorageDF(param, outType = outType)
                endif
            endif
            cellDFR = returnCellEventsDFR_bySubType(cellName, specSubType, eventsOrSum = eventsOrSum)
            break
        default:
            cellDFR = getCellSummaryDF(cellName)
            
            break
    endswitch
    return cellDFR
end

///////////
/// returnGroupDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: Returns the DFR for the location of waves/events based on the subset (subType) of the data
/// If subType is "specify" that means a different location is being used for different parameters
/// outType specifies which type of output it is (likely the name of a graph or table), which might have different
/// decision making about the subType that should be used for each parameter
///
/// this makes the folder if it doesn't yet exist
///////////
function/DF returnGroupDFR_bySubType(string groupName, string subType[, string param, string outType])
    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    string passList = cellSum.passiveSeriesWaves + cellSum.passiveVarNames

    // If this is a passive parameter, always use all
    if(!paramIsDefault(param) && isInList(param, passList))
        subType = "all"
    endif

    variable useOverallGroup = 0
    if(stringMatch(groupName, "overallGroupInfo"))
        useOverallGroup = 1
    endif
    
    DFREF groupDFR
    strswitch (subType)
        case "all":
            if(useOverallGroup)
                groupDFR = getGroupsInfoDF()
                if(dataFolderRefStatus(groupDFR)==0)
                    createGroupsInfoDF()
                    groupDFR = getGroupsInfoDF()
                endif
                break
            endif

            groupDFR = getGroupDF(groupName)
            if(dataFolderRefStatus(groupDFR)==0)
                createGroupDF(groupName)
                groupDFR = getGroupDF(groupName)
            endif
            break
        case "avg":
            if(useOverallGroup)
                groupDFR = getGroupInfoForAvgDF()
                if(dataFolderRefStatus(groupDFR)==0)
                    createGroupInfoForAvgDF()
                    groupDFR = getGroupInfoForAvgDF()
                endif
                break
            endif

            groupDFR = getGroupForAvgDF(groupName)
            if(dataFolderRefStatus(groupDFR)==0)
                createGroupForAvgDF(groupName)
                groupDFR = getGroupForAvgDF(groupName)
            endif
            break
        case "limited":
            if(useOverallGroup)
                groupDFR = getGroupInfoForAvgLimitedDF()
                if(dataFolderRefStatus(groupDFR)==0)
                    createGroupInfoForAvgLimitedDF()
                    groupDFR = getGroupInfoForAvgLimitedDF()
                endif
                break
            endif

            groupDFR = getGroupForAvgLimitedDF(groupName)
            if(dataFolderRefStatus(groupDFR)==0)
                createGroupForAvgLimitedDF(groupName)
                groupDFR = getGroupForAvgLimitedDF(groupName)
            endif
            break
        case "interval":
            if(useOverallGroup)
                groupDFR = getGroupInfoForIntervalDF()
                if(dataFolderRefStatus(groupDFR)==0)
                    createGroupInfoForIntervalDF()
                    groupDFR = getGroupInfoForIntervalDF()
                endif
                break
                
            endif
            groupDFR = getGroupForIntervalDF(groupName)
            if(dataFolderRefStatus(groupDFR)==0)
                createGroupForIntervalDF(groupName)
                groupDFR = getGroupForIntervalDF(groupName)
            endif
            break
        case "specify":
            string specSubType
            if(paramIsDefault(param))
                print "returnGroupDFR_bySubType: Did not specify param, defaulting to average"
                specSubType = "avg"
            else
                if(paramIsDefault(outType))
                    specSubType = getTypeStorageDF(param)
                else
                    specSubType = getTypeStorageDF(param, outType = outType)
                endif
            endif
            groupDFR = returnGroupDFR_bySubType(groupName, specSubType)
            break
        default:
            if(useOverallGroup)
                groupDFR = getGroupsInfoDF()
                if(dataFolderRefStatus(groupDFR)==0)
                    createGroupsInfoDF()
                    groupDFR = getGroupsInfoDF()
                endif
                break
            endif

            groupDFR = getGroupDF(groupName)
            if(dataFolderRefStatus(groupDFR)==0)
                createGroupDF(groupName)
                groupDFR = getGroupDF(groupName)
            endif
            
            break
    endswitch
    return groupDFR
end

///////////
/// makeCellRegionsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
///////////
function/DF makeCellRegionsDFR_bySubType(string cellName, string subType[, string regionName])
    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1
    endif

    DFREF cellSumDFR = getCellSummaryDF(cellName)
    if(dataFolderRefStatus(cellSumDFR)==0)
        print "makeCellRegionsDFR_bySubtype: no cell summary folder yet for cell. Quitting\nHave you concatenated?"
        abort
    endif
    
    strswitch (subType)
        case "all":
            if(!specRegion)
                createCellRegionsDF(cellName)
            else
                createCellSpecRegionDF(cellName, regionName)
            endif
            break
        case "avg":
            if(!specRegion)
                createCellRegionsDF_avg(cellName)
            else
                createCellSpecRegionDF_avg(cellName, regionName)
            endif
            break
        case "limited":
            if(!specRegion)
                createCellRegionsDF_limitedAvg(cellName)
            else
                createCellSpecRegionDF_limitedAvg(cellName, regionName)
            endif
            break
        case "interval":
            if(!specRegion)
                createCellRegionsDF_interval(cellName)
            else
                createCellSpecRegionDF_interval(cellName, regionName)
            endif
            break
        default:
            if(!specRegion)
                createCellRegionsDF(cellName)
            else
                createCellSpecRegionDF(cellName, regionName)
            endif            
            break
    endswitch

    DFREF createdDFR
    if(!specRegion)
        createdDFR = returnCellRegionsDFR_bySubType(cellName, subType)
    else
        createdDFR = returnCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)
    endif
    return createdDFR
end

///////////
/// returnCellRegionsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Returns the DFR for the location of waves/events based on the subset (subType) of the data
/// If subType is "specify" that means a different location is being used for different parameters
/// The series event waves for the "all" subset are stored in the root folder for historical reasons with detection
/// but the summary averages for these waves for the series are stored within the series specific folder
/// so if trying to get the summary information, eventsOrSum should be set to "sum"
/// outType specifies which type of output it is (likely the name of a graph or table), which might have different
/// decision making about the subType that should be used for each parameter
///////////
function/DF returnCellRegionsDFR_bySubType(string cellName, string subType[, string param, string outType, string regionName])
    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    string passList = cellSum.passiveSeriesWaves + cellSum.passiveVarNames

    // If this is a passive parameter, always use all
    if(!paramIsDefault(param) && isInList(param, passList))
        subType = "all"
    endif

    DFREF cellDFR

    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1

        if(stringmatch(regionName, "Full duration"))
            if(!paramIsDefault(param) && !paramIsDefault(outType))
                cellDFR = returnCellEventsDFR_bySubType(cellName, subType, param = param, outType = outType)
            else
                if(!paramIsDefault(param))
                    cellDFR = returnCellEventsDFR_bySubType(cellName, subType, param = param)
                else
                    if(!paramIsDefault(outType))
                        cellDFR = returnCellEventsDFR_bySubType(cellName, subType, outType = outType)
                    else
                        cellDFR = returnCellEventsDFR_bySubType(cellName, subType)
                    endif
                endif
            endif
            return cellDFR
        endif
    endif
    
    strswitch (subType)
        case "all":
            if(!specRegion)
                cellDFR = getCellRegionsDF(cellName)
            else
                cellDFR = getCellSpecRegionDF(cellName, regionName)
            endif
            break
        case "avg":
            if(!specRegion)
                cellDFR = getCellRegionsDF_avg(cellName)
            else
                cellDFR = getCellSpecRegionDF_avg(cellName, regionName)
            endif
            break
        case "limited":
            if(!specRegion)
                cellDFR = getCellRegionsDF_limitedAvg(cellName)
            else
                cellDFR = getCellSpecRegionDF_limitedAvg(cellName, regionName)
            endif
            break
        case "interval":
            if(!specRegion)
                cellDFR = getCellRegionsDF_interval(cellName)
            else
                cellDFR = getCellSpecRegionDF_interval(cellName, regionName)
            endif
            break
        case "specify":
            string specSubType
            if(paramIsDefault(param))
                print "returnCellRegionsDFR_bySubType: Did not specify param, defaulting to average"
                specSubType = "avg"
            else
                if(paramIsDefault(outType))
                    specSubType = getTypeStorageDF(param)
                else
                    specSubType = getTypeStorageDF(param, outType = outType)
                endif
            endif
            if(!specRegion)
                cellDFR = returnCellRegionsDFR_bySubType(cellName, specSubType)
            else
                cellDFR = returnCellRegionsDFR_bySubType(cellName, specSubType, regionName = regionName)
            endif
            break
        default:
            if(!specRegion)
                cellDFR = getCellRegionsDF(cellName)
            else
                cellDFR = getCellSpecRegionDF(cellName, regionName)
            endif            
            break
    endswitch
    return cellDFR
end

///////////
/// makeGroupRegionsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
///////////
function/DF makeGroupRegionsDFR_bySubType(string groupName, string subType[, string regionName])
    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1
    endif


    variable useOverallGroup = 0
    if(stringMatch(groupName, "overallGroupInfo"))
        useOverallGroup = 1
    endif
    
    if(!useOverallGroup)
        DFREF groupSumDFR = getGroupDF(groupName)
        if(dataFolderRefStatus(groupSumDFR)==0)
            print "makeGroupRegionsDFR_bySubtype: no group summary folder yet for group. Quitting"
            abort
        endif
    endif
    
    strswitch (subType)
        case "all":
            if(!specRegion)
                if(!useOverallGroup)
                    createGroupRegionsDF(groupName)
                else
                    createGroupInfoRegionDF()
                endif
            else
                if(!useOverallGroup)
                    createGroupSpecRegionDF(groupName, regionName)
                else
                    createGroupInfoSpecRegionDF(regionName)
                endif
            endif
            break
        case "avg":
            if(!specRegion)
                if(!useOverallGroup)
                    createGroupForAvgRegionDF(groupName)
                else
                    createGroupInfoForAvgRegionDF()
                endif
            else
                if(!useOverallGroup)
                    createGroupForAvgSpecRegionDF(groupName, regionName)
                else
                    createGroupInfoForAvgSpecRegionDF(regionName)
                endif
            endif
            break
        case "limited":
            if(!specRegion)
                if(!useOverallGroup)
                    createGroupForAvgLimitedRegionDF(groupName)
                else
                    createGroupInfoForAvgLimitedRegionDF()
                endif
            else
                if(!useOverallGroup)
                    createGroupForAvgLimitedSpecRegionDF(groupName, regionName)
                else
                    createGroupInfoForAvgLimitedSpecRegionDF(regionName)
                endif
            endif
            break
        case "interval":
            if(!specRegion)
                if(!useOverallGroup)
                    createGroupForIntervalRegionDF(groupName)
                else
                    createGroupInfoForIntervalRegionDF()
                endif
            else
                if(!useOverallGroup)
                    createGroupForIntervalSpecRegionDF(groupName, regionName)
                else
                    createGroupInfoForIntervalSpecRegionDF(regionName)
                endif
            endif
            break
        default:
            if(!specRegion)
                if(!useOverallGroup)
                    createGroupRegionsDF(groupName)
                else
                    createGroupInfoRegionDF()
                endif
            else
                if(!useOverallGroup)
                    createGroupSpecRegionDF(groupName, regionName)
                else
                    createGroupInfoSpecRegionDF(regionName)
                endif
            endif
            break
    endswitch

    DFREF createdDFR
    if(!specRegion)
        createdDFR = returnGroupRegionsDFR_bySubType(groupName, subType)
    else
        createdDFR = returnGroupRegionsDFR_bySubType(groupName, subType, regionName = regionName)
    endif
    return createdDFR
end

///////////
/// returnGroupRegionsDFR_bySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Returns the DFR for the location of waves/events based on the subset (subType) of the data
/// If subType is "specify" that means a different location is being used for different parameters
/// The series event waves for the "all" subset are stored in the root folder for historical reasons with detection
/// but the summary averages for these waves for the series are stored within the series specific folder
/// so if trying to get the summary information, eventsOrSum should be set to "sum"
/// outType specifies which type of output it is (likely the name of a graph or table), which might have different
/// decision making about the subType that should be used for each parameter
///////////
function/DF returnGroupRegionsDFR_bySubType(string groupName, string subType[, string param, string outType, string regionName])
    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    string passList = cellSum.passiveSeriesWaves + cellSum.passiveVarNames

    // If this is a passive parameter, always use all
    if(!paramIsDefault(param) && isInList(param, passList))
        subType = "all"
    endif

    DFREF groupDFR

    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1

        if(stringmatch(regionName, "Full duration"))
            if(!paramIsDefault(param) && !paramIsDefault(outType))
                groupDFR = returnGroupDFR_bySubType(groupName, subType, param = param, outType = outType)
            else
                if(!paramIsDefault(param))
                    groupDFR = returnGroupDFR_bySubType(groupName, subType, param = param)
                else
                    if(!paramIsDefault(outType))
                        groupDFR = returnGroupDFR_bySubType(groupName, subType, outType = outType)
                    else
                        groupDFR = returnGroupDFR_bySubType(groupName, subType)
                    endif
                endif
            endif
            return groupDFR
        endif
    endif

    variable useOverallGroup = 0
    if(stringMatch(groupName, "overallGroupInfo"))
        useOverallGroup = 1
    endif
    
    strswitch (subType)
        case "all":
            if(!specRegion)
                if(!useOverallGroup)
                    groupDFR = getGroupRegionsDF(groupName)
                else
                    groupDFR = getGroupInfoRegionDF()
                endif
            else
                if(!useOverallGroup)
                    groupDFR = getGroupSpecRegionDF(groupName, regionName)
                else
                    groupDFR = getGroupInfoSpecRegionDF(regionName)
                endif
            endif
            break
        case "avg":
            if(!specRegion)
                if(!useOverallGroup)
                    groupDFR = getGroupForAvgRegionDF(groupName)
                else
                    groupDFR = getGroupInfoForAvgRegionDF()
                endif
            else
                if(!useOverallGroup)
                    groupDFR = getGroupForAvgSpecRegionDF(groupName, regionName)
                else
                    groupDFR = getGroupInfoForAvgSpecRegionDF(regionName)
                endif
            endif
            break
        case "limited":
            if(!specRegion)
                if(!useOverallGroup)
                    groupDFR = getGroupForAvgLimitedRegionDF(groupName)
                else
                    groupDFR = getGroupInfoForAvgLimitedRegionDF()
                endif
            else
                if(!useOverallGroup)
                    groupDFR = getGroupForAvgLimitedSpecRegionDF(groupName, regionName)
                else
                    groupDFR = getGroupInfoForAvgLimitedSpecRegionDF(regionName)
                endif
            endif
            break
        case "interval":
            if(!specRegion)
                if(!useOverallGroup)
                    groupDFR = getGroupForIntervalRegionDF(groupName)
                else
                    groupDFR = getGroupInfoForIntervalRegionDF()
                endif
            else
                if(!useOverallGroup)
                    groupDFR = getGroupForIntervalSpecRegionDF(groupName, regionName)
                else
                    groupDFR = getGroupInfoForIntervalSpecRegionDF(regionName)
                endif
            endif
            break
        case "specify":
            string specSubType
            if(paramIsDefault(param))
                print "returnGroupRegionsDFR_bySubType: Did not specify param, defaulting to average"
                specSubType = "avg"
            else
                if(paramIsDefault(outType))
                    specSubType = getTypeStorageDF(param)
                else
                    specSubType = getTypeStorageDF(param, outType = outType)
                endif
            endif
            if(!specRegion)
                groupDFR = returnGroupRegionsDFR_bySubType(groupName, specSubType)
            else
                groupDFR = returnGroupRegionsDFR_bySubType(groupName, specSubType, regionName = regionName)
            endif
            break
        default:
            if(!specRegion)
                if(!useOverallGroup)
                    groupDFR = getGroupRegionsDF(groupName)
                else
                    groupDFR = getGroupInfoRegionDF()
                endif
            else
                if(!useOverallGroup)
                    groupDFR = getGroupSpecRegionDF(groupName, regionName)
                else
                    groupDFR = getGroupInfoSpecRegionDF(regionName)
                endif
            endif
            break
    endswitch
    return groupDFR
end