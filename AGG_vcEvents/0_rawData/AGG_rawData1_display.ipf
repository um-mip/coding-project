function updateRawDataGraph(string cellName, variable seriesNum[, string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif

    string seriesWaveName = buildSeriesWaveName(cellName, seriesNum)

    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)

    if(DataFolderRefStatus(evDetectDFR)!=0)
        DFREF waveDFR = evDetectDFR
    else
        DFREF waveDFR = root:
    endif

    Wave/SDFR=waveDFR rawWave = $seriesWaveName


    DFREF rawDFR = getEventsRawDataDF()

    string rawDataGraph = panelName + "#rawData"
    if(WinType(rawDataGraph)==1)
        clearDisplay(rawDataGraph)
        if(WaveExists(rawWave))
            string concatSweeps = AGG_wnoteSTRbyKey( seriesWaveName, "concatSweeps", waveDFR, suppressWarning = 1 )
            
            variable hasConcatSweeps = 0
            if(strlen(concatSweeps)>0)
                hasConcatSweeps = 1
            endif
            
            ControlInfo/W=$panelName smoothWave
            variable smoothWave = V_value
            string traceN = seriesWaveName
            if(smoothWave)
                string smoothWN = seriesWaveName+"_smth"
                traceN = smoothWN
                Duplicate/O rawWave, evDetectDFR:$smoothWN
                Wave/SDFR=evDetectDFR rawWave = $smoothWN
                Smooth/B 9, rawWave
            endif

            AppendToGraph/W=$rawDataGraph rawWave
            ModifyGraph/W=$rawDataGraph rgb($traceN)=(0,0,0)

            ControlInfo/W=$panelName scaleCheck
            variable useScale = V_value

            if(useScale)
                NVAR/Z/SDFR=rawDFR startTime, duration, yMin, yRange
                if(NumType(startTime)==0 && numType(duration)==0)
                    variable endTime = startTime + duration
                    SetAxis/W=$rawDataGraph bottom startTime, endTime
                endif
                if(NumType(yMin)==0 && numType(yRange)==0)
                    variable yMinScale, yRangeScale
                    yMinScale = yMin * 1e-12
                    yRangeScale = yRange * 1e-12
                    SetAxis/W=$rawDataGraph left yMinScale, yMinScale+yRangeScale
                endif
            else
                SetAxis/W=$rawDataGraph/A/E=3 bottom
                SetAxis/W=$rawDataGraph/A/E=3 left
            endif

            NVAR/Z/SDFR=rawDFR showGaps
            if(hasConcatSweeps && showGaps)
                string gapSTARTSstring = AGG_wnoteSTRbyKey( seriesWaveName, "sweepGapStarts", waveDFR )
                string gapENDSstring = AGG_wnoteSTRbyKey( seriesWaveName, "sweepGapEnds", waveDFR )

                DFREF dfrForPlotWaves = rawDFR
                variable rawDuration = pnt2x(rawWave,numpnts(rawWave)-1) - pnt2x(rawWave, 0)

                Wave/SDFR=dfrForPlotWaves gapPlotX, gapPlotY
                if(WaveExists(gapPlotX) && waveExists(gapPlotY))
                    redimension/N=0 gapPlotX, gapPlotY
                endif

                variable foundGaps = makeGapPlotWaves_fromString(gapStartsString, gapEndsString, dfrForPlotWaves = dfrForPlotWaves, addStartEnd = 1, totalDur = rawDuration)

                if(foundGaps)
                    Wave/SDFR=dfrForPlotWaves gapPlotX, gapPlotY
                    if(WaveExists(gapPlotX) && WaveExists(gapPlotY))
                        appendGapPlotsToGraph(gapPlotX, gapPlotY, hostN = rawDataGraph)
                    endif
                endif
            endif
        endif
    endif
end
    