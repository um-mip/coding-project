function updateRawGraphProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    string panelName = B_Struct.win
    if(B_Struct.eventCode == 2)
        string selCell
        variable selSeries, validCell, prevPassives, prevDetection
        [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = panelName)
        updateRawDataGraph(selCell, selSeries, panelName = panelName)
    endif
end