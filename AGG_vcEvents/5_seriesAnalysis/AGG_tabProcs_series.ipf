function selectedCellListProc_series(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF panelDFR = getEventsPanelDF()

        variable selectedCellNum = LB_Struct.row
        WAVE/T cellNames = panelDFR:cellName
        if(selectedCellNum < numpnts(cellNames))
            string selectedCell = cellNames[selectedCellNum]
            // print selectedCell
            updateSeriesWave(selectedCell)
            updatePanelForSelectedSeries(panelName = LB_struct.win)
        endif
    endif
    return 0
end

function updateSeriesWave (cellName)
    String cellName
    variable makeBlank = 0

    DFREF panelDFR = getEventsPanelDF()
    if(strlen(cellName)>0)
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
        if(DataFolderRefStatus(cellInfoDFR)==0)
            createEventsCellInfoDF(cellName)
            cellInfoDFR = getEventsCellInfoDF(cellName)
        endif

        Wave cellEvents_series = cellInfoDFR:Events_series

        if(WaveExists(cellEvents_series))
            Duplicate/O cellEvents_series, panelDFR:selCellSeries

            Wave selSeries = panelDFR:selSeries

            redimension/N=(numpnts(cellEvents_series)) selSeries
        else
            makeBlank = 1
        endif
    else
        makeBlank = 1 
    endif

    if(makeBlank)
        Wave selCellSeries = panelDFR:selCellSeries
        redimension/N=0 selCellSeries

        Wave selSeries = panelDFR:selSeries
        redimension/N=0 selSeries
    endif
end

function selectedSeriesListProc_series(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    string panelName = LB_struct.win

    if(LB_Struct.eventcode == 4)
        DFREF panelDFR = getEventsPanelDF()

        variable selectedSeriesNum = LB_Struct.row
        WAVE/T seriesNames = panelDFR:selCellSeries
        if(selectedSeriesNum < numpnts(seriesNames))
           updatePanelForSelectedSeries(panelName = panelName)
        endif
    endif
    return 0
end

///////////
/// updateTabDisplay_eventsPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: Use this to update only the relevant tab of the panel
/// to save processing time
///////////
function updateTabDisplay_eventsPanel()
    string panelName = "AGG_events"
    string tabLabel = getSelectedTab(panelName = panelName)

    string selCell, cellName
    variable selSeries, validCell, prevPassives, prevDetection
    [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = panelName, suppressWarning = 1)

    strswitch (tabLabel)
        case "Passives":
            if(prevPassives)
                // print "updating passives for cell", selCell, "series", selSeries 
                changePassiveTableWaves(selCell)
                changePassiveSeriesWaves(selCell, selSeries)
                DFREF currentFolder = getDataFolderDFR()
                // still not working. checkboxes on panel
                setDataFolder root:
                // remakePassiveGraph(expcode=selCell, graphName="passives") // don't automake standalone
                remakePassiveGraph(expcode=selCell, graphName= panelName+"#panelPassivesGraph")
                setDataFolder currentFolder
            else
                // print "no previous passives"
                changePassiveTableWaves("")
                changePassiveSeriesWaves("", NaN)
                // clearDisplay("passives")
                clearDisplay(panelName+"#passives")
            endif
            break
        case "Raw Data":
            updateRawDataGraph(selCell, selSeries, panelName = panelName)
            break
        case "Confirmation":
            updateConfirmationParams(selCell, selSeries, prevDetection, panelName = panelName)
            break
        case "Series Events":
            if(prevDetection)
                calcSeriesAvgSubsetWaves(selCell, selSeries)
            endif
            updateSeriesEventInfo(selCell, selSeries, prevDetection, panelName = panelName)
            break
        case "Series Summary":
            // Update average events only subset
            calcSeriesAvgSubsetWaves(selCell, selSeries)

            // Calculate the averages for this series
            calcSeriesEventAvgs(selCell, selSeries)
            if(WinType(panelName + "#seriesSumGraph") == 1)
                updateSeriesSummary(selCell, selSeries, prevDetection, panelName = panelName)
            endif
            break
        case "Series Output":
            updateSeriesOutTable()
            break
        case "Cell Output":
            updateCellsOutTable()
            break
        case "Cell Concat":
            cellName = getSelectedItem("cellListBox_cellConcat", hostName = "AGG_events")
            updateSeriesWave_cell(cellName)
            updateConcSeriesLB(cellName)
            updateCellConcatDisplays()
            break
        case "Cell Summary":
            cellName = getSelectedItem("cellListBox_cell", hostName = "AGG_events")
            clearDisplay("AGG_events#cellSumGraph")
            updateCellRegionsLB(cellName)
            displayCellParams()
            updateCellSumTable(cellName)
        case "By series plots":
            updateGroupGraphs()
            break
        case "By cell plots":
            updateGroupGraphs()
            break
        case "Group traces":
            displayGroupParams()
            break
        default:
            
            break
    endswitch
end


function updatePanelForSelectedSeries([string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif

    // !!!!!!!!!!!!!! IMPORTANT UPDATE DEC 2023 !!!!!!!!!!!!!!!!!!!!!!
    // If this is the AGG_Events panel that is calling this, it follows this alt
    // function and doesn't continue through the rest of the steps in this function
    if(StringMatch(panelName, "AGG_events"))
        updateTabDisplay_eventsPanel()
        return 0
    endif
    
    string selCell
    variable selSeries, validCell, prevPassives, prevDetection
    [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = panelName)
    if(prevPassives)
        // print "updating passives for cell", selCell, "series", selSeries 
        changePassiveTableWaves(selCell)
        changePassiveSeriesWaves(selCell, selSeries)
        DFREF currentFolder = getDataFolderDFR()
        // still not working. checkboxes on panel
        setDataFolder root:
        // remakePassiveGraph(expcode=selCell, graphName="passives") // don't automake standalone
        remakePassiveGraph(expcode=selCell, graphName= panelName+"#panelPassivesGraph")
        setDataFolder currentFolder
    else
        // print "no previous passives"
        changePassiveTableWaves("")
        changePassiveSeriesWaves("", NaN)
        // clearDisplay("passives")
        clearDisplay(panelName+"#passives")
    endif

    updateRawDataGraph(selCell, selSeries, panelName = panelName)
    updateSeriesEventInfo(selCell, selSeries, prevDetection, panelName = panelName)
    // Calculate the averages for this series
    calcSeriesAvgSubsetWaves(selCell, selSeries)
    calcSeriesEventAvgs(selCell, selSeries)
    if(WinType(panelName + "#seriesSumGraph") == 1)
        updateSeriesSummary(selCell, selSeries, prevDetection, panelName = panelName)
    endif
    updateConfirmationParams(selCell, selSeries, prevDetection, panelName = panelName)
    if(WinType(panelName + "#rawOptoData")==1)
        updateRawOptoGraph(selCell, selSeries, panelName = panelName)
    endif  
    string evokedDisplays = "selectedWave;peristimHistGraph;evoked;not_evoked;"
    variable iDisplay
    for(iDisplay=0; iDisplay<itemsinlist(evokedDisplays); iDisplay++)
        string thisDisplay = StringFromList(iDisplay, evokedDisplays)
        if(WinType(panelName + "#" + thisDisplay)==1)
            clearDisplay(panelName + "#" + thisDisplay)
        endif     
    endfor
end

function [string selCell, variable selSeries, variable validCell, variable prevPassives, variable prevDetection] getInfoAboutSelectedSeries([string panelName, variable suppressWarning])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif

    if(paramIsDefault(suppressWarning))
        suppressWarning = 0
    endif
    

    // print "getInfo, panelName", panelName

    variable makeBlank = 0
    selSeries = 0
    validCell = 0
    prevPassives = 0
    prevDetection = 0
    selCell = ""

    string cellName, seriesName, seriesString

    cellName = getSelectedItem("cellListBox_series", hostName = panelName)
    seriesName = getSelectedItem("seriesListBox_series", hostName = panelName)

    if(strlen(cellName)>0 && strlen(seriesName)>0)
        seriesString = buildSeriesWaveName(cellName, str2num(seriesName))

        selCell = cellName
        selSeries = str2num(seriesName)
    else
        makeBlank = 1
    endif

    if(strlen(cellName)>0)
        if(WaveExists(root:$(cellName + "_passn"))) // if passive
            prevPassives = 1
            // print "passives wave exists", prevPassives
        else
            variable isExtracellular = checkIsExtracellular()
            if(!isExtracellular && !suppressWarning)
                print "getInfoAboutSelectedSeries: No PASSN wave for ", cellName, "series", seriesName, ". Have you checked passives?"  
            endif
        endif
    endif

    if(!makeBlank)
        DFREF cellDFR = getEventsdetectWavesDF(cellName)
        variable dfrStatus = DataFolderRefStatus(cellDFR)

        if(dfrStatus == 0)
            print "cell detect exists"
            makeBlank = 1
        else
            validCell = 1
            
            if(WaveExists(cellDFR:$(seriesString + "_ptb"))) // if ptb wave
                prevDetection = 1
            else
                makeBlank = 1
                if(!suppressWarning)
                    print "getInfoAboutSelectedSeries: No PTB wave for ", cellName, "series", seriesName, ". Have you detected?" 
                endif
            endif
        endif
    endif

    return [selCell, selSeries, validCell, prevPassives, prevDetection]
end

///////////
/// displaySeriesGraphProc
/// AUTHOR: Amanda
/// ORIGINAL DATE: 2022-04-25
/// NOTES: 
///////////
function displaySeriesGraphProc(LB_Struct) : ListBoxControl
    STRUCT WMListboxAction & LB_Struct
    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)

        DFREF seriesSumDFR = getEventsSeriesSumOutDF()

        string selDisplay = getSelectedItem("displayTypeListBox_series", hostName = LB_Struct.win)
        if(StringMatch(selDisplay, "distribution plot"))
            Wave/T/SDFR=seriesSumDFR titleWave = titleWave_params
            Wave/SDFR=seriesSumDFR selWave = selWave_params
        else
            make/O/N=0/T seriesSumDFR:blankTitle/Wave=titleWave
            make/O/N=0 seriesSumDFR:blankSel/Wave=selWave
        endif
        ListBox paramListBox_series, listWave = titleWave, selWave = selWave
        updateSeriesSummaryDisplay(panelName = LB_struct.win)
    endif
    return 0
end


///////////
/// testTimingFunc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: 
///////////
function testTimingFunc()
    variable postEventCodeTimerRef = StartMSTimer
    DFREF panelDFR = getEventsPanelDF()

    string panelName = "AGG_events"
    updatePanelForSelectedSeries(panelName = panelName)

    variable postEventCodeTime = StopMSTimer(postEventCodeTimerRef)
    print "external timing func", postEventCodeTime
end
