function/Wave getSeriesEventsForAvgIndex(string cellName, variable seriesNum[, variable maxEvents, variable useAllEvents])
    string seriesName = num2str(seriesNum)

    string seriesTraceName = buildSeriesWaveName(cellName, seriesNum)

    string seriesAvgListName = seriesTraceName + returnext("ave list")
    // print seriesTraceName, seriesAvgListName

    DFREF eventsDFR = getEventsDetectWavesDF(cellName)

    if(dataFolderRefStatus(eventsDFR)==0)
        return $("")
    endif

    DFREF avgSubsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)
    if(DataFolderRefStatus(avgSubsetDFR)==0)
        createSeriesEventsForAvgDF(cellName, seriesName)
        avgSubsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)
    endif
    
    Wave/SDFR=eventsDFR seriesAvgList = $seriesAvgListName

    if(!WaveExists(seriesAvgList))
        return $("")
    endif

    if(paramIsDefault(useAllEvents))
        useAllEvents = 0
    endif

    if(useAllEvents) // don't actually store this anywhere
        make/FREE/D/O/N=(numpnts(seriesAvgList)) avgIndex = p+0
        return avgIndex
    endif

    Extract/O/INDX seriesAvgList, avgSubsetDFR:avgIndex, seriesAvgList>0
    Wave/SDFR = avgSubsetDFR avgIndex

    if(!paramIsDefault(maxEvents) && maxEvents>0)
        DFREF limitedDFR = getSeriesEventsForAvgLimitedDF(cellName, seriesName)
        if(dataFolderRefStatus(limitedDFR)==0)
            createSeriesEventsForAvgLimitedDF(cellName, seriesName)
            limitedDFR = getSeriesEventsForAvgLimitedDF(cellName, seriesName)
        endif
        duplicate/O avgIndex, limitedDFR:avgIndex_all/Wave=avgIndex_all
        Wave avgIndex = randomSubsetOfWave(avgIndex_all, maxEvents, subWN = "avgIndex")
    endif
    
    return avgIndex
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-16
/// NOTES: Added lines so that an average subset distribution is created for the series
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-16
/// NOTES: added "avgSubType" string optional parameter
///         - "all" -> save in avgSubset folder for cell/series
///         - "limited" -> save in limitedAvgSubset folder for cell/series
///         - defaults to all
///         If limited, likely need to be providing own avgIndex wave, or should be specifying maxEvents
///     maxEvents: if avgIndex is not provided, if maxEvents is provided, then this is passed on to getSeriesEventsForAvgIndex
///         which then only takes a random subset up to this amount of events for this series
///         - if maxEvents is provided, then avgSubType is set to limited
function/Wave getEventsAvgSubset(string cellName, variable seriesNum, Wave waveToSubset, string paramName[, Wave avgIndex, string avgSubType, variable maxEvents])
    string seriesName = num2str(seriesNum)
    if(paramisdefault(avgIndex))
        if(!paramIsDefault(maxEvents) && maxEvents > 1)
            avgSubType = "limited"
            Wave avgIndex = getSeriesEventsForAvgIndex(cellName, seriesNum, maxEvents = maxEvents) 
        else
            Wave avgIndex = getSeriesEventsForAvgIndex(cellName, seriesNum)
        endif
    endif
    
    if(!WaveExists(avgIndex) || !waveExists(waveToSubset))
        print "getEventsAvgSubset: Cannot find avgIndex and/or waveToSubset"
        return $("")
    endif

    DFREF eventsDFR = getEventsDetectWavesDF(cellName)

    if(dataFolderRefStatus(eventsDFR)==0)
        print "getEventsAvgSubset: Cannot find eventsDFR data folder"
        return $("")
    endif

    if(paramIsDefault(avgSubType) && !(!paramIsDefault(maxEvents) && maxEvents > 1))
        avgSubType = "all"
    endif
    
    strswitch (avgSubType)
        case "all":
            DFREF subsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)
            
            break
        case "limited":
            DFREF subsetDFR = getSeriesEventsForAvgLimitedDF(cellName, seriesName)

            break
        case "interval":
            DFREF subsetDFR = getSeriesEventsForIntervalDF(cellName, seriesName)
            break
        default:
            DFREF subsetDFR = getSeriesEventsForAvgDF(cellName, seriesName)
            
            break
    endswitch

    if(dataFolderRefStatus(subsetDFR)==0)
        print "getEventsAvgSubset: Cannot find subsetDFR data folder"
        return $("")
    endif

    // print "param", paramName, numpnts(waveToSubset), numpnts(avgIndex)
    string waven = nameOfWave(waveToSubset)
    make/O/N=(numpnts(avgIndex)) subsetDFR:$waven/Wave=subWave
    subWave = waveToSubset[avgIndex[p]]

    copyDataScale(waveToSubset, subWave)

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)

    string fullName = returnFullNameFromExt(paramName)
    string probDistribution = stringbykey(fullName, dds.probDistribution)

    duplicate/O subWave, subsetDFR:$(waven + "_dist")/Wave=distWave
    copyDataScale(waveToSubset, distWave)

    strswitch (probDistribution)
        case "bySign":
            struct analysisparameters ps
            getSeriesDetectParams(cellName, num2str(seriesNum), paramStruct = ps) // read the panel parameters for this cell/series

            probdistp(nameOfWave(distWave), ps.peaksign, distWave = distWave)
            break
        case "pos":
            probdistp(nameOfWave(distWave), 1, distWave = distWave)
            break
        default:
            
            break
    endswitch

    return subWave
end

function calcSeriesAvgSubsetWaves(string cellName, variable seriesNum[, wave avgIndex, string subType])
    string seriesName = num2str(seriesNum)
    string seriesTraceName = buildSeriesWaveName(cellName, seriesNum)

    DFREF eventsDFR = getEventsDetectWavesDF(cellName)

    if(dataFolderRefStatus(eventsDFR)==0)
        print "calcSeriesAvgSubsetWaves: Couldn't find eventsDFR"
        return 0
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
        string avgSubTypes = "avg;limited;"
        // if subtype doesn't exist, or isn't an average type, default to avg
        if(strlen(subType)==0 || itemsInList(ListMatch(avgSubTypes, subType))== 0)
            subType = "avg"
        endif
    endif
    

    strswitch (subType)
        case "avg":
            DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
            if(dataFolderRefStatus(seriesSumDFR)==0)
                createSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
            endif
            break
        case "limited":
            DFREF seriesSumDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
            if(dataFolderRefStatus(seriesSumDFR)==0)
                createSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                DFREF seriesSumDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
            endif
            break
        case "interval":
            DFREF seriesSumDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
            if(dataFolderRefStatus(seriesSumDFR)==0)
                createSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
                DFREF seriesSumDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
            endif
            break
        default:
            DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
            if(dataFolderRefStatus(seriesSumDFR)==0)
                createSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
            endif
            break
    endswitch
    
    if(paramIsDefault(avgIndex))
        // Kill any existing waves in the seriesSumDFR if it already exists
        // chose to not just kill DFR, as if any of them happen to be in use, then
        // none get killed
        if(dataFolderRefStatus(seriesSumDFR)!=0)
            killWavesInFolder("*", seriesSumDFR)
        endif

        Wave avgIndex = getSeriesEventsForAvgIndex(cellName, seriesNum)
    endif
    

    if(DataFolderRefStatus(seriesSumDFR)!=0 & WaveExists(avgIndex))
        Struct outputWaves outputWaves
        fillOutWavesStrings(outputWaves)
        string analysisTypes = outputWaves.avgSubsetAnalysisTypes
        // If doing the interval subset, add the interval wave to the
        // list of what should be stitched together
        if(stringmatch(subType, "interval"))
            analysisTypes += "int;"
        endif

        variable numAnalyses = itemsInList(analysisTypes), iAnalysis = 0
        for(iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++)
            string analysisType = stringfromlist(iAnalysis, analysisTypes)

            string waveN = seriesTraceName + "_" + analysisType
            Wave waveToSubset = eventsDFR:$waveN
            if(waveExists(waveToSubset))
                // print "trying to subset", cellName, seriesNum, analysisType
                getEventsAvgSubset(cellName, seriesNum, waveToSubset, analysisType, avgIndex = avgIndex, avgSubType = subType)
            endif           
        endfor

        // Make the average wave in this folder
        Wave/SDFR=eventsDFR rawWave = $seriesTraceName
        make/Wave/FREE/N=1 wavesToAvg
        wavesToAvg[0] = rawWave
        make/Wave/FREE/N=1 avgIndexWaves
        avgIndexWaves[0] = avgIndex

        make/D/O seriesSumDFR:$(seriesTraceName + returnext("event average"))/Wave=aveWave
        make/D/O seriesSumDFR:$(seriesTraceName + returnext("normalized average"))/Wave=nAveWave

        string analysisParamAsString = getSeriesDetectParams(cellName, num2str(seriesNum))

        recalculateAverages5(wavesToAvg, aveWave = aveWave, nAveWave = nAveWave, analysisParamAsString = analysisParamAsString, avgIndexWaves = avgIndexWaves)
    endif




end

///////////
/// calcSeriesSubsetWavesForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function calcSeriesSubsetWavesForCell(string cellName, variable maxAvgNumEvents, variable maxIntNumEvents, wave selectedSeriesNums)
    if(!WaveExists(selectedSeriesNums))
        print "calcSeriesSubsetWavesForCell: couldn't find selectedSeriesNums waves"
        return 0
    endif

    Wave concatAvgIndices_avg = calcSeriesSubsetTypeIndices(cellName, "avg", selectedSeriesNums)
    Wave concatAvgIndices_limited = calcSeriesSubsetTypeIndices(cellName, "limited", selectedSeriesNums, maxNumEvents = maxAvgNumEvents)
    Wave concatAvgIndices_interval = calcSeriesSubsetTypeIndices(cellName, "interval", selectedSeriesNums, maxNumEvents = maxIntNumEvents)

    calcSeriesAvgsBySubType(cellName, "avg", selectedSeriesNums, concatAvgIndices_avg)
    calcSeriesAvgsBySubType(cellName, "limited", selectedSeriesNums, concatAvgIndices_limited)
    calcSeriesAvgsBySubType(cellName, "interval", selectedSeriesNums, concatAvgIndices_interval)


end

///////////
/// calcSeriesSubsetTypeIndices
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function/Wave calcSeriesSubsetTypeIndices(string cellName, string subType, wave selectedSeriesNums[, variable maxNumEvents])
    variable iSeries = 0, numSeries = numpnts(selectedSeriesNums), seriesNum

    variable maxEvents, useAllEvents = 0
    strswitch (subType)
        case "avg":
            DFREF cellSumDFR = getCellAvgSubDF(cellName)
            maxEvents = NaN
            break
        case "limited":
            DFREF cellSumDFR = getCellAvgSubLimitedDF(cellName)
            if(paramIsDefault(maxNumEvents))
                maxEvents = 100 // default to 100
            else
                maxEvents = maxNumEvents  
            endif
            break
        case "interval":
            DFREF cellSumDFR = getCellIntSubDF(cellName)
            useAllEvents = 1
            if(paramIsDefault(maxNumEvents))
                maxEvents = 250 // default to 250
            else
                maxEvents = maxNumEvents  
            endif
            break
        default:
            print "calcSeriesSubsetTypeIndices: Unknown subType. Quitting"
            return $("")
            break
    endswitch

    make/N=(0,2)/D/O cellSumDFR:concatAvgIndices/Wave=concatAvgIndices

    // Get the initial indices for events that can be included in the average
    // from each series, and concatenate into a combined wave
    // the second column of this wave is the series number, so that we know
    // which series the event index belongs to
    for(iSeries = 0; iSeries<numSeries; iSeries++)
        seriesNum = selectedSeriesNums[iSeries]
        Wave avgIndex = getSeriesEventsForAvgIndex(cellName, seriesNum, useAllEvents = useAllEvents)
        if(!WaveExists(avgIndex))
            print "calcSeriesSubsetTypeIndices: WARNING: couldn't find an avgIndex for a selected series", seriesNum, ". Quitting"
            abort
        endif
        variable seriesNumEvents = numpnts(avgIndex)
        make/FREE/N=(seriesNumEvents, 2) tempAvgIndex
        tempAvgIndex[][0] = avgIndex[p]
        tempAvgIndex[][1] = seriesNum
        
        Concatenate/NP=0 {tempAvgIndex}, concatAvgIndices
    endfor

    if(maxEvents > 0)
        duplicate/O concatAvgIndices, cellSumDFR:concatAvgIndices_all/Wave=concatAvgIndices_all
        Wave concatAvgIndices = randomSubsetOfWave_cell(concatAvgIndices_all, maxNumEvents, subWN = "concatAvgIndices")
    endif
    return concatAvgIndices
end

///////////
/// calcSeriesAvgsBySubType
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-17
/// NOTES: 
///////////
function calcSeriesAvgsBySubType(string cellName, string subType, wave selectedSeriesNums, wave concatAvgIndices)
    variable iSeries = 0, numSeries = numpnts(selectedSeriesNums), seriesNum

    for(iSeries = 0; iSeries<numSeries; iSeries++)
        seriesNum = selectedSeriesNums[iSeries]

        strswitch (subType)
            case "avg":
                DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                if(dataFolderRefStatus(seriesSumDFR)==0)
                    createSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                    DFREF seriesSumDFR = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
                endif
                break
            case "limited":
                DFREF seriesSumDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                if(dataFolderRefStatus(seriesSumDFR)==0)
                    createSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                    DFREF seriesSumDFR = getSeriesEventsForAvgLimitedDF(cellName, num2str(seriesNum))
                endif
                break
            case "interval":
                DFREF seriesSumDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
                if(dataFolderRefStatus(seriesSumDFR)==0)
                    createSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
                    DFREF seriesSumDFR = getSeriesEventsForIntervalDF(cellName, num2str(seriesNum))
                endif
                break
            default:
                print "calcSeriesAvgsBySubType: Unknown subType. Quitting"
                return 0
                break
        endswitch
        
        killWavesInFolder("*", seriesSumDFR)
        duplicate/O/FREE/RMD=[][1,1] concatAvgIndices, seriesCol
        duplicate/O/FREE/RMD=[][0,0] concatAvgIndices, indicesCol
        // Find the indices (of the index wave) that match the current series
        Extract/O/FREE/INDX seriesCol, indicesMatchingSeries, seriesCol == seriesNum
        
        make/N=(numpnts(indicesMatchingSeries))/O seriesSumDFR:avgIndex/Wave=avgIndex

        // Set the values of the avg index wave for this series equal to
        // the appropriate indices that we selected above
        avgIndex = indicesCol[indicesMatchingSeries[p]]

        // Calculate all of the other average subset waves, but provide
        // an avgIndex wave, instead of having it try to recalculate for us
        calcSeriesAvgSubsetWaves(cellName, seriesNum, avgIndex = avgIndex, subType = subType)
    endfor
end



///////////
/// calcAvgSubsetWaves_limitEventsPerCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-06
/// NOTES: In cases where we want to limit the number of events per cell, the average subset for each series
/// is dependent on the other series for that cell, and so becomes more complex
///////////
function calcAvgSubsetWaves_limitEventsPerCell(string cellName, variable maxNumEvents, wave selectedSeriesNums)
    if(!WaveExists(selectedSeriesNums))
        print "calcAvgSubsetWaves_limitEventsPerCell: couldn't find selectedSeriesNums waves"
        return 0
    endif

    variable iSeries = 0, numSeries = numpnts(selectedSeriesNums), seriesNum

    make/N=(0, 2)/FREE concatAvgIndices
    // Get the initial indices for events that can be included in the average
    // from each series, and concatenate into a combined wave
    // the second column of this wave is the series number, so that we know
    // which series the event index belongs to
    for(iSeries = 0; iSeries<numSeries; iSeries++)
        seriesNum = selectedSeriesNums[iSeries]
        Wave avgIndex = getSeriesEventsForAvgIndex(cellName, seriesNum)
        variable seriesNumEvents = numpnts(avgIndex)
        make/FREE/N=(seriesNumEvents, 2) tempAvgIndex
        tempAvgIndex[][0] = avgIndex[p]
        tempAvgIndex[][1] = seriesNum
        
        Concatenate/NP=0 {tempAvgIndex}, concatAvgIndices
    endfor

    // Get the subset of this combined set of indices
    Wave subsetCellAvgs = randomSubsetOfWave_cell(concatAvgIndices, maxNumEvents)
    
    for(iSeries = 0; iSeries<numSeries; iSeries++)
        seriesNum = selectedSeriesNums[iSeries]
        DFREF avgSubDF = getSeriesEventsForAvgDF(cellName, num2str(seriesNum))
        killWavesInFolder("*", avgSubDF)
        duplicate/O/FREE/RMD=[][1,1] subsetCellAvgs, seriesCol
        duplicate/O/FREE/RMD=[][0,0] subsetCellAvgs, indicesCol
        // Find the indices (of the index wave) that match the current series
        Extract/O/FREE/INDX seriesCol, indicesMatchingSeries, seriesCol == seriesNum
        
        make/N=(numpnts(indicesMatchingSeries))/O avgSubDF:avgIndex/Wave=avgIndex

        // Set the values of the avg index wave for this series equal to
        // the appropriate indices that we selected above
        avgIndex = indicesCol[indicesMatchingSeries[p]]

        // Calculate all of the other average subset waves, but provide
        // an avgIndex wave, instead of having it try to recalculate for us
        calcSeriesAvgSubsetWaves(cellName, seriesNum, avgIndex = avgIndex)
    endfor
end


///////////
/// testLimitedSubset
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-06
/// NOTES: 
///////////
function testLimitedSubset()
    Wave avgIndex = root:Events:cellInfo:c20220115e:s20:avgSubset:avgIndex

    // StatsSample/N=10 avgIndex
    // sort W_Sampled, W_Sampled
    // edit/K=1 W_Sampled

    Wave subWave = randomSubsetOfWave(avgIndex, 10)
    edit/K=1 subWave
end


///////////
/// randomSubsetOfWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-06
/// NOTES: Returns a random subset of the waveToSubset containing the
/// maxPnts specified. 
/// If subWN is not specified, adds "_sub" to the end of the waveToSubset name
/// If the wave has fewer points than maxPnts, still duplicates waveToSubset, but
/// keeps it as is
///////////
function/Wave randomSubsetOfWave(wave waveToSubset, variable maxPnts[, string subWN, DFREF subDFR])
    if(!WaveExists(waveToSubset))
        print "randomSubsetOfWave: Cannot find waveToSubset"
        return $("")
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(waveToSubset)

    if(paramIsDefault(subWN))
        subWN = waveN + "_sub"
    endif

    if(paramIsDefault(subDFR))
        subDFR = waveDFR
    endif
    

    variable nPnts = numpnts(waveToSubset)

    if(nPnts <= maxPnts)
        duplicate/O waveToSubset, subDFR:$(subWN)/Wave=subWave
        return subWave
    endif

    StatsSample/N=(maxPnts) waveToSubset
    duplicate/O W_Sampled, subDFR:$(subWN)/Wave=subWave
    sort subWave, subWave
    KillWaves/Z W_Sampled

    return subWave
end

///////////
/// randomSubsetOfWave_cell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-06
/// NOTES: Returns a random subset of the waveToSubset containing the
/// maxPnts specified. 
/// If subWN is not specified, adds "_sub" to the end of the waveToSubset name
/// If the wave has fewer points than maxPnts, still duplicates waveToSubset, but
/// keeps it as is
///////////
function/Wave randomSubsetOfWave_cell(wave waveToSubset, variable maxPnts[, string subWN])
    if(!WaveExists(waveToSubset))
        print "randomSubsetOfWave: Cannot find waveToSubset"
        return $("")
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(waveToSubset)

    if(paramIsDefault(subWN))
        subWN = waveN + "_sub"
    endif

    variable nPnts = DimSize(waveToSubset, 0)

    if(maxPnts <= 0 || nPnts <= maxPnts)
        return waveToSubset
    endif

    // StatsSample/N=(maxPnts)/MC waveToSubset
    // SortColumns/KNDX={1, 0} sortWaves = {M_Sampled}
    // return M_Sampled

    // For when finished testing - reduce # of extra waves
    StatsSample/N=(maxPnts)/MC waveToSubset
    duplicate/O M_Sampled, waveDFR:$(subWN)/Wave=subWave
    SortColumns/KNDX={1, 0} sortWaves = {subWave}
    KillWaves/Z M_Sampled

    return subWave
end
