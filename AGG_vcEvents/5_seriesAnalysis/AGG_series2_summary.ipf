function calcSeriesEventAvgs(cellName, seriesNum[, convertUnits])
    string cellName
    variable seriesNum, convertUnits

    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0
        endif
    endif

    string seriesName = num2str(seriesNum)

    string seriesString = buildSeriesWaveName(cellName, seriesNum)
    DFREF seriesDFR = getSeriesDF(cellName, seriesName)

    DFREF cellDFR = getEventsDetectWavesDF(cellName)    

    if(dataFolderRefStatus(cellDFR)==0 || dataFolderRefStatus(seriesDFR)==0)
        return 0
    endif

    Wave/SDFR=cellDFR rawWave = $seriesString // get this first before deciding whether or not to use subset

    if(!WaveExists(rawWave))
        print "calcSeriesEventAvgs: Can't find rawWave for", cellName, seriesNum
        return 0
    endif

    storeSeriesEventAvg(seriesString, rawWave, cellDFR, seriesDFR, convertUnits = convertUnits)

    DFREF avgSubDFR = getSeriesEventsForAvgDF(cellName, seriesName)

    if(dataFolderRefStatus(avgSubDFR)==0)
        print "calcSeriesEventAvgs: avgSubDFR doesn't exist yet for", cellName, seriesName
        print "Did not store average subset"
    else
        storeSeriesEventAvg(seriesString, rawWave, avgSubDFR, avgSubDFR, convertUnits = convertUnits) 
    endif

    DFREF limitedDFR = getSeriesEventsForAvgLimitedDF(cellName, seriesName)
    
    if(dataFolderRefStatus(limitedDFR)!=0)
        storeSeriesEventAvg(seriesString, rawWave, limitedDFR, limitedDFR, convertUnits = convertUnits)  
    endif
    
    DFREF intervalDFR = getSeriesEventsForIntervalDF(cellName, seriesName)
    
    if(dataFolderRefStatus(intervalDFR)!=0)
        storeSeriesEventAvg(seriesString, rawWave, intervalDFR, intervalDFR, convertUnits = convertUnits)  
    endif
end

///////////
/// storeSeriesEventAvg
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-16
/// NOTES: Made this sub function to save both the full subset and the average list subset
/// average values for each series. The process is the same, but where the function should
/// look for the _analysisType waves differs (root vs. series avgSubset folder)
/// as does where the avg variables should be stored (seriesDFR vs series avgSubset folder)
///////////
function storeSeriesEventAvg(string seriesString, wave rawWave, DFREF eventInfoWavesDFR, DFREF avgVarsDFR[, variable convertUnits])
    if(dataFolderRefStatus(eventInfoWavesDFR)==0 || dataFolderRefStatus(avgVarsDFR)==0)
        print "storeSeriesEventAvg: Cannot find the data folder for either where the waves are stored and/or where the avg variables should be saved"
    endif

    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0
        endif
    endif

    // The average variables for each series are stored in a folder
    // When filling the structure with the existing values,
    // The variables will be created if they don't already exist
    STRUCT eventAvgVars seriesEventAvgs
    if(stringmatch(getDFREFstring(avgVarsDFR), "root:"))
        print "series", seriesString, "storeSeriesEventAvg is making variables in root"
    endif
    StructFill/SDFR = avgVarsDFR/AC=1 seriesEventAvgs
    fillAvgVarNames(seriesEventAvgs)

    Wave/SDFR=eventInfoWavesDFR ptbWave = $(seriesString + "_ptb")

    // To fill from a multi-return function, can't use the structure directly
    // Create local variables first, then fill the structure
    variable seriesTime, duration, numEvents, freq
    [seriesTime, duration] = calcSeriesTimeAndDuration(rawWave)
    [numEvents, freq] = calcSeriesCountAndFreq(ptbWave, duration)
    seriesEventAvgs.seriesTime = seriesTime
    seriesEventAvgs.duration = duration
    seriesEventAvgs.numEvents = numEvents
    seriesEventAvgs.freq = freq

    string analysis_types = seriesEventAvgs.analysis_types

    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    string nameVariable = ""

    // For each analysis type, get the average of the wave and 
    // store it in the appropriate variable 
    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameVariable = StringFromList( iAnalysis, analysis_types )
        nameAnalysis = stringbykey(nameVariable, seriesEventAvgs.waveFromVar)

        string seriesAnalysisWN = seriesString + "_" + nameAnalysis
        Wave/SDFR = eventInfoWavesDFR thisWave = $seriesAnalysisWN
        NVAR/Z/SDFR = avgVarsDFR thisVar = $nameVariable
        if(waveExists(thisWave))
            if(convertUnits == 1)
                duplicate/O thisWave, $("d" + nameOfWave(thisWave))/Wave=dupWave
                Wave convertWave = convertWaveUnits(dupWave, "fromBase")
                thisVar = calcWaveAvg(convertWave)
                killwaves/Z dupWave
            else
                thisVar = calcWaveAvg(thisWave)
            endif
        else
            thisVar = NaN
        endif
    endfor
end


function [variable seriesTime, variable duration] calcSeriesTimeAndDuration(Wave seriesWave)
    duration = NaN
    seriesTime = NaN
    if(WaveExists(seriesWave))
        duration =  str2num( stringbykey( "DURATION", note( seriesWave ) ) )
        if( numtype( duration ) != 0 )
            duration = rightx( seriesWave ) - leftx( seriesWave )
        endif

        seriesTime = PMsecs2Igor(str2num(stringbykey("START",note(seriesWave))))//acqtime(removequotes(seriesString)))
    endif

    return [seriesTime, duration]
end

function [variable count, variable freq] calcSeriesCountAndFreq(Wave ptbWave, variable duration)
    count = NaN
    freq = NaN
    if(WaveExists(ptbWave))
        count = numpnts(ptbWave)
    endif

    if(numtype(count) == 0)
        if(numType(duration) == 0 && duration > 0)
            freq = count / duration
        endif
    endif
    return [count, freq]
end