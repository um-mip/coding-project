function updateSeriesEventInfo(string cellName, variable seriesNum, variable prevDetection[, variable convertUnits, string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif

    DFREF panelDFR = getEventsPanelDF()

    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0
        endif
    endif

    string subType = getSelectedDispType()
    
    if(stringMatch(subType, "specify"))
        print "updateSeriesEventInfo: Only show one subset for all events. Defaulting to show all"
        subType = "all"
    endif
    
    Struct outputWaves outputWaves
    fillOutWavesStrings(outputWaves)
    string analysis_types = outputWaves.seriesEventsAnalysisTypes
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    DFREF indivSeriesOutDFR = getEventsIndivSeriesOutDF()

    if(!prevDetection)
        // Print "updateCellRegionEventsTable: No analysis exists for selected cell/analysis name combo"

        for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
            nameAnalysis = StringFromList( iAnalysis, analysis_types )
            if(stringmatch(nameAnalysis, "area"))
                nameAnalysis = "area2"
            endif

            // Replace values for the display waves with nan
            WAVE ansysWave = indivSeriesOutDFR:$nameAnalysis
            if(WaveExists(ansysWave))    
                redimension/N=0 ansysWave
                ansysWave = nan
            endif
        endfor
    else
        string seriesString = buildSeriesWaveName(cellName, seriesNum)
        DFREF eventsDFR = returnSeriesEventsDFR_bySubType(cellName, num2str(seriesNum), subType)

        if(dataFolderRefStatus(eventsDFR)==0)
            print "updateSeriesEventInfo: Couldn't find DFR where events are supposed to be stored"
            return 0
        endif

        for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
            nameAnalysis = StringFromList( iAnalysis, analysis_types )

            string seriesAnalysisWN = seriesString + "_" + nameAnalysis

            if(stringmatch(nameAnalysis, "area"))
                nameAnalysis = "area2"
            endif

            Wave analysisWave = eventsDFR:$seriesAnalysisWN

            // Replace values for the display waves with the value of the selected cell/region
            if(WaveExists(analysisWave))
                duplicate/O analysisWave, indivSeriesOutDFR:$nameAnalysis
                Wave/SDFR = indivSeriesOutDFR thisWave = $nameAnalysis
                if(convertUnits == 1)
                    Wave convert = convertWaveUnits(thisWave, "fromBase")
                    thisWave[] = convert[p]
                endif
            else
                // Replace values for the display waves with nan if wave doesn't exist
                WAVE ansysWave2 = indivSeriesOutDFR:$nameAnalysis
                redimension/N=0 ansysWave2
                ansysWave2 = nan
            endif
        endfor
    endif

end

function updateSeriesSummary(string cellName, variable seriesNum, variable prevDetection[, string panelName])
    DFREF panelDFR = getEventsPanelDF()
    variable makeBlankTable
    
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif

    string subType = getSelectedDispType()

    if(prevDetection)
        makeBlankTable = 0
    else
        makeBlankTable = 1
    endif

    string seriesString = buildSeriesWaveName(cellName, seriesNum)
    DFREF cellDFR = getEventsdetectWavesDF(cellName)

    string seriesName = num2str(seriesNum)

    if(!makeBlankTable)
        variable dfrStatus = DataFolderRefStatus(cellDFR)

        if(dfrStatus == 0)
            makeBlankTable = 1
        endif
    endif

    string analysis_types = "sumCount;sumTime;sumFreq;pks;int;der;t50r;fwhm;decay9010;"
    string variable_names = "numEvents;seriesTime;freq;relPeak;interval;derivative;riseTime;fwhm;decay9010;"
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    string nameVariable = ""
    DFREF seriesSumOutDFR = getEventsSeriesSumOutDF()

    // Print "updateCellRegionEventsTable: No analysis exists for selected cell/analysis name combo"
    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameAnalysis = StringFromList( iAnalysis, analysis_types )
        nameVariable = StringFromList( iAnalysis, variable_names )
    
        DFREF seriesDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, subType, param = nameAnalysis, outType = "seriesSummaryTable", eventsOrSum = "sum")
        
        if(dataFolderRefStatus(seriesDFR)==0)
            continue
        endif

        // Replace values for the display waves with nan
        WAVE ansysWave = seriesSumOutDFR:$nameAnalysis
        ansysWave = nan

        if(!makeBlankTable)
            NVAR/Z/SDFR = seriesDFR avgVar = $nameVariable
            variable thisAvg = NaN
            if(numType(avgVar) == 0)
                thisAvg = avgVar
            endif
            ansysWave[0] = thisAvg
        endif
    endfor

    // updateSeriesSummaryDisplay(panelName = panelName) // this seems to be duplicated, maybe to handle when updating but not the one displayed?
    updateSeriesSummaryDisplay(cellName = cellName, seriesNum = seriesNum, prevDetection = prevDetection, panelName = panelName)
end

///////////
/// updateSeriesSummaryDisplay
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// NOTES: Use the information about the selected cell, series, graph type, and parameter
/// to update the series summary display
///////////
function updateSeriesSummaryDisplay([cellName, seriesNum, prevDetection, panelName])
    // Get selection information
    string cellName, panelName
    variable seriesNum, prevDetection
    variable validCell, prevPassives
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif
    if(paramIsDefault(cellName) || paramIsDefault(seriesNum) || paramIsDefault(prevDetection))
        [cellName, seriesNum, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = panelName)

    endif

    
    string selDisplay = getSelectedItem("displayTypeListBox_series", hostName = panelName)
    string selParameter = getSelectedItem("paramListBox_series", hostName = panelName)

    string subType = getSelectedDispType()

    // Clear the Display
    string sumGraphName = panelName + "#seriesSumGraph"
    clearDisplay(sumGraphName)

    if(strlen(selDisplay) == 0)
        return 0
    endif

    // Decisions about whether or not to clear the display
    DFREF panelDFR = getEventsPanelDF()
    variable makeBlankDisplay
    
    if(prevDetection)
        makeBlankDisplay = 0
    else
        makeBlankDisplay = 1
    endif

    string seriesString = buildSeriesWaveName(cellName, seriesNum)

    string seriesName = num2str(seriesNum)

    DFREF seriesSumOutDFR = getEventsSeriesSumOutDF()

    // clear the average wave that is plotted
    WAVE/Z averageEvent = seriesSumOutDFR:averageEvent
    if(WaveExists(averageEvent))
        averageEvent = nan
    endif

    WAVE/Z seriesSumTrace = seriesSumOutDFR:seriesSumTrace
    if(WaveExists(seriesSumTrace))
        seriesSumTrace = nan
    endif

    STRUCT seriesSummaryInfo seriesSum
    fillStringsinSeriesSummaryInfo(seriesSum)

    string specDisplay = stringbykey(selDisplay, seriesSum.appendixFromNiceName)
    string specAnalysis = stringbykey(selParameter, seriesSum.appendixFromNiceName)

    if(stringmatch(specDisplay,"dist"))
        if(!strlen(selParameter)>0) // no parameter selected
            makeBlankDisplay = 1
        endif
    endif

    if(makeBlankDisplay)
        return 0
    endif

    variable plotAvg = 0, plotDist = 0
    strswitch (specDisplay)
        case "ave":
            plotAvg = 1
            break
        case "nave":
            plotAvg = 1
            break
        case "dist":
            plotDist = 1
            break
        default:
            
            break
    endswitch

    string waveN = ""
    if(plotAvg)
        waveN = seriesString + "_" + specDisplay

        strswitch (subType)
            case "all":
                print "For displaying the average trace, resorting to default, not all events"
                subType = "specify"
                break
            case "interval":
                print "For displaying the average trace, resorting to default, not intervals subset"
                subType = "specify"
                break
            default:
                
                break
        endswitch

        DFREF seriesDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, subType, param = specDisplay, eventsOrSum = "sum", outType = "seriesAvgDisplay")
        Wave waveToPlot = seriesDFR:$waveN
        if(!WaveExists(waveToPlot))
            return 0
        endif

        duplicate/O waveToPlot, seriesSumOutDFR:seriesSumTrace/Wave=sumWave

        AppendToGraph/W=$sumGraphName sumWave
        if(stringMatch(specDisplay,"nave"))
            Label/W=$sumGraphName/Z left, "normalized current"
        else
            Label/W=$sumGraphName/Z left, "current"
        endif
        Label/W=$sumGraphName/Z bottom, "elapsed time"

        ModifyGraph/W=$sumGraphName gfSize=14
        ModifyGraph/W=$sumGraphName rgb(seriesSumTrace) = (0,0,0)
        SetAxis/A/E=3/W=$sumGraphName bottom
        SetAxis/A/E=3/W=$sumGraphName left
        return 0
    endif

    if(plotDist)
        waven = seriesString + "_" + specAnalysis + "_" + specDisplay
        
        DFREF distDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, subType, param = specAnalysis, eventsOrSum = "sum", outType = "seriesDistDisplay")

        Wave waveToPlot = distDFR:$waveN
        if(!WaveExists(waveToPlot))
            return 0
        endif

        duplicate/O waveToPlot, seriesSumOutDFR:seriesSumTrace/Wave=sumWave

        variable sortDir = appendDistWave(sumWave, displayHost = sumGraphName)
        setDistPlotAxes(displayHost = sumGraphName, sortDir = sortDir, bottomLabel = selParameter, leftLabel = "proportion of events")
    endif
end
