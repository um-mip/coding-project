function updateSeriesOutProc(B_Struct): ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        variable recalc = NumberByKey("recalc", B_Struct.userdata)
        updateSeriesOutTable(reAvgSeries = recalc)
    endif
end

function updateSeriesOutTable([variable reAvgSeries])
    if(paramIsDefault(reAvgSeries))
        reAvgSeries = 0 // default to not reaverage the series
    endif
    
    clearUnnamedCellRows()

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName, groupNames2 = groupName2, groupNames3 = groupName3

    DFREF seriesOutDFR = getSeriesOutputDF()
    Struct outputWaves outWaves
    StructFill/SDFR=seriesOutDFR outWaves

    redimension/N=0 outWaves.seriesName, outWaves.groupName, outWaves.groupName2, outWaves.groupName3, outWaves.confDetect, outWaves.duration, outWaves.sumCount, outWaves.sumTime, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.decay9010, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc

    if(!WaveExists(cellNames))
        print "updateSeriesOutTable: cellName wave doesn't exist"
        return 0
    endif

    variable numCells = numpnts(cellNames)
    variable iCell = 0
    variable numAddedSeries = 0
    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]
        string groupName = groupNames[iCell]
        string groupName2 = groupNames2[iCell]
        string groupName3 = groupNames3[iCell]
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
        variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
        if(dfrStatus == 0)
            continue
        endif

        Wave/T/SDFR=cellInfoDFR Events_series
        if(!WaveExists(Events_series))
            continue
        endif

        variable numSeries = numpnts(Events_series)
        variable iSeries = 0
        for( iSeries = 0; iSeries < numSeries; iSeries++ )
            string seriesName = Events_series[iSeries]
            if(strlen(seriesName) == 0)
                continue
            endif
            
            variable seriesNum = str2num(seriesName)
            redimension/N=(numAddedSeries+1) outWaves.seriesName, outWaves.groupName, outWaves.groupName2, outWaves.groupName3, outWaves.confDetect, outWaves.duration, outWaves.sumCount, outWaves.sumTime, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.decay9010, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc
            outWaves.seriesName[numAddedSeries] = buildSeriesWaveName(cellName, seriesNum)
            outWaves.groupName[numAddedSeries] = groupName
            outWaves.groupName2[numAddedSeries] = groupName2
            outWaves.groupName3[numAddedSeries] = groupName3

            if(reAvgSeries)
                DFREF seriesDFR = getSeriesDF(cellName, seriesName)
                fillPassivesForEventSeries(cellName, str2num(seriesName))
                if(DataFolderRefStatus(seriesDFR) != 0)
                    // Update the avg subset waves
                    calcSeriesAvgSubsetWaves(cellName, seriesNum)

                    // Calculate the averages for this series
                    calcSeriesEventAvgs(cellName, str2num(seriesName))
                endif
            endif

            // Add info to table
            // updateOutWaves(seriesDFR, seriesOutDFR, numAddedSeries)
            variable success = updateOutWaves("series", cellName, numAddedSeries, seriesName = seriesName)
            if(success)
                numAddedSeries++     
            endif
        endfor
    endfor
end

// function updateOutWaves(DFREF seriesDFR, DFREF outDFR, variable numAddedSeries[, DFREF passiveDFR])

///////////
/// updateOutWaves
/// AUTHOR: Amanda Gibson
/// EDIT DATE: 2024-01-18
/// NOTES: This updates the output waves that are already shown in a table
/// used for both the cell and series summary tabs
/// Edited 2024-01-18 to make it so that it looks for the appropriate
/// folder based on the parameter and the state of the display option radio buttons
///////////
function updateOutWaves(string seriesOrCell, string cellName, variable numAddedItem[, string seriesName])
    DFREF outDFR
    
    strswitch (seriesOrCell)
        case "series":
            outDFR = getSeriesOutputDF()
            if(paramIsDefault(seriesName))
                print "updateOutWaves: Updating series, but no series name"
                return 0
            endif
            
            break
        case "cell":
            outDFR = getAllCellsOutputDF()
            break
        default:
            print "updateOutWaves: Did not specify series or cell"
            return 0
            break
    endswitch

    Struct outputWaves outputWaves
    fillOutWavesStrings(outputWaves)
    string analysis_types = outputWaves.analysis_types
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    string nameVariable = ""

    string subType = getSelectedDispType()

    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameAnalysis = StringFromList( iAnalysis, analysis_types )
        nameVariable = stringbykey(nameAnalysis, outputWaves.varFromWave)

        Wave/SDFR = outDFR thisAnalysis = $nameAnalysis

        if(!WaveExists(thisAnalysis))
            continue
        endif

        strswitch (seriesOrCell)
            case "series":
                DFREF dataDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, subType, param = nameAnalysis, eventsOrSum = "sum", outType = "seriesSummaryTable")
                break
            default:
                string selRegion = getSelectedItem("regionListBox_all", hostName = "AGG_events")
                DFREF dataDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = nameAnalysis, outType = "cellDisplay", regionName = selRegion)
                break
        endswitch

        variable thisAvg = NaN
        if(DataFolderRefStatus(dataDFR) != 0)
            NVAR/Z/SDFR = dataDFR avgVar = $nameVariable
            if(numType(avgVar) == 0)
                thisAvg = avgVar
            endif
        endif
        thisAnalysis[numAddedItem] = thisAvg
    endfor
    return 1 // success in added an item
end