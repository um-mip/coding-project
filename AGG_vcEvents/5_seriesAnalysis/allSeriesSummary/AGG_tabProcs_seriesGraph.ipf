function updateGroupGraphProc(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        updateGroupGraphs(winN = LB_Struct.win, paramLBName = LB_Struct.ctrlName)
    endif
    return 0
end

///////////
/// updateGroupGraph
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-18
/// NOTES: This plots both the series and the cell group graphs
///////////
function updateGroupGraphs([string winN, string paramLBName])
    updateSeriesOutTable() // this updates the output waves, should update based on current state of radio buttons
    updateCellsOutTable()
    DFREF panelDFR = getEventsPanelDF()
    DFREF seriesOutDFR = getSeriesOutputDF()
    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR = seriesOutDFR groupNamesSeries = groupName
    Wave/SDFR = cellsOutDFR groupNamesCells = groupName
    
    if(paramIsDefault(winN))
        winN = "AGG_Events"
    endif

    if(paramIsDefault(paramLBName))
        paramLBName = "analysisLB" // not sure what this might have broke, but fixed group series/cell out graphs
        // paramLBName = "parameterText"
    endif
    
    
    ControlInfo/W=$winN $paramLBName
    variable selectedAnalysisNum = V_value
    WAVE/T analysisNames = panelDFR:analysisListW

    STRUCT outputWaves outputWaves
    fillOutWavesStrings(outputWaves)

    if(selectedAnalysisNum < numpnts(analysisNames))
        string analysisLabel = stringfromlist(selectedAnalysisNum, outputWaves.niceNames)
        string selectedAnalysis = stringbykey(analysisLabel, outputWaves.waveFromNiceNames)
        Wave/SDFR = seriesOutDFR analysisWaveSeries = $selectedAnalysis
        if(WaveExists(analysisWaveSeries))
            plotGroupData(groupNamesSeries, analysisWaveSeries, yLabel = analysisLabel, displayhost="AGG_Events#groupSeriesPlot", storageDF = seriesOutDFR)
        else
            // print "couldn't find wave", selectedAnalysis, "for analysis label", analysisLabel, "without parentheses", analysisLab_noParen
        endif
        Wave/SDFR = cellsOutDFR analysisWaveCells = $selectedAnalysis
        if(WaveExists(analysisWaveCells))
            plotGroupData(groupNamesCells, analysisWaveCells, yLabel = analysisLabel, displayhost="AGG_Events#groupCellsPlot", storageDF = cellsOutDFR)
        else
            // print "couldn't find wave", selectedAnalysis, "for analysis label", analysisLabel, "without parentheses", analysisLab_noParen
        endif
    endif
end
