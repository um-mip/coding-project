function getRawWaveDuration (rawWave)
    Wave rawWave

    variable duration =  str2num( stringbykey( "DURATION", note( rawWave ) ) )
	if( numtype( duration ) != 0 )
		duration = rightx( rawWave ) - leftx( rawWave )
	endif

    return duration
end