///////////
/// TauFitFunc_WithOptions
/// AUTHOR: Jenn Jaime and Amanda Gibson
/// ORIGINAL DATE: 2022-07-06
/// NOTES: This function allows you to find the tau of an event
/// it gives options for the different parameters, serving as the basis for the tau fit interface
/// options are
///     doSmoothing - smooth the input wave - default to smooth
///     doNorm - normalize the input wave - default to re-norm
///         if smoothing the wave and not allowing Y0 and A to vary, then should renormalized
///         as smoothing will shift the peak a bit from 0
///     highPercPeak - upper percentage limit to which to fit the exponential function - default to 80
///     lowPercPeak - lower percentage limit to which to fit the exponential function - default to 20
///     allowY0vary - allow Y0 to vary - default to no
///     allowAvary - allow A to vary - default to no
///     allowTauVary - allow tau to vary - default to yes (should basically always be yes)
///     Y0 - starting value of Y0 - default to 0
///     A - starting value of A - default to 1
///     tau - starting value of tau - default to 10
///     useX0constant - provide the X0 constant - default to yes
///     X0constant - X0 value - default to 0
///     fitWaveN - wave name for the generated fit wave - if not provided, appends "_tauFit%%to%%"
///     makeGraph - display the waves - if not provided, defaults to yes
///     displayGraphName - provide a display name - defaults to original wave name +"_tau"+ highPercPeak + "to"+ lowPercPeak
///     extrapolate - extrapolate the fit from the time of the peak to the end of the wave - if not provided, default to no
///     shiftPeakToZero - shift the peak time of the working wave to zero. Makes it easier to specify that X0 should be zero. Default to yes
///
/// this function uses the exp_XOffset function to do the curve fitting
/// if it can't find a level crossing for the upper % limit, it will fit from the time of the peak
/// if it can't find a level crossing for the lower % limit, it will fit to the end of the provided wave
///
/// this function generates a plot - this could be extracted out and handled in a different function if preferred
/// Could provide additional optional parameters to control the colors of the created plots
///////////
function [variable fitTau, variable fitY0, variable fitA, variable extendX0constant, variable decayTime] TauFitFunc_WithOptions(wave avgWave[, variable doSmoothing, variable doNorm, variable highPercPeak, variable lowPercPeak, variable allowY0vary, variable allowAvary, variable allowTauVary, variable y0, variable A, variable Tau, variable useX0constant, variable X0constant, string fitWaveN, variable makeGraph, string displayGraphName, variable extrapolate, variable shiftPeakToZero])
    variable minPeak, timeofMinPeak, workingMinPeak, workingTimeMinPeakOrig, workingTimeMinPeak
    variable highPercPeakVal, highPercPeakTime, lowPercPeakVal, lowPercPeakTime

    variable startTimeOrig, endTimeOrig, startTime, endTime

    if(paramIsDefault(doSmoothing))
        doSmoothing = 1 // default to smooth
    endif

    if(paramIsDefault(doNorm))
        doNorm = 1 // default to re-norm
    endif
    
    if(paramIsDefault(highPercPeak))
        highPercPeak = 80 // default to 80
    endif

    if(paramIsDefault(lowPercPeak))
        lowPercPeak = 20 // default to 20
    endif

    if(paramIsDefault(allowY0vary))
        allowY0vary = 0 // default to no
    endif

    if(paramIsDefault(allowAvary))
        allowAvary = 0 // default to no
    endif

    if(paramIsDefault(allowTauvary))
        allowTauvary = 1 // default to yes
    endif

    if(paramIsDefault(Y0))
        Y0 = 0 // default to 0
    endif

    if(paramIsDefault(A))
        A = -1 // default to -1
    endif

    if(paramIsDefault(Tau))
        Tau = 10 // start guess 10
    endif

    if(paramIsDefault(useX0constant))
        useX0constant = 1 // default to yes
    endif
    
    if(paramIsDefault(X0constant))
        X0constant = 0 // default to 0
    endif

    if(paramIsDefault(makeGraph))
        makeGraph = 1 // default to yes
    endif

    if(paramIsDefault(extrapolate))
        extrapolate = 0 // default to not extrapolate
    endif

    if(paramIsDefault(shiftPeakToZero))
        shiftPeakToZero = 1 // default to shift
    endif
    

    string origWaveN
    DFREF origWaveDF
    [origWaveN, origWaveDF] = getWaveNameAndDFR(avgWave)

    [minPeak, timeofMinPeak] = getMinValues(avgWave)
    // print "The minimum peak of the original wave is", minPeak, "which occurs at time", timeofMinPeak

    if(paramIsDefault(displayGraphName))
        displayGraphName = origWaveN+"_tau"+num2str(highPercPeak)+ "to"+ num2str(lowPercPeak)
    endif

    variable graphExists = 0
    if(WinType(displayGraphName)!=0) // if it already exists
        // print "Graph already exists"
        graphExists = 1
    endif

    // copy so that if the scale is being changed to shift to zero, 
    // we don't change the original wave
    // 2024-01-05, moved out of makeGraph check - make even if not
    // plotting now, so that the graph exists if want to recreate plot
    string origWaveToPlotN = origWaveN + "_plot"
    duplicate/O avgWave, origWaveDF:$origWaveToPlotN
    Wave origWavePlot = origWaveDF:$origWaveToPlotN

    // Make the graph
    if(makeGraph)
        if(graphExists)
            clearDisplay(displayGraphName)
            string hostN, subWinN
            [hostN, subWinN] = splitFullWindowName(displayGraphName)
            DoWindow/F $hostN // should only work if not a subwindow
        else
            AGG_makeDisplay(40, 5, 80, 50, displayGraphName)
        endif
        SetActiveSubWindow $displayGraphName
        appendtograph/W=$(displayGraphName) origWavePlot
        makeColorGrey(origWaveToPlotN, displayName = displayGraphName)
    endif

    string workingWaveN = origWaveN
    Wave workingWave = origWaveDF:$origWaveN

    string smoothStrVarN = origWaveN + "_smthVar"
    string normStrVarN = origWaveN + "_normVar"
    string fitStrVarN = origWaveN + "_fitVar"

    string/G origWaveDF:$smoothStrVarN/N=smoothStrVar
    string/G origWaveDF:$normStrVarN/N=normStrVar
    string/G origWaveDF:$fitStrVarN/N=fitStrVar
    
    smoothStrVar = ""; normStrVar = ""; fitStrVarN = ""

    if(doSmoothing)
        // print "smoothing the normalized raw event:"

        workingWaveN = origWaveN + "_smthTau"
        duplicate/O avgWave, origWaveDF:$workingWaveN
        Wave workingWave = origWaveDF:$workingWaveN

        Smooth/B 9, workingWave

        // Add smoothed wave to graph
        if(makeGraph)
            appendtograph/W=$(displayGraphName) workingWave
            makeColorGreen(nameofWave(workingWave), displayName = displayGraphName)
        endif
        string smoothWaveN = workingWaveN
        smoothStrVar = smoothWaveN
        wave smoothWave = workingWave
    endif

    if(doNorm)
        // print "re-norming the wave:"

        Wave newWorkingWave = AGG_normalizeWave(workingWave)
        string newWaveN
        DFREF newWaveDF
        [newWaveN, newWaveDF] = getWaveNameAndDFR(newWorkingWave)

        Wave workingWave = newWaveDF:$newWaveN

        // Add normed wave to graph
        if(makeGraph)
            appendtograph/W=$(displayGraphName) workingWave
            makeColorSky(nameofWave(workingWave), displayName = displayGraphName)
        endif
        string normWaveN = newWaveN
        normStrVar = normWaveN
        wave normWave = workingWave
    endif

    [workingMinPeak, workingTimeMinPeakOrig] = getMinValues(workingWave)
    variable workingTimeMinPeakInPnts = x2pnt(workingWave, workingTimeMinPeakOrig)
    // print "The minimum peak of the working wave is", workingMinPeak, "which occurs at time", workingTimeMinPeakOrig

    highPercPeakVal = workingMinPeak * highPercPeak / 100
    lowPercPeakVal = workingMinPeak * lowPercPeak / 100

    // print "We'll be looking for a value of", highPercPeakVal, "which is", highPercPeak, "% of the working minimum peak"
    // print "We'll be looking for a value of", lowPercPeakVal, "which is", lowPercPeak, "% of the working minimum peak"

    startTimeOrig=pnt2x(workingWave,0)
    endTimeOrig=pnt2x(workingWave, numpnts(workingWave)-1)

    if(shiftPeakToZero)
        // shift to make the peak occur at 0
        // print "We're shifting the x-axis scale by", workingTimeMinPeakOrig, "to set the peak of the working wave at 0"
        SetScale/I x, startTimeOrig - workingTimeMinPeakOrig, endTimeOrig - workingTimeMinPeakOrig, workingWave

        SetScale/I x, startTimeOrig - workingTimeMinPeakOrig, endTimeOrig - workingTimeMinPeakOrig, origWavePlot
        if(doSmoothing)
            SetScale/I x, startTimeOrig - workingTimeMinPeakOrig, endTimeOrig - workingTimeMinPeakOrig, smoothWave
        endif
        if(doNorm)
            SetScale/I x, startTimeOrig - workingTimeMinPeakOrig, endTimeOrig - workingTimeMinPeakOrig, normWave
        endif

        startTime = startTimeOrig - workingTimeMinPeakOrig
        endTime = endTimeOrig - workingTimeMinPeakOrig
        workingTimeMinPeak = workingTimeMinPeakOrig - workingTimeMinPeakOrig // should be zero
    else
        startTime = startTimeOrig
        endTime = endTimeOrig
        workingTimeMinPeak = workingTimeMinPeakOrig
    endif

    // Find the times where the working wave crosses the specified levels
    findlevel/R= (workingTimeMinPeak, endtime)/Q workingWave, highPercPeakVal
    if(V_flag==0)
        highPercPeakTime=V_levelX
    else
        print "WARNING!!! THE VALUE OF", highPercPeakVal, "(", highPercPeak, "% of the working minimum peak), WAS NOT FOUND. DEFAULTING TO THE TIME OF THE PEAK"
        highPercPeakTime = workingTimeMinPeak
    endif

    findlevel/R= (workingTimeMinPeak, endtime)/Q workingWave, lowPercPeakVal
    if(V_flag==0)
        lowPercPeakTime=V_levelX
    else
        print "WARNING!!! THE VALUE OF", lowPercPeakVal, "(", lowPercPeak, "% of the working minimum peak), WAS NOT FOUND. DEFAULTING TO THE END OF THE WAVE"
        lowPercPeakTime = endTime
    endif

    decayTime = lowPercPeakTime - highPercPeakTime

    // Coefficient wave
    make/O/N=3/D tauCoefs
    tauCoefs[0] = Y0
    tauCoefs[1] = A
    tauCoefs[2] = tau / 1000 // convert to s

    // Generate a fit wave name if not provided
    if(paramIsDefault(fitWaveN))
        fitWaveN = nameofWave(workingWave) + "_TauFit"+num2str(highPercPeak)+ "to"+ num2str(lowPercPeak)
    endif

    if(stringmatch(fitWaveN, origWaveN))
        print "Warning: fit name and original wave name are the same, adding tau fit to end"
        fitWaveN = nameofWave(workingWave) + "_TauFit"+num2str(highPercPeak)+ "to"+ num2str(lowPercPeak)
    endif

    string currentName
    DFREF currentDF
    [currentName, currentDF] = getWaveNameAndDFR(workingWave)
    duplicate/O workingWave, currentDF:$fitWaveN /Wave=fitWave
    fitStrVar = fitWaveN // save so can plot later

    // Make everything not a number, because the fit is only
    // going to replace the values for the x-range provided
    fitWave = NaN

    // Generate the hold string for the flag based on user choices
    string holdString = ""
    if(allowY0vary)
        holdString += "0"
    else
        holdString += "1"
    endif
    if(allowAvary)
        holdString += "0"
    else
        holdString += "1"
    endif
    if(allowTauvary)
        holdString += "0"
    else
        holdString += "1"
    endif
    
    // Need to capture the X0constant in case user wants to extend the fit function
    // Start with the provided value, but if not using the provided value, we'll replace with
    // the value generated by the curvefit exp_Xoffset function
    extendX0constant = X0constant
    if(useX0constant)
        curvefit/Q/K={X0constant}/H=holdString exp_XOffset kwCWave = tauCoefs, workingWave(highPercPeakTime, lowPercPeakTime) /D=fitWave
        
    else
        curvefit/Q/H=holdString exp_XOffset kwCWave = tauCoefs, workingWave(highPercPeakTime, lowPercPeakTime) /D=fitWave
        Wave W_fitConstants
        extendX0constant = W_fitConstants[0]
    endif

    fitY0 = tauCoefs[0]
    fitA = tauCoefs[1]
    fitTau = tauCoefs[2] * 1000 // convert to ms

    if(extrapolate)
        // extrapolate the fit from the time of the peak to the end time
        // print workingMinPeak, "working min peak", workingMinPeak, "working min peak in points"
        fitWave[workingTimeMinPeakInPnts, numpnts(fitWave)-1] = tauCoefs[0]+tauCoefs[1]*exp(-(x-extendX0constant)/tauCoefs[2])
    endif

    // print "The Y0 after running curve fit is", fitY0
    // print "The A after running curve fit is", fitA
    // print "The tau after running curve fit is", fitTau, "ms"

    // Add fit wave to graph
    if(makeGraph)
        appendtograph/W=$(displayGraphName) fitWave
        makeColorPretty(fitWaveN, displayName = displayGraphName)

        if(doNorm)
            sueApprovedYAxis()    
        endif
    endif

    return [fitTau, fitY0, fitA, extendX0constant, decayTime]
end



///////////
/// fitTauForTopGraph
/// AUTHOR: Jenn Jaime
/// EDITOR: Amanda Gibson
/// UPDATED: 2022-07-06
/// NOTES: This will apply TauFitFunc_WithOptions for each wave in the top graph
/// See the notes on that function for the optional parameters and their associated defaults
/// Difference here is that fitWaveN will append to the existing name, as there could be multiple waves in the top graph
///////////
function fitTauForTopGraph([variable doSmoothing, variable doNorm, variable highPercPeak, variable lowPercPeak, variable allowY0vary, variable allowAvary, variable allowTauVary, variable y0, variable A, variable Tau, variable useX0constant, variable X0constant, string fitWaveN, variable makeGraph, variable extrapolate, variable shiftPeakToZero])
    Wave/Wave theWaves = gettheWavesFromtheTopGraph() //pull all of the waves that exist on the top graph first
    Variable numofwaves = numpnts (theWaves) //IGOR will count the total number of waves that exist
    print "number of waves:", numofwaves

    if(paramIsDefault(doSmoothing))
        doSmoothing = 1 // default to smooth
    endif

    if(paramIsDefault(doNorm))
        doNorm = 1 // default to re-norm
    endif
    
    if(paramIsDefault(highPercPeak))
        highPercPeak = 80 // default to 80
    endif

    if(paramIsDefault(lowPercPeak))
        lowPercPeak = 20 // default to 20
    endif

    if(paramIsDefault(allowY0vary))
        allowY0vary = 0 // default to no
    endif

    if(paramIsDefault(allowAvary))
        allowAvary = 0 // default to no
    endif

    if(paramIsDefault(allowTauvary))
        allowTauvary = 1 // default to yes
    endif

    if(paramIsDefault(Y0))
        Y0 = 0 // default to 0
    endif

    if(paramIsDefault(A))
        A = -1 // default to -1
    endif

    if(paramIsDefault(Tau))
        Tau = 10 // start guess 10
    endif

    if(paramIsDefault(useX0constant))
        useX0constant = 1 // default to yes
    endif
    
    if(paramIsDefault(X0constant))
        X0constant = 0 // default to 0
    endif

    variable useFitWaveN = 0
    if(!paramIsDefault(fitWaveN))
        useFitWaveN = 1
    endif

    if(paramIsDefault(makeGraph))
        makeGraph = 1 // default to yes
    endif

    if(paramIsDefault(extrapolate))
        extrapolate = 0 // default to not extrapolate
    endif

    if(paramIsDefault(shiftPeakToZero))
        shiftPeakToZero = 1 // default to shift
    endif
    

    variable iwave = 0
    make/O/N=(numofwaves)/D tauFits, Y0Fits, AFits, X0constants, decayTimes
    make/O/N=(numofwaves)/T topGraphWaveNs

    for (iWave=0; iWave<numofwaves; iwave++)
        wave thiswave = theWaves[iWave]
        if (waveExists(thiswave)) ///for loop that will loop and run each tau fit function on each wave if more than one exsists///

            string thisWaveN = nameOfWave(thisWave)
            print "name of wave:", thisWaveN
            topGraphWaveNs[iWave] = thisWaveN

            variable thisTau, fitY0, fitA, extendX0constant, decayTime
            if(useFitWaveN)
                [thisTau, fitY0, fitA, extendX0constant, decayTime] = TauFitFunc_WithOptions(thisWave, doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, fitWaveN = thisWaveN + fitWaveN, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
            else
                [thisTau, fitY0, fitA, extendX0constant, decayTime] = TauFitFunc_WithOptions(thisWave, doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
            endif

            // Add tau value to wave
            if(numType(thisTau)==0)
                tauFits[iWave] = thisTau
                Y0Fits[iWave] = fitY0
                AFits[iWave] = fitA
                X0constants[iWave] = extendX0constant
                decayTimes[iWave] = decayTime
            endif
        endif
    endfor

    AGG_makeTable(0, 62, 42, 90, "tauFits")
    AppendToTable topGraphWaveNs, decayTimes, tauFits, Y0Fits, AFits, X0constants
end


///////////
/// makeTauPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-06
/// NOTES: Creates a basic user interface panel for selecting the parameters for the tau fit function to be run on the top graph
/// Uses the function fitTauForGraph, which relies on TauFitFunc_withOptions
///////////
function/S makeTauPanel([variable showFitButtons])
    if(WinType("tauInterface")!=0)
		// print "panelExists"
		KillWindow/Z tauInterface
	endif

    if(paramIsDefault(showFitButtons))
        showFitButtons = 1 // default to show
    endif
    

	NewDataFolder/O tauPanel
	DFREF tauPanel = root:tauPanel

    variable/G tauPanel:doSmoothing/N=doSmoothing
    variable/G tauPanel:doNorm/N=doNorm
    variable/G tauPanel:highPercPeak/N=highPercPeak
    variable/G tauPanel:lowPercPeak/N=lowPercPeak
    variable/G tauPanel:allowY0vary/N=allowY0vary
    variable/G tauPanel:allowTauvary/N=allowTauvary
    allowTauVary = 1
    variable/G tauPanel:allowAvary/N=allowAvary
    variable/G tauPanel:Y0/N=Y0
    variable/G tauPanel:tau/N=tau
    variable/G tauPanel:A/N=A
    variable/G tauPanel:useX0constant/N=useX0constant
    variable/G tauPanel:X0constant/N=X0constant
    variable/G tauPanel:useFitName/N=useFitName
    string/G tauPanel:fitWaveN/N=fitWaveN
    variable/G tauPanel:makeGraph/N=makeGraph
    variable/G tauPanel:extrapolate/N=extrapolate
    variable/G tauPanel:shiftPeakToZero/N=shiftPeakToZero

	variable panelWidth = 32, panelHeight = 50
	variable panelX = 0, panelY = 5
	string panelName = "tauInterface"
	AGG_makePanel(panelX, panelY, panelX + panelWidth, panelY + panelHeight, panelName)

	DefaultGuiFont/W=# popup={"_IgorMedium",12,0}

	variable xPos = posRelPanel(0.02, "width")
	variable yPos = posRelPanel(0.01, "height")
	variable buttonWidth, buttonHeight
	[buttonWidth, buttonHeight] = xyRelPanel(0.45, 0.075)

    // Smooth
    CheckBox doSmoothingCheck title = "Smooth the wave", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = doSmoothing, fstyle = 1, fsize = 12
    yPos += buttonHeight

    // Normalize
    CheckBox doNormCheck title = "Normalize wave", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = doNorm, fstyle = 1, fsize = 12
    yPos += buttonHeight

    // Upper % peak limit
    if(numType(highPercPeak)!=0 || highPercPeak == 0)
		highPercPeak = 80
	endif

    TitleBox highPercPeakPickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Upper % peak limit:", frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight
    SetVariable highPercPeakPick value = highPercPeak, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

    // Lower % peak limit
    if(numType(lowPercPeak)!=0 || lowPercPeak == 0)
		lowPercPeak = 20
	endif
    TitleBox lowPercPeakPickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Lower % peak limit:", frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight

    SetVariable lowPercPeakPick value = lowPercPeak, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

    // Fit name
    // yPos += buttonHeight * 0.2
    CheckBox useFitNameCheck title = "Use custom fit name", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = useFitName, fstyle = 1, fsize = 12
    yPos += buttonHeight

    TitleBox fitNameText title = "Custom fit name appendix:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fsize = 12
	yPos += buttonHeight

	SetVariable fitNameToPick value = fitWaveN, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12
	yPos += buttonHeight

    // Make graphs
    // yPos += buttonHeight * 0.2
    CheckBox makeGraphCheck title = "Make graphs", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = makeGraph, fstyle = 1, fsize = 12
	yPos += buttonHeight

    // Extrapolate
    CheckBox extrapolateCheck title = "Extrapolate fit", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = extrapolate, fstyle = 1, fsize = 12
	yPos += buttonHeight

    if(showFitButtons)
        // Do it!
        Button fitButton title = "FIT TOP GRAPH", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = fitTauTopGraphProc
        yPos += buttonHeight

        // Do it!
        Button fitButtonAllCells title = "FIT ALL CELLS", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = fitTauAllCellsProc
        yPos += buttonHeight
    endif


    // Second column
    xPos = posRelPanel(0.52, "width")
    yPos = posRelPanel(0.01, "height")
    // Y0
    // yPos += buttonHeight * 0.2
    CheckBox allowY0varyCheck title = "Allow Y0 to vary", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = allowY0vary, fstyle = 1, fsize = 12
    yPos += buttonHeight

    if(numType(Y0)!=0 || Y0 == 0)
		Y0 = 0
	endif
    TitleBox Y0PickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Starting value for Y0:", frame = 0, fsize = 12
	yPos += buttonHeight
    
    SetVariable Y0Pick value = Y0, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

    // A
    // yPos += buttonHeight * 0.2
    CheckBox allowAvaryCheck title = "Allow A to vary", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = allowAvary, fstyle = 1, fsize = 12
    yPos += buttonHeight

    if(numType(A)!=0 || A == 0)
		A = -1
	endif
    TitleBox APickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Starting value for A:", frame = 0, fsize = 12
	yPos += buttonHeight
    
    SetVariable APick value = A, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

    // Tau
    // yPos += buttonHeight * 0.2
    CheckBox allowTauCheck title = "Allow tau to vary", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = allowTauvary, fstyle = 1, fsize = 12
    yPos += buttonHeight

    if(numType(Tau)!=0 || Tau == 0)
		Tau = 10
	endif
    TitleBox TauPickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Starting value for Tau (ms):", frame = 0, fsize = 12
	yPos += buttonHeight
    
    SetVariable TauPick value = Tau, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

    // Shift peak to zero
    // yPos += buttonHeight * 0.2
    CheckBox shiftPeakToZeroCheck title = "Shift peak to zero", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = shiftPeakToZero, fstyle = 1, fsize = 12
    yPos += buttonHeight

    // X0
    CheckBox useX0constantCheck title = "Use X0 constant", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, variable = useX0constant, fstyle = 1, fsize = 12
    yPos += buttonHeight

    if(numType(X0constant)!=0 || X0constant == 0)
		X0constant = 0
	endif
    TitleBox X0constantPickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Value for X0 constant:", frame = 0, fsize = 12
	yPos += buttonHeight
    
    SetVariable X0constantPick value = X0constant, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, limits={0, 100, 5}
	yPos += buttonHeight

	
	return panelName
end

///////////
/// fitTauTopGraphProc
/// AUTHOR: Amandad Gibson
/// ORIGINAL DATE: 2022-07-06
/// NOTES: Gets the values from the tau interface panel and runs fitTauForGraph, which relies on TauFitFunc_withOptions
///////////
function fitTauTopGraphProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        DFREF tauPanel = root:tauPanel

        variable/G tauPanel:doSmoothing/N=doSmoothing
        variable/G tauPanel:doNorm/N=doNorm
        variable/G tauPanel:highPercPeak/N=highPercPeak
        variable/G tauPanel:lowPercPeak/N=lowPercPeak
        variable/G tauPanel:allowY0vary/N=allowY0vary
        variable/G tauPanel:allowTauvary/N=allowTauvary
        variable/G tauPanel:allowAvary/N=allowAvary
        variable/G tauPanel:Y0/N=Y0
        variable/G tauPanel:tau/N=tau
        variable/G tauPanel:A/N=A
        variable/G tauPanel:useX0constant/N=useX0constant
        variable/G tauPanel:X0constant/N=X0constant
        variable/G tauPanel:useFitName/N=useFitName
        variable/G tauPanel:makeGraph/N=makeGraph
        variable/G tauPanel:extrapolate/N=extrapolate
        variable/G tauPanel:shiftPeakToZero/N=shiftPeakToZero
        string/G tauPanel:fitWaveN/N=fitWaveN

        if(useFitName)
            fitTauForTopGraph(doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant,fitWaveN = fitWaveN, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
        else
            fitTauForTopGraph(doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
        endif
    endif
    return 0
end

///////////
/// fitTauAllCellsProc
/// AUTHOR: Amandad Gibson
/// ORIGINAL DATE: 2022-07-06
/// NOTES: Gets the values from the tau interface panel and runs runTauForAllWaves, which relies on TauFitFunc_withOptions
/// First gets all of the normalized average waves for each cell, and then runs the fit function to find tau for each cell
///////////
function fitTauAllCellsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        DFREF tauPanel = root:tauPanel

        variable/G tauPanel:doSmoothing/N=doSmoothing
        variable/G tauPanel:doNorm/N=doNorm
        variable/G tauPanel:highPercPeak/N=highPercPeak
        variable/G tauPanel:lowPercPeak/N=lowPercPeak
        variable/G tauPanel:allowY0vary/N=allowY0vary
        variable/G tauPanel:allowTauvary/N=allowTauvary
        variable/G tauPanel:allowAvary/N=allowAvary
        variable/G tauPanel:Y0/N=Y0
        variable/G tauPanel:tau/N=tau
        variable/G tauPanel:A/N=A
        variable/G tauPanel:useX0constant/N=useX0constant
        variable/G tauPanel:X0constant/N=X0constant
        variable/G tauPanel:useFitName/N=useFitName
        variable/G tauPanel:makeGraph/N=makeGraph
        variable/G tauPanel:extrapolate/N=extrapolate
        variable/G tauPanel:shiftPeakToZero/N=shiftPeakToZero
        string/G tauPanel:fitWaveN/N=fitWaveN

        getAllCellNormWaves()

        if(useFitName)
            runTauForAllWaves(doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant,fitWaveN = fitWaveN, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
        else
            runTauForAllWaves(doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
        endif
    endif
    return 0
end

///////////
/// getAllCellNormWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-07
/// NOTES: Duplicate all of the cell norm waves into a different folder and add the cell name to the wave name
///////////
function getAllCellNormWaves()
    KillDataFolder/Z normCellWaves // if stuff is open, it won't delete
    NewDataFolder/O normCellWaves
    DFREF cellWavesDF = normCellWaves

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell]
            string groupName = groupNames[iCell]
            DFREF cellSummaryDF = getCellSummaryDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellSummaryDF)
            if(dfrStatus != 0)
                Wave/T/SDFR=cellSummaryDF cellNAvgEvent
                if(WaveExists(cellNAvgEvent))
                    string cleanGroup = ReplaceString("-", groupName, "")
                    cleanGroup = ReplaceString(" ", cleanGroup, "")
                    if(strlen(cleanGroup)>0)
                        cleanGroup = "_" + cleanGroup
                    endif
                    string newName = "c" + cellName + cleanGroup + "_normAvg"
                    duplicate/O cellNAvgEvent, cellWavesDF:$newName
                endif
            endif
        endfor
    else
        
    endif
end

///////////
/// runTauForAllWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-07
/// NOTES: Runs the TauFitFunc_withOptions for all of the waves that end in normAvg within the wavesDF
/// the wavesDF is defaulted to normCellWaves
/// The options are described in TauFitFunc_withOptions
///////////
function runTauForAllWaves([DFREF wavesDF, variable doSmoothing, variable doNorm, variable highPercPeak, variable lowPercPeak, variable allowY0vary, variable allowAvary, variable allowTauVary, variable y0, variable A, variable Tau, variable useX0constant, variable X0constant, string fitWaveN, variable makeGraph, variable extrapolate, variable shiftPeakToZero])
    if(paramIsDefault(wavesDF))
        wavesDF = normCellWaves
    endif

    if(paramIsDefault(doSmoothing))
        doSmoothing = 1 // default to smooth
    endif

    if(paramIsDefault(doNorm))
        doNorm = 1 // default to re-norm
    endif
    
    if(paramIsDefault(highPercPeak))
        highPercPeak = 80 // default to 80
    endif

    if(paramIsDefault(lowPercPeak))
        lowPercPeak = 20 // default to 20
    endif

    if(paramIsDefault(allowY0vary))
        allowY0vary = 0 // default to no
    endif

    if(paramIsDefault(allowAvary))
        allowAvary = 0 // default to no
    endif

    if(paramIsDefault(allowTauvary))
        allowTauvary = 1 // default to yes
    endif

    if(paramIsDefault(Y0))
        Y0 = 0 // default to 0
    endif

    if(paramIsDefault(A))
        A = -1 // default to -1
    endif

    if(paramIsDefault(Tau))
        Tau = 10 // start guess 10
    endif

    if(paramIsDefault(useX0constant))
        useX0constant = 1 // default to yes
    endif
    
    if(paramIsDefault(X0constant))
        X0constant = 0 // default to 0
    endif

    variable useFitWaveN = 0
    if(!paramIsDefault(fitWaveN))
        useFitWaveN = 1
    endif

    if(paramIsDefault(makeGraph))
        makeGraph = 1 // default to yes
    endif

    if(paramIsDefault(extrapolate))
        extrapolate = 0 // default to not extrapolate
    endif

    if(paramIsDefault(shiftPeakToZero))
        shiftPeakToZero = 1 // default to shift
    endif
    

    if(dataFolderRefStatus(wavesDF)!=0)
        string allWaves
        #if (IgorVersion() < 9.00)
            Variable index = 0
            string objName
            do
                objName = GetIndexedObjNameDFR(wavesDF, 1, index)
                // print objName
                if (strlen(objName) == 0)
                    break
                endif
                if(stringmatch(objName, "*normAvg")) // only waves that end in normAvg
                    allWaves += objName + ";"
                endif
                index += 1
            while(1)
        #endif

        #if (IgorVersion() >= 9.00)
            allWaves = WaveList("*normAvg", ";", "", wavesDF) // only waves that end in normAvg
        #endif

        variable numWaves = itemsInList(allWaves)
        variable iWave = 0
        make/O/N=(numWaves)/D wavesDF:tauFits/Wave=tauFits
        make/O/N=(numWaves)/D wavesDF:Y0Fits/Wave=Y0Fits
        make/O/N=(numWaves)/D wavesDF:AFits/Wave=AFits
        make/O/N=(numWaves)/D wavesDF:X0constants/Wave=X0constants
        make/O/N=(numWaves)/D wavesDF:decayTimes/Wave=decayTimes
        make/O/N=(numWaves)/T wavesDF:cellWaveNames/Wave=cellWaveNames
        tauFits = NaN; Y0Fits = NaN; AFits = NaN; X0constants = NaN; decayTimes = NaN
        for(iWave=0; iWave<numWaves; iWave++)
            string thisWaveN = StringFromList(iWave, allWaves)
            Wave/SDFR=wavesDF thisWave = $thisWaveN
            if(WaveExists(thisWave))
                cellWaveNames[iWave] = thisWaveN
                if(numpnts(thisWave)>0)

                    variable thisTau, fitY0, fitA, extendX0constant, decayTime
                    if(useFitWaveN)
                        [thisTau, fitY0, fitA, extendX0constant, decayTime] = TauFitFunc_WithOptions(thisWave, doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, fitWaveN = thisWaveN + fitWaveN, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
                    else
                        [thisTau, fitY0, fitA, extendX0constant, decayTime] = TauFitFunc_WithOptions(thisWave, doSmoothing = doSmoothing, doNorm = doNorm, highPercPeak = highPercPeak, lowPercPeak = lowPercPeak, allowY0vary = allowY0vary, allowAvary = allowAvary, allowTauVary = allowTauVary, y0 = y0, A = A, Tau = Tau, useX0constant = useX0constant, X0constant = X0constant, makeGraph = makeGraph, extrapolate = extrapolate, shiftPeakToZero = shiftPeakToZero)
                    endif
                    
                    // Add tau value to wave
                    if(numType(thisTau)==0)
                        tauFits[iWave] = thisTau
                        Y0Fits[iWave] = fitY0
                        AFits[iWave] = fitA
                        X0constants[iWave] = extendX0constant
                        decayTimes[iWave] = decayTime
                    endif
                else
                    print thisWaveN, "does not have any points"
                endif
            endif
        endfor
        AGG_makeTable(2, 62, 50, 85, "tauFits")
        AppendToTable cellWaveNames, decayTimes, tauFits, Y0Fits, AFits, X0constants
    endif
end


///////////
/// fitTauForCell
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Uses the tau panel to set the parameters
///////////
function fitTauForCell(string cellName[, variable convertUnits])
    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0 // default to not convert
        endif
    endif

    string subTypes = "avg;limited;"

    variable iSubType = 0, nSubTypes = itemsInList(subTypes)
    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)
        DFREF cellSummaryDF = returnCellEventsDFR_bySubType(cellName, subType)

        fitTauForCell_specSubtype(cellName, cellSummaryDF, convertUnits)

        DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
        if(dataFolderRefStatus(regionDFR)==0)
            // no regionDFR
            continue
        endif
        
        string regionsNamesString = getChildDFRList(regionDFR)
        wave/T regionsNames = listToTextWave(regionsNamesString, ";")
        if(!WaveExists(regionsNames) || numpnts(regionsNames) == 0)
            continue
        endif

        variable iRegion = 0, nRegions = numpnts(regionsNames)
        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = regionsNames[iRegion]
            DFREF cellSummaryDF = returnCellRegionsDFR_bySubType(cellName, subType, regionName = regionName)
            if(dataFolderRefStatus(cellSummaryDF)==0)
                continue
            endif

            fitTauForCell_specSubtype(cellName, cellSummaryDF, convertUnits)
        endfor
    endfor

    // make/DF/FREE/N=2 cellDFRs

    // // Do this for both the avg subset and the limited average subset for the cell
    // cellDFRs[0] = getCellAvgSubDF(cellName)
    // cellDFRs[1] = getCellAvgSubLimitedDF(cellName)

    // variable iDFR = 0, nDFRs = numpnts(cellDFRs)

    // for(iDFR=0; iDFR<nDFRs; iDFR++)
    //     DFREF cellSummaryDF = cellDFRs[iDFR]

    //     fitTauForCell_specSubtype(cellName, cellSummaryDF, convertUnits)
    // endfor
end

///////////
/// fitTauForCell_specSubtype
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-18
/// NOTES: the tau structure doesn't seem to update when you update data folders
/// so this needs to be its own function instead of just in a loop
///////////
function fitTauForCell_specSubtype(string cellName, DFREF cellSummaryDF, variable convertUnits)
    if(dataFolderRefStatus(cellSummaryDF)==0)
        print "fitTauForCell: Couldn't find summary folder for cell", cellName
        return 0
    endif
    
    Wave/SDFR=cellSummaryDF cellNAvgEvent
    if(!WaveExists(cellNAvgEvent))
        print "fitTauForCell: Couldn't find cell's normalized average event", cellName
        return 0
    endif

    if(numpnts(cellNAvgEvent)==0)
        print "fitTauForCell: average event for this cell", cellName, "doesn't have any points"
        return 0
    endif

    Struct tauPanelStruct tp
    DFREF tauDF = root:tauPanel

    if(dataFolderRefStatus(tauDF)==0)
        print "fitTauForCell: tauPanel data folder doesn't exist yet"
        return 0
    endif

    StructFill/SDFR=tauDF tp

    Struct tauFitVarsStruct tauFit
    StructFill/SDFR=cellSummaryDF/AC=1 tauFit
    fillTauFitVarsStruct(tauFit)

    // reset vars to NaN
    variable nVars = itemsInList(tauFit.varNames), iVar = 0
    for(iVar=0; iVar<nVars; iVar++)
        string thisVarN = StringFromList(iVar, tauFit.varNames)
        NVAR/Z/SDFR=cellSummaryDF thisVar = $thisVarN
        thisVar = NaN
    endfor

    variable tau, y0, A, X0, decayTime
    string waveN = nameOfWave(cellNAvgEvent)
    if(tp.useFitName)
        [tau, y0, A, X0, decayTime] = TauFitFunc_WithOptions(cellNAvgEvent, doSmoothing = tp.doSmoothing, doNorm = tp.doNorm, highPercPeak = tp.highPercPeak, lowPercPeak = tp.lowPercPeak, allowY0vary = tp.allowY0vary, allowAvary = tp.allowAvary, allowTauVary = tp.allowTauVary, y0 = tp.y0, A = tp.A, Tau = tp.Tau, useX0constant = tp.useX0constant, X0constant = tp.X0constant, fitWaveN = waveN + tp.fitWaveN, makeGraph = tp.makeGraph, extrapolate = tp.extrapolate, shiftPeakToZero = tp.shiftPeakToZero)
    else
        [tau, y0, A, X0, decayTime] = TauFitFunc_WithOptions(cellNAvgEvent, doSmoothing = tp.doSmoothing, doNorm = tp.doNorm, highPercPeak = tp.highPercPeak, lowPercPeak = tp.lowPercPeak, allowY0vary = tp.allowY0vary, allowAvary = tp.allowAvary, allowTauVary = tp.allowTauVary, y0 = tp.y0, A = tp.A, Tau = tp.Tau, useX0constant = tp.useX0constant, X0constant = tp.X0constant, makeGraph = tp.makeGraph, extrapolate = tp.extrapolate, shiftPeakToZero = tp.shiftPeakToZero)
    endif

    tauFit.tau = tau
    tauFit.y0 = y0
    tauFit.A = A
    tauFit.X0 = X0
    if(convertUnits)
        decayTime = convertVarUnits(decayTime, "decayTime", "fromBase")
    endif
    tauFit.decayTime = decayTime

    string/G cellSummaryDF:tauFitParams/N=fitString
    copyTauPanelParams(tp, tauDF = tauDF)
    StructPut/S tp, fitString
end


///////////
/// fitTauAllCells
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Run through all cells in events panel, save in summary folder, not combined folder
///////////
function fitTauAllCells()
    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    if(!WaveExists(cellNames))
        print "fitTauAllCells: couldn't find cellNames wave. Quitting"
        abort
    endif

    variable numCells = numpnts(cellNames)
    variable iCell = 0
    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]
        fitTauForCell(cellName)
    endfor
end

///////////
/// deleteTauVarsFromGenSumDF
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Run through all cells in events panel, save in summary folder, not combined folder
///////////
function deleteTauVarsFromGenSumDF()
    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName

    if(!WaveExists(cellNames))
        print "deleteTauVarsFromGenSumDF: couldn't find cellNames wave. Quitting"
        abort
    endif

    variable numCells = numpnts(cellNames)
    variable iCell = 0
    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]

        DFREF cellDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellDFR)==0)
            continue 
        endif

        string cellAvgWaves = WaveList("cell*AvgEvent*", ";", "", cellDFR)

        deleteWavesFromList(cellAvgWaves, dfr = cellDFR)

        // string wavesToDelete = "cellNAvgEvent*;cellAvgEvent;"
        string varsToDelete = "A;decayTime;tau;X0;y0;"
        string stringsToDelete = "cellNAvgEvent_fitVar;cellNAvgEvent_normVar;cellNAvgEvent_smthVar;tauFitParams"
        
        // variable iWave, nWaves = itemsinlist(wavesToDelete)
        variable iVar, nVars = itemsinlist(varsToDelete)
        variable iString, nStrings = itemsinlist(stringsToDelete)

        for(iVar=0; iVar<nVars; iVar++)
            string varN = StringFromList(iVar, varsToDelete)
            NVAR/Z/SDFR=cellDFR thisVar = $varN
            KillVariables/Z thisVar
        endfor

        for(iString=0; iString<nStrings; iString++)
            string stringN = StringFromList(iString, stringsToDelete)
            SVAR/SDFR=cellDFR thisString = $stringN
            KillStrings/Z thisString
        endfor
    endfor
end

