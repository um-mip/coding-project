///// Function hierarchy
/// makeAllSubtypeDistWaves
///     makeGroupsDistsWaves (for a specific subtype)
///         makeGroupDistWave (for a specific group and distribution parameter)
///
///
/// Also a function `getGroupDistWaves` to return the waves for a specific distribution wave type

///////////
/// makeAllSubtypeDistWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function makeAllSubtypeDistWaves([string distTypes, wave/T groupWave, wave/T cellNames])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName // user could select a different group wave if desired
    endif

    if(paramIsDefault(cellNames) || !WaveExists(cellNames))
        Wave/SDFR = cellsOutDFR/T cellNames = cellName
    endif
    
    if(!WaveExists(groupWave) || !WaveExists(cellNames))
        print "makeGroupDistWaves: Couldn't find either group wave or cell name wave. Quitting"
        return 0
    endif

    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    Wave/SDFR=cellsOutDFR/T allRegionNames

    if(!WaveExists(allRegionNames))
        print "updateGroupAvgWaves: Can't find allRegionNames wave"
        return NaN
    endif

    variable iRegion = 0, nRegions = numpnts(allRegionNames)

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)

        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = allRegionNames[iRegion]

            if(paramIsDefault(distTypes) || itemsInList(distTypes)<1)
                makeGroupsDistWaves(groupWave = groupWave, cellNames = cellNames, subType = subType, regionName = regionName)
            else
                makeGroupsDistWaves(distTypes = distTypes, groupWave = groupWave, cellNames = cellNames, subType = subType, regionName = regionName)
            endif
        endfor
    endfor
end

///////////
/// makeGroupsDistWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES:
///////////
function makeGroupsDistWaves([string distTypes, wave/T groupWave, wave/T cellNames, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif

    if(paramIsDefault(cellNames) || !WaveExists(cellNames))
        Wave/SDFR = cellsOutDFR/T cellNames = cellName
    endif
    
    if(!WaveExists(groupWave) || !WaveExists(cellNames))
        print "makeGroupDistWaves: Couldn't find either group wave or cell name wave. Quitting"
        return 0
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup, itemsInGroup = 0

    if(paramIsDefault(distTypes) || itemsInList(distTypes)<1)
        distTypes = "peaks;der;t50r;fwhm;decay9010;int;"
    endif
    
    variable iDist = 0, nDists = itemsInList(distTypes)

    for(iGroup = 0; iGroup < numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        Wave/T cellsInGroup = getTextWaveSubsetFromGroup(groupWave, cellNames, groupName = groupName)
        
        for(iDist=0; iDist<nDists; iDist++)
            string distType = StringFromList(iDist, distTypes)
            makeGroupDistWave(distType, groupName, cellsInGroup, subType = subType, regionName = regionName)
        endfor
    endfor
end

///////////
/// makeGroupDistWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
/// 2024-01-21, probably change this so that it outputs all of the waves as a wave/wave
///////////
function/Wave makeGroupDistWave(string distType, string groupName, wave/T cellsInGroup[, string subType, DFREF storageDF, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    if(!WaveExists(cellsInGroup))
        print "makeGroupDistWave: Couldn't find cell names to include. Quitting"
        return $("")
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    if(paramIsDefault(storageDF))
        DFREF storageDF = returnGroupRegionsDFR_bySubType(groupName, subType, param = distType, outType = "cellDisplay", regionName = regionName)
    endif
    
    variable itemsInGroup = numpnts(cellsInGroup)
    
    variable iCell
    variable updatedUnits = 0, peakSign = -1

    make/N=0/FREE/Wave distWaves // may not find a dist wave for each cell in the group, especially once getting into regions
    for(iCell=0; iCell<itemsInGroup; iCell++)
        string cellName = cellsInGroup[iCell]
        DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = distType, outType = "cellDisplay", regionName = regionName)
        
        if(dataFolderRefStatus(cellDFR)==0)
            // print "createGroupDistPlots: couldn't find cell summary df", cellName
            continue
        endif

        Wave/SDFR=cellDFR distWave = $(distType + "_dist")

        if(!WaveExists(distWave))
            print "createGroupDistPlots: couldn't find dist wave", distType, cellName, subType, regionName
            continue
        endif

        if(!updatedUnits)
            string unitsString = getWaveUnits(distWave)
            variable sortDir = NumberByKey("sortDir", note(distWave))
            
            if(numType(sortDir) != 0)
                struct analysisparameters ps
                variable worked = readpanelparams2(ps)
                variable panelPeakSign = ps.peakSign
                if(panelPeakSign == 1 || panelPeakSign == -1)
                    peakSign = panelPeakSign
                endif

                string probDistribution = StringByKey(distType, cellSum.probDistribution)
                strswitch (probDistribution)
                    case "bySign":
                        sortDir = peakSign
                        break
                    case "pos":
                        sortDir = 1
                        break
                    default:
                        sortDir = 1 // default to positive sort
                        break
                endswitch
            endif

            if(strlen(unitsString)>0)
                updatedUnits = 1
            endif
        endif

        distWaves[numpnts(distWaves)] = {distWave} // append to the end            
    endfor

    string groupDistN = distType + "_dist"
    if(numpnts(distWaves)>0)
        concatenate/NP/O {distWaves}, storageDF:$(groupDistN)
        Wave/SDFR=storageDF/D groupDist = $(groupDistN)
        setScale d, 0, 1, unitsString, groupDist
    else
        make/N=0/D/O storageDF:$(groupDistN)/Wave=groupDist
    endif

    variable numConcatPnts = numpnts(groupDist)

    if(numConcatPnts < 1)
        return groupDist
    endif

    Note/K groupDist // get rid of existing note (concats all notes from each cell above)
    probdistp(nameOfWave(distWave), sortDir, distWave = groupDist)

    return groupDist
end

///////////
/// getGroupDistWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function[Wave/T uniqueGroups, Wave/Wave distWaves] getGroupDistWaves(string distType[, wave/T groupWave, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "getGroupDistWaves: Couldn't find either group wave. Quitting"
        return [$(""), $("")]
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup, itemsInGroup = 0

    make/FREE/Wave/N=(numGroups) distWaves

    for(iGroup=0; iGroup<numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        DFREF storageDF = returnGroupRegionsDFR_bySubType(groupName, subType, param = distType, outType = "cellDisplay", regionName = regionName)
        if(dataFolderRefStatus(storageDF)==0)
            continue
        endif

        Wave distWave = storageDF:$(distType+"_dist")
        if(!WaveExists(distWave))
            continue
        endif
        distWaves[iGroup] = distWave
    endfor

    return[uniqueGroups, distWaves]
end
