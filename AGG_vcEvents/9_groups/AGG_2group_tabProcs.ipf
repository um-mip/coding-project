function displayGroupParamsProc(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF groupsGraphDFR = getAllGroupsOutputDF()

        string selDisplay = getSelectedItem("paramListBox_group", hostName = LB_Struct.win)
        if(StringMatch(selDisplay, "distribution plot") || StringMatch(selDisplay, "decimated dist plot"))
            Wave/T/SDFR=groupsGraphDFR titleWave = titleWave_distParams
            Wave/SDFR=groupsGraphDFR selWave = selWave_distParams
        else
            if(stringmatch(selDisplay, "group avg events"))
                Wave/T/SDFR=groupsGraphDFR titleWave = titleWave_groupAvgParams
                Wave/SDFR=groupsGraphDFR selWave = selWave_groupAvgParams
            else
                if(stringmatch(selDisplay, "event property table"))
                    Wave/T/SDFR=groupsGraphDFR titleWave = titleWave_propParams
                    Wave/SDFR=groupsGraphDFR selWave = selWave_propParams
                else
                    make/O/N=0/T groupsGraphDFR:blankTitle/Wave=titleWave
                    make/O/N=0 groupsGraphDFR:blankSel/Wave=selWave
                endif
            endif
        endif
        ListBox paramListBox_groupParams, listWave = titleWave, selWave = selWave
        if(stringmatch(LB_struct.ctrlName, "paramListBox_whichGroup")) // if user is clicking on a different group type, update the group waves
            updateGroupWaves()
        endif
        displayGroupParams()
    endif
    return 0
end

///////////
/// updateGroupWavesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-16
/// NOTES: 
///////////
function updateGroupWavesProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        updateGroupWaves()
    endif
    return 0
end

///////////
/// updateGroupWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-16
/// NOTES: 
///////////
function updateGroupWaves([wave/T groupWave])
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        [string groupWN, string groupLabel] = getSelectedGroup()
            
        DFREF cellsOutDFR = getAllCellsOutputDF()
        Wave/SDFR=cellsOutDFR/T groupWave = $groupWN
        if(!WaveExists(groupWave))
            print "updateGroupDistWavesProc: Couldn't find selected group wave. Defaulting to groupName"
            Wave/SDFR=cellsOutDFR/T groupWave = groupName 
        endif
    endif
    
    makeAllGroupFolders(groupNames = groupWave)
    updateGroupAvgWaves(groupNames = groupWave)
    makeAllSubtypeDistWaves(groupWave = groupWave)
    makeAllSubtypePropWaves(groupWave = groupWave)
end

///////////
/// updateGroupAvgWavesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function updateGroupAvgWavesProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        updateGroupAvgWaves()
    endif
    return 0
end


///////////
/// updateGroupDistWavesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function updateGroupDistWavesProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        [string groupWN, string groupLabel] = getSelectedGroup()
        
        DFREF cellsOutDFR = getAllCellsOutputDF()
        Wave/SDFR=cellsOutDFR groupWave = $groupWN
        if(!WaveExists(groupWave))
            print "updateGroupDistWavesProc: Couldn't find selected group wave. Defaulting to groupName"
            Wave/SDFR=cellsOutDFR groupWave = groupName 
        endif

        makeAllSubtypeDistWaves(groupWave = groupWave)
    endif
    return 0
end

///////////
/// updateGroupPropWavesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function updateGroupPropWavesProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        [string groupWN, string groupLabel] = getSelectedGroup()
        
        DFREF cellsOutDFR = getAllCellsOutputDF()
        Wave/SDFR=cellsOutDFR groupWave = $groupWN
        if(!WaveExists(groupWave))
            print "updateGroupPropWavesProc: Couldn't find selected group wave. Defaulting to groupName"
            Wave/SDFR=cellsOutDFR groupWave = groupName 
        endif

        makeAllSubtypePropWaves(groupWave = groupWave)
    endif
    return 0
end


//\\//\\//\\//\\//\\            GET SELECTION PROCEDURES          //\\//\\//\\//\\//\\

function [string groupWN, string groupLabel] getSelectedGroup()
    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    ControlInfo /W=AGG_Events paramListBox_whichGroup
    variable selParamRow = V_Value
    // print V_value, "selected row"

    Wave/T/SDFR=groupsGraphDFR listWave_group, titleWave_group
    groupWN = ""
    groupLabel = ""
    if(selParamRow < numpnts(titleWave_group))
        groupWN = listWave_group[selParamRow]
        groupLabel = titleWave_group[selParamRow]
    endif   

    return [groupWN, groupLabel]
end

function [string paramWN, string paramLabel] getSelectedDisplay_groupSum()
    DFREF groupsGraphDFR = getAllGroupsOutputDF()


    ControlInfo /W=AGG_Events paramListBox_group
    variable selParamRow = V_Value
    // print V_value, "selected row"

    Wave/T/SDFR=groupsGraphDFR listWave, titleWave
    paramWN = ""
    paramLabel = ""
    if(selParamRow < numpnts(titleWave))
        paramWN = listWave[selParamRow]
        paramLabel = titleWave[selParamRow]
    endif   

    return [paramWN, paramLabel]
end

function [string paramWN, string paramLabel] getSelectedParam_groupSum([string dispType])
    if(paramIsDefault(dispType))
        dispType = "groupAvg"
    endif
    
    DFREF groupsGraphDFR = getAllGroupsOutputDF()


    string listWaveN, titleWaveN, selWaveN
    strswitch (dispType)
        case "groupAvg":
            listWaven = "listWave_groupAvgParams"
            titleWaven = "titleWave_groupAvgParams"
            selWaven = "selWave_groupAvgParams"
            break
        case "dist":
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        case "dist_dec":
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        case "prop":
            listWaven = "listWave_propParams"
            titleWaven = "titleWave_propParams"
            selWaven = "selWave_propParams"
            break
        default:
            listWaven = "listWave_groupAvgParams"
            titleWaven = "titleWave_groupAvgParams"
            selWaven = "selWave_groupAvgParams"
            break
    endswitch

    ControlInfo /W=AGG_Events paramListBox_groupParams
    variable selParamRow = V_Value
    // print V_value, "selected row"

    Wave/T/SDFR=groupsGraphDFR listWave = $listWaveN, titleWave=$titleWaveN
    paramWN = ""
    paramLabel = ""
    if(selParamRow < numpnts(titleWave))
        paramWN = listWave[selParamRow]
        paramLabel = titleWave[selParamRow]
    endif   

    return [paramWN, paramLabel]
end


//\\//\\//\\//\\//\\            MAKE DISPLAY WAVES          //\\//\\//\\//\\//\\

function [wave/T listWave, wave/T titleWave, wave selWave] makeGroupDisplayParamsWaves()
    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder groupsGraphDFR

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string listWaveNames = cellSum.groupDisplayListNameWaves
    string niceNames = cellSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/O/N=0/T listWave, titleWave
    make/O/N=0 selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave, titleWave, selWave]
end

function [wave/T listWave, wave/T titleWave, wave selWave] makeGroupDisplayWaves()
    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder groupsGraphDFR

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string listWaveNames = "groupName;groupName2;groupName3;"
    string niceNames = "groupName:group wave 1;groupName2:group wave 2;groupName3:group wave 3;"

    variable iWave = 0, addedWaves = 0
    make/O/N=0/T listWave_group, titleWave_group
    make/O/N=0 selWave_group
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            redimension/N=(addedWaves + 1) listWave_group, titleWave_group, selWave_group
            listWave_group[addedWaves] = thisWaveName
            titleWave_group[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave_group, titleWave_group, selWave_group]
end


///////////
/// makeGroupsParamWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// EDITOR: Amanda Gibson
/// NOTES: 
/// Create the list wave, title wave, and select wave for the group display parameter waves
/// uses the CellSummaryInfo structure to get these waves and names
/// the dispType variable tells the function which field of the cellSummaryInfo structure
/// to get the liststring from
/// options are
/// - groupAvg - Different ways of calculating the group average
/// - dist - parameters available to show the distribution of events for the cell
///////////
function [wave/T listWave, wave/T titleWave, wave selWave] makeGroupsParamWaves([string dispType])
    if(paramIsDefault(dispType))
        dispType = "groupAvg"
    endif
    
    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    DFREF currentDF = GetDataFolderDFR()
    SetDataFolder groupsGraphDFR

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)
    
    string listWaveNames = "", listWaveN, titleWaveN, selWaveN
    strswitch (dispType)
        case "groupAvg":
            listWaveNames = cellSum.groupAvgWaves
            listWaven = "listWave_groupAvgParams"
            titleWaven = "titleWave_groupAvgParams"
            selWaven = "selWave_groupAvgParams"
            break
        case "dist":
            listWaveNames = cellSum.avgByEventWaves
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        case "dist_dec":
            listWaveNames = cellSum.avgByEventWaves
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        case "prop":
            listWaveNames = cellSum.avgByEventWaves
            listWaven = "listWave_propParams"
            titleWaven = "titleWave_propParams"
            selWaven = "selWave_propParams"
            listWaveNames = "allProps;" + listWaveNames
            break
        default:
            listWaveNames = cellSum.groupAvgWaves
            listWaven = "listWave_seriesParams"
            titleWaven = "titleWave_seriesParams"
            selWaven = "selWave_seriesParams"
            break
    endswitch
    string niceNames = cellSum.niceNames

    variable iWave = 0, addedWaves = 0
    make/N=0/T/O groupsGraphDFR:$listWaveN/Wave=listWave
    make/N=0/T/O groupsGraphDFR:$titleWaveN/Wave=titleWave
    make/N=0/O groupsGraphDFR:$selWaveN/Wave=selWave
    for(iWave = 0; iWave < itemsInList(listWaveNames); iWave ++)
        string thisWaveName = StringFromList(iWave, listWaveNames)
        if(!stringmatch(thisWaveName, "startTimesW") & !stringmatch(thisWaveName, "passStartTimesW"))
            string thisNiceName = stringbykey(thisWaveName, niceNames)
            if(strlen(thisNiceName) == 0)
                print "missing nice name for", thisWaveName
            endif
            redimension/N=(addedWaves + 1) listWave, titleWave, selWave
            listWave[addedWaves] = thisWaveName
            titleWave[addedWaves] = thisNiceName
            addedWaves++
        endif
    endfor
    SetDataFolder currentDF
    return [listWave, titleWave, selWave]
end

function runMakeGroupsParams()
    [wave/T listWave, wave/T titleWave, wave selWave] = makeGroupsParamWaves(dispType = "groupAvg")
    [wave/T listWave, wave/T titleWave, wave selWave] = makeGroupsParamWaves(dispType = "dist")
    [wave/T listWave, wave/T titleWave, wave selWave] = makeGroupsParamWaves(dispType = "prop")
end