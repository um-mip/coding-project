function updateGroupAvgWaves([Wave/T groupNames])
    DFREF allCellsDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupNames) || !WaveExists(groupNames))
        Wave/T/SDFR=allCellsDFR groupNames = groupName
    endif

    Wave/T uniqueGroups = getUniqueGroups(groupNames)
    variable iGroup = 0, nGroups = numpnts(uniqueGroups)

    Wave/SDFR=allCellsDFR/T allRegionNames

    if(!WaveExists(allRegionNames))
        print "updateGroupAvgWaves: Can't find allRegionNames wave"
        return NaN
    endif

    variable iRegion = 0, nRegions = numpnts(allRegionNames)

    string subTypes = "avg;limited;"
    variable iSubType = 0, nSubTypes = itemsinlist(subTypes)

    for(iGroup = 0; iGroup<nGroups;iGroup++)
        string groupName = uniqueGroups[iGroup]
        
        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = allRegionNames[iRegion]

            for(iSubType=0; iSubType<nSubTypes; iSubType++)
                string subType = StringFromList(iSubType, subTypes)

                calcAvgEventForGroup(groupName, subType, regionName)
            endfor
        endfor
    endfor
end


function calcAvgEventForGroup(string groupName, string subType, string regionName)
    DFREF allCellsDFR = getAllCellsOutputDF()
    Wave/T/SDFR=allCellsDFR groupNames = groupName
    Wave/T/SDFR=allCellsDFR cellName

    Wave/T cellsInGroup = getTextWaveSubsetFromGroup(groupNames, cellName, groupName = groupName)

    DFREF groupDFR = returnGroupRegionsDFR_bySubType(groupName, subType, regionName = regionName)
    if(dataFolderRefStatus(groupDFR)==0)
        print "calcAvgEventForGroup: Can't find group data folder for", groupName, subType, regionName
        return NaN
    endif

    string addOnName = ""

    // Average by event
    addOnName = "_byEvent"
    make/O/N=0 groupDFR:$("aveWave" + addOnName)/Wave=aveWave
    make/O/N=0 groupDFR:$("nAveWave" + addOnName)/Wave=nAveWave
    calcGroupAvgEvent(cellsInGroup, aveWave, nAveWave, avgType = subType, regionName = regionName)

    // Average by cell
    addOnName = "_byCell"
    make/O/N=0 groupDFR:$("aveWave" + addOnName)/Wave=aveWave
    make/O/N=0 groupDFR:$("nAveWave" + addOnName)/Wave=nAveWave
    calcGroupAvgEvent_byCell(cellsInGroup, aveWave, nAveWave, avgType = subType, regionName = regionName)
end

// Average wave form for a group
// Provide the cellNames associated with the group
// and where you want the aveEvent and nAveEvent stored
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-15
/// NOTES: Added avgType and regionName optional parameters
/// This function does the averaging by event for each group
function calcGroupAvgEvent(Wave/T cellNames, Wave aveWave, Wave nAveWave[, string avgType, string regionName])
    if(!WaveExists(cellNames) || !WaveExists(aveWave) || !WaveExists(nAveWave))
        return NaN
    endif

    if(paramIsDefault(avgType) || itemsInList(ListMatch("avg;limited;", avgType)) == 0)
        avgType = "avg"
    endif
    
    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    variable numCells = numpnts(cellNames), iCell = 0
    make/Wave/FREE=1/N=0 wavesToAvg, avgIndexWaves
    for(iCell=0; iCell<numCells; iCell++)
        string cellName = cellNames[iCell]
        addCellRawSeriesToWave(wavesToAvg, cellName, avgType = avgType, regionName = regionName, avgIndexWaves = avgIndexWaves)
    endfor
    recalculateAverages5(wavesToAvg, aveWave = aveWave, nAveWave = nAveWave, avgIndexWaves = avgIndexWaves)
end

// Average wave form for a group
// Provide the cellNames associated with the group
// and where you want the aveEvent and nAveEvent stored
// Average of all the cell average traces
//
//
/// This function averages the cells' average wave forms to get the group average event
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-15
/// NOTES: Added avgType and regionName optional parameters to determine where to look for 
/// the cell average wave form
/// This function should be called multiple times in order to save the aveWave and nAveWave in the
/// correct subtype/region folder
function calcGroupAvgEvent_byCell(Wave/T cellNames, Wave aveWave, Wave nAveWave[, string avgType, string regionName])
    if(!WaveExists(cellNames) || !WaveExists(aveWave) || !WaveExists(nAveWave))
        return NaN
    endif

    if(paramIsDefault(avgType) || itemsInList(ListMatch("avg;limited;", avgType)) == 0) // avg or limited
        avgType = "avg"
    endif

    if(paramIsDefault(regionName))
        regionName = "Full duration"
    endif

    variable numCells = numpnts(cellNames), iCell = 0
    aveWave = NaN
    nAveWave = NaN
    variable addedCells = 0
    // Display/N=test/K=1
    for(iCell=0; iCell<numCells; iCell++)
        string cellName = cellNames[iCell]

        DFREF cellSumDFR = returnCellRegionsDFR_bySubType(cellName, avgType, regionName = regionName)
        if(DataFolderRefStatus(cellSumDFR)==0)
            continue
        endif

        Wave/SDFR = cellSumDFR cellAvgEvent, cellnAvgEvent

        if(!WaveExists(cellAvgEvent))
            continue
        endif

        if(numpnts(cellAvgEvent) == 0)
            continue
        endif
                
        if(addedCells == 0)
            duplicate/O cellAvgEvent, aveWave
            duplicate/O cellnAvgEvent, nAveWave
        else
            aveWave += cellAvgEvent
            nAveWave += cellnAvgEvent
        endif
        // AppendToGraph cellAvgEvent // from the peak, the average I've calculated 
        // seems to be the same as averaging all of the cell average traces together on a graph
        // using the "Analysis->Packages->Avg Waves" interface.
        // Could also create a graph with the SEM of the average wave displayed

        addedCells ++
    endfor

    if(addedCells>0)
        aveWave/=addedCells
        nAveWave/=addedCells // could also think about normalizing to the peak of the group avg trace instead
    endif
end


// wavesToAvg is updated in the background with this function
// as is avgIndexWaves if provided
// can't have the same wave be both input and output (would have to do some naming/duplication)
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-15
/// NOTES: Added avg type and region name optional parameters
function addCellRawSeriesToWave(Wave/Wave wavesToAvg, string cellName[, string avgType, string regionName, wave/Wave avgIndexWaves])
    if(paramIsDefault(avgType) || itemsInList(ListMatch("avg;limited;", avgType)) == 0)
        avgType = "avg"
    endif

    if(paramIsDefault(regionName))
        regionName = "Full duration"
    endif

    if(StringMatch(regionName, "Full duration"))
        DFREF cellSumDFR = getCellSummaryDF(cellName) // always find the combinedSeriesW in the general summary folder if full duration
    else
        DFREF cellSumDFR = returnCellRegionsDFR_bySubType(cellName, avgType, regionName = regionName)
    endif

    if(dataFolderRefStatus(cellSumDFR)==0)
        // if that cell didn't have that region, shouldn't have a data folder
        // print "addCellRawSeriesToWave: no summary folder for cell", cellName
        return NaN
    endif
    
    DFREF evDetectDFR = getEventsDetectWavesDF(cellName)
    if(dataFolderRefStatus(evDetectDFR)==0)
        // print "addCellRawSeriesToWave: no event detection folder for cell", cellName
        return NaN
    endif

    Wave/SDFR=cellSumDFR combinedSeriesW
    if(!WaveExists(combinedSeriesW))
        print "addCellRawSeriesToWave: no selected series for cell", cellName, "avgType", avgType, "region", regionName
        return NaN
    endif

    variable numSeries = numpnts(combinedSeriesW), iSeries = 0
    if(numSeries == 0)
        // this won't be that uncommon if there aren't any events for that cell during a given region
        // print "addCellRawSeriesToWave: no selected series for cell", cellName, "avgType", avgType, "region", regionName
        return NaN
    endif
    
    variable numExistingWaves = numpnts(wavesToAvg)
    // doing it this way so that the rows match even if can't find one of the waves
    redimension/N=(numExistingWaves+numSeries) wavesToAvg
    if(!paramIsDefault(avgIndexWaves))
        redimension/N=(numExistingWaves+numSeries) avgIndexWaves    
    endif

    for(iSeries = 0; iSeries < numSeries; iSeries++)
        variable seriesNum = combinedSeriesW[iSeries]
        string seriesName = buildSeriesWaveName(cellName, seriesNum)
        Wave rawWave = evDetectDFR:$seriesName
        if(!WaveExists(rawWave))
            continue
        endif
        
        wavesToAvg[iSeries+numExistingWaves] = rawWave

        if(paramIsDefault(avgIndexWaves))
            continue
        endif
        
        if(StringMatch(regionName, "Full duration"))
            // if full duration, just go to the correct series folder to find the avgIndex wave
            DFREF avgIndexDFR = returnSeriesEventsDFR_bySubType(cellName, num2str(seriesNum), avgType, eventsOrSum = "sum")
            Wave/SDFR=avgIndexDFR avgIndex
        else
            // if in a region folder, find the average index for each series within the cell summary folder for that region
            DFREF avgIndexDFR = cellSumDFR 
            Wave/SDFR=avgIndexDFR avgIndex = $("s" + num2str(seriesNum) + "avgIndex")
        endif
        
        if(!WaveExists(avgIndex))
            continue
        endif

        avgIndexWaves[iSeries + numExistingWaves] = avgIndex
    endfor
end

///////////
/// getGroupAvgWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function[Wave/T uniqueGroups, Wave/Wave avgWaves] getGroupAvgWaves([variable avgByEvent, string avgOrNAvg, wave/T groupWave, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "getGroupAvgWaves: Couldn't find either group wave. Quitting"
        return [$(""), $("")]
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    if(paramIsDefault(avgByEvent))
        avgByEvent = 1
    endif

    if(paramIsDefault(avgOrNAvg))
        avgOrNAvg = "avg"
    endif

    string avgString
    strswitch (avgOrNAvg)
        case "avg":
            avgString = "aveWave"
            break
        case "nAvg":
            avgString = "nAveWave"
            break
        default:
            avgString = "aveWave"
            break
    endswitch

    string appendix
    switch (avgByEvent)
        case 0:
            appendix = "_byCell"
            break
        case 1:
            appendix = "_byEvent"
            break
        default:
            appendix = "_byEvent"
            break
    endswitch
    
    

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup, itemsInGroup = 0

    make/FREE/Wave/N=(numGroups) avgWaves

    for(iGroup=0; iGroup<numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        DFREF storageDF = returnGroupRegionsDFR_bySubType(groupName, subType, param = avgString, outType = "cellDisplay", regionName = regionName)
        if(dataFolderRefStatus(storageDF)==0)
            continue
        endif

        Wave avgWave = storageDF:$(avgString+appendix)
        if(!WaveExists(avgWave))
            continue
        endif
        avgWaves[iGroup] = avgWave
    endfor

    return[uniqueGroups, avgWaves]
end
