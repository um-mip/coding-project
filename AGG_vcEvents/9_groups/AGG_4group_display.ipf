function displayGroupParams()
    string dispType, dispTypeLabel
    [dispType, dispTypeLabel] = getSelectedDisplay_groupSum()
    string paramWN, paramLabel
    [paramWN, paramLabel] = getSelectedParam_groupSum(dispType = dispType)

    string subType = getSelectedDispType()

    variable showGraph = 1, showTable = 0
    string displayHost = "AGG_events#groupEventPlot"
    clearDisplay(displayHost)

    string tableHost = "AGG_events#groupPropTable"
    RemoveWavesFromTable_AGG(tableHost, "*")

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    DFREF groupsGraphDFR = getAllGroupsOutputDF()

    [string groupWN, string groupLabel] = getSelectedGroup()

    string regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
    if(strlen(regionName)==0)
        regionName = "Full duration"
    endif
        
    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR=cellsOutDFR/T groupWave = $groupWN
    if(!WaveExists(groupWave))
        print "displayGroupParams: Couldn't find selected group wave. Defaulting to groupName"
        Wave/SDFR=cellsOutDFR/T groupWave = groupName 
    endif

    strswitch (dispType)
        case "groupAvg":
            variable avgByEvent
            string avgOrNAvg

            strswitch (paramLabel)
                case "avg by cell":
                    avgByEvent = 0
                    avgOrNAvg = "avg"
                    break
                case "norm avg by cell":
                    avgByEvent = 0
                    avgOrNAvg = "nAvg"                    
                    break
                case "avg by event":
                    avgByEvent = 1
                    avgOrNAvg = "avg"
                    break
                case "norm avg by event":
                    avgByEvent = 1
                    avgOrNAvg = "nAvg"
                    break
                default:
                    avgByEvent = 0
                    avgOrNAvg = "avg"
                    break
            endswitch

            if(!stringmatch(subType, "avg") && !stringmatch(subType, "limited"))
                subType = "specify"
                print "Defaulting to an average subtype for showing the average wave"
            endif
            plotAvgEventForGroups(groupWave = groupWave, subType = subType, avgByEvent = avgByEvent, displayName = displayHost, avgOrNAvg = avgOrNAvg, regionName = regionName)
            break
        case "dist":
            // add xmaxes from an entry on the panel
            // 2024-01-22 - testing
            variable isExtracellular = checkIsExtracellular()
            string xMaxes
            if(isExtracellular)
                xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.0015;t50r:0.0025;int:10;decay9010:0.001;" 
            else
                xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.03;t50r:0.0025;int:10;"// add decay9010
            endif
            Wave/Wave distWaves = displayGroupDists(paramWN, groupWave = groupWave, subType = subType, displayHost = displayHost, clearHost = 1, xmaxes = xmaxes)
            if(numpnts(distWaves) == 2)
                Wave wave0 = distWaves[0], wave1 = distWaves[1]
                if(waveExists(wave0) && waveExists(wave1))
                    StatsKSTest/Z/Q distWaves[0], distWaves[1]
                    Wave W_KSResults
                    DFREF groupsGraphDFR = getAllGroupsOutputDF()
                    duplicate/O W_KSResults, groupsGraphDFR:$(paramWN + "_KS_"+ groupWN)/Wave=KS_results
                    KillWindow/Z KS_results
                    AGG_makeTable(78, 20, 100, 60, "KS_results")
                    AppendToTable KS_results.ld
                    // Only works if AGG_Events window is brought to the front
                    // DoWindow/F AGG_Events
                    // ModifyControl/Z paramListBox_groupParams activate, win = AGG_Events
                endif
            endif
            break
        case "dist_dec":
            // add xmaxes from an entry on the panel
            // 2024-01-22 - testing
            if(isExtracellular)
                xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.0015;t50r:0.0025;int:10;decay9010:0.001;" 
            else
                xMaxes = "peaks:-2.5e-10;der:-2.25e-7;fwhm:0.03;t50r:0.0025;int:10;"// add decay9010
            endif
            Wave/Wave distWaves = displayGroupDists(paramWN, groupWave = groupWave, subType = subType, displayHost = displayHost, clearHost = 1, decimate = 1, xmaxes = xmaxes)
            
            if(numpnts(distWaves) == 2)
                Wave wave0 = distWaves[0], wave1 = distWaves[1]
                if(waveExists(wave0) && waveExists(wave1))
                    StatsKSTest/Z/Q distWaves[0], distWaves[1]
                    Wave W_KSResults
                    DFREF groupsGraphDFR = getAllGroupsOutputDF()
                    duplicate/O W_KSResults, groupsGraphDFR:$(paramWN + "_KS_"+ groupWN)/Wave=KS_results
                    KillWindow/Z KS_results
                    AGG_makeTable(78, 20, 100, 60, "KS_results")
                    AppendToTable KS_results.ld
                    ModifyControl/Z paramListBox_groupParams activate, win = AGG_Events
                endif
            endif
            break
        case "prop":
            showGraph = 0
            showTable = 1

            string propTypes = ""
            if(stringmatch(paramWN, "allProps"))
                propTypes = cellSum.avgByEventWaves
            else
                propTypes = paramWN + ";"
            endif

            variable iProp = 0, nProps = itemsInList(propTypes)
            variable nEvents, thisNEvents, addedCellGroupWaves = 0
            for(iProp=0; iProp<nProps; iProp++)
                string propName = StringFromList(iProp, propTypes)

                [Wave/D comboProp, wave/T cellNames, wave/T groupNames] = returnConcatedGroupPropWaves(propName, subType = subType, regionName = regionName)

                if(!WaveExists(comboProp) || !WaveExists(cellNames) || !WaveExists(groupNames))
                    continue
                endif

                thisNEvents = numpnts(comboProp)

                if(!addedCellGroupWaves)
                    if(numpnts(cellNames)!=thisNEvents || numpnts(groupNames)!=thisNEvents)
                        print "displayGroupParams: The number of points in the cell and group name waves don't match the number of events.", propName, " Not adding"
                        continue
                    endif

                    duplicate/O groupNames, cellsOutDFR:groupNamesTable/Wave=gTable
                    duplicate/O cellNames, cellsOutDFR:cellNamesTable/Wave=cTable
                    duplicate/O comboProp, cellsOutDFR:$(propName+"table")/Wave=pTable

                    AppendToTable/W=$tableHost gTable, cTable, pTable
                    ModifyTable/W=$tableHost title(gTable) = "group", title(cTable) = "cell", title(pTable) = propName
                    nEvents = thisNEvents
                    addedCellGroupWaves = 1
                else
                    if(thisNEvents != nEvents)
                        print "displayGroupParams: The number of points in the", propName, "wave doesn't match the previous number of events"
                        continue
                    endif
                    duplicate/O comboProp, cellsOutDFR:$(propName+"table")/Wave=pTable

                    AppendToTable/W=$tableHost pTable
                    ModifyTable/W=$tableHost title(pTable) = propName
                endif
            endfor

            break
        default:
            
            break
    endswitch

    string currentTab = getSelectedTab()
    if(stringmatch(currentTab, "Group traces"))
        SetWindow $displayHost Hide=(!(showGraph == 1)) // enable when hide = 0
        SetWindow $tableHost Hide=(!(showTable == 1)) // enable when hide = 0
    endif
end

function plotAvgEventForGroups([Wave/T groupWave, string subType, variable avgByEvent, string displayName, string avgOrNAvg, string regionName])
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "plotAvgEventForGroup: Couldn't find group wave. Quitting"
        return NaN
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif
    
    if(ParamIsDefault(avgByEvent))
        avgByEvent = 1
    endif
    if(ParamIsDefault(displayName))
        displayName = "AGG_Events#groupEventPlot"
    endif

    if(WinType(displayName)!=1)
        displayName = "groupEventPlot"
        if(WinType(displayName)==1)
            print "killing existing groupEventPlot"
            KillWindow/Z $displayName
        endif
        Display/N=$displayName
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    if(paramIsDefault(avgOrNAvg))
        avgOrNAvg = "avg"
    endif
    
    clearDisplay(displayName)

    [Wave/T uniqueGroups, Wave/Wave avgWaves] = getGroupAvgWaves(avgByEvent = avgByEvent, avgOrNAvg = avgOrNAvg, groupWave = groupWave, subType = subType, regionName = regionName)

    variable iGroup = 0, numGroups = numpnts(uniqueGroups)
    variable red, green, blue, plottingSomething
    string colors, legendString =""

    if(numGroups > 0)
        wave rgb_table = makeColorTableWave(numGroups)
    endif

    for(iGroup = 0; iGroup<numGroups;iGroup++)
        string groupName = uniqueGroups[iGroup]
        Wave aveWave = avgWaves[iGroup]
        if(!WaveExists(aveWave))
            continue
        endif

        string aveWaveN = "aveWave"+num2str(iGroup)
        DFREF cellsOutDFR = getAllCellsOutputDF()

        duplicate/O aveWave, cellsOutDFR:$aveWaveN/Wave=aveWave
        
        if(numpnts(aveWave)==0)
            continue
        endif
        if(strlen(legendString)>0)
            legendString += "\n"
        endif
                    
        legendString += "\s('" + aveWaveN + "')" + groupName
        plottingSomething = 1
        AppendToGraph/W=$displayName aveWave
        red = rgb_table[iGroup][0]
        green = rgb_table[iGroup][1]
        blue = rgb_table[iGroup][2]
        ModifyGraph/W=$displayName rgb($aveWaveN)=(red, green, blue)
    endfor
    if(plottingSomething)
        SetAxis/W=$displayName/A/E=3 bottom
        SetAxis/W=$displayName/A/E=3 left
        string leftAxisText = ""
        if(stringmatch(avgOrNAvg, "avg"))
            leftAxisText = "current (pA)"
        else
            leftAxisText = "normalized current"
        endif
        // string bottomAxisText = ""
        Label /W=$displayName left leftAxisText
        // Label /W=$displayName bottom bottomAxisText
        // Legend/W=$displayName /N=text0 /C/J/B=1/G=(65535,65535,65535) legendString
        Legend/W=$displayName /N=text0 /C/J/B=1/A=RB/G=(0,0,0) legendString
        // ModifyGraph /W=$displayName axRGB=(65535,65535,65535),tlblRGB=(65535,65535,65535),alblRGB=(65535,65535,65535)

        variable isAGG = 0
        if(isAGG)
            fixAGGTreatColors(avgByEvent = avgByEvent, displayName = displayName, avgOrNAvg = avgOrNAvg)
        endif
    endif
end

///////////
/// fixAGGTreatColors
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-03-13
/// NOTES: Update the legend and colors for early-life and adult stress
///////////
function fixAGGTreatColors([variable avgByEvent, string displayName, string avgOrNAvg])
    if(ParamIsDefault(avgByEvent))
        avgByEvent = 1
    endif
    if(ParamIsDefault(displayName))
        displayName = "AGG_Events#groupEventPlot"
    endif
    if(WinType(displayName)!=1)
        displayName = "groupEventPlot"
        if(WinType(displayName)==1)
            print "killing existing groupEventPlot"
            KillWindow/Z $displayName
        endif
        Display/N=$displayName
    endif
    if(paramIsDefault(avgOrNAvg))
        avgOrNAvg = "avg"
    endif

    string addOnName = ""
    if(avgByEvent)
        addOnName = "byEvent_"
    else
        addOnName = "byCell_"
    endif

    string aveWaveN = ""
    
    if(stringmatch(avgOrNAvg, "avg"))
        aveWaveN = "aveWave_"+ addOnName
    else
        aveWaveN = "nAveWave_" + addOnName
    endif

    string SC_wName = aveWaveN + "STD-CON"
    string SA_wName = aveWaveN + "STD-ALPS"
    string LC_wName = aveWaveN + "LBN-CON"
    string LA_wName = aveWaveN + "LBN-ALPS"

    string legendString = "\s('" + SC_wName + "')" + "STD-CON"
    legendString += "\n\s('" + LC_wName + "')" + "LBN-CON"
    legendString += "\n\s('" + SA_wName + "')" + "STD-ALPS"
    legendString += "\n\s('" + LA_wName + "')" + "LBN-ALPS"

    Legend/W=$displayName /N=text0 /C/J/B=1/A=RB/G=(0,0,0) legendString
    
    ModifyGraph/W=$displayName rgb($SA_wName)=(0,0,0)
    ModifyGraph/W=$displayName rgb($SC_wName)=(39321,39321,39321)
    ModifyGraph/W=$displayName rgb($LC_wName)=(13107,52428,52428)
    ModifyGraph/W=$displayName rgb($LA_wName)=(65535,13107,39321)
end

///////////
/// displayGroupDists
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function/Wave displayGroupDists(string distType[, wave/T groupWave, string subType, string displayHost, variable clearHost, string xMaxes, variable decimate])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(dataFolderRefStatus(cellsOutDFR)==0)
        return $("")
    endif

    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "getGroupDistWaves: Couldn't find group wave. Quitting"
        return $("")
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    if(paramIsDefault(decimate))
        decimate = 0
    endif
    

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramisdefault(displayHost))
        displayHost = "groupDist_" + distType
    endif
    
    if(WinType(displayHost)==0)
        Display/K=1/N=$displayHost
    endif

    [string hostN, string subN] = splitFullWindowName(displayHost)
    doWindow/F $hostN

    SetActiveSubWindow $displayHost

    [Wave/T uniqueGroups, Wave/Wave distWaves] = getGroupDistWaves(distType, groupWave = groupWave, subType = subType)
    
    if(decimate)
        Wave/Wave decDistWaves = decimateAGG(theWaves = distWaves)
        Wave/Wave distWaves = decDistWaves
    endif

    variable iGroup = 0, numGroups = numpnts(uniqueGroups)

    variable red, green, blue, plottingSomething = 0
    string colors, legendString =""

    for(iGroup=0; iGroup<numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]
        Wave distWave = distWaves[iGroup]
        if(!WaveExists(distWave))
            continue
        endif

        string dispWaveN = "distWave"+num2str(iGroup)
        duplicate/O distWave, cellsOutDFR:$dispWaveN/Wave=distWave

        string traceString = TraceNameList(displayHost, ";", 1)
        variable nTraces = itemsInList(traceString)
        string traceN = "t" + num2str(nTraces)
        variable sortDir = appendDistWave(distWave, traceN = traceN, displayHost = displayHost)

        if(iGroup != 0)
            legendString += "\n"
        endif
                    
        legendString += "\s('" + traceN + "')" + groupName
        plottingSomething = 1
        setTraceColors(iGroup, numGroups, traceN, displayHost = displayHost)
    endfor

    if(plottingSomething)
        if(paramIsDefault(xMaxes))
            xMaxes = ""
        endif
        variable xMax = NumberByKey(distType, xMaxes)
        if(numtype(xMax)==0) // if exists
            setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, xMax=xMax, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
        else
            setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, bottomLabel=StringByKey(distType, cellSum.niceNames), legendString=legendString)
        endif    
    endif
    return distWaves
end