///////////
/// makeAllGroupFolders
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-15
/// NOTES: Make the group info folder, then specific group folders, then subtype folders, then region folders
/// If groupNames is not provided, will run with whatever group set is selected in the panel
///////////
function makeAllGroupFolders([Wave/T groupNames])
    deleteExistingGroupFolders()

    DFREF groupsDF = getGroupsInfoDF()
    
    DFREF allCellsDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupNames) || !WaveExists(groupNames))    
        [string groupWN, string groupLabel] = getSelectedGroup()
        Wave/SDFR=allCellsDFR/T groupNames = $groupWN
        if(!WaveExists(groupNames))
            print "displayGroupParams: Couldn't find selected group wave. Defaulting to groupName"
            Wave/SDFR=allCellsDFR/T groupNames = groupName 
        endif
    endif

    Wave/T/SDFR=allCellsDFR groupNames = groupName

    Wave/T uniqueGroups_official = getUniqueGroups(groupNames)
    Duplicate/FREE/T uniqueGroups_official, uniqueGroups
    insertPoints 0, 1, uniqueGroups
    uniqueGroups[0] = "overallGroupInfo"

    variable iGroup = 0, nGroups = numpnts(uniqueGroups)

    Wave/SDFR=allCellsDFR/T allRegionNames
    variable iRegion = 0, nRegions = numpnts(allRegionNames)

    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)


    for(iGroup=0; iGroup<nGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]
        if(!stringmatch(groupName, "overallGroupInfo"))
            createGroupDF(groupName)
        endif

        for(iSubType=0; iSubType<nSubTypes; iSubType++)
            string subType = StringFromList(iSubType, subTypes)
            DFREF groupSubDFR = returnGroupDFR_bySubType(groupName, subType) // this also creates the folder if it doesn't exist already
        endfor

        makeGroupRegionFolders(groupName) // the outer region folder

        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = allRegionNames[iRegion]
            if(stringMatch(regionName, "Full duration"))
                continue
            endif
            makeGroupRegionFolders(groupName, regionName = regionName)
        endfor
    endfor
end

///////////
/// deleteExistingGroupFolders
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-15
/// NOTES: 
///////////
function deleteExistingGroupFolders()
    // This should take care of it, but if waves/folders in use, might not delete
    DFREF groupsDF = getGroupsInfoDF()
    KillDataFolder/Z groupsDF
    createGroupsInfoDF()
    DFREF groupsDF = getGroupsInfoDF()

    string listOfWaves = WaveList("*", ";", "", groupsDF)
    deleteWavesFromList(listOfWaves, dfr = groupsDF)

    if(dataFolderRefStatus(groupsDF)==0)
        return NaN
    endif

    Wave/DF childDFRsWave = getChildDFRWave(groupsDF)
    variable iChild = 0, nChildren = numpnts(childDFRsWave)
    if(nChildren == 0)
        return NaN
    endif

    for(DFREF childDFR : childDFRsWave)
        KillDataFolder/Z childDFR
    endfor
end



///////////
/// deleteExistingGroupRegions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Deletes the existing regions within each subset's region folder
///////////
function deleteExistingGroupRegions(string groupName)
    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)
        DFREF regionDFR = returnGroupRegionsDFR_bySubType(groupName, subType)
        if(dataFolderRefStatus(regionDFR)==0)
            continue
        endif

        Wave/DF childDFRsWave = getChildDFRWave(regionDFR)
        variable iChild = 0, nChildren = numpnts(childDFRsWave)
        if(nChildren == 0)
            continue
        endif

        for(DFREF childDFR : childDFRsWave)
            KillDataFolder/Z childDFR
        endfor
    endfor
end

///////////
/// makeGroupRegionFolders
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Makes the region folder for all subtypes of data
/// if optional startTime, endTime are provided, they will be saved as global variables within the folders
///////////
function makeGroupRegionFolders(string groupName[, string regionName])
    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    variable specRegion = 0
    if(!paramIsDefault(regionName))
        specRegion = 1
    endif
    

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)
        if(!specRegion)
            DFREF regionDFR = makeGroupRegionsDFR_bySubType(groupName, subType)
        else
            DFREF regionDFR = makeGroupRegionsDFR_bySubType(groupName, subType, regionName = regionName)
        endif       
    endfor
end