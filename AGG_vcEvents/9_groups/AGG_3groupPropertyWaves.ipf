///// Function hierarchy
/// makeAllSubtypePropWaves
///     makeGroupsPropsWaves (for a specific subtype)
///         makeGroupPropWave (for a specific group and property parameter)
///
///
/// Also a function `getGroupPropWaves` to return the waves for a specific property wave type
/// and a concatenation function to stitch them all together to facilitate output to another program

///////////
/// makeAllSubtypePropWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function makeAllSubtypePropWaves([string propTypes, wave/T groupWave, wave/T cellNames])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif

    if(paramIsDefault(cellNames) || !WaveExists(cellNames))
        Wave/SDFR = cellsOutDFR/T cellNames = cellName
    endif
    
    if(!WaveExists(groupWave) || !WaveExists(cellNames))
        print "makeGroupPropWaves: Couldn't find either group wave or cell name wave. Quitting"
        return 0
    endif

    string subTypes = "all;avg;limited;interval;"
    variable iSubType = 0, nSubTypes = itemsInList(subTypes)

    wave/SDFR=cellsOutDFR/T allRegionNames

    if(!WaveExists(allRegionNames))
        print "updateGroupAvgWaves: Can't find allRegionNames wave"
        return NaN
    endif

    variable iRegion = 0, nRegions = numpnts(allRegionNames)

    for(iSubType=0; iSubType<nSubTypes; iSubType++)
        string subType = StringFromList(iSubType, subTypes)

        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = allRegionNames[iRegion]
            if(paramIsDefault(propTypes) || itemsInList(propTypes)<1)
                makeGroupsPropWaves(groupWave = groupWave, cellNames = cellNames, subType = subType, regionName = regionName)
            else
                makeGroupsPropWaves(propTypes = propTypes, groupWave = groupWave, cellNames = cellNames, subType = subType, regionName = regionName)
            endif
        endfor
    endfor
end

///////////
/// makeGroupsPropWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES:
///////////
function makeGroupsPropWaves([string propTypes, wave/T groupWave, wave/T cellNames, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif

    if(paramIsDefault(cellNames) || !WaveExists(cellNames))
        Wave/SDFR = cellsOutDFR/T cellNames = cellName
    endif
    
    if(!WaveExists(groupWave) || !WaveExists(cellNames))
        print "makeGroupPropWaves: Couldn't find either group wave or cell name wave. Quitting"
        return 0
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup, itemsInGroup = 0

    if(paramIsDefault(propTypes) || itemsInList(propTypes)<1)
        propTypes = "peaks;der;t50r;fwhm;decay9010;int;"
    endif
    
    variable iProp = 0, nProps = itemsInList(propTypes)

    for(iProp=0; iProp<nProps; iProp++)
        string propType = StringFromList(iProp, propTypes)
        for(iGroup = 0; iGroup < numGroups; iGroup++)
            string groupName = uniqueGroups[iGroup]

            Wave/T cellsInGroup = getTextWaveSubsetFromGroup(groupWave, cellNames, groupName = groupName)
            
            makeGroupPropWave(propType, groupName, cellsInGroup, subType = subType, regionName = regionName)
        endfor
        [Wave props_concat, Wave/T cellNames_concat, Wave/T groupNames_concat] = concatAllGroupPropWaves(propType, groupWave = groupWave, subType = subType, regionName = regionName)
    endfor
end

///////////
/// makeGroupPropWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-02-18
/// NOTES: 
///////////
function/Wave makeGroupPropWave(string propType, string groupName, wave/T cellsInGroup[, string subType, DFREF storageDF, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    if(!WaveExists(cellsInGroup))
        print "makeGroupPropWave: Couldn't find cell names to include. Quitting"
        return $("")
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    if(paramIsDefault(storageDF))
        DFREF storageDF = returnGroupRegionsDFR_bySubType(groupName, subType, param = propType, outType = "cellDisplay", regionName = regionName)
    endif
    
    variable itemsInGroup = numpnts(cellsInGroup)
    
    variable iCell
    variable updatedUnits = 0, peakSign = -1

    make/N=0/FREE/Wave propWaves // may not find a prop wave for each cell in the group, especially once getting into regions
    make/N=0/FREE/Wave cellIDWaves
    for(iCell=0; iCell<itemsInGroup; iCell++)
        string cellName = cellsInGroup[iCell]
        DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = propType, outType = "cellDisplay", regionName = regionName)
        
        if(dataFolderRefStatus(cellDFR)==0)
            print "createGroupPropPlots: couldn't find cell summary df", cellName
            continue
        endif

        Wave/SDFR=cellDFR propWave = $(propType + "_concat")

        if(!WaveExists(propWave))
            print "createGroupPropPlots: couldn't find prop wave", propType, cellName
            continue
        endif

        if(!updatedUnits)
            string unitsString = getWaveUnits(propWave)

            if(strlen(unitsString)>0)
                updatedUnits = 1
            endif
        endif

        propWaves[numpnts(propWaves)] = {propWave} // append to the end

        make/N=(numpnts(propWave))/T/FREE cellNameWave = cellName
        cellIDWaves[numpnts(cellIDWaves)] = {cellNameWave} // append to the end         
    endfor
    string groupPropN = propType + "_prop"
    string groupCellN = propType + "_prop_cells" // this should be the same for all of the properties, but just in case
    if(numpnts(propWaves)>0)
        concatenate/NP/O {propWaves}, storageDF:$(groupPropN)
        concatenate/NP/O {cellIDWaves}, storageDF:$(groupCellN)
        Wave/SDFR=storageDF/D groupProp = $(groupPropN)

        setScale d, 0, 1, unitsString, groupProp
    else
        make/N=0/D/O storageDF:$(groupPropN)/Wave=groupProp
        make/N=0/T/O storageDF:$(groupCellN)
    endif

    return groupProp
end


///////////
/// getGroupPropWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-21
/// NOTES: 
///////////
function[Wave/T uniqueGroups, Wave/Wave propWaves, Wave/Wave cellNameWaves, Wave/Wave groupNameWaves] getGroupPropWaves(string propType[, wave/T groupWave, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "getGroupPropWaves: Couldn't find either group wave. Quitting"
        return [$(""), $(""), $(""), $("")]
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup, itemsInGroup = 0

    make/FREE/Wave/N=(numGroups) propWaves, cellNameWaves, groupNameWaves

    for(iGroup=0; iGroup<numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        DFREF storageDF = returnGroupRegionsDFR_bySubType(groupName, subType, param = propType, outType = "cellDisplay", regionName = regionName)
        if(dataFolderRefStatus(storageDF)==0)
            continue
        endif

        Wave propWave = storageDF:$(propType+"_prop")
        if(!WaveExists(propWave))
            continue
        endif


        Wave/T cellNameWave = storageDF:$(propType+"_prop_cells")
        if(!WaveExists(cellNameWave))
            continue
        endif

        make/T/FREE/N=(numpnts(propWave)) groupNameWave = groupName
        
        propWaves[iGroup] = propWave
        cellNameWaves[iGroup] = cellNameWave
        groupNameWaves[iGroup] = groupNameWave
    endfor

    return[uniqueGroups, propWaves, cellNameWaves, groupNameWaves]
end

///////////
/// concatAllGroupPropWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-02-18
/// NOTES: 
/// 2024-05-15, incomplete. Want to make a table to display the prop values within the panel
/// Also probably store the concatenated waves within the groups info folder for all props
/// then recall which one(s) you want to view
///////////
function[Wave/D comboProp, wave/T cellNames, wave/T groupNames] concatAllGroupPropWaves(string propType[, wave/T groupWave, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(dataFolderRefStatus(cellsOutDFR)==0)
        return [$(""), $(""), $("")]
    endif

    if(paramIsDefault(groupWave) || !WaveExists(groupWave))
        Wave/SDFR = cellsOutDFR/T groupWave = groupName
    endif
    
    if(!WaveExists(groupWave))
        print "concatAllGroupPropWaves: Couldn't find group wave"
        return [$(""), $(""), $("")]
    endif

    STRUCT cellSummaryInfo cellSum
    StructFill/SDFR=cellOutDFR cellSum
    fillStringsinCellSummaryInfo(cellSum)

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif
    
    [Wave/T uniqueGroups, Wave/Wave propWaves, Wave/Wave cellNameWaves, Wave/Wave groupNameWaves] = getGroupPropWaves(propType, groupWave = groupWave, subType = subType, regionName = regionName)
    
    DFREF groupsDF = returnGroupRegionsDFR_bySubType("overallGroupInfo", subtype, param = propType, outType = "cellDisplay", regionName = regionName)

    if(dataFolderRefStatus(groupsDF)==0)
        print "concatAllGroupPropWaves: Couldn't find group data folder"
        return [$(""), $(""), $("")]
    endif

    make/D/O/N=0 groupsDF:$(propType+"_allEvents")/Wave=comboProp
    make/T/O/N=0 groupsDF:$(propType+"_cellNames")/Wave=cellNames
    make/T/O/N=0 groupsDF:$(propType+"_groupNames")/Wave=groupNames
    if(numpnts(propWaves)>0)
        concatenate/NP/O {propWaves}, groupsDF:$(propType+"_allEvents") // if use the wave refs here, it seems to make a new wave in root
        concatenate/NP/O/T {cellNameWaves}, groupsDF:$(propType+"_cellNames")
        concatenate/NP/O/T {groupNameWaves}, groupsDF:$(propType+"_groupNames")
    endif
    
    return [comboProp, cellNames, groupNames]
end

///////////
/// returnConcatedGroupPropWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-02-18
/// NOTES: 
/// 2024-05-15, incomplete. Want to make a table to display the prop values within the panel
/// Also probably store the concatenated waves within the groups info folder for all props
/// then recall which one(s) you want to view
///////////
function[Wave/D comboProp, wave/T cellNames, wave/T groupNames] returnConcatedGroupPropWaves(string propType[, string subType, string regionName])
    DFREF cellsOutDFR = getAllCellsOutputDF()
    if(dataFolderRefStatus(cellsOutDFR)==0)
        return [$(""), $(""), $("")]
    endif

    if(paramIsDefault(subType))
        subType = getSelectedDispType()
    endif

    if(paramIsDefault(regionName))
        regionName = getSelectedItem("regionListBox_all", hostName = "AGG_events")
        if(strlen(regionName)==0)
            regionName = "Full duration"
        endif
    endif
    
    DFREF groupsDF = returnGroupRegionsDFR_bySubType("overallGroupInfo", subtype, param = propType, outType = "cellDisplay", regionName = regionName)

    if(dataFolderRefStatus(groupsDF)==0)
        print "returnConcatedGroupPropWaves: Couldn't find group data folder"
        return [$(""), $(""), $("")]
    endif

    Wave/SDFR=groupsDF comboProp = $(propType+"_allEvents")
    Wave/SDFR=groupsDF/T cellNames = $(propType+"_cellNames"), groupNames = $(propType+"_groupNames")
    
    return [comboProp, cellNames, groupNames]
end