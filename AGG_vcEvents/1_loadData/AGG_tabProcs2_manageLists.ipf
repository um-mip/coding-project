function removeBlankCellsProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventcode == 2)
        clearUnnamedCellRows()
    endif
end

function clearUnnamedCellRows()
    DFREF panelDFR = getEventsPanelDF()

    WAVE/SDFR=panelDFR/T cellName, groupName, groupName2, groupName3

    variable numRowsInTable = numpnts(cellName)

    redimension/N=(numRowsInTable) groupName, groupName2, groupName3

    if(numRowsInTable > 0)
        variable row = numRowsInTable - 1
        do // work from the end, b/c deleting point changes the index
            string thisCellName = cellName[row]
            if(strlen(thisCellName) == 0)
                DeletePoints row,1, cellName, groupName, groupName2, groupName3
            endif
            row = row - 1
        while( row >= 0 )
    endif

    updateCellNameListBox()
end

function updateCellNameListBox()

    DFREF panelDFR = getEventsPanelDF()
    DFREF evDetectDFR = getEvDetectDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    WAVE/SDFR=panelDFR/T cellName

    variable numNames = numpnts(cellName)

    redimension/N=(numNames) panelDFR:cellSelWave, panelDFR:cellSelWave_series, evDetectDFR:cellSelWave

    if(WaveExists(cellSumDFR:cellSelWave ))
        redimension/N=(numNames) cellSumDFR:cellSelWave 
    endif
    
    if(WaveExists(cellSumDFR:cellSelWave_concat ))
        redimension/N=(numNames) cellSumDFR:cellSelWave_concat
    endif
    
    if(WaveExists(panelDFR:cellSelWave_burst ))
        redimension/N=(numNames) panelDFR:cellSelWave_burst
    endif
end

function updateCellNamesListProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        updateCellNameListBox()
    endif
end

///////////
/// clearUnnamedEventsSeriesRows
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-09
/// NOTES: 
///////////
function clearUnnamedEventsSeriesRows(string cellName)
    DFREF cellDFR = getEventsCellInfoDF(cellName)

    if(dataFolderRefStatus(cellDFR)==0)
        return NaN
    endif

    WAVE/SDFR=cellDFR/T Events_series, passive1, passive2

    variable numRowsInTable = numpnts(Events_series)

    redimension/N=(numRowsInTable) passive1, passive2

    if(numRowsInTable > 0)
        variable row = numRowsInTable - 1
        do // work from the end, b/c deleting point changes the index
            string thisEvents_series = Events_series[row]
            if(strlen(thisEvents_series) == 0)
                DeletePoints row,1, Events_series, passive1, passive2
            endif
            row = row - 1
        while( row >= 0 )
    endif
end