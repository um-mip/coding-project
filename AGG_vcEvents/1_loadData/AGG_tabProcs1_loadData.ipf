function getSeriesInfo_LBproc(LB_struct) : ListBoxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        fillCellInfoTable(panelName = LB_Struct.win)
    endif

    return 0
end

///////////
/// fillCellInfoTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-20
/// NOTES: 
///////////
function fillCellInfoTable([string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif
    
    string selectedCell = getSelectedItem("cellListBox", hostName = panelName)
    if(strlen(selectedCell) == 0)
        string tableName = panelName + "#cellSeriesInfoTable"
            RemoveWavesFromTable_AGG(tableName, "*")
        return NaN
    endif

    DFREF cellDFR = getEventsCellInfoDF(selectedCell)
    if(DataFolderRefStatus(cellDFR) == 0)
        // print "folder doesn't exist"
        createEventscellInfoDF(selectedCell)
        cellDFR = getEventsCellInfoDF(selectedCell)
        makeCellInfoWaves(cellDFR)
    endif

    updateCellInfoEventsTable(selectedCell)

    variable fillCard = getCBVal("fillExpCardCheck", panelName = panelName)
    if(fillCard)
        readexpcard(selectedCell)
        if(getCBVal("showExpCardCheck", panelName = panelName))
            DoWindow/F experimentCard
        else
            DoWindow/F $panelName
        endif
    endif
end


// AGG 2024-06-09... not sure why I would have made these text waves, other than it might make it easier to use
// them in listboxes later, but it's an odd choice
function makeCellInfoWaves(cellDFR)
    DFREF cellDFR

    make/O/T cellDFR:Events_series, cellDFR:passive1, cellDFR:passive2
end

function updateCellInfoEventsTable (selectedCell)
    string selectedCell

    DFREF panelDFR = getEventsPanelDF()
    WAVE/T/Z/SDFR = panelDFR cellName
    // ControlInfo /W=AGG_Events cellListBox_tab0 
    // variable selCellAnalysis = V_value

    string tableName = "AGG_Events#cellSeriesInfoTable"

    if(WinType(tableName)==2) // if it's a table that exists
        // Accept in-progress changes
        ModifyTable/w=$tableName entryMode = 1
    endif

    RemoveWavesFromTable_AGG(tableName, "*")

    string infoWaveNames = "Events_series;passive1;passive2;"
    variable iWaves = 0
    variable numBParams = itemsInList(infoWaveNames)
    string nameWave = ""
    DFREF cellInfoDFR = getEventsCellInfoDF(selectedCell)

    for( iWaves = 0; iWaves < numBParams; iWaves++ )
        nameWave = StringFromList( iWaves, infoWaveNames )
        
        // Replace values for the display waves with the value of the selected cell/region
        AppendToTable /W=$tableName cellInfoDFR:$nameWave
    endfor
    ModifyTable/W=$tableName autosize={0, 0, -1, 0, 10}
end

///////////
/// removeExtraSeriesPoints
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-07-31
/// NOTES: Cleaning up the series to be displayed for each cell to only include the ones in final dataset
///////////
function removeExtraSeriesPoints(variable firstPoint, variable numPoints, [string selectedCell])

    if(paramIsDefault(selectedCell))
        selectedCell = getSelectedItem("cellListBox", hostName = "AGG_Events")
    endif
    
    string infoWaveNames = "Events_series;passive1;passive2;"
    variable iWaves = 0
    variable numBParams = itemsInList(infoWaveNames)
    string nameWave = ""
    DFREF cellInfoDFR = getEventsCellInfoDF(selectedCell)

    for( iWaves = 0; iWaves < numBParams; iWaves++ )
        nameWave = StringFromList( iWaves, infoWaveNames )
        
        Wave thisWave = cellInfoDFR:$nameWave
        DeletePoints firstPoint, numPoints, thisWave
    endfor

    ModifyControl/Z cellListBox activate, win = AGG_Events
end

///////////
/// checkDefTraceNumProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Check that the default trace number is an integer >=1, round if not
///////////
function checkDefTraceNumProc(Var_Struct) :SetVariableControl
    STRUCT WMSetVariableAction & Var_Struct
    if(Var_Struct.eventcode == 1 || Var_Struct.eventcode == 6 || Var_Struct.eventCode == 8)
        DFREF panelDFR = getEventsPanelDF()
        NVAR/Z/SDFR=panelDFR defTraceNum
        if(numtype(defTraceNum)!=0)
            print "defTraceNum doesn't exist"
            return 0
        endif
        if(! defTraceNum >= 1)
            defTraceNum = 1
        endif
        if(! isInteger(defTraceNum) )
            defTraceNum = round(defTraceNum)
        endif
    endif
    return 0
end

///////////
/// checkIsExtracellular
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Return 0/1 for if the experiment is extracellular
/// If it can't find the data folder or variable for any reason
/// Default is to say that it's not extracellular
///////////
function checkIsExtracellular()
    variable expIsExtracellular = 0
    DFREF panelDFR = getEventsPanelDF()
    if(dataFolderRefStatus(panelDFR)!=0)
        NVAR/Z/SDFR=panelDFR isExtracellular
        if(numtype(isExtracellular)==0)
            expIsExtracellular = isExtracellular
        endif
    endif
    return expIsExtracellular
end


///////////
/// isExtracellularProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-22
/// NOTES: 
///////////
function isExtracellularProc(CB_Struct) : CheckBoxControl
    STRUCT WMCheckboxAction & CB_Struct
    if(CB_Struct.eventcode == 2) // Mouse up
        NVAR/Z/SDFR=root: traceDur = gtdur
        if(!NVar_Exists(traceDur))
            return 0
        endif

        variable isExtracellular = checkIsExtracellular()
        if(isExtracellular)
            traceDur = 10
        else
            traceDur = 200
        endif
    endif
    return 0
end

///////////
/// loadFromCollectorProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-09
/// NOTES: 
///////////
function loadFromCollectorProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        copyCellsFromCollector()
    endif
    return 0
end



///////////
/// copyCellsFromCollector
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-09
/// NOTES: 
///////////
function copyCellsFromCollector()
    DFREF panelDFR = getEventsPanelDF()

    if(dataFolderRefStatus(panelDFR)==0)
        return NaN
    endif

    Wave/SDFR=panelDFR/T cellNames = cellName

    if(!WaveExists(cellNames))
        return NaN
    endif

    Wave/SDFR=root:/T expW = expListW

    if(!WaveExists(expW))
        print "copyCellsFromCollector: Can't find experiment list wave"
        return NaN
    endif

    variable iExp = 0, nExps = numpnts(expW)
    for(iExp=0; iExp<nExps; iExp++)
        string expName = expW[iExp]
        if(strlen(expName)==0)
            continue
        endif

        if(!isStringInWave(expName, cellNames))
            cellNames[numpnts(cellNames)] = {expName}
        endif
    endfor

    clearUnnamedCellRows()
end

///////////
/// updateExpSeriesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-09
/// NOTES: Updates the events_series waves for any cells that currently have a blank wave
/// doesn't update cells with existing series entered
/// will redimension all Events_series waves
/// Looks for waves that match a given list of labels for the events series
///////////
function updateExpSeriesProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        SVAR/SDFR=getEventsPanelDF() seriesLabelList
        if(!SVAR_Exists(seriesLabelList) || strlen(seriesLabelList)==0)
            print "updateExpSeriesProc: seriesLabelList is empty, please enter at least one series label"
            return NaN
        endif

        buildAndFindEventSeriesWave(eventLabelList = seriesLabelList)
    endif
    return 0
end


///////////
/// buildAndFindEventSeriesWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-09
/// NOTES: 
///////////
function buildAndFindEventSeriesWave([string eventLabelList, DFREF dfr, variable addToExisting])
    if(paramIsDefault(eventLabelList))
        eventLabelList = "EC-10kHz;EC-20kHz;VC-10kHz;"
    endif
    variable iLabel = 0, nLabels = itemsInList(eventLabelList)

    if(paramIsDefault(dfr))
        DFREF dfr = root:
    endif

    if(paramIsDefault(addToExisting))
        addToExisting = 0
    endif

    DFREF panelDFR = getEventsPanelDF()

    if(dataFolderRefStatus(panelDFR)==0)
        return NaN
    endif

    Wave/SDFR=panelDFR/T cellNames = cellName

    if(!WaveExists(cellNames))
        return NaN
    endif
    variable iCell = 0, nCells = numpnts(cellNames)
    
    for(iLabel=0; iLabel<nLabels; iLabel++)
        string matchingWaves = ""
        for(iLabel=0; iLabel<nLabels; iLabel++)
            string thisLabel = StringFromList(iLabel, eventLabelList)

            if(strlen(thisLabel)==0)
                continue
            endif

            matchingWaves += findWavesWithLabel(labelStr = thisLabel, dfr = dfr)
        endfor
    endfor

    for(iCell=0; iCell<nCells; iCell++)
        string cellName = cellNames[iCell]
        if(strlen(cellName)==0)
            continue 
        endif


        DFREF cellDFR = getEventsCellInfoDF(cellName)
        if(DataFolderRefStatus(cellDFR) == 0)
            // print "folder doesn't exist"
            createEventscellInfoDF(cellName)
            cellDFR = getEventsCellInfoDF(cellName)
            makeCellInfoWaves(cellDFR)
        endif

        Wave/SDFR=cellDFR/T eventsW = Events_series, passive1, passive2

        clearUnnamedEventsSeriesRows(cellName)

        variable iSeries = 0, nSeries = numpnts(eventsW)
        variable emptyWave = 0
        if(nSeries == 0)
            emptyWave = 1
        endif
        
        if(emptyWave || addToExisting)
            // Does the default trace number variable exist?
            NVAR/Z/SDFR=getEventsPanelDF() defTraceNum
            variable traceNum = 1
            if(NVar_Exists(defTraceNum) && numtype(defTraceNum)==0)
                traceNum = defTraceNum
            endif
            string searchStr = cellName + "*" + "t" + num2str(traceNum)
            string cellMatchingWaves = listmatch(matchingWaves, searchStr)

            variable iMatching = 0, nMatching = itemsInList(cellMatchingWaves)
            for(iMatching=0; iMatching<nMatching; iMatching++)
                string thisWaveN = StringFromList(iMatching, cellMatchingWaves)
                [string cellID, string seriesNum] = getCellIDAndSeriesFromWaveN(thisWaveN)

                if(!isStringInWave(seriesNum, eventsW))
                    eventsW[numpnts(eventsW)] = {seriesNum}
                    passive1[numpnts(passive1)] = {""}
                    passive2[numpnts(passive2)] = {""}
                endif
            endfor

            Sort/A eventsW, eventsW, passive1, passive2
        endif
    endfor
end
