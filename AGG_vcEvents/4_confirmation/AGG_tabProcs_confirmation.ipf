// This changes the global xmin and xmax to match the range of analysis graph 1
function getrangeproc_AGG(B_Struct) : buttoncontrol
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventcode == 2)
        string ctrlName = B_Struct.ctrlName
        string panelName = B_Struct.win

        NVAR gxmin1 = root:gxmin1
        NVAR gxmax1 = root:gxmax1

        // print "inside getrange proc: ", gxmin1,gxmax1

        string analysis1Name = panelName + "#AnalysisGraph1"
        
        if(strlen(AxisList(analysis1Name))==0)
            return NaN
        endif
        getaxis/W=$analysis1Name/Q bottom
        // reset global variables to new range
        gxmin1=v_min
        gxmax1=v_max
    endif

end

// This changes the global xmin and xmax to match the range of analysis graph 1
function setrangeproc_AGG(B_Struct) : buttoncontrol
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventcode == 2)
        string ctrlName = B_Struct.ctrlName
        string panelName = B_Struct.win

        NVAR gxmin1 = root:gxmin1
        NVAR gxmax1 = root:gxmax1

        // print "inside setrange proc: ", gxmin1,gxmax1

        string analysis1Name = panelName + "#AnalysisGraph1"

        if(strlen(AxisList(analysis1Name))==0)
            return NaN
        endif
        
        SetAxis/W=$analysis1Name/E=3/A bottom
        SetAxis/W=$analysis1Name/E=3/A left
        doupdate
        getaxis/W=$analysis1Name/Q bottom
        // reset global variables to new range
        gxmin1=v_min
        gxmax1=v_max

        string analysis2Name = panelName + "#AnalysisGraph2"
        SetAxis/W=$analysis2Name bottom, gxmin1, gxmax1
        SetAxis/W=$analysis2Name/E=3/A=2 left
    endif

end

// This generates a wave intrinsic window for the selected series
// based on the cell name and series number combination in the list boxes
function plotWaveIntrinsicProc (B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        DFREF dfr = root:
        
        // Get the string for the series name constructed by the
        // selected cell and selected series in the list boxes
        // dfr is where the wave is expected to be
        String selectedSeries = getSelectedSeriesName_events(dfr)
        if(strlen(selectedSeries))
		    WAVE wd = $(selectedSeries+"-deriv")
            if(waveExists(wd))
                // AGG - 2023-07-11: Always update the detect parameters with the values used to detect this cell
                changeDetectParamsFromConfirmParams()
                
                navigator2(selectedSeries, useSepViewDur = 1)
            else
                print "No derivative yet to plot wave intrinsic. You probably need to detect, still"
            endif
        endif
    endif
end

// Return the series name for the selected cell/series combination
function/S getSelectedSeriesName_events(dfr)
    DFREF dfr // where wave should be

    string thisSeries = ""

    string cellName = getSelectedItem("cellListBox_series", hostName = "AGG_Events")
    string seriesName = getSelectedItem("seriesListBox_series", hostName = "AGG_Events")

    if(strlen(cellName)>0 && strlen(seriesName)>0)
        string seriesString = buildSeriesWaveName(cellName, str2num(seriesName))
        if(waveExists(dfr:$seriesString))
            thisSeries = seriesString
        endif
    endif

    return(thisSeries)
end

// Create a cerebro plot
function plotCerebroProc (B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        DFREF dfr = root:
        // String selectedSeries = getSelectedSeriesName_events(dfr)
        // if(strlen(selectedSeries))
		//     WAVE wd = $(selectedSeries+"-deriv")
        //     if(waveExists(wd))
        //         navigator2(selectedSeries)
        //     else
        //         print "No derivative yet to plot wave intrinsic. You probably need to detect, still"
        //     endif
        // endif
        killwindow/Z cerebro

        Wave/T detectedWaves = updateCellDetectedWavesForCerebro(root)
        if(numpnts(detectedWaves)>0)
            // AGG - 2023-07-11: Always update the detect parameters with the values used to detect this cell
            changeDetectParamsFromConfirmParams()

            variable/G root:useWavesForCerebro/N=useWavesForCerebro

            useWavesForCerebro = 1

            panelMaker(useWavesForCerebro = useWavesForCerebro) // could eventually pass in parameters through this function, get them from the detected info
            // could also make it so that you pass in the wave reference directly, to make this more flexible for which waves to show in cerebro dropdown
            // would need to change this in retanalwaves function (in JP_CEREBRO_v2-8z.ipf)
        else
            print "no detected waves from this cell"
        endif
    endif
end

function updateDetectParams(B_Struct) : buttoncontrol
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventcode == 2)
        // print "in button control for update detection"
        changeDetectParamsFromConfirmParams()
    endif

end

function/Wave updateCellDetectedWavesForCerebro(dfr)
    DFREF dfr // where the waves should be stored

    string cellName = getSelectedItem("cellListBox_series", hostName = "AGG_Events")
    
    DFREF panelDFR = getEventsPanelDF()
    Wave seriesNums = panelDFR:selCellSeries

    Wave/T seriesNums_noBlanks = removeBlankCells(seriesNums)

    variable numSeries = numpnts(seriesNums_noBlanks), iSeries = 0, addedSeries = 0
    DFREF evDetectDFR = getEvDetectDF()
    make/O/T/N=0 evDetectDFR:wavesForCerebro/Wave=seriesTraceNames
    
    print "number of series", numSeries

    variable startSeries = 0
    if(numSeries>=128)
        ControlInfo/W=AGG_Events seriesListBox_series
        startSeries = V_value // if more than 128, start at the selected series
    endif

    for(iSeries = startSeries; iSeries < numSeries; iSeries ++)
        string seriesName = seriesNums_noBlanks[iSeries]
        string seriesTraceName = buildSeriesWaveName(cellName, str2num(seriesName))
        
        Wave/T detectedWaves = evDetectDFR:detectedWaves
        if(isStringInWave(seriesTraceName, detectedWaves))
            redimension/N=(addedSeries + 1) seriesTraceNames
            seriesTraceNames[addedSeries] = seriesTraceName
            addedSeries ++ 
        endif
    endfor

    // print seriesTraceNames
    return seriesTraceNames
end

///////////
/// recheckAvgListProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-07-31
/// NOTES: Button procedure to recheck the average events for a series without having to add/delete an event
///////////
function recheckAvgListProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        recheckAvgList()
    endif
    return 0
end

///////////
/// recheckAvgList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-07-31
/// NOTES: Will recheck the average list for the series that is currently selected
/// in the confirmation tab of the AGG_Events panel
///////////
function recheckAvgList()
    DFREF dfr = root:
        
    // Get the string for the series name constructed by the
    // selected cell and selected series in the list boxes
    // dfr is where the wave is expected to be
    String selectedSeries = getSelectedSeriesName_events(dfr)

    string cellName = getSelectedItem("cellListBox_series", hostName = "AGG_Events")
    string seriesName = getSelectedItem("seriesListBox_series", hostName = "AGG_Events")
    variable seriesNum = str2num(seriesName)

    Wave rawWave = dfr:$(selectedSeries)
    Wave ptbWave = dfr:$(selectedSeries + returnExt("peak time"))
    if(WaveExists(ptbWave)&&WaveExists(rawWave))
        updateConfirmationParams(cellName, seriesNum, 1)
        changeDetectParamsFromConfirmParams()

        Wave/D backInt, forInt
        [backInt, forInt] = calculateIntervals(ptbWave, estUnknowns = 1)

        duplicate/O backInt, dfr:$(selectedSeries + returnExt("interval"))
        Wave/SDFR=dfr intWave = $(selectedSeries + returnExt("interval"))
        intWave[0] = NaN
        string unitString = returnUnitType(rawWave, "time")
        setScale d, 0, 1, unitString, backInt
        setScale d, 0, 1, unitString, forInt
        setScale d, 0, 1, unitString, intWave
        probDistP(selectedSeries + returnExt("interval"), 1)

        calculateAverageList(backInt, forInt, checkForRejected = 1)
    endif
end

///////////
/// showAvgListAndAvgEventProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-07-31
/// NOTES: Button procedure to recheck the average events for a series without having to add/delete an event
///////////
function showAvgListAndAvgEventProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        showAvgListAndAvgEvent()
    endif
    return 0
end

///////////
/// showAvgListAndAvgEvent
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-03
/// NOTES: Show the average list in a table along with the intervals and display the average event
///////////
function showAvgListAndAvgEvent()
    DFREF dfr = root:
        
    // Get the string for the series name constructed by the
    // selected cell and selected series in the list boxes
    // dfr is where the wave is expected to be
    String selectedSeries = getSelectedSeriesName_events(dfr)

    string cellName = getSelectedItem("cellListBox_series", hostName = "AGG_Events")
    string seriesName = getSelectedItem("seriesListBox_series", hostName = "AGG_Events")
    variable seriesNum = str2num(seriesName)

    Wave rawWave = dfr:$(selectedSeries)
    Wave ptbWave = dfr:$(selectedSeries + returnExt("peak time"))
    Wave backIntWave = dfr:$(selectedSeries + returnExt("peak time") + "_backInt")
    Wave forIntWave = dfr:$(selectedSeries + returnExt("peak time") + "_forInt")
    Wave averageList = dfr:$(selectedSeries + returnExt("ave list"))
    // Wave averageEvent = dfr:$(selectedSeries + returnExt("event average"))
    DFREF seriesDFR = returnSeriesEventsDFR_bySubType(cellName, seriesName, "avg", param = "ave", eventsOrSum = "sum", outType = "seriesAvgDisplay")
    
    if(dataFolderRefStatus(seriesDFR)==0)
        return 0
    endif

    Wave averageEvent = seriesDFR:$(selectedSeries + returnExt("event average"))
    
    if(WaveExists(ptbWave)&&WaveExists(rawWave) && WaveExists(averageEvent))
        killwindow/Z aveEvent
        
        AGG_makeDisplay(60, 55, 95, 90, "aveEvent")
        AppendToGraph averageEvent

        killwindow/Z aveList
        AGG_makeTable(60, 0, 95, 45, "aveList")
        AppendToTable averageList, backIntWave, forIntWave
        ModifyTable Title(backIntWave) = "back int", Title(forIntWave) = "forward int"
    endif
end


///////////
/// changeTraceDurationProc
/// AUTHOR: 
/// ORIGINAL DATE: 2024-05-18
/// NOTES: 
///////////
function changeTraceDurationProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        string cellName = getSelectedItem("cellListBox_series", hostName = "AGG_Events")
        string seriesName = getSelectedItem("seriesListBox_series", hostName = "AGG_Events")
        DFREF confirmDF = getEvConfirmDF()
        NVAR/Z/SDFR = confirmDF updatedTraceDur
        if(numtype(updatedTraceDur) == 0 && updatedTraceDur > 1)
            variable updatedTraceDur_s = updatedTraceDur / 1000
            changeTraceDurationForDetectedSeries(cellName, str2num(seriesName), updatedTraceDur_s)
            recheckAvgList()
            calcSeriesAvgSubsetWaves(cellName, str2num(seriesName))
        endif
    endif
    return 0
end
