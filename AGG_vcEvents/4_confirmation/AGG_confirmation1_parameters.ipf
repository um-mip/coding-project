// This is called when a cell/series is selected from the series list boxes
function updateConfirmationParams(string cellName, variable seriesNum, variable prevDetection[, string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Events"
    endif
    DFREF panelDFR = getEventsPanelDF()
    // print "updating confirmation parameters for cell", cellName, "series", seriesNum
    // print prevDetection, "previousDetections"
    variable makeBlankTable
    
    if(prevDetection)
        makeBlankTable = 0
    else
        makeBlankTable = 1
    endif
    // print "blank table", makeBlankTable

    string seriesName = num2str(seriesNum)

    if(!makeBlankTable)
        DFREF cellDFR = getEventsCellInfoDF(cellName)
        variable dfrStatus = DataFolderRefStatus(cellDFR)
        DFREF seriesDFR = getSeriesDF(cellName, seriesName)
        variable seriesDFRStatus = DataFolderRefStatus(seriesDFR)
        // printDFREF(dfrStatus)
        if(dfrStatus == 0 | seriesDFRStatus == 0)
            makeBlankTable = 1
        endif
    endif
    // print "blank table", makeBlankTable
    if(!makeBlankTable)
        STRUCT analysisParameters seriesParams
        string detectParamsString
        detectParamsString = getSeriesDetectParams(cellName, seriesName, paramStruct = seriesParams)
        
        // if there was no previous detection
        if(!strlen(detectParamsString)>0)
            makeBlankTable = 1
        endif
    endif

    // print "blank table", makeBlankTable
    if(makeBlankTable)
        changeConfirmVarsToBlanks()
        variable/G panelDFR:placeHolderCheck/N=placeHolderCheck = 0
        
        if(winExists("AGG_Events"))
            CheckBox cellDetectConfirmCheck, variable = placeHolderCheck, win=AGG_Events // hard coded - and leaving it this way since confirmation isn't in opto
        endif
    else
        changeConfirmVarsFromDetectParams(seriesParams)
        variable/G seriesDFR:confirmedDetect/N=confirmedDetect
        if(numtype(confirmedDetect)!=0)
            confirmedDetect = 0
            print "trying to make confirmedDetect variable"
        endif
        // NVAR/Z/SDFR=seriesDFR confirmedDetect
        if(winExists("AGG_Events"))
            CheckBox cellDetectConfirmCheck, variable = confirmedDetect, win=AGG_Events // hard coded
        endif
    endif
end

// This will return the binary numeric data string for the 
// analysisParameters structure that was used for detection with this cell series
// If an analysisParameters structure is provided as an input, this function
// will fill the structure with the StructGet function
// If it is not provided, then the structure will need to be filled in the calling function
// The structure-as-string is basically gibberish by eye because it's binary data

////// EXAMPLE USAGES
    // function testDetectParams ()
    //     STRUCT analysisParameters ps
    //     string detectParams = getSeriesDetectParams("20220115c", "18")
    //     StructGet /S ps, detectParams
    //     print ps.peakThreshold_pA
    // end
    // function testDetectParams_withStruct ()
    //     STRUCT analysisParameters ps
    //     getSeriesDetectParams("20220115c", "18", paramStruct = ps)
    //     print ps.peakThreshold_pA
    // end
/////// END EXAMPLE

function/S getSeriesDetectParams (cellName, seriesName [, paramStruct])
    STRUCT analysisParameters &paramStruct
    string cellName, seriesName

    variable updateStructure
    if(paramisdefault(paramStruct))
        updateStructure = 0
    else
        updateStructure = 1
    endif
    DFREF seriesDFR = getSeriesDF(cellName, seriesName)
    string returnString
    if(DataFolderRefStatus(seriesDFR) != 0)
        // print "found data folder for", cellName, seriesName
        SVAR/SDFR = seriesDFR detectParams

        if(SVAR_exists(detectParams) && strlen(detectParams)>0)
            if(updateStructure)
                StructGet /S paramStruct, detectParams
            endif
            returnString = detectParams 
        else
            returnString = ""
            print "No previous detection params found"
        endif
    else
        // print "detection folder doesn't exist for", cellName, seriesName
        returnString = ""
    endif

    return returnString
end

// function testDetectParams_withStruct ()
//     STRUCT analysisParameters ps
//     getSeriesDetectParams("20220115c", "18", paramStruct = ps)
//     changeConfirmVarsFromDetectParams(ps)
//     print ps.peakThreshold_pA
// end

// This function will update the parameters in the confirmation tab with the values from
// a structure for a given series containing the parameters used for detection
// for that series
function changeConfirmVarsFromDetectParams(paramStruct)
    STRUCT analysisParameters &paramStruct

    Struct analysisParamsFromNVARS confirmParams

    DFREF confirmDF = getEvConfirmDF()
    StructFill/SDFR=confirmDF confirmParams

    // derivative pre-smooth
    confirmParams.gpredsmooth = paramStruct.dPreDerivativeSmoothPoints
    // derivative smoothing
    confirmParams.gdsmooth = paramStruct.dSmoothPoints
    // derivative threshold
    confirmParams.gdthresh = paramStruct.dThreshold_pA_ms / 1e-9
    // derivative min width
    confirmParams.gdmin_dur = paramStruct.dMinWidth_ms / 1e-3
    // derivative max width
    confirmParams.gdmax_dur = paramStruct.dMaxWidth_ms / 1e-3
    // window search
    confirmParams.gdpwin = paramStruct.peakWindowSearch_ms / 1e-3
    // peak sign
    confirmParams.gdetectsign = paramStruct.peakSign
    // peak threshold
    confirmParams.gpthresh = paramStruct.peakThreshold_pA / 1e-12
    // Peak smoothing
    confirmParams.gpsmooth = paramStruct.peakSmoothPoints
    // Area threshold
    confirmParams.gathresh = paramStruct.areaThreshold_pA_ms / 1e-15
    // Area Window
    confirmParams.gawin = paramStruct.areaWindow_ms / 1e-3
    // Baseline offset
    confirmParams.gboffset = paramStruct.baseOffset_ms / 1e-3
    // Baseline duration
    confirmParams.gbdur = paramStruct.baseDuration_ms / 1e-3
    // Ave cutoff
    confirmParams.gavecut = paramStruct.averageCutoff_pA / 1e-12
    // Trace duration
    confirmParams.gtdur = paramStruct.traceDuration_ms / 1e-3
    // Trace offset
    confirmParams.gtoffset = paramStruct.traceOffset_ms / 1e-3
end

function changeDetectParamsFromConfirmParams()
    // print "in change detect"

    Struct analysisParamsFromNVARS confirmParams
    Struct analysisParamsFromNVARS detectParams

    DFREF confirmDF = getEvConfirmDF()
    StructFill/SDFR=confirmDF confirmParams
    DFREF rootDFR
    StructFill/SDFR=rootDFR detectParams

    // derivative pre-smooth
    detectParams.gpredsmooth = confirmParams.gpredsmooth
    // derivative smoothing
    detectParams.gdsmooth = confirmParams.gdsmooth
    // derivative threshold
    detectParams.gdthresh = confirmParams.gdthresh
    // derivative min width
    detectParams.gdmin_dur = confirmParams.gdmin_dur
    // derivative max width
    detectParams.gdmax_dur = confirmParams.gdmax_dur
    // window search
    detectParams.gdpwin = confirmParams.gdpwin
    // peak sign
    detectParams.gdetectsign = confirmParams.gdetectsign
    // peak threshold
    detectParams.gpthresh = confirmParams.gpthresh
    // Peak smoothing
    detectParams.gpsmooth = confirmParams.gpsmooth
    // Area threshold
    detectParams.gathresh = confirmParams.gathresh
    // Area Window
    detectParams.gawin = confirmParams.gawin
    // Baseline offset
    detectParams.gboffset = confirmParams.gboffset
    // Baseline duration
    detectParams.gbdur = confirmParams.gbdur
    // Ave cutoff
    detectParams.gavecut = confirmParams.gavecut
    // Trace duration
    detectParams.gtdur = confirmParams.gtdur
    // Trace offset
    detectParams.gtoffset = confirmParams.gtoffset
end

function changeConfirmVarsToBlanks()
    Struct analysisParamsFromNVARS confirmParams

    DFREF confirmDF = getEvConfirmDF()
    StructFill/SDFR=confirmDF confirmParams

    // derivative pre-smooth
    confirmParams.gpredsmooth = NaN
    // derivative smoothing
    confirmParams.gdsmooth = NaN
    // derivative threshold
    confirmParams.gdthresh = NaN
    // derivative min width
    confirmParams.gdmin_dur = NaN
    // derivative max width
    confirmParams.gdmax_dur = NaN
    // window search
    confirmParams.gdpwin = NaN
    // peak sign
    confirmParams.gdetectsign = NaN
    // peak threshold
    confirmParams.gpthresh = NaN
    // Peak smoothing
    confirmParams.gpsmooth = NaN
    // Area threshold
    confirmParams.gathresh = NaN
    // Area Window
    confirmParams.gawin = NaN
    // Baseline offset
    confirmParams.gboffset = NaN
    // Baseline duration
    confirmParams.gbdur = NaN
    // Ave cutoff
    confirmParams.gavecut = NaN
    // Trace duration
    confirmParams.gtdur = NaN
    // Trace offset
    confirmParams.gtoffset = NaN
end


///////////
/// changeTraceDurationForDetectedSeries
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-18
/// NOTES: Change the stored duration for the detected series, and recalculate average list
///////////
function changeTraceDurationForDetectedSeries(string cellName, variable seriesNum, variable updatedTraceDur)
    string seriesN = buildSeriesWaveName(cellName, seriesNum)
    
    if(!WaveExists($(seriesN + "_ptb")))
        print "changeTraceDurationForDetectedSeries: No PTB wave", cellName, seriesNum, ". Have you detected?"
        return 0
    endif

    DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
    if(dataFolderRefStatus(seriesDFR)==0)
        return 0
    endif

    String/G seriesDFR:detectParams/N=detectParams

    STRUCT analysisParameters paramStruct
    StructGet /S paramStruct, detectParams

    paramStruct.traceDuration_ms = updatedTraceDur

    // Save the structure information
    StructPut /S paramStruct, seriesDFR:detectParams

    updateConfirmationParams(cellName, seriesNum, 1)
end
