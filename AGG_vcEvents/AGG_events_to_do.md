# Errors
* [ ] When clicking on a trace name in collector, if you've got a control in a different window selected, it tries to plot in a graph in that window and leads to errors. Should specify collector window
  * [ ] 2023-08-11, AGG can't figure out exactly what this error is referring to. Can't easily recreate
* [x] The import settings button, though hidden, can still be clicked within the panel it appears
  * If you get to the point where you're tabbing through controls, after "align" in AGG_Events, you get to import settings. I think that there's also an export settings.
  * See issue 40
* [ ] When doing averages for a cell, if there were two different passives for two adjacent series, it messes with the average. Need to average those first, and then average with the rest of them
* [ ] Opto panel - cell info table doesn't exist, `updateCellInfoEventsTable` is hard coded to use AGG_events, and to assume that it's on the top

## Average Event Calc
* [ ] Check relationship to end of series
* [ ] Check intervals

# Cell Info
* [ ] Pull cells / series from collector

# Detection / Confirmation
* [ ] Track if detection changes
  * [ ] Add "completed confirmation" and other tracking information to confirmation report for each cell. When detection changes, this should reset
  * [x] Added confirmed detection checkbox to confirmation tab
  * [x] Need to add stuff so that this changes to a variable for each cell/series folder, and that changes where series changes. Needs to reset when detection is done

# Wave Intrinsic
* [x] Link x axis for raw wave and derivative

# Averages
* [ ] Add series/cell option to only include average subset values for report out
  * [x] Completed 2022-04-25 for series events. Clear interval as not meaningful for average subset
  * [x] Completed for all table outputs - except all series output table, because updating this takes too long
    * [ ] Keep an eye on cell output with more cells and make sure it doesn't take too long if check/uncheck
  * [ ] Update cell group graph
* [ ] Recalculate when showing series
  * Currently, when viewing the average trace from the series, it's going to show it from the parameters at the time of detection for that cell, not the current panel values for offset, duration, etc
* [ ] Think about whether this function should be checking for contamination within the interval, rather than relying solely on the average list
  * [ ] Possible option would be to add another optional function to re-check intervals with current derivative
    * Could mark events with a certain number (like -3 if an auto-detected event shouldn't be included or -4 if a manually-added event shouldn't be included)
    * When recheck, could look for previous events that were rejected for interval. Might not want to automatically add, or somehow check with user
    * Could even just print out a list or somehow highlight events that shouldn't be included because of the interval
* [ ] Add function to calculate tau from average wave

# Concatinating
* [ ] Add smart concatenation option
  * See discussion below for histogram and why this would be necessary

# Results displays
* [ ] Add histogram
  * This is going to be a bit of a lift for cell-based. Need to know the time base associated with each parameter before you can do the parameter over time. Have to get concatentation (probably modified smart conc with the option for whether or not to consider gaps) working
  * Should be easier for series plot, as you can directly associate with the existing PTB
* [ ] Add distribution plot
  * [x] Completed for series summary
    * [ ] Need to incorporate the function that makes the parameter waves into the building of the interface: makeSeriesParamWaves
  * [ ] Need to add to cell summary
    * [ ] Mimic the series summary where there is one list box for the type of plot, and another list box for the parameters that can be plotted with that type of plot
      * Passive parameters over time
      * Event parameters over time
      * Average event
      * Norm average event
      * Distribution - for combined cell
        * [x] To do this, will need to concatenate all of the full event parameter waves together (`_concat` in `summary` folder for each cell)
        * [ ] Then, use the `probdistp` to get the distribution wave for the concatenated
      * Distribution - each series overlayed
      * Use structures to facilitate these decisions/wave creation
    * [ ] Maybe add a function to calculate the parameter value for a certain percentile. Cursor doesn't show it at first glance, at least
* [ ] Add KS plot
* [ ] plot each event per group?
* [ ] Add each cell's/series's trace separately onto a graph (for average event, distribution, etc)

# Standalone panels

## Plot Group Data
* [ ] Select numeric wave, group wave, where to save, where to plot, label

## Raw Data Viewing/Saving
* [ ] Options for smoothing or not, setting axes, etc



# Opto panel

## Raw data
* [ ] Add a wave-specfic variable for indicating which trace is which
  * [ ] Current clamp - voltage trace as primary raw. This will probably be 2
  * [ ] Voltage clamp - current trace as primary raw. This will probably be 1
  * [ ] Red/blue stim. Can be switched

## Add an "info" tab
* [ ] Show the label for each trace
* [ ] Indicate primary 
* [ ] Indicate stim channels
  * [ ] This may be easiest if it's a table format, something like the passive indication
  * [ ] Pull the label with getWaveLabel function
  * [ ] Use the label to make a guess about the traces - CC* -> primary 2
* [ ] Show the label in other places when working with the opto panel


## Action Potential Analysis tab
* [x] Use the updated AP detection functions to be able to do AP analysis on selected traces
  * Completed 2022-12-13 with Rudi
  * Can choose to store and display the individual APs
  * [ ] Level crossing width determined physiologically
* [ ] Could think about a detection panel similar to the blast panel
  * [ ] Need a way to delete and add APs
* [ ] Minimum functionality, though, need a PTB for events


## Evoked opto
* [ ] Need to be able to specify which trace (i.e., APs in trace 2. Don't assume trace 1)
* [ ] Compare PTBs to stim information - use this to mark each stim as evoked or not. Want this to be more automatic than current evoked panel, which requires lots of clicks for each series, and there are hundreds.
  * [ ] Need to be able to specify interval after which counts as evoked
  * [ ] Count the number of events within the evoked period


# Completed

## Cerebro
* [x] Check wave exists prior to running cerebro from events panel

# Concatinating
* [x] Recopy all series waves into detect waves DF - 2022-04-25
  * Added `copySeriesEventsWaves` before concatenating each series
* [x] Make sure that it always works when there's only one series
  * As far as I can tell, this is working

# Unique Groups
* [x] check for no groups before duplicating
  * 2022-04-25: I didn't change anything, but `FindDuplicates` within `GetUniqueGroups` seemed to work even when there was just a blank list provided to it

# Units
* [x] Update convert units for cell calculations, etc
  * Right now, if this is changed, it wouldn't change previous cell concatenations - 2022-04-25 fixed
  * Doesn't change previous series calculations unless the table is updated, or the series is clicked on