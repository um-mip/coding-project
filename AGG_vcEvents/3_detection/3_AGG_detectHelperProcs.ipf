///////////
/// differentiateRawWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-13
/// NOTES: Differentiates the raw wave
/// first smooths the raw wave with box smoothing according to the number of
/// points given by dpresmooth, and then differentiates, and then smooths further by
/// points given by dsmooth
/// 
/// if the function can't find the rawWave, it will return a blank wave reference
///
/// the derivative wave name is the same as the rawWave, with "-deriv" appended at the end
/// the wave is saved in the same folder as rawWave
///////////
function/Wave differentiateRawWave(Wave rawWave, variable dpresmooth, variable dsmooth)
    if(!WaveExists(rawWave))
        print "differentiateRawWave: cannot find rawWave"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    string derivN = waveN + "-deriv"
    duplicate/O rawWave, waveDFR:$(derivN)
    Wave deriv = waveDFR:$(derivN)

    if(dpresmooth>0)
        smooth /B dpresmooth, deriv
    endif

    differentiate deriv
    SetScale d 0, 0, "A/sec", deriv

    if(dsmooth>0)
        smooth /B dsmooth, deriv
    endif

    return deriv
end

///////////
/// findDerivLevelCrossings
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-13
/// NOTES: Finds the level crossings within the derivative wave
/// returns the levels crossings wave and the number of level crossings
///////////
function[Wave/D derivLevelCrossings, variable nLevels] findDerivLevelCrossings(wave deriv, variable dthresh, variable peakSign, string baseWaveN)
    if(!WaveExists(deriv))
        print "findDerivLevelCrossings: Could not find the derivative wave"
        abort
    endif

    dthresh*=peakSign // convert derivative threshold based on peak sign
    
    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(deriv)

    string levelCrossingN = baseWaveN + "_lev"
    make/D/O waveDFR:$levelCrossingN/Wave=derivLevelCrossings

    findlevels/Q/D=derivLevelCrossings deriv, dthresh

    nLevels = V_levelsFound

    if(nLevels>50000)
        string mymessages="Lots of events!  N="+num2str(nLevels)+"  Continue? 1 is yes, 0 is no"
        variable accept=acceptReject(mymessages)
        if(accept==0)
            print "User abort"
            abort
        endif
    endif

    return [derivLevelCrossings, nLevels]
end


///////////
/// makeDetectionStorageWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: Globally makes the waves that will be used to save the derivative detection
/// data. The waves will be made in the same folder as the rawWave
/// The waves to be made and the extensions will be determined by
/// the derivDetectStruct and the `returnExt` function
/// onlyIfDontExist : optional parameter
///     if set to 1, will only make and set to NaN waves that don't already exist
///     this will be useful for reprocessing based on a PTB wave, since there are
///     some new waves created that didn't exist in old detection protocols
///     ** Will still redimension the already existing wave to make sure that it has the same # of points, though
///////////
function makeDetectionStorageWaves(Wave rawWave, variable nLevels[, variable onlyIfDontExist])
    if(!WaveExists(rawWave))
        print "makeDetectionStorageWaves: rawWave does not exist. Quitting"
        abort
    endif
    
    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    Struct derivDetectStruct derivDetectStruct

    fillDerivDetectStruct(derivDetectStruct)

    string waveTypes = derivDetectStruct.waveTypes
    variable nWaves = itemsInList(waveTypes)
    variable iWave
    for(iWave=0; iWave<nWaves; iWave++)
        string thisWaveType = StringFromList(iWave, waveTypes)
        string waveExt = returnExt(thisWaveType)
        Wave/Z/D/SDFR=waveDFR thisWave = $(waveN + waveExt)
        variable makeWaves = 1
        if(onlyIfDontExist && WaveExists(thisWave))
            makeWaves = 0
            redimension/N=(nLevels)/D thisWave // make all of these double precision
        endif

        if(makeWaves)
            make/D/N=(nLevels)/O waveDFR:$(waveN+waveExt)/Wave=thisWave
            thisWave = NaN
        endif
    endfor
end


///////////
/// smoothRawWaveForDerivDetect
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-13
/// NOTES: Smooths the raw wave with box smoothing according to the number of
/// points given by peak_smooth
/// 
/// if the function can't find the rawWave, it will abort
///
/// the smoothed wave name is the same as the rawWave, with "-smth" appended at the end
/// the wave is saved in the same folder as rawWave
///////////
function/Wave smoothRawWaveForDerivDetect(Wave rawWave, variable peak_smooth)
    if(!WaveExists(rawWave))
        print "smoothRawWaveForDerivDetect: cannot find rawWave"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    string smoothN = waveN + "-smth"
    duplicate/O rawWave, waveDFR:$(smoothN)
    Wave smoothedW = waveDFR:$(smoothN)

    if(peak_smooth>0)
        smooth /B peak_smooth, smoothedW
    endif

    return smoothedW
end

///////////
/// resetDetectVarsToNaN
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: resets the global detect variables to NaN
/// should be reset before each level that is checked
/// default folder is derivDetectVars
///////////
function resetDetectVarsToNaN([DFREF varsDFR])
    if(paramIsDefault(varsDFR))
        varsDFR = getDerivDetectVarsDF()
        if(dataFolderRefStatus(varsDFR)==0)
            createDerivDetectVarsDF()
            varsDFR = getDerivDetectVarsDF()
        endif
    endif

    Struct derivDetectStruct dds

    StructFill/SDFR = varsDFR/AC=1 dds

    fillDerivDetectStruct(dds)

    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        string thisType = StringFromList(iWaveType, dds.waveTypes)
        string thisVar = StringByKey(thisType, dds.typeToVar)
        NVAR/Z/SDFR=varsDFR localVar = $(thisVar)
        localVar = NaN
    endfor

    if(strlen(dds.varsOnly)>0)
        variable nVarsOnly = itemsInList(dds.varsOnly)
        variable iVar
        for(iVar=0; iVar<nVarsOnly; iVar++)
            string thisOnlyVar = StringFromList(iVar, dds.varsOnly)
            NVAR/Z/SDFR=varsDFR localVar = $(thisOnlyVar) 
            localVar = NaN
        endfor
    endif
end

///////////
/// checkRisingPhaseDeriv
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: Checks that the time for the local derivative peak is after the time of the level crossing
/// this tells us that we're looking at a level crossing that is on the rising phase
/// of a derivative peak
/// Function returns 0 (not on rising phase) or 1 (on rising phase)
///////////
function checkRisingPhaseDeriv(wave deriv, variable crossingTime, variable min_dur, variable thissign)
    variable isRising = 0
    if(!WaveExists(deriv))
        print "checkRisingPhaseDeriv: Can't find derivative wave. Quitting"
        abort
    endif

    WaveStats/Q/R=(crossingTime-min_dur, crossingTime+min_dur) deriv
    
    variable localPeakTime = NaN
    if(thissign<0)
        localPeakTime = V_minloc
    else
        localPeakTime = V_maxloc
    endif

    if(localPeakTime > crossingTime)
        isRising = 1
    endif

    return isRising
end


///////////
/// summarizeAcceptDecisions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-16
/// NOTES: 
///////////
function summarizeAcceptDecisions(wave levelDecisions)
    variable nReasons = WaveMax(levelDecisions) - WaveMin(levelDecisions)  + 1
    Make/O/N=(nReasons) reasonHisto   //Create the histogram wave with required bins


    histogram/B=2 levelDecisions, reasonHisto
    
    variable numLevels = numpnts(levelDecisions)
    print numLevels, "level crossings were found"

    variable iReason
    for(iReason=0; iReason<nReasons; iReason++)
        variable countOfReason = reasonHisto[iReason]
        if(countOfReason > 0)
            string reasonText = textForAcceptDecision(iReason)
            if(StringMatch(reasonText, "accept"))
                print countOfReason, "level crossings were accepted (", (countOfReason/numLevels) * 100 , "%)"
            else
                print countOfReason, "level crossings were rejected for the following reason:", reasonText, "(", (countOfReason/numLevels) * 100 , "%)"
            endif
        endif
    endfor

    Killwaves/Z reasonHisto
end

    ///////////
    /// textForAcceptDecision
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-16
    /// NOTES: Return the text for the reason for accept decision/rejection
    ///////////
    function/S textForAcceptDecision(variable acceptDecision)
        string reason = ""
        switch (acceptDecision)
            case 0:
                reason = "not rising"
                break
            case 1:
                reason = "too close to end"
                break
            case 2:
                reason = "peak doesn't pass threshold"
                break
            case 3:
                reason = "area doesn't pass threshold"
                break
            case 4:
                reason = "manual rejection"
                break
            case 5: 
                reason = "accept"
                break
            case 6:
                reason = "too close to previous event"
                break
            case 7:
                reason = "level crossing before previous event"
                break
            default:
                reason = "unknown"
                break
        endswitch
        return reason
    end
    
///////////
/// compareLevelDecisions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-16
/// NOTES: Compares the level crossing sorting decisions
///////////
function compareLevelDecisions(wave decisions1, wave decisions2)
    variable numLevels1 = numpnts(decisions1)
    variable numLevels2 = numpnts(decisions2)

    if(numLevels1 == numLevels2)
        variable iLevel = 0
        for(iLevel=0; iLevel<numLevels1; iLevel++)
            variable decision1 = decisions1[iLevel]
            variable decision2 = decisions2[iLevel]

            if(decision1 != decision2 && !(decision1 == 6 && decision2 == 7))
                string reason1 = textForAcceptDecision(decision1)
                string reason2 = textForAcceptDecision(decision2)
                print "Level", iLevel, "decision 1:", reason1, "decision 2:", reason2
            endif
        endfor
    endif
end



///////////
/// calculateAverageList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-18
/// NOTES: 
/// codes for average list wave - see avgDecisionVal function
/// 0 = intervals don't pass, no user action
/// 1 = intervals pass, no user action
/// 2 = user marked to include, but prompt if interval is too short in future
/// 3 = user marked to include, don't reprompt regardless of future intervals
/// -2 = user marked to not include, but prompt if interval is good in the future
/// -3 = user marked to not include, don't reprompt regardless of future intervals
///////////
function/Wave calculateAverageList(Wave backInt, wave forInt[, variable traceDur, variable checkForRejected])
    if(!WaveExists(backInt) || !WaveExists(forInt))
        print "calculateAverageList couldn't find either the backwards interval or forwards interval wave"
        return $("") // return a blank wave
    endif

    string backIntN
    DFREF backIntDFR
    [backIntN, backIntDFR] = getWaveNameAndDFR(backInt)

    string baseN = getWaveBasename(backIntN)

    string avgListN = baseN + returnExt("ave list")
    Wave/SDFR=backIntDFR avgList = $(avgListN)

    variable pntsBack = numpnts(backInt)
    variable pntsFor = numpnts(forInt)
    variable pntsAvg = numpnts(avgList)
    if(pntsBack != pntsFor || pntsBack != pntsAvg)
        print "calculateAverageList: the number of points in the backInt, forInt, and average list waves don't match:", pntsBack, "in backInt", pntsFor, "in forInt", pntsAvg, "in average list"
        return $("") // return a blank wave
    endif



    // Option to double-check events with a good interval
    if(paramIsDefault(checkForRejected))
		checkForRejected = 0 //default to false
	endif

    // If traceDur isn't provided, use what's in the panel now
    if(paramIsDefault(traceDur))
        STRUCT analysisParameters ps
        variable worked = readpanelparams2(ps)
        traceDur = ps.traceDuration_ms	
    endif

    variable iEvent = 0

    variable autoAccept = avgDecisionVal("autoAccept")
    variable autoReject = avgDecisionVal("autoReject")
    variable manualAccept = avgDecisionVal("manualAccept")
    variable manualReject = avgDecisionVal("manualReject")
    variable manualAcceptDontAsk = avgDecisionVal("manualAcceptDontAsk")
    variable manualRejectDontAsk = avgDecisionVal("manualRejectDontAsk")

    for(iEvent=0; iEvent<pntsBack; iEvent++)
        variable thisBackInt = backInt[iEvent]
        variable thisForInt = forInt[iEvent]
        variable avgDecision = avgList[iEvent]

        variable passesIntervals = (thisBackInt > traceDur) && (thisForInt > traceDur)
        
        variable isNaN = numtype(avgDecision) == 2

        if(passesIntervals)
            // can't use a switch statement because they must be constants, can't use variables
            if(avgDecision == autoReject || isNaN)
                // was previously excluded automatically or NaN
                // but the interval is good now
                avgList[iEvent] = autoAccept
            else
                if(checkForRejected && avgDecision == manualReject)
                    avgList[iEvent] = checkAvgRejectWithUser(iEvent, thisBackInt, thisForInt, traceDur)
                else
                    // don't change if the previous tag was
                    // - manualRejectDontAsk
                    // - auto or manual accepts
                endif
            endif
        else // at least one of the intervals is too small
            if(avgDecision == autoAccept || isNaN)
                // was previously included automatically or NaN
                // but the interval is bad now
                avgList[iEvent] = autoReject
            else
                if(avgDecision == manualAccept)
                    // marked to accept, interval is bad
                    // prompt user
                    avgList[iEvent] = checkAvgRejectWithUser(iEvent, thisBackInt, thisForInt, traceDur)
                else
                    if(avgDecision == manualAcceptDontAsk)
					    print "WARNING: Event", iEvent, "has an interval that is less than the trace duration, but is force-included in the average list"
                    else
                        // don't change if the previous decision was rejection
                    endif
                endif
            endif
        endif
    endfor
    return averageList
end

    ///////////
    /// checkAvgRejectWithUser
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-18
    /// NOTES: Returns a retag value based on the user
    /// decision for an event that was rejected but has a good interval
    ///////////
    function checkAvgRejectWithUser(variable iEvent, variable backInt, variable forInt, variable traceDur)
        string warningMessage = ""
        variable userChoice, retagEvent

        warningMessage = "Event " + num2str(iEvent)
        warningMessage += " has an front interval of " + num2str(forInt)
        warningMessage += " and a back interval of " + num2str(backInt)
        warningMessage += " both of which are more than the trace duration of " + num2str(traceDur)
        warningMessage += ". How do you want to proceed?"

        Prompt userChoice, warningMessage, popup, "Don't include in average - don't ask again;Don't include in average - ask again;Include in average"
        DoPrompt "Address interval", userChoice
        if (V_Flag)
            userChoice = 2 // If user cancels, don't include event in average, but ask again
        endif

        switch (userChoice)
            case 1:
                retagEvent = avgDecisionVal("manualRejectDontAsk") // don't include in average, don't ask again
                break
            case 2:
                retagEvent = avgDecisionVal("manualReject") // don't include in average, ask again
                break
            case 3:
                retagEvent = avgDecisionVal("autoAccept") // Include
                break
            default:
                
                break
        endswitch
        
        return retagEvent
    end

    ///////////
    /// checkAvgAcceptWithUser
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-18
    /// NOTES: Returns a retag value based on the user
    /// decision for an event that was accepted but has a bad interval
    ///////////
    function checkAvgAcceptWithUser(variable iEvent, variable backInt, variable forInt, variable traceDur)
        string warningMessage = ""
        variable userChoice, retagEvent

        warningMessage = "Event " + num2str(iEvent)
        warningMessage += " has an front interval of " + num2str(forInt)
        warningMessage += " and a back interval of " + num2str(backInt)
        warningMessage += " at least one of which is less than the trace duration of " + num2str(traceDur)
        warningMessage += ". How do you want to proceed?"

        Prompt userChoice, warningMessage, popup, "Don't include in average;Include in average - but ask me again;Include in average, and don't ask again"
        DoPrompt "Address interval", userChoice
        if (V_Flag)
            userChoice = 1 // If user cancels, don't include event in average
        endif

        switch (userChoice)
            case 1:
                retagEvent = avgDecisionVal("autoReject") // don't include in average
                break
            case 2:
                retagEvent = avgDecisionVal("manualAccept") // Include, but keep prompting user
                break
            case 3:
                retagEvent = avgDecisionVal("manualAcceptDontAsk") // Include, don't reprompt the user about this event's interval
                break
            default:
                
                break
        endswitch
        
        return retagEvent
    end


    ///////////
    /// checkDerivChangeWithUser
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-18
    /// NOTES: Returns a retag value based on the user
    /// decision for an event that was accepted but has a bad interval
    ///////////
    function checkDerivChangeWithUser()
        string warningMessage = ""
        variable userChoice

        warningMessage = "We have found a new derivative peak. How do you want to proceed?"

        Prompt userChoice, warningMessage, popup, "Keep the old derivative peak;Update the derivative peak;"
        DoPrompt "Different peak", userChoice
        if (V_Flag)
            // If user cancels, abort. Learned the hard way can get into a massive loop here
            abort 
        endif

        return userChoice // note that 1 = keep, 2 = update
    end


    ///////////
    /// avgDecisionVal
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-18
    /// NOTES: 
    ///////////
    function avgDecisionVal(string decisionText)
        variable decisionVal
        strswitch (decisionText)
            case "autoReject":
                decisionVal = 0
                break
            case "autoAccept":
                decisionVal = 1
                break
            case "manualAccept":
                decisionVal = 2
                break
            case "manualReject":
                decisionVal = -2
                break
            case "manualAcceptDontAsk":
                decisionVal = 3
                break
            case "manualRejectDontAsk":
                decisionVal = -3
                break
            default:
                decisionVal = NaN
                break
        endswitch
        return decisionVal
    end