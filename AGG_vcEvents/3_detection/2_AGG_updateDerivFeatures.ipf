///////////
/// updateDerivFeatures
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: Updates the event variables within the derivDetectVars folder
/// returns 0/1 for accepting the event or not
///////////
function [variable accept, variable acceptDecision] updateDerivFeatures(Wave rawWave, Wave smoothedW, Wave deriv, variable derivTime[, variable checkThreshold, variable checkArea, variable manualConfirm, variable updateGraphs, variable askIfDerChange, variable keepDerTime])
    // print "derivTime", derivTime
    if(!WaveExists(rawWave) || !WaveExists(deriv))
        print "updateDerivFeatures: cannot find either rawWave or derivative wave. Quitting"
        abort
    endif

    if(paramIsDefault(checkThreshold))
        checkThreshold = 1
    endif

    if(paramIsDefault(checkArea))
        checkArea = 1
    endif
    
    if(paramIsDefault(manualConfirm))
        manualConfirm = 0
    endif

    if(paramIsDefault(updateGraphs))
        updateGraphs = 0
    endif

    if(paramIsDefault(askIfDerChange))
        askIfDerChange = 0 // turn on when reprocessing events
    endif

    if(paramIsDefault(keepDerTime))
        keepDerTime = 0 // turn on when adding by peak time from wave intrinsic/cerebro. Already looked backwards for d peak, will find later local mins sometimes
    endif
    

    DFREF varsDFR = getDerivDetectVarsDF()

    Struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    StructFill/SDFR = varsDFR dds // to use NVARs
    
    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	

    // baseline params
    variable base_offset = ps.baseOffset_ms	
    variable base_dur = ps.baseDuration_ms	

    // peak parameters
    variable thissign = ps.peaksign					
    variable thresh = ps.peakThreshold_pA		
    variable peak_smooth = ps.peakSmoothPoints			
    variable area_thresh = ps.areaThreshold_pA_ms	

    // output trace parameters
    variable trace_dur = ps.traceDuration_ms	
    variable trace_offset = ps.traceOffset_ms	
    variable avecutoff =  ps.averageCutoff_pA		

    //control parameters
    variable automan =  ps.automan
    variable displayplots =  ps.displayplots

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    accept = 0
    acceptDecision = NaN

    //////////////////     Start processing      \\\\\\\\\\\\\\\\\\\\\\\

    variable isFarFromEnd = checkDerFarFromEnd(derivTime, dx, maxTime, chunk, peak_smooth)

    if(!isFarFromEnd)
        // too close to end of series to detect
        acceptDecision = 1
        return [accept, acceptDecision]
    endif

    if(!keepDerTime)
        
        [variable dPeak, variable dPeak_time] = getLocalDerivPeak(deriv, derivTime, chunk, thissign)

        [variable peak, variable peak_time] = getLocalPeak(smoothedW, dPeak_time, chunk, thisSign)
        
        // Have to have these before for values to update to plot
        // if there are differences
        dds.dPeak = dPeak
        dds.dPeak_time = dPeak_time
        dds.peak = peak
        dds.peak_time = peak_time

        if(askIfDerChange && (dPeak_time - derivTime) >=  dx) // at least one point different
            // Struct derivDetectStruct prevVals 
            // StructFill/SDFR=getPrevDerivDetectVarsDF() prevVals
            // print prevVals
            comparePrevDetectDisplay(rawWave, smoothedW, deriv)

            doupdate/W=AGG_Events/E=1

            print "checked about", nameOfWave(rawWave), "around time", dPeak_time
            variable userChoice = checkDerivChangeWithUser()
            if(userChoice == 1) // keep previous derivative time
                variable roundedDerivTime = round(1/dx * derivTime) / (1/dx)
                dPeak_time = roundedDerivTime // round it to the nearest xStep. Originally not double precision. This should fix some of that
                dPeak = deriv(dPeak_time) // get the derivative at dPeak_time

                [peak, peak_time] = getLocalPeak(smoothedW, dPeak_time, chunk, thisSign)
        
                dds.dPeak = dPeak
                dds.dPeak_time = dPeak_time
                dds.peak = peak
                dds.peak_time = peak_time

                comparePrevDetectDisplay(rawWave, smoothedW, deriv)
                doupdate
            endif
        endif
    else
        roundedDerivTime = round(1/dx * derivTime) / (1/dx)
        dPeak_time = roundedDerivTime // round it to the nearest xStep. Originally not double precision. This should fix some of that
        dPeak = deriv(dPeak_time) // get the derivative at dPeak_time

        [peak, peak_time] = getLocalPeak(smoothedW, dPeak_time, chunk, thisSign)

        dds.dPeak = dPeak
        dds.dPeak_time = dPeak_time
        dds.peak = peak
        dds.peak_time = peak_time
    endif

    [variable baseline, variable base_start, variable base_end] = getLocalBaseline(rawWave, dPeak_time, base_dur, base_offset)
    dds.baseline = baseline
    dds.base_start = base_start
    dds.base_end = base_end

    [variable relPeak, variable passesThreshold] = checkPeakPassesThreshold(peak, baseline, thresh, thisSign)
    dds.relPeak = relPeak

    if(checkThreshold && !passesThreshold) 
        // if we want to check the threshold, and it doesn't pass the threshold
        // return with the values as is, and don't accept
        // print "Derivative time of", derivTime, "has a peak", relPeak * 1e12, "that is less than the threshold. Not including"
        acceptDecision = 2
        return [accept, acceptDecision]
    endif

    //TO-DO: Rise search needs to be longer than the window search!!
    [variable p50, variable p10, variable p90, variable t50rise_time, variable t50rise, variable rise50_time, variable fall50_time, variable fwhm, variable fall90_time, variable fall10_time, variable decay1090, variable diffRiseSmooth] = returnPercPeakFeatures(rawWave, smoothedW, relPeak, peak_time, thisSign, baseline, chunk * 2, trace_dur) // chunk is the window search, and rise search. Trace_dur is used for duration for fall search
    dds.t50rise_time = t50rise_time
    dds.t50rise = t50rise
    dds.fwhm = fwhm
    dds.decay1090 = decay1090
    dds.diffRiseSmooth = diffRiseSmooth
    dds.rise50_time = rise50_time
    dds.fall50_time = fall50_time
    dds.fall10_time = fall10_time
    dds.fall90_time = fall90_time
    dds.p50 = p50
    dds.p10 = p10
    dds.p90 = p90

    // to-do: Decide if the area based on FWHM should be raw rise or smoothed rise
    // also, need to determine what to do if couldn't find a t50rise_time
    variable this_area = calcDeflectionArea(rawWave, t50rise_time, fall50_time, baseline)
    dds.this_area = this_area

    variable passesArea = checkAreaPassesThreshold(this_area, area_thresh, thisSign)

    // TO-DO: This means that it will reject any events that don't have a t50rise found
    // because the area will then be NaN
    // Determine if that's the intended behavior, or want to set a different start time
    // for the area detection without a t50rise
    if(checkArea && !passesArea)
        // if we want to check the area, and it doesn't pass the area threshold
        // return with the values as is, and don't accept
        // print "Derivative time of", derivTime, "has an area of", this_area * 1e15, "that is less than the threshold. Not including"
        acceptDecision = 3
        return [accept, acceptDecision]
    endif

    if(updateGraphs)
        displayDerivProcessing(rawWave, smoothedW, deriv)
    endif

    if(manualConfirm)
        print "Derivative time", derivTime, "area threshold",area_thresh*1e15,  "area", this_area*1e15, "peak",relPeak*1e12, "baseline",baseline*1e12
        accept = acceptReject("Accept?")
        if(accept == 0)
            acceptDecision = 4
        else
            acceptDecision = 5
        endif
    else
        acceptDecision = 5
        accept = 1
    endif

    return [accept, acceptDecision]
end

///////////
/// updateRiseFallFeatures
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-29
/// NOTES: Updates the rise/fall50 times, fall10/90 times, P50, P10, and P90
/// To be used to update the series that were already updated, but didn't save these values
/// Shouldn't need to be used frequently in the future
///////////
function updateRiseFallFeatures(Wave rawWave, Wave smoothedW, Wave deriv, variable iEvent)
    // print "derivTime", derivTime
    if(!WaveExists(rawWave) || !WaveExists(deriv))
        print "updateRiseFallFeatures: cannot find either rawWave or derivative wave. Quitting"
        abort
    endif    

    DFREF varsDFR = getDerivDetectVarsDF()

    Struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    StructFill/SDFR = varsDFR dds // to use NVARs
    
    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	

    // baseline params
    variable base_offset = ps.baseOffset_ms	
    variable base_dur = ps.baseDuration_ms	

    // peak parameters
    variable thissign = ps.peaksign					
    variable thresh = ps.peakThreshold_pA		
    variable peak_smooth = ps.peakSmoothPoints			
    variable area_thresh = ps.areaThreshold_pA_ms	

    // output trace parameters
    variable trace_dur = ps.traceDuration_ms	
    variable trace_offset = ps.traceOffset_ms	
    variable avecutoff =  ps.averageCutoff_pA		

    //control parameters
    variable automan =  ps.automan
    variable displayplots =  ps.displayplots

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    DFREF waveDFR
    string waveN
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    //////////////////     Start processing      \\\\\\\\\\\\\\\\\\\\\\\

    variable peak_time, relPeak, baseline

    Wave/SDFR=waveDFR relPeakW = $(waveN + returnExt("relative peak")), ptbW = $(waveN + returnExt("peak time")), baseW = $(waveN + returnExt("baseline"))
    peak_time = ptbW[iEvent]
    relPeak = relPeakW[iEvent]
    baseline = baseW[iEvent]

    [variable p50, variable p10, variable p90, variable t50rise_time, variable t50rise, variable rise50_time, variable fall50_time, variable fwhm, variable fall90_time, variable fall10_time, variable decay1090, variable diffRiseSmooth] = returnPercPeakFeatures(rawWave, smoothedW, relPeak, peak_time, thisSign, baseline, chunk * 2, trace_dur) // chunk is the window search, and rise search. Trace_dur is used for duration for fall search
    dds.t50rise_time = t50rise_time
    dds.t50rise = t50rise
    dds.fwhm = fwhm
    dds.decay1090 = decay1090
    dds.diffRiseSmooth = diffRiseSmooth
    dds.rise50_time = rise50_time
    dds.fall50_time = fall50_time
    dds.fall10_time = fall10_time
    dds.fall90_time = fall90_time
    dds.p50 = p50
    dds.p10 = p10
    dds.p90 = p90

    return 0
end


///////////
/// checkDerFarFromEnd
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: 
///////////
function checkDerFarFromEnd(variable derivTime, variable dx, variable maxTime, variable winSearch, variable peak_smooth)
    variable isFarFromEnd = derivTime < (maxTime - winSearch - dx * peak_smooth)
    return isFarFromEnd
end



///////////
/// getLocalDerivPeak
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: This returns the value and time of the local derivative peak
///////////
function [variable dPeak, variable dPeak_time] getLocalDerivPeak(wave deriv, variable derivTime, variable winSearch, variable thissign)
    if(!WaveExists(deriv))
        print "getLocalDerivPeak: cannot find derivative wave. Quitting"
        abort
    endif

    WaveStats/Q/R=(derivTime, derivTime + winSearch) deriv
    if(thissign<0)
        dPeak = V_min
        dPeak_time = V_minloc
    else
        dPeak = V_max
        dPeak_time = V_maxloc
    endif

    return[dPeak, dPeak_time]
end

///////////
/// getLocalPeak
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: This returns the value and time of the local signal peak
/// The raw data wave should already be smoothed before passing to this function
/// Previous iterations duplicated the chunk to be searched, and then
/// just smoothed on that chunk, but this could leads to different values for the peaks
/// if they were less than the # of smoothed points from the edge of the search chunk.
/// It's better to smooth the entire raw data wave, and then just look at the chunk of interest for the peak
///////////
function [variable peak, variable peak_time] getLocalPeak(wave smoothedW, variable startSearchTime, variable searchDur, variable thissign)
    if(!WaveExists(smoothedW))
        print "getLocalPeak: cannot find raw data wave. Quitting"
        abort
    endif

    WaveStats/Q/R=(startSearchTime, startSearchTime + searchDur) smoothedW

    if(thissign<0)
        peak = V_min
        peak_time = V_minloc
    else
        peak = V_max
        peak_time = V_maxloc
    endif

    variable showPeak = 0
    if(showPeak)
        display/K=1 smoothedW
        SetAxis bottom, startSearchTime, startSearchTime + searchDur
        SetAxis/A=2 left
        make/O/N=1 plotPeak_local = {peak}
        make/O/N=1 plotPeak_time_local = {peak_time}

        appendtograph plotPeak_local vs plotPeak_time_local
        makeDotMarkers("plotPeak_local") 
        
    endif


    return[peak, peak_time]
end

///////////
/// getLocalBaseline
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Returns the local baseline for the raw wave starting base_offset before the 
/// startSearchTime, and going further backwards base_dur
///////////
function [variable baseline, variable base_start, variable base_end] getLocalBaseline(wave rawWave, variable startSearchTime, variable base_dur, variable base_offset)
    if(!WaveExists(rawWave))
        print "getLocalBaseline: cannot find raw data wave. Quitting"
        abort
    endif

    base_start = startSearchTime - base_dur - base_offset
    base_end = startSearchTime - base_offset

    wavestats/Q/R=(base_start, base_end) rawWave
    baseline = V_avg
    return [baseline, base_start, base_end]
end

///////////
/// checkPeakPassesThreshold
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Check that the peak of the signal data passes the
/// peak threshold and is also greater than the baseline
///////////
function [variable relPeak, variable passesThreshold] checkPeakPassesThreshold(variable peak, variable baseline, variable thresh, variable thisSign)
    passesThreshold = 0

    relPeak = peak - baseline

    // want to make sure that the relative peak is an absolute value
    // to compare to the absolute value of the threshold
    variable relAmpPassesThresh = (relPeak * thisSign) > thresh
    
    // the thisSign is necessary to convert based on 
    // the expected direction of the peak
    variable peakGreaterBasline = (thisSign * peak) > (thisSign * baseline)
    // if(relAmpPassesThresh && peakGreaterBasline) // defazio 20240816 i don't understand the second criterium
    if(relAmpPassesThresh) 
        passesThreshold = 1
    endif

    return [relPeak, passesThreshold]
end

///////////
/// returnPercPeakFeatures
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Calculate the following
/// - t50rise: TO-DO: Decide if this should be on the smoothed or raw wave
/// - fwhm: Need the rise 50 and fall 50 times
/// - decay1090: 90% to 10% of peak decay time
/// Using the already calculated peak and peak_time to be sure that the peak that is
/// being considered is the correct peak, instead of refinding it
/// 
/// Recommended value for the rise search time: window search
/// Recommended value for the fall search time: trace duration
///////////
function [variable p50, variable p10, variable p90, variable t50rise_time, variable t50rise, variable rise50_time, variable fall50_time, variable fwhm, variable fall90_time, variable fall10_time, variable decay1090, variable diffRiseSmooth] returnPercPeakFeatures(wave rawWave, wave smoothedW, variable relPeak, variable peak_time, variable thisSign, variable baseline, variable rise50_searchWindow, variable fall_searchWindow)
    if(!WaveExists(rawWave) || !WaveExists(smoothedW))
        print "returnPercPeakFeatures: cannot find raw data wave and/or smoothed data wave. Quitting"
        abort
    endif

    p50 = (relPeak * .5) + baseline
    p10 = (relPeak * .1) + baseline
    p90 = (relPeak * .9) + baseline

    // TO-DO: Decide if want this to continue to be run on the unsmoothed data
    [t50rise_time, t50rise] = calcRisePeakCrossing(rawWave, p50, peak_time, rise50_searchWindow)

    [rise50_time, variable rise50] = calcRisePeakCrossing(smoothedW, p50, peak_time, rise50_searchWindow)
    [fall50_time, variable fall50] = calcFallPeakCrossing(smoothedW, p50, peak_time, fall_searchWindow)

    fwhm = fall50_time - rise50_time

    diffRiseSmooth = t50rise_time - rise50_time

    [fall90_time, variable fall90] = calcFallPeakCrossing(smoothedW, p90, peak_time, fall_searchWindow)
    [fall10_time, variable fall10] = calcFallPeakCrossing(smoothedW, p10, peak_time, fall_searchWindow)

    decay1090 = fall10_time - fall90_time

    // print "relPeak", relPeak * 1e12, "p50", p50 * 1e12, "p90", p90 * 1e12, "p10", p10 * 1e12
    // print "t50rise_time, unsmoothed", t50rise_time, "rise50_time, smoothed", rise50_time, "fall50_time", fall50_time
    // print "difference unsmoothed smoothed", diffRiseSmooth
    // print "fwhm", fwhm, "decay1090", decay1090

    variable showPlot = 0
    if(showPlot)
        display/K=1 rawWave
        SetAxis bottom, peak_time - rise50_searchWindow * 2, fall10_time + rise50_searchWindow
        SetAxis/A=2 left
        ModifyGraph rgb=(0,0,0)

        AppendToGraph smoothedW
        makeColorSky(nameOfWave(smoothedW))

        make/O/N=1 plotPeak = {relPeak + baseline}
        make/O/N=1 plotP50 = {p50}
        make/O/N=1 plotP10 = {p10}
        make/O/N=1 plotP90 = {p90}

        make/O/N=1 plotPeak_time = {peak_time}
        make/O/N=1 plotT50_time = {t50rise_time}
        make/O/N=1 plotrise50_time = {rise50_time}
        make/O/N=1 plotfall50_time = {fall50_time}
        make/O/N=1 plotfall10_time = {fall10_time}
        make/O/N=1 plotfall90_time = {fall90_time}

        AppendToGraph plotPeak/TN=peak vs plotPeak_time 
        AppendToGraph plotP50/TN=t50 vs plotT50_time 
        AppendToGraph plotP50/TN=rise50 vs plotrise50_time 
        AppendToGraph plotP50/TN=fall50 vs plotfall50_time 
        AppendToGraph plotP10/TN=fall10 vs plotfall10_time 
        AppendToGraph plotP90/TN=fall90 vs plotfall90_time 

        string traces = "peak;t50;rise50;fall50;fall10;fall90"
        variable iTrace, nTraces = itemsinlist(traces)
        for(iTrace=0; iTrace<nTraces; iTrace++)
            string traceN = StringFromList(iTrace, traces)
            makeDotMarkers(traceN)
        endfor

        makeColorLavender("t50")
        makeColorOrange("fall90")
        makeColorOrange("fall10")
        makeColorGreen("rise50")
        makeColorGreen("fall50")
        
        KillWindow/Z #
    endif

    return [p50, p10, p90, t50rise_time, t50rise, rise50_time, fall50_time, fwhm, fall90_time, fall10_time, decay1090, diffRiseSmooth]
end

    ///////////
    /// makeDotMarkers
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-15
    /// NOTES: 
    ///////////
    function makeDotMarkers(string tracen[, string displayName])
        if(paramIsDefault(displayName))
            displayName = ""
        endif
        
        ModifyGraph/W=$displayName mode($tracen) = 3, marker($tracen)=19
    end


    ///////////
    /// calcRisePeakCrossing
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-15
    /// NOTES: This function looks for the time of the level crossing (crossVal)
    /// for the rising phase of an event
    /// It will start the search at the peak_time and look backwards the given searchInt
    /// The function returns the crossingTime (x-value where crossing occurred, if found)
    /// and the absolute interval of time from the crossingTime to the peak_time
    ///////////
    function [variable crossingTime, variable intToPeak] calcRisePeakCrossing(wave thisWave, variable crossVal, variable peak_time, variable searchInt)
        if(!WaveExists(thisWave))
            print "calcRisePeakCrossing: cannot find provided data wave. Quitting"
            abort
        endif

        variable endSearch = peak_time - searchInt

        findlevel/Q/R=(peak_time, endSearch) thisWave, crossVal

        if(V_flag == 0) // found a crossing
            crossingTime = V_levelX
            intToPeak = peak_time - crossingTime
        else
            crossingTime = NaN
            intToPeak = NaN
        endif

        return [crossingTime, intToPeak]
    end

    ///////////
    /// calcFallPeakCrossing
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-12-15
    /// NOTES: This function looks for the time of the level crossing (crossVal)
    /// for the falling phase of an event
    /// It will start the search at the peak_time and look forwards the given searchInt
    /// The function returns the crossingTime (x-value where crossing occurred, if found)
    /// and the absolute interval of time from the crossingTime to the peak_time
    ///////////
    function [variable crossingTime, variable intToPeak] calcFallPeakCrossing(wave thisWave, variable crossVal, variable peak_time, variable searchInt)
        if(!WaveExists(thisWave))
            print "calcFallPeakCrossing: cannot find provided data wave. Quitting"
            abort
        endif

        variable endSearch = peak_time + searchInt

        findlevel/Q/R=(peak_time, endSearch) thisWave, crossVal

        if(V_flag == 0) // found a crossing
            crossingTime = V_levelX
            intToPeak = crossingTime - peak_time
        else
            crossingTime = NaN
            intToPeak = NaN
        endif

        return [crossingTime, intToPeak]
    end

///////////
/// calcDeflectionArea
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Returns the area for a wave from the start time to the end time, adjusted for baseline
///////////
function calcDeflectionArea(wave thisWave, variable startTime, variable endTime, variable baseline)
    if(!WaveExists(thisWave))
        print "calcDeflection: cannot find provided data wave. Quitting"
        abort
    endif

    if(numType(startTime) == 2 || numType(endTime) == 2 || numtype(baseline) == 2) // if either start or end time or basline is NaN
        return NaN
    endif

    variable dupBuffer = 0.02 // add a 20ms buffer on either side of the start/end when duplicating
    // because start/End may not correspond exactly to one of the points that was sampled
    // leads to differences in the estimated error, depending on whether or not the area function
    // can "see" the points on either side of the start/end time points

    // Duplicate just the portion from which area will be taken. Saves time
    duplicate/O/R=(startTime - dupBuffer, endTime + dupBuffer) thisWave, tempBaseWave2
    tempBaseWave2 -= baseline
    variable this_area = area(tempBaseWave2, startTime, endTime)

    KillWaves/Z tempBaseWave
    return this_area
end

///////////
/// checkAreaPassesThreshold
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Returns the area for a wave from the start time to the end time, adjusted for baseline
///////////
function checkAreaPassesThreshold(variable this_area, variable area_thresh, variable thisSign)
    variable passesArea

    variable areaThreshIsZero = area_thresh == 0

    if(areaThreshIsZero)
        // if the threshold is zero, than everything should pass
        passesArea = 1
        return passesArea
    endif

    variable areaSignMatches = (sign(this_area) == thisSign)
    variable areaGreaterThresh = (thisSign * this_area) > area_thresh

    if(areaSignMatches && areaGreaterThresh)
        passesArea = 1
    endif

    return passesArea
end


///////////
/// displayDerivProcessing
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Within the analysis windows, display the processing of the event
///////////
function displayDerivProcessing(Wave rawWave, wave smoothedW, wave deriv[, variable showSmoothed])
    if(!WaveExists(rawWave) || !WaveExists(smoothedW) || !WaveExists(deriv))
        print "displayDerivProcessing: cannot find provided data waves. Quitting"
        abort
    endif

    if(paramIsDefault(showSmoothed))
        showSmoothed = 1
    endif
    

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    Struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    StructFill/SDFR = varsDFR dds // to use NVARs

    variable startTime = dds.dPeak_time - ps.traceOffset_ms
    // variable endTime = dds.dPeak_time + ps.traceDuration_ms - ps.traceOffset_ms
    variable endTime = dds.fall10_time+ps.traceOffset_ms

    setActiveAnalysisWindowSelect(1, 1) // set the first analysis window and clear
    
    duplicate/O rawWave, gui_baseline
    gui_baseline = dds.baseline
    duplicate/O rawWave, gui_thresh
    gui_thresh = ps.peakThreshold_pA * ps.peakSign + dds.baseline

    AppendToGraph rawWave
    SetAxis bottom, startTime, endTime
    SetAxis/A=2 left
    ModifyGraph rgb=(21845,21845,21845) //(0,0,0)
    AppendToGraph gui_baseline, gui_thresh
    ModifyGraph rgb(gui_baseline) = (0, 0, 60000), rgb(gui_thresh) = (0, 60000, 0)

    if(showSmoothed)
        AppendToGraph smoothedW
        ModifyGraph rgb($(nameOfWave(smoothedW)))=(1,16019,65535)
    endif

    make/O/N=1 plotPeak = {dds.peak}
    make/O/N=1 plotP50 = {dds.p50}
    make/O/N=1 plotP10 = {dds.p10}
    make/O/N=1 plotP90 = {dds.p90}

    make/O/N=1 plotPeak_time = {dds.peak_time}
    make/O/N=1 plotT50_time = {dds.t50rise_time}
    make/O/N=1 plotrise50_time = {dds.rise50_time}
    make/O/N=1 plotfall50_time = {dds.fall50_time}
    make/O/N=1 plotfall10_time = {dds.fall10_time}
    make/O/N=1 plotfall90_time = {dds.fall90_time}

    AppendToGraph plotPeak/TN=peak vs plotPeak_time 
    AppendToGraph plotP50/TN=rise50 vs plotrise50_time 
    AppendToGraph plotP50/TN=fall50 vs plotfall50_time 
    AppendToGraph plotP50/TN=t50 vs plotT50_time 
    AppendToGraph plotP10/TN=fall10 vs plotfall10_time 
    AppendToGraph plotP90/TN=fall90 vs plotfall90_time 

    string traces = "peak;t50;rise50;fall50;fall10;fall90"
    variable iTrace, nTraces = itemsinlist(traces)
    for(iTrace=0; iTrace<nTraces; iTrace++)
        string traceN = StringFromList(iTrace, traces)
        makeDotMarkers(traceN)
    endfor

    makeColorLavender("t50")
    makeColorOrange("fall90")
    makeColorOrange("fall10")
    makeColorGreen("rise50")
    makeColorGreen("fall50")

    setActiveAnalysisWindowSelect(2, 1) // set the first analysis window and clear
    
    duplicate/O rawWave, gui_dthresh
    gui_dthresh = ps.dThreshold_pA_ms * ps.peakSign

    AppendToGraph deriv
    SetAxis bottom, startTime, endTime
    SetAxis/A=2 left
    ModifyGraph rgb=(0,0,0)
    AppendToGraph gui_dthresh

    make/O/N=1 plotDPeak = {dds.dPeak}
    
    make/O/N=1 plotDPeak_time = {dds.dPeak_time}

    AppendToGraph plotDPeak/TN=dPeak vs plotDPeak_time 

    traces = "dpeak;"
    nTraces = itemsinlist(traces)
    for(iTrace=0; iTrace<nTraces; iTrace++)
        traceN = StringFromList(iTrace, traces)
        makeDotMarkers(traceN)
    endfor
end


///////////
/// comparePrevDetectDisplay
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Within the analysis windows, display the processing of the event
///////////
function comparePrevDetectDisplay(Wave rawWave, wave smoothedW, wave deriv[, variable showSmoothed])
    if(!WaveExists(rawWave) || !WaveExists(smoothedW) || !WaveExists(deriv))
        print "comparePrevDetectDisplay: cannot find provided data waves. Quitting"
        abort
    endif

    if(paramIsDefault(showSmoothed))
        showSmoothed = 1
    endif

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    Struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    StructFill/SDFR = varsDFR dds // to use NVARs

    variable startTime = dds.dPeak_time - ps.traceOffset_ms
    variable endTime = dds.dPeak_time + ps.traceOffset_ms

    string analysisWin = setActiveAnalysisWindowSelect(1, 1) // set the first analysis window and clear
    
    AppendToGraph/W=$analysisWin rawWave
    SetAxis/W=$analysisWin bottom, startTime, endTime
    SetAxis/A=2/W=$analysisWin left
    ModifyGraph/W=$analysisWin rgb=(21845,21845,21845) //(0,0,0)
   
    if(showSmoothed)
        AppendToGraph/W=$analysisWin smoothedW
        ModifyGraph/W=$analysisWin rgb($(nameOfWave(smoothedW)))=(1,16019,65535)
    endif

    make/O/N=1 plotPeak = {dds.peak}
    make/O/N=1 plotPeak_time = {dds.peak_time}

    string fullPeaksWN = nameOfWave(rawWave)+returnExt("absolute peak")
    AppendToGraph/W=$analysisWin $(fullPeaksWN) vs $(nameOfWave(rawWave)+returnExt("peak time"))
    
    AppendToGraph/W=$analysisWin plotPeak/TN=peak vs plotPeak_time

    string traces = "peak;"
    traces+= fullPeaksWN + ";"
    variable iTrace, nTraces = itemsinlist(traces)
    for(iTrace=0; iTrace<nTraces; iTrace++)
        string traceN = StringFromList(iTrace, traces)
        makeDotMarkers(traceN, displayName = analysisWin)
    endfor

    makeColorLavender(fullPeaksWN, displayName = analysisWin)

    analysisWin = setActiveAnalysisWindowSelect(2, 1) // set the first analysis window and clear
    
    AppendToGraph/W=$analysisWin deriv
    SetAxis/W=$analysisWin bottom, startTime, endTime
    SetAxis/A=2/W=$analysisWin left
    ModifyGraph/W=$analysisWin rgb=(0,0,0)

    make/O/N=1 plotDPeak = {dds.dPeak}
    make/O/N=1 plotDPeak_time = {dds.dPeak_time}
    
    string fullDPeaksWN = nameOfWave(rawWave) + returnExt("derivative")
    AppendToGraph/W=$analysisWin $(fullDPeaksWN) vs $(nameOfWave(rawWave)+returnExt("derivative time"))
    AppendToGraph/W=$analysisWin plotDPeak/TN=dPeak vs plotDPeak_time 

    traces = "dpeak;"
    traces += fullDPeaksWN + ";"
    nTraces = itemsinlist(traces)
    for(iTrace=0; iTrace<nTraces; iTrace++)
        traceN = StringFromList(iTrace, traces)
        makeDotMarkers(traceN, displayName = analysisWin)
    endfor

    makeColorLavender(fullDPeaksWN, displayName = analysisWin)

    addPrevDetectVarsToDisplay()
end

///////////
/// addPrevDetectVarsToDisplay
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-15
/// NOTES: Within the analysis windows, display the processing of the event
///////////
function addPrevDetectVarsToDisplay()
    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    Struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getPrevDerivDetectVarsDF()
    StructFill/SDFR = varsDFR dds // to use NVARs

    setActiveAnalysisWindowSelect(1, 0) // set the first analysis window and DON'T clear
    
    make/O/N=1 plotPeak_prev = {dds.peak}
    // make/O/N=1 plotP50_prev = {dds.p50}
    // make/O/N=1 plotP10_prev = {dds.p10}
    // make/O/N=1 plotP90_prev = {dds.p90}

    make/O/N=1 plotPeak_time_prev = {dds.peak_time}
    // make/O/N=1 plotT50_time_prev = {dds.t50rise_time}
    // make/O/N=1 plotrise50_time_prev = {dds.rise50_time}
    // make/O/N=1 plotfall50_time_prev = {dds.fall50_time}
    // make/O/N=1 plotfall10_time_prev = {dds.fall10_time}
    // make/O/N=1 plotfall90_time_prev = {dds.fall90_time}

    AppendToGraph plotPeak_prev/TN=peak_prev vs plotPeak_time_prev 
    // AppendToGraph plotP50_prev/TN=rise50_prev vs plotrise50_time_prev 
    // AppendToGraph plotP50_prev/TN=fall50_prev vs plotfall50_time_prev 
    // AppendToGraph plotP50_prev/TN=t50_prev vs plotT50_time_prev 
    // AppendToGraph plotP10_prev/TN=fall10_prev vs plotfall10_time_prev 
    // AppendToGraph plotP90_prev/TN=fall90_prev vs plotfall90_time_prev 

    // string traces = "peak_prev;t50_prev;rise50_prev;fall50_prev;fall10_prev;fall90_prev;"
    // variable iTrace, nTraces = itemsinlist(traces)
    // for(iTrace=0; iTrace<nTraces; iTrace++)
    //     string traceN = StringFromList(iTrace, traces)
    //     makeDotMarkers(traceN)
    // endfor

    makeDotMarkers("peak_prev")
    makeColorGrey("peak_prev")

    // makeColorLavender("t50")
    // makeColorOrange("fall90")
    // makeColorOrange("fall10")
    // makeColorGreen("rise50")
    // makeColorGreen("fall50")

    setActiveAnalysisWindowSelect(2, 0) // set the first analysis window and don't clear

    make/O/N=1 plotDPeak_prev = {dds.dPeak}
    
    make/O/N=1 plotDPeak_time_prev = {dds.dPeak_time}

    AppendToGraph plotDPeak_prev/TN=dPeak_prev vs plotDPeak_time_prev
    makeDotMarkers("dpeak_prev")
    makeColorGrey("dpeak_prev")
end