function DetectButtonProc_AGG(B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    DFREF panelDFR = getEventsPanelDF()
    DFREF evDetectDFR = getEvDetectDF()

	if (B_Struct.eventcode == 2)
        // print "userdata", B_Struct.userdata
		string whichDF = stringbykey("df", B_Struct.userdata)
		string detectListWN = stringbykey("detect", B_Struct.userdata)
        DFREF dfr
        strswitch (whichDF)
            case "panel":
                dfr = getEventsPanelDF()
                break
            case "evDetect":
                dfr = getEvDetectDF()
                break
            case "root":
                dfr = root:
                break
            default:
                dfr = getEventsPanelDF()
        endswitch

        // print detectListWN

        Wave/SDFR=dfr /Z /T detectW = $detectListWN

		variable iwave=0, nwaves=DimSize(detectW, 0 )
    
        string waven =detectW[iwave]
        //print waven,waveexists($removequotes(waven))
        do

            // Run the detection procedure
            derivdetectp2(waven)
            // derivDetectP2_AGG(waven)

            // Get the analysis parameters
            struct analysisparameters ps
            variable worked = readpanelparams2(ps)

            // Store detection information for this cell/series
            updateCellDetectionInformation(waven, dfr, ps)

            iwave+=1
            waven = detectW[iwave]
            // print waven,waveexists($removequotes(waven))

        while((iwave<nwaves)&&waveexists($removequotes(waven)))

        // AGG 2023-12-10 comment: removing the refreshIntervals, as this updates ALL PTB interval
        // waves and replaces with forward interval waves, which has 1 less interval than the number of events
        // // refreshing intervals to reflect continuity with vary burst window and shuffle
        // refreshIntervals() // 20170911 :: first event had no interval and was equal to zero (!)
        // //now event 0 has interval between event 0 and event 1
        // end AGG 2023-12-10 comment
        
	endif
end

// Add the analysis parameters to global variables for the cell/series
// that was detected
// Also add the series name to the list of detected waves
function updateCellDetectionInformation(waven, dfr, analysisParams)
    string waven
    DFREF dfr
    STRUCT analysisParameters &analysisParams
    
    Wave/SDFR = dfr detectedWave = $waven

    string cellName, seriesName
    [cellName, seriesName] = getCellandSeriesFromWaveNote(detectedWave)

    if(strlen(cellName)>0)  
        // print "cell name", cellName
    else
        print "no cell name", cellName
    endif

    if(strlen(cellName)>0 && strlen(seriesName)>0)
        DFREF seriesDFR = getSeriesDF(cellName, seriesName)
        if(DataFolderRefStatus(seriesDFR)==0)
            createSeriesDF(cellName, seriesName)
            seriesDFR = getSeriesDF(cellName, seriesName)
        endif

        String/G seriesDFR:detectParams

        // Save the structure information
        StructPut /S analysisParams, seriesDFR:detectParams

        // Set the confirmed detection parameter to 0
        variable/G seriesDFR:confirmedDetect = 0
    else
        print "could not find cell and/or series name to save detection params"
    endif

    // Add wave to detectedWaves
    DFREF evDetectDFR = getEvDetectDF()
    Wave/T/SDFR = evDetectDFR detectedWaves
    if(!WaveExists(detectedWaves))
        make/T/O/N=(1) evDetectDFR:detectedWaves /Wave = detectedWaves
        detectedWaves[0] = waven
    else
        if(!isStringInWave(waven, detectedWaves))
            variable numDetectedWaves = numpnts(detectedWaves)
            redimension/N=(numDetectedWaves + 1) detectedWaves
            detectedWaves[numDetectedWaves] = waven
        endif
    endif
end