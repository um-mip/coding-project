// Run when a user interacts with the cell list wave
// Update the list wave for series based on the selected cell
function updateDetectSeriesListProc(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        string selectedCell = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)
        updateSeriesWave_detect(selectedCell) // allow to update regardless of length; make blank
    endif
    return 0
end

function updateSeriesWave_detect (cellName)
    String cellName
    variable makeBlank = 0

    DFREF panelDFR = getEventsPanelDF()
    DFREF evDetectDFR = getEvDetectDF()
    if(strlen(cellName)>0)
        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)

        Wave cellEvents_series = cellInfoDFR:Events_series

        if(WaveExists(cellEvents_series))

            Duplicate/O cellEvents_series, evDetectDFR:selCellSeries

            Wave selSeries = evDetectDFR:selSeries

            redimension/N=(numpnts(cellEvents_series)) selSeries
            selSeries = 0
        else
            makeBlank = 1
        endif
    else 
        makeBlank = 1
    endif

    if(makeBlank)
        Wave selCellSeries = evDetectDFR:selCellSeries
        redimension/N=0 selCellSeries

        Wave selSeries = evDetectDFR:selSeries
        redimension/N=0 selSeries
        // selSeries = 0
    endif
end

// Procedure for the "Add to Detect Waves" button
// Add the series name for the selected cell/series combo to the detect listbox
function addCellSeriesToDetectProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        // print "add to detect press"
        addSelectedSeriesToDetect()
    endif
end

// Get the information about the selected cell and the selected series (potentially multiple)
// In the detect tab to then add to the detection listbox
function addSelectedSeriesToDetect()
    DFREF evDetectDFR = getEvDetectDF()

    WAVE/T/Z seriesNums = evDetectDFR:selCellSeries
    WAVE/B/Z selSeries = evDetectDFR:selSeries
    // print selSeries

    variable numSeries = numpnts(seriesNums), iSeries = 0
    string thisCell, thisSeries
    thisCell = getSelectedItem("cellListBoxDetect", hostName = "AGG_events")
    if(strlen(thisCell)>0)
        for(iSeries = 0; iSeries<numSeries; iSeries++)
            if(selSeries[iSeries]==1)
                thisSeries=seriesNums[iSeries]
                if(strlen(thisSeries)>0)
                    // print "trying to add", thisCell, thisSeries
                    addCellSeriesToDetect(thisCell, str2num(thisSeries))
                else
                    // print "thisSeries", thisSeries, "is having issues"
                endif
            endif
        endfor
    else
        // ADD in safety
    endif
end

// TO-DO: AGG 2022-03-08: add ability to select multiple cells and then add
// all of their series to the detect wave box

// Get the information about the selected cell and the selected series (potentially multiple)
// In the detect tab to then add to the detection listbox
function addSelectedCellsSeriesToDetect()
    DFREF panelDFR = getEventsPanelDF()
    DFREF evDetectDFR = getEvDetectDF()

    // WAVE/T/Z cellNames = panelDFR:cellName, seriesNums = evDetectDFR:selCellSeries
    
    // ControlInfo /W=AGG_Events cellListBoxDetect
    // variable selCellRow = V_value
    // // print V_value, "selected row"

    // WAVE/B/Z selCell = evDetectDFR:cellSelWave, selSeries = evDetectDFR:selSeries
    // // print selSeries

    // variable numCells = numpnts(cellNames), numSeries = numpnts(seriesNums), iSeries = 0
    // string thisCell, thisSeries
    // if(selCellRow < numCells)
    //     thisCell = cellNames[selCellRow]
    //     // print "selected", thisCell
    //     for(iSeries = 0; iSeries<numSeries; iSeries++)
    //         if(selSeries[iSeries]==1)
    //             thisSeries=seriesNums[iSeries]
    //             if(strlen(thisSeries)>0)
    //                 // print "trying to add", thisCell, thisSeries
    //                 addCellSeriesToDetect(thisCell, str2num(thisSeries))
    //             else
    //                 print "thisSeries", thisSeries, "is having issues"
    //             endif
    //         endif
    //     endfor
    // else
    //     // ADD in safety
    // endif
end

function addCellSeriesToDetect(cellName, seriesName)
    string cellName
    variable seriesName

    string seriesTraceName = buildSeriesWaveName(cellName, seriesName)

    DFREF evDetectDFR = getEvDetectDF()
    DFREF panelDFR = getEventsPanelDF()

    DFREF rootRef = root:

    // Wave/T/SDFR= evDetectDFR detectWaves
    Wave/T/SDFR= rootRef detectWaves = ImportListWave
    
    variable numWavesForDetect = numpnts(detectWaves)

    variable matchFlag = 0

    if(numWavesForDetect > 0)
        variable row = numWavesForDetect - 1
        do // work from the end, b/c deleting point changes the index
            string thisName = detectWaves[row]
            if(strlen(thisName) > 0)
                if(StringMatch(thisName, seriesTraceName))
                    matchFlag = 1
                endif
            else
                // delete any empty cells of detect wave
                DeletePoints row,1, detectWaves
            endif
            row = row - 1
        while( row >= 0 )
    endif

    variable newNumWaves = numpnts(detectWaves)
    // print newNumWaves, "detect waves"

    Wave/B/U/SDFR = rootRef detectWavesSel = ImportSelWave

    variable seriesExists = 0
    print "seriesTrace", seriesTraceName
    if(WaveExists($seriesTraceName))
        seriesExists = 1
        // Add the cell name and series name to wave note
        addCellAndSeriesToWaveNote($seriesTraceName, cellName, seriesName)
        // print "added cell/series to wavenote"
    else
        print "warning: did not add cell/series to wave note when adding to detect list"
    endif

    // Add wave to detectedWaves
    Wave/T/SDFR = evDetectDFR detectedWaves
    variable ignoreDuplicate = 0
    if(WaveExists(detectedWaves))
        if(isStringInWave(seriesTraceName, detectedWaves))
            string addDuplicate
            Prompt addDuplicate, "Wave already detected. Do you want to detect again?: ", popup, "no;yes;"
            DoPrompt "Detected Wave", addDuplicate
            if(V_flag)
                ignoreDuplicate = 1
            endif
            if(StringMatch(addDuplicate, "no"))
                ignoreDuplicate = 1
            endif
        endif
    endif

    if(matchFlag == 0 && seriesExists == 1 && ignoreDuplicate == 0) // no match, and series exists, and not ignoring duplicate
        // print "adding wave"
        redimension/N=(newNumWaves+1) detectWaves
        redimension/N=(newNumWaves+1, 1, 2) detectWavesSel
        detectWaves[newNumWaves] = seriesTraceName
    else
        print "not adding wave", "matchFlag", matchFlag, "seriesExists", seriesExists 
        redimension/N=(newNumWaves) detectWaves
        redimension/N=(newNumWaves, 1, 2) detectWavesSel
    endif
end

// Remove the selected waves from the detection list box
function AGG_RemoveSelWavesProc(B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

	if (B_Struct.eventcode == 2)
        string swavename = stringbykey("selwave", B_Struct.userdata)
        string listwavename = stringbykey("listwave", B_Struct.userdata)
        string whichDF_sel = stringbykey("dfSelWave", B_Struct.userdata)
        string whichDF_del = stringbykey("dfDelWave", B_Struct.userdata)
        variable clearAnal = str2num(stringbykey("clearAnal", B_Struct.userdata))
        DFREF dfr_sel
        strswitch (whichDF_sel)
            case "panel":
                dfr_sel = getVBWPanelDF()
                break
            case "sctw":
                dfr_sel = getSCTWavesDF()
                break
            case "eventsPanel":
                dfr_sel = getEventsPanelDF()
                break
            case "root":
                dfr_sel = root:
                break
            default:
                dfr_sel = getVBWPanelDF()
        endswitch

        DFREF dfr_del
        variable doSCTupdate = 0
        strswitch (whichDF_del)
            case "panel":
                dfr_del = getVBWPanelDF()
                break
            case "sctw":
                dfr_del = getSCTWavesDF()
                doSCTupdate = 1
                break
            case "eventsPanel":
                dfr_sel = getEventsPanelDF()
                break
            case "root":
                dfr_sel = root:
                break
            default:
                dfr_del = getVBWPanelDF()
        endswitch

        WAVE /B /Z swave = dfr_sel:$swavename
        WAVE /T /Z listwave = dfr_sel:$listwavename

        duplicate/O/B sWave, oneDSelWave
        redimension/N=(-1,0) oneDSelWave // Sepecific for the ImportSelWave because it's multidimensional

        Extract/O/B oneDSelWave, swave, oneDSelWave !=1
        redimension/N=(numpnts(swave), 1, 2) swave
        Extract/O/T listwave, listWave, oneDSelWave[p] !=1
        KillWaves/Z unSel_selWave, unSel_listWave, oneDSelWave

        if(clearAnal == 1)
            print "removing analyses"
            removeAllTracesNOKILL(B_Struct.win+"#AnalysisGraph1")
            removeAllTracesNOKILL(B_Struct.win+"#AnalysisGraph2")
        endif

    endif
end