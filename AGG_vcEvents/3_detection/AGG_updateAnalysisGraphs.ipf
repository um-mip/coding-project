// When the user clicks on a wave in the detection listbox
// Update the analysis graphs
function ImportFileListboxProc_AGG(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    string ctrlName = LB_Struct.ctrlName
    string mypanelName = LB_Struct.win

    variable row = LB_Struct.row // row if click in interior, -1 if click in title
    variable col = LB_Struct.col // column number

    variable event = LB_Struct.eventCode
    Wave/T ListFromListBox = LB_Struct.listWave
    Wave selWave = LB_Struct.selWave

	NVAR gxmin1 = root:gxmin1
	NVAR gxmax1 = root:gxmax1
	struct analysisparameters ps

	variable worked = readpanelparams2(ps) // AGG - TO-DO: Save detection parameters specific for each cell, refer back to these if exist, rather than panel every time
    //	print "importfilelistboxproc2: event", event 

    string graph1Path = mypanelName + "#AnalysisGraph1"
    string graph2Path = mypanelName + "#AnalysisGraph2"

    // mouse click in list box
	if ((event==4)) //||(event==5))
        string SelectedWaveName=ListFromListBox[row]
        if(strlen(SelectedWaveName)>0)
		
            // print "user click in imported list box", selectedWaveName
            // remove waves from plots

            removeAllTracesNOKILL(graph1Path)
            removeAllTracesNOKILL(graph2Path)

            updateAnalysisDisplayColors(graph1Path, 1)
            updateAnalysisDisplayColors(graph2Path, 2)

            Wave selectedWave = $selectedWaveName
            if(WaveExists(selectedWave))
                updateAnalysisWithParams(myPanelName, ps, selectedWave, gxmin1, gxmax1)
            endif
            SetActiveSubWindow $mypanelName
        endif
    endif
return 0            // other return values reserved	
end

///////////
/// updateDetectDisplayProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES:
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-06-14
/// NOTES: Add 6 event code for dependency update. Also change so that just updating scale and thresh lines, not replotting
/// too visually buggy
///////////
function updateDetectDisplayProc(SV_Struct) :SetVariableControl
    STRUCT WMSetVariableAction & SV_Struct
    if(SV_Struct.eventcode == 8 || SV_Struct.eventcode == 6)
        // debugger
        changeDetectAnalysisGraphXScales(SV_Struct.win)
    endif
    return 0
end

///////////
/// updateDetectDisplay
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: 
///////////
function updateDetectDisplay([string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_events"
    endif

    string graph1Path = panelName + "#AnalysisGraph1"
    string graph2Path = panelName + "#AnalysisGraph2"

    removeAllTracesNOKILL(graph1Path)
    removeAllTracesNOKILL(graph2Path)

    updateAnalysisDisplayColors(graph1Path, 1)
    updateAnalysisDisplayColors(graph2Path, 2)
    
    string selSeries = getFirstSelectedDetectWave()

    wave seriesWave = root:$selSeries

    if(!WaveExists(seriesWave))
        return NaN
    endif

    NVAR gxmin1 = root:gxmin1
	NVAR gxmax1 = root:gxmax1
	struct analysisparameters ps

	variable worked = readpanelparams2(ps) // AGG - TO-DO: Save detection parameters specific for each cell, refer back to these if exist, rather than panel every time
    //	print "importfilelistboxproc2: event", event 

    updateAnalysisWithParams(panelName, ps, seriesWave, gxmin1, gxmax1)
    SetActiveSubWindow $panelName
end


///////////
/// getFirstSelectedDetectWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: Returns the first selected detect wave in the list box on the detect & analyze page of burst panel
///////////
function/S getFirstSelectedDetectWave()
    DFREF panelDFR = getEventsPanelDF()

    Wave/T ImportListWave
    Wave ImportSelWave

    if(!WaveExists(ImportListWave) || !WaveExists(ImportSelWave))
        return ""
    endif

    FindValue/V=1 ImportSelWave

    if(V_value == -1)
        return ""
    endif

    string selSeries = ImportListWave[V_value]
    return selSeries
end


function updateAnalysisDisplayColors(fullWinName, analysisType)
    string fullWinName
    variable analysisType

    variable r=0,g=0,b=0
    if(analysisType == 1)
        r = 48896
        g = 59904
        b = 65280
    else 
        if(analysisType == 2)
            r = 65280
            g = 65280
            b = 48896
        endif
    endif

    ModifyGraph/W=$fullWinName wbRGB=((r),(g),(b)),gbRGB=((r),(g),(b)), rgb=(0,0,0)
end

function updateAnalysisWave(fullWinName, analysisType)
    string fullWinName
    variable analysisType

    variable r=0,g=0,b=0
    if(analysisType == 1)
        r = 48896
        g = 59904
        b = 65280
    else 
        if(analysisType == 2)
            r = 65280
            g = 65280
            b = 48896
        endif
    endif

    ModifyGraph/W=$fullWinName wbRGB=((r),(g),(b)),gbRGB=((r),(g),(b)), rgb=(0,0,0)
end

function changeDetectAnalysisGraphXScales(panelName)
    string panelName

    string graph1Path = panelName + "#AnalysisGraph1"
    string graph2Path = panelName + "#AnalysisGraph2"

    NVAR gxmin1 = root:gxmin1
	NVAR gxmax1 = root:gxmax1

    if(numtype(gxmin1)!=0 || numtype(gxmax1)!=0)
        return NaN 
    endif

    string rawWaveN = getFirstSelectedDetectWave()
    Wave waveToAdd = $(rawWaveN)
    if(!WaveExists(waveToAdd))
        removeAllTracesNOKILL(graph1Path)
        removeAllTracesNOKILL(graph2Path)
        return NaN 
    endif

    struct analysisparameters ps
	variable worked = readpanelparams2(ps)

    // units from parameter window are pA/msec/mV
    // units from data files are A, V, sec--convert params to be convenient for user!!!
    // derivative params
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms		
    variable max_dur = ps.dMaxWidth_ms		
    variable chunk = ps.peakWindowSearch_ms	

    // baseline params
    variable base_dur = ps.baseDuration_ms	

    // peak parameters
    variable peak_sign = ps.peakSign					
    variable thresh = ps.peakThreshold_pA	

    //control parameters
    variable usetb = ps.usetb

    Wave gui_baseline = root:gui_baseline
    variable remakeBaseWave = 0
    if(!WaveExists(gui_baseline))
        remakeBaseWave = 1
    else
        CheckDisplayed/W=$graph1Path gui_baseline
        if(V_flag != 1)
            remakeBaseWave = 1
        endif
    endif

    variable mybase = mean(waveToAdd,(gxmin1),(gxmin1+base_dur))

    if(remakeBaseWave)
        variable myxmin = leftx(waveToAdd)
        variable myxmax = rightx(waveToAdd)
        addGUILineToGraph(graph1Path, mybase, "gui_baseline", myxmin, myxmax, 16385,16388,65535 )
    else
        gui_baseline = mybase
    endif

    Wave gui_threshold = root:gui_threshold
    variable remakeThreshWave = 0
    if(!WaveExists(gui_threshold))
        remakeThreshWave = 1
    else
        CheckDisplayed/W=$graph1Path gui_threshold
        if(V_flag != 1)
            remakeThreshWave = 1
        endif
    endif

    variable myThreshold = (peak_sign * thresh) + mybase

    if(remakeThreshWave)
        myxmin = leftx(waveToAdd)
        myxmax = rightx(waveToAdd)
        addGUILineToGraph(graph1Path, myThreshold, "gui_threshold", myxmin, myxmax, 16385,16388,65535 )
    else
        gui_threshold = myThreshold
    endif

	setAxis /W=$graph1Path bottom, gxmin1, gxmax1
    SetAxis /W=$graph1Path /A=2 left

    string deriv_wavename = testderivative(rawWaveN)
    Wave derivWave = $(deriv_wavename)

    if(!WaveExists(derivWave))
        removeAllTracesNOKILL(graph2Path)
        return NaN
    endif

    Wave deriv_threshold = root:deriv_threshold
    variable remakeDerivThresh = 0
    if(!WaveExists(deriv_threshold))
        remakeDerivThresh = 1
    else
        CheckDisplayed/W=$graph2Path deriv_threshold
        if(V_flag != 1)
            remakeDerivThresh = 1
        endif
    endif

    variable derivVal = peak_sign * dthresh

    if(remakeDerivThresh)
        myxmin = leftx(waveToAdd)
        myxmax = rightx(waveToAdd)
        addGUILineToGraph(graph2Path, derivVal, "deriv_threshold", myxmin, myxmax, 16385,16388,65535 )
        setScale d 0,0,"A/msec", deriv_threshold
    else
        deriv_threshold = derivVal
    endif

    setAxis /W=$graph2Path bottom, gxmin1, gxmax1
    SetAxis /W=$graph2Path /A=2 left
end

function updateAnalysisWithParams(panelName, ps, waveToAdd, gxmin1, gxmax1)
    string panelName
    STRUCT analysisParameters &ps
    Wave waveToAdd
    variable gxmin1, gxmax1

    string thisWaveName = nameOfWave(waveToAdd)

    // units from parameter window are pA/msec/mV
    // units from data files are A, V, sec--convert params to be conventent for user!!!
    // derivative params
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms		
    variable max_dur = ps.dMaxWidth_ms		
    variable chunk = ps.peakWindowSearch_ms	

    // baseline params
    variable base_dur = ps.baseDuration_ms	

    // peak parameters
    variable peak_sign = ps.peakSign					
    variable thresh = ps.peakThreshold_pA	

    //control parameters
    variable usetb = ps.usetb

    string graphName = panelName + "#AnalysisGraph1"

    addAnalysisWaveToGraph(graphName, ps, waveToAdd)

    variable myxmin=0,myxmax=0
	string timebase=""
	if(usetb==0)
		myxmin=leftx(waveToAdd)
		myxmax=rightx(waveToAdd)
	else
		timebase = thisWaveName+"_tb"
		if(waveexists($timebase))
			WAVE tb = $timebase
			wavestats/Z/Q tb
			myxmin=V_min
			myxmax=V_max
		else
			print "failed to locate time base:",thisWaveName, timebase
			abort
		endif
	endif
	variable mybase = mean(waveToAdd,(gxmin1),(gxmin1+base_dur))
    addGUILineToGraph(graphName, mybase, "gui_baseline", myxmin, myxmax, 16385,16388,65535 )
	
    variable myThreshold = (peak_sign * thresh) + mybase
    addGUILineToGraph(graphName, myThreshold, "gui_threshold", myxmin, myxmax, 3,52428,1 )

	setAxis /W=$graphName bottom, gxmin1, gxmax1
    SetAxis /W=$graphName /A left

    string deriv_wavename=""
	deriv_wavename = testderivative(thisWaveName)

    Wave derivWave = $(deriv_wavename)

    graphName = panelName + "#AnalysisGraph2"
    if(WaveExists(derivWave))
        addAnalysisWaveToGraph(graphName, ps, derivWave)
        variable derivVal = peak_sign * dthresh
        addGUILineToGraph(graphName, derivVal, "deriv_threshold", myxmin, myxmax, 65535,0,0 )
        setScale d 0,0,"A/msec", deriv_threshold
        setAxis /W=$graphName bottom, gxmin1, gxmax1
        SetAxis /W=$graphName /A left
    else
        print "Derivative wave does not exist"
    endif
    // Align proc in original (alignplotsproc)
end

function addGUILineToGraph(graphName, lineVal, lineName, xmin, xmax, r, g, b)
    string graphName
    variable lineVal
    string lineName
    variable xmin, xmax, r, g, b

    make /O/N=2 $lineName /Wave = thisLine
	setscale/I x, xmin, xmax, thisLine
	thisLine = lineVal

    variable alreadyPlotted = 0

    CheckDisplayed /W=$graphName thisLine
    if(V_flag == 1)
        alreadyPlotted = 1
    endif

	if(alreadyPlotted == 0)
		appendtograph/W=$graphName thisLine
	else
        // print "already on graph"
	endif

    // baseline color
    // print "r, g, b", r, g, b
    ModifyGraph/W=$graphName rgb($lineName)=(r, g, b)
end

function addAnalysisWaveToGraph(graphName, ps, waveToAdd)
    string graphName
    STRUCT analysisParameters &ps
    Wave waveToAdd

    string thisWaveName = nameOfWave(waveToAdd)

    variable alreadyPlotted = 0

    CheckDisplayed /W=$graphName waveToAdd
    if(V_flag == 1)
        alreadyPlotted = 1
    endif

    variable xmin,xmax,ymin,ymax
    variable usetb = ps.usetb

    // print "Already plotted, add analysis", alreadyPlotted
    // print tracenamelist(graphName, ";", 1)
    if(alreadyPlotted == 0)
        if(usetb==0)
            appendtograph/W=$graphName waveToAdd
            modifygraph/W=$graphName rgb=(0,0,0)					
        else
            string timebase=thisWaveName+"_tb" // may want to add specifics about path
            if(waveexists($timebase))
                appendtograph/W=$graphName waveToAdd vs $timebase
                modifygraph/W=$graphName rgb=(0,0,0)					
            else
                timebase = timebase[1,strlen(timebase)]
                if(waveexists($timebase))
                    appendtograph/W=$graphName waveToAdd vs $timebase
                    modifygraph/W=$graphName rgb=(0,0,0)					
                else
                    print "failed to find timebase:",thisWaveName,timebase
                endif
            endif
        endif	
        doupdate
    endif
end

// Run when the analysis window buttons are pressed; However, these are currently not displayed on AGG_events panel
function analWin_AGG(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    string ctrlname = B_Struct.ctrlName 
    string panelName = B_Struct.win

    if(B_Struct.eventCode == 2)
        variable AGG_eventsFlag = 0
        if(StringMatch(panelname, "AGG_Events"))
            AGG_eventsFlag = 1
        endif

        string graph1name="AnalysisGraph1",graph2name="AnalysisGraph2",graphname=""
        variable analysisType = 0
        variable x=0,y=0,u=0,w=0
        variable x2=78.75,y2=280.25,u2=473.25,w2=488.75
        variable x1=78.75,y1=46.25,u1=473.25,w1=254.75
        //print "analysis window: ",ctrlname
        if(stringmatch(ctrlname,"analysisWin1butt"))
            graphname=graph1name
            analysisType = 1
            x=x1
            y=y1
            u=u1
            w=w1
        else
            graphname=graph2name
            analysisType = 2
            x=x2
            y=y2
            u=u2
            w=w2
        endif
        string mywinlist = ""
        variable which
        if(AGG_eventsFlag == 0)
            mywinlist=winlist("*",";","WIN:1") // will only find standalone graph
            which=whichlistitem(graphname,mywinlist)
            //print graphname,mywinlist, which
            if(which!=-1)
                //graph exists, bring to the top
                doWindow /F $(graphname)
            else
                //graph doesn't exist, create
                // print "making new window",which
                Display /K=1/W=((x),(y),(u),(w))
                string thiswindowsname=WinName(0,1)
                DoWindow/T $(thiswindowsname), graphname
                RenameWindow $(thiswindowsname), $(graphname)
                updateAnalysisDisplayColors(graphName, analysisType)
            endif
        else
            mywinlist = ChildWindowList(panelName)
            print "child list", myWinList
            which = whichlistitem(graphname, mywinlist)
            if(which!=-1)
                print "Analysis graph does exist", graphName
                string fullDisplay = panelName + "#" + graphname
                updateAnalysisDisplayColors(fullDisplay, analysisType)
            else
                print "Analysis graph doesn't exist", graphName
            endif
        endif
        //endif
    endif

end