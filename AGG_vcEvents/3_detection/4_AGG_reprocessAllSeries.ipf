///////////
/// reprocessDetectCellSeries
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-21
/// NOTES: Need to click on series in confirmation panel first to update the confirmation parameters
///////////
function reprocessDetectCellSeries(string cellName, variable seriesNum[, variable consistentDur])
    variable changeTraceDuration = 0
    if(!paramIsDefault(consistentDur))
        changeTraceDuration = 1
    endif
    
    string seriesN = buildSeriesWaveName(cellName, seriesNum)
    
    if(WaveExists($(seriesN + "_ptb")))
        if(changeTraceDuration)
            DFREF seriesDFR = getSeriesDF(cellName, num2str(seriesNum))
            String/G seriesDFR:detectParams/N=detectParams

            STRUCT analysisParameters paramStruct
            StructGet /S paramStruct, detectParams

            paramStruct.traceDuration_ms = consistentDur

            // Save the structure information
            StructPut /S paramStruct, seriesDFR:detectParams
        endif

        updateConfirmationParams(cellName, seriesNum, 1)

        changeDetectParamsFromConfirmParams()
        

        Wave rawWave = $(seriesN)
        if(WaveExists(rawWave))
            reprocessPrevDetectedEvents(rawWave)
            // AGG - this doesn't really belong here, but easiest structure to
            // take advantage of to update just the rise and fall time features
            // createRiseFallInfoWaves(rawWave)
        endif
        
    endif

end

///////////
/// reprocessDetectAllSeries
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: consistentDur should be entered in s, not ms
///////////
function reprocessDetectAllSeries([variable consistentDur])

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell]
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
            if(dfrStatus != 0)
                Wave/T/SDFR=cellInfoDFR Events_series
                if(WaveExists(Events_series))
                    variable numSeries = numpnts(Events_series)
                    variable iSeries = 0
                    for( iSeries = 0; iSeries < numSeries; iSeries++ )
                        string seriesName = Events_series[iSeries]
                        if(strlen(seriesName)>0)
                            variable seriesNum = str2num(seriesName)
                            print "Updating", cellName, "series", seriesNum
                            if(paramIsDefault(consistentDur))
                                reprocessDetectCellSeries(cellName, seriesNum)
                            else
                                reprocessDetectCellSeries(cellName, seriesNum, consistentDur = consistentDur)
                            endif
                            
                        endif
                    endfor
                endif
            endif
        endfor
    else
        print "reprocessDetectAllSeries: cellName wave doesn't exist"
    endif

    updateSeriesOutTable()
end

///////////
/// resetConfirmedDetection
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: For all series, reset confirmed detection to 0
///////////
function resetConfirmedDetection()

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName

    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell]
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
            if(dfrStatus != 0)
                Wave/T/SDFR=cellInfoDFR Events_series
                if(WaveExists(Events_series))
                    variable numSeries = numpnts(Events_series)
                    variable iSeries = 0
                    for( iSeries = 0; iSeries < numSeries; iSeries++ )
                        string seriesName = Events_series[iSeries]
                        if(strlen(seriesName)>0)
                            DFREF seriesDFR = getSeriesDF(cellName, seriesName)
                            // Set the confirmed detection parameter to 0
                            variable/G seriesDFR:confirmedDetect = 0
                        endif
                    endfor
                endif
            endif
        endfor
    else
        print "reprocessDetectAllSeries: cellName wave doesn't exist"
    endif

    updateSeriesOutTable()
end

///////////
/// concatenateDiffsWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: Concatenate all of the waves that end in "_diffs" to be able
/// to see what the differences from the updated detection are with all cells
///////////
function concatenateDiffsWaves()
    String diffsWaves = wavelist("*_diffs", ";", "")
    // print diffsWaves

    Concatenate/O/D/NP=0 diffsWaves, combinedDiffsWaves

    edit/K=1 combinedDiffsWaves
    modifyTable horizontalIndex=2 // show labels

    analyzeDetectDifferencesWave(combinedDiffsWaves)
end

///////////
/// concatenateAbsDiffWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: Concatenate all of the waves that end in "_diffs" to be able
/// to see what the differences from the updated detection are with all cells
///////////
function concatenateAbsDiffWaves()
    String diffsWaves = wavelist("*_diffsAbs", ";", "")
    // print diffsWaves

    Concatenate/O/D/NP=0 diffsWaves, combinedAbsDiffsWaves

    edit/K=1 combinedAbsDiffsWaves
    modifyTable horizontalIndex=2 // show labels
end

///////////
/// concatenatePrevValsWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: Concatenate all of the waves that end in "_diffs" to be able
/// to see what the differences from the updated detection are with all cells
///////////
function concatenatePrevValsWaves()
    String prevValsWaves = wavelist("*_prevVals", ";", "")
    // print prevValsWaves

    Concatenate/O/D/NP=0 prevValsWaves, combinedPrevValsWaves

    edit/K=1 combinedPrevValsWaves
    modifyTable horizontalIndex=2 // show labels
end


///////////
/// createT50rAbsDiffDistributionPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: 
///////////
function createT50rAbsDiffDistributionPlots()
    Wave combinedAbsDiffsWaves
    make/O/N=(dimSize(combinedAbsDiffsWaves, 0)) t50rAbsDiffs
    t50rAbsDiffs = combinedAbsDiffsWaves[p][%t50r]

    Extract/O t50rAbsdiffs, t50rAbsdiffs_pos, t50rAbsdiffs[p] > 0
    Extract/O t50rAbsdiffs, t50rAbsdiffs_neg, t50rAbsdiffs[p] < 0
    SetScale d, 0, 1, "s", t50rAbsDiffs, t50rAbsdiffs_pos, t50rAbsdiffs_neg

    probdistp("t50rAbsdiffs_pos", 1)
    probdistp("t50rAbsdiffs_neg", -1)

    Display/K=1/VERT t50rAbsdiffs_pos_dist
    Label/Z bottom, "t50r"
    Label/Z left, "proportion of events"

    Display/K=1/VERT t50rAbsdiffs_neg_dist
    Label/Z bottom, "t50r"
    Label/Z left, "proportion of events"

    Wave combinedPrevValsWaves
    make/O/N=(dimSize(combinedPrevValsWaves, 0)) t50rPrevVals
    t50rPrevVals = combinedPrevValsWaves[p][%t50r]
    SetScale d, 0, 1, "s", t50rPrevVals

    WaveStats/Q t50rPrevVals
    print "average previous t50rs", V_avg
end


///////////
/// createT50rDiffDistributionPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: 
///////////
function createT50rDiffDistributionPlots()
    Wave combinedDiffsWaves
    make/O/N=(dimSize(combinedDiffsWaves, 0)) t50rDiffs
    t50rDiffs = combinedDiffsWaves[p][%t50r] * 100

    Extract/O t50rdiffs, t50rdiffs_pos, t50rdiffs[p] > 0
    Extract/O t50rdiffs, t50rdiffs_neg, t50rdiffs[p] < 0
    SetScale d, 0, 1, "%", t50rDiffs, t50rdiffs_pos, t50rdiffs_neg

    probdistp("t50rdiffs_pos", 1)
    probdistp("t50rdiffs_neg", -1)

    Display/K=1/VERT t50rdiffs_pos_dist
    Label/Z bottom, "% change t50r"
    Label/Z left, "proportion of events"

    Display/K=1/VERT t50rdiffs_neg_dist
    Label/Z bottom, "% change t50r"
    Label/Z left, "proportion of events"

    Wave combinedPrevValsWaves
    make/O/N=(dimSize(combinedPrevValsWaves, 0)) t50rPrevVals
    t50rPrevVals = combinedPrevValsWaves[p][%t50r]
    SetScale d, 0, 1, "s", t50rPrevVals

    WaveStats/Q t50rPrevVals
    print "average previous t50rs", V_avg
end

///////////
/// createFWHMAbsDiffDistributionPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: 
///////////
function createFWHMAbsDiffDistributionPlots()
    Wave combinedAbsDiffsWaves
    make/O/N=(dimSize(combinedAbsDiffsWaves, 0)) FWHMAbsDiffs
    FWHMAbsDiffs = combinedAbsDiffsWaves[p][%FWHM]

    Extract/O FWHMAbsdiffs, FWHMAbsdiffs_pos, FWHMAbsdiffs[p] > 0
    Extract/O FWHMAbsdiffs, FWHMAbsdiffs_neg, FWHMAbsdiffs[p] < 0
    SetScale d, 0, 1, "s", FWHMAbsDiffs, FWHMAbsdiffs_pos, FWHMAbsdiffs_neg

    probdistp("FWHMAbsdiffs_pos", 1)
    probdistp("FWHMAbsdiffs_neg", -1)

    Display/K=1/VERT FWHMAbsdiffs_pos_dist
    Label/Z bottom, "FWHM"
    Label/Z left, "proportion of events"

    Display/K=1/VERT FWHMAbsdiffs_neg_dist
    Label/Z bottom, "FWHM"
    Label/Z left, "proportion of events"

    Wave combinedPrevValsWaves
    make/O/N=(dimSize(combinedPrevValsWaves, 0)) FWHMPrevVals
    FWHMPrevVals = combinedPrevValsWaves[p][%FWHM]
    SetScale d, 0, 1, "s", FWHMPrevVals

    WaveStats/Q FWHMPrevVals
    print "average previous FWHMs", V_avg
end


///////////
/// createFWHMDiffDistributionPlots
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-23
/// NOTES: 
///////////
function createFWHMDiffDistributionPlots()
    Wave combinedDiffsWaves
    make/O/N=(dimSize(combinedDiffsWaves, 0)) FWHMDiffs
    FWHMDiffs = combinedDiffsWaves[p][%FWHM] * 100

    Extract/O FWHMdiffs, FWHMdiffs_pos, FWHMdiffs[p] > 0
    Extract/O FWHMdiffs, FWHMdiffs_neg, FWHMdiffs[p] < 0
    SetScale d, 0, 1, "%", FWHMDiffs, FWHMdiffs_pos, FWHMdiffs_neg

    probdistp("FWHMdiffs_pos", 1)
    probdistp("FWHMdiffs_neg", -1)

    Display/K=1/VERT FWHMdiffs_pos_dist
    Label/Z bottom, "% change FWHM"
    Label/Z left, "proportion of events"

    Display/K=1/VERT FWHMdiffs_neg_dist
    Label/Z bottom, "% change FWHM"
    Label/Z left, "proportion of events"

    Wave combinedPrevValsWaves
    make/O/N=(dimSize(combinedPrevValsWaves, 0)) FWHMPrevVals
    FWHMPrevVals = combinedPrevValsWaves[p][%FWHM]
    SetScale d, 0, 1, "s", FWHMPrevVals

    WaveStats/Q FWHMPrevVals
    print "average previous FWHMs", V_avg
end
