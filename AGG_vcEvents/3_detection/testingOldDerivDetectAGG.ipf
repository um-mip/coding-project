//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////
//////			detect from derivative
////// This is just a slightly cleaned and reformatted (indented)
////// version of the existing derivative detection, which AGG used
////// to try to troubleshoot and compare differernces
//////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

function derivDetectP2_AGG(pwavelet)
    string pwavelet
    
    variable localderivpeak=0

    string wavelet=""
    wavelet = removequotes( pwavelet )
    wavelet = pwavelet

    struct analysisparameters ps

    variable worked = readpanelparams2(ps)

    // units from parameter window are pA/msec/mV
    // units from data files are A, V, sec--convert params to be convenient for user!!!
    // units now converted in readpanelparams2!!!!

    // derivative params
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	

    // baseline params
    variable base_offset = ps.baseOffset_ms	
    variable base_dur = ps.baseDuration_ms	

    // peak parameters
    variable thissign = ps.peaksign					
    variable thresh = ps.peakThreshold_pA		
    variable peak_smooth = ps.peakSmoothPoints			
    variable area_thresh = ps.areaThreshold_pA_ms	

    // output trace parameters
    variable trace_dur = ps.traceDuration_ms	
    variable trace_offset = ps.traceOffset_ms	
    variable avecutoff =  ps.averageCutoff_pA		

    //control parameters
    variable automan =  ps.automan
    variable displayplots =  ps.displayplots

    variable this_area,accept,area_time=0

    variable time0,dx,pretime,maxtime
    variable peak,peak_time,dPeak,dPeak_time,baseline,base_start,base_end

    variable nevents=0,ipeak=0,ievent=0,npeaks=0,iavetrace=0, bad_int=0
    variable t50rise,p50, missedT50=0

    string trace="",prevtrace="",mymessages="",label=""

    dx=dimDelta($(wavelet),0)
    maxtime=dx*dimSize($(wavelet),0)

    duplicate /O $(wavelet), deriv
    if( dpresmooth > 0 )
        smooth /B dpresmooth, deriv
    endif

    differentiate deriv
    SetScale d 0,0,"A/sec", deriv
    if( dsmooth > 0 )
        smooth /B dsmooth, deriv
    endif

    dthresh*=thissign
    make /O w_levels
    findlevels /Q/D=w_levels deriv, dthresh
    nevents = V_LevelsFound

    if (nevents>50000) 
        mymessages="Lots of events!  N="+num2str(nevents)+"  Continue? 1 is yes, 0 is no"
        accept=acceptReject(mymessages)
        if(accept==0)
            print "User abort"
            abort
        endif
    endif
    variable firstevent=1

    ievent=0
    time0=w_levels[ievent]

    variable acceptDecision = 0

    make/D/O/N=(nevents) levelDecisions

    //create waves to hold the data
    // AGG - 2022-12-06, made all of these waves double precision
    make/D /o/N=(nevents) wpderiv
    make/D /o/N=(nevents) wpderiv_tb
    make/D /O/N=(nevents) wpeaks
    make/D /O/N=(nevents) wpeaks2
    make/D /O/N=(nevents) wpeaks_tb 
    make/D/o/N=(nevents)  wbase
    make/D/o/N=(nevents)  wbase_tb
    make/D/o/N=(nevents)  warea
    make/D/o/N=(nevents)  winterval
    make/D/o/N=(nevents)  wfwhm
    make/D/o/N=(nevents)  wdecaytime
    make/D/o/N=(nevents) wrisetime
    make/D/O/N=(nevents) w_avelist
    
    //initialize the waves
    wpderiv=0
    wpderiv_tb=0
    wpeaks=0
    wpeaks2=0
    wpeaks_tb=0
    wbase=0
    wbase_tb=0
    warea=0
    winterval=0
    wfwhm=0
    wdecaytime=0
    wrisetime=0
    w_avelist=0

    ipeak=0
    ievent=0
    pretime=0
    bad_int=0

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //start searching for events using the threshold crossings in the derivative
    variable t50rise2=nan

    // here is the big loop over all the threshold crossings
    do
        // verify we are on the "rising" phase of derivative threshold crossing (corrected for sign of course).
        wavestats /Q/R=(time0-min_dur,time0+min_dur) deriv

        if(thissign<0)
            localderivpeak = V_minloc
        else
            localderivpeak = V_maxloc
        endif

        if(localDerivPeak<=time0)
            //	print "DANGER WILL ROBINSON! DANGER!  aborting event", time0,localderivpeak,ievent
            acceptDecision = 0 // not rising
        else
            // chunk is peak window search!
            if( time0 < ( maxtime - chunk - dx * peak_smooth ) )
                
                // get peak of derivative
                duplicate /o/R=(time0,time0+chunk) deriv,dwave_chunk

                wavestats /Q dwave_chunk
                if (thissign<0) 
                    dPeak=V_min
                    dPeak_time=V_minloc
                else
                    dPeak=V_max
                    dPeak_time=V_maxloc
                endif

                // modification of the original code, now searching from the time of the peak of the derivative,
                // rather than the threshold crossing.  This should tighten burst mode searching.
                time0=dPeak_time

                // locate peak in raw data
                // get peak of event looking forward in time from when derivative crossed threshold.
                
                duplicate /o/R=(time0,time0+chunk) $(wavelet),wave_chunk

                smooth /B peak_smooth, wave_chunk
                wavestats /Q wave_chunk

                if (thissign<0) 
                    peak=V_min
                    peak_time=V_minloc
                else
                    peak=V_max
                    peak_time=V_maxloc
                endif
                
                // baseline
                // original code offset baseline from time of raw peak, instead of derivative peak
                base_start=time0-base_dur-base_offset
                base_end=time0-base_offset

                // Create a baseline chunk
                duplicate /o/R=(base_start,base_end) $(wavelet), wave_base
                wavestats /Q wave_base
                baseline = V_avg

                // show what's going on!!!
                duplicate /O/R=(peak_time-trace_offset, peak_time+trace_dur-trace_offset) $(wavelet), dispthis
                duplicate /O dispthis, deriv_dispthis

                if( peak_smooth > 0 )
                    smooth /b peak_smooth, dispthis
                endif
                if( dpresmooth > 0 )
                    smooth /B dpresmooth, deriv_dispthis
                endif

                differentiate deriv_dispthis
                SetScale d 0,0,"A/sec", deriv_dispthis
                if( dsmooth > 0 )
                    smooth /B dsmooth, deriv_dispthis
                endif 
                
                duplicate /O dispthis, gui_baseline
                gui_Baseline = baseline
                duplicate /O dispthis, gui_thresh
                gui_thresh = thissign*thresh+baseline

                if(firstevent==1)
                    make/O/N=1 showpeak
                    make/O/N=1 showpeakD
                    make/O/N=1 showpeaktime
                    make/O/N=1 showpeakDtime

                    setactiveanalysiswindowselect(1,1)  //set the first analysis window and clear
                    appendtograph dispthis
                    setaxis /A bottom
                    modifygraph rgb=(0,0,0)
                    appendtograph gui_baseline, gui_thresh
                    appendtograph showpeak vs showpeaktime
                    ModifyGraph mode(showpeak)=3,marker(showpeak)=19
                endif

                showpeak[0] = peak
                showpeaktime[0]= peak_time
                showpeakd[0] = dPeak
                showpeakdtime[0] = dPeak_time

                duplicate /O dispthis, gui_dthresh
                gui_dthresh = dthresh
            
                if(firstevent==1)	
                    setactiveanalysiswindowselect(2,1)
                    appendtograph deriv_dispthis
                    modifygraph rgb=(0,0,0)
                    appendtograph gui_dthresh
                    appendtograph showpeakd vs showpeakdtime
                    ModifyGraph mode(showpeakd)=3,marker(showpeakd)=19
                    firstevent=0
                endif

                if(((thissign*(peak-baseline))>thresh)%&((thissign*peak)>(thissign*baseline)))	
                    
                    // get t50 rise time to align events
                    p50 = 0.5*(peak-baseline)+baseline
                    findlevel /Q/R=(peak_time,peak_time-2*chunk) $(wavelet), p50
                    // original code only looked back 1 chunk time, instead of two

                    if(v_flag==0)
                        t50rise = V_levelX
                        // print "t50rise absolute", t50rise
                        variable t50rise_time = t50rise
                        duplicate /o/R=(t50rise-trace_offset,t50rise+trace_dur) $(wavelet),newtrace
                        t50rise = peak_time - t50rise
                        // print "peak time after t50rise", t50rise
                        missedT50=0
                    else
                        print "Missed level crossing, using peak to align event instead of t50", peak_time, chunk, peak*10^12, p50*10^12, (peak-baseline)*10^12, ipeak,ievent,peak_time
                        duplicate /o/R=(peak_time-trace_offset,peak_time+trace_dur) $(wavelet),newtrace
                        missedT50=1 /// sets the flag so this will not be added to average //must confirm!
                        t50rise=NaN
                    endif

                    //dx is set near the top from raw trace
                    setScale /P x,(-trace_offset),dx,newtrace
                    newtrace -= baseline
                    duplicate /o newtrace, smoothnewtrace
                    smooth /b peak_smooth, smoothnewtrace
                    
                    //modified 20130717 now using FWHM for area instead of area_win parameter
                    area_time=returnFWHM_AGG_troubleshoot("smoothnewtrace",thissign)
                    
                    this_area = area(newtrace,(0),(area_time))

                    // The original code did not check for the sign of area
                    // and it failed when a large positive area due to an artifact
                    // is detected when looking for negative events
                    accept = 0
                    if((thissign*(this_area)>area_thresh)&&(sign(this_area)==thissign))
                        if((this_area>0)&&(area_thresh!=0))  
                            // print "A:  area issues!",this_area
                        endif
                        accept=1
                        if(automan == 0) 
                            print "MANUAL DETECTION"
                            prevtrace=trace
                            print ipeak," Area ",area_thresh*1e15,this_area*1e15," peak ",(peak-baseline)*1e12," baseline ",baseline*1e12
                            accept = acceptReject("Accept?")
                            if(accept == 0)
                                acceptDecision = 4
                            else
                                acceptDecision = 5
                            endif
                        else
                            acceptDecision = 5
                        endif
                    else
                        if(area_thresh==0)
                            accept=1
                            if(automan == 0) 
                                print "MANUAL DETECTION"
                                prevtrace=trace
                                print ipeak," Area ",area_thresh*1e15,this_area*1e15," peak ",(peak-baseline)*1e12," baseline ",baseline*1e12
                                accept = acceptReject("Accept?")			
                            endif
                            if(accept == 0)
                                acceptDecision = 4
                            else
                                acceptDecision = 5
                            endif	
                        else
                            acceptDecision = 3 // area doesn't pass threshold
                            // print "More area issues."
                        endif					
                    endif		
                    if (accept==1)				
                        if((this_area>0)&&(area_thresh!=0))
                            // print "B: area issues!",this_area
                        endif				
                        wpderiv[ipeak]=dPeak
                        wpderiv_tb[ipeak]=dPeak_time
                        wpeaks[ipeak]=peak -baseline
                        wpeaks2[ipeak]=peak
                        wpeaks_tb[ipeak]=peak_time
                        wbase[ipeak]=baseline
                        wbase_tb[ipeak]=base_end
                        
                        // revised central t50rise calculator
                        // suppressing this 20211207, uses fixed window
                        // t50rise2 = returnT50rise("newtrace",thissign, trace_offset)
                        t50rise2 = returnT50rise_AGG_troubleshoot("newtrace",thissign, trace_offset)

                       
                        if( abs(t50rise-t50rise2) > 0.1*t50rise )
                            print "in detection, t50rise discrepancy:", t50rise, t50rise2
                        endif
                        if((t50rise>0)&&(missedT50<=0))
                            wrisetime[ipeak]=t50rise
                        else
                            wrisetime[ipeak]=nan
                        endif
                        
                        // original code used the trace duration to calculate area for output				
                        // this_area = area(newtrace,0,trace_dur)
                        // Another modification used 1090decay for area
                        // Now using FWHM for area

                        wfwhm[ipeak]=returnFWHM("smoothnewtrace",thissign)
                        wdecaytime[ipeak]=return1090Decay_AGG_troubleshoot("smoothnewtrace",thissign)
                        
                        area_time=wfwhm[ipeak]
                        if(numtype(area_time)==0)
                            this_area = area(newtrace,0,area_time)
                            
                            if( (thissign*this_area) < 0 )
                                // area has opposite sign than peak
                                warea[ipeak]=nan
                            else
                                warea[ipeak]=this_area
                            endif
                        else // no area measurement
                            warea[ipeak]=nan
                        endif

                        if(ipeak==0)
                            winterval[ipeak]=0
                            duplicate/O newtrace,oldtrace
                            iavetrace=0
                        else
                            winterval[ipeak]=dPeak_time-pretime
                            if((winterval[ipeak]>trace_dur)&&(missedT50==0))
                                if((winterval[ipeak-1]>trace_dur)%&(oldtrace[0]!=0)) // bitwise AND
                                    w_avelist[ipeak-1]=1
                                    if(iavetrace==0)
                                        duplicate /O oldtrace,wavetrace
                                    else
                                        wavetrace+=oldtrace
                                    endif
                                    
                                    iavetrace+=1
                                endif
                                if ((thissign*(peak-baseline))<(avecutoff))
                                    duplicate /O newtrace,oldtrace
                                else
                                    oldtrace=0.0
                                endif
                            endif
                            
                        endif
                        pretime = dPeak_time
                        
                        if((winterval[ipeak]>chunk)%|(ipeak==0)) // bitwise OR
                            ipeak+=1
                        else
                            acceptDecision = 6
                            bad_int+=1
                        endif
                    endif
                else
                    acceptDecision = 2 // peak doesn't pass threshold
                endif
            else
                acceptDecision = 1 // too close to end
            endif //maxtime edge
        endif  // endif statement regarding location of derivative max to avoid double event detection, or falling phase.
        levelDecisions[iEvent] = acceptDecision
        ievent+=1
        time0=w_levels[ievent]
    while((ievent<=nevents)&&((time0+chunk)<maxtime))

    summarizeAcceptDecisions(levelDecisions)

    if(iavetrace!=0)
        wavetrace/=iavetrace
    else
        print "Failed to identify any events that meet averaging criteria."
    endif

    npeaks=ipeak
    print "npeaks nevents,averaged traces", npeaks,nevents,iavetrace
    print "bad intervals: ", bad_int
    deletepoints (npeaks),(nevents-npeaks),wpderiv
    deletepoints (npeaks),(nevents-npeaks),wpderiv_tb
    deletepoints (npeaks),(nevents-npeaks),wpeaks
    deletepoints (npeaks),(nevents-npeaks),warea
    deletepoints (npeaks),(nevents-npeaks),wpeaks2
    deletepoints (npeaks),(nevents-npeaks),wpeaks_tb
    deletepoints (npeaks),(nevents-npeaks),wbase
    deletepoints (npeaks),(nevents-npeaks),wbase_tb
    deletepoints (npeaks),(nevents-npeaks),winterval
    deletepoints (npeaks),(nevents-npeaks),wfwhm
    deletepoints (npeaks),(nevents-npeaks),wdecaytime
    deletepoints (npeaks),(nevents-npeaks), wrisetime
    deletepoints (npeaks),(nevents-npeaks), w_avelist
        
    if(displayplots==1)
        if (iavetrace!=0)
            display wavetrace
        endif
        display deriv
        appendtograph wpderiv vs wpderiv_tb
        ModifyGraph mode(wpderiv)=3,marker(wpderiv)=19,rgb(wpderiv)=(0,0,65000)
        display $(wavelet)
        AppendToGraph wpeaks2 vs wpeaks_tb
        ModifyGraph mode(wpeaks2)=3,marker(wpeaks2)=19,rgb(wpeaks2)=(0,0,65000)
    endif

    // rename waves to reflect source data
    if(iavetrace!=0)
        string renamed=wavelet+"_ave"
        duplicate/O wavetrace,$renamed
        wavestats/Q wavetrace

        if(thissign==-1)
            wavetrace/=-V_min		//assumes negative going peak!!!
        else
            wavetrace/=V_max
        endif
        renamed=wavelet+"_nave"
        duplicate/O wavetrace,$renamed
        setscale d,0,1,"",$renamed
    endif

    string info=waveinfo($wavelet,0),timeunits="",signalunits="",myunits=""
    timeunits = stringbykey("XUNITS",info)
    signalunits = stringbykey("DUNITS",info)

    renamed=wavelet+"_lev"
    duplicate/O w_levels,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed

    renamed=wavelet+returnext("ave list")
    duplicate/O w_avelist,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed

    renamed=wavelet+"_der"
    duplicate/O wpderiv,$renamed
    myunits = signalunits+"/"+timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,thissign)

    renamed=wavelet+"_pks"
    duplicate/O wpeaks,$renamed
    myunits = signalunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,thissign)

    renamed=wavelet+"_int"
    duplicate/O winterval,$renamed
    myunits =timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,1)

    renamed=wavelet+"_pk2"
    duplicate/O wpeaks2,$renamed
    myunits = signalunits
    setScale d,0,1,myunits,$renamed

    renamed=wavelet+"_dtb"
    duplicate/O wpderiv_tb,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed

    renamed=wavelet+"_ptb"
    duplicate/O wpeaks_tb,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed

    renamed=wavelet+"_t50r"
    duplicate/O wrisetime,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,1)

    renamed=wavelet+"_fwhm"
    duplicate/O wfwhm,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,1)

    renamed=wavelet+"_1090d"
    duplicate/O wdecaytime,$renamed
    myunits = timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,1)

    renamed=wavelet+"_area"
    duplicate/O warea,$renamed
    myunits = signalunits+"*"+timeunits
    setScale d,0,1,myunits,$renamed
    probdistp(renamed,thissign)

    wavestats /Z/Q winterval
    print "Mean interval = ",V_avg
    print "Number of events = ",V_npnts

    // 20160229 need -deriv for wave intrinsic wave navigator
    renamed=wavelet+"-deriv"
    duplicate /O deriv, $renamed

    // 20150824 stop saving the derivative!!!
    killwaves /Z deriv

    updatedetectsummary() // creates and updates the summary waves
end