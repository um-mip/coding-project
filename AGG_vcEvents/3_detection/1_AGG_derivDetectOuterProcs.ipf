///////////
/// derivDetectP2
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-13
/// NOTES: 
/// 2023-12-22
/// Switched name from derivDetectAGG to derivDetectP2
/// so that the existing derivative detection workflows
/// use the updated functions
///////////
function derivDetectP2(string waveN)
    Wave rawWave = $(waveN)

    if(!WaveExists(rawWave))
        print "derivDetect: cannot find wave by name of", waveN, "within the root folder"
        abort
    endif

    struct analysisParameters ps
    variable worked = readpanelparams2(ps)

    // derivative parameters
    variable dpresmooth = ps.dPreDerivativeSmoothPoints
    variable dsmooth = ps.dSmoothPoints
    variable dthresh = ps.dThreshold_pA_ms	
    variable thissign = ps.peakSign
    
    Wave deriv = differentiateRawWave(rawWave, dpresmooth, dsmooth)

    if(!WaveExists(deriv))
        print "derivDetect: cannot find derivative wave. Stopping detection"
        abort
    endif

    [Wave/D derivLevelCrossings, variable nLevels] = findDerivLevelCrossings(deriv, dthresh, thissign, waveN)

    // global effect is to make these waves in the same folder as the rawWave
    // adds the appropriate endings and starts with the number of level crossings
    // we'll fix the number after detection
    makeDetectionStorageWaves(rawWave, nLevels)

    processDerivLevels(rawWave, deriv, derivLevelCrossings, nLevels)
end

///////////
/// processDerivLevels
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-14
/// NOTES: Use the parameters provided by the user to determine if level crossings should be considered events
/// This is the main loop function through the level crossings
///////////
function processDerivLevels(Wave rawWave, Wave deriv, Wave/D derivLevelCrossings, variable nLevels)
    if(!WaveExists(deriv) || !WaveExists(derivLevelCrossings) || !WaveExists(rawWave))
        print "processDerivLevels: no wave found for either the derivate, derivate level crossings or raw data. Quitting"
        abort
    endif

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	
    variable thissign = ps.peaksign

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    variable iLevel = 0
    variable derivCrossingTime = derivLevelCrossings[iLevel]
    variable nRising = 0
    make/O/N=(nLevels) risingLevels
    make/O/N=(nLevels) levelDecisions
    risingLevels = NaN
    levelDecisions = NaN

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    if(dataFolderRefStatus(varsDFR)==0)
        createDerivDetectVarsDF()
        varsDFR = getDerivDetectVarsDF()
    endif

    variable iEvent = 0

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    Wave smoothedW = smoothRawWaveForDerivDetect(rawWave, ps.peakSmoothPoints)

    variable prev_dPeak_time = 0
    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType = 0
    
    do
        // creates and resets the NVARs that will contain the event
        // information for each level check to NaNs
        resetDetectVarsToNaN()

        variable accept = 0, acceptDecision = 0
        variable isRising = checkRisingPhaseDeriv(deriv, derivCrossingTime, min_dur, thissign)

        if(isRising)
            risingLevels[nRising] = derivCrossingTime
            nRising++
            [accept, acceptDecision] = updateDerivFeatures(rawWave, smoothedW, deriv, derivCrossingTime)
        else
            acceptDecision = 0
        endif

        NVAR/Z/SDFR=varsDFR dPeak_time

        if(accept)
            // don't store a new peak that isn't at least the window search after the last peak
            if(iEvent == 0 || (dPeak_time - prev_dPeak_time) > ps.peakWindowSearch_ms)
                for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
                    string thisWaveType = StringFromList(iWaveType, dds.waveTypes)
                    string analysisWaveN = waveN + returnExt(thisWaveType)
                    Wave/SDFR=waveDFR analysisWave = $analysisWaveN

                    if(WaveExists(analysisWave))
                        string varName = stringbykey(thisWaveType, dds.typeToVar)
                        NVAR/Z/SDFR=varsDFR thisVar = $(varName)
                        analysisWave[iEvent] = thisVar
                    else
                        print "processDerivLevels: Couldn't find analysis wave", thisWaveType, analysisWaveN
                    endif

                endfor
                iEvent ++
            else
                acceptDecision = 6
            endif
            prev_dPeak_time = dPeak_time
        endif

        // FindValue/I=(iLevel) levelsWithDiffs
        // if(V_Value >= 0)
        //     displayDerivProcessing(rawWave, $(waveN + "-smth"), deriv)
        // endif

        levelDecisions[iLevel] = acceptDecision

        // make sure that the next level crossing is after this dPeak_time
        do
            iLevel++
            derivCrossingTime = derivLevelCrossings[iLevel]
            if(derivCrossingTime < prev_dPeak_time)
                acceptDecision = 7
                levelDecisions[iLevel] = acceptDecision
            endif
        while (iLevel<nLevels && derivCrossingTime < prev_dPeak_time)

        // print "level num", iLevel, "deriv crossing time", derivCrossingTime, "accept", accept
    while ((iLevel<nLevels)&&((derivCrossingTime+chunk)<maxtime))

    summarizeAcceptDecisions(levelDecisions)

    // Redimension, set the scales, and update probability distribution
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        thisWaveType = StringFromList(iWaveType, dds.waveTypes)
        analysisWaveN = waveN + returnExt(thisWaveType)
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            redimension/N=(iEvent) analysisWave

            string unitType = StringByKey(thisWaveType, dds.unitTypes)
            string unitString = returnUnitType(rawWave, unitType)

            setScale d, 0, 1, unitString, analysisWave

            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, thisSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "processDerivLevels: couldn't find analysis wave", thisWaveType, analysisWaveN
        endif

    endfor
    
    redimension/N=(nRising) risingLevels

    // Do intervals
    //Wave/D backInt
    //if (!WaveExists(backInt))
        Make/D/O backInt
    //    WAVE/D backInt
    //endif
    //Wave/D forInt
    //if (!WaveExists(forInt))
        Make/D/O forInt
     //   WAVE/D forInt
    //endif
    // TO-DO: decide on peak time or derivative peak time for interval
    [backInt, forInt] = calculateIntervals($(waveN + returnExt("peak time")), estUnknowns = 1, waveDur = maxTime)
    
    // Duplicate backwards interval into generic _int
    // Set the scales for intervals, and make probability distributions
    duplicate/O backInt, waveDFR:$(waveN + returnExt("interval"))
    Wave/SDFR=waveDFR intWave = $(waveN + returnExt("interval"))
    intWave[0] = NaN // remove the first backwards interval. Don't actually know what it was
    unitString = returnUnitType(rawWave, "time")
    setScale d, 0, 1, unitString, backInt
    setScale d, 0, 1, unitString, forInt
    setScale d, 0, 1, unitString, intWave

    probDistP(waveN + returnExt("interval"), 1)

    // Update average event list based on forwards and backwards intervals
    make/N=(numpnts(backInt))/O waveDFR:$(waveN + returnExt("ave list"))/Wave=averageList
    averageList = NaN
    // This function in theory returns averageList, but since there could be an
    // empty wave returned if waves can't be found, or numbers of points are different
    // Igor won't let averageList be updated from it. Ambiguous wave point number is error
    // But averageList global wave gets updated in the background
    calculateAverageList(backInt, forInt, traceDur = ps.traceDuration_ms)
    
    // Calculate avg event
    // TO-DO: AGG event panel also makes a copy of the average event within the cell summary folder
    // this probably results in duplicates, but there may be other places
    // that are expecting this in this data folder, so just leave it for now
    make/Wave/N=1/O waveToAvg = {rawWave}

    recalculateAverages5(waveToAvg)

    killwaves/Z waveToAvg

    updatedetectsummary() // creates and updates the summary waves
end

///////////
/// addEvent
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-19
/// NOTES: Use the parameters provided by the user to add a manual event
/// 2023-12-22, changed the name from addEventAGG to addEvent to use in existing cerebro/wave intrinsic workflow
/// updated the previous addEvent function to addEvent_20231222
///////////
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-18
/// NOTES: Instead of passing the "addTime" to the `updateDerivFeatures` function, which then would try to
/// find a local derivative after the time of the raw peak where the cursor was placed,
/// changed it so that a local derivative peak time is found within the window search
/// just before the addEvent time, and this is passed to the `updateDerivFeatures` as the derivTime variable
function addEvent(variable addTime, string rawWaveN)
    Wave rawWave = $(rawWaveN)

    if(!WaveExists(rawWave))
        print "addEvent: Raw data wave could not be found in the current data folder"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    Wave/SDFR=waveDFR  deriv = $(rawWaveN + returnExt("derivative waveIntrinsic"))

    if(!WaveExists(deriv))
        print "addEvent: Could not find the derivative wave in the current data folder. Quitting"
        abort
    endif

    string ptbWaveN = rawWaveN + returnExt("peak time")
    Wave/SDFR=waveDFR ptbWave = $(ptbWaveN)
    string dtbWaveN = rawWaveN + returnExt("derivative time")
    Wave/SDFR=waveDFR dtbWave = $(dtbWaveN)

    if(!WaveExists(ptbWave) || !WaveExists(dtbWave))
        print "addEvent: could not find PTB wave and/or DTB wave in the current data folder. Quitting"
        abort
    endif

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	
    variable thissign = ps.peaksign

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    if(dataFolderRefStatus(varsDFR)==0)
        createDerivDetectVarsDF()
        varsDFR = getDerivDetectVarsDF()
    endif

    Wave smoothedW = smoothRawWaveForDerivDetect(rawWave, ps.peakSmoothPoints)

    variable prev_dPeak_time = 0
    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType = 0

    resetDetectVarsToNaN()
    variable accept = 0, acceptDecision = 0

    // Find local derivative *before* raw add time first, then go through the updateDerivFeatures process
    variable startX=addTime-chunk
	variable endX=addTime

	wavestats/Q/R=(startX,endX) deriv
    variable tempDerivPeakTime
	if(thissign<0)
		tempDerivPeakTime = V_minloc
	else
		tempDerivPeakTime = V_maxloc
	endif

    [accept, acceptDecision] = updateDerivFeatures(rawWave, smoothedW, deriv, tempDerivPeakTime, checkThreshold = 0, checkArea = 0, manualConfirm = 1, keepDerTime = 1)

    if(!accept)
        return 0
    endif
    
    NVAR/Z/SDFR=varsDFR dPeak_time, peak_time

    // note that the nextEventIndex could be the number of points if the 
    // time is after the last event
    // Don't want to use this to find the time of the *next* event
    // because it could return the time of the last event in this case
    // which could be before the event that we're trying to add
    variable nextEventIndex = getIndex(ptbWave, peak_time)
    variable currentNumPnts = numpnts(ptbWave)
    variable nextEventDTime, prevEventDTime
    variable tooCloseToOthers = 0
    if(currentNumPnts > 0) // if there were events previously detected
        if(nextEventIndex == 0)
            prevEventDTime = 0
        else
            prevEventDTime = dtbWave[nextEventIndex - 1]
        endif
        if(nextEventIndex == currentNumPnts)
            nextEventDTime = maxTime
        else
            nextEventDTime = dtbWave[nextEventIndex]
        endif
        
        if((nextEventDTime - dPeak_time) <= ps.peakWindowSearch_ms || (dPeak_time - prevEventDTime) <= ps.peakWindowSearch_ms)
            tooCloseToOthers = 1
        endif
    endif

    if(tooCloseToOthers)
        print "too close to previously added events. Not adding"
        return 0
    endif

    // Store the values for the new event
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        string thisWaveType = StringFromList(iWaveType, dds.waveTypes)
        string analysisWaveN = waveN + returnExt(thisWaveType)
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            string varName = stringbykey(thisWaveType, dds.typeToVar)
            NVAR/Z/SDFR=varsDFR thisVar = $(varName)
            insertpoints/V=(thisVar) nextEventIndex, 1, analysisWave

            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, thisSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "addEvent: Couldn't find analysis wave", thisWaveType, analysisWaveN
        endif

    endfor

    // Update intervals
    Wave/D backInt, forInt
    // TO-DO: decide on peak time or derivative peak time for interval
    [backInt, forInt] = calculateIntervals($(waveN + returnExt("peak time")), estUnknowns = 1, waveDur = maxTime)
    
    // Duplicate backwards interval into generic _int
    // Set the scales for intervals, and make probability distributions
    duplicate/O backInt, waveDFR:$(waveN + returnExt("interval"))
    Wave/SDFR=waveDFR intWave = $(waveN + returnExt("interval"))
    intWave[0] = NaN // remove the first backwards interval. Don't actually know what it was
    probDistP(waveN + returnExt("interval"), 1)

    // Add point to avg list wave
    Wave/SDFR=waveDFR averageList = $(rawWaveN + returnExt("ave list"))
    insertpoints/V=(NaN) nextEventIndex, 1, averageList

    // This function in theory returns averageList, but since there could be an
    // empty wave returned if waves can't be found, or numbers of points are different
    // Igor won't let averageList be updated from it. Ambiguous wave point number is error
    // But averageList global wave gets updated in the background
    calculateAverageList(backInt, forInt, traceDur = ps.traceDuration_ms)

    // Calculate avg event
    // TO-DO: AGG event panel also makes a copy of the average event within the cell summary folder
    // this probably results in duplicates, but there may be other places
    // that are expecting this in this data folder, so just leave it for now
    make/Wave/N=1/O waveToAvg = {rawWave}

    recalculateAverages5(waveToAvg)

    killwaves/Z waveToAvg

    updatedetectsummary() // creates and updates the summary waves
end


///////////
/// deleteEvent
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-19
/// NOTES: 2023-12-22: updated name from deleteEventAGG to deleteEvent to use
/// existing cerebro/wave intrinsic structure
///////////
function deleteEvent(variable eventNum, string rawWaveN, variable peakSign) 
    Wave rawWave = $(rawWaveN)

    if(!WaveExists(rawWave))
        print "deleteEvent: Raw data wave could not be found in the current data folder"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)

    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType = 0

    // Delete the event from all of the waves, handle interval and avg later
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        string thisWaveType = StringFromList(iWaveType, dds.waveTypes)
        string analysisWaveN = waveN + returnExt(thisWaveType)
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            DeletePoints eventNum, 1, analysisWave

            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, peakSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "deleteEvent: Couldn't find analysis wave", thisWaveType, analysisWaveN
        endif

    endfor

    // Update intervals
    Wave/D backInt, forInt
    // TO-DO: decide on peak time or derivative peak time for interval
    [backInt, forInt] = calculateIntervals($(waveN + returnExt("peak time")), estUnknowns = 1, waveDur = maxTime)
    
    // Duplicate backwards interval into generic _int
    // Set the scales for intervals, and make probability distributions
    duplicate/O backInt, waveDFR:$(waveN + returnExt("interval"))
    Wave/SDFR=waveDFR intWave = $(waveN + returnExt("interval"))
    intWave[0] = NaN // remove the first backwards interval. Don't actually know what it was
    probDistP(waveN + returnExt("interval"), 1)

    // Delete point from avg list wave
    Wave/SDFR=waveDFR averageList = $(rawWaveN + returnExt("ave list"))
    DeletePoints eventNum, 1, averageList

    // This function in theory returns averageList, but since there could be an
    // empty wave returned if waves can't be found, or numbers of points are different
    // Igor won't let averageList be updated from it. Ambiguous wave point number is error
    // But averageList global wave gets updated in the background
    calculateAverageList(backInt, forInt, traceDur = ps.traceDuration_ms)

    // Calculate avg event
    // TO-DO: AGG event panel also makes a copy of the average event within the cell summary folder
    // this probably results in duplicates, but there may be other places
    // that are expecting this in this data folder, so just leave it for now
    make/Wave/N=1/O waveToAvg = {rawWave}

    recalculateAverages5(waveToAvg)

    killwaves/Z waveToAvg
    
    updatedetectsummary() // creates and updates the summary waves
end


///////////
/// reprocessPrevDetectedEvents
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-19
/// NOTES: Use the existing derivative times to reprocess the 
/// events that were either detected by derivDetect or added manually by the user
///////////
function reprocessPrevDetectedEvents(Wave rawWave)
    if(!WaveExists(rawWave))
        print "reprocessPrevDetectedEvents: Raw data wave could not be found"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    Wave/SDFR=waveDFR  deriv = $(waveN + returnExt("derivative waveIntrinsic"))

    if(!WaveExists(deriv))
        print "reprocessPrevDetectedEvents: Could not find the derivative wave in the same data folder as the raw wave. Quitting"
        abort
    endif

    string ptbWaveN = waveN + returnExt("peak time")
    Wave/SDFR=waveDFR ptbWave = $(ptbWaveN)
    string dtbWaveN = waveN + returnExt("derivative time")
    Wave/SDFR=waveDFR dtbWave = $(dtbWaveN)

    if(!WaveExists(ptbWave) || !WaveExists(dtbWave))
        print "reprocessPrevDetectedEvents: could not find PTB wave and/or DTB wave in the same data folder as the raw wave. Quitting"
        abort
    endif

    variable nEvents = numpnts(dtbWave)

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	
    variable thissign = ps.peaksign

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    variable iEvent = 0

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    if(dataFolderRefStatus(varsDFR)==0)
        createDerivDetectVarsDF()
        varsDFR = getDerivDetectVarsDF()
    endif

    Wave smoothedW = smoothRawWaveForDerivDetect(rawWave, ps.peakSmoothPoints)

    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType = 0

    // Make the detection storage waves, but only if they don't already exist. Don't want to rewrite them
    // This will redimension, though, if the wave doesn't equal the number of events in the DTB wave
    makeDetectionStorageWaves(rawWave, nEvents, onlyIfDontExist = 1)

    // Save the prop difference from the old value and new value
    // If already exists, redimensions and sets new values to zero
    make/N=(nEvents,nWaveTypes)/D/O waveDFR:$(waveN + "_diffs")/Wave=diffWave
    make/N=(nEvents,nWaveTypes)/D/O waveDFR:$(waveN + "_diffsAbs")/Wave=diffAbsWave
    make/N=(nEvents,nWaveTypes)/D/O waveDFR:$(waveN + "_prevVals")/Wave=prevValsWave

    for(iEvent=0; iEvent<nEvents; iEvent++)
        resetDetectVarsToNaN()
        variable accept = 0, acceptDecision = 0
        variable prevCalcDerivPeak = dtbWave[iEvent]

        [accept, acceptDecision] = updateDerivFeatures(rawWave, smoothedW, deriv, prevCalcDerivPeak, checkThreshold = 0, checkArea = 0, askIfDerChange = 1)

        for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
            string thisWaveType = StringFromList(iWaveType, dds.waveTypes)
            string thisExt = returnExt(thisWaveType)
            string analysisWaveN = waveN + thisExt
            Wave/SDFR=waveDFR analysisWave = $analysisWaveN

            if(WaveExists(analysisWave))
                variable prevVal = analysisWave[iEvent]

                string varName = stringbykey(thisWaveType, dds.typeToVar)
                NVAR/Z/SDFR=varsDFR thisVar = $(varName)
                analysisWave[iEvent] = thisVar

                variable diffProp = (thisVar - prevVal) / prevVal
                if(stringmatch(thisWaveType, "derivative") && diffProp > 0.01) // more than 1% diff in derivative
                    displayDerivProcessing(rawWave, smoothedW, deriv)
                    addPrevDetectVarsToDisplay()
                    print waveN, "iEvent", iEvent, "derivative", thisVar, "is more than 1% different from previously detected val", prevVal
                endif
                diffWave[iEvent][iWaveType] = diffProp
                diffAbsWave[iEvent][iWaveType] = (thisVar - prevVal)
                prevValsWave[iEvent][iWaveType] = prevVal
            else
                print "reprocessPrevDetectedEvents: Couldn't find analysis wave for", thisWaveType, analysisWaveN
            endif

        endfor
    endfor

    // Update probability distribution
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        thisWaveType = StringFromList(iWaveType, dds.waveTypes)
        thisExt = returnExt(thisWaveType)
        analysisWaveN = waveN + thisExt
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            string unitType = StringByKey(thisWaveType, dds.unitTypes)
            string unitString = returnUnitType(rawWave, unitType)

            setScale d, 0, 1, unitString, analysisWave

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, thisSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "reprocessPrevDetectedEvents: Couldn't find analysis wave for", thisWaveType , analysisWaveN
        endif

        SetDimLabel 1, iWaveType, $(thisExt[1, strlen(thisExt)]), diffWave
        SetDimLabel 1, iWaveType, $(thisExt[1, strlen(thisExt)]), diffAbsWave
        SetDimLabel 1, iWaveType, $(thisExt[1, strlen(thisExt)]), prevValsWave
    endfor
    
    // Do intervals
    Wave/D backInt, forInt
    // TO-DO: decide on peak time or derivative peak time for interval
    [backInt, forInt] = calculateIntervals($(waveN + returnExt("peak time")), estUnknowns = 1, waveDur = maxTime)
    
    // Duplicate backwards interval into generic _int
    // Set the scales for intervals, and make probability distributions
    duplicate/O backInt, waveDFR:$(waveN + returnExt("interval"))
    Wave/SDFR=waveDFR intWave = $(waveN + returnExt("interval"))
    intWave[0] = NaN // remove the first backwards interval. Don't actually know what it was
    unitString = returnUnitType(rawWave, "time")
    setScale d, 0, 1, unitString, backInt
    setScale d, 0, 1, unitString, forInt
    setScale d, 0, 1, unitString, intWave

    probDistP(waveN + returnExt("interval"), 1)

    // Update average event list based on forwards and backwards intervals
    make/N=(numpnts(backInt))/O waveDFR:$(waveN + returnExt("ave list"))/Wave=averageList
    // To-DO: making the choice here to start fresh for average determination
    // but there could be cases where you wouldn't want to do this
    averageList = NaN
    
    // This function in theory returns averageList, but since there could be an
    // empty wave returned if waves can't be found, or numbers of points are different
    // Igor won't let averageList be updated from it. Ambiguous wave point number is error
    // But averageList global wave gets updated in the background
    calculateAverageList(backInt, forInt, traceDur = ps.traceDuration_ms)
    
    // Calculate avg event
    // TO-DO: AGG event panel also makes a copy of the average event within the cell summary folder
    // this probably results in duplicates, but there may be other places
    // that are expecting this in this data folder, so just leave it for now
    make/Wave/N=1/O waveToAvg = {rawWave}

    recalculateAverages5(waveToAvg)

    killwaves/Z waveToAvg

    // edit/K=1 diffWave
    // modifyTable horizontalIndex=2 // show labels

    analyzeDetectDifferencesWave(diffWave)

    updatedetectsummary() // creates and updates the summary waves
end

///////////
/// redoDetectionDistWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-19
/// NOTES: Reprocess the derivative distribution waves for each parameter
///////////
function redoDetectionDistWaves(Wave rawWave)
    if(!WaveExists(rawWave))
        print "redoDetectionDistWaves: Raw data wave could not be found"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    [string cellName, string seriesName] = getCellIDAndSeriesFromWaveN(waveN)

    variable thisSign
    if(strlen(cellName)>0 && strlen(seriesName)>0)
        STRUCT analysisParameters detectPS
        getSeriesDetectParams(cellName, seriesName, paramStruct = detectPS)
        thisSign = detectPS.peaksign
    endif

    if(numtype(thisSign)!=0)
        struct analysisparameters ps
        variable worked = readpanelparams2(ps)
        thisSign = ps.peakSign
    endif

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)

    variable nWaveTypes = itemsInList(dds.waveTypes)
    variable iWaveType = 0

    // Update probability distribution
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        string thisWaveType = StringFromList(iWaveType, dds.waveTypes)
        string thisExt = returnExt(thisWaveType)
        string analysisWaveN = waveN + thisExt
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            string unitType = StringByKey(thisWaveType, dds.unitTypes)
            string unitString = returnUnitType(rawWave, unitType)

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, thisSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "redoDetectionDistWaves: Couldn't find analysis wave for", thisWaveType , analysisWaveN
        endif
    endfor
    
    Wave/SDFR=waveDFR intWave = $(waveN + returnExt("interval"))
    if(WaveExists(intWave))
        probDistP(waveN + returnExt("interval"), 1)
    endif
end

///////////
/// createRiseFallInfoWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-19
/// NOTES: Use the existing derivative times to reprocess the 
/// events that were either detected by derivDetect or added manually by the user
///////////
function createRiseFallInfoWaves(Wave rawWave)
    if(!WaveExists(rawWave))
        print "createRiseFallInfoWaves: Raw data wave could not be found"
        abort
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(rawWave)

    Wave/SDFR=waveDFR  deriv = $(waveN + returnExt("derivative waveIntrinsic"))

    if(!WaveExists(deriv))
        print "createRiseFallInfoWaves: Could not find the derivative wave in the same data folder as the raw wave. Quitting"
        abort
    endif

    string ptbWaveN = waveN + returnExt("peak time")
    Wave/SDFR=waveDFR ptbWave = $(ptbWaveN)
    string dtbWaveN = waveN + returnExt("derivative time")
    Wave/SDFR=waveDFR dtbWave = $(dtbWaveN)

    if(!WaveExists(ptbWave) || !WaveExists(dtbWave))
        print "createRiseFallInfoWaves: could not find PTB wave and/or DTB wave in the same data folder as the raw wave. Quitting"
        abort
    endif

    variable nEvents = numpnts(dtbWave)

    struct analysisparameters ps
    variable worked = readpanelparams2(ps)

    // derivative params
    variable dthresh = ps.dThreshold_pA_ms	
    variable min_dur = ps.dMinWidth_ms	
    variable chunk = ps.peakWindowSearch_ms	
    variable thissign = ps.peaksign

    variable dx=dimDelta(rawWave,0)
    variable maxtime=dx*dimSize(rawWave,0)

    variable iEvent = 0

    struct derivDetectStruct dds
    fillDerivDetectStruct(dds)
    DFREF varsDFR = getDerivDetectVarsDF()
    if(dataFolderRefStatus(varsDFR)==0)
        createDerivDetectVarsDF()
        varsDFR = getDerivDetectVarsDF()
    endif

    Wave smoothedW = waveDFR:$(waveN+"-smth")
    if(!WaveExists(smoothedW))
        Wave smoothedW = smoothRawWaveForDerivDetect(rawWave, ps.peakSmoothPoints)
    endif
    

    variable nWaveTypes = itemsInList(dds.riseFallWaves)
    variable iWaveType = 0

    // Make the detection storage waves, but only if they don't already exist. Don't want to rewrite them
    // This will redimension, though, if the wave doesn't equal the number of events in the DTB wave
    makeDetectionStorageWaves(rawWave, nEvents, onlyIfDontExist = 1)


    for(iEvent=0; iEvent<nEvents; iEvent++)
        resetDetectVarsToNaN()

        updateRiseFallFeatures(rawWave, smoothedW, deriv, iEvent)

        for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
            string thisWaveType = StringFromList(iWaveType, dds.riseFallWaves)
            string thisExt = returnExt(thisWaveType)
            string analysisWaveN = waveN + thisExt
            Wave/SDFR=waveDFR analysisWave = $analysisWaveN

            if(WaveExists(analysisWave))
                string varName = stringbykey(thisWaveType, dds.typeToVar)
                NVAR/Z/SDFR=varsDFR thisVar = $(varName)
                if(numtype(thisVar) == 2)
                    print waveN, iEvent, "NaN", varName
                endif
                analysisWave[iEvent] = thisVar
            else
                print "createRiseFallInfoWaves: Couldn't find analysis wave for", thisWaveType, analysisWaveN
            endif

        endfor
    endfor

    // Update probability distribution
    for(iWaveType=0; iWaveType<nWaveTypes; iWaveType++)
        thisWaveType = StringFromList(iWaveType, dds.riseFallWaves)
        thisExt = returnExt(thisWaveType)
        analysisWaveN = waveN + thisExt
        Wave/SDFR=waveDFR analysisWave = $analysisWaveN

        if(WaveExists(analysisWave))
            string probDistribution = StringByKey(thisWaveType, dds.probDistribution)

            string unitType = StringByKey(thisWaveType, dds.unitTypes)
            string unitString = returnUnitType(rawWave, unitType)

            setScale d, 0, 1, unitString, analysisWave

            strswitch (probDistribution)
                case "bySign":
                    probdistp(analysisWaveN, thisSign)
                    break
                case "pos":
                    probdistp(analysisWaveN, 1)
                    break
                default:
                    
                    break
            endswitch
        else
            print "createRiseFallInfoWaves: Couldn't find analysis wave for", thisWaveType , analysisWaveN
        endif
    endfor
end


///////////
/// analyzeDetectDifferencesWave
/// AUTHOR: Amanda GIbson
/// ORIGINAL DATE: 2023-12-20
/// NOTES: 
///////////
function analyzeDetectDifferencesWave(Wave/D waveRef)
    Variable numCols = DimSize(waveRef, 1)
    Variable numRows = DimSize(waveRef, 0)
    
    String dimLabel
    Variable i, j
    Variable avgVal, avgAbsVal, negAvgVal, posAvgVal
    Variable minNegVal, maxNegVal, minPosVal, maxPosVal
    Variable countZero, countAbsMoreThan5perc, countNaNs

    string summaryLabels = "nRows;nZeros;nNaNs;nGreater5Perc;"
    summaryLabels += "avgPercDiff;avgAbsPercDiff;negAvgPercDiff;posAvgPercDiff;"
    summaryLabels += "minNegPercDiff;maxNegPercDiff;minPosPercDiff;maxPosPercDiff;"

    variable nSummaries = itemsInList(summaryLabels)

    duplicate/O waveRef, $(nameOfWave(waveRef)+"_sum")/Wave=summaryWave
    redimension/N=(nSummaries, numCols) summaryWave

    summaryWave = NaN

    for(i = 0; i < numCols; i += 1)
        dimLabel = GetDimLabel(waveRef, 1, i)
        // print "Dimensional Label: ", dimLabel
        
        avgVal = 0; avgAbsVal = 0; negAvgVal = 0; posAvgVal = 0;
        minNegVal = 0; maxNegVal = 0; minPosVal = 0; maxPosVal = 0;
        countZero = 0; countAbsMoreThan5perc = 0; countNaNs = 0
        for(j = 0; j < numRows; j += 1)
            variable thisVal = waveRef[j][i]
            if(numType(thisVal) == 2)
                countNaNs++
            endif
            if(numtype(thisVal) == 0)
                avgVal += thisVal
                avgAbsVal += abs(thisVal)
                if(thisVal > 0) 
                    posAvgVal += thisVal
                endif
                if(thisVal < 0) 
                    negAvgVal += thisVal
                endif
                if(thisVal == 0) 
                    countZero += 1
                endif
                if(abs(thisVal) > 0.05) 
                    countAbsMoreThan5perc += 1
                endif
                if(thisVal < 0 && thisVal < minNegVal) 
                    minNegVal = thisVal
                endif
                if(thisVal < 0 && thisVal > maxNegVal) 
                    maxNegVal = thisVal
                endif
                if(thisVal > 0 && thisVal < minPosVal) 
                    minPosVal = thisVal
                endif
                if(thisVal > 0 && thisVal > maxPosVal) 
                    maxPosVal = thisVal
                endif
            endif
        endfor
        avgVal /= (numRows - countNaNs)
        avgAbsVal /= (numRows - countNaNs)
        negAvgVal /= (numRows - countNaNs)
        posAvgVal /= (numRows - countNaNs)

        variable iSummary = 0
        for(iSummary=0; iSummary<nSummaries; iSummary++)
            string sumLabel = StringFromList(iSummary, summaryLabels)
            SetDimLabel 0, iSummary, $(sumLabel), summaryWave
            variable sumVal
            strswitch (sumLabel)
                case "nRows":
                    sumVal = numRows
                    break
                case "nZeros":
                    sumVal = countZero
                    break
                case "nNaNs":
                    sumVal = countNaNs
                    break
                case "nGreater5perc":
                    sumVal = countAbsMoreThan5perc
                    break
                case "avgPercDiff":
                    sumVal = avgVal * 100
                    break
                case "avgAbsPercDiff":
                    sumVal = avgAbsVal * 100
                    break
                case "negAvgPercDiff":
                    sumVal = negAvgVal * 100 
                    break
                case "posAvgPercDiff":
                    sumVal = posAvgVal * 100
                    break
                case "minNegPercDiff":
                    sumVal = minNegVal * 100
                    break
                case "maxNegPercDiff":
                    sumVal = maxNegVal * 100
                    break
                case "minPosPercDiff":
                    sumVal = minPosVal * 100
                    break
                case "maxPosPercDiff":
                    sumVal = maxPosVal * 100
                    break
                default:
                    sumVal = NaN
                    break
            endswitch
            summaryWave[iSummary][i] = sumVal
        endfor
    endfor
    // edit/K=1/N=sumTable summaryWave.l, summaryWave
    // ModifyTable horizontalIndex = 2
    // saveTable("sumTable", fileName = nameOfWave(waveRef))
    // KillWindow/Z sumTable
end
