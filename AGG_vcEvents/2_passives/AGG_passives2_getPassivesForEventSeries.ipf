// Searches the cell info waves to find the corresponding passive series
// for each event series
// if there isn't a provided value, it will default to the series before and after

// this information is stored in the data folder for the event series
// as the global variable passiveSeries1 and passiveSeries2
function addPassiveSeriesNumForEventSeries(string cellName, variable eventSeriesNum)
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    string eventSeriesName = num2str(eventSeriesNum)

    if(DataFolderRefStatus(cellInfoDFR) != 0)
        Wave/T/SDFR = cellInfoDFR Events_series, passive1, passive2
        if(WaveExists(Events_series) & WaveExists(passive1) & WaveExists(passive2))
            FindValue/TEXT=eventSeriesName/Z Events_series
            variable indx = V_value
            string passSeriesName1 = passive1[indx]
            string passSeriesName2 = passive2[indx]
            variable passSeries1 = str2num(passSeriesName1)
            variable passSeries2 = str2num(passSeriesName2)

            // If no value, default to one before/after
            if(numtype(passSeries1)!=0)
                passSeries1 = eventSeriesNum - 1
                // print cellName, "series", eventSeriesNum, "no pre passive val, default to", passSeries1, "addPassiveSeriesNumForEventSeries"
            endif

            if(numType(passSeries2)!=0)
                passSeries2 = eventSeriesNum + 1
                // print cellName, "series", eventSeriesNum, "no post-passive val, default to", passSeries2, "addPassiveSeriesNumForEventSeries"
            endif

            DFREF eventSeriesDFR = getSeriesDF(cellName, num2str(eventSeriesNum))
            variable/G eventSeriesDFR:passiveSeries1 = passSeries1, eventSeriesDFR:passiveSeries2 = passSeries2
        endif
    endif
end

// This function stores the average passive properties for two surrounding
// passive series for an event series

// It calls the "addPassiveSeriesNumForEventSeries" to determine which
// passive series should be averaged

// If one is missing, the average is NaN
// These average values are stored within the series data folder for the
// event series. 

// The variable names match the names for the passive series, so that the
// same structure can be used to reference these values
function fillPassivesForEventSeries(cellName, eventSeriesNum)
    string cellName
    variable eventSeriesNum


    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    string eventSeriesName = num2str(eventSeriesNum)

    if(DataFolderRefStatus(cellInfoDFR) != 0)
        // print "cellInfoDFR exists in fillPassives"
        DFREF eventSeriesDFR = getSeriesDF(cellName, eventSeriesName)
        if(DataFolderRefStatus(eventSeriesDFR)==0)
            createSeriesDF(cellName, eventSeriesName)
            eventSeriesDFR = getSeriesDF(cellName, eventSeriesName)
        endif
        if(DataFolderRefStatus(eventSeriesDFR))
            // print "eventSeriesDFR exists in fillPassivesForEventSeries"
            addPassiveSeriesNumForEventSeries(cellName, eventSeriesNum)
            NVAR/Z/SDFR=eventSeriesDFR passiveSeries1, passiveSeries2

            STRUCT passiveProps passProps1
            STRUCT passiveProps passProps2
            // print passProps1.Rinput // NaN

            if(numType(passiveSeries1)==0) // normal number
                fillPassivesForSeries(cellName, passiveSeries1)
                DFREF passSeriesDFR1 = getSeriesDF(cellName, num2str(passiveSeries1))
                StructFill/SDFR = passSeriesDFR1 passProps1
                // print "after fill", passProps1.Rinput
            endif

            if(numType(passiveSeries2)==0) // normal number
                fillPassivesForSeries(cellName, passiveSeries2)
                DFREF passiveSeriesDFR2 = getSeriesDF(cellName, num2str(passiveSeries2))
                StructFill/SDFR = passiveSeriesDFR2 passProps2
            endif

            STRUCT passiveProps avgPassProps
            StructFill/AC=1/SDFR = eventSeriesDFR avgPassProps

            avgPassProps.Rinput = avgTwoVals(passProps1.Rinput, passProps2.Rinput)
            avgPassProps.Rseries = avgTwoVals(passProps1.Rseries, passProps2.Rseries)
            avgPassProps.RseriesSub = avgTwoVals(passProps1.RseriesSub, passProps2.RseriesSub)
            avgPassProps.capa = avgTwoVals(passProps1.capa, passProps2.capa)
            avgPassProps.holdingc = avgTwoVals(passProps1.holdingc, passProps2.holdingc)
            KillVariables/Z eventSeriesDFR:Tstart_rel, eventSeriesDFR:Tstart
        endif
    endif
end