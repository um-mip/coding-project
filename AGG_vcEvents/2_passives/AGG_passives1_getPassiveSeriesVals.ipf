// adds the passive values to the series folder using the passiveProps structure
// Can pull these out with StructFill/SDFR=seriesDFR passiveProp in a calling function
function fillPassivesForSeries(cellName, passiveSeriesNum)//, passiveProps)
    string cellName
    variable passiveSeriesNum

    string passiveSeriesName = num2str(passiveSeriesNum)
    string passiveWaveName = buildSeriesWaveName(cellName, passiveSeriesNum)

    // Get/create a folder for this passive series
    DFREF seriesDFR = getSeriesDF(cellName, passiveSeriesName)
    if(DataFolderRefStatus(seriesDFR)==0)
        createSeriesDF(cellName, passiveSeriesName)
        seriesDFR = getSeriesDF(cellName, passiveSeriesName)
    endif

    // Create the property variables using a structure
    STRUCT passiveProps passives
    StructFill/AC=1/SDFR = seriesDFR passives
    // print V_flag, "variables initialized", V_error, "not initialized"

    // Set everything to NaN initiallly
    string passProps = "Rinput;Rseries;RseriesSub;capa;holdingc;Tstart;Tstart_rel"
    variable numWaves = itemsInList(passProps)
    variable iWave = 0
    for(iWave = 0; iWave<numWaves; iWave++)
        string passProp = StringFromList(iWave, passProps)
        NVAR/Z/SDFR=seriesDFR passVar = $passprop
        passVar = NaN
    endfor

    // Find the passn wave for this cell
    string passnWN = cellName + "_passn"
    Wave/T passnWave = root:$passnWN

    if(WaveExists(passnWave))
        // Find which point in this passn wave matches this series
        // Extract /INDX passnWave, indxOfMatching, StringMatch(passnWave, passiveWaveName)
        // print "this series is point", indxOfMatching, "in the list"
        FindValue/TEXT=passiveWaveName/Z passnWave
        variable indx = V_value
        // print "by findValue, the point is", V_value
        
        // find the point in each passive property wave that matches the series index
        // Update the variable in the folder
        for(iWave = 0; iWave<numWaves; iWave++)
            passProp = StringFromList(iWave, passProps)
            NVAR/Z/SDFR=seriesDFR passVar = $passprop
            passVar = getPassiveValForSeries(cellName, passProp, indx)
        endfor
    endif
end

// This uses the index of the passive series (from the wave of passive wave names)
// to then find the corresponding value of the passive property indicated by the 
// passProp string for the indicted cellName
function getPassiveValForSeries(string cellName, string passProp, variable indx)
    string passWN = cellName + "_" + passProp

    Wave/D passWave = root:$passWN // when setting up, the current DF is panelDFR
    variable passiveVal = NaN
    if(WaveExists(passWave))
        passiveVal = passWave[indx]
        // print passProp, ":", passiveVal
    endif
    return passiveVal
end