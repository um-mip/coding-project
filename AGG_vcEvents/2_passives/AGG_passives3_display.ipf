function changePassiveTableWaves(string cellName)
    DFREF passiveDFR = getPassivePanelDF()
    if(DataFolderRefStatus(passiveDFR) == 0)
        createPassivePanelDF()
        passiveDFR = getPassivePanelDF()
    endif

    string passWaves = "Rinput;Rseries;RseriesSub;capa;holdingc;Tstart;Tstart_rel;passn"

    variable numWaves = itemsInList(passWaves)
    variable iWave = 0
    for(iWave = 0; iWave<numWaves; iWave++)
        string passWave = StringFromList(iWave, passWaves)
        string cellWaveN = cellName + "_" + passWave
        if(stringmatch(passWave, "passn"))
            Wave/T/SDFR=passiveDFR panelWaveT = $passWave
            Wave/T cellWaveT = root:$cellWaveN
            if(WaveExists(cellWaveT))
                duplicate/O/T cellWaveT, panelWaveT
            else
                redimension/N=0 panelWaveT
            endif
        else
            Wave/D/SDFR=passiveDFR panelWave = $passWave
            Wave/D cellWave = root:$cellWaveN
            if(WaveExists(cellWave))
                duplicate/O/D cellWave, panelWave
            else
                redimension/N=0 panelWave
            endif
        endif

    endfor
end

function changePassiveSeriesWaves(string cellName, variable eventSeriesNum)
    fillPassivesForEventSeries(cellName, eventSeriesNum)

    string passWaves = "passn;Rinput;Rseries;RseriesSub;capa;holdingc;"

    variable numWaves = itemsInList(passWaves)
    variable iWave = 0
    for(iWave = 0; iWave<numWaves; iWave++)
        string passProp = StringFromList(iWave, passWaves)
        updateSeriesPassiveWave(cellName, eventSeriesNum, passProp)
    endfor
end

function updateSeriesPassiveWave(string cellName, variable eventSeriesNum, string passProp)
    DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
    string eventSeriesName = num2str(eventSeriesNum)

    DFREF passiveDFR = getPassivePanelSeriesDF()
    if(StringMatch(passProp, "passn"))
        Wave/T/SDFR = passiveDFR passn
        passn = ""
    else
        Wave/D/SDFR = passiveDFR passWave = $passProp
        passWave = NaN
    endif

    if(DataFolderRefStatus(cellInfoDFR) != 0)
        DFREF eventSeriesDFR = getSeriesDF(cellName, eventSeriesName)
        if(DataFolderRefStatus(eventSeriesDFR))
			// print "found eventSeries DFR"
            NVAR/Z/SDFR = eventSeriesDFR passiveSeries1, passiveSeries2
            DFREF passive1DFR = getSeriesDF(cellName, num2str(passiveSeries1))
            DFREF passive2DFR = getSeriesDF(cellName, num2str(passiveSeries2))
            if(StringMatch(passProp, "passn"))
                passn[0] = "before s" + num2str(passiveSeries1)
                passn[1] = "after s" + num2str(passiveSeries2)
                passn[2] = "average"
            else
                NVAR/Z/SDFR = eventSeriesDFR avgProp = $passProp
                NVAR/Z/SDFR = passive1DFR prop1 = $passProp
                NVAR/Z/SDFR = passive2DFR prop2 = $passProp
				// print prop1, prop2
                passWave[0] = prop1
                passWave[1] = prop2
                passWave[2] = avgProp
            endif
		else
			// print "did not find eventSeriesDFR"
        endif
    endif

end


function remakePassiveGraph( [expcode, graphName] )
	string expcode // supports multiple passive windows
	string graphName
	variable /G g_sn=0
	string /G g_sstr=""

	string name="passiveTable0", wn=""

		string rinw = "rinput"
		string rsw = "rseries"
		string rssw = "rseriessub"
		string cw = "capa"
		string hcw = "holdingc"
		string tw = "tstart"
		string trw = "tstart_rel"
		string pw = "passn"
		string psw = "passnshort"
	
	if( paramisdefault( expcode ) )
		WAVE holdingc = holdingc
		WAVE tstart = tstart
		WAVE rinput = rinput
		WAVE rseries = rseries
		WAVE rseriessub = rseriessub
		WAVE capa = capa
		WAVE tstart_rel = tstart_rel
		WAVE/T passn = passn
		WAVE/T passnshort = passnshort
		string temp = datecodefromanything( passn[0] )
		expcode = temp
	else

		wn = expcode + "_Rinput"
		WAVE Rinput = $wn // Rinput
		rinw = wn
		
		wn = expcode + "_Rseries"
		WAVE Rseries = $wn // Rseries
		rsw = wn
		
		wn = expcode + "_RseriesSub"
		WAVE RseriesSub = $wn // RseriesSub
		rssw = wn
		
		wn = expcode + "_capa"
		WAVE capa = $wn // capa
		cw = wn
		
		wn = expcode + "_holdingc"
		WAVE holdingc = $wn // holdingc
		hcw = wn
		
		wn = expcode + "_Tstart"
		WAVE tstart = $wn // Tstart
		tw = wn
		
		wn = expcode + "_Tstart_rel"
		WAVE tstart_rel = $wn 
		trw = wn
		
		wn = expcode + "_passn"
		WAVE/T passn = $wn
		pw = wn
		
		wn = expcode + "_passnshort"
		WAVE/T passnshort = $wn
		psw = wn
		
		name = "T_" + expcode + "_0"
	endif	
	PauseUpdate; Silent 1		// building window...
	
	variable updateGraph, makeNewGraph
	if(paramisdefault(graphName))
		name = "PG_" + expcode + "_0" // "passiveGraph0"
		doWindow $name
		if(V_flag == 0)
			updateGraph = 1
            makeNewGraph = 1
			// Display/N=$name/k=1 /W=(239,389,1217,953)
		else
			updateGraph = 0
		endif
	else
		name = graphName
        // doWindow $name // alt is winType which suppports subwindows
        // print V_flag
        variable windowType = winType(name)
		// if(V_flag == 0)
		if(windowType == 0) // doesn't exist
			updateGraph = 1
            makeNewGraph = 1
		else
            clearDisplay(name)
			updateGraph = 1
		endif
		// updateGraph = 1
	endif
    if(makeNewGraph)
        // define the screen parameter structure
        STRUCT ScreenSizeParameters ScrP
        // initialize it
        InitScreenParameters(ScrP)
        
        variable winW = ScrP.scrwpnt
        variable winH = ScrP.scrhpnt
        variable leftStart = 40*winW
        variable topStart = 20*winH
        variable winWidth = 60*winW
        variable winHeight = 70*winH
        Display/N=$name/k=1 /W=(leftStart, topStart, leftStart+winWidth, topStart+winHeight)
        variable xStart = 0.9*winWidth
        variable yStart = 0.85*winHeight
        variable yInc = 0.03*winHeight
		// Need to explicitly tell these where to go maybe
        CheckBox checkRin,win=$name,pos={xStart,yStart + yInc * 0},size={31,14},title="Rin",value= 1, proc=passcheck
		CheckBox checkCap,win=$name,pos={xStart,yStart + yInc * 1},size={34,14},title="Cap",labelBack=(0,65535,0)
		CheckBox checkCap,fColor=(0,65535,0),value= 1, proc=passcheck
		CheckBox checkRs,win=$name,pos={xStart,yStart + yInc * 2},size={28,14},title="Rs",labelBack=(65535,0,0)
		CheckBox checkRs,fColor=(65535,0,0),value= 1, proc=passcheck
		CheckBox checkHc,win=$name,pos={xStart,yStart + yInc * 3},size={30,14},title="HC",labelBack=(1,16019,65535)
		CheckBox checkHc,fColor=(1,16019,65535),value= 1, proc=passcheck
    endif
	if(updateGraph )
        // The hook sets up the tool tips to investigate which series/value you're hovering over
		SetWindow $name,hook(testhook)= MyWinHook, userdata(expCode) = expcode // Can't be an interior subwindow to hook
        // DoWindow/F $name
        // Need to tell it which, but need to not end with this on top, or return the panel to the top when making the panel
		if(WaveExists(Tstart))
			if(!WaveExists(holdingC))
				make/O/N=0 tempHoldingC
				Wave holdingC = tempHoldingC
				hcw = "tempHoldingC"
			endif
			if(!WaveExists(Rinput))
				make/O/N=0 tempRinput 
				Wave Rinput = tempRinput
				rinw = "tempRinput"
			endif
			if(!WaveExists(RseriesSub))
				make/O/N=0 tempRseriesSub 
				Wave RseriesSub = tempRseriesSub
				rssw = "tempRseriesSub"
			endif
			if(!WaveExists(capa))
				make/O/N=0 tempCapa 
				Wave capa = tempCapa
				cw = "tempCapa"
			endif
			if(!WaveExists(Rseries))
				make/O/N=0 tempRseries 
				Wave Rseries = tempRseries
				rsw = "tempRseries"
			endif
			AppendToGraph/W=$name holdingc vs Tstart
			AppendToGraph/W=$name/R Rinput vs Tstart
			Appendtograph/W=$name/R=rseries RseriesSub vs Tstart
			AppendToGraph/W=$name/L=cap capa vs Tstart
			AppendToGraph/W=$name/R=rseries/T Rseries vs Tstart_rel

			ModifyGraph/W=$name mode( $hcw )=4, mode( $rinw )=3,mode( $rssw )=4,mode( $cw )=3,mode( $rsw )=3
			ModifyGraph/W=$name marker( $hcw )=19,marker( $rinw )=19,marker( $rssw )=19,marker( $cw )=19
			ModifyGraph/W=$name lStyle($hcw)=2,lStyle($rssw)=2
			ModifyGraph/W=$name rgb($hcw)=(0,0,65535),rgb($rinw)=(0,0,0),rgb($cw)=(0,65535,0)
			ModifyGraph/W=$name rgb($rsw)=(0,0,0)
			ModifyGraph/W=$name msize($hcw)=3
		//	ModifyGraph/W=$name textMarker(Rseries#1)={passnshort,"default",0,0,5,0.00,0.00}
			ModifyGraph/W=$name axRGB(left)=(0,0,65535),axRGB(rseries)=(65535,0,0),axRGB(cap)=(0,65535,0)
			ModifyGraph/W=$name tlblRGB(left)=(0,0,65535),tlblRGB(rseries)=(65535,0,0),tlblRGB(cap)=(0,65535,0)
			ModifyGraph/W=$name alblRGB(left)=(0,0,65535),alblRGB(rseries)=(65535,0,0),alblRGB(cap)=(0,65535,0)
			ModifyGraph/W=$name lblPos(left)=88,lblPos(right)=82,lblPos(rseries)=50,lblPos(cap)=50
			ModifyGraph/W=$name freePos(rseries)={1,kwFraction}
			ModifyGraph/W=$name freePos(cap)={1,kwFraction}
		
			//ModifyGraph/W=$name hideTrace( $rsw )=1
		
			ModifyGraph/W=$name dateInfo(bottom)={0,0,0}
			ModifyGraph/W=$name minor(top)=1,sep(bottom)=30,sep(top)=30
			ModifyGraph/W=$name minor(bottom)=1
		//	ModifyGraph/W=$name manTick(top)={0,60,0,0},manMinor(top)={1,30}
		
			Label/W=$name left "Holding current ( pA )"
			Label/W=$name bottom "Time"
			Label/W=$name right "\\K(0,0,0)Input Resistance ( MOhm )"
			Label/W=$name top "Elapsed time (seconds )"
			Label/W=$name rseries "Series resistance ( MOhm )"
			Label/W=$name cap "Capacitance (pF )"
			SetAxis/W=$name left -100,100
			SetAxis/W=$name right 0,1500
			SetAxis/W=$name rseries 0,30
			SetAxis/W=$name cap 0,30
		endif
	endif
End