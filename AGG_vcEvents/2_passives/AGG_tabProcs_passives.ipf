function switchPassDisplayProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct
    if(B_Struct.eventCode == 2)
        string tableName = getuserdata("", B_Struct.ctrlName, "table")
        string graphName = getuserdata("", B_Struct.ctrlName, "graph")
        
        string panelName = B_Struct.win

        string fullTable = panelName + "#" + tableName
        string fullGraph = panelName + "#" + graphName

        GetWindow/Z $fullTable hide
        variable tableIsHidden = V_value // 1 when hidden
        
        if(tableIsHidden == 1)
            SetWindow $fullTable Hide = 0 // show table
            SetWindow $fullGraph Hide = 1 // hide graph
            Button $B_Struct.ctrlName title = "Show Passive Graph"
        else
            SetWindow $fullTable Hide = 1 // hide table
            SetWindow $fullGraph Hide = 0 // show graph
            Button $B_Struct.ctrlName title = "Show Passive Table"
        endif
    endif
end

function makePassDisplayProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct
    if(B_Struct.eventCode == 2)
        string selCell
        variable selSeries, validCell, prevPassives, prevDetection
        [selCell, selSeries, validCell, prevPassives, prevDetection]  = getInfoAboutSelectedSeries(panelName = B_Struct.win)
        if(prevPassives)
            DFREF currentFolder = getDataFolderDFR()
            setDataFolder root:
            remakePassiveGraph(expcode=selCell, graphName="passives_" + selCell) // don't automake standalone
            DoWindow/F passives
            setDataFolder currentFolder
        else
            // print "no previous passives"
            clearDisplay("passives")
        endif

    endif
end