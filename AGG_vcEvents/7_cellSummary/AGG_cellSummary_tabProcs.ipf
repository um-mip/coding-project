
// Run when a user interacts with the cell list wave
// Update the list wave for series based on the selected cell
function selectedCellListProc_cell(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF panelDFR = getEventsPanelDF()

        string selectedCell = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)

        // print selectedCell
        clearDisplay("AGG_events#cellSumGraph")
        updateCellRegionsLB(selectedCell, whichLB = LB_Struct.ctrlName)
        displayCellParams()
        updateCellSumTable(selectedCell)
    endif
    return 0
end

function updateCellRegionsLB (string cellName[, string whichLB, Wave/T listWave, Wave selWave])
    DFREF panelDFR = getEventsPanelDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    if(paramIsDefault(whichLB))
        whichLB = "cellListBox_cell"
    endif

    if(paramIsDefault(listWave) || !WaveExists(listWave))
        strswitch (whichLB)
            case "cellListBox_cell":
                Wave/T/SDFR=cellSumDFR listWave = cellRegionNames
                
                break
            case "cellListBox_burst":
                Wave/T/SDFR=getVBWPanelDF() listWave = cellRegionNames
                break
            default:
                Wave/T/SDFR=cellSumDFR listWave = cellRegionNames
                
                break
        endswitch
    endif

    if(paramIsDefault(selWave) || !WaveExists(selWave))
        strswitch (whichLB)
            case "cellListBox_cell":
                Wave/B/SDFR=cellSumDFR selWave = regionSelWave_cell
                break
            case "cellListBox_burst":
                Wave/B/SDFR=getVBWPanelDF() selWave = regionSelWave_CellBurst
                break
            default:
                Wave/B/SDFR=cellSumDFR selWave = regionSelWave_cell
                break
        endswitch
    endif

    if(!WaveExists(listWave) || !WaveExists(selWave))
        return NaN
    endif

    duplicate/FREE selWave, prevSelWave
    
    redimension/N=1 listWave
    listWave = "Full duration"

    redimension/N=1 selWave
    selWave = 1

    if(strlen(cellName)==0)
        return NaN
    endif

    DFREF regionDFR = returnCellRegionsDFR_bySubType(cellName, "all") // only going to look in the all folder. Assuming regions match elsewhere
    if(dataFolderRefStatus(regionDFR)==0)
        return NaN
    endif

    string regionsNames = "Full duration;"
    regionsNames += getChildDFRList(regionDFR)
    wave/T regionsNamesAsWave = listToTextWave(regionsNames, ";")
    if(!WaveExists(regionsNamesAsWave) || numpnts(regionsNamesAsWave) == 0)
        return NaN
    endif

    string listWaveN
    DFREF listWaveDFR
    [listWaveN, listWaveDFR] = getWaveNameAndDFR(listWave)
    duplicate/O/T regionsNamesAsWave, listWaveDFR:$(listWaveN)/Wave=listWave

    redimension/N=(numpnts(listWave)) selWave
    if(numpnts(selWave) == numpnts(prevSelWave))
        selWave = prevSelWave
    else
        selWave = 0
        if(numpnts(listWave)>0)
            selWave[0] = 1
        endif
    endif
end

function selectedRegionListProc_cell(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        string selectedCell = getSelectedItem("cellListBox_cell", hostName = LB_Struct.win)

        displayCellParams()
        updateCellSumTable(selectedCell)
    endif
    return 0
end

function displayCellParamsProc(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF cellSumDFR = getEventsCellSumOutDF()

        string selDisplay = getSelectedItem("paramListBox_cell", hostName = LB_Struct.win)
        string selRegion = getSelectedItem("regionListBox_cell", hostName = LB_Struct.win)
        if(StringMatch(selDisplay, "distribution plot"))
            Wave/T/SDFR=cellSumDFR titleWave = titleWave_distParams
            Wave/SDFR=cellSumDFR selWave = selWave_distParams
        else
            if(stringmatch(selDisplay, "plot series avg v time") && stringMatch(selRegion, "Full duration"))
                Wave/T/SDFR=cellSumDFR titleWave = titleWave_seriesParams
                Wave/SDFR=cellSumDFR selWave = selWave_seriesParams
            else
                make/O/N=0/T cellSumDFR:blankTitle/Wave=titleWave
                make/O/N=0 cellSumDFR:blankSel/Wave=selWave
            endif
        endif
        ListBox paramListBox_cellParams, listWave = titleWave, selWave = selWave
        displayCellParams()
    endif
    return 0
end

function [string paramWN, string paramLabel] getSelectedDisplay_cellSum()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    ControlInfo /W=AGG_Events paramListBox_cell
    variable selParamRow = V_Value
    // print V_value, "selected row"

    Wave/T/SDFR=cellSumDFR listWave, titleWave
    paramWN = ""
    paramLabel = ""
    if(selParamRow < numpnts(titleWave))
        paramWN = listWave[selParamRow]
        paramLabel = titleWave[selParamRow]
    endif   

    return [paramWN, paramLabel]
end

function [string paramWN, string paramLabel] getSelectedParam_cellSum([string dispType])
    if(paramIsDefault(dispType))
        dispType = "seriesByTime"
    endif
    
    DFREF cellSumDFR = getEventsCellSumOutDF()

    string listWaveN, titleWaveN, selWaveN
    strswitch (dispType)
        case "seriesByTime":
            listWaven = "listWave_seriesParams"
            titleWaven = "titleWave_seriesParams"
            selWaven = "selWave_seriesParams"
            break
        case "dist":
            listWaven = "listWave_distParams"
            titleWaven = "titleWave_distParams"
            selWaven = "selWave_distParams"
            break
        default:
            listWaven = "listWave_seriesParams"
            titleWaven = "titleWave_seriesParams"
            selWaven = "selWave_seriesParams"
            break
    endswitch

    ControlInfo /W=AGG_Events paramListBox_cellParams
    variable selParamRow = V_Value
    // print V_value, "selected row"

    Wave/T/SDFR=cellSumDFR listWave = $listWaveN, titleWave=$titleWaveN
    paramWN = ""
    paramLabel = ""
    if(selParamRow < numpnts(titleWave))
        paramWN = listWave[selParamRow]
        paramLabel = titleWave[selParamRow]
    endif   

    return [paramWN, paramLabel]
end

function/Wave getSelectedSeries_cellSum()
    DFREF panelDFR = getEventsPanelDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    WAVE/T/Z seriesNums = cellSumDFR:selCellSeries
    WAVE/B/Z selSeries = cellSumDFR:selSeries
    // print selSeries

    variable numSeries = numpnts(seriesNums), iSeries = 0, addedSeries = 0
    string thisSeries

    make/O/N=0 cellSumDFR:selectedSeriesNums/Wave=selectedSeriesNums

    for(iSeries = 0; iSeries<numSeries; iSeries++)
        if(selSeries[iSeries]==1)
            thisSeries=seriesNums[iSeries]
            if(strlen(thisSeries)>0)
                redimension/N=(addedSeries+1) selectedSeriesNums
                selectedSeriesNums[addedSeries] = str2num(thisSeries)
                addedSeries++
            endif
        endif
    endfor
    return selectedSeriesNums
end

///////////
/// openTauFitProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Open the tau fit panel
///////////
function openTauFitProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        makeTauPanel(showFitButtons = 0)
    endif
    return 0
end

///////////
/// fitTauCellProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Open the tau fit panel
///////////
function fitTauCellProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        string cellName = getSelectedItem("cellListBox_cell", hostName = "AGG_events")
        fitTauForCell(cellName)
        displayCellParams()
        updateCellSumTable(cellName)
    endif
    return 0
end

///////////
/// fitTauAllCellsEventsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Open the tau fit panel
///////////
function fitTauAllCellsEventsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        fitTauAllCells()
        string cellName = getSelectedItem("cellListBox_cell", hostName = "AGG_events")
        updateCellSumTable(cellName)
    endif
    return 0
end