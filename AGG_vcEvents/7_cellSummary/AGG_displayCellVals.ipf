function displayCellParams([variable suppressWarning])
    string dispType, dispTypeLabel
    [dispType, dispTypeLabel] = getSelectedDisplay_cellSum()
    string paramWN, paramLabel
    [paramWN, paramLabel] = getSelectedParam_cellSum(dispType = dispType)
    string selRegion = getSelectedItem("regionListBox_cell", hostName = "AGG_events")

    string cellName = getSelectedItem("cellListBox_cell", hostName = "AGG_events")

    string subType = getSelectedDispType()

    string displayHost = "AGG_events#cellSumGraph"
    clearDisplay(displayHost)

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    if(paramIsDefault(suppressWarning))
        suppressWarning = 0
    endif
    

    if(strlen(ListMatch(cellSum.tauFitDisplay, dispType))>0) // if tau fit display
        recreateTauFitGraph(cellName, displayGraphName = displayHost)
        return 0
    endif

    DFREF cellSumDFR = getEventsCellSumOutDF()

    string tableHost = "AGG_Events#cellSumHistoTable"
    string currentTab = getSelectedTab()
    if(stringmatch(currentTab, "Cell Summary") && winExists(tableHost))
        if(winExists(tableHost))
            SetWindow $tableHost Hide=(1) // enable when hide = 0
        endif
        ModifyControl paramListBox_cellParams, disable = 0
    endif
    
    if(strlen(ListMatch(cellSum.eventAvgWaves, dispType))>0)
        strswitch (subType)
            case "all":
                if(!suppressWarning)
                    print "For displaying the average trace, resorting to default, not all events"  
                endif
                subType = "specify" // so only have to change default in getTypeStorageDF
                break
            case "interval":
                if(!suppressWarning)
                    print "For displaying the average trace, resorting to default, not intervals subset" 
                endif
                subType = "specify" // so only have to change default in getTypeStorageDF
                break
            default:
                
                break
        endswitch
        
        DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = dispType, outType = "cellAvgDisplay", regionName = selRegion)

        if(dataFolderRefStatus(cellDFR)==0)
            return 0
        endif

        Wave/SDFR=cellDFR thisWave = $dispType
        if(!WaveExists(thisWave))
            print "displayCellParams: couldn't find wave to plot", dispType
            return 0
        endif
        duplicate/O thisWave, cellSumDFR:cellSumTrace/Wave=sumWave

        AppendToGraph/W=$displayHost sumWave
        if(stringmatch(dispType,"cellAvgEvent"))
            Label/W=$displayHost left, "current"
        else
            Label/W=$displayHost left, "normalized current"
        endif
        Label/W=$displayHost bottom, "elapsed time"
        ModifyGraph/W=$displayHost rgb(cellSumTrace) = (0,0,0)
        ModifyGraph/W=$displayHost gfSize=14
        SetAxis/A/E=3/W=$displayHost bottom
        SetAxis/A/E=3/W=$displayHost left
        return 0    
    endif

    if(stringmatch("smartConcDisplay", dispType))
        DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, regionName = selRegion, outType = "cellHistoGraph", param = "eventHisto")

        if(dataFolderRefStatus(cellDFR)==0)
            return 0
        endif
        
        createEventsCellSumOutDF_smartConc()
        DFREF secondSumDFR = getEventsCellSumOutDF_smartConc() // otherwise waves overwrite in other display

         // Cell histogram
        Wave/SDFR=secondSumDFR eventHisto, eventHistoByFreq
        if(waveExists(eventHisto))
            redimension/N=0 eventHisto
        endif
        if(WaveExists(eventHistoByFreq))
            redimension/N=0 eventHistoByFreq
        endif

        if(stringmatch(currentTab, "Cell Summary") && winExists(tableHost))
            SetWindow $tableHost Hide=(0) // enable when hide = 0
            ModifyControl paramListBox_cellParams, disable = 1
        endif

        if(!stringmatch(selRegion, "Full duration"))
            NVAR/Z/SDFR=cellDFR startTime, endTime
            plotPTBHisto(cellName, cellSmartConcDF = cellDFR, histoHost = displayHost, cellSumDFR = secondSumDFR, startTime = startTime, endTime = endTime)
        else
            plotPTBHisto(cellName, cellSmartConcDF = cellDFR, histoHost = displayHost, cellSumDFR = secondSumDFR)
        endif

        showRegionsEventHistoTable(tableName = tableHost)
        return 0
    endif

    
    if(StringMatch("dist", dispType))
        DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = paramWN, outType = "cellDisplay", regionName = selRegion)
        
        if(dataFolderRefStatus(cellDFR)==0)
            return 0
        endif

        Wave/SDFR=cellDFR thisWave = $paramWN + "_dist"
        if(!WaveExists(thisWave))
            print "displayCellParams: couldn't find wave to plot", paramWN
            return 0
        endif
        
        duplicate/O thisWave, cellSumDFR:cellSumTrace/Wave=sumWave
        variable sortDir = appendDistWave(sumWave, displayHost = displayHost)
        setDistPlotAxes(displayHost=displayHost, sortDir=sortDir, bottomLabel=paramLabel)
        ModifyGraph/W=$displayHost rgb(cellSumTrace) = (0,0,0)
        return 0
    endif

    if(stringmatch("seriesByTime", dispType) && stringmatch("Full duration", selRegion))
        DFREF cellDFR = returnCellEventsDFR_bySubType(cellName, subType, param = paramWN, outType = "cellDisplay")

        if(dataFolderRefStatus(cellDFR)==0)
            return 0
        endif

        Wave/SDFR=cellDFR thisWave = $paramWN
        if(!WaveExists(thisWave))
            print "displayCellParams: couldn't find wave to plot", paramWN
            return 0
        endif
        duplicate/O thisWave, cellSumDFR:cellSumTrace/Wave=sumWave
        
        if(strlen(ListMatch(cellSum.passiveSeriesWaves, paramWN))>0)
            Wave/SDFR=cellDFR timeWave = passStartTimesW
            duplicate/O timeWave, cellSumDFR:cellSumTime/Wave=timeWave
        else
            Wave/SDFR=cellDFR timeWave = startTimesW
            duplicate/O timeWave, cellSumDFR:cellSumTime/Wave=timeWave
        endif
        plotValByTime(sumWave, timeWave, paramLabel = paramLabel, displayHost = displayHost)
        return 0
    endif
end


///////////
/// showRegionsEventHistoTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-04
/// NOTES: 
///////////
function showRegionsEventHistoTable([string tableName])
    if(paramIsDefault(tableName))
        tableName = "cellHistoTable"
    endif

    if(!winExists(tableName))
        AGG_makeTable(70, 5, 99, 90, "cellHistoTable")
    else
        [string hostN, string subN] = splitFullWindowName(tableName)
        DoWindow/F hostN

        RemoveWavesFromTable_AGG(tableName, "*")
    endif
    
    createEventsCellSumOutDF_smartConc()
    DFREF secondSumDFR = getEventsCellSumOutDF_smartConc() // otherwise waves overwrite in other display

    // Cell histogram
    Wave/SDFR=secondSumDFR eventHisto, eventHistoByFreq
    if(!waveExists(eventHisto) || !waveExists(eventHistoByFreq))
        return NaN
    endif
    
    AppendToTable/W=$tableName eventHisto.x, eventHisto, eventHistoByFreq.d
    ModifyTable/W=$tableName showParts=126
    ModifyTable/W=$tableName title(Point)="bin#"
    ModifyTable/W=$tableName title(eventHisto.x)="binStart"
    ModifyTable/W=$tableName title(eventHistoByFreq.d)="freq(Hz)"
    ModifyTable/W=$tableName title(eventHisto.d)="#events"
    ModifyTable/W=$tableName format(eventHistoByFreq.d)=3, digits(eventHistoByFreq.d)=2
    ModifyTable/W=$tableName autosize={0, 0, 0, 0, 10}
end


function updateCellSumTable(string cellName)
    DFREF panelCellSumDFR = getEventsCellSumOutDF()

    string selRegion = getSelectedItem("regionListBox_cell", hostName = "AGG_events")
    
    string subType = getSelectedDispType()

    Struct outputWaves outputWaves
    StructFill/SDFR=panelCellSumDFR/AC=3 outputWaves
    fillOutWavesStrings(outputWaves)

    STRUCT cellSummaryInfo cellSum
    fillStringsinCellSummaryInfo(cellSum)

    string ansysWaveList = outputWaves.analysis_types
    variable numWaves = itemsInList(ansysWaveList), iWave
    
    for(iWave = 0; iWave<numWaves; iWave++)
        string waveN = stringfromlist(iWave, ansysWaveList)
        string varN = stringbykey(waveN, outputWaves.varFromWave)
    
        if(strlen(waveN)==0)
            continue
        endif
        Wave/SDFR=panelCellSumDFR thisWave = $waveN
        if(!WaveExists(thisWave))
            continue
        endif
        redimension/N=1 thisWave
        thisWave[0]=NaN
        
        if(strlen(varN)==0)
            continue
        endif

        DFREF cellSumDFR = returnCellRegionsDFR_bySubType(cellName, subType, param = waveN, outType = "cellDisplay", regionName = selRegion)

        if(DataFolderRefStatus(cellSumDFR)==0)
            continue
        endif
        NVAR/Z/SDFR=cellSumDFR thisVar = $varN

        if(numtype(thisVar)!=0)
            continue
        endif
        thisWave[0] = thisVar
    endfor
end

///////////
/// recreateTauFitGraph
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Use this function to recreate the graph with tau fits
///////////
function recreateTauFitGraph(string cellName[, string displayGraphName])
    if(paramIsDefault(displayGraphName))
        displayGraphName = "tauFit"
    endif

    string origWaveN = "cellNAvgEvent"

    string selRegion = getSelectedItem("regionListBox_cell", hostName = "AGG_events")
    string subType = getSelectedDispType()
    if(stringmatch(subType, "all") || stringmatch(subType, "interval"))
        subType = "specify" // so if a decision about default changes, only have to change getTypeStorageDF 
    endif

    DFREF cellSummaryDF = returnCellRegionsDFR_bySubType(cellName, subType, param = "tauFitDisplay", outType = "cellDisplay", regionName = selRegion)

    Wave/SDFR=cellSummaryDF origWave = $origWaveN

    string origWaveToPlotN = origWaveN + "_plot"
    Wave/SDFR=cellSummaryDF origWavePlot = $origWaveToPlotN 

    if(!WaveExists(origWave) || !WaveExists(origWavePlot))
        print "recreateTauFitGraph: Couldn't find original wave and/or wave to plot. Quitting"
        abort
    endif

    string smoothStrVarN = origWaveN + "_smthVar"
    string normStrVarN = origWaveN + "_normVar"
    string fitStrVarN = origWaveN + "_fitVar"
    
    SVAR/SDFR=cellSummaryDF smoothStrVar = $(smoothStrVarN), normStrVar = $(normStrVarN),  fitStrVar = $(fitStrVarN)
    Wave/SDFR=cellSummaryDF smoothWave = $smoothStrVar, normWave = $normStrVar, fitWave = $fitStrVar

    variable graphExists = 0
    if(WinType(displayGraphName)!=0) // if it already exists
        // print "Graph already exists"
        graphExists = 1
    endif

    if(graphExists)
        clearDisplay(displayGraphName)
        string hostN, subWinN
        [hostN, subWinN] = splitFullWindowName(displayGraphName)
        DoWindow/F $hostN // should only work if not a subwindow
    else
        AGG_makeDisplay(40, 5, 80, 50, displayGraphName)
    endif
    SetActiveSubWindow $displayGraphName
    appendtograph/W=$(displayGraphName) origWavePlot
    makeColorGrey(origWaveToPlotN, displayName = displayGraphName)

    if(WaveExists(smoothWave))
        appendtograph/W=$(displayGraphName) smoothWave
        makeColorGreen(nameofWave(smoothWave), displayName = displayGraphName)
    endif

    if(WaveExists(normWave))
        appendtograph/W=$(displayGraphName) normWave
        makeColorSky(nameofWave(normWave), displayName = displayGraphName)
    endif

    if(WaveExists(fitWave))
        appendtograph/W=$(displayGraphName) fitWave
        makeColorPretty(nameOfWave(fitWave), displayName = displayGraphName)

        if(WaveExists(normWave))
            sueApprovedYAxis()    
        endif
    endif

end