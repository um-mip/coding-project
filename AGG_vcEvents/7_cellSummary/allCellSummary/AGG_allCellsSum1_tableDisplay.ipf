function updateCellsOutTable([variable doReconcat])
    if(paramIsDefault(doReconcat))
        doReconcat = 0
    endif
    
    clearUnnamedCellRows()

    DFREF panelDFR = getEventsPanelDF()
    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName, groupNames2 = groupName2, groupNames3 = groupName3

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Struct outputWaves outWaves
    StructFill/SDFR=cellsOutDFR outWaves

    redimension/N=0 outWaves.cellName, outWaves.groupName, outWaves.groupName2, outWaves.groupName3, outWaves.duration, outWaves.sumCount, outWaves.sumTime, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outWaves.decay9010, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc, outWaves.decayTimes, outWaves.tauFits, outWaves.y0Fits, outWaves.AFits, outWaves.X0Fits
    
    if(WaveExists(cellNames))
        variable numCells = numpnts(cellNames)
        variable iCell = 0
        variable numAddedCells = 0
        for( iCell=0; iCell<numCells; iCell++ )
            string cellName = cellNames[iCell] // These could be just duplicated directly, but this could be safer, or if only want cells with event folders
            string groupName = groupNames[iCell]
            string groupName2 = groupNames2[iCell]
            string groupName3 = groupNames3[iCell]
            DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
            variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
            if(dfrStatus != 0)
                if(doReconcat)
                    print "reconcatenating", cellName, time()
                    variable timerRef = StartMSTimer
                    // Reconcatenate
                    concatAndCalcAvgForCell(cellName)
                    if(timerRef == -1)
                        print "updateCellsOutTable: All timers are in use"
                    else
                        variable concatTime = StopMSTimer(timerRef)
                        print cellName, "concat time", concatTime / 1e6, "s"
                    endif
                endif

                // redimension
                redimension/N=(numAddedCells+1) outWaves.cellName, outWaves.groupName, outWaves.groupName2, outWaves.groupName3, outWaves.sumCount, outWaves.sumTime, outWaves.duration, outWaves.sumFreq, outWaves.pks, outWaves.int, outWaves.der, outWaves.t50r, outWaves.fwhm, outwaves.decay9010, outWaves.Rinput, outWaves.RseriesSub, outWaves.capa, outWaves.holdingc, outWaves.decayTimes, outWaves.tauFits, outWaves.y0Fits, outWaves.AFits, outWaves.X0Fits
                
                // Save cell and group name to output waves (if not duplicating)
                outWaves.cellName[numAddedCells] = cellName
                outWaves.groupName[numAddedCells] = groupName
                outWaves.groupName2[numAddedCells] = groupName2
                outWaves.groupName3[numAddedCells] = groupName3

                // Update output waves
                // updateOutWaves(cellSumDFR, cellsOutDFR, numAddedCells, passiveDFR = passiveDFR)
                variable success = updateOutWaves("cell", cellName, numAddedCells)
                if(success)
                    // Add one to numAddedCells
                    numAddedCells++
                endif
            endif
        endfor
    else
        print "updateCellsOutTable: cellName wave doesn't exist"
    endif
end

///////////
/// updateAllCellAvgs
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// NOTES: for all cells, update the tables
///////////
function updateAllCellAvgs()
    updateCellsOutTable(doReconcat=1) // reconcatenates and calculates cell averages within 
    string cellName = getSelectedItem("cellListBox_cell", hostName = "AGG_events")
    updateCellSumTable(cellName)
end


///////////
/// getAllEventsInfo
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-03-12
/// NOTES: Get all of the events to export from Igor into R
///////////
function/Wave getAllEventsInfo([variable convertUnits])
    if(paramisdefault(convertUnits))
        DFREF unitsDFR = getEventsUnitsDF()
        if(DataFolderRefStatus(unitsDFR)!=0)
            NVAR/Z/SDFR = unitsDFR globalConvert = convertUnits
            convertUnits = globalConvert
        else
            convertUnits = 0
        endif
    endif
    convertUnits = 1

    DFREF panelDFR = getEventsPanelDF()
    if(dataFolderRefStatus(panelDFR)==0)
        return $("")
    endif

    Wave/SDFR=panelDFR/T cellNames = cellName, groupNames = groupName, groupNames2 = groupName2, groupNames3 = groupName3

    DFREF cellsOutDFR = getAllCellsOutputDF_allEvents()

    if(dataFolderRefStatus(cellsOutDFR)==0)
        createAllCellsOutputDF_allEvents()
        DFREF cellsOutDFR = getAllCellsOutputDF_allEvents()
    endif

    if(dataFolderRefStatus(cellsOutDFR)==0)
        return $("")
    endif

    if(!WaveExists(cellNames))
        return $("")
    endif

    Struct outputWaves outputWaves
    fillOutWavesStrings(outputWaves)
    string analysis_types = outputWaves.allEventsOutputs
    variable iAnalysis = 0
    variable numAnalyses = itemsInList(analysis_types)
    string nameAnalysis = ""
    DFREF indivSeriesOutDFR = getEventsIndivSeriesOutDF()

    for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
        nameAnalysis = StringFromList( iAnalysis, analysis_types )
        if(stringmatch(nameAnalysis, "area"))
            nameAnalysis = "area2"
        endif

        make/N=0/D/O cellsOutDFR:$nameAnalysis
    endfor

    make/N=0/T/O cellsOutDFR:cellID/Wave=cellID
    make/N=0/D/O cellsOutDFR:seriesID/Wave=seriesID

    variable numCells = numpnts(cellNames)
    variable iCell = 0
    variable numAddedCells = 0, addedRow = 0

    for( iCell=0; iCell<numCells; iCell++ )
        string cellName = cellNames[iCell]

        DFREF cellInfoDFR = getEventsCellInfoDF(cellName)
        variable dfrStatus = DataFolderRefStatus(cellInfoDFR)
        if(dfrStatus == 0)
            print "getAllEventsInfo: cell data folder doesn't exist", cellName
            continue
        endif

        DFREF cellSumDFR = getCellSummaryDF(cellName)
        if(dataFolderRefStatus(cellSumDFR)==0)
            print "getAllEventsInfo: cell summary data folder doesn't exist", cellName
            continue
        endif

        Wave/SDFR=cellSumDFR combinedSeriesW
        if(!WaveExists(combinedSeriesW))
            print "getAllEventsInfo: cell combined series wave doesn't exist", cellName
            continue
        endif

        variable numSeries = numpnts(combinedSeriesW)
        variable iSeries = 0, numAddedSeries = 0
        for(iSeries=0; iSeries<numSeries; iSeries++)
            variable seriesNum = combinedSeriesW[iSeries]

            string seriesString = buildSeriesWaveName(cellName, seriesNum)
            string subType = "all"
            DFREF eventsDFR = returnSeriesEventsDFR_bySubType(cellName, num2str(seriesNum), subType)

            if(dataFolderRefStatus(eventsDFR)==0)
                print "updateSeriesEventInfo: Couldn't find DFR where events are supposed to be stored"
                return $("")
            endif

            for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
                nameAnalysis = StringFromList( iAnalysis, analysis_types )

                string seriesAnalysisWN = seriesString + "_" + nameAnalysis

                if(stringmatch(nameAnalysis, "area"))
                    nameAnalysis = "area2"
                endif

                Wave analysisWave = eventsDFR:$seriesAnalysisWN

                if(!WaveExists(analysisWave))
                    continue
                endif

                DFREF dupDFR = getAllCellsOutputDF_allEvents_dup()
                if(dataFolderRefStatus(dupDFR)==0)
                    createAllCellsOutputDF_allEvents_dup()
                    DFREF dupDFR = getAllCellsOutputDF_allEvents_dup()
                endif

                duplicate/O analysisWave, dupDFR:$nameAnalysis
                Wave/SDFR = dupDFR thisWave = $nameAnalysis
                if(convertUnits == 1)
                    Wave convert = convertWaveUnits(thisWave, "fromBase")
                    thisWave[] = convert[p]
                endif

                variable numEventsInSeries = numpnts(thisWave)

                Wave/SDFR=cellsOutDFR outputWave = $nameAnalysis

                concatenate/NP {thisWave}, outputWave
                killwaves/Z thisWave
            endfor

            make/FREE/T/O/N=(numEventsInSeries) seriesCellID = cellName
            make/FREE/D/O/N=(numEventsInSeries) seriesSeriesID = seriesNum

            concatenate/NP/T {seriesCellID}, cellID
            concatenate/NP {seriesSeriesID}, seriesID
        endfor
    endfor

    string outTable = "allEventsTable"
    if(!winExists(outTable))
        AGG_makeTable(0, 0, 60, 60, "allEventsTable")

        AppendToTable/W=$outTable cellID, seriesID

        for( iAnalysis = 0; iAnalysis < numAnalyses; iAnalysis++ )
            nameAnalysis = StringFromList( iAnalysis, analysis_types )
            if(stringmatch(nameAnalysis, "area"))
                nameAnalysis = "area2"
            endif

            AppendToTable/W=$outTable cellsOutDFR:$(nameAnalysis)
        endfor
    endif
    DoWindow/F $outTable

    print "use saveTable(\"allEventsTable\") to save this table"
end