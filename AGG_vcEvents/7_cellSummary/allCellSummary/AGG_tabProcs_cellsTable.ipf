function updateCellsOutProc(B_Struct): ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        variable doReconcat = NumberByKey("concat", B_Struct.userdata)
        if(numtype(doReconcat)==0 && doReconcat)
            updateCellsOutTable(doReconcat = 1)
        else
            updateCellsOutTable(doReconcat = 0)
        endif
    endif
end

///////////
/// selectedRegionListProc_all
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-14
/// NOTES: 
///////////
function selectedRegionListProc_all(LB_Struct) : ListBoxControl
    STRUCT WMListboxAction & LB_Struct
    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        updateGroupGraphs() // this updates the cellsOutTable as part of the function
        displayGroupParams()
    endif
    return 0
end
