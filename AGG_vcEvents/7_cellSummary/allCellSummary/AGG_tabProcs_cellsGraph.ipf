// Updated by updateGroupGraphProc - shared list box

function updateGroupEventsProc(B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        string uData = B_Struct.userdata
        variable avgByEvent = NumberByKey("avgByEvent", uData)
        if(numtype(avgByEvent)!=0)
            avgByEvent = 1 // default to average by event
        endif
        plotAvgEventForGroups(avgByEvent = avgByEvent)
    endif
end