macro inactCaAnalysis(inactSweep)
variable inactSweep
variable tstart = 0.3715, tend = 0.4715 
variable nsmooth = 25
// subtract trace
string wl = tracenamelist( "", ";", 1 ) // wavelist of raw data
string subwl = subtracesPanel( inactSweep, wl )
string smth_wl = smoothmywaves( subwl, nsmooth )
nsmooth = 0 // presmoothing, turning off smooth in smartpeak2
displaywavelist2( smth_wl )
string suffix = "_iPk" + num2str(inactsweep)
string analysisWaves = smartPeak2( smth_wl, tstart, tend, nsmooth, suffix)
// display sub traces
// display inact curve
endmacro 

macro subtractseries( datecode, series1, series2 )
// subtracts series 2 from series 1
	string datecode = "12092022d"
	variable series1 = 4, series2 = 6
	variable tstart = 0.345, tend = 0.605
	string sublist = subsweeps( datecode, series1, series2 )
	displaywavelist2( sublist )
	rainbow()
	SetAxis bottom tstart, tend
endmacro

// subtract a set of sweeps from another set of sweeps
function/s subsweeps(datecode, s1, s2) // subtract s2 from s1, sweep by sweep
	string datecode
	variable s1, s2 
	variable nsmooth = 25
	
	string wn1, wn2 
	wn1 = datecode + "g1s" + num2str(s1) + "sw1t1"
	wn2 = datecode + "g1s" + num2str(s2) + "sw1t1"
	string s1list = sweepsfromseries(wn1)
	string s2list = sweepsfromseries(wn2)

	//smooth
	string smth1list = smoothmywaves( s1list, nsmooth )
	string smth2list = smoothmywaves( s2list, nsmooth )

	// match and sub
	string subtractList = matchandsublist( 0, smth1list, smth2list )
	return subtractlist
end 

macro smoothing(nsmooth)

variable nsmooth //= 100
// subtract trace
string wl = tracenamelist( "", ";", 1 ) // wavelist of raw data
string smth_wl = smoothmywaves( wl, nsmooth )

endmacro 

function/S smoothmywaves( wl, nsmooth )
	string wl // wavelist to be smotthed
	variable nsmooth // box car smooth points
	variable i,n = itemsinlist(wl)
	string wn1, wn2, smoothwl, suffix = "_smth"+num2str(nsmooth)
	string outwl =""
	display/k=1
	for(i=0;i<n;i+=1)
		wn1 = removequotes(stringfromlist(i,wl) )
		wn2 = wn1 + suffix 
		duplicate/O $wn1, $wn2
		//WAVE w1 = $wn1 
		WAVE w2 = $wn2 
		smooth/B nsmooth, w2 
		appendtograph w2 
		outwl += wn2 + ";"
	endfor 
	return outwl
end

// calcium current analysis macros
macro CaCurrentBSL( bstart, bend, tstart, tend, nsmooth )
variable bstart = 0
variable bend = 0.1 
variable tstart = 2.1005 // sec
variable tend = 2.35 // sec 
variable nsmooth = 10
string suffix = "_CaPk"

string tl = tracenamelist("",";",1), tlBSL = ""

tlBSL = adjbaseWL( tl, bstart, bend, "b" )

string analysisWaves = smartPeak2( tlBSL, tstart, tend, nsmooth, suffix )
// analysis waves lists the output waves of smartpeak
// these are peaks and peaks timing
// note that smartPeak assumes the baseline is corrected, 
// peaks are reported relative to zero current
string CaPeaks = stringbykey("peak",analysisWaves)
string CaPeaksTimes = stringbykey("timing",analysisWaves)
print CaPeaks, CaPeaksTimes
edit/k=1 $CaPeaks, $CaPeaksTimes

endmacro

// calcium current analysis macros
macro CaCurrentAnalysisTopGraph( tstart, tend, nsmooth )
variable tstart = 1.101 // sec
variable tend = 1.3 // sec 
variable nsmooth = 10
string suffix = "_CaPk"

string tl = tracenamelist("",";",1)

string analysisWaves = smartPeak2( tl, tstart, tend, nsmooth, suffix )
// analysis waves lists the output waves of smartpeak
// these are peaks and peaks timing
// note that smartPeak assumes the baseline is corrected, 
// peaks are reported relative to zero current
string CaPeaks = stringbykey("peak",analysisWaves)
string CaPeaksTimes = stringbykey("timing",analysisWaves)
print CaPeaks, CaPeaksTimes
edit/k=1 $CaPeaks, $CaPeaksTimes

endmacro

/////////////////////////////////
/////////////////////////////////

// measure peak from waves in wavelist wl, in range tstart to tend


// returns wavenames containing the peak values and times for all waves in wavelist (wl)
// takes the peak as whichever is greater, min or max in the range between tstart and tend
// smartPeak doesn't test for significant difference between baseline and peak

/////////////////////////////////
function/S smartPeak2( wl, tstart, tend, nsmth, suffix, [do_avg, order ] )
string wl // wavelist to analyze
variable tstart, tend, nsmth // time range to analyze waves
string suffix, do_avg //, do_tau // codename to add at end of wavename // do_avg returns average of range
variable order // set to 1 if you want to search forwards from first sweep (i.e. first sweep has biggest peak)
//variable posneg // +1 for pos (default) , -1 for negative peak, 0 for smart

variable direction = -1
if(paramisdefault(order))
	// go backwards through the data, assuming biggest peak is at the end therefore analyze first
else
	if( abs( order ) == 1 )
		direction = order
	endif
endif

variable iwave = 0, nwaves = itemsinlist( wl )
string wn = removequotes(stringfromlist( iwave, wl))

string out = wn + num2str(tstart) + suffix // datecodeGREP2(wn)+ "s" + num2str( seriesnumber( wn ) ) + suffix
string out_timing = out + "_timing"
string outstring = "peak:" + out + ";" + "timing:" + out_timing + ";"

// hard coding Vm
variable stepstart = -70
variable stepint = 10

make/O/N=(nwaves) $out, $out_timing
WAVE o = $out
o = NaN
setscale/P x, stepstart, stepint, o 

WAVE ot = $out_timing
ot = NaN
// save timing


variable V_FitError = 0, V_fitquitreason = 0
variable noise=0, peak=0, loc = 0, avg = 0, it = 0, ravg = 0 //range average
variable dt = 0.002
variable prev_loc =0
string tmp="temporary", twn=""

// reversing to use last loc for average
if( direction == 1 )
	iwave = 0
else
	iwave = nwaves - 1
endif

variable minpeak, maxpeak, minloc, maxloc
do

	wn = removequotes( stringfromlist( iwave, wl ) )
	WAVE/Z temp = $wn

//	duplicate/O temp, $tmp
//	adjustbasevar( tstart-0.005, tstart, tmp )

	twn  = datecodeGREP2( wn ) + "s" + num2str( seriesnumber( wn ) ) + "sw" + num2str( sweepnumber( wn ) )+"t"
	duplicate/O/R=( tstart, tend ) temp, $twn
	WAVE/Z w = $twn

	smooth/B nsmth, w
	wavestats /Q /Z  w

	if( ParamIsDefault( do_avg ) )

		maxpeak = V_max
		maxloc = V_maxloc
		minpeak = V_min
		minloc = V_minloc 

		if( abs(maxpeak) > abs( minpeak ) )
			peak = maxpeak
			loc = maxloc
		else
			peak = minpeak
			loc = minloc
		endif 

		it = peak
		print it, loc
	else
		it = V_avg
		loc = tstart
	endif

	//clean up
	WAVE/Z w=$""

	o[ iwave ] = it //V_max // assumes positive peak, no smoothing
	ot[ iwave ] = loc

	iwave +=  direction
while( ( direction -1 ) ? (iwave >= 0) : (iwave < nwaves ) )

display/k=1 o

return outstring

end  // smartpeak