#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later

macro testdist2m(a,b)//nnums,mn,sd,appnd)
variable a=2,b=2
variable sd = 4
variable lambda =2, pamp =10
variable nnums = 20000 // original sample size
variable mn = 1

variable appnd = 0 // append to existing graphs
variable dec = 10
variable nreps = 3

testdist2(nnums,mn,a,b,appnd,dec,nreps)
end

function testdist2(nnums,mn,a,b,appnd,dec,nreps) //,lambda, pamp)
variable nnums// = 100000 // original sample size
variable mn// = 10
variable a, b // sd// = 1
variable appnd// = 0 // append to existing graphs
variable dec// = 10
variable nreps// = 4

variable lambda, pamp 

string basewn, wn, dwn, hwn, bwn
string basewn1, wn1, dwn1, hwn1
string basewn1s, wn1s, dwn1s, hwn1s

basewn = "w" + num2str(nnums)
bwn = GammaRNDnums( basewn, nnums, mn, a, b )
probdistP( bwn, sign(mn) )
WAVE bw = $bwn
dwn = basewn + "_dist"
WAVE dw = $dwn
hwn = basewn + "_hist"
make/O $hwn
histogram $basewn, $hwn
WAVE hw = $hwn
hw/=nnums

//display/N=dist/k=1/VERT dw 
//display/N=histo/k=1 hw

// now decimate
variable i
for(i=0;i<nreps;i+=1)
	nnums/=dec
	basewn1 = "w" + num2str(nnums)
	make/O/N=(nnums) $basewn1
	WAVE bw1 = $basewn1
	WAVE bw = $basewn
	bw1 = bw[p*dec]
	probdistP( basewn1, sign(mn) )
	dwn1 = basewn1 + "_dist"
	hwn1 = basewn1 + "_hist"
	make/O $hwn1
	histogram $basewn1, $hwn1
	WAVE hw1 = $hwn1
	hw1 /= nnums
		
	basewn1s = "w" + num2str(nnums) + "s"
	make/O/N=(nnums) $basewn1s
	WAVE bw1s = $basewn1s
	WAVE dw = $dwn
	bw1s = dw[p*dec]
	probdistP( basewn1s, sign(mn) )
	dwn1s = basewn1s + "_dist"
	hwn1s = basewn1s + "_hist"
	make/O $hwn1s
	histogram $basewn1s, $hwn1s
	WAVE hw1s = $hwn1s
	hw1s /= nnums	

	// reset names so decimate is iterative
	basewn = basewn1
	dwn = dwn1

//	appendtograph/W=dist/VERT $dwn1, $dwn1s
//	ModifyGraph/W=dist lstyle($dwn1s)=3, rgb($dwn1)=(0,0,0),rgb($dwn1s)=(0,0,0)
//	appendtograph/W=histo hw1, hw1s
//	ModifyGraph/W=histo lstyle($hwn1s)=3, rgb($hwn1)=(0,0,0),rgb($hwn1s)=(0,0,0)
endfor


end


macro testdist_AGG(nnums,mn,sd,appnd)
variable nnums = 100000 // original sample size
variable mn = 10
variable sd = 1
variable appnd = 0 // append to existing graphs
variable dec = 10
variable nreps = 4

string basewn, wn, hwn
string basewn1, wn1, hwn1
string basewn2, wn2, hwn2
string basewn3, wn3, hwn3
string basewn4, wn4, hwn4

string wdist = basewn + "_dist"

basewn = "w" + num2str(nnums)
wn = rndnums(basewn, nnums, mn, sd)
probdistP( wn, sign(mn) )
hwn = basewn + "_hist"
make/O $hwn
histogram $wn, $hwn
$hwn/=nnums

nnums/=dec
basewn1 = "w" + num2str(nnums)
basewn1s = "w" + num2str(nnums) + "s"
make/O/N=(nnums) $basewn1
$basewn1 = $basewn[p*dec]
make/O/N=(nnums) $basewn1s

$basewn1s = $basewn[p*dec]
probdistP( basewn1, sign(mn) )
hwn1 = basewn1 + "_hist"
make/O $hwn1
histogram $basewn1, $hwn1
$hwn1/=nnums

nnums/=dec
basewn2 = "w" + num2str(nnums)
make/O/N=(nnums) $basewn2
$basewn2 = $basewn1[p*dec]
probdistP( basewn2, sign(mn) )
hwn2 = basewn2 + "_hist"
make/O $hwn2
histogram $basewn2, $hwn2
$hwn2/=nnums

nnums/=dec
basewn3 = "w" + num2str(nnums)
make/O/N=(nnums) $basewn3
$basewn3 = $basewn2[p*dec]
probdistP( basewn3, sign(mn) )
hwn3 = basewn3 + "_hist"
make/O $hwn3
histogram $basewn3, $hwn3
$hwn3/=nnums


nnums/=dec
basewn4 = "w" + num2str(nnums)
make/O/N=(nnums) $basewn4
$basewn4 = $basewn3[p*dec]
probdistP( basewn4, sign(mn) )
hwn4 = basewn4 + "_hist"
make/O $hwn4
histogram $basewn4, $hwn4
$hwn4/=nnums

//WAVE w = $wn

if(!appnd)

//display/N=points/k=1 $basewn,$basewn1,$basewn2,$basewn3,$basewn4
display/N=dist/k=1/VERT $(basewn+"_dist"),$(basewn1+"_dist"),$(basewn2+"_dist"),$(basewn3+"_dist"),$(basewn4+"_dist")
display/N=histo/k=1 $hwn,$hwn1,$hwn2,$hwn3,$hwn4

else
appendtograph/W=points $wn
appendtograph/W=dist/VERT $(wn+"_dist")
appendtograph/W=histo $hwn
endif
end

function/S Grndnums(wn,nnums,mn,sd)
string wn
variable nnums // number of random numbers
variable mn, sd // mean and sd of gaussian
variable i
make/O/N=(nnums) $wn
WAVE w = $wn
for( i=0; i<nnums; i+=1 )
	w[ i ] = gnoise( sd ) + mn
endfor
return wn
end

function/S GPrndnums(wn,nnums,mn,sd, lambda, pamp)
string wn
variable nnums // number of random numbers
variable mn, sd // here sd is lambda! mean and sd of gaussian
variable lambda,pamp
variable i
make/O/N=(nnums) $wn
WAVE w = $wn
for( i=0; i<nnums; i+=1 )
	if(pamp!=0)
		w[ i ] = gnoise(sd) * pamp * poissonNoise( lambda ) + mn
	else
		w[ i ] = gnoise(sd) + mn
	endif
endfor
return wn
end

function/S Prndnums(wn,nnums,mn,sd)
string wn
variable nnums // number of random numbers
variable mn, sd // here sd is lambda! mean and sd of gaussian
variable i
make/O/N=(nnums) $wn
WAVE w = $wn
for( i=0; i<nnums; i+=1 )
	w[ i ] = poissonNoise( sd ) + mn
endfor
return wn
end

function/S GammaRNDnums(wn,nnums,mn, a,b)
string wn
variable nnums // number of random numbers
variable mn, a,b // mean and sd of gaussian
variable i
make/O/N=(nnums) $wn
WAVE w = $wn
for( i=0; i<nnums; i+=1 )
	w[ i ] = gammaNoise( a,b ) + mn
endfor
return wn
end