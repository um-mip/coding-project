function graphx(  )
variable x=0 // index of the wave(s) 0-9 i think
variable i=0, n=9
string w1,w2,w3
for ( i=0; i<=n; i+=1 ) 
	w1 = "wave" + num2str(i+x)
	w2 = "wave" + num2str(i+x+10)
	w3 = "wave" + num2str(i+x+20)
	display/k=1 $w2, $w1, $w3
	rainbow()
endfor
endmacro
// macro runkonn( xwn, ywn, t0, t1, t2)
// 	string xwn = "tw_20211119_6" //xwavename( "", ywn )
// 	string ywn = "test" // stringfromlist( 0, wavel )
// 	variable t0=0.2, t1=0.75, t2= 3
// 	konn( xwn, ywn, t0, t1, t2 )
// end

macro TESTrunkonnTL( t0, t1, t2, t4)
	variable t0=0.2, t1=0.5, t2= 500, t4=1
if(t4 == 0)	
	konnTL( t0, t1, t2, test = "z" )
else
	konnTL( t0, t1, t2, test = "z", t4=t4 )
endif
end

macro runkonnTL( t0, t1, t2)
	variable t0=0.2, t1=1, t2= 500
	konnTL( t0, t1, t2 )
end

macro dFoF0_td( t0, t1, t2, t4)
	variable t0=0.2, t1=0.5, t2= 500, t4=1
	konnTL( t0, t1, t2, t4=t4 )
end


// handles background subtraction (this is not F0)
// assumes data are plotted against an x-wave!
// assumes one ROI has background signal
// returns wavelist
function backSubTL( backsubROI, [autoscale, autooffset, wlist] ) //TOP GRAPH!
	variable backsubROI // ROI to subtract, must be >1
	variable autoscale // set to 0 for no scale, set to 1 for auto, set to scale value for fixed scaling
	variable autooffset // set to 0 for no offset, set to 1 for auto, set to offset value for fixed offset
	string wlist // optional wavelist overides tracelist
	string outlist = ""

	string xwn, ywn 
	string wavel = tracenamelist("", ";", 1); //wavelist("*", ";", "WIN:" + mytable ) //
	if( !paramisdefault(wlist) )
		wavel = wlist 
	endif

	string regexp = "([[:digit:]]+)([[:alpha:]])_r([[:digit:]]+)"
	string yearmonday="", letter="", roi=""


	ywn = removequotes( stringfromlist( 0, wavel ) )
	xwn = xwavename( "", ywn )
	variable i, n=itemsinlist(wavel)

	// find the roi for background subtraction
	string backsubwn = "" 
	for( i=0; i<n; i+=1 )
		ywn = removequotes( stringfromlist( i, wavel ) )
		splitstring/E=(regexp) ywn, yearmonday, letter, roi 
		if( str2num(roi) == backsubROI )
			backsubwn = ywn 
			i=inf 
		endif 
	endfor 
	WAVE backsubw = $backsubwn 

	//edit/k=1
	string graphname = "bs_" + ywn + "0"
	string ext = "_bs" + roi 
	display/k=1/N=$graphname
	string new_wn, wn, rwn  

	for( i=0; i<n; i+=1 )
		ywn = removequotes( stringfromlist( i, wavel ) )
		wn = ywn + ext 
		WAVE yw = $ywn 
		duplicate/O yw, $wn
		WAVE w = $wn 
		// autoscale
		// autooffset
		w -= backsubw  

		appendtograph/W=$graphname w vs $xwn
		//ModifyGraph/W=$graphname zColor($new_wn)={$new_wn,-0.002,-0.002,Grays,0},zColorMax($new_wn)=(0,0,0),zColorMin($new_wn)=(65535,0,0)
		ModifyGraph/W=$graphname manTick(bottom)={0,60,0,0},manMinor(bottom)={0,50}
		Label/W=$graphname bottom "time (s)";DelayUpdate
		Label/W=$graphname left "F - back (arb units)";DelayUpdate	
	endfor 
	//rainbow() 
	if(n>3)
		dowindow/f $graphname
		waterfall(0.05)
	endif
end


// konnerth dF/F0, uses waves in the top graph and their X-wave
// doesn't work if X wave is not used to make graph
function konnTL( t0, t1, t2, [ t4, test] ) //TOP GRAPH!
	variable t0, t1, t2 //=0.2, t1=0.75, t2= 3
	string test // if set makes graph of each step
	variable t4 // threshold for prominent event

	string xwn, ywn 
	string wavel = tracenamelist("", ";", 1); //wavelist("*", ";", "WIN:" + mytable ) //

	ywn = removequotes( stringfromlist( 0, wavel ) )
	xwn = xwavename( "", ywn )
	variable i, n=itemsinlist(wavel)
	string new_wn, wn, rwn  
	//edit/k=1
	string graphname = "k" + ywn + "0"
	display/k=1/N=$graphname
	make/O M_WaveStats
	for( i=0; i<n; i+=1 )
		ywn = removequotes( stringfromlist( i, wavel ) )
		if (!paramisdefault(test))
			wn = konn( xwn, ywn, t0, t1, t2, t4 = t4, test=test ) //, "test" )
		else
			if(!paramisdefault(t4))
				wn = konn( xwn, ywn, t0, t1, t2, t4 = t4 ) //, "test" )
			else
				wn = konn( xwn, ywn, t0, t1, t2 ) //, "test" )
			endif
		endif 

		new_wn = removequotes(wn) //+ "_" + num2str(t1)
		WAVE nw = $new_wn 
		nw[ numpnts(nw)-1 ] = 0 // clean up end effect
		appendtograph/W=$graphname nw 
		ModifyGraph/W=$graphname zColor($new_wn)={$new_wn,-0.002,-0.002,Grays,0},zColorMax($new_wn)=(0,0,0),zColorMin($new_wn)=(65535,0,0)
		ModifyGraph/W=$graphname manTick(bottom)={0,60,0,0},manMinor(bottom)={0,50}
		Label/W=$graphname bottom "time (s)";DelayUpdate
		Label/W=$graphname left "( F - F0 ) / F0";DelayUpdate	
	endfor 
	//rainbow() 
	if(n>3)
		dowindow/f $graphname
		waterfall(0.5)
	endif
end


function areaBinningTL( bindur ) //, t1, t2, [test]) //TOP GRAPH!
	variable bindur

	string xwn, ywn 
	string wavel = tracenamelist("", ";", 1); //wavelist("*", ";", "WIN:" + mytable ) //

	ywn = removequotes( stringfromlist( 0, wavel ) )
	//xwn = xwavename( "", ywn )
	variable i, n=itemsinlist(wavel)
	string new_wn, wn, rwn  
	//edit/k=1
	string graphname = "area" + ywn + "0"
	display/k=1/N=$graphname

	ywn = removequotes( stringfromlist( 0, wavel ) )
	WAVE w = $ywn 	
	variable ts, te, tdur, j, nj, dx, t0, t1, nbins 
	ts = leftx( w )
	te = rightx( w )
	nj = numpnts( w )
	dx = deltax( w ) // interval between points
	tdur = te - ts 
	nbins = round( tdur / bindur )
	//debugger
	string ext = "a" + num2str( bindur ), outwn = ywn + ext 
	make/N=(nbins)/O $outwn 
	WAVE binw = $outwn 
	for( i=0; i<n; i+=1 ) // loop over traces

		ywn = removequotes( stringfromlist( i, wavel ) )
		WAVE w = $ywn 
		outwn = ywn + ext 
		make/N=(nbins)/O $outwn 
		WAVE binw = $outwn 
		setscale/P x, ts, bindur, binw // ts is start time, /P means increment by bindur

		// loop over duration of trace
		for( j=0; j < nbins; j += 1 )
			t0 = ts + j * bindur 
			t1 = t0 + bindur
			binw[j] = area( w, t0, t1 ) 


		endfor
		appendtograph/W=$graphname binw 
		Label/W=$graphname bottom "time (s)";DelayUpdate
		string labelstr = "integrated dF / F0 (" + num2str(bindur) + " s bin)"
		Label/W=$graphname left labelstr;DelayUpdate	
		ModifyGraph/W=$graphname manTick(bottom)={0,60,0,0},manMinor(bottom)={0,50}
	endfor 
end

// macro testkonn( )
// 	string xwn = "tw_20211119_6" //xwavename( "", ywn )
// 	string ywn = "test" // stringfromlist( 0, wavel )
// 	variable t0=0.2, t1=0.75, t2= 3 // konnerth default for 30Hz imaging
// 	variable t1s =0.1 , t1e =2.1, nt=20
// 	varykonn( xwn, ywn, t0, t1, t2, t1s, t1e, nt )
// endmacro 

// macro testvarykonn( )
// 	string xwn = "tw_20211119_6" //xwavename( "", ywn )
// 	string ywn = "test" // stringfromlist( 0, wavel )
// 	variable t0=0.2, t1=0.75, t2= 3 // konnerth default for 30Hz imaging
// 	variable t1s =0.1 , t1e =2.1, nt=20
// 	varykonn( xwn, ywn, t0, t1, t2, t1s, t1e, nt )
// endmacro 

function varykonn( xwn, ywn, t0, t1, t2, t1s, t1e, nt )
	string xwn, ywn
	variable t0, t1, t2, t1s, t1e, nt 

	display/k=1
//	edit/k=1
	variable i, n=nt, dt1 
	string wn, new_wn 
	dt1 = (t1e - t1s) / n 
	for( i= 0; i<n; i+=1 )
		t1 = t1s + i * dt1
		wn = konn( xwn, ywn, t0, t1, t2 )
		new_wn = wn + "_" + num2str(t1)
		WAVE w = $wn
		duplicate/O w, $new_wn 
		WAVE nw = $new_wn 
		appendtograph nw 
		ModifyGraph zColor($new_wn)={$new_wn,-0.002,-0.002,Grays,0},zColorMax($new_wn)=(0,0,0),zColorMin($new_wn)=(65535,0,0)

	endfor 
	rainbow() 
	waterfall(0.05)
end

// konnerth dF over F0
function/s konn( xwn, ywn, t0, t1, t2 [ t4, test ] )
	string xwn, ywn
	variable t0 //= 0.2 // final output noise filter, not implemented
	variable t1 //= 1 // 0.75 // smoothing for generation of F0
	variable t2 //= 3 // time to search backwards for the next minimum, duration of longest event
	variable t4 // new parameter for duration threshold for prominent events, same units as data

	string test // set to make explanatory graphs
	string rawfn = ywn + "_" + "konn" 
	duplicate/O $ywn, rawf 

	WAVE/Z rawfx = $xwn
    if( waveexists( rawfx ) )
	    variable i, n=numpnts(rawf)
	    variable dx = rawfx[1]-rawfx[0]
	    variable xmin = rawfx[0], xmax = rawfx[ n-1 ]
		setscale/P x, xmin, dx, rawf // use approximate dt to set wave scaling
	endif
// \\ // \\ heavy lifting here \/ \/ \/
	string barfwn = barfofx( "rawf", t1 ) 

	string barfwn2 = rawfn + "_barf" 
	duplicate/O $barfwn, $barfwn2 // store it for display

// \\ // \\ heavy lifting here \/ \/ \/
	string minbarfwn = ""
	if( paramisdefault( t4 ) )
		minbarfwn = minbarf( barfwn, t2 )
	else 
		minbarfwn = slopebarF( barfwn, t2, t4 )
	endif

	string F0wn = rawfn + "_F0"
	duplicate/O $minbarfwn, $F0wn // unique name to store it for display
  	
  	WAVE F0 = $F0wn 
  	duplicate/O F0, F0w 
	string righteousfn = ywn + "_dFoverF0"
	duplicate/O rawf, $righteousfn
	WAVE righteousf = $righteousfn

// \\ // \\ heavy lifting here \/ \/ \/
	righteousf = (rawf - F0) / F0

	duplicate/O righteousf, dfof0
	//righteousf[ n-1 ] = 0 // termination is a problem
	if( !paramisdefault( test ) )
		string graphname = test + "_" + num2str(i)  
		display/R/k=1/N=$graphname dfof0 
		appendtograph/L/W=$graphname rawf, barf, f0  
		ModifyGraph/W=$graphname rgb(barf)=(0,0,65535)
		ModifyGraph/W=$graphname rgb(rawf)=(0,65535,0)
		ModifyGraph/W=$graphname rgb(dfof0)=(0,0,0)

		ModifyGraph/W=$graphname lsize(rawf)=2
		ModifyGraph/W=$graphname axisEnab(left)={0.5,1},axisEnab(right)={0,0.5}
		Label/W=$graphname left "raw fluorescence (arbitrary units)";DelayUpdate
		Label/W=$graphname bottom "time (s)";DelayUpdate
		Label/W=$graphname right "dF / F0 (fraction)";DelayUpdate
		string str = "\\s(rawf) raw trace (rawf)\r\\s(barf) Konnerth preF0 ( t1 = " + num2str(t1) + "s )\r\\s(" +f0wn+ ") Konnerth F0 ( t2 = " + num2str(t2) + "s )"
		Legend/W=$graphname/C/N=text0/J/F=0/A=RT str;DelayUpdate
		AppendText/W=$graphname/N=text0 "\\s(dfof0) dF / F0 = ( raw - F0 ) / F0"

	endif
	return righteousfn 
end

function/s retromin( xwn, ywn, t0, t1, t2 ) // dF over F0 using backwards minimum
	string xwn, ywn
	variable t0 //= 0.2
	variable t1 //= 1 // 0.75
	variable t2 //= 3 
	duplicate/O $ywn, rawf 
	WAVE/Z rawfx = $xwn
  variable i, n=numpnts(rawf)
  variable dx = rawfx[1]-rawfx[0]
  variable xmin = rawfx[0], xmax = rawfx[ n-1 ]
	setscale/P x, xmin, dx, rawf // use approximate dt to set wave scaling
	//duplicate/O rawf, barf 
	//string barfwn = barfofx( "rawf", t1 )
	//WAVE barf = $barfwn
	string F0wn = retrominF( "rawf", t2 )
	WAVE F0 = $F0wn
	string deltaF = ywn + "_RdF" 
	duplicate/O rawf, $deltaF
	WAVE dF = $deltaF 
	dF = rawf - F0 
	string dFoverF0 = ywn + "_RdFoF0" 
	duplicate/O rawf, $dFoverF0 
	WAVE dFoF0 = $dFoverF0 
	dFoF0 = dF / F0
	//righteousf[ n-1 ] = 0 // termination is a problem
	return dFoverF0
end

// implement konnerth rolling baseline; 
// Jia nat prot box 1 doi:10.1038/nprot.2010.169
// function/s konnerth_dFoverF0( f, t0, t1, t2 )
// 	wave f // this is the trace
	

// end // konnerth_dFoverF0

function/s barFofx( fwn, t1 ) // returns bar F(x)
	//variable x // essentially time
	string fwn 
	variable t1 // tau1 from konnerth
	
	wave f = $fwn  
	duplicate/O f, barf 
	variable absmin = wavemin( f )
	variable i, n=numpnts(f), dt = deltax(f), xmin = leftx(f), xmax = rightx(f)
	variable xinf, x0, x1  
	// integrate f from x - t1/2 to x + t1/2
	//integrate f /D=intf
	for( i=0; i<n; i+=1 )
		xinf = xmin + i * dt
		//x0 = ( xinf > (t1/2) ) ? xinf-(t1/2) : xmin
		x0 = xinf - ( t1 / 2 )
		//x1 = ( xinf < (xmax - t1/2) ) ? xinf+t1/2 : xmax
		x1 = xinf + t1 / 2
		//duplicate/O/R=(x0,x1) intf, subintf 
		if ( ( x0 > xmin ) && ( x1 < xmax ) )
//			barF[i] = (0.5*dt/t1) * sum( f, x0, x1  )//subintf )
			barF[i] = (1/t1) * area( f, x0, x1  )//subintf )
		else 
			barF[i] = f[i]
		endif
		if( barF[i] < absmin )
			barF[i] = f[i]
		endif
	endfor 
	return "barF" 
end 

function/s minbarf( barfwn, t2 )
	string barfwn
	variable t2 

	wave/z f = $barfwn 
	variable absmin = abs( wavemin( f ) )
	wavestats/Q f 
	//print barfwn, "min: ", V_min, V_minloc
	duplicate/O f, minbarfw 
	minbarfw = 0 // minbarfw(inf)
	variable i, n=numpnts(f), dt = deltax(f), xmin = leftx(f), xmax = rightx(f)
	variable xinf, x0, x1  
	for( i=0; i<n; i+=1 )
		xinf = xmin + i*dt
		x0 = ( xinf > (t2) ) ? (xinf-t2) : xmin
		x1 = xinf
		//duplicate/O/R=(x0,x1) intf, subintf 
		minbarFw[i] = wavemin( f, x0, x1 )
	endfor 
	wavestats/Q minbarfw 
	//print "after: min: ", V_min, V_minloc
	variable absmin2 = abs( wavemin( minbarfw ) )
	//if( absmin2 < absmin )
		//print "abnormality in minbarf: premin: ", absmin, "postmin:", absmin2
	//endif
	return "minbarfw"
end

////////////////
//
// SLOPE BAR F
//
////////////////
function/s slopeBarF( barfwn, t2, t4 ) // upgrade from flat to slope correction
	string barfwn
	variable t2 // konnerth backwards window search
	variable t4 // defazio duartion threshold for large event

	wave/z f = $barfwn 
	variable absmin = abs( wavemin( f ) )
	wavestats/Q f 
	//print barfwn, "min: ", V_min, V_minloc
	duplicate/O f, minbarfw 
	minbarfw = 0 // minbarfw(inf)
	variable i, n=numpnts(f), dt = deltax(f), xmin = leftx(f), xmax = rightx(f)
	variable xinf, x0, x1, dx=inf, minflag = 0, prevmin = inf, prevminloc = inf, xstart = nan, xend = nan
	variable fmin = inf, fminloc = inf, fminloci, prevminloci=n-1
	variable slope_dx, slope_dy, slope 
	for( i=(n-1); i>=0; i-=1 )
		x1 = pnt2x( f, i )
		xstart = x1 - t2  
		x0 = ( xstart > 0 ) ? xstart : xmin 
		//duplicate/O/R=(x0,x1) intf, subintf 
		//minbarFw[i] = wavemin( f, x0, x1 )
		wavestats /Q/R=(x1,x0) f 
		fminloci = V_minRowLoc
		fminloc = V_minloc
		fmin = V_min 
		if(( numtype(fmin) != 0 )||(fmin==0))
			fmin = f[i]
			fminloc = x1 
			fminloci = i
		endif
		dx = x1 - fminloc
		minbarFw[fminloci, prevminloci] = fmin // temporarily fill with flat
		if( ( dx > t4 ) && ( fmin != prevmin ) ) // is the event legitimate or just a little thing?
			if( minflag )
				// fix the flat
				// slope = dx/dy
				slope_dx = prevminloc - fminloc
				slope_dy = prevmin - fmin
				slope = slope_dy / slope_dx 
				minbarfw[ fminloci, prevminloci ] = fmin + (p-fminloci) * dt * slope 
				// move index!!

				//print x0,x1, "legitimate event: prev: ", prevmin, prevminloc, " cur: ", fmin, fminloc, slope_dy, slope_dx, slope
			else 
			// handle first min
				//print i, " slopebarf: first min: ", fmin, fminloci 
				minbarFw[fminloci, prevminloci] = fmin 
				minflag = 1 
			endif

		else
			//print i, " slopebarf: dx (",dx,") < t4 (", t4,") or fmin = prevmin! fmin, prevmin:", fmin, prevmin, fminloci, prevminloci, x0
			if( x0 == xmin )
				fminloci = xmin 
			endif
			minbarfw[ fminloci, prevminloci ] = fmin
		endif 
		wavestats/Q/R=(fminloc,prevminloc) minbarfw
		if(V_numINFs >0)
			print "INF! ARG!"
		endif
		prevmin = fmin 
		prevminloc = fminloc 
		prevminloci = fminloci
		i = fminloci - 1
	endfor 
	//handle the first point
	minbarfw[0]=fmin

		wavestats/Q minbarfw
		if(V_numINFs >0)
			print "slopeBarF: after loop! INF! ARG!"
		endif

	//wavestats/Q minbarfw 
	//print "after: min: ", V_min, V_minloc
	//variable absmin2 = abs( wavemin( minbarfw ) )
	//if( absmin2 < absmin )
		//print "slopebarf: abnormality in minbarf: premin: ", absmin, "postmin:", absmin2
	//endif
	//print "slopebarf done!", V_numINFs
	return "minbarfw"
end

function/s retroMinF( barfwn, t2 )
	string barfwn  // assumes setscale of x dimension is appropriate
	variable t2 // units are based on x dimension, usually seconds

	wave/z f = $barfwn 
	string outwn = barfwn + "_retroMin"
	duplicate/O f, $outwn 
	WAVE retrominfw = $outwn  
	variable i, n=numpnts(f), dt = deltax(f), xmin = leftx(f), xmax = rightx(f)
	variable xinf, x0, x1  
	for( i=0; i<n; i+=1 )
		xinf = xmin + i*dt
		x0 = ( xinf > (t2) ) ? (xinf-t2) : xmin
		x1 = xinf
		//duplicate/O/R=(x0,x1) intf, subintf 
		retrominfw[i] = wavemin( f, x0, x1 )
	endfor 
	return outwn 
end

// linear fits to find F0 from raw or background subtracted data
macro getRealF0( xstart, xend )
	variable xstart = 1 // seconds
	variable xend = 6 // seconds

	string wavelist=tracenamelist("",";",1)
	string wavelet=removequotes(stringfromlist(0,wavelist))
	variable nwaves=itemsinlist(wavelist)
	variable iwave, realF0

print "wavename \t y0 \t slope \t average"
	iwave=0
	do
		wavelet=removequotes(stringfromlist(iwave, wavelist))
		if (!stringmatch(wavelet,"f*"))
	//linear		CurveFit/Q/X=1 line $wavelet(xstart,xend) /D // wave7[pcsr(A),pcsr(B)] /D 
CurveFit/Q/X=1/K={0} dblexp_XOffset $wavelet(xstart,xend) /D 
			realF0 = td_dblexp(w_coef,0)
			print wavelet,"\t",realF0, "\t", W_coef[0],"\t",W_coef[1], "\t", W_coef[2],"\t",W_coef[3], "\t", W_coef[4]//,"\t",V_avg
		endif
		iwave+=1
	while (iwave<nwaves)
end










