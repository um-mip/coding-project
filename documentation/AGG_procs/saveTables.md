AGG: Save Tables <!-- omit in toc -->
=====================

**Author:** *Amanda Gibson*

**Updated:** *April 13, 2022*

* [Save tables](#save-tables)
  * [`saveTable`](#savetable)
  * [`saveTableProc`](#savetableproc)

# Save tables

## `saveTable`

### Parameters

* `tableName`: string of table name, including host
* \[optional\] `fileName`: string of file name
    * user will also get a chance to change this when saving

### Purpose

Save an Igor table as a .csv file. Works with standalone tables or tables within a panel. Default file name if not provided is the name of the table

### File

[AGG_saveTables.ipf](../../AGG_procs/tables/AGG_saveTables.ipf)

## `saveTableProc`

### Parameters

* Button structure - passed by Button
    * Should have userdata with a `"tableName"` field containing the name of the table
    * Optional `"tableWindow"` field of user data. If not provided, it will default to looking within the same window as the button
    * Optional `"fileName"` field of user data. Default is table name if not provided

### Purpose

Save a table as a .csv file by pressing a button within a panel

### File

[AGG_saveTables.ipf](../../AGG_procs/tables/AGG_saveTables.ipf)

