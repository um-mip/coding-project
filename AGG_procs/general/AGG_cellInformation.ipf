#pragma rtGlobals=3		//Use modern global access method and strict wave access

///////////////////////////////////////////////////////
/// Pass information about cell and series numbers for waves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: ~ Jan 2022
///////////////////////////////////////////////////////

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-03
/// NOTES: Added an optional traceNum parameter that uses the global defTraceNum to define the
/// trace number that should be used for each series
function/S buildSeriesWaveName(cellName, Events_series[, traceNum])
    string cellName
    variable Events_series, traceNum

    if(paramIsDefault(traceNum))
        // Does the events panel exist?
        DFREF panelDFR = getEventsPanelDF()
        if(dataFolderRefStatus(panelDFR)==0)
            // If not, default to trace 1
            traceNum = 1
        else
            // Does the default trace number variable exist?
            NVAR/Z/SDFR=panelDFR defTraceNum
            if(numtype(defTraceNum)!=0)
                // If not, default to trace 1
                traceNum = 1
            else
                // Use the default trace number as the trace number
                // This should already be checked by checkDefTraceNumProc
                // to be an integer and >=1
                traceNum = defTraceNum
            endif
        endif
    endif
    

    string seriesName = cellName + "g1s" + num2str(Events_series) + "sw1t" + num2str(traceNum)
    return seriesName
end

function/S buildSeriesTraceWaveName(cellName, Events_series, traceNum)
    string cellName
    variable Events_series, traceNum

    string seriesName = cellName + "g1s" + num2str(Events_series) + "sw1t" + num2str(traceNum)
    return seriesName
end

function addCellAndSeriesToWaveNote(Wave seriesWave, string cellName, variable seriesName)
    string currentNote = Note(seriesWave)

    string curCellName = stringbykey("cellName", currentNote)
    string curSeriesName = stringbykey("seriesName", currentNote)
    
    string noteString
    if(strlen(curCellName)==0)
        noteString = ";CellName:" + cellName
        Note seriesWave, noteString
    endif
    if(strlen(curSeriesName)==0)
        noteString = ";SeriesName:" + num2str(seriesName)
        Note seriesWave, noteString
    endif
end

function[string cellName, string seriesName] getCellandSeriesFromWaveNote(Wave thisWave)
    string waveNote = Note(thisWave)
    // print waveNote

    cellName = stringbykey("cellName", waveNote)
    cellName = ReplaceString("\r", cellName, "")
    seriesName = stringbykey("seriesName", waveNote)
    seriesName = ReplaceString("\r", seriesName, "")
    // print "cell", cellName, "series", seriesName
    return [cellName, seriesName]
end

///////////
/// findWavesWithLabel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-13
/// NOTES: Find all waves in root with a label that matches the labelStr
/// Default is to look in root for passive-16swp label
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-05-14
/// NOTES: Changed to make name more generic, from just searching for passive waves
/// and added the option to specify the data folder
///////////
function/S findWavesWithLabel([string labelStr, DFREF dfr])
    if(paramIsDefault(dfr))
        dfr = root:
    endif
    
    if(paramIsDefault(labelStr))
        labelStr = "Passive-16swp"    
    endif

    string objName
    string allWaves = ""
    
	#if (IgorVersion() < 9.00)
        Variable index = 0
        do
            objName = GetIndexedObjNameDFR(dfr, 1, index)
            // print objName
            if (strlen(objName) == 0)
                break
            endif
            allWaves += objName + ";"
            index += 1
        while(1)
    #endif

    // This is WAY faster, as it searches for matching waves with wavelist
    // instead of indexing through all objects
    // Need Igor 9 to be able to specify the folder that should be searched in WaveList
    #if (IgorVersion() >= 9.00)
        allWaves = WaveList("*", ";", "", dfr)
    #endif

    string matchingWaves = ""

    variable iWave = 0, nWaves = itemsinlist(allWaves)
    for(iWave=0; iWave<nWaves; iWave++)
        objName = StringFromList(iWave, allWaves)
        Wave thisWave = dfr:$objName
        if(WaveExists(thisWave))
            string label = getWaveLabel(thisWave)
            if(stringmatch(label, labelStr))
                matchingWaves += objName + ";"
            endif
        else
            print "findWavesWithLabel: can't find the wave anymore"
        endif
    endfor
    return matchingWaves
end


///////////
/// getWaveLabel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-13
/// NOTES: Get the label for a wave
///////////
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-10-27
/// NOTES: check if wave exists
function/S getWaveLabel(Wave waveToCheck)
    string waveLabel = "", waveNote = ""
    if(WaveExists(waveToCheck))
        waveNote = note(waveToCheck)
        waveLabel = stringbykey("LABEL", waveNote)
    endif
    return waveLabel
end

///////////
/// removeWavesWithLabel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-13
/// NOTES: Delete all waves in root that have the given label
/// This doesn't actually _have_ to be a passive label, but 
/// can use this to clean up extraneous waves
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-05-14
/// NOTES: changed name to be more generic - not only passive label
/// Made general "deleteWavesFromList" function to delete the waves
/// generated by "findWavesWithLabel"
///////////
function removeWavesWithLabel([string labelStr, DFREF dfr])
    if(paramIsDefault(dfr))
        dfr = root:
    endif

    if(paramIsDefault(labelStr))
        labelStr = "Passive-16swp"    
    endif

    string matchingWaves = findWavesWithLabel(labelStr = labelStr, dfr = dfr)

    deleteWavesFromList(matchingWaves, dfr = dfr)
end

///////////
/// getYearMonthDayFromCellID
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Modified datecodeGREP2 so that only returns a cellID if found
///////////
function[variable year, variable month, variable day] getYearMonthDayFromCellID(string cellID)
	// debugger
	string regExp="([[:digit:]]+)([[:alpha:]])"
	string datecode, letter
	variable out=0
	splitstring /E=(regExp) cellID, datecode, letter

    if(strlen(dateCode) == 0)
        return [NaN, NaN, NaN]
    endif

    [year, month, day] = getYearMonthDay(dateCode)
    return [year, month, day]
end

///////////
/// getYearMonthDay
/// AUTHOR: 
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Gets the year, month, and day from a date code
/// Pay attention to the search order preference. For ambiguous strings, the first hit will be the one that is returned
/// The top preferred format is YYYYMMDD
///////////
function[variable year, variable month, variable day] getYearMonthDay(string dateCode)
    String yyyyPattern = "^(20[0-3][0-9])(0[1-9]|1[0-2])([0-2][0-9]|3[01])$" // Pattern for YYYYMMDD format assuming a range of years from 2000 to 2039
    String yyPattern = "^([0-9]{2})(0[1-9]|1[0-2])([0-2][0-9]|3[01])$" // Pattern for YYMMDD format
    String yyyPattern = "^([0-9]{3})(0[1-9]|1[0-2])([0-2][0-9]|3[01])$" // Pattern for YYYMMDD format
    String mmddyyyyPattern = "^(0[1-9]|1[0-2])([0-2][0-9]|3[01])(20[0-3][0-9])$" // Pattern for MMDDYYYY format
    String mmddyyPattern = "^(0[1-9]|1[0-2])([0-2][0-9]|3[01])([0-9]{2})$" // Pattern for MMDDYY format
    String ddmmyyyyPattern = "^([0-2][0-9]|3[01])(0[1-9]|1[0-2])(20[0-3][0-9])$" // Pattern for DDMMYYYY format
    String ddmmyyPattern = "^([0-2][0-9]|3[01])(0[1-9]|1[0-2])([0-9]{2})$" // Pattern for DDMMYY format
    String patterns = yyyyPattern + ";" + yyPattern + ";" + yyyPattern + ";" + mmddyyyyPattern + ";" + mmddyyPattern + ";" + ddmmyyyyPattern + ";" + ddmmyyPattern + ";"
    String pattern, dateStr, yearStr, monthStr, dayStr

    variable nPatterns = itemsInList(patterns), iPattern = 0, foundMatch = 0
    for(iPattern=0; iPattern<nPatterns; iPattern++)
        pattern = StringFromList(iPattern, patterns)

        if(GrepString(dateCode, pattern) && !foundMatch)
            foundMatch = 1
            if(iPattern > 0)
                print "getYearMonthDay: match was not with YYYYMMDD pattern, be sure that we've found the correct date"
            endif
            switch (iPattern) // order has to stay the same
                case 0: // YYYYMMDD
                    splitString/E=(pattern) dateCode, yearStr, monthStr, dateStr
                    break
                case 1: // YYMMDD
                    splitString/E=(pattern) dateCode, yearStr, monthStr, dateStr
                    yearStr = "20" + yearStr
                    break
                case 2: // YYYMMDD - treat as error in year
                    continue
                case 3: // MMDDYYYY
                    splitString/E=(pattern) dateCode, monthStr, dateStr, yearStr
                    break
                case 4: // MMDDYY
                    splitString/E=(pattern) dateCode, monthStr, dateStr, yearStr
                    yearStr = "20" + yearStr
                    break
                case 5: // DDMMYYYY
                    splitString/E=(pattern) dateCode, dateStr, monthStr, yearStr
                    break
                case 6: // DDMMYY
                    splitString/E=(pattern) dateCode, dateStr, monthStr, yearStr
                    yearStr = "20" + yearStr
                    break
            endswitch
        endif
    endfor

    if(foundMatch)
        return [str2num(yearStr), str2num(monthStr), str2num(dateStr)]
    else
        return [NaN, NaN, NaN]
    endif
end
