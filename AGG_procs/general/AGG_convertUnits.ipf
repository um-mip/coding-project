#pragma rtGlobals=3		//Use modern global access method and strict wave access

///////////////////////////////////////////////////////
/// CONVERT UNITS FOR EVENT INFORMATION
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: ~ 2022-03-11
/// NOTES: Uses a structure to facilitate the conversion 
/// of waves or variables
/// to and from SI base units
///////////////////////////////////////////////////////

STRUCTURE unitConversions
    NVAR peakTimeU, absPeakU, relPeakU, intervalU, derivativeU, areaU, riseTimeU, decay10to90U, fwhmU , decayTimeU
    NVAR convertUnits
    SVAR uNameFromWave, uNameFromVar
endstructure

function fillUnitConversions(unitStruct)
    STRUCT unitConversions &unitStruct

    unitStruct.convertUnits = 0

    unitStruct.peakTimeU = 1 // s
    unitStruct.absPeakU = 1e-12 //pA
    unitStruct.relPeakU = 1e-12 //pA
    unitStruct.intervalU = 1 // s
    unitStruct.derivativeU = 1e-9 // pA/ms -> 1e-12/1e-3
    unitStruct.areaU = 1e-15 // pA * ms -> 1e-12 * 1e-3
    unitStruct.riseTimeU = 1e-3 // ms
    unitStruct.decay10to90U = 1e-3 // ms
    unitStruct.fwhmU = 1e-3 // ms
    unitStruct.decayTimeU = 1e-3 // ms

    unitStruct.uNameFromWave = "ptb:peakTimeU;"
    unitStruct.uNameFromWave += "pk2:absPeakU;"
    unitStruct.uNameFromWave += "pks:relPeakU;"
    unitStruct.uNameFromWave += "int:intervalU;"
    unitStruct.uNameFromWave += "der:derivativeU;"
    unitStruct.uNameFromWave += "area2:areaU;"
    unitStruct.uNameFromWave += "t50r:riseTimeU;"
    unitStruct.uNameFromWave += "1090d:decay10to90U;"
    unitStruct.uNameFromWave += "decay9010:decay10to90U;"
    unitStruct.uNameFromWave += "fwhm:fwhmU;"
    unitStruct.uNameFromWave += "decayTimes:decayTimeU;"

    unitStruct.uNameFromVar = "relPeak:relPeakU;"
    unitStruct.uNameFromVar += "interval:intervalU;"
    unitStruct.uNameFromVar += "derivative:derivativeU;"
    unitStruct.uNameFromVar += "riseTime:riseTimeU;"
    unitStruct.uNameFromVar += "fwhm:fwhmU;"
    unitStruct.uNameFromVar += "decayTime:decayTimeU;"
    unitStruct.uNameFromVar += "decay9010:decay10to90U;"
    unitStruct.uNameFromVar += "1090d:decay10to90U;"
end

function/WAVE convertWaveUnits(Wave waveToConvert, string direction) // "toBase" -> to base, "fromBase" -> from base
    if(WaveExists(waveToConvert))
        string waveN = nameOfWave(waveToConvert)
        
        DFREF unitsDFR = getEventsUnitsDF() // structure created and filled by events interface
        STRUCT unitConversions units
        StructFill/SDFR = unitsDFR units
        if(strlen(units.uNameFromWave)>0)
            string unitVarN = stringbykey(waveN, units.uNameFromWave)
            if(strlen(unitVarN)==0) // if failed, check the end of the wave name
                string junk, waveNEnd
                SplitString /E=("(_)(.*$)") waveN, junk, waveNEnd
                // string waveNEnd = s_value
                // print waveNEnd
                unitVarN = stringbykey(waveNEnd, units.uNameFromWave)
            endif
            if(strlen(unitVarN)>0)
                // print "trying to convert"
                NVAR/Z/SDFR=unitsDFR unitVar = $unitVarN

                if(StringMatch(direction, "toBase"))
                    if(numpnts(waveToConvert)>0)
                        waveToConvert = waveToConvert * unitVar
                    endif
                endif

                if(StringMatch(direction, "fromBase"))
                    if(numpnts(waveToConvert)>0)
                        waveToConvert = waveToConvert / unitVar
                    endif
                endif
            endif
        endif
    else
        print "wave to convert does not exist", nameOfWave(waveToConvert)
    endif
   return waveToConvert
end

function convertVarUnits(variable numToConvert, string varName, string direction) // "toBase" -> to base, "fromBase" -> from base
    if(numtype(numToConvert)==0)
        DFREF unitsDFR = getEventsUnitsDF() // structure created and filled by events interface
        STRUCT unitConversions units
        StructFill/SDFR = unitsDFR units

        if(strlen(units.uNameFromVar)>0)
            string unitVarN = stringbykey(varName, units.uNameFromVar)
            if(strlen(unitVarN)>0)
                NVAR/Z/SDFR=unitsDFR unitVar = $unitVarN

                if(StringMatch(direction, "toBase"))
                    numToConvert = numToConvert * unitVar
                endif

                if(StringMatch(direction, "fromBase"))
                    numToConvert = numToConvert / unitVar
                endif
            endif
        endif
    else
    endif
    // print numToConvert
    return numToConvert
end

// function testConvert()
//     DFREF unitsDFR = getEventsUnitsDF()
//     NVAR/Z/SDFR=unitsDFR isBase
    
//     DFREF seriesOutDFR = getEventsIndivSeriesOutDF()

//     string analysisWaves = "ptb;pk2;pks;int;der;area2;t50r;1090d;fwhm"
//     variable numAnalyses = itemsinlist(analysisWaves), iAnalysis
//     for(iAnalysis = 0; iAnalysis<numAnalyses;iAnalysis++)
//         string thisAnalysis = stringfromlist(iAnalysis, analysisWaves)
//         string direction
//         if(isBase == 0)
//             direction = "toBase"
//         else
//             if(isBase == 1)
//                 direction = "fromBase"
//             endif
//         endif
//         if(strlen(direction)>0)
//             // print direction
//             Wave/SDFR = seriesOutDFR thisWave = $thisAnalysis
//             if(WaveExists(thisWave))
//                 Wave test = convertWaveUnits(thisWave, direction)
//                 thisWave[] = test[p]
//             endif
//         endif
//     endfor
//     isBase = !isBase
// end