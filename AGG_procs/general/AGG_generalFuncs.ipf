#pragma rtGlobals=3		//Use modern global access method and strict wave access
#include <Waves Average>
#include <AnnotationInfo Procs>

///////////////////////////////////////////////////////
/// GENERAL FUNCTIONS
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: Jan-April 2022
/// NOTES: Miscellaneous general use functions
///////////////////////////////////////////////////////

ThreadSafe function printDFREF(DFREF dfr)
    print "folder", GetDataFolder(1, dfr), "exists", DataFolderRefStatus(dfr)
end

ThreadSafe function/S getDFREFstring(DFREF dfr)
    return GetDataFolder(1, dfr)
end

// Ignores NaNs in the wave
function calcWaveAvg(Wave thisWave)
    variable thisAvg = NaN
    if(WaveExists(thisWave))
        wavestats/Q/Z thisWave
        thisAvg = V_avg
    else
        // print "calcWaveAvg: the wave doesn't exist"
    endif
    return thisAvg
end

function avgTwoVals(variable val1, variable val2)
    variable avg = NaN
    if(numType(val1) == 0 & numType(val2) == 0)
        avg = (val1 + val2)/2
    endif
    return avg
end

///////////
/// isInteger
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Return 0/1 based on whether or not supplied variable is an integer
///////////
function isInteger(variable varToCheck)
    if( mod(varToCheck, 1) == 0 )
        // It's an integer
        return 1
    else
        // It's not an integer
        return 0
    endif
end


// Remove blank cells from fullWave
// Note - this does not create a new wave, removes them directly from fullWave
// If this wave is used in a listbox, have to update the selection waves
function/Wave removeBlankCells(fullWave)
    Wave/T fullWave
    if(WaveExists(fullWave))
        duplicate/T/O fullWave, shortWave
        variable numCells = numpnts(shortWave)
        if(numCells>0)
            variable cell = numCells - 1
            do // work from end, b/c deleting points changes the index
                string thisVal = shortWave[cell]
                if(strlen(thisVal) == 0)
                    DeletePoints cell, 1, shortWave
                endif
                cell = cell - 1
            while (cell >= 0)
        endif
        // print shortWave
        return shortWave
    else
        print "removeBlankCells: wave does not exist"
        return fullWave
    endif
end

// This returns the wave name and data folder for a wave
// Should work with different types of waves, not just numeric waves
function [string waveN, DFREF waveDFR] getWaveNameAndDFR(Wave thisWave)
    if(WaveExists(thisWave))
        waveN = nameOfWave(thisWave)
        DFREF waveDFR = getWavesDataFolderDFR(thisWave)
        return [waveN, waveDFR]
    endif
end

///////////
/// getWaveBasename
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Get everything before the first "_" in a wave name
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-08-08
/// NOTES: Changed from splitString to stringFromList, which should work even if
/// there isn't an _ in the name
///////////
function/S getWaveBasename(string waveN)
    String basename = StringFromList(0, waveN, "_")
    return basename
end


function/Wave getTextWaveSubsetFromGroup(Wave/T groupWave, Wave/T waveToSubset[, variable groupNum, string groupName])

    DFREF storageDF = getWavesDataFolderDFR(groupWave)
    Wave/T uniqueGroups = getUniqueGroups(groupWave, storageDF=storageDF)

    variable numGroups = numpnts(uniqueGroups) 

    if(numGroups>0)
        if(numpnts(groupWave) == numpnts(waveToSubset))
            string groupOfInterest
            variable defToFirst = 0
            if(ParamIsDefault(groupName))
                if(ParamIsDefault(groupNum))
                    defToFirst = 1
                else
                    if(groupNum < numGroups && groupNum >= 0)
                        groupOfInterest = uniqueGroups[groupNum]
                    else
                        defToFirst = 1
                    endif
                endif
            else
                groupOfInterest = groupName
            endif

            if(defToFirst)
                groupOfInterest = uniqueGroups[0]
            endif

            string waveN
            DFREF waveDFR
            [waveN, waveDFR] = getWaveNameAndDFR(waveToSubset)

            string subWaveN = waveN+"_"+groupOfInterest

            Extract/O/INDX groupWave, storageDF:groupIndex, stringmatch(groupWave, groupOfInterest)
            Wave/SDFR=storageDF groupIndex
            
            make/T/O/N=(numpnts(groupIndex)) waveDFR:$subWaveN/Wave=subWave
            subWave=waveToSubset[groupIndex]
            return subWave
        else
            print "getTextWaveSubsetFromGroup: groupWave", nameOfWave(groupWave), "and waveToSubset", nameOfWave(waveToSubset), "are different lengths"
        endif
    endif
end

///////////
/// getPercentileFromDist
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-25
/// EDITOR: 
/// UPDATED: 
/// NOTES: Provide a distribution wave and a desired percentile,
/// and the function will return the value of the distribution at that percentile
///////////
function getPercentileFromDist(Wave distWave, variable percentile)
    variable valAtPercentile = distWave(percentile) // () uses x values
    return valAtPercentile
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-04-15
/// NOTES: Changed the storageDF default from eventsPanelDF to the data folder of groupWave
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-08-15
/// NOTES: Changed to check if there are more than 1 point in group wave, as with only 1,
/// FindDuplicates fails
function/Wave getUniqueGroups(Wave groupWave[, DFREF storageDF])
    if(paramisdefault(storageDF))
        // storageDF = getEventsPanelDF() // default to events panel DF
        storageDF = getWavesDataFolderDFR(groupWave) // Default to group wave's folder
    endif
    string uniqueGroupsWN = "uniqueGroups_" + nameOfWave(groupWave)
    if(numpnts(groupWave)>1)
        FindDuplicates /RT=storageDF:$uniqueGroupsWN groupWave // Gets rid of duplicates
        WAVE/T uniqueGroups = storageDF:$uniqueGroupsWN // Get local reference

        Extract/T /O uniqueGroups, uniqueGroups, strlen(uniqueGroups) > 0 // Get rid of blank group
    else
        if(numpnts(groupWave)== 1)
            duplicate/O/T groupWave, storageDF:$uniqueGroupsWN/Wave=uniqueGroups
            Extract/T /O uniqueGroups, uniqueGroups, strlen(uniqueGroups) > 0 // Get rid of blank group
        else
            make/O/T/N=0 storageDF:$uniqueGroupsWN/Wave=uniqueGroups
        endif
    endif
    return uniqueGroups
end

///////////
/// getUniqueValuesAndCounts
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Return the unique values, sorted, in a wave, along with the counts of each unique value
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-08-04
/// NOTES: Updated to check first if the wave exists, and if it exists but has no points,
/// still using the waveName to get the names for the val and count waves that will
/// also have no points
///////////
function [wave uniqueVals, wave uniqueCounts] getUniqueValuesAndCounts(Wave fullWave[, DFREF storageDF, string valsName, string countName])
    if(paramisdefault(storageDF))
        storageDF = getWavesDataFolderDFR(fullWave) // Default to group wave's folder
    endif
    if(WaveExists(fullWave))
        string basename = getWaveBasename(nameOfWave(fullWave))
        if(paramIsDefault(valsName))
            valsName = basename + "_uniq"
        endif
        if(paramIsDefault(countName))
            countName = basename + "_uniqCounts"
        endif
        if(numpnts(fullWave)>0)
            FindDuplicates /UN=storageDF:$valsName /UNC=storageDF:$countName fullWave
            WAVE uniqueVals = storageDF:$valsName // Get local reference
            WAVE uniqueCounts = storageDF:$countName // Get local reference
        else
            make/O/N=0 storageDF:$valsName/Wave=uniqueVals
            make/O/N=0 storageDF:$countName/Wave=uniqueCounts
        endif
    else
        print "the wave provided to getUniqueValuesAndCounts doesn't exist"
    endif
    return [uniqueVals, uniqueCounts]
end


Function PrintAllWaveNames(DFREF dfr)
	String objName
	Variable index = 0
	do
		objName = GetIndexedObjNameDFR(dfr, 1, index)
		if (strlen(objName) == 0)
			break
		endif
		Print objName
		index += 1
	while(1)
End

///////////
/// deleteWavesFromList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-14
/// NOTES: Delete the waves from a list of wave names. Optionally specify the data folder
/// where the waves should be found. 
/// Most likely source of listOfWaves is waveList function
/// Before Igor 9, this could only look in the current data folder, so
/// GetIndexedObjNameDFR may have been used
/// If dfr is not provided to function, then deleteWavesFromList will look in
/// current data folder
///////////
function deleteWavesFromList(string listOfWaves[, DFREF dfr])
    if(paramIsDefault(dfr))
        dfr = GetDataFolderDFR()
    endif
    
    string objName
    variable iWave = 0, nWaves = itemsinlist(listOfWaves)
    for(iWave=0; iWave<nWaves; iWave++)
        objName = StringFromList(iWave, listOfWaves)
        Wave thisWave = dfr:$objName
        if(WaveExists(thisWave))
            KillWaves/Z thisWave
        else
            print "unable to find wave", objName, "in data folder", getDFREFstring(dfr)
        endif
    endfor
end

///////////
/// winExists
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-09-08
/// NOTES: Return a 1 if a window exists, 0 if it does not
///////////
function winExists(string windowName)
    variable wType = WinType(windowName)
    variable winExists = 0
    if(wType != 0)
        winExists = 1
    endif
    return winExists
end

// Have not yet tested for waves with xWaves
// Can use this to get average wave from a group of waves, and save an error wave if desired
// This uses the package average waves function, but allows you to take averages of waves that are in different locations
function fWaveAverage_waveRefs(Wave/Wave yWavesToAvg, string ErrorTypeStr, wave avgWave[, Wave/Wave xWavesToAvg, variable ErrorInterval, wave errorWave])
    if(WaveExists(yWavesToAvg) && WaveExists(avgWave))
        string ListOfWaves = WaveRefWaveToList(yWavesToAvg, 0) // full path
        string ListOfXWaves = ""
        if(!ParamIsDefault(xWavesToAvg) && WaveExists(xWavesToAvg))
            if(numpnts(xWavesToAvg)>0)
                ListOfXWaves = WaveRefWaveToList(xWavesToAvg, 0) // full path
            endif
        endif


        // Error type
        variable ErrorType
        strswitch( ErrorTypeStr)
            case "none":
                ErrorType = 0
                break
            case "stdDev":
                ErrorType = 1
                break
            case "CI":
                ErrorType = 2
                break
            case "SEM":
                ErrorType = 3
                break
            default:
                ErrorType = 0 // default to none
                break
        endswitch

        // Error Interval
        if(ErrorType == 1)
            if(ParamIsDefault(ErrorInterval) || numType(ErrorInterval) != 0)
                ErrorInterval = 2 // default to 2 std dev
            else
                if(ErrorInterval<=0)
                    ErrorInterval = 2 // default to 2 std dev
                    print "WARNING: The interval you entered was less than or equal to zero. Using 2 STD DEV instead"
                else
                    if(ErrorInterval > 3)
                        print "WARNING: You've chosen to use more than 3 standard deviations for the error interval"
                    endif
                endif
            endif
        else
            if(ErrorType == 2)
                if(ParamIsDefault(ErrorInterval) || numType(ErrorInterval) != 0)
                    ErrorInterval = 95 // default to 95% CI
                else
                    if(ErrorInterval >= 100 || ErrorInterval <=0)
                        ErrorInterval = 95 // default to 95% CI
                        print "WARNING: The interval you entered was not between 0 and 100. Using 95% CI instead"
                    endif
                endif
            else // Not stdDev or CI
                ErrorInterval = NaN
            endif
        endif

        // Average wave
        string AveName = GetWavesDataFolder(avgWave, 2 ) // full path with wave name

        // Error wave
        string ErrorName = ""
        if(ErrorType > 0)
            // If errorWave doesn't exist, append _error the the name of the avg wave
            // Save in the same data folder
            if(ParamIsDefault(errorWave) || !WaveExists(errorWave))
                string avgWaveN
                DFREF avgWaveDFR
                [avgWaveN, avgWaveDFR] = getWaveNameAndDFR(avgWave)
                make/O avgWaveDFR:$(avgWaveN + "_error")/Wave=errorWave
            endif

            ErrorName = GetWavesDataFolder(errorWave, 2) // full path with wave name
        endif
        
        print "taking average of", ListOfWaves, "with xwaves", ListOfXWaves
        print "error type", ErrorTypeStr, ErrorType
        if(ErrorType == 1 || ErrorType == 2)
            print "error interval", ErrorInterval
        endif
        print "saving average at", AveName
        if(ErrorType>0)
            print "saving error wave at", ErrorName
        endif
        fWaveAverage(ListOfWaves, ListOfXWaves, ErrorType, ErrorInterval, AveName, ErrorName)
    endif
end

///////////
/// findWavesInDFRByRegEx
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-12-14
/// NOTES: 
///////////
function/S findWavesInDFRByRegEx(string regEx[, DFREF dfr])
    if(paramIsDefault(dfr))
        dfr = root:
    endif
    

    #if (IgorVersion() >= 9.00)
        string allWaves = WaveList("*", ";", "", dfr)

        string matchingWaves = GrepList(allWaves, regEx)
        return matchingWaves
    #else
        do
            objName = GetIndexedObjNameDFR(dfr, 1, index)
            if (strlen(objName) == 0)
                break
            endif
            if(GrepString(objName, regEx))
                matchingWaves += objName + ";"
            endif
            index += 1
        while(1)
    #endif
    return ""
end

///////////
/// getChildDFRWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: Returns the DFRs (as a wave of DFs) of the immediate children data folders of the parentDFR
///////////
function/Wave getChildDFRWave(DFREF parentDFR)
    string childDFRlist = getChildDFRList(parentDFR)

    variable iChild = 0, nChildren = itemsInList(childDFRlist)
    
    if(nChildren == 0)
        return $("")
    endif

    make/DF/FREE/N=0 childDFRsWave

    for(iChild=0; iChild<nChildren; iChild++)
        string childDFRname = StringFromList(iChild, childDFRlist)
        if(!strlen(childDFRname))
            continue
        endif
        DFREF childDFR = parentDFR:$childDFRname
        if(dataFolderRefStatus(childDFR)==0)
            continue
        endif
        childDFRsWave[numpnts(childDFRsWave)] = {childDFR} // appends to end
    endfor

    return childDFRsWave
end

///////////
/// getChildDFRList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-11
/// NOTES: 
///////////
function/S getChildDFRList(DFREF parentDFR)
    if(dataFolderRefStatus(parentDFR)==0)
        print "getSubFolderDFRs: parentDFR doesn't exist"
        return ""
    endif

    string childDFRlist

    #if (IgorVersion() >= 9.00)
        childDFRlist = dataFolderList("*", ";", parentDFR)
    #else
        variable index = 0
        string childDFRname
        do
            childDFRname = GetIndexedObjNameDFR(parentDFR, 4, index)
            if (strlen(childDFRname) == 0)
                break
            endif
            if(GrepString(childDFRname, regEx))
                childDFRlist += childDFRname + ";"
            endif
            index += 1
        while(1)
    #endif

    return childDFRlist
end





///////////
/// splitFullWindowName
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// EDITOR: 
/// UPDATED: 
/// NOTES: This will split a window string name into the host and subwindow
/// If there are multiple subwindow layers, it will provide the last one
/// as the subWin string. It would be easy to use StringFromList in other
/// functions if you had to extract the names of intermediate subwindows
///////////
function [string hostN, string subWin] splitFullWindowName(string winStr)

	variable numLayers = itemsInList(winStr, "#")

	hostN = StringFromList(0, winStr, "#")

	subWin = ""
	if(numLayers>1)
		subWin = StringFromList(numLayers-1, winStr, "#") // this only takes the last window from the list. There could be multiple layers
	endif

end

// From Igor help doc
// I think that I could probably provide some sort of wrapper around this to facilitate use
// with a wave of wave references and provide the average and error waves as waves


// Function you can use in Igor programming
// You may wish to write Igor procedures that make wave averages. You can use the same user-defined function in Waves Average.ipf that the control panel uses:

// Function fWaveAverage(ListOfWaves, ListOfXWaves, ErrorType, ErrorInterval, AveName, ErrorName)
// 	String ListOfWaves, ListOfXWaves, AveName, ErrorName
// 	Variable ErrorType, ErrorInterval

// ListOfWaves	A string containing a semicolon-separated list of (y) waves to be averaged.
// ListOfXWaves	A string containing a semicolon-separated list of Xwaves associated with the y waves (may be "" or a list with empty list items).
// ErrorType	= 0, don't generate an error wave.
// 	=1, generate data standard deviation.
// 	=2, generate confidence interval of the mean.
// 	=3, generate standard error of the mean.
// ErrorInterval	ErrorType = 1, the number of standard deviations for the Error Wave. 
// 	ErrorType = 2, the per cent confidence interval. Must be between 0 and 100.
// 	ErrorType = 0 or 3, ignored.
// AveName	String giving the name to use when creating the average wave
// ErrorName	String giving the name to use when creating the error wave. If ErrorType is zero, this parameter is ignored and can be set to "".
// Example

// This call will average all waves with names starting with "N_", and put the average in a wave called "NAve". It will make an error wave giving the 3-standard-deviation interval for each average point, and it will be called "Nsd":

// 	fWaveAverage(WaveList("N_*", ";", ""), "", 1, 3, "NAve", "Nsd")


///////////
/// calculateIntervals
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-10
/// NOTES: Calculate both a forward and backwards interval wave based off of a wave that contains the timing of events
///////////
function [wave/D backInt, wave/D forInt] calculateIntervals(wave eventTimeWave[, variable estUnknowns, variable waveDur, DFREF storageDF])

    // Check if the wave exists
    if(!WaveExists(eventTimeWave))
        print "The wave provided to calculateIntervals does not exist"
        return [backInt, forInt]
    endif

    if(paramIsDefault(estUnknowns))
        estUnknowns = 1 // default to estimate the first backwards and the last forwards interval based on series time
    endif
    
    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(eventTimeWave)

    if(paramIsDefault(storageDF))
        storageDF = waveDFR
    endif

    if(paramIsDefault(waveDur))
        // if wave duration isn't provided, try to find it based on wave basename

        variable seriesStartTime
        string baseWaveN = getWaveBasename(waveN)
        Wave baseWave = $baseWaveN

        if(WaveExists(baseWave))
            [seriesStartTime, waveDur] = calcSeriesTimeAndDuration(baseWave)
            print "duration", waveDur
        else
            print "calculateIntervals: couldn't find base wave and no wave duration provided. Defaulting to NaN"
        endif

    endif

    variable numEvents = numpnts(eventTimeWave)

    make/D/O/N=(numEvents) storageDF:$(waveN + "_backInt")/Wave=backInt
    make/D/O/N=(numEvents) storageDF:$(waveN + "_forInt")/Wave=forInt

    variable iEvent = 0, eventTime, prevEventTime, nextEventTime


    for(iEvent=0; iEvent<numEvents; iEvent++)
        eventTime = eventTimeWave[iEvent];

        if(numType(waveDur) == 0 && eventTime > waveDur) // if there's a normal number for wave duration and the event time is after duration
            print "calculateIntervals: Warning: The eventTime for event", iEvent, "(", eventTime, ") is after the identified wave duration (", waveDur,")"
        endif

        if(iEvent == 0) // first event
            if(estUnknowns)
                prevEventTime = 0
            else
                prevEventTime = NaN
            endif
        endif

        if(iEvent == numEvents - 1) // last event
            if(estUnknowns)
                nextEventTime = waveDur
            else
                nextEventTime = NaN
            endif
        else
            // not last event
            nextEventTime = eventTimeWave[iEvent + 1]
        endif

        backInt[iEvent] = eventTime - prevEventTime
        forInt[iEvent] = nextEventTime - eventTime

        prevEventTime = eventTime
    endfor

    return [backInt, forInt]
end

// ///////////
// /// testCalcInts
// /// AUTHOR: 
// /// ORIGINAL DATE: 2023-08-23
// /// NOTES: 
// ///////////
// function testCalcInts()
//     Wave/D backInt, forInt
//     Wave testWave = $("20220226cg1s14sw1t1_ptb")
//     [backInt, forInt] = calculateIntervals(testWave, estUnknowns = 1, waveDur = 100)

//     print testWave, backInt, forInt
// end


///////////
/// AGG_normalizeWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-05
/// NOTES: Generalized normalization function
/// stores the normalized wave in the data folder of the wave that is being normalized
/// if no name is provided, defaults to append _norm
/// if sign is not provided, defaults to negative
///////////
function/Wave AGG_normalizeWave(Wave waveToNorm[, variable thisSign, string normN])
    if(paramIsDefault(thisSign))
        thisSign = -1
    endif
    
    if(!(thisSign == -1 || thisSign == 1)) 
        print "Normalize wave: thisSign must be -1 or 1. Adjusting by sign"
        if(thisSign < 0)
            thisSign = -1
        else
            thisSign = 1
        endif
    endif
    
    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(waveToNorm)

    if(paramIsDefault(normN))
        normN = waveN + "_norm"
    endif

    duplicate/O waveToNorm, waveDFR:$normN/Wave=normWave

    WaveStats/Q waveToNorm

    if(thisSign == -1)
        normWave = - waveToNorm / V_min
    else
        normWave = waveToNorm / V_max
    endif

    return normWave
end


///////////
/// getWaveUnits
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: Return the unit string from a wave
///////////
function/s getWaveUnits(Wave thisWave[, variable dimNum])
    // waveInfo only works on 1D waves
    // string waveInfoString = waveInfo(thisWave, 0)
    // string unitsString = StringByKey("DUNITS", waveInfoString)

    if(paramIsDefault(dimNum))
        dimNum = -1 // default to data units
    endif
    

    string unitsString = WaveUnits(thisWave, dimNum)
    return unitsString
end

///////////
/// copyDataScale
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-08
/// NOTES: Copy the data scaling of the source wave to the toWave
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-08
/// NOTES: Added optional parameter to duplicate the note from the source wave to the new wave
///////////
function copyDataScale(wave srcWave, wave toWave[, variable dupNote])
    if(!(waveExists(srcWave)) || !(waveExists(toWave)))
        print "copyDataScale: One of the waves doesn't exist"
        return 0
    endif

    string unitsString = getWaveUnits(srcWave)
    setScale d, 0, 1, unitsString, toWave

    if(paramIsDefault(dupNote))
        string srcNote = note(srcWave)
        Note/NOCR toWave, srcNote
    endif
end

///////////
/// decimateAGG
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-15
/// NOTES: Keep only every xth point from a wave
/// npoints: Keep every [npoints]th row from waves
///     - if not provided, defaults to 10, keeps 1 event for every 9 deleted
/// theWaves: Wave/Wave reference of the waves to be decimated
///     - if not provided, will search in the graph
/// originalHost: The graph name from which waves should be found
///     - if theWaves is provided, this is not used
///     - if not provided, and theWaves is not provided, defaults to top graph
/// newHost: The host for the display of the decimated waves
///     - if not provided, not plotted
/// clearHost: should the host window for the new waves be cleared before plotting?

/// Important note: I'm not sure that there's a way to know if a trace is plotted
/// vertically or not. I'll look for "_dist" somewhere in the wave name and assume
/// that if it is, it should be plotted as a distribution plot

/// Assumes that if the original wave is scaled such that dx * numPnts = 1,
/// this decimated wave should also be scaled from dx to 1, adjusted to the new numEvents
///////////
function/Wave decimateAGG([variable nPoints, Wave/Wave theWaves, string originalHost, string newHost, variable clearHost])
    if(paramIsDefault(nPoints))
        nPoints = 10
    endif

    string bottomLabel = "", leftLabel = "", legendString = ""
    if(paramIsDefault(theWaves) || numpnts(theWaves) < 1 )
        if(paramIsDefault(originalHost))
            originalHost = ""
        endif
        
        Wave/T traceInfoFromGraph, traceNs
        [theWaves, traceNs, traceInfoFromGraph, bottomLabel, leftLabel, legendString]  = getWavesAndTraceInfoFromGraph(graphName = originalHost)
    endif

    if(numpnts(theWaves)<1)
        print "decimateAGG: No waves in theWaves"
        return $("")
    endif

    variable plot = 0
    if(!paramIsDefault(newHost))
        plot = 1
        variable hostExists = winExists(newHost)
        if(hostExists)
            string hostN, subN
            [hostN, subN] = splitFullWindowName(newHost)
            DoWindow/F $hostN
            if(!paramIsDefault(clearHost) && clearHost == 1)
                clearDisplay(newHost)
            endif
        else
            AGG_makeDisplay(50, 0, 90, 40, newHost)
        endif
    endif
    
    variable iWave = 0, nWaves = numpnts(theWaves)
    make/Wave/FREE/N=(nWaves) decimatedWaves

    variable updatedUnits = 0, isDist = 0

    for(iWave=0; iWave<nWaves; iWave++)
        wave thisWave = theWaves[iWave]

        if(!WaveExists(thisWave))
            continue
        endif

        string waveN
        DFREF waveDFR
        [waveN, waveDFR] = getWaveNameAndDFR(thisWave)

        if(dataFolderRefStatus(waveDFR)==0)
            print "couldn't find the data folder ref for", waveN, ". Defaulting to current data folder"
            DFREF waveDFR = GetDataFolderDFR()
        endif

        string decWaveN = waveN + "_dec"

        variable currentPnts = numpnts(thisWave)
        // variable newPnts = currentPnts / nPoints
        
        duplicate/O thisWave, waveDFR:$decWaveN/Wave=decWave
        
        redimension/N=(0) decWave
        decWave = NaN

        variable startingVal = floor(nPoints/2) // start in middle of set
        variable iEventsAdded = 0, iEvent

        for(iEvent=startingVal; iEvent<currentPnts; iEvent+= nPoints)
            redimension/N=(iEventsAdded + 1) decWave
            decWave[iEventsAdded] = thisWave[iEvent]
            iEventsAdded++
        endfor

        variable thisWaveDx = deltax(thisWave)
        variable scaled0to1 = 0

        variable tolerance = 1e-6  //Set your required tolerance here
        variable diff = abs(thisWaveDx * currentPnts - 1)

        if(diff <= tolerance)
            scaled0to1 = 1
        endif

        if(scaled0to1)
            variable decDx = 1/numpnts(decWave) // recalc and not newPnts which could be a decimal
            setscale /P x, decDx, decDx, decWave
        else
            SetScale x leftx(thisWave), rightx(thisWave), decWave  
        endif

        // This updates every nPoints points, but also shifts to the midpoint of the
        // nPoints sets, so that it removes from both beginning and end of wave
        // decWave = thisWave[p*nPoints + floor(nPoints/2)]
        // decWave = thisWave[p*nPoints + nPoints]



        if(plot)
            string traceN = traceNs[iWave]
            if(doesWaveNameContainsDist(waveN))
                variable sortDir = appendDistWave(decWave, traceN = traceN, displayHost = newHost)
                isDist = 1
            else
                AppendToGraph/W=$newHost decWave/TN=$traceN
            endif

            if(WaveExists(traceInfoFromGraph))
                string thisTraceInfo = traceInfoFromGraph[iWave]
                // print thisTraceInfo
                // print "color", StringByKey("rbg", thisTraceInfo)

                String rgbStr
                rgbStr = StringByKey("rgb(x)", thisTraceInfo, "=")       // e.g., "rgb(x)=(0,0,65535)
                print rgbStr, "rgb"
                variable red, green, blue
                sscanf rgbStr, "(%d,%d,%d)", red, green, blue
                ModifyGraph/W=$newHost rgb($traceN) = (red, green, blue)

            endif

        endif

        decimatedWaves[iWave] = decWave
    endfor

    if(!plot)
        return decimatedWaves
    endif

    if(isDist)
        if(strlen(bottomLabel) > 0)
            if(strlen(legendString) >0)
                setDistPlotAxes(displayHost=newHost, sortDir=sortDir, bottomLabel=bottomLabel, legendString=legendString)
            else
                setDistPlotAxes(displayHost=newHost, sortDir=sortDir, bottomLabel=bottomLabel)
            endif
        else
            if(strlen(legendString) >0)
                setDistPlotAxes(displayHost=newHost, sortDir=sortDir, legendString=legendString)
            else
                setDistPlotAxes(displayHost=newHost, sortDir=sortDir)
            endif
        endif
    else
        if(strlen(legendString) > 0)
            Legend/W=$newHost /N=text0 /C/J/B=1/A=RB/G=(0,0,0) legendString            
        endif
        if(strlen(bottomLabel) > 0)
            Label /W=$newHost/Z bottom, bottomLabel
        endif
        if(strlen(leftLabel) > 0)
            Label /W=$newHost/Z left, leftLabel
        endif
    endif
    return decimatedWaves
end

Function doesWaveNameContainsDist(String waveName)
    // Checks if provided wave name contains "_dist"

    // Search for "_dist" in the wave name
    Variable foundPos = StrSearch(waveName, "_dist", 0)
    
    // If foundPos is -1, it means "_dist" is not found
    if (foundPos == -1)
        return 0   // Returns 0 for False: "_dist" not found
    else
        return 1   // Returns 1 for True: "_dist" is found
    endif
End


Function MakeGaussianWave(string waveN)
    Variable i
    
    // Create a wave with 100 points
    Make /O/D/N=(100) $waveN/Wave=w
    w = 0
    
    // Set the parameters for the normal distribution
    Variable sigma = 15    // Standard deviation
    Variable mu = 50       //Mean or center
    Variable amplitude=1  // Amplitude 
    
    // Generate Gaussian distribution
    for(i = 0; i < 100; i += 1)
        w[i] = amplitude*exp(-(i - mu)^2/(2*sigma^2))    // Gaussian function
    endfor
    
    return 0
End

///////////
/// extractWaveWithinRegion
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: 
///////////
function/Wave extractWaveWithinRegion(wave timeBasedWave, variable startTime, variable endTime[, wave infoWave])
    if(!WaveExists(timeBasedWave))
        print "extractWaveWithinRegion: timeBasedWave doesn't exist"
        return $""
    endif

    if(paramIsDefault(infoWave))
        Extract/FREE timeBasedWave, resWave, (isWithinRange(timeBasedWave[p], startTime, endTime))
        return resWave
    endif

    if(!WaveExists(infoWave))
        print "extractWaveWithinRegion: infoWave doesn't exist"
        return $""
    endif

    variable timePnts = DimSize(timeBasedWave, 0), infoPnts = DimSize(infoWave, 0) // since infoWave could be multidimensional
    if(timePnts != infoPnts)
        print "extractWaveWithinRegion: There are not the same number of points in the time wave and info wave", nameOfWave(infoWave)
        return $""
    endif

    Extract/INDX/FREE timeBasedWave, matchingIndex, (isWithinRange(timeBasedWave[p], startTime, endTime))
    variable nColsInfo = DimSize(infoWave, 1)
    if(nColsInfo >= 1)
        make/FREE=1/D/N=(numpnts(matchingIndex), nColsInfo) subInfoWave
        if(numpnts(matchingIndex)>0)
            variable iCol = 0
            for(iCol=0; iCol<nColsInfo; iCol++)
                // 2024-05-14 AGG: It feels like there should be a cleaner way to
                // do this extraction for multicolumn waves, but I can't figure out a way to
                // index and assign that takes all column values for a specified row
                duplicate/FREE=1/RMD=[][iCol, iCol] infoWave, tempInfoWave
                make/FREE=1/N=(numpnts(matchingIndex)) tempSubWave
                tempSubWave = tempInfoWave[matchingIndex]
                subInfoWave[][iCol] = tempSubWave[p]
            endfor
        endif
    else
        make/FREE=1/D/N=(numpnts(matchingIndex)) subInfoWave
        if(numpnts(matchingIndex)>0)
            subInfoWave = infoWave[matchingIndex]
        endif
    endif

    return subInfoWave
end

// Rounds a number to a specified number of decimal places.
Function RoundToDecimal(variable number, variable numDecimals[, string method])
    variable roundFactor, scaledNumber, roundedNumber
    
    roundFactor = 10^(-numDecimals)
    scaledNumber = number / roundFactor

    if(paramIsDefault(method))
        method = "round"
    endif

    strswitch (method)
        case "floor":
            roundedNumber = floor(scaledNumber)
            break
        case "ceiling":
            roundedNumber = ceil(scaledNumber)
            break
        case "ceil":
            roundedNumber = ceil(scaledNumber)
            break
        case "round":
            roundedNumber = round(scaledNumber)
            break
        default:
            roundedNumber = round(scaledNumber)
            break
    endswitch
    
    return roundedNumber * roundFactor
End

// ///////////
// /// testExtract
// /// AUTHOR: Amanda Gibson
// /// ORIGINAL DATE: 2024-05-12
// /// NOTES: 
// ///////////
// function testExtract()
//     Wave timeWave = root:Events:cellInfo:c20240424a:summary:avgSubset:'20240424a_sct'
//     // Wave infoWave = root:Events:cellInfo:c20240424a:summary:avgSubset:concatAvgIndices
//     Wave infoWave = root:Events:cellInfo:c20240424a:summary:avgSubset:fwhm_concat
    
//     Wave subTimeWave = extractWaveWithinRegion(timeWave, 4, 120)
//     Wave subInfoWave = extractWaveWithinRegion(timeWave, 4, 120, infoWave = infoWave)
//     duplicate/O subTimeWave, testTime
//     duplicate/O subInfoWave, testInfo
    
//     edit/K=1 timeWave, infoWave, testTime, testInfo
// end

