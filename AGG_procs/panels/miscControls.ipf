#pragma rtGlobals=3		//Use modern global access method and strict wave access

///////////////////////////////////////////////////////
/// PANEL AND CONTROL INFORMATION
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-10
///////////////////////////////////////////////////////

function/S getSelectedItem(string listBoxCtrlName[, string hostName])
    if(paramisdefault(hostName))
        hostName = ""
    endif
    ControlInfo/W=$hostName $listBoxCtrlName
    if(V_flag == 0) // control doesn't exist, try AGG_Events
        print "No control", listBoxCtrlName, "in panel", hostName, "trying AGG_events"
        hostName = "AGG_Events"
    endif
    ControlInfo/W=$hostName $listBoxCtrlName
    if(V_flag == 0) // control doesn't exist, return ""
        print "No control", listBoxCtrlName, "in panel", hostName, "returning empty string"
        return ""
    endif

    variable selRow = V_value
    // print "selected row", selRow

    DFREF dfr = $(S_DataFolder)
    string selected = ""
    if(DataFolderRefStatus(dfr)!=0)
        string waveN = S_value
        Wave/T/SDFR=dfr listWave = $waven
        if(waveExists(listWave))
            variable numItems = numpnts(listWave)
            if(selRow >= 0)
                if(selRow < numItems)
                    selected = listWave[selRow]
                else
                    // print "getSelectedRow: selected row is > num items in wave. Selected #:", selRow, " Total #:", numItems
                endif
            else
                // print "getSelectedRow: no row selected"
            endif
        else
            print "getSelectedRow: couldn't find list wave", waven
        endif
    else
        print "getSelectedRow: couldn't find list wave data folder", getDFREFstring(dfr)
    endif
    // print "getSelectedItem", listBoxCtrlName, "selected", selected
    return selected
end

function/S returnGraphsInPanel(string panelName[, variable useFullName])
	string graphs = ""
	if(strlen(panelName)>0)
		string allSubWindows = childwindowlist(panelName)

		variable numWindows = itemsInList(allSubWindows), iWindow = 0
		for(iWindow = 0; iWindow<numWindows;iWindow++)
			string thisWindow = stringfromlist(iWindow, allSubWindows)
			string fullName = panelName + "#" + thisWindow
			if(WinType(fullName)==1)
				if(paramisdefault(useFullName))
					graphs += thisWindow + ";"
				else
					if(useFullName == 0)
						graphs += thisWindow + ";"
					else
						graphs += fullName + ";"
					endif
				endif
			endif
		endfor
	else
		graphs = WinList("*", ";", "Win:1")
	endif
	return graphs
end

///////////
/// getSetVarString
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-02
/// NOTES: If the SetVariable uses an actual variable as the value, then S_Value is the name of the variable
/// and not the value of the control. S_Value is only the value of the control if _STR: syntax is used
/// 
/// this function was created to aid with the reading of the experiment card, to also check that the control
/// exists before returning a string
///////////
function/S getSetVarString(string ctrlName[, string panelName])
	
    if(paramIsDefault(panelName))
        panelName = "experimentCard"
    endif

    ControlInfo/W=$panelName $ctrlName
    if(V_flag != 5)
        return ""
    endif

    return S_Value    
end

///////////
/// getSetVarVal
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-02
/// NOTES: this function was created to aid with the reading of the experiment card, to also check that the control
/// exists before returning a value
///////////
function getSetVarVal(string ctrlName[, string panelName])
	
    if(paramIsDefault(panelName))
        panelName = "experimentCard"
    endif

    ControlInfo/W=$panelName $ctrlName
    if(V_flag != 5)
        return NaN
    endif

    return V_value
end

///////////
/// getCBVal
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-02
/// NOTES: 
///////////
function getCBVal(string ctrlName[, string panelName])
    if(paramIsDefault(panelName))
        panelName = "experimentCard"
    endif

    ControlInfo/W=$panelName $ctrlName
    if(V_flag != 2)
        return NaN
    endif

    return V_value
end
