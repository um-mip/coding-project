// See help topic: Control Panel Resolution on Windows
// There are different ways that panels are created depending on the DPI of a windows
// computer. On higher resolution screens, panels are made with pixels instead of points

// SetIgorOption PanelResolution = 0

// structure to store screen parameters
STRUCTURE ScreenSizeParameters
    variable scrwpxl // width in pixels
    variable scrhpxl // height in pixels
    variable scrwpnt // width in points
    variable scrhpnt // width in points
    variable scrRes // screen resolution
    variable panelRes // panel resolution
ENDSTRUCTURE

///////////
/// InitScreenParameters
/// AUTHOR: Jeffery J Weimer
/// (Data from ScreenSizer package download): 2010-09-26
/// EDITOR: Amanda Gibson
/// UPDATED: 2022-04-15
/// NOTES: Update the ScreenSize Parameters Structure
/// with the width and height of the available screen size
/// in both points and pixels
/// Also updates the screen resolution and panel resolution
/// These can be used to make panels and displays that are
/// relative to the size of the screen
///////////
Function InitScreenParameters(ScrP)
    STRUCT ScreenSizeParameters &ScrP

    ScrP.scrRes = ScreenResolution
    ScrP.panelRes = PanelResolution("")
    
    if (cmpstr(IgorInfo(2),"Macintosh")==0)
        // MacOS
       
        string IgorStuff=""
        variable scr0,scr1,scr2
   
        IgorStuff=IgorInfo(0)
        scr0 = strsearch(IgorStuff,"RECT",0)
        scr1 = strsearch(IgorStuff,",",scr0+9)
        scr2 = strlen(IgorStuff)-2     
        ScrP.scrwpxl = str2num(IgorStuff[scr0+9,scr1-1])
        ScrP.scrhpxl = str2num(IgorStuff[scr1+1,scr2])
        
        // set screen point sizes
        ScrP.scrwpnt = ScrP.scrwpxl*ScrP.panelRes/ScrP.scrRes
        ScrP.scrhpnt = ScrP.scrhpxl*ScrP.panelRes/ScrP.scrRes
    else
        GetWindow kwFrameInner wsizeDC // pixels
        ScrP.scrwpxl = (v_right-v_left)
        ScrP.scrhpxl = (v_bottom-v_top)

        GetWindow kwFrameInner wsize // points
        ScrP.scrwpnt = (v_right-v_left)
        ScrP.scrhpnt = (v_bottom-v_top)
    endif
    return 0
end

///////////
/// usePixelsForPanels
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: return 0 if pixels should not be used (most cases)
/// and return 1 if pixels should be used (either)
///     - any time the PanelResolution option is set to 72 (always pixels)
///     - when the screen resolution is 96, and PanelResolution option is set to 1
/// This is needed because of the odd ways that Igor sizes things on Windows
/// when the screen resolution is 96
///////////
function usePixelsForPanels()
    variable usePixels = 0
    string cmdStr = "SetIgorOption PanelResolution = ?"
    Execute cmdStr
    NVAR V_flag
    variable currPanelResSettings = V_flag
    // print "currentPanelResSettings", currPanelResSettings

    if(currPanelResSettings == 72)
        usePixels = 1
    else
        if(ScreenResolution==96 && currPanelResSettings == 1)
            usePixels = 1
        else
            usePixels = 0
        endif
    endif
    return usePixels
end

///////////
/// AGG_makePanel
/// AUTHOR: Amanda Gibson
/// UPDATED: 2022-04-15
/// NOTES: Use this function to make a new panel relative to the available screen size
/// Will take into account pixel/points based on operating system and resolution
///////////
Function AGG_makePanel(leftPerc, topPerc, rightPerc, bottomPerc, strname[, kill])
	variable leftPerc, topPerc, rightPerc, bottomPerc, kill
	string strname
    if(paramIsDefault(kill))
        kill = 1 // default to kill without asking when closed
    endif
    // define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP
   
    // initialize it
   
    InitScreenParameters(ScrP)
    // print ScrP

    variable left, top, right, bottom
    if(usePixelsForPanels()) // use pixels
        left = leftPerc / 100 * ScrP.scrwPxl
        right = rightPerc / 100 * ScrP.scrwPxl
        top = topPerc / 100 * ScrP.scrhPxl
        bottom = bottomPerc / 100 * ScrP.scrhPxl
    else // use points
        left = leftPerc / 100 * ScrP.scrwPnt
        right = rightPerc / 100 * ScrP.scrwPnt
        top = topPerc / 100 * ScrP.scrhPnt
        bottom = bottomPerc / 100 * ScrP.scrhPnt
    endif

    // print leftPnts, rightPnts, topPnts, bottomPnts
	NewPanel/W=(left, top, right, bottom)/N=$strname/K=(kill)
end

///////////
/// AGG_makeTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-03
/// NOTES: 
///////////
function AGG_makeTable(left, top, right, bottom, strname[, kill])
    variable left, top, right, bottom
	string strname
    variable kill

	// define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP

    if(paramIsDefault(kill))
        kill = 1 // default to kill without asking when closed
    endif
   
    // initialize it
    InitScreenParameters(ScrP)

    variable leftPnts = left/100*ScrP.scrwpnt
    variable rightPnts = right/100*ScrP.scrwpnt
    variable topPnts = top/100*ScrP.scrhpnt
    variable bottomPnts = bottom/100*ScrP.scrhpnt

	Edit/W=(leftPnts, topPnts, rightPnts, bottomPnts)/N=$strname/K=(kill)
end


///////////
/// AGG_makeDisplay
/// AUTHOR: Amanda Gibson
/// UPDATED: 2022-04-15
/// NOTES: Use this function to make a new display graph relative to the available screen size
///////////
Function AGG_makeDisplay(left, top, right, bottom, strname[, kill])
	variable left, top, right, bottom
	string strname
    variable kill

	// define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP

    if(paramIsDefault(kill))
        kill = 1 // default to kill without asking when closed
    endif
   
    // initialize it
    InitScreenParameters(ScrP)

    // print ScrP
    variable leftPnts = left/100*ScrP.scrwpnt
    variable rightPnts = right/100*ScrP.scrwpnt
    variable topPnts = top/100*ScrP.scrhpnt
    variable bottomPnts = bottom/100*ScrP.scrhpnt

    // print "left", leftPnts, "top", topPnts, "right", rightPnts, "bottom", bottomPnts
    // print "width", rightPnts - leftPnts
    // print "height", bottomPnts - topPnts

    // print "points", leftPnts, rightPnts, topPnts, bottomPnts
	Display/W=(leftPnts, topPnts, rightPnts, bottomPnts)/N=$strname/K=(kill)
end

///////////
/// getWindowDims
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: Return the width and height of a window
/// if no windowName is provided, default to top window
/// type should be "pixels" or "points"
/// if no type is provided, and if windowName is a window
/// then check if it should be using pixels or points
/// for size
///////////
function[variable width, variable height] getWindowDims([string windowName, string type])
    variable windowType
    if(paramIsDefault(windowName) || strlen(windowName) == 0)
        windowName = "kwTopWin"
        windowType = WinType("")
    else
        windowType = WinType(windowName)
    endif
 
    variable isPanel = (windowType == 7)

    if(paramIsDefault(type) || strlen(type) == 0)
        type = "points"
        if(isPanel)
            if(usePixelsForPanels()==1)
                type = "pixels"
            endif
        endif
    endif

    if(stringmatch(type, "pixels"))
        GetWindow/Z $windowName wSizeDC
    else
        GetWindow/Z $windowName wSize
    endif
    
    // print "Size:", V_left, V_top, V_right, V_bottom

    width = V_right - V_left
    height = V_bottom - V_top
    // print "getWindowDims: returning", type, "width:", width, "height:", height

    return [width, height]
end

///////////
/// getWindowLocation
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: Return the width and height of a window
/// if no windowName is provided, default to top window
/// type should be "pixels" or "points"
/// if no type is provided, and if windowName is a window
/// then check if it should be using pixels or points
/// for size
///////////
function[variable left, variable top, variable right, variable bottom] getWindowLocation([string windowName, string type])
    variable windowType
    if(paramIsDefault(windowName) || strlen(windowName) == 0)
        windowName = "kwTopWin"
        windowType = WinType("")
    else
        windowType = WinType(windowName)
    endif
 
    variable isPanel = (windowType == 7)

    if(paramIsDefault(type) || strlen(type) == 0)
        type = "points"
        if(isPanel)
            if(usePixelsForPanels()==1)
                type = "pixels"
            endif
        endif
    endif

    if(stringmatch(type, "pixels"))
        GetWindow/Z $windowName wSizeDC
    else
        GetWindow/Z $windowName wSize
    endif
    
    // print "Size:", V_left, V_top, V_right, V_bottom

    left = V_left
    top = V_top
    right = V_right
    bottom = V_bottom
    
    return [left, top, right, bottom]
end



///////////
/// posRelPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: Return the width or height (specified by dimension) relative proportion
/// to the size of a panel
/// If panelName is not provided, defaults to the top window
/// Type is points or pixels. This should generally be left to default
/// so that the type can be determined by operating system and resolution
/// 
/// This should be used when sizing control items within a panel
///////////
function posRelPanel(variable rel, string dimension[, string panelName, string type])
    
    variable width, height
    if(paramIsDefault(panelName))
        panelName = ""
    endif
    if(paramIsDefault(type))
        type = ""
    endif

    [width, height] = getWindowDims(windowName = panelName, type = type)
    
    variable size
    if(numtype(width)==0 && numtype(height)==0)
        if(stringmatch(dimension, "width"))
            size = width * rel
            // print "width", width, "rel size", rel, "size", size
        else
            size = height * rel
            // print "height", height, "rel size", rel, "size", size
        endif
    endif
    return size
end

///////////
/// xyRelPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: Return the width and height relative to the size of the panel
///////////
function[variable width, variable height] xyRelPanel(variable xRel, variable yRel[, string panelName, string type])
    if(paramIsDefault(panelName))
        panelName = ""
    endif
    if(paramIsDefault(type))
        type = ""
    endif

    width = posRelPanel(xRel, "width", panelName = panelName, type = type)
    height = posRelPanel(yRel, "height", panelName = panelName, type = type)

    return [width, height]
end

/////// Helper functions to understand screen size

function printInnerWindowSizeWindows()
    GetWindow kwFrameInner wSize
    print "Size (in pnts):", V_left, V_top, V_right, V_bottom
    GetWindow kwFrameInner wSizeDC
    print "Size (in pixels):", V_left, V_top, V_right, V_bottom

    STRUCT ScreenSizeParameters ScrP
    InitScreenParameters(ScrP)

    print ScrP

    // AGG External
    // Size (in pnts):  0  0  1440  787.5
    // Size (in pixels):  0  0  1920  1050

    // AGG Computer
    // Size (in pnts):  0  0  1152  541.8
    // Size (in pixels):  0  0  1920  903
end

///////////
/// testPanelMaking
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-15
/// NOTES: This should show that on windows screens with resolution 96
/// the display made with points and the panel made with pixels
/// are the same size.
///     (when SetIgorOption panelResolution = 1)
/// On all other screens, (or if SetIgorOption panelResolution = 0),
/// then the display and panel made with pnts should be the same size
///////////
function testPanelMaking()
    STRUCT ScreenSizeParameters ScrP

    InitScreenParameters(ScrP)

    print ScrP

    Display/W=(0, 0, ScrP.scrwpnt * .9, ScrP.scrhpnt * .8)/N=test_dPnt/K=1
    Display/W=(0, 0, ScrP.scrwPxl * .9, ScrP.scrhPxl * .8)/N=test_dPxl/K=1
    NewPanel/W=(0, 0, ScrP.scrwpnt * .9, ScrP.scrhpnt * .8)/N=test_PPnt/K=1
    NewPanel/W=(0, 0, ScrP.scrwPxl * .9, ScrP.scrhPxl * .8)/N=test_PPxl/K=1
end

///////////
/// testWindowDims
/// AUTHOR: 
/// ORIGINAL DATE: 2022-04-15
/// NOTES: 
///////////
function testWindowDims()
    variable width, height
    killwindow/Z test
    AGG_makePanel(0, 0, 10, 15, "test")
    [width, height] = getWindowDims(type = "points")

    variable half = posRelPanel(50, "width", type = "points")
    print "half width", half
    half = posRelPanel(50, "height")
    print "half height", half

    variable halfWidth, halfHeight
    [halfWidth, halfHeight] = xyRelPanel(50, 50)
    print "half width v2", halfWidth, "half height v2", halfHeight
end

///////////
/// scaleForScreenSize
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-04
/// NOTES: Contract if window is too large for the size of the screen
/// Defaults to 90% of the screen for the smallest dimension
/// Currently only works for panels and graphs, but other window types
/// could be tested for and modified
/// Requires Igor 9
///////////
function scaleForScreenSize(string windowName)
	// Adjust size if taller than screen
	STRUCT ScreenSizeParameters ScrP
	InitScreenParameters(ScrP)
	
	variable usePixels = usePixelsForPanels(), windowHeight, windowWidth
	variable windowType = WinType(windowName) 
    variable isPanel = (windowType == 7)
    variable isGraph = (windowType == 1)
	if(isPanel && usePixels)
		windowHeight = ScrP.scrhpxl
		windowWidth = ScrP.scrwpxl
	else
		windowHeight = ScrP.scrhpnt
		windowWidth = ScrP.scrwpnt
	endif

	// print "windowWidth", windowWidth, "windowHeight", windowHeight

	variable calHeight, calWidth
	[calWidth, calHeight] = getWindowDims(windowName = windowName)

	variable adjust = 0, adjHeight = 1, adjWidth = 1
	if(calHeight>windowHeight)
		variable targetHeight = windowHeight * .9
		adjHeight = targetHeight/calHeight
		adjust = 1
	endif
	if(calWidth>windowWidth)
		variable targetWidth = windowWidth * .9
		adjWidth = targetWidth/calWidth
		adjust = 1
	endif
    
    #if (IgorVersion() >= 9.00)
	if(adjust)
		variable expandFactor = min(adjHeight, adjWidth)
		if(isPanel)
			ModifyPanel/W=$windowName expand = expandFactor
		else
			if(isGraph)	
				ModifyGraph/W=$windowName expand = expandFactor
			endif
		endif
	endif
    #endif
end
