// This should be called when constructing a panel with tabs
// tabCtrlName: Control name for the tab control
// tabs: A list of the tabs (backend labels)
    // in the panelDFR, there should be a numeric variable for each tab name that equals the corresponding tab number
// tabLabels: A keyed string with the backend tab names and the front end nice labels
// panelDFR: data folder reference where the tab numbers are stored
function makeTabControls(string tabCtrlName, string tabs, string tabLabels, DFREF panelDFR)
    if(strlen(tabs)>0 && DataFolderRefStatus(panelDFR)!=0)
        variable numTabs = itemsinlist(tabs), iTab = 0
        for(iTab = 0; iTab < numTabs; iTab++)
            print "iTab", iTab
            string thisTab = StringFromList(iTab, tabs);

            // store a numeric variable making tab name in panelDFR with corresponding number
            // This is best achieved with a structure
            NVAR/Z/SDFR=panelDFR tabNum = $thisTab 

            if(numType(tabNum)==0) // normal number
                string tabLabel = stringbykey(thisTab, tabLabels)
                print "tabNum", tabNum, "tabLabel", tabLabel
                TabControl $tabCtrlName tabLabel(tabNum)=tabLabel
            endif
        endfor
    endif
end

////////////////////////////////////////////////////////////////////////
/////////////// ADD TAB INFO TO CONTROLS AND DISPLAYS \\\\\\\\\\\\\\\\\\
//
// These functions should be called at the end of each function that
// creates a tab. Userdata is used specify which controls and displays 
// should be displayed when that tab is active
//
// A control or display can be associated with multiple tabs
////////////////////////////////////////////////////////////////////////

function addTabUserData(tabControls, tabNum)
    string tabControls
    variable tabNum

    string tabNumStr = num2str(tabNum) + ";"

    variable numControls = itemsInList(tabControls)
    variable iControl = 0
    for(iControl = 0; iControl < numControls; iControl ++ )
        string thisCtrl = StringFromList(iControl, tabControls)
        ModifyControl $thisCtrl, userdata(tab) += (tabNumStr) // add to the existing string
    endfor
end

function addTabUserData_forNoEdit(tabControls, tabNum)
    string tabControls
    variable tabNum

    string tabNumStr = num2str(tabNum) + ";"

    variable numControls = itemsInList(tabControls)
    variable iControl = 0
    for(iControl = 0; iControl < numControls; iControl ++ )
        string thisCtrl = StringFromList(iControl, tabControls)
        ModifyControl $thisCtrl, userdata(tab_noEdit) += (tabNumStr)
    endfor
end

function addDisplayUserData(tabDisplays, tabNum, panelName)
    string tabDisplays
    variable tabNum
    string panelName

    string tabNumStr = num2str(tabNum) + ";"

    variable numDisplays = itemsInList(tabDisplays)
    variable iDisplay = 0
    for(iDisplay = 0; iDisplay < numDisplays; iDisplay ++ )
        string thisDisplay = StringFromList(iDisplay, tabDisplays)
        // print "addTab", tabNum, "for", thisDisplay
        SetWindow $(panelName + "#" + thisDisplay), userdata(tab) += (tabNumStr)
    endfor
end


////////////////////////////////////////////////////////////////////////
//////////////////// CHANGE DISPLAY FOR EACH TAB \\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////////////////////////////////

Function changeDisplayForTab(TC_Struct) : TabControl
    STRUCT WMTabControlAction &TC_Struct

    string panelName = TC_Struct.win
    
    switch(TC_Struct.eventCode)
        case 2: // Mouse up

        variable tabNum = TC_Struct.tab
        string tabCtrlName = TC_Struct.ctrlName

        string panelString = getuserdata("", tabCtrlName, "panelDFR")
        DFREF panelDFR = $panelString
        string tabs = getuserdata("", tabCtrlName, "tabs")
        string tabLabels = getuserdata("", tabCtrlName, "tabLabels")

        // If not provided by user data, try to look in panelDFR
        if(strlen(tabs)==0)
            SVAR/SDFR = panelDFR tabVarNames
            tabs = tabVarNames
        endif
        if(strlen(tabLabels)==0)
            SVAR/SDFR = panelDFR gTabLabels = tabLabels
            tabLabels = gTabLabels
        endif
        
        colorTabsBySelection(tabCtrlName, tabs, tabLabels, tabNum)
        enableControlsByTabData("", tabNum)
        enableDisplaysByTabData("", tabNum, panelName)

        strswitch (panelName)
            case "AGG_events":
                updateTabDisplay_eventsPanel()
                
                break
            case "AGG_bursts":
                updateTabDisplay_burstsPanel(panelName = panelName)
            default:
                
                break
        endswitch
        break
    endswitch
    return 0
End

///////////
/// getSelectedTab
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: 
///////////
function/S getSelectedTab([string tabCtrlName, string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_events"
    endif
    
    if(paramIsDefault(tabCtrlName))
        tabCtrlName = "EventsTabs"
    endif

    string panelString = getuserdata(panelName, tabCtrlName, "panelDFR")
    DFREF panelDFR = $panelString
    string tabs = getuserdata(panelName, tabCtrlName, "tabs")
    string tabLabels = getuserdata(panelName, tabCtrlName, "tabLabels")

    // If not provided by user data, try to look in panelDFR
    if(strlen(tabs)==0)
        SVAR/SDFR = panelDFR tabVarNames
        tabs = tabVarNames
    endif
    if(strlen(tabLabels)==0)
        SVAR/SDFR = panelDFR gTabLabels = tabLabels
        tabLabels = gTabLabels
    endif

    ControlInfo/W=$panelName $tabCtrlName

    variable tabNum = V_value

    string thisTab = StringFromList(tabNum, tabs)
    string tabLabel = ""
    if(numType(tabNum)==0) // normal number
        tabLabel = thisTab
        if(strlen(tabLabels)>0)
            tabLabel = stringbykey(thisTab, tabLabels)
        endif
    endif
    return tabLabel
end


function colorTabsBySelection(string tabCtrlName, string tabs, string tabLabels, variable tabNum[, string selectedColor, string nonSelectedColor])
    if(strlen(tabs)>0)
        variable numTabs = itemsinlist(tabs), iTab = 0
        string thisTabLabel
        
        if(ParamIsDefault(selectedColor))
            selectedColor = "\K(65535,0,0)"
        endif

        if(ParamIsDefault(nonSelectedColor))
            nonSelectedColor = "\K(0,0,0)"
        endif

        ControlInfo $tabCtrlName
        if(V_Flag == 8) // if control is a tabControl that exists
            for(iTab = 0; iTab < numTabs; iTab++)
                string thisTab = StringFromList(iTab, tabs);
                if(numType(tabNum)==0) // normal number
                    string tabLabel = thisTab
                    if(strlen(tabLabels)>0)
                        tabLabel = stringbykey(thisTab, tabLabels)
                    endif
                    
                    if(iTab == tabNum)
                        thisTabLabel = selectedColor
                        thisTabLabel += tabLabel
                    else
                        thisTabLabel = nonSelectedColor
                        thisTabLabel += tabLabel
                    endif
                    TabControl/Z $tabCtrlName, tablabel(iTab) = thisTabLabel // update the label
                endif
            endfor
        endif
    endif
end

function enableControlsByTabData(winName, tabToMatch)
    string winName
    variable tabToMatch

    // // Modify display for tab-tagged controls
    // // from: https://www.wavemetrics.net/doc/igorman/V-01%20Reference.pdf p. V-517

    string tab = num2str(tabToMatch)

    string allControls = ControlNameList("")
    variable numControls = itemsInList(allControls)

    variable iControl = 0
    for(iControl = 0; iControl < numControls; iControl ++)
        string thisCtrl = StringFromList(iControl, allControls)
        string tabData = getuserdata("", thisCtrl, "tab")
        string tabData_noEdit = getuserdata("", thisCtrl, "tab_noEdit")
        if(strlen(tabData)>0) // If there's info in the tab userdata
            if(itemsInList(listmatch(tabData, tab))>0) // If any of the list matches this tab
                if(strlen(tabData_noEdit)>0)
                    if(itemsInList(listmatch(tabData_noEdit, tab))>0)
                        ModifyControl $thisCtrl disable = 2 // no user input
                    else
                        ModifyControl $thisCtrl disable = 0 // enable
                    endif
                else
                    ModifyControl $thisCtrl disable = 0 // enable
                endif
            else
                ModifyControl $thisCtrl disable = 1 // disable
            endif
        endif
    endfor

end

function enableDisplaysByTabData(winName, tabToMatch, panelName)
    string winName, panelName
    variable tabToMatch

    string tab = num2str(tabToMatch)

    string allDisplays = ChildWindowList(panelName)
    // print allDisplays, "all displays"
    variable numDisplays = itemsInList(allDisplays)

    variable iDisplay = 0
    for(iDisplay = 0; iDisplay < numDisplays; iDisplay ++)
        string thisDisplay = StringFromList(iDisplay, allDisplays)
        string fullDisplay = panelName + "#" + thisDisplay
        string tabData = getuserdata(fullDisplay, "", "tab")
        if(strlen(tabData)>0) // If there's info in the tab userdata
            // print "display", fullDisplay, "tabData", tabData
            if(itemsInList(listmatch(tabData, tab))>0) // If any of the list matches this tab
                SetWindow $fullDisplay Hide=0 // enable
            else
                SetWindow $fullDisplay Hide=1 // disable
            endif
        else
            SetWindow $fullDisplay hide = 1
        endif
    endfor

end