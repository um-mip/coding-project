/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-04-15
/// NOTES: Changes the default storageDF from events panel DF
/// to the data frame where the numWave is storaged
function plotGroupData(Wave/T groupWave, Wave numWave[, DFREF storageDF, string displayHost, string yLabel])
    if(paramisdefault(storageDF))
        storageDF = getWavesDataFolderDFR(numWave) // default to numWave data folder
    endif

    if(paramisdefault(displayHost))
        doWindow/F groupData
        // if groupData doesn't already exist, make it, otherwise bring to front
        if(V_Flag != 1)
            Display/N=groupData/K=1
        endif
        displayHost = "groupData"
    endif
    
    clearDisplay(displayHost)
    // clearDisplay(displayHost) // don't know why I need this twice

    variable numVals = numpnts(numWave)

    Wave/T uniqueGroups = getUniqueGroups(groupWave)
    variable numGroups = numpnts(uniqueGroups)
    variable iGroup

    make/N=(numVals, numGroups)/O storageDF:dataWave/Wave=dataWave // make to maximum if all in one group
    dataWave = NaN
    
    variable maxItemsInGroup = 0, itemsInGroup = 0

    for(iGroup = 0; iGroup < numGroups; iGroup++)
        string groupName = uniqueGroups[iGroup]

        // set the dimension label based on the group
        SetDimLabel 1, iGroup, $groupName, dataWave
        
        Extract/O/INDX groupWave, storageDF:groupIndex, stringmatch(groupWave, groupName)
        Wave/SDFR = storageDF groupIndex
        itemsInGroup = numpnts(groupIndex)
        // print itemsInGroup

        if(itemsInGroup > maxItemsInGroup)
            maxItemsInGroup = itemsInGroup
        endif
        // store the numeric values for the subset of the numeric wave
        // that match the group index
        dataWave[0, itemsInGroup - 1][iGroup] = numWave[groupIndex[p]]
    endfor

    if(numGroups > 0)
        // Plot it
        AppendBoxPlot/W=$displayHost dataWave vs uniqueGroups
        ModifyBoxPlot/W=$displayHost markers={8,8,8},markerSizes={2,2,2}
        ModifyGraph/W=$displayHost lsize(dataWave)=0
        ModifyGraph/W=$displayHost toMode(dataWave)=3
        SetAxis/W=$displayHost/Z/E=3 left, *,* // force to zero
        
        Wave meanWave = getGroupMean(dataWave)
        AppendToGraph/W=$displayHost meanWave[%avg][] vs uniqueGroups
        ModifyGraph/W=$displayHost mode(meanWave) = 3
        Wave SEMWave = getGroupSEM(dataWave)
        ErrorBars/W=$displayHost meanWave Y, wave=(SEMWave[%sem][*], SEMWave[%sem][*])
        ModifyGraph/W=$displayHost rgb(meanWave) = (0,0,0)

        if(!paramisdefault(yLabel))
            // print "not default", yLabel
            Label/W=$displayHost left, yLabel
        endif
    endif
end

function/WAVE getGroupCI(Wave dataWave)
    DFREF waveDFR = getWavesDataFolderDFR(dataWave)
    wavestats/PCST dataWave
    Wave M_waveStats
    Duplicate/O/RMD=[24, 25] M_waveStats, waveDFR:CIWave
    Wave/SDFR = waveDFR CIWave
    return CIWave
end

function/WAVE getGroupSEM(Wave dataWave[, string semName])
    if(paramIsDefault(semName))
        semName = "semWave"
    endif
    
    DFREF waveDFR = getWavesDataFolderDFR(dataWave)
    wavestats/PCST dataWave
    Wave M_waveStats
    Duplicate/O/RMD=[26] M_waveStats, waveDFR:$semName
    Wave/SDFR = waveDFR SEMWave = $semName
    return SEMWave
end

function/WAVE getGroupMean(Wave dataWave[, string meanName])
    if(paramIsDefault(meanName))
        meanName = "meanWave"
    endif
    
    DFREF waveDFR = getWavesDataFolderDFR(dataWave)
    wavestats/PCST dataWave
    Wave M_waveStats
    Duplicate/O/RMD=[3] M_waveStats, waveDFR:$meanName
    Wave/SDFR = waveDFR meanWave = $meanName
    return meanWave
end