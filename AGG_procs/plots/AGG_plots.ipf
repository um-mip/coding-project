#include <Axis Utilities>
#include <AnnotationInfo Procs>

// Clear output display 
function clearDisplay(string target)
    #if (IgorVersion() >= 9.00)
        if(WinType(target)==1)
            RemoveFromGraph/W=$target/ALL
            DeleteAnnotations/W=$target/A
        endif
    #else    
        string wl = "", rwn2 = ""
        wl = tracenamelist(target,  ";" , 27 ) // bit 3 includes boxplots; 4 includes violin plot traces // 2022-03-06 changed from 26 to 27, includes regular traces now
        // print "wave list", wl
        variable item2=0, nitems2=itemsinlist(wl)

        if (nitems2>0)
            do
                // 2022-04-13 this was still set to option 1, instead of 27
                // this was working because it seems that providing a blank wave name
                // removes the first trace, so since the number was correct, it would
                // loop through and still remove everything
                wl = tracenamelist(target,  ";" , 27 )
                // print "wave list", wl
                rwn2 = stringfromlist( item2, wl )
                WAVE /Z rw2 = $rwn2
                removefromgraph /W=$target $rwn2
                item2+=1
            while(item2<nitems2)
        endif
    #endif
end

function plotValByTime(Wave valWave, Wave timeWave[, string paramLabel, string displayHost])
	if(paramisdefault(displayHost))
		Display/K=1
		GetWindow kwTopWin title
		displayHost = s_value // this is ""
	endif
	if(WaveExists(valWave) & waveExists(timeWave))
		AppendToGraph/W=$displayHost valWave vs timeWave
		SetScale d 0,0,"dat" timeWave
		SetAxis/W=$displayHost/Z/E=3 left, *,* // force to include zero
		Label/W=$displayHost bottom, "time"
		ModifyGraph/W=$displayHost mode=4,marker=19
		if(!paramisdefault(paramLabel))
			Label/W=$displayHost left, paramLabel
		endif
	endif
end

function averageWavesInGraph(string ErrorTypeStr, wave avgWave[, string displayName, variable addToGraph, variable ErrorInterval, wave errorWave])
    variable  doAvg = 0
    if(paramisdefault(displayName))
        displayName = "" // top graph
    endif
    if(strlen(displayName)==0)
        GetWindow/Z kwTopWin title
        // print "s_value", s_value
        displayName = s_value // This is probably going to be ""
    endif
    if(WinType(displayName) != 1)
        print displayName, "is not a graph"
    else
        print displayName, "is a graph"
        doAvg = 1
    endif

    if(paramisdefault(addToGraph))
        addToGraph = 0
    endif

    if(doAvg && waveExists(avgWave))
        string tracesInGraph = tracenamelist(displayName, ";", 1)

        variable numTraces = itemsInList(tracesInGraph), iTrace = 0 
        make/Wave/N=(numTraces)/O root:wavesToAvg/Wave=wavesToAvg
        for(iTrace = 0; iTrace<numTraces;iTrace++)
            string thisTrace = StringFromList(iTrace, tracesInGraph)
            // print "trace", thisTrace, "ref", nameOfWave(traceNameToWaveRef("", thisTrace))
            wavesToAvg[iTrace] = traceNameToWaveRef(displayName, thisTrace)
        endfor

        // Error type
        variable ErrorType
        strswitch( ErrorTypeStr)
            case "none":
                ErrorType = 0
                break
            case "stdDev":
                ErrorType = 1
                break
            case "CI":
                ErrorType = 2
                break
            case "SEM":
                ErrorType = 3
                break
            default:
                ErrorType = 0 // default to none
                break
        endswitch

        if(ErrorType > 0)
            // If errorWave doesn't exist, append _error the the name of the avg wave
            // Save in the same data folder
            if(ParamIsDefault(errorWave) || !WaveExists(errorWave))
                string avgWaveN
                DFREF avgWaveDFR
                [avgWaveN, avgWaveDFR] = getWaveNameAndDFR(avgWave)
                make/O avgWaveDFR:$(avgWaveN + "_error")/Wave=errorWave
            endif
        endif

        if(ParamIsDefault(ErrorInterval))
            fWaveAverage_waveRefs(wavesToAvg, ErrorTypeStr, avgWave, errorWave = errorWave)
        else
            fWaveAverage_waveRefs(wavesToAvg, ErrorTypeStr, avgWave, ErrorInterval = ErrorInterval, errorWave = errorWave)
        endif

        if(addToGraph)
            AppendToGraph/W=$displayName avgWave
            if(ErrorType>0)
                ErrorBars/W=$displayName $(nameOfWave(avgWave)) SHADE= {0,4,(0,0,0,0),(0,0,0,0)},wave=(errorWave,errorWave)
            endif
        endif
    endif
end

///////////
/// removeAxes
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-04
/// NOTES: Remove the axes from a graph - useful for exporting traces to Igor
///////////
function removeAxes([string graphName])
    if(paramIsDefault(graphName))
        ModifyGraph nticks=0,noLabel=2,axThick=0 // default to active
    else
        ModifyGraph/W=$graphName nticks=0,noLabel=2,axThick=0
    endif
end

///////////
/// alignXaxes
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-09
/// NOTES: Align the x axes of the indicated graphs
/// Aligns graph2 to graph1
///////////
function alignXaxes(string graph1, string graph2)
    getAxis/W=$graph1/Q bottom
    SetAxis/W=$graph2 bottom, V_min, V_max
    SetAxis/W=$graph2/A=2/E=3 left
end

///////////
/// getWavesFromGraph
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-16
/// NOTES: If graphName is not provided, uses top graph
/// graphTypes is the bit parameter for traceNameList
/// Defaults to just normal waves
///
// function testDisplay()
//     Wave/Wave test = getWavesFromGraph()
//     edit test // will show gibberish, but you can use the wave
//     Wave fromGraph = test[2]
//     display fromGraph
// end
///////////
function/Wave getWavesFromGraph([string graphName, variable graphTypes])
    if(paramIsDefault(graphName))
        graphName = "" // default to top graph
    endif

    if(paramIsDefault(graphTypes))
        graphTypes = 1 // default to normal waves
    endif
    
    
    string traces = tracenamelist(graphName, ";", graphTypes)

    variable iTrace = 0, numTraces = itemsInList(traces), iWaves = 0

    make/Wave/N=0/O wavesFromGraph // wave of wave references
    for(iTrace=0; iTrace<numTraces; iTrace++)
        string trace = StringFromList(iTrace, traces)
        
        Wave traceWave = traceNameToWaveRef(graphName, trace)

        if(WaveExists(traceWave))
            redimension/N=(iWaves+1) wavesFromGraph
            wavesFromGraph[iWaves] = traceWave
            iWaves++
        endif
    endfor
    return wavesFromGraph
end

///////////
/// getTraceInfoFromGraph
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-01-15
/// NOTES: If graphName is not provided, uses top graph
/// graphTypes is the bit parameter for traceNameList
/// Defaults to just normal waves
///////////
function [Wave/Wave wavesFromGraph, Wave/T traceNs, Wave/T traceInfoFromGraph, string bottomLabel, string leftLabel, string legendString] getWavesAndTraceInfoFromGraph([string graphName, variable graphTypes])
    if(paramIsDefault(graphName))
        graphName = "" // default to top graph
    endif

    if(paramIsDefault(graphTypes))
        graphTypes = 1 // default to normal waves
    endif
    
    
    string traces = tracenamelist(graphName, ";", graphTypes)

    variable iTrace = 0, numTraces = itemsInList(traces), iWaves = 0

    make/Wave/N=0/O wavesFromGraph // wave of wave references
    make/T/N=0/O traceInfoFromGraph // wave of traceInfo
    make/T/N=0/O traceNs // wave of traceInfo
    for(iTrace=0; iTrace<numTraces; iTrace++)
        string trace = StringFromList(iTrace, traces)
        
        Wave traceWave = traceNameToWaveRef(graphName, trace)

        if(WaveExists(traceWave))
            redimension/N=(iWaves+1) wavesFromGraph, traceInfoFromGraph, traceNs
            wavesFromGraph[iWaves] = traceWave
            // string traceN = removeQuotes_fromString(trace)
            // traceInfoFromGraph[iWaves] = traceInfo(graphName, traceN, 0)
            // traceNs[iWaves] = traceN 
            traceInfoFromGraph[iWaves] = traceInfo(graphName, trace, 0)
            traceNs[iWaves] = trace
            iWaves++
        endif
    endfor

    bottomLabel = AxisLabel(graphName, "bottom")
    leftLabel = AxisLabel(graphName, "left")

    String list = AnnotationList(graphName)
    Variable i, numBoxes = ItemsInList(list)
    for(i=0;i<numBoxes;i+=1)
        String thisBox = StringFromList( i, list )
        String infoStr = AnnotationInfo(graphName, thisBox, 1)
        String text = AnnotationText(infoStr)
        String type = AnnotationType(infoStr) //  "Tag", "TextBox", "ColorScale", or "Legend"
        strswitch(type)
            case "legend":
                legendString = text
                break
            default:
                break
        endswitch
    endfor
    return [wavesFromGraph, traceNs, traceInfoFromGraph, bottomLabel, leftLabel, legendString]
end

///////////
/// makeColorTableWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-12
/// NOTES: Return a color table wave for a given number of regions
///////////
function/Wave makeColorTableWave(variable nRegions[, variable skipBlack])
    if(paramIsDefault(skipBlack))
        skipBlack = 1
    endif

    if(skipBlack)
        nRegions++
    endif
    
    make/FREE/N=(nRegions, 3) rgb_table
    if(nRegions <= 0)
        return rgb_table
    endif

    if(nRegions == 1) // black
        rgb_table[0][0] = 0 //r
        rgb_table[0][1] = 0 //g
        rgb_table[0][2] = 0 //b
    elseif (nRegions == 2) // black and magenta
        rgb_table[0][0] = 0 //r
        rgb_table[0][1] = 0 //g
        rgb_table[0][2] = 0 //b

        rgb_table[1][0] = 65535 //r
        rgb_table[1][1] = 0 //g
        rgb_table[1][2] = 26214 //b
    elseif (nregions == 3) // black, orange, and blue
        rgb_table[0][0] = 0 //r
        rgb_table[0][1] = 0 //g
        rgb_table[0][2] = 0 //b

        rgb_table[1][0] = 61814 //r
        rgb_table[1][1] = 35488 //g
        rgb_table[1][2] = 25186 //b
        
        rgb_table[2][0] = 26497 //r
        rgb_table[2][1] = 43433 //g
        rgb_table[2][2] = 53271 //b
    elseif (nregions == 4) // black, green, orange, purple
        rgb_table[0][0] = 0 //r
        rgb_table[0][1] = 0 //g
        rgb_table[0][2] = 0 //b

        rgb_table[1][0] = 6947 //r
        rgb_table[1][1] = 40395 //g
        rgb_table[1][2] = 30583 //b
        
        rgb_table[2][0] = 55610 //r
        rgb_table[2][1] = 24354 //g
        rgb_table[2][2] = 512 //b
        
        rgb_table[3][0] = 29959 //r
        rgb_table[3][1] = 28673 //g
        rgb_table[3][2] = 45875 //b
    elseif (nregions == 5) // black, green, orange, purple, magenta
        rgb_table[0][0] = 0 //r
        rgb_table[0][1] = 0 //g
        rgb_table[0][2] = 0 //b

        rgb_table[1][0] = 6947 //r
        rgb_table[1][1] = 40395 //g
        rgb_table[1][2] = 30583 //b
        
        rgb_table[2][0] = 55610 //r
        rgb_table[2][1] = 24354 //g
        rgb_table[2][2] = 512 //b
        
        rgb_table[3][0] = 29959 //r
        rgb_table[3][1] = 28673 //g
        rgb_table[3][2] = 45875 //b
        
        rgb_table[4][0] = 59367 //r
        rgb_table[4][1] = 10509 //g
        rgb_table[4][2] = 35507 //b
    else 
        variable iRegion = 0
        for(iRegion=0; iRegion<nRegions; iRegion++)
            string colors = returnColors(iRegion, nRegions)
            variable red = str2num(stringfromlist(0, colors))
            variable green = str2num(stringfromlist(1, colors))
            variable blue = str2num(stringfromlist(2, colors))

            rgb_table[iRegion][0] = red
            rgb_table[iRegion][1] = green
            rgb_table[iRegion][2] = blue
        endfor
    endif

    if(skipBlack)
        DeletePoints 0, 1, rgb_table
    endif

    return rgb_table
end