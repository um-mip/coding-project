function/S makeSavePanel([string hostPanel])
	if(WinType("saveInterface")!=0)
		// print "panelExists"
		KillWindow/Z saveInterface
	endif

	NewDataFolder/O savePanel
	DFREF savePanel = root:savePanel

	string/G savePanel:hostPanel
	SVAR/SDFR=savePanel gHost = hostPanel
	if(!ParamIsDefault(hostPanel))
		gHost = hostPanel
	else
		hostPanel = ""
	endif

	variable panelWidth = 11, panelHeight = 76
	variable panelX = 0, panelY = 5
	string panelName = "saveInterface"
	AGG_makePanel(panelX, panelY, panelX + panelWidth, panelY + panelHeight, panelName)

	DefaultGuiFont/W=# popup={"_IgorMedium",12,0}

	variable xPos = posRelPanel(0.02, "width")
	variable yPos = posRelPanel(0.01, "height")
	variable buttonWidth, buttonHeight
	[buttonWidth, buttonHeight] = xyRelPanel(0.9, 0.047)

	TitleBox hostPanelText title = "Host Panel:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight

	string panels = WinList("!saveInterface", ";", "win:64")
	string quote = "\""
	panels = quote + "none;" + panels + quote
	// print "host panel", hostPanel
	PopupMenu hostPanelToPick value = #panels, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = updateGraphsProc
	PopupMenu hostPanelToPick popmatch=hostPanel, bodyWidth = buttonWidth
	yPos += buttonHeight

	string graphs = returnGraphsInPanel(hostPanel)
	graphs = quote + graphs + quote	

	TitleBox graphsToPickText pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Pick a graph:", frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight
	PopupMenu graphsToPick value = #graphs, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, bodyWidth = buttonWidth
	yPos += buttonHeight

	Button dupGraphButton title = "REMAKE GRAPH", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = dupGraphProc
	yPos += buttonHeight * 1.2

	Button removeAxesButton title = "remove axes", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = removeAxesProc
	yPos += buttonHeight

	Button openCalibratorButton title = "open calibrator", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = openCalibratorProc
	yPos += buttonHeight

	TitleBox unitsText title = "Units:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight
	PopupMenu unitsToPick value = "in;cm;pts", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, bodyWidth = buttonWidth, fsize = 12, proc = updateSavePanelSelsProc
	yPos += buttonHeight

	string/G savePanel:units
	SVAR/SDFR = savePanel units
	if(strlen(units)>0)
		// print "units", units
		PopupMenu unitsToPick popmatch=units
	endif
	
	TitleBox widthText title = "Width:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight

	variable/G savePanel:width/N=width
	if(numType(width)!=0 || width == 0)
		width = 11
	endif
	SetVariable widthToPick value = width, title = " ", limits = {0, inf, 0.5}, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12
	yPos += buttonHeight

	TitleBox heightText title = "Height:", pos = {xPos, yPos}, limits = {0, inf, 0.5}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight

	variable/G savePanel:height/N=height
	if(numType(height)!=0 || height == 0)
		height = 5
	endif
	SetVariable heightToPick value = height, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12
	yPos += buttonHeight

	TitleBox fileTypeText title = "File type:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight
	PopupMenu fileTypeToPick value = "png;pdf;tiff;jpg;svg;eps;emf", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = updateSavePanelSelsProc, bodyWidth = buttonWidth
	yPos += buttonHeight

	string/G savePanel:fileType
	SVAR/SDFR = savePanel fileType
	if(strlen(fileType)>0) // if filetype exists 
		PopupMenu fileTypeToPick popmatch=fileType
	endif

	TitleBox suggestionText title = "For Illustrator\nPDF on Mac, EMF on Windows", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight *1.5}, frame = 0, anchor=LB
	yPos += buttonHeight * 1.5	
	Button copyButton title = "COPY", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = copyGraphButtonProc
	yPos += buttonHeight
	
	TitleBox fileNameText title = "File Name:", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, frame = 0, fstyle = 1, fsize = 12
	yPos += buttonHeight

	string/G savePanel:fileName
	SVAR/SDFR=savePanel fileName
	SetVariable fileNameToPick value = fileName, title = " ", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12
	yPos += buttonHeight
	
	Button saveButton title = "SAVE", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, fsize = 12, proc = saveGraphButtonProc
	yPos += buttonHeight
	// string fileName, string units, variable width, variable height, string fileType
	
	return panelName
end