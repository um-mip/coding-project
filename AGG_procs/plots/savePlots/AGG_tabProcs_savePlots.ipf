#include <Append Calibrator>

function copyGraphButtonProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)

		DFREF savePanel = root:savePanel
		NVAR/Z/SDFR = savePanel width, height

		updateSavePanelSelections()
		SVAR/SDFR=savePanel hostPanel, graphName, units, fileType

		string displayName
		if(strlen(hostPanel)>0)
			displayName = hostPanel + "#" + graphName
		else
			displayName = graphName
		endif

		savePlot(displayName, units = units, width = width, height = height, fileType = fileType)
		
		// print "host:", hostPanel, "graph:", graphName, "units:", units, "file type:", fileType, "width", width, "height", height
	endif
end

function saveGraphButtonProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)

		DFREF savePanel = root:savePanel
		NVAR/Z/SDFR = savePanel width, height

		updateSavePanelSelections()
		SVAR/SDFR=savePanel hostPanel, graphName, units, fileType, fileName

		string displayName
		if(strlen(hostPanel)>0)
			displayName = hostPanel + "#" + graphName
		else
			displayName = graphName
		endif
		
		if(strlen(fileName)==0)
			fileName = graphName
			print "defaulting file name to graph name"
		endif
		savePlot(displayName, fileName = fileName, units = units, width = width, height = height, fileType = fileType)
		
		// print hostPanel, graphName, units, fileType, width, height
	endif
end

function dupGraphProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)

		DFREF savePanel = root:savePanel
		NVAR/Z/SDFR = savePanel width, height

		updateSavePanelSelections()
		SVAR/SDFR=savePanel hostPanel, graphName, units, fileType, fileName

		string displayName
		if(strlen(hostPanel)>0)
			displayName = hostPanel + "#" + graphName
		else
			displayName = graphName
		endif

		DuplGraphInPanelSubwndw(displayName)
		PopupMenu hostPanelToPick popmatch="none", win = saveInterface
		updateSaveGraphList()
		// print hostPanel, graphName, units, fileType, width, height
	endif
end

function updateGraphsProc (P_struct) :PopUpMenuControl
	STRUCT WMPopUpAction &P_struct

	if(P_struct.eventCode == 2)
		updateSaveGraphList()
	endif
end

///////////
/// updateSaveGraphList
/// AUTHOR: Amanda
/// ORIGINAL DATE: 2022-05-04
/// NOTES: 
///////////
function updateSaveGraphList()
	updateSavePanelSelections()
	DFREF savePanel = root:savePanel
	SVAR/SDFR=savePanel hostPanel

	string graphs = returnGraphsInPanel(hostPanel)
	string quote = "\""
	graphs = quote + graphs + quote	

	print "graphs", graphs

	PopupMenu graphsToPick win=saveInterface, value = #graphs, popmatch = "*"
	updateSavePanelSelections()
end


function updateSavePanelSelsProc(P_struct) : PopUpMenuControl
	STRUCT WMPopUpAction &P_struct

	if(P_struct.eventCode == 2)
		updateSavePanelSelections()
	endif
end

function updateSavePanelSelections()
	DFREF savePanel = root:savePanel
	string/G savePanel:graphName, savePanel:units, savePanel:fileType
	SVAR/SDFR=savePanel hostPanel, graphName, units, fileType

	ControlInfo/W=saveInterface hostPanelToPick
	hostPanel = s_value
	if(stringmatch(hostPanel, "none"))
		hostPanel = ""
	endif

	ControlInfo/W=saveInterface graphsToPick
	graphName = s_value

	ControlInfo/W=saveInterface unitsToPick
	units = s_value

	ControlInfo/W=saveInterface fileTypeToPick
	fileType = s_value
end

function openSaveIntProc (B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
		string uData = B_Struct.userdata

		string hostPanel = B_Struct.win

		string panelName = makeSavePanel(hostPanel = hostPanel)
		PopupMenu graphsToPick popmatch=uData, win = $panelName
	endif
end

///////////
/// removeAxesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-04
/// NOTES: Remove axes for selected graph in save interface
///////////
function removeAxesProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		updateSavePanelSelections()
		SVAR/SDFR=savePanel hostPanel, graphName, units, fileType, fileName

		string displayName
		if(strlen(hostPanel)>0)
			displayName = hostPanel + "#" + graphName
		else
			displayName = graphName
		endif

		removeAxes(graphName = displayName)
	endif
	return 0
end

///////////
/// openCalibratorProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-05-04
/// NOTES: Open the calibrator panel
///////////
function openCalibratorProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		updateSavePanelSelections()
		SVAR/SDFR=savePanel hostPanel, graphName, units, fileType, fileName

		string displayName
		if(strlen(hostPanel)>0)
			displayName = hostPanel + "#" + graphName
			DoWindow/F $hostPanel
		else
			displayName = graphName
			DoWindow/F $graphName
		endif

		SetActiveSubwindow $displayName
		calibrator()
		
		scaleForScreenSize("calibratorPanel")

	endif
	return 0
end

