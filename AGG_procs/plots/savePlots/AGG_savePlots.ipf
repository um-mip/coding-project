#pragma rtGlobals=3		//Use modern global access method and strict wave access

///////////////////////////////////////////////////////
/// COPY, SAVE, or DUPLICATE PLOTS
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-04-10
/// NOTES: 
/// savePlot function has the most options
/// simpleCopyPlot will just copy a default PNG to the clipboard
/// These functions are called by button procedures in the save plot interface
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2022-05-04
/// NOTES: Trying to improve copy for use in Illustrator
/// When using the clipboard on a Windows computer, Illustrator won't paste a PDF
/// It will paste a EMF (enhanced metafile) which is what the default seems to be
/// if the graph is copied with ctrl+c.
/// The size of copied files does not match the exact export size, though 
/// the ratio seems to be correct. If the file is saved first, and then imported
/// into the Illustrator file, the size is correct, and PDFs work
///////////////////////////////////////////////////////

function savePlot(string displayName[, string fileName, string units, variable width, variable height, string fileType])
	if(WinType(displayName) == 1) // is a graph
		string cmdStr = ""
		cmdStr = "SavePICT/TRAN=1/EF=2/WIN=" + displayName
		if(!paramisdefault(width) && !paramisdefault(height))
			string sizeString = "(0, 0, " + num2str(width) + "," + num2str(height) +")"
			cmdStr += "/W=" + sizeString 
			if(!paramisdefault(units))
				strswitch( units )
					case "in":
						cmdStr += "/I"
						break
					case "cm":
						cmdStr += "/M"
						break
					default:
						break
				endswitch
			endif
		endif

		if(paramisdefault(fileType))
			fileType = "PNG"
		endif

		variable saveToClip = paramisdefault(fileName)

		cmdStr += "/E="
		strswitch(fileType)
			case "SVG":
				cmdStr += "-9"
				break
			case "PDF":
				if(cmpstr(IgorInfo(2),"Macintosh")==0)
					cmdStr += "-2" // Quartz PDF
				else
					cmdStr += "-8" // Igor PDF on windows
				endif
				break
			case "TIFF":
				cmdStr += "-7"
				break
			case "JPEG":
				cmdStr += "-6"
				break
			case "PNG":
				cmdStr += "-5"
				break
			case "EPS":
				if(saveToClip)
					cmdStr += "-2"
					if(cmpstr(IgorInfo(2),"Macintosh")==0)
						print "You selected EPS, but this can't be saved to clipboard - defaulting to pdf instead"
					else
						print "You selected EPS, but this can't be saved to clipboard - defaulting to EMF instead"
					endif
				else
					cmdStr += "-3"
				endif
				break
			case "EMF": // this should only be used on a windows computer
				if(cmpstr(IgorInfo(2),"Macintosh")==0)
					cmdStr += "-2"
					print "You selected EMF on a mac - defaulting to pdf instead"
				else
					cmdStr += "-2"
				endif
				break
			default:
				cmdStr += "-5" // default to PNG
				break
		endswitch
		
		if(saveToClip)
			fileName = "clipboard"
		else
			fileName += "." + fileType
		endif
		cmdStr += " as \"" + fileName + "\""
		print "save command", cmdStr
		execute cmdStr
	endif
end

function simpleCopyPlot(string displayName)
	SavePICT/Win=$displayName as "Clipboard"
end