function saveTable(string tableName[, string fileName]) // tablename has to be full name, host and all
    if(paramisdefault(fileName)) // user has chance when saving to change the name, so default should generally be fine
        fileName = tableName
        if(GrepString(tableName, ".*#")==1)
            string panelName, onlyTableName
            SplitString /E=("(.*#)(.*$)") tableName, panelName, onlyTableName
            fileName = onlyTableName
        endif
    endif
    SetActiveSubWindow $tableName // As far as i can tell, only way to save this with a table that's in a panel
    saveTableCopy/F=1/M="\n"/T=2/N=3 as fileName
end

// For a saveTable button. Add userdata = "tableName:nameOfTable;"
// optional if want a different file name, use userdata = "tableName:nameOfTable;fileName:nameOfFile"
// the table should be in the same window as the button
// if it is not in the same window, add "tableWindow:nameOfWindow;" to the userdata string
function saveTableProc (B_Struct) : ButtonControl
    STRUCT WMButtonAction &B_Struct

    if(B_Struct.eventCode == 2)
        string uData = B_Struct.userdata
        variable promptName = 0
        variable haveTableName = 0
        string tableName, fileName
        // print uData, "userdata"
        if(strlen(uData)>0)
            tableName = stringbykey("tableName", uData)
            // print tableName, "tableName"
            if(strlen(tableName)>0)
                string tableWindow = stringbykey("tableWindow", udata)
                if(strlen(tableWindow)==0)
                    tableWindow = B_Struct.win // default to same window as button
                endif
                string fullTableName = tableWindow + "#" + tableName
                if(WinType(fullTableName)==2) // if it's a table that exists
                    fileName = stringbykey("fileName", uData)
                    if(strlen(fileName)>0)
                        saveTable(fullTableName, fileName = fileName)
                    else
                        saveTable(fullTableName)
                    endif
                endif
            endif
        endif
    endif
end