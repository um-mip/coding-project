// RemoveWavesFromTable_AGG(tableName, matchStr)
//	This is a building-block for RemoveWavesFromWindow.
//	NOTE: This function NOW WORKS reliably if the table contains waves from other than the current data folder.
//	NOTE: This function no longer brings the table to the front.
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-07
/// NOTES: Modified to allow this function to work with a table that is a subwindow
/// matchStr = "*" to remove all
Function RemoveWavesFromTable_AGG(string tableName, string matchStr[, variable acceptChanges])
	if(paramIsDefault(acceptChanges))
        acceptChanges = 1 // default to accept
    endif

    if(acceptChanges)
        acceptChangesInTable(tableName)
    endif
    

	if( strlen(tableName) == 0 )
		tableName= WinName(0,2,1)
	endif

    if(!WinType(tableName) == 2)
        print "RemoveWavesFromTable_AGG: Provided table name", tableName, "doesn't match any existing tables"
        return NaN
    endif

    Variable i=0
    String listOfWavePaths=""
    String pathToWave
    do
        WAVE/Z w= WaveRefIndexed(tableName,i,3)
        if (!WaveExists(w))
            break										// all done
        endif
        String wn = NameOfWave(w)
        if( stringmatch(wn, matchStr) )
            pathToWave= GetWavesDataFolder(w,2)
            if( WhichListItem(pathToWave, listOfWavePaths) < 0 )	// avoid duplicates
                listOfWavePaths += pathToWave+";"
            endif
        endif
        i += 1
    while (1)
    i=0
    do
        pathToWave= StringFromList(i,listOfWavePaths)
        if( strlen(pathToWave) == 0 )
            break
        endif
        RemoveFromTable/W=$tableName $(pathToWave).l
        RemoveFromTable/W=$tableName $(pathToWave).i
        RemoveFromTable/W=$tableName $(pathToWave).d
        i += 1
    while (1)
End

///////////
/// getWavesInTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-07
/// NOTES: tableName can be a table that's a subwindow
/// matchStr "*" to get all waves
/// type - 1 = data column, 2 = index or dimension label columns, 3 = either
/// default to data column only
///////////
function/Wave getWavesInTable(string tableName, string matchStr[, variable type])
	if( strlen(tableName) == 0 )
		tableName= WinName(0,2,1)
	endif

    if(paramIsDefault(type))
        type = 1
    endif
    make/FREE=1/Wave/N=0 wavesInTable
    
    [string hostN, string subN] = splitFullWindowName(tableName)
    doWindow $hostN
	if (V_flag == 0)
        return wavesInTable
    endif
    Variable i=0
    do
        WAVE/Z w= WaveRefIndexed(tableName,i,3) // This includes both 
        if (!WaveExists(w))
            break										// all done
        endif

        String wn = NameOfWave(w)
        if( stringmatch(wn, matchStr) )
            wavesInTable[numpnts(wavesInTable)] = {w}
        endif
        i += 1
    while (1)
    return wavesInTable
End

///////////
/// acceptChangesInTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-10
/// NOTES: Accepts any in-progress changes from a table
/// first checks that the table exists
/// this is important to run before executing a function that depends
/// on info entered in a table. The user could have entered the last value
/// but if they don't move the focus out of that cell before clicking out
/// of the table, the changes are still in-progress
///////////
function acceptChangesInTable(string tableName)
    if(WinType(tableName)==2) // if it's a table that exists
        ModifyTable/w=$tableName entryMode = 0
        if(V_flag != 0) // entry in progress
            // Accept in-progress changes
            ModifyTable/w=$tableName entryMode = 1
        endif
    endif
end
