// make panel interface
macro buildKvABFpanel()

// build panel
KvABFpanel()

endmacro

function KvABFpanel()

//size
variable xwidth=1000,yheight=800, xpos, ypos, dx = 200, dy = 20
variable/G ABFchannel = 1 // zero based. current setup channel 0 is raw
variable/G actVstart=-100, actVstep=10
variable/G inactVstart=-100, inactVstep=10, inactEpisode=7

variable/G actXstart=0.5, actXend=1.0
variable/G inactXstart=0.647, inactXend=1.147

string/G act100str="", act40str="", inactstr=""
Display/k=1/N=KvABF
// Create panel on right with min height of 200 points, width of 100.
NewPanel/HOST=KvABF/N=KvABFpanel/EXT=0/W=(0,400,400,0)
// make act listbox

xpos = 10
ypos = 10

ypos += dy
PopupMenu popupActFile title="activation file", value=abfexp(), pos={ xpos, ypos }  // "Red;Green;Blue;"
PopupMenu popupActFile proc=popActFileProc
ypos += dy
SetVariable svAct_100 title="activation -100", value=act100str, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
ypos += dy
SetVariable svAct_40 title="activation -40", value=act40str, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"

// make act params: start, step, xstart, xend
ypos += dy
SetVariable svAct_Vstart title="act V start", value=actVstart, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos += dx
SetVariable svAct_Xstart title="X start", value=actXstart, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos = 10
ypos += dy
SetVariable svAct_Vstep title="act V step", value=actVstep, pos={ xpos, ypos }, size={200,20} 
xpos += dx// "Red;Green;Blue;"
SetVariable svAct_Xend title="X end", value=actXend, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos = 10
ypos += dy
button bActAnalysis title="act analysis", pos={ xpos, ypos }, size={200,20}
button bActAnalysis proc=bActAnalysis

ypos += 2*dy
PopupMenu popupINActFile title="INactivation file", value=abfexp(), pos={ xpos, ypos }  // "Red;Green;Blue;"
PopupMenu popupINActFile proc=popINactFileProc
ypos += dy
SetVariable svINAct_100 title="INactivation -100", value=INact100str, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
ypos += dy
SetVariable svINActEpisdoe title="INact sub episode", value=INActEpisode, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"

// make act params: start, step, xstart, xend
ypos += dy
SetVariable svINAct_start title="INact V start", value=INactVstart, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos += dx
SetVariable svINAct_Xstart title="X start", value=INactXstart, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos = 10
ypos += dy
SetVariable svINAct_step title="INact V step", value=INactVstep, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos += dx
SetVariable svINAct_Xend title="X end", value=INactXend, pos={ xpos, ypos }, size={200,20}  // "Red;Green;Blue;"
xpos = 10
ypos += dy
button bINActAnalysis title="INact analysis", pos={ xpos, ypos }, size={200,20}
button bINActAnalysis proc=bINActAnalysis

end 

Function PopINActFileProc(PU_Struct) : PopupMenuControl
	STRUCT WMPopupAction &PU_Struct
	SVAR g_inact = inactstr
	//SVAR g_act40 = act40str
	variable sel = nan
	string str = "", act100="", act40="", temp=""
	string act100list = ""
	string act40list = ""

variable int100=nan, int40=nan

	if( PU_Struct.eventCode == 2 )
		sel = PU_Struct.popnum
		str = PU_Struct.popstr // this the selected file name
		print PU_Struct.popnum, sel, str
		// populate -100, -40
		temp = returnABFcodefromFilename( str ) 
		g_inact = temp + "0"
		//g_act40 = temp + "-40"

		//clean graph
		string wn, tl = tracenamelist( "KvABF", ";", 1 )
		variable i, n=itemsinlist(tl)
		
		pauseupdate
		if(n>0)
			for( i = 0; i < n; i += 1 )
				wn = removequotes( stringfromlist( i, tl ) )
				WAVE w = $wn
				removefromgraph/W=KvABF $wn
			endfor
		endif
		doupdate

    act100list = listABFdatecodesPrefix( g_inact )
    displaywavelist2subwin( act100list, "KvABF")
    //act40list = listABFdatecodesPrefix( g_act40 )
    //displaywavelist2subwin( act40list, "KvABF", nowipe="nowipe" )

	else
		//print PU_Struct.eventcode
	endif

	return 0
End

Function PopActFileProc(PU_Struct) : PopupMenuControl
	STRUCT WMPopupAction &PU_Struct
	SVAR g_act100 = act100str
	SVAR g_act40 = act40str
	variable sel = nan
	string str = "", act100="", act40="", temp=""
	string act100list = ""
	string act40list = ""

variable int100=nan, int40=nan

	if( PU_Struct.eventCode == 2 )
		sel = PU_Struct.popnum
		str = PU_Struct.popstr // this the selected file name
		print PU_Struct.popnum, sel, str
		// populate -100, -40
		temp = returnABFcodefromFilename( str ) 
		g_act100 = temp + "-100"
		g_act40 = temp + "-40"

		//clean graph
		string wn, tl = tracenamelist( "KvABF", ";", 1 )
		variable i, n=itemsinlist(tl)
		
		pauseupdate
		if(n>0)
			for( i = 0; i < n; i += 1 )
				wn = removequotes( stringfromlist( i, tl ) )
				WAVE w = $wn
				removefromgraph/W=KvABF $wn
			endfor
		endif
		doupdate

    act100list = listABFdatecodesPrefix( g_act100 )
    displaywavelist2subwin( act100list, "KvABF")
    act40list = listABFdatecodesPrefix( g_act40 )
    displaywavelist2subwin( act40list, "KvABF", nowipe="nowipe" )

	else
		//print PU_Struct.eventcode
	endif

	return 0
End

function/S resamplewavelist( wl, newinterval, ext )
	string wl // wavelist
	variable newinterval // target resampling rate
	string ext // extension for new wave names
	variable i, n=itemsinlist(wl)
  string wn, newwn, outlist = ""
	for( i = 0; i < n; i += 1 )
		wn = stringfromlist( i, wl )
		newwn = wn + ext 
		duplicate/O $wn, $newwn
		WAVE nw = $newwn 
		resample/RATE=(1/newinterval) nw 
		outlist += newwn + ";"
  endfor
  return outlist
end 

Function bActAnalysis(B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct	
	SVAR g_act100 = act100str
	SVAR g_act40 = act40str
	NVAR g_Xs = actXstart, g_Xe = actXend
	NVAR g_Vstart = actVstart, g_Vstep = actVstep

	if( B_Struct.eventcode == 2 )
	  string act100list = "", act100list_rs = ""
	  string act40list = "", act40list_rs = ""
	  string sublist = ""
	  variable int40, int100

	  print "hi from act analysis button"

// get list of -100 waves
    act100list = listABFdatecodesPrefix( g_act100 )
     // displaywavelist2subwin( act100list, "KvABF")
    act40list = listABFdatecodesPrefix( g_act40 )
      //displaywavelist2subwin( act40list, "KvABF", nowipe="nowipe" )

      int100 = deltax( $stringfromlist( 0, act100list ) )
      int40 = deltax( $stringfromlist( 0, act40list ) )
      if( int40 != int100 )
      	print "WARNING! SAMPLING AT DIFFERENT INTERVALS!", int100, int40
      	if( int40 < int100 ) // resample to largest sampling interval
	      	act40list_rs = resamplewavelist( act40list, int100, "_rs" )
	      	act40list = act40list_rs
	      else
	      	act100list_rs = resamplewavelist( act100list, int40, "_rs" )
	      	act100list = act100list_rs
	      endif
 			endif

// do subtraction
		sublist = matchandsublist( 0, act100list, act40list )

// display result
    displaywavelist2subwin( sublist, "KvABF" )

    string suffix = "_act", strout=""


		strout = updateAct( sublist, g_Xs, g_Xs+0.1, suffix, Vstart=g_Vstart/1000, Vstep=g_Vstep/1000, Vzero=-0.1 ) // xxx add "svR" option, xxx add Vrev option

		print strout

  	endif
	return 0
End

Function bINActAnalysis(B_Struct) : ButtonControl
	STRUCT WMButtonAction &B_Struct	
	SVAR g_inact = inactstr
	NVAR g_inactEpisode = inactEpisode
	NVAR g_Xs = inactXstart, g_Xe = inactXend
	NVAR g_Vstart = inactVstart, g_Vstep = inactVstep

	if( B_Struct.eventcode == 2 )
	  string inactlist = ""
	  string sublist = ""

	  print "hi from INact analysis button"

// get list of -100 waves
   inactlist = listABFdatecodesPrefix( g_inact )

   //print "inactlist", inactlist
   
// do subtraction
		sublist = subTracesPanel( g_inactEpisode, inactlist )

		//print "sublist", sublist

// display result
    displaywavelist2subwin( sublist, "KvABF" )

    setaxis bottom 0.9*g_Xs, 1.1*g_Xe

    string suffix = "_inact", strout = ""
    strout = updateInact( sublist, g_Xs, g_Xe, suffix, Vstart= g_Vstart/1000, Vstep= g_Vstep/1000 ) 

    print strout

  	endif
	return 0
End

function/s returnABFcodefromFilename( fn ) //abfFileNameDecoder( wn )
string fn 
//use GREP to return abf date code
string regExp="([[:digit:]]+)([[:alpha:]])([[:alpha:]]+)-?([[:digit:]]+)\.([[:alpha:]]+)"
string datecode, letter, protocol, voltage, extension 

splitstring /E=(regExp) fn, datecode, letter, protocol, voltage, extension //, episode, channel

//print  "in returnABFcode", fn, datecode, letter, protocol, voltage, extension //, episode, channel
return datecode + letter + protocol
end 

function/s abfWaveNameDecoder( wn )
string wn 
//use GREP to return abf date code
string regExp="([[:digit:]]+)([[:alpha:]])([[:alpha:]]+)-?([[:digit:]]+)e([[:digit:]]+)c([[:digit:]]+)"
string datecode, letter, protocol, voltage, episode, channel

splitstring /E=(regExp) wn, datecode, letter, protocol, voltage, episode, channel

//print  wn, datecode, letter, protocol, voltage, episode, channel
return datecode + letter
end 

function abfChannelDecoder( wn )
string wn
variable ch
//use GREP to return channel number //abf date code
string regExp="([[:digit:]]+)([[:alpha:]])([[:alpha:]]+)-?([[:digit:]]+)e([[:digit:]]+)c([[:digit:]]+)"
string datecode, letter, protocol, voltage, episode, channel

splitstring /E=(regExp) wn, datecode, letter, protocol, voltage, episode, channel

//print  wn, datecode, letter, protocol, voltage, episode, channel
ch = str2num(channel)
return ch
end 

function/S abfExtensionDecoder( wn )
string wn
string ext = ""
//use GREP to return channel number //abf date code
string regExp="([[:digit:]]+)([[:alpha:]])([[:alpha:]]+)-?([[:digit:]]+)e([[:digit:]]+)c([[:digit:]]+)_?([[:alpha:]]+)"
string datecode, letter, protocol, voltage, episode, channel, extension

splitstring /E=(regExp) wn, datecode, letter, protocol, voltage, episode, channel, extension

//print  wn, datecode, letter, protocol, voltage, episode, channel
ext = extension
return ext
end 

function/s listABFdatecodesPrefix( prefix ) // eliminates based on channel here!!!

string prefix // datecode, letter, protocol, voltage, etc
NVAR ch = ABFchannel
//print "prefix", ch 

// lists all waves as interpreted by ABF filename
string wl = wavelist( prefix+"*", ";", ""), wn="", ext=""
variable i, n=itemsinlist(wl)
string outlist = ""
for( i = 0; i < n; i += 1 )
  wn = stringfromlist( i, wl )
  // print wn, "channel:", abfChanneldecoder( wn )
  // check for modified waves with extensions
  ext = abfextensiondecoder( wn )
  if( strlen(ext) > 0 )
  	print "warning extension detected in abfdatecode list", wn, ext
  elseif( abfChanneldecoder( wn ) == ch )
  	outlist += wn + ";"
  endif
endfor
//print "pr4efix", outlist
return outlist

end 

function/s listABFdatecodes( feature, factor )
string feature // datecode, letter, protocol, voltage, etc
string factor // actual value, i.e. for protocol "act"

// lists all waves as interpreted by ABF filename
string wl = wavelist( "*"+factor+"*", ";", "")
variable i, n=itemsinlist(wl)

for( i = 0; i < n; i += 1 )
  print abfwavenamedecoder( stringfromlist( i, wl ) )
endfor

end 

//make list of experiments from file list
function/s ABFexp(  )
string explist = ""
controlinfo/W=BlastPanel10_0 filelist // gets list of files direct from panel
print s_value
WAVE/T lw = $s_value
//print lw
variable i, n=numpnts( lw )
for( i=0; i<n; i+=1 )
  if( strlen( lw[i] ) > 1 )
  	 explist += lw[i] + ";"
  endif
endfor

return explist

end