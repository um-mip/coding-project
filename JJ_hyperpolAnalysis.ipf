function testFunction(Wave thisWave)
	variable minpeak,peaktime //create a variable, string, etc. for the variables you want to pull out
	[minpeak,peaktime]=getsagpeak(thiswave, doSmoothing = 0) //this function gives a value to those variables, can adjust the start and end times here so the function defaults to NOT pulling the 
	//the original values that you set 
	print "min peak value is:", minPeak, "peak time value is:", peaktime

end

function [variable minPeak, variable peakTime] getSagPeak (Wave thisWave [, Variable startTime, Variable endTime, Variable doSmoothing])

    // startTime and endTime are optional parameters
    // Since this should be the same for all of your waves,
    // we'll set sensible defaults, but this makes it easier for someone
    // else to use this function in the future with different PGFs
    if(ParamIsDefault(startTime))
        startTime = 0.5 // To-do: Change to default seconds
    endif

    if(ParamIsDefault(endTime))
        endTime = 0.65 // To-do: Change to default seconds
    endif
    
    if(ParamIsDefault(doSmoothing))
    	doSmoothing = 1 // Default to smooth the wave
    endif
    
    duplicate/O thisWave, waveToAnalyze
    
    if(doSmoothing == 1)
    	print "trying to smooth"
    	Smooth/B 9, waveToAnalyze
    endif

    // get all of the stats for the part of the wave between the start and end time
    // this is the /R flag
    // the /Q flag keeps it from printing this to the console
    WaveStats/Q/R=(startTime, endTime) waveToAnalyze

    // If just need the peak without corresponding time, this is probably faster
    // minPeak = WaveMin(thisWave, startTime, endTime)
    // This will find the minimum value and the corresponding time 
    minPeak = V_min
    peakTime = V_minloc

    return [minPeak, peakTime]
end

function getSteadyStateAvg (Wave thisWave [, Variable startTime, Variable endTime])

    if(ParamIsDefault(startTime))
        startTime =0.98995 // To-do: Change to default seconds
    endif

    if(ParamIsDefault(endTime))
        endTime = 0.99995 // To-do: Change to default seconds
    endif

    variable thisWaveAvg = mean(thisWave, startTime, endTime)
    
   // print "steadystageaverage", thiswaveavg

    return thisWaveAvg

end

function [variable maxPeak, variable peakTime] getReboundPeak (Wave thisWave [, Variable startTime, Variable endTime, Variable doSmoothing])

    if(ParamIsDefault(startTime))
        startTime = 1.0 // To-do: Change to default seconds
    endif

    if(ParamIsDefault(endTime))
        endTime = 1.2 // To-do: Change to default seconds
    endif

    if(ParamIsDefault(doSmoothing))
    	doSmoothing = 1 // Default to smooth the wave
    endif
    
    duplicate/O thisWave, waveToAnalyze
    
    if(doSmoothing == 1)
    	print "trying to smooth"
    	Smooth/B 9, waveToAnalyze
    endif

    // get all of the stats for the part of the wave between the start and end time
    // this is the /R flag
    // the /Q flag keeps it from printing this to the console
    WaveStats/Q/R=(startTime, endTime) waveToAnalyze

    // If just need the peak without corresponding time, this is probably faster
    // maxPeak = Wavemax(thisWave, startTime, endTime) the Q tells the computer to NOT print each piece of info to the console
    maxPeak = V_max
    peakTime = V_maxloc

    return [maxPeak, peakTime]
end

function testFunction2(Wave thisWave)
	variable maxpeak,peaktime //create a variable, string, etc. for the variables you want to pull out
	[maxpeak,peaktime]=getreboundpeak(thiswave) //this function gives a value to those variables, can adjust the start and end times here so the function defaults to NOT pulling the 
	//the original values that you set 
	print "max peak value is:", maxPeak, "peak time value is:", peaktime

end

function getBaselineAverage(Wave thisWave  [, Variable startTime, Variable endTime])
    if(ParamIsDefault(startTime))
        startTime = 0.49 // To-do: Change to default seconds
    endif

    if(ParamIsDefault(endTime))
        endTime = 0.5 // To-do: Change to default seconds
    endif

    variable thisWaveAvg = mean(thisWave, startTime, endTime)
    
print "baselineavg:", thiswaveavg

    return thisWaveAvg
end


function testFunction3(Wave thisWave)
	variable sagPeak, sagPeakTime,  SteadyStateAvg,  reboundPeak, reboundPeakTime, baselineAvg //create a variable, string, etc. for the variables you want to pull out
	[sagPeak, sagPeakTime,  SteadyStateAvg,  reboundPeak, reboundPeakTime, baselineAvg]=getHyperpolVals(thiswave) //this function gives a value to those variables, can adjust the start and end times here so the function defaults to NOT pulling the 
	//the original values that you set 
	print sagPeak, sagPeakTime,  SteadyStateAvg,  reboundPeak, reboundPeakTime, baselineAvg 

end

function [variable sagPeak, variable sagPeakTime, variable SteadyStateAvg, variable reboundPeak, variable reboundPeakTime, variable baselineAvg]  getHyperpolVals (Wave thisWave [,Variable doSmoothing])
		
	if(ParamIsDefault(doSmoothing))
    	doSmoothing = 1 // Default to smooth the wave
    endif
    
    [sagPeak, sagPeakTime] = getSagPeak(thisWave, doSmoothing = doSmoothing)
    
    SteadyStateAvg = getSteadyStateAvg(thisWave)
    
    [reboundPeak, reboundPeakTime] = getReboundPeak(thisWave, doSmoothing = doSmoothing)
    
    baselineAvg = getBaselineAverage(thisWave)

    return [sagPeak, sagPeakTime, SteadyStateAvg, reboundPeak, reboundPeakTime, baselineAvg]
end

Structure JJ_hyperpol
    WAVE/T hyperpolWaves
    WAVE sagPeaks, sagPeakTimes, SteadyStateAvgs,  reboundPeaks, reboundPeakTimes, baselineAvgs
ENDSTRUCTURE

function analyzeHyperpolWave(Wave waveToAnalyze [, variable doSmoothing])

	if(ParamIsDefault(doSmoothing))
    	doSmoothing = 1 // Default to smooth the wave
    endif
    
    variable sagPeak, sagPeakTime, SteadyStateAvg, reboundPeak, reboundPeakTime, baselineAvg
    [sagPeak, sagPeakTime, SteadyStateAvg,  reboundPeak, reboundPeakTime, baselineAvg] = getHyperpolVals(waveToAnalyze, doSmoothing = doSmoothing)
		
	Struct JJ_hyperpol JJ_hyperpol 
    StructFill /AC = 3 JJ_hyperpol // get or make the waves for summary table if they don't exist yet

    if(V_error)
        print "no data"
        return -1
    endif

    // Count the current number of rows in the wave
    variable numWavesSummarized = numpnts(JJ_hyperpol.hyperpolWaves)
    print "There have already been", numWavesSummarized, "waves summarized before"

    // Add one more point to all of the waves
    redimension /N=(numWavesSummarized + 1) JJ_hyperpol.hyperpolWaves, JJ_hyperpol.sagPeaks, JJ_hyperpol.sagPeakTimes, JJ_hyperpol.SteadyStateAvgs,  JJ_hyperpol.reboundPeaks, JJ_hyperpol.reboundPeakTimes, JJ_hyperpol.baselineAvgs

    // Add the values from this analyzed wave to the table
    JJ_hyperpol.hyperpolWaves[numWavesSummarized] = nameOfWave(waveToAnalyze)
    JJ_hyperpol.sagPeaks[numWavesSummarized] = sagPeak
    JJ_hyperpol.sagPeakTimes[numWavesSummarized] = sagPeakTime
    JJ_hyperpol.SteadyStateAvgs[numWavesSummarized] = SteadyStateAvg
    JJ_hyperpol.reboundPeaks[numWavesSummarized] = reboundPeak
    JJ_hyperpol.reboundPeakTimes[numWavesSummarized] = reboundPeakTime
    JJ_hyperpol.baselineAvgs[numWavesSummarized] = baselineAvg
end

function analyzeAllHyperpolWaves(Wave/T listWavesToAnalyze [, variable doSmoothing])

	if(ParamIsDefault(doSmoothing))
    	doSmoothing = 1 // Default to smooth the wave
    endif
    
    // make the summary waves
	make/O/N=0 sagPeaks, sagPeakTimes, SteadyStateAvgs, reboundPeaks, reboundPeakTimes, baselineAvgs
	make/O/T/N=0 hyperpolWaves
	
    // count the number of waves to be analyzed
	variable numWaves = numpnts(listWavesToAnalyze)
	print "There are", numWaves, "to analyze"

    // start with the first wave
	variable iWave = 0

    // for each wave
	for (iWave = 0; iWave < numWaves; iWave ++)
			print "We're looking at the wave with index", iWave
        // get the name of the wave to be analyzed from the full list
		string thisWave = listWavesToAnalyze[iWave]
		Wave waveToAnalyze = $thisWave
        // if the wave exists, analyze it
		if(WaveExists(waveToAnalyze))
			print "the wave exists"
			analyzeHyperpolWave(waveToAnalyze, doSmoothing = doSmoothing)
		else
			print "the wave doesn't exist"
		endif
	endfor
	
    // show the summary table
	edit/K=1/N = hyperpolSummary hyperpolWaves, sagPeaks, sagPeakTimes, SteadyStateAvgs,  reboundPeaks, reboundPeakTimes, baselineAvgs
end

function makehyperpollist()
	
make/T/O/N=0 listallwavesfromcollector
	end

function pullfromCOLLECTOR()
	wave/T list_series = serieslistw // you can update the list in collector as many times during this analysis
	if(WaveExists(list_series))
			print "the wave exists"
	else
		print "couldn't find collector wave list"
	endif
	
wave/T listallwavesfromcollector

variable numwavestoanalyze=numpnts (listallwavesfromcollector)
	
	variable numpointsinlist= numpnts(list_series)
	variable iwave=0
	for (iwave=0; iwave<numpointsinlist; iwave++)
		string thiswavename=list_series[iwave]
		string wavesearch= thiswavename+"sw*t1"
//		string currentSearch = thiswavename + "sw*2" // you could ultimately use this to make a different list of current sweep names and then calculate the current applied for the sweep
		string matchingwaves= WaveList(wavesearch, ";", "")
		variable numbersweeps= itemsinlist(matchingwaves)
		variable isweep=0
		for (isweep=0; isweep<numbersweeps; isweep++)
		string sweepname=stringfromlist(isweep,matchingwaves)
		
		redimension /n=(numwavestoanalyze+1) listallwavesfromcollector
		listallwavesfromcollector[numwavestoanalyze]=sweepname
		
		numwavestoanalyze++ //++ adds one
		
		print "the matching sweeps are", sweepname
		endfor
		
	endfor
end

function testFunc4()
	string theWaves = WaveList("20210908ag1s14sw*t1", ";", "")
	print theWaves
end