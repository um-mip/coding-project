10#pragma rtGlobals=3		// Use modern global access method and strict wave access.
strconstant ksCdataPath = "collector_data", ksCollectorPanelName="the_Collector"
strconstant ksExpListwn="explistw",  ksExpSelwn="expselw",ksLabelListwn="labellistw", ksLabelSelwn="labelselw"
strconstant ksSeriesListwn="serieslistw", ksSeriesSelwn="seriesSelw", ksExpgrplistwn="expGrpListw", ksExpgrpselwn="expGrpSelw"
constant kvgrey=5, kvwhite=0, kvNumSweeps=20, kvNumTraces=4

// reads every .dat file in a directory, creates a correlated  list of all PGF labels

Function DFile(select[, panelName])
	variable select // 0 use default, 1 select
	string panelName

	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif
	
	
	/// wave names from panel
//	string explistwn=ksExplistwn, expselwn=ksExpselwn,labellistwn=ksLabellistwn, labelselwn=ksLabelselwn
	WAVE/T explistw = $ksexplistwn
	WAVE expselw = $ksexpselwn
	WAVE/T labellistw = $kslabellistwn
	WAVE labelselw = $kslabelselwn
	
	String pathName = "Igor", pathstring="" // Refers to "Igor Pro Folder"
	string extension=".dat"
	// Get a semicolon-separated list of all files in the folder
	if(select)
		string message="select a folder"
		open /D/R/M=message/T=extension refnum
		if(strlen(s_filename) == 0) // user canceled
			print "DFile in Collector: user canceled on file selected. Stopping here"
			return NaN
		endif
		pathstring = parsefilepath(1,s_filename, ":",1,0)
		newPath /O collector_data pathstring
	endif
	
	variable t0=ticks,t1
	
	String list = IndexedFile(collector_data, -1, extension)
	Variable numItems = ItemsInList(list)
	// Sort using combined alpha and numeric sort
	list = SortList(list, ";", 16)
	
	// exp card info	
	String expext=".txt", expcardlist = IndexedFile(collector_data,-1,expext)
	variable ncard=itemsinlist(expcardlist)
	expcardlist = SortList(expcardlist,";",16)
		
	// Process the list

	redimension/N=(numitems,1) explistw
	redimension/N=(numitems,1,-1) expselw

	// AGG 2024-06-03, set sel wave values back to 0 when loading new files
	expSelW = 0
	
	Variable i,refnum
	
	string labellist="",explist="",labelwn
	variable success=0	

	for(i=0; i<numItems; i+=1)
		String fileName = StringFromList(i, list)
		explistw[i]=datecodefromfilename(filename) //filename
		
	//	Print i, fileName
	//	open .dat file
		open /R /P=collector_data refnum as filename
	//	scan series	
		labelwn = returnserieslist(0,refnum,filename,"","")
		WAVE/T labelw = $labelwn
//		if(i==0)
//			edit/k=1 labelw
//		else
//			appendtotable labelw
//		endif
		close refnum
		mergew(labelwn, kslabellistwn)
		// match labellistw and labelselw
		redimension/N=(numpnts(labellistw),1,-1) labelselw
		labelselw = 0 // AGG 2024-06-03, reset label colors to 0

		variable fillCard = getCBVal("fillExpCardCheck", panelName = panelName)
		if(fillCard)
			success=readexpcard(explistw[i])
			if(!success)
				expselw[i][0][2] = kvGrey
			else
				expselw[i][0][2] = kvwhite
			endif
		endif
	endfor

	if(numItems>=1 && fillCard)
		success=readexpcard(explistw[0])
		if(getCBVal("showExpCardCheck", panelName = panelName))
			DoWindow/F experimentCard
		endif
	endif

	ListBox list_exp selRow = -1, win = $panelName
	ListBox list_label selRow = -1, win = $panelName
	ListBox list_series selRow = -1, win = $panelName

	clearCollectorSeriesList()

	t1=(ticks-t0)/60
	print "Dfile time: ", (ticks-t0)/60,"; avg time per exp:",t1/numitems
end

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// \\\\\\\///////// DISPLAY SWEEPS FROM ALL SERIES IN LISTBOX !!!! \\\\\\\//////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// given a datecode and a series number, import all sweeps and traces associated with series number
function importseriesALL([string panelName]) // import a series (and associated sweeps and traces) from a file
	string datecode 
	variable seriesnum	
	variable t0=ticks,t1
	Variable i,refnum

	if(paramIsDefault(panelName))
		panelName = "the_collector"
	endif
	
	
	// get list from series listbox
	string lbname = "list_series" // hardcoded for now!
	string seriesL = returnStringListfromLB( lbname )
	variable nseries = itemsinlist( seriesL )
	string sweeplist="", tracelist="", thisseries=""
	
	string filename = "", mywavelist = ""
	
	for( i = 0 ; i < nseries ; i += 1 )
		
		thisSeries = stringfromlist( i, seriesL )
		datecode = datecodeGREP2( thisseries )
		seriesnum = seriesnumber( thisseries )
		filename = filenamefromdatecode(datecode)

		Print i, fileName
		open /R /P=collector_data refnum as filename

		mywavelist += returnserieslist(0,refnum,filename,"",num2str(seriesnum)) + ";"
		
		close refnum
		
	endfor	
	sweeplist = chkstatus("checkSW",20, panelName = panelName)
	tracelist = chkstatus("checkTR",4, panelName = panelName)
	displaywavelist( mywavelist, sweeplist, tracelist, collectorPanelName = panelName )

	t1=(ticks-t0)/60
	print "imported series time: ", (ticks-t0)/60, " ; for series number:", nseries
	print "testing", sweeplist 
end

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
function/s returnstringlistfromLB( lbname )
string lbname
string LBlist="", listwn=""

controlinfo $lbname
listwn = S_value
WAVE/T listw = $listwn

variable item=0, nitems = numpnts( listw )

for(item=0; item<nitems; item+=1 )
	lblist += listw[item] + ";"
endfor

return lbList
end

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// \\\\\\\///////// CAUTION HEAVY LIFTING \\\\\\\//////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// given a datecode and a series number, import all sweeps and traces associated with series number
function importseries( datecode, series, [ app, panelName] ) // import a series (and associated sweeps and traces) from a file
	string datecode, panelName
	variable series
	variable app // append = 1 to not clear, default is clear
	
	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif
	
	
	variable t0=ticks,t1
	Variable i, n, refnum
	
	string filename = filenamefromdatecode(datecode),mywavelist

		//Print i, fileName
		open /R /P=collector_data refnum as filename
		
		mywavelist = returnserieslist(0,refnum,filename,"",num2str(series) ) //, AmpStemp = Amp )
		string sweeplist="",tracelist=""

		sweeplist = chkstatus("checkSW",20, panelName = panelName)
		if( strlen( sweeplist ) == 0 )
			print "test tr"
			setchkboxfromWaveList( "checkSW", mywavelist )
			n = itemsinlist( mywavelist )
			sweeplist = ""
			for( i=1; i<=n; i+=1 )
				sweeplist += num2str( i ) + ";"
			endfor
		endif // display something if no checkboxes

		tracelist = chkstatus("checkTR",4, panelName = panelName)
		if( strlen( tracelist ) == 0 )
//			print "test tr"
//			setchkboxfromWaveList( "checkTR", sweeplist )			
		endif // display something if no checkboxes

		if( paramisdefault( app ) )
			displaywavelist( mywavelist, sweeplist, tracelist, collectorPanelName = panelName )
		else
			displaywavelist( mywavelist, sweeplist, tracelist, app=app, collectorPanelName = panelName )
		endif				
		close refnum
		
	t1=(ticks-t0)/60
	print "import series time: ", (ticks-t0)/60, " seconds"
	//print sweeplist
end

///////////
/// updateCollectorDisplayOnlyProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: 
///////////
function updateCollectorDisplayOnlyProc(CB_Struct) : CheckBoxControl
	STRUCT WMCheckboxAction & CB_Struct
	if(CB_Struct.eventcode == 2) // Mouse up
		updateCollectorDisplayOnly(panelName = CB_Struct.win)
	endif
	return 0
end


///////////
/// updateCollectorDisplayOnly
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: update the collector display without importing. To be used if checkboxes change
///////////
function updateCollectorDisplayOnly([string panelName])
	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif

	removealltraces(panelName + "#G0")
	
	string series = getSelectedItem("list_series", hostName = panelName)
	string swList = chkstatus("checkSW", 20, panelName = panelName)
	if(strlen(swList) == 0)
		return NaN
	endif

	string traceList = chkstatus("checkTr", 4, panelName = panelName)
	if(strlen(traceList)==0)
		return NaN
	endif

	variable nSweeps = itemsinlist(swList), iSweep = 0, nTraces = itemsinlist(traceList), iTrace = 0
	string wList = ""
	for(iSweep=0; iSweep<nSweeps; iSweep++)
		string thisSweep = StringFromList(iSweep, swList)
		for(iTrace=0; iTrace<nTraces; iTrace++)
			string thisTrace = StringFromList(iTrace, traceList)
			string waveN = series + "sw" + thisSweep + "t" + thisTrace
			Wave/Z thisWave = $waveN
			if(WaveExists(thisWave))
				wList += waveN + ";"
			endif
		endfor
	endfor

	displaywavelist(wList, swList, tracelist, collectorPanelName = panelName)
end

///////////////////////////////////////////////
function/S importtrace(datecode,series,sweep,trace) // import a series (and associated sweeps and traces) from a file
	string datecode 
	variable series, sweep, trace
	
	variable t0=ticks,t1
	Variable i,refnum
	
	string filename = filenamefromdatecode(datecode),wn=""

//		Print i, fileName
		open /R /P=collector_data refnum as filename

		wn = returnserieslist( 0, refnum, filename, "", num2str(series), sweepn=sweep, tracen=trace )
		
		close refnum
		
	t1=(ticks-t0)/60
//	print series, sweep, trace, wn, "import trace time: ", (ticks-t0)/60
	return wn
end
//\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ ////////////////////////
/////////////////////////////////////////////////////////////
///////\\\\\\ CAUTION HEAVY LIFTING ///////\\\\\\\\\\\\\\\\\
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

function displaywavelist( wlist, swlist, tlist, [app, collectorPanelName] )
string wlist, swlist, tlist, collectorPanelName // list strings containing waven's, sweeps, and traces to display
variable app // append  =1 default is clear
variable i,n=itemsinlist(wlist),nsweeps=itemsinlist(swlist),j, ntraces=itemsinlist(tlist),k
variable swn, trn
string wn,axisname="",axispre="trace", axislabel

if(paramIsDefault(collectorPanelName))
	collectorPanelName = "the_Collector"
endif

dowindow/F $collectorPanelName
string displayN = collectorPanelName + "#G0"
setactivesubwindow $displayN

if( paramisdefault( app ) )
	removealltraces(displayN)
else 
	modifygraph rgb=(0,0,0)
endif 

for(i=0;i<n;i+=1)
	wn = stringfromlist(i,wlist)
	WAVE/Z w = $wn
	if(waveexists( w ))
		swn = sweepnumber(wn)
		trn = tracenumber(wn)
		//loop over sweeps in sweep list
		for(j=0; j<nsweeps; j+=1)
			if( swn == str2num( stringfromlist( j, swlist ) ) )
				//loop over traces in tracelist				
				for ( k=0; k<ntraces; k+=1 )
					axisname=axispre+num2str(k)
					if ( trn == str2num(stringfromlist(k,tlist)) )
						if(mod(k,2)==0)
							appendtograph/L=$axisname  w
							ModifyGraph freePos($axisname)=0
							ModifyGraph lblPosMode($axisname)=1
							axislabel = "trace "+ num2str( k + 1 ) + " ( \\U )"
							Label $axisname axislabel
							if (ntraces>1)
								ModifyGraph axisEnab( $axisname )={0.2,1.0}
							endif
						else
							appendtograph/R=$axisname  w
							ModifyGraph axisEnab( $axisname )={0,0.2}
							ModifyGraph rgb( $wn )=(0,0,0)
							ModifyGraph freePos( $axisname )=0
							ModifyGraph lblPosMode( $axisname )=1
							axislabel = "trace "+ num2str( k + 1 ) + " ( \\U )"
							Label $axisname axislabel						
						endif
						modifygraph rgb( $wn ) = ( 65535, 0 , 0 )				
						k=inf
					endif
				endfor // loop over traces
			endif
		endfor // loop over sweeps
	else
		print "in display: i, n, no wave:", i, n, wn
	endif
endfor // loop over waves

if( paramisdefault( app ) )
	rainbow() // ! because
else 
	
endif 


end

////////////////////////////////////////////////////////////////////
//
//		//		reads open patchmaster bundle, returns either list of labels OR wavenames from import series list (contains series numbers)
//
// use importserieslist="" to return labels only !!!
//  import series list is a string list of series numbers, e.g. "1;2;3;4;"
//
////////////////////////////////////////////////////////////////////
//item = whats requested (0 returns labels)
//refnum = file needs to be opened before, (not opening itself, better be opened first)
//filename2 = passing filename through  (.dat file)
//showFiles (go back to understand) 
//importserieslist = import multiple series in one call, whatever calls it has to know whatever that series is
//brackets are optional  (rescale = deals with units since patchmaster saves them differently than what experimenters need) 
function/s returnserieslist(item,refnum,filename2,showFiles,importserieslist, [sweepn, tracen, slabel, rescale] ) //, ampStemp ])
variable item, refnum		// item =0 gives labels only! 

//show files =0 none 1 stim 2 pulsed, 3 amp
string filename2,showFiles
string importserieslist //list of series to import, causes return of a string of wavenames of imported data
variable sweepn, tracen
string slabel // option to get only series with matching label
variable rescale


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////ADDING IN STUFF TO READ CUSTOM AMPLIFIER STUFF////////////////////////
		STRUCT ampSeriesArr	AmpStructArr		// ampSeriesArr is defined in HekaFileDefs, 3 amps, 100 series at this time 20220323
// access is Amp.AmpNumber[0-2].AmpSeries[0-99] = ...amplifierStateRecord.AmAmplifierState
		variable AmpIndex = 1 // 0 based number of amp, 0 1 2 for EPC10 trippel
		string datecode = datecodeGREP2( filename2 )
		string tempstr = getAmpStateStruct( refnum, datecode, AmpStructArr )
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////AMP PANEL STUFF/////////////////////////////////////////////
		
		//create the elements wave for storage
		createAmpWaveName()
		
		//create the values wave for storage
		createAmpValues(AmpStructArr)
		
		//can make macro, can put a button on collector
			//shortest path is to probably make a macro to call it
			
//		DoWindow/F Parameter_Panel    // bring panel to the front if it exists
//		if( V_Flag == 0 )       // panel doesn't exist
//    		WaveParametersPanel() 
//		endif	

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


// cannot predict interleave, first attempt 20220115
variable interleaveThreshold = inf /// 600000
variable interleaveSize = nan 
variable interleaveSkip = nan 
variable interleaveFlag = 0
string tempwn1a="", tempwn1b="", tempwn2a="", tempwn2b="", destwn1="", destwn2="" // trace1 part a trace2 part a are read in first interleave, part b in second 
variable datafactor_a, datafactor_b
string xunit_a, yunit_a, xunit_b, yunit_b 

if(paramisdefault(slabel))
	slabel=""
endif

variable rescalefactor = 1
if(!paramisdefault(rescale))
	rescalefactor = rescale
endif

variable singlewave=0
variable importflag=0,im=0
string serieslist=""
//
string labellist=""
//
//this turns off and on making waveforms, turn off to manage memory better!
variable makewaveforms=0
//
string signature="", version="",sIsLittleEndian=""
variable time0, items, isLittleEndian, bundleItems, filesize
//positions
variable datapos=0,pTheBeginningOfTheFile=0
variable pSignature=0, pVersion=8, pTime0=40, pItems=48, pIslittleendian=52, pBundleItems=64
//header of entire tree
variable bundleheaderSize=256

//Pulsed Record Sizes from HEKA
//repeat this for the amp portion of binary file 
////////////?? are sizes different now ?? Should I change ????????????????????????????????? ASK 
variable PulsedRootRecordSize=536, PulsedGroupRecordSize=128, PulsedSeriesRecordSize=1120
variable PulsedSweepRecordSize=160, PulsedLockinRecordSize=96, PulsedTraceRecordSize=280

/////////////////////////ADDING IN STRUCTURE STUFF FOR AMPLIFIER FILE///////////////////////////
//Amped Record Sizes from HEKA
variable AmplifierRootRecordSize=80, AmplifierSeriesRecordSize=16, AmplifierStateRecordSize=560
variable LockInParamsSize=96, AmplifierStateSize=400
//incrementing variables for Pulsed files
variable aseries=0,Xseries=0,astates=0,Xstates=0
////////////////////////////////////////////////////////////////////////////////////////////////

//incrementing variables for Pulsed files
variable igroup=0,ngroups=0,iseries=0,nseries=0,isweep=0,nsweeps=0,itrace=0,ntraces=0
string grouplabel="",serieslabel="",sweeplabel="",tracelabel=""
string thiswavename="",listofpmwaves=""
//sizes
variable sSignature=4, sVersion=32, sBundleItems=12
variable thiswavelength,MAXWAVELENGTH=1E9
variable maxchildren=9999
//assorted variables
variable bundleInfoStart=64, thisbundlestart, start, length
variable nLevels, levelsizes,pos,nchildren
string extension="XXX",magicnumber=""
//loop over bundle items info
variable i,j
variable byteOrder=2
variable starttime=0, stopwatchtime=0
string notestring
// temp storage of parameters
variable ccihold=inf, vhold=inf
variable e9mode=inf,chAmplMode=inf
variable chstimtodacid = inf, temp=inf

fstatus refnum
filesize=V_logEOF //get the size of the file

// NOW READ THE BUNDLE HEADER USING A STRUCTURE
STRUCT	bundleHeader 					myBundleHeader	

STRUCT	pulsedRootRecord 				pulsedRoot
STRUCT	pulsedGroupRecord 				pulsedGroup
STRUCT	pulsedSeriesRecord				pulsedSeries
STRUCT	pulsedSweepRecord				pulsedSweep
STRUCT	pulsedTraceRecord				pulsedTrace

/////////////////////////ADDING IN STRUCTURE STUFF FOR AMPLIFIER FILE///////////////////////////
STRUCT   amplifierRootRecord			ampedRoot
STRUCT	amplifierSeriesRecord		ampedSeries
STRUCT	amplifierStateRecord			ampedState
////////////////////////////////////////////////////////////////////////////////////////////////

STRUCT	StimulationRootRecord 			StimulationRoot
STRUCT	StimulationRecord				Stimulation
STRUCT	StimulationChannelRecord		StimulationChannel
STRUCT	StimulationSegmentRecord		StimulationSegment

STRUCT VariableTimingStruct			varyTiming
STRUCT VTSstorage						VTSstore

////////////////////////////////////////////////////////////////////////////////////////////////


//start reading the tree, set back to beginning of file 
Fsetpos refnum, pTheBeginningOfTheFile
//reads header, predefined in heka file definitions
FBinRead /B=(BYTEoRDER) refnum, myBundleHeader

//ERROR CHECKING
if((myBundleHeader.oItems<0)||(myBundleHeader.oItems>10))
	byteOrder=3
	Fsetpos refnum, pTheBeginningOfTheFile
	//tries to fix by changing byte order
	FBinRead /B=(BYTEoRDER) refnum, myBundleHeader
//	print "Something wrong with byte order, trying again", myBundleHeader.oIslittleendian
	if((myBundleHeader.oItems<0)||(myBundleHeader.oItems>10))
		print "Still failing to read header properly.  Contact tech support!  tony.defazio@gmail.com"
		abort
	endif
endif	

if(myBundleHeader.oIslittleendian==1)
	byteOrder=3
else
	byteOrder=2
endif

//modern data format, Signature is DAT2 
if(stringmatch(myBundleHeader.oSignature,"DAT2")==1)
	// DAT2 files have informative headers.
	// However, this program is so well written that we don't need this information.
else  
	// if not DAT2, we can handle DAT1 bundles 
	// by skipping the header read and asking the loop
	// to read the file until nothing remains
	print filename2, "Blank or corrupted header.  This is not your fault.  There is nothing you can do about it.",mybundleheader.oitems
	mybundleheader.oitems=10
	print "Corrected header for current analysis only: ",mybundleheader.oitems	
endif

//print mybundleheader


//do while (maybe add parameters) 
//looping over oItems (main branches of the tree)
//puls = data are, (one of the oItems)amp = info for user, sim = wave form 
i=0
do

// Here is where we check for the corrupted header information:
	start = mybundleheader.oBundleItems[i].oStart
	length = mybundleheader.oBundleItems[i].oLength
	extension = mybundleheader.oBundleItems[i].oExtension

//	print "ReadHEKA loop: item in bundle, start, length, ext: ",i, start,length,extension
	
	if((i==0)&&((start==0)||(length==0)))
		print "Looks like the header in this file is messed up.  We'll try to get data out the hard way."
		variable go=1,myEOF=0
		string mySearchString=""
		mySearchString=padstring(mySearchString,4,0)
		datapos=0
		fstatus refnum
		myEOF = V_logEOF
		do
			fsetpos refnum, datapos
			fbinread /b=(byteOrder) refnum, myBSearchString
		//	print myBloodySearchString
			if(stringmatch(mysearchstring,"Tree")||stringmatch(mysearchstring,"eerT"))
				go=0
				print "got it!!"
				start=datapos-4
			endif
			datapos+=4
		while((go==1)&&(datapos<myEOF))
// here are the magic lines of code that will accomplish this:
//  la la la la
// blah blah blah

		if(go==1)
			print "Failed to locate file information.  Sorry."
			abort
		endif
	endif
	
//	if(stringmatch(showfiles,extension))
	
	strswitch (extension)
		case ".dat":  //loads all data into one wave, for now
			datapos=start
//			make/o/n=(length/4) $(filename2)
//			fsetpos refnum, start
//			fbinread /b=(byteOrder) /f=2 refnum, $(filename2)
//			print "Could be loading data now, but we'll do that later."
			break
		case ".pgf":  //read the PGF file, Tree format

			variable irec=0,nrec=0,iseg=0,nseg=0,ichan=0,nchan=0
			string mytrunc="",units=""
			variable sampleInterval=0, holding=0,sweepduration=0,begz=0, endz=0, nsamples=0,myclass=0
			variable pbegz=0, pendz=0
			nsweeps=0
			
			magicnumber=padstring(magicnumber,4,0)
			pos=start
			fsetpos refnum,start
			fbinread /b=(byteOrder) refnum, magicnumber
			pos+=4
//			print "Stim File Magic number: ",magicnumber
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nLevels
			pos+=4
//			print "levels: ",nlevels
			make/n=4/o stimLevelSizes
			stimlevelsizes=0
			fsetpos refnum,pos
			fbinread /b=(byteOrder)/f=3 refnum, stimlevelsizes
			pos+=4*nlevels
			fstatus refnum
//			print "level, levelsizes: ", nlevels,stimlevelsizes,"=======difference in pos? ",pos-V_filepos

		// read the root record--THERE IS ONLY ONE ROOT RECORD
			fsetpos refnum, pos
			fbinread /b=(byteOrder) refnum, stimulationRoot
//			print "stimulationRoot:",stimulationRoot
			pos+=stimlevelsizes[0]
// 			read the number of children
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nchildren
//			print "ROOT RECORD:  NCHILDREN=",nchildren
			pos+=4	
				
			nrec=nchildren //=nchildren  //FOR DIAG
			
			variable nVTS = 0 //number of variable timing PGFs
			make/T/O/N=(nrec*10) recordNames
			// 20190710 adding storage of segment durations
			variable nsegmax = 3600
			make/O/N=( nrec, nsegmax ) seg_dur_store

			for(irec=0;irec<nrec;irec+=1) // 20220421 this is correlated with the number of series
			// read the Stimulation record--THERE MAY BE MORE THAN ONE STIM RECORD!!
				fsetpos refnum, pos
				fbinread /b=(byteOrder) refnum, Stimulation
				pos+=stimlevelsizes[1]
//				fstatus refnum
//				print iseries,"after STIMULATION RECORD=======difference in pos? ",pos-V_filepos,Stimulation
				fsetpos refnum, pos
				fbinread/b=(byteOrder)/f=3 refnum, nchildren
				pos+=4				
				nchan = nchildren //!!!!!! 20140811
// information for generating waveform
				nsweeps = stimulation.stNumberSweeps
				sampleInterval = stimulation.stSampleInterval
				
//				print stimulation.stEntryname
				mytrunc = stimulation.stentryname

//				print "STIMULATION RECORD", irec," ;  NCHILDREN=",nchildren," ; name: ",mytrunc," ; n channels: ",nchan
				variable dTf_flag = 0, dTF_seg = nan
				nseg=0

				for(ichan=0;ichan<nchan;ichan+=1)
					fsetpos refnum, pos
					fbinread /b=(byteOrder) refnum, StimulationChannel
					pos+=(stimlevelsizes[2])
//					fstatus refnum
//					print iseries,"after STIMULATION CHANNEL=======difference in pos? ",pos-V_filepos,StimulationChannel
					fsetpos refnum, pos
					fbinread/b=(byteOrder)/f=3 refnum, nchildren
					nseg=nchildren //!!!!!!!!!! 20140811
					
//print "readserieslist: nrecs:", nrec, "nchan:", nchan 

//					print irec,"STIM CHAN ",ichan," :  NCHILDREN=",nchildren
//					print stimulationchannel
					pos+=4		
					units = stimulationchannel.chDACUnit
					holding = stimulationchannel.chHolding
					champlmode = stimulationchannel.champlmode
					chStimtoDACid = stimulationchannel.chstimtodacid //bit  0 use stimscale, 1 relative to vm, 2 file template, 3 lockin
					
//					print mytrunc, "units:",units,"holding:",holding,"mode:", champlmode,"vhold:", vhold, "ccihold:",ccihold
					
					grouplabel="g"
					serieslabel="s"
					sweeplabel="sw"
					tracelabel="tZ"
					igroup=0
					iseries=irec
					
					sweepduration=0
					make/O/N=(nseg) segClass
					make/O/N=(nseg) segVoltage
					make/O/N=(nseg) segVoltageInc
					make/O/N=(nseg) segDuration
					make/O/N=(nseg) segDurationInc
					make/O/N=(nseg) segDurationFactor
					segClass=0
					segVoltage=0
					segVoltageInc=0
					segDuration=0
					segdurationfactor=0
					
					dTf_flag = 0
					dTf_seg = nan
					variable dTF_mode = nan
					if( nseg > nsegmax )
						print "warning: nseg > nseg max, durations not stored", nseg, nsegmax
					endif
					for(iseg=0;iseg<nseg;iseg+=1)
						fsetpos refnum, pos
						fbinread /b=(byteOrder) refnum, StimulationSegment
						pos+=(stimlevelsizes[3])
//						fstatus refnum
//						print iseries,"STIM SEGMENT=======difference in pos? ",pos-V_filepos,StimulationSegment
						fsetpos refnum, pos
						fbinread/b=(byteOrder)/f=3 refnum, nchildren
//						print irec,ichan,"STIM SEG ",iseg," :  NCHILDREN=",nchildren
//						print stimulationsegment
						pos+=4	
						if(stimulationsegment.seDeltaTIncrement!=0)
//							print "Variable durations are not yet handled for PM files.  tony.defazio@gmail.com.",mytrunc
						endif
						segClass[iseg]=stimulationsegment.seClass
						switch(chstimtodacid)
						case 0: //bit 0 use stim scale, should be raw voltage
							segVoltage[iseg]=stimulationsegment.seVoltage
							break
						case 1: //bit 1 relative to Vmemb, value should be added to "hold"
							segVoltage[iseg]=stimulationsegment.seVoltage
							break
						case 2: //file template
							segVoltage[iseg]=inf
							print "warning! file template not read"
							break
						case 3: //lockin
						//	print "lockin not implemented"
							segvoltage[iseg]=-1
							break
						default:
							segVoltage[iseg]=0
							break
						endswitch
						segVoltageInc[iseg]=stimulationsegment.seDeltaVIncrement
						segDuration[iseg]=stimulationsegment.seDuration
						segDurationInc[iseg]=stimulationsegment.seDeltaTIncrement
						segDurationfactor[iseg]=stimulationsegment.seDeltaTfactor
						sweepduration+=stimulationsegment.seDuration

						//print "rec:", irec, "chan:", ichan, "seg:", iseg, "dur:", segDuration[iseg]
						
						//debugger

						// store segment durations
						if( ( ichan == 0 ) && ( iseg < nsegmax ) )  // only do the first channel
							seg_dur_store[ irec ][ iseg ] = segDuration[ iseg ]
							//print irec, iseg, segduration[iseg]
						endif

						variable dTf = stimulationsegment.sedeltatfactor
						variable dTinc = stimulationsegment.seDeltaTIncrement
						//print filename2, iseries, isweep, "seg:", iseg, "factor: ", dTf, "dTinc: ", dTinc
						// if( (dTf != 1) && ( dTinc != 0) ) // 20191001 possible to set dtf without inc by mistake
						if( (dTf != 1) || ( dTinc != 0) ) // 20191001 possible to set dtf without inc by mistake
							dTf_flag = 1
							dtf_seg = iseg
							dtf_mode = stimulationsegment.seDurationIncMode
							seg_dur_store[ irec ][ iseg ] = inf
							//print stimulationsegment.seDuration, stimulationsegment.seDeltaTincrement, dTf 
							//print "ipso facto: ", segDuration
							//print seg_dur_store[irec][iseg]
						endif 

					endfor  //iseg
					//dTf_flag = 1 // troubleshooting jenn 20220211
					if(dTf_flag == 1)
						//print stimulation.stEntryName, stimulation.stFileName
						//print "segment: ",dtf_seg, "refnum:",refnum
						//print "presegdur:", segduration[dtf_seg-1], "duration: ", segDuration[dtf_seg],"inc: ", segDurationInc[dtf_seg], "factor:",segDurationFactor[dtf_seg]
						//VTSstore.vts[ nVTS ].pgf_label =  stimulation.stEntryName
						recordNames[ nVTS ] = stimulation.stEntryName
						VTSstore.vts[ nVTS].record = irec +1 // to match series number
						VTSstore.vts[ nVTS].nsweeps = nsweeps
						VTSstore.vts[ nVTS ].nsegments = nseg
						for(iseg=0;iseg<nseg;iseg+=1)
							VTSstore.vts[ nVTS ].segment_durations[iseg] = segDuration[iseg]
						endfor
						VTSstore.vts[ nVTS ].variable_segment = dtf_seg
						VTSstore.vts[ nVTS ].mode = dTf_mode
						VTSstore.vts[ nVTS ].duration = segDuration[ dtf_seg ]
						VTSstore.vts[ nVTS ].t_factor = segDurationFactor[ dtf_seg ]
						VTSstore.vts[ nVTS ].dt_Incr = segDurationInc[ dtf_seg ]
						
						nVTS += 1
					endif
					//print sweepduration
					//make wave for each sweep
					if(makewaveforms==1)
						for(isweep=0;isweep<nsweeps;isweep+=1)
							thisWaveName = filename2+grouplabel+num2str(igroup+1)+serieslabel+num2str(iseries+1)+sweeplabel+num2str(isweep+1)+tracelabel
							nsamples=round(sweepduration/sampleinterval)
							if(nsamples>MAXWAVELENGTH)
								print "problem with duration", mytrunc,sweepduration,sampleinterval,nsamples
							endif
							make/O/N=(nsamples) $thiswavename
							WAVE mywave=$thiswavename
//							appendtograph mywave
							mywave=inf
							setScale/P x, 0, sampleinterval, "s", mywave
							SetScale/P y,0,1,units,mywave
							endz=0
							begz=0
							for(iseg=0;iseg<nseg;iseg+=1)
								endz=begz+segDuration[iseg]
								myclass = segClass[iseg]
								pbegz=x2pnt(mywave,begz)
								pendz=x2pnt(mywave,endz)
//								print "Segments!!!",myclass,	pulsedSeries.seAmplifierState.E9CCIHold, pulsedSeries.seAmplifierState.E9Vhold
								switch(myclass)
								case 0: //constant
									mywave[pbegz,pendz]=segVoltage[iseg]+(isweep+1)*segVoltageInc[iseg]
									break
								case 1: //ramp
									//print "ramp not yet",segClass[iseg]
									mywave[pbegz,pendz]=  ((p-pbegz) * (segVoltage[iseg]- segVoltage[iseg-1])/(pendz-pbegz))+segVoltage[iseg-1]//* segVoltage[iseg]+isweep*segVoltageInc[iseg]
									break
								case 2: //continuous
								
									mywave[pbegz,pendz]= holding //needs to be upgraded to be sensitive to vc or cc
									break
								default:
									print "Segment class not recognized.",mytrunc,segClass[iseg]
								endswitch
								begz=endz
							endfor  //iseg in isweep
//							doupdate
						endfor  //isweep
					endif //if make waveforms==1
				
				endfor //ichan
			
			endfor  //irec
			
			// store the VTSstore !!!
			string short = datecodeGREP( filename2 ) // e.g. 20161004f ... this stores all the VTS for the experiment, .dat file // + "s" + num2str( irec )
			string VTS_wn = short + "_VTS"
			make/O/N=(nVTS) $VTS_wn
			WAVE VTS_w = $VTS_wn
			
			variable iVTS = 0
			for( iVTS = 0; iVTS < nVTS; iVTS+=1 )
				// print iVTS, VTSstore.vts[ iVTS ]
				structput VTSstore.vts[ iVTS ], VTS_w[ iVTS ]
			endfor			
			
			// store the seg dur store
			string segdur_wn = short + "_SDS"
			duplicate/O seg_dur_store, $segdur_wn
			break
			
		case ".pul":
			variable newposition, oldposition
			string Xunit, Yunit			
//	print "inside the .pul bundled file extractor"
			magicnumber=padstring(magicnumber,4,0)
//			datapos=0
			pos=start
			fsetpos refnum,pos
			fbinread /b=(byteOrder) refnum, magicnumber
//			print "Magic number: ",magicnumber
			pos+=4
//		read the number of levels
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nLevels
//			print "levels: ",nlevels," ====for pulsed files this better be 5!!"
			pos+=4
//		read the level sizes for pulsed file
			make /o/n=(nlevels) PulsedLevelSizes
			fsetpos refnum,pos
			fbinread /b=(byteOrder)/f=3 refnum, pulsedlevelsizes
			//print "position, levelsizes: ", pos,pulsedlevelsizes
			pos+=nlevels*4
//		read the root of the tree, there is only one root record
			fsetpos refnum,pos
			fbinread /b=(byteOrder) refnum, pulsedRoot
//			print pulsedRoot
			//print "CRC: ",stringCRC(pulsedRoot.RoCRC[0],pulsedroot.rocrc)
			//print "time of recording: ",PMsecs2dateTime(pulsedroot.RoStartTime,0,3), pulsedroot.rostarttime
//		increment fiile position, there is only one root record
			pos+=pulsedlevelsizes[0]

// 		read the number of children
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nchildren
			//print "Root:  NCHILDREN=",nchildren
			pos+=4
			
			//	FIRST GROUP
			//the next record is the group, there may be more than one group
			fsetpos refnum,pos
			fbinread /b=(byteOrder) refnum, pulsedGroup
			
// number of groups is given by  number of children!!			ngroups=pulsedGroup.GrGroupCount
			ngroups=nchildren
			
			//print "Reading records from each group, n=",ngroups
			if((ngroups<1)||(ngroups>maxchildren))
				print "readHEKAfiles_v6_0: PatchMasterBundleHeader: number of groups is strange,",ngroups,".  setting to 1."
				ngroups=1
			endif
			// start at 0 and re-read first group record
			for(igroup=0;igroup<ngroups;igroup+=1)

				fsetpos refnum, pos
				fbinread /b=(byteOrder) refnum, pulsedGroup
				pos+=pulsedlevelsizes[1]
				
				fsetpos refnum,pos
				fbinread /b=(byteOrder)/f=3 refnum, nchildren
				pos+=4				
				
				fsetpos refnum, pos
				fbinread /b=(byteOrder) refnum, pulsedSeries

				nseries = nchildren
				if((nseries<0)||(nseries>maxchildren))
					print "readHEKAfiles_v6_0: PatchMasterBundleHeader: number of series is strange,", nseries,".  Changing to 1."
					nseries=1
				endif
				make/O/N=(nseries) e9ccihold
				make/O/N=(nseries) e9vhold
				e9ccihold=inf
				e9vhold=inf
				

///////////////////////////////////				
///////////////////////////////////				
////////////////// STORING LABELS				
				string labelwaven=datecodefromfilename(filename2)+"_lab"
				make/O/T/N=(nseries) $labelwaven
				WAVE/T labelw = $labelwaven
///////////////////////////////////
///////////////////////////////////
						
				variable impseries
				for(iseries=0;iseries<nseries;iseries+=1)
					importflag=0
					for(im=0;im<itemsinlist(importserieslist);im+=1)
						impseries=str2num(stringfromlist(im,importserieslist))-1 // NOTE CORRECTION TO ZERO BASED NUMBERS!
						if(impseries==iseries)
							importflag=1
							im=inf
						endif
					endfor

/////////////////////////////MOVE TO AMP
					fsetpos refnum, pos
					fbinread /b=(byteOrder) refnum, pulsedSeries
					e9ccihold[iseries]=pulsedSeries.seAmplifierState.E9CCIHold
					e9vhold[iseries]=pulsedSeries.seAmplifierState.E9Vhold
					pos+=pulsedlevelsizes[2]
					
					fsetpos refnum,pos
					fbinread /b=(byteOrder) /f=3 refnum, nchildren
					pos+=4		
						
					nsweeps= nchildren	
					if((nsweeps<0)||(nsweeps>maxchildren))
						print "readHEKAfiles_v6_0: PatchMasterBundleHeader: nsweeps is strange ",nsweeps," setting to 1."
						nsweeps=1
					endif
					for(isweep=0;isweep<nsweeps;isweep+=1)
					
					// set import flag if sweepn is set and matched
						if(!ParamIsDefault(sweepn))
							importflag=0
							if(isweep == (sweepn-1)) // NOTE CORRECTION TO ZERO BASED NUMBERS
								importflag =1
							endif
						endif
						fsetpos refnum, pos
						fbinread /b=(byteOrder) refnum, pulsedSweep

						pos+=pulsedlevelsizes[3]
						fsetpos refnum,pos
						fbinread /b=(byteOrder) /f=3 refnum, nchildren
						pos+=4								
						ntraces=nchildren
						if((nchildren<0)||(nchildren>maxchildren))
							print "readHEKAfiles_v6_0: PatchMasterBundleHeader: .pul -- too many traces",nchildren
							abort
						endif
						
						for(itrace=0;itrace<ntraces;itrace+=1)
						// set import flag if tracen is set and matched
							if( !ParamIsDefault(tracen)  )
								importflag = 0
								if( (itrace == (tracen-1)) && (impseries==iseries) )
									importflag=1
								endif
								if((itrace == (tracen-1))&&(isweep == (sweepn-1))&&(impseries==iseries))
									importflag =1
							 		singlewave=1
								endif
					
							endif
							fsetpos refnum, pos
							fbinread /b=(byteOrder) refnum, pulsedTrace
							pos+=pulsedlevelsizes[4]												
// 						read the number of children
							fsetpos refnum,pos
							fbinread /b=(byteOrder) /f=3 refnum, nchildren
							//print "traces don't have CHILDREN=",nchildren
							pos+=4								
							//print pulsedTrace
//this is where we'll start saving data into waves
//for now let's just get the wave names: filename-gN-seN-swN-tN
							if((pulsedTrace.TrDataPoints<1)||(pulsedTrace.TrDataPoints>MAXWAVELENGTH))
								print "readHEKAfiles_v6_0: PatchMasterBundleHeader: too many data points (or too few): ",pulsedTrace.TrDataPoints
								//print "aborting wavelet creation"
							else
								thisWaveLength = pulsedTrace.TrDataPoints
//								grouplabel=pulsedgroup.grlabel
								if(strlen(grouplabel)==0)
									grouplabel="g"
								endif
//								serieslabel=pulsedseries.selabel
								if(strlen(serieslabel)==0)
									serieslabel="s"
								endif				
//								sweeplabel=pulsedsweep.swlabel
								if(strlen(sweeplabel)==0)
									sweeplabel="sw"
								endif				
//								tracelabel=pulsedtrace.trlabel
								if(strlen(tracelabel)==0)
									tracelabel="t"
								endif
// 20180216					thisWaveName = datecodefromfilename(filename2)+grouplabel+num2str(igroup+1)+serieslabel+num2str(iseries+1)+sweeplabel+num2str(isweep+1)+tracelabel+num2str(itrace+1)
								thisWaveName = datecodeGREP2(filename2)+grouplabel+num2str(igroup+1)+serieslabel+num2str(iseries+1)+sweeplabel+num2str(isweep+1)+tracelabel+num2str(itrace+1)
								variable skipimport = 0
								if(waveexists($thiswavename))
									print "readserieslist: skipping import, wave exists, ", thiswavename
									skipimport = 1
								endif
								//else
									datapos = pulsedTrace.trData
									fsetpos refnum, datapos
									//print "trace:", itrace, "datapos:", datapos, pulsedtrace.trinterleaveSize, pulsedtrace.trinterleaveskip, pulsedtrace.trdatapoints 
	// if matches series list, then import
								//	if(iseries==30)
								//		thiswavelength = 435714
								//	endif
									variable nelements = 0, ielement = 0, nbinreads = 0, ibinread = 0, chunksize = 0, pointsread = 0, pointsleft = 0

									if(importflag==1)
										// handle aberrant interleave 20220115; only works if there's one interleave event!!
										//if(( thiswavelength > interleaveThreshold ) && ( ntraces>1) )
										interleaveSize = pulsedTrace.TrInterleaveSize
										interleaveSkip = pulsedTrace.TrInterleaveSkip //+ 20 

										nbinreads = thiswavelength / interleaveSize 
									if( skipimport == 0 )
										if( interleaveSize != 0 )
											interleaveFlag = 1

											print "COLLECTOR: readserieslist; interleave threshold crossed, attempting to append waves"
											nelements = interleaveSize / 2 // 20220120 interleave size is in bytes, two bytes per element // was hardcoded round( thiswavelength / 2 )
											make/O/N=(thiswavelength) $thiswavename	= 1 // absorbs scale factor		
											WAVE dataw = $thiswavename 
											variable ichunk=0
											do
												chunkSize = nelements // size is in bytes, each element 2 bytes  
												pointsleft = thiswavelength - pointsread
												if( pointsleft < chunksize )
													chunksize = pointsleft
												endif
												make/O/N=(chunkSize) chunk 
												fsetpos refnum, datapos 
												fstatus refnum
												//print "chunk:", ichunk, "curr pos:", V_filepos, " pre: ", prepos, " p1: ", pos1, " p2: ", pos2, " p3: ", pos3, " p4: ", pos4
						
												fbinread /b=(byteOrder) /f=2 refnum, chunk 
			 			
												datapos += interleaveSkip // interleaveSize + interleaveSkip
												variable ipt = 0
												for( ipt = 0; ipt < chunksize; ipt+=1 )
													dataw[ pointsread +  ipt ] = chunk[ ipt ]
												endfor 
												pointsRead += chunkSize
												ichunk += 1
												// print thiswavename, "s:", iseries+1, "sw:", isweep+1,"t:", itrace+1, "interleave chunk:", ichunk, "pointsread: ", pointsread, "skipped: ", interleaveSkip/2
											while( pointsread < thiswavelength )
										else
											make/O/N=(thisWaveLength) $thiswavename
											fbinread /b=(byteOrder) /f=2 refnum, $thiswavename //assumes int16
										endif
									

										// historical
										//make/O/N=(thisWaveLength) $thiswavename
										//fbinread /b=(byteOrder) /f=2 refnum, $thiswavename //assumes int16									

										serieslist+=thiswavename+";"

										Xunit=pulsedTrace.TrXunit
										Yunit=pulsedTrace.TrYunit
										setScale/P x,0,pulsedTrace.TrXinterval,Xunit,$(thiswavename)
										SetScale/P y,0,1,Yunit,$(thiswavename)
										WAVE w=$(thiswavename)
										w=w*pulsedTrace.TrDataFactor
									
										// these are most of the params users want in wave note
										// params from pulsed sweep
										starttime = pulsedSweep.SwTime
										stopwatchtime = pulsedSweep.SwTimer
										temp = pulsedSweep.swTemperature

										// params from pulsed trace
										variable TrADCchannel = pulsedTrace.TrADCchannel
										variable TrAmplIndex = pulsedTrace.TrAmplIndex
										variable mode = NaN // sMode
										variable Rs = NaN //sRsValue
										variable RsFrac = NaN // sRsFraction
										variable Cslow = NaN // sCSlow
										variable LJP = NaN // sVLiquidJunction
										variable VPoff = NaN // sVpOffset
										vhold = NaN 
										ccihold = NaN 

										// params from amp series array
										variable ampN = 0 
										if (pulsedTrace.TrAmplIndex == 0) // no amplifier used
											ampN = NaN 
										else
											if( pulsedTrace.TrAmplIndex < 4 ) // only supporting 3 amps
												AmpN = pulsedTrace.TrAmplIndex - 1 // 1,2,3 are amp numbers, 0 means not amp channel
												vhold = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sVHold 
												ccihold = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sCCIHold 
												mode = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sMode
												Rs = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sRsValue
												RsFrac = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sRsFraction
												Cslow = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sCSlow	
												LJP = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sVLiquidJunction
												VPoff = ampStructArr.ampNumber[ ampN ].ampSeries[ iseries ].sVpOffset										
											else
												AmpN = NaN
											endif
										endif 


									//	PLEASE NOTE NO SPACES IN WAVE NOTE. NOTE DELIMITERS: KEY:VALUE;KEY2:VALUE2;
										note w, "" // clear the note
										sprintf notestring, "LABEL:%s;START:%20.20g;STOPWATCH:%20.20g;DATE:%s;", pulsedSeries.SeLabel,starttime, stopwatchtime,secs2date(PMsecs2Igor(starttime),3)
										note w, notestring
										sprintf notestring, "TIME:%s;INT:%g;BW:%g;",secs2time(PMsecs2Igor(starttime),3,1),pulsedTrace.TrXinterval,pulsedTrace.TrBandwidth
										note w, notestring
										sprintf notestring, "VHOLD:%g;CCIHOLD:%g;", vhold, ccihold // to add TEMP:%g;MODE:%g;", temp,pulsedSeries.SeAmplifierState.E9Mode  // added temp 20151009
										note w, notestring
										// mode, Rs etc here
										sprintf notestring, "Mode:%g;RsValue:%g;RsFraction:%g;Cslow:%g;" , mode, Rs, RsFrac, Cslow  // added 20160210
										note w, notestring
										sprintf notestring, "LJP:%g;VPoff:%g;", LJP, VPoff
										note w, notestring
										sprintf notestring, "TrADCchannel:%g;TrAmpIndex:%g;", TrADCchannel, TrAmplIndex
										note w, notestring
										sprintf notestring, "Reader:the_collector v2_4 20220323b;"
										note w, notestring
									
										if(itrace==0)
											w*=rescalefactor
										endif
									else // if we skipped import because wave exists, still need to add to series list
										serieslist += thiswavename + ";"
									endif // skip import because wave exists

									if(singlewave)
										listofpmwaves = thiswavename
										serieslist = thiswavename
									endif

								else // if import flag is not set
									if( stringmatch( pulsedSeries.SeLabel, slabel )  )
										serieslist += thiswavename + ";"
									endif
								endif

								datapos += 2 * thisWaveLength			//increment pos by 2 bytes for each data point
/////////////////////////////////////////////// LABEL LABEL LABEL LABEL
								labelw[iseries] = pulsedSeries.SeLabel
								if(!singlewave)
									ListofPMWaves = listofPMwaves+thiswavename+";"			
								endif							
								//print thiswavename, thiswavelength
								//print pulsedtrace
							endif // if import flag
							//endif // if wave doesn't exist
						endfor // loop over traces

					endfor	// loop over sweeps			
				endfor	// loop over series
			endfor // loop over groups

//			print "readHEKAfiles_v6_0: PatchMasterBundleHeader: ",ngroups,nseries,nsweeps,ntraces

			break
		case ".amp":
			// amp now handled separately, see functions below		
			
			break
		case ".txt": // skipping txt component
//			magicnumber=padstring(magicnumber,4,0)
// 			fsetpos refnum,start
// 			fbinread /b=(byteOrder) refnum, magicnumber
// 			print ".TXT Magic number: ",magicnumber
// 			print ".TXT extension in bundled files not supported in this version. Contact tony.defazio@gmail.com if necessary."
// 			fsetpos refnum,start+4
// 			fbinread /b=(byteOrder) /f=3 refnum, nLevels
			
// 			if(nlevels>maxchildren)
// 				print ".TXT levels too big: ",nlevels,maxchildren
// 				print "Reduced number of children. Some data in TXT file may be lost."
// 				nlevels=1
// 			endif
// 			for(j=0;j<nlevels;j+=1)
// 				fsetpos refnum,start+4*(j+2)
// 				fbinread  /b=(byteOrder) /f=3 refnum, levelsizes
// //				print ".TXT level, levelsizes: ", j, levelsizes
// 			endfor

			break
		default:
//			print "Unknown extension in bundle file.",extension
	endswitch
	
//	endif
	i+=1
while (i<=mybundleheader.oItems)
string output
//variable updateAmpSuccess = 0
if( (strlen(importserieslist)>0 ) || strlen(slabel) > 0) 
	// update the amp settings if waves were generated
	// updateAmpSuccess = updateAmpSeriesList( serieslist, refnum )
	output=serieslist
else
	output=labelwaven
endif
return output
end

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//
// 		POPULATE SERIES LIST : given label, list all series with that label
//	
////////////////////////////////////////////////////////////
function populateSeriesList( thislabel, expnum, expwn, expselwn, label_ext,serieswn ,serselwn)
string thislabel
variable expnum //inf if all exp, otherwise only process this experiment
string expwn, expselwn, label_ext,serieswn,serselwn

variable thiscolor = 1

WAVE/T expw = $expwn
variable i, nexp = dimsize(expw, 0), j, nseries=0, k=0,expstart=0

WAVE expselw = $expselwn
string thisexp="", explabellist="", serieslabel=""

WAVE/T seriesw = $serieswn
nseries = dimsize(seriesw, 0)

WAVE serselw = $serselwn

make/O/T/N=2000 labelledseries
labelledseries=""
if(expnum!=inf)
	expstart=expnum
	nexp = expstart+1
	thiscolor = 3
endif
for( i=expstart ; i < nexp ; i+=1 )
	thisexp = expw[i]
//	print "in populate series list: ",thisexp	
	explabellist=thisexp+label_ext // AGG - labeled list of series for a given experiment
	WAVE/T explabellistw = $explabellist
	nseries = dimsize(explabellistw,0)
	expselw[i][0][1]=0
	for( j=0; j < nseries; j+=1 )
		serieslabel = explabellistw[j]
		if( stringmatch(thislabel,serieslabel) )
			labelledseries[k]=thisexp+"g1s"+num2str(j+1)
			expselw[i][0][1]=thiscolor
			k+=1
		endif
	endfor
endfor
nseries = k //dimsize(labelledseries,0)
redimension/N=(nseries,1) seriesw
redimension/N=(nseries,1,-1) serselw
seriesw = labelledseries
end

///////////
/// bc_SeriesForExp_proc
/// AUTHOR: 
/// ORIGINAL DATE: 2022-09-08
/// NOTES: 
///////////
function bc_SeriesForExp_proc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		updateSeriesList_allSeriesForExp("_lab",ksserieslistwn,ksseriesselwn, panelName = B_Struct.win) // uses constants at the top of this ipf
	endif
	return 0
end


///////////
/// updateSeriesList_allSeriesForExp
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-09-08
/// NOTES: Update the series list box with all of the series for selected experiment
///////////
function updateSeriesList_allSeriesForExp(label_ext,serieswn ,serselwn[, panelName])
	string label_ext,serieswn,serselwn, panelName

	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif
	
	
	// Get selected experiment
	string thisexp = getSelectedItem("List_Exp")

	string explabellist="", serieslabel=""
	
	// Get the series list wave and select wave
	WAVE/T seriesw = $serieswn
	WAVE serselw = $serselwn

	variable j, nseries=0
	nseries = dimsize(seriesw, 0) // will update this later
	
	// Make a new version of the labelledseries wave
	// Will update this with the series names associated with this experiment
	// But make it blank to start
	make/O/T/N=2000 labelledseries
	labelledseries=""
	
	if( strlen(thisexp) > 0 )
		explabellist=thisexp+label_ext // AGG - labeled list of series for a given experiment
		// Going to use this wave to count how many series there are for this experiment
		WAVE/T explabellistw = $explabellist
		Wave labelSelW
		Wave/T labellistW
		labelSelW[][][1] = 0
		if(WaveExists(explabellistw))
			nseries = dimsize(explabellistw,0)
			// For each series, add name string to the labelledseries wave
			for( j=0; j < nseries; j+=1 )
				labelledseries[j]=thisexp+"g1s"+num2str(j+1)
				string thisLabel = explabellistw[j]
				FindValue/Text=thisLabel labellistW
				if(V_value >= 0)
					labelSelW[V_value][][1] = 3
				endif
			endfor
		endif
	endif
	// Redimension according to the number of series
	nseries = j //dimsize(labelledseries,0)
	redimension/N=(nseries,1) seriesw
	redimension/N=(nseries,1,-1) serselw
	seriesw = labelledseries

	ListBox list_label selRow = -1, win = $panelName
	Wave expSelW, labelselw
	expSelW[][][1] = 0
end


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////     FUNCTION TO CONVERT PARAMETERS     ///////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

//Create the Amp Name wave
Function/S createAmpWaveName() //specfic to the amplifier
	
	string wn = "ampElementList"
	
	Make /T/O/N=(111) $wn
	WAVE /T w = $wn
	
//	structure AmplifierState
	w[0] = "sStateVersion" //	char   sStateVersion[8]   // (*8 = SizeStateVersion)
   w[1] = "sCurrentGain" // double
   w[2] = "sF2Bandwidth" // double
	w[3] = "sF2Frequency" // double 
	w[4] = "sRsValue" // double
	w[5] = "sRsFraction" // double
	w[6] = "sGLeak" // double
	w[7] = "sCFastAmp1" // double 
	w[8] = "sCFastAmp2" // double
	w[9] = "sCFastTau" //double
	w[10] = "sCSlow" //double
	w[11] = "sGSeries" //double
	w[12] = "sVCStimDacScale" // double
	w[13] = "sCCStimScale" // double
	w[14] = "sVHold" // double
	w[15] = "sLastVHold" // double 
	w[16] = "sVpOffset" // double
	w[17] = "sVLiquidJunction" // double
	w[18] = "sCCIHold" // double 
	w[19] = "sCSlowStimVolts" // double 
	w[20] = "sCCTrackVHold" // double
	w[21] = "sTimeoutCSlow" // double
	w[22] = "sSearchDelay" // double
	w[23] = "sMConductance" // double 
	w[24] = "sMCapacitance" // double
	w[25] = "sSerialNumber" // char[8] 
	w[26] = "E9Boards" // int
	w[27] = "sCSlowCycles" // int
	w[28] = "sIMonAdc" // int
	w[29] = "sVMonAdc" // int
	w[30] = "sMuxAdc" // int
	w[31] = "sTestDac" // int
	w[32] = "sStimDac" // int 
	w[33] = "sStimDacOffset" // int 
	w[34] = "sMaxDigitalBit" // int 
	w[35] = "sHasCFastHigh" // char
	w[36] = "sCFastHigh" // char
	w[37] = "sHasBathSense" // char
	w[38] = "sBathSense" // char
	w[39] = "sHasF2Bypass" // char
	w[40] = "sF2Mode" // char
	w[41] = "sAmplKind" // char
	w[42] = "sIsEpc9N" // char
	w[43] = "sADBoard" // char
	w[44] = "sBoardVersion" // char
	w[45] = "sActiveE9Board" // char
	w[46] = "sMode" // char
	w[47] = "sRange"  // char
	w[48] = "sF2Response" // char
	w[49] = "sRsOn"  // char
	w[50] = "sCSlowRange"  // char
	w[51] = "sCCRange"  // char
	w[52] = "sCCGain" // char
	w[53] = "sCSlowToTestDac"  // char
	w[54] = "sStimPath" // char
	w[55] = "sCCTrackTau" // char
	w[56] = "sWasClipping" // char
	w[57] = "sRepetitiveCSlow" // char
	w[58] = "sLastCSlow" // char
	w[59] = "sOld1" // char filler
	w[60] = "sCanCCFast" // char
	w[61] = "sCanLowCCRange" // char
	w[62] = "sCanHighCCRange" // char
	w[63] = "sCanCCTracking" // char
	w[64] = "sHasVmonPath" // char
	w[65] = "sHasNewCCMode" // char
	w[66] = "sSelector" // char
	w[67] = "sHoldInverted" // char
	w[68] = "sAutoCFast" // char
	w[69] = "sAutoCSlow" // char
	w[70] = "sHasVmonX100" // char
	w[71] = "sTestDacOn" // char
	w[72] = "sQMuxAdcOn" // char 
	w[73] = "sImon1Bandwidth" // double
	w[74] = "sStimScale" // double
	w[75] = "sGain" // char
	w[76] = "sFilter1" // char
	w[77] = "sStimFilterOn" // char
	w[78] = "sRsSlow" // char
	w[79] = "sOld2" // char filler
	w[80] = "sCCCFastOn" // char
	w[81] = "sCCFastSpeed" // char
	w[82] = "sF2Source" // char
	w[83] = "sTestRange" // char
	w[84] = "sTestDacPath" // char
	w[85] = "sMuxChannel" // char
	w[86] = "sMuxGain64" // char
	w[87] = "sVmonX100" // char
	w[88] = "sIsQuadro" // char
	w[89] = "sF1Mode" // char
	w[90] = "sOld3" // char
	w[91] = "sStimFilerHz" // double
	w[92] = "sRsTau" // double
	w[93] = "sDacToAdcDelay" // double
	w[94] = "sInputFilterTau" // double
	w[95] = "sOutputFilterTau" // double
	w[96] = "sVmonFactor" // double
	w[97] = "sCalibDate" // char[16]
	w[98] = "sVmonOffset" //double
	w[99] = "sEEPROMKind" // char
	w[100] = "sVrefX2" // char
	w[101] = "sHasVrefX2AndF2Vmon" // char
	w[102] = "sSpare1" // char filler
	w[103] = "sSpare2" // char filler
	w[104] = "sSpare3" // char filler
	w[105] = "sSpare4" // char filler
	w[106] = "sSpare5" // char filler
	w[107] = "sCCStimDacScale" // double
	w[108] = "sVmonFiltBandwidth" // double
	w[109] = "sVmonFiltFrequency" // double 

	return wn

End


Function/S createAmpValues(ampStructArr)

	 STRUCT ampSeriesArr &ampStructArr
	
	 variable nElements = 111    
    variable nAmps = 3
    variable nSeries = 100 
    
    variable iamps
    variable iseries
    
    string wn = "ampStorage"

    Make /T/O/N= (nAmps,nSeries,nElements) $wn
    WAVE /T w = $wn
    w = ""
   
   for(iamps=0;iamps<nAmps;iamps+=1)
   		for(iseries=0;iseries<nseries;iseries+=1)
   		

   			w[iamps][iseries][0] = ampStructArr.ampNumber[iamps].ampSeries[iseries].sStateVersion //char[8]
				
				w[iamps][iseries][1] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCurrentGain) //double 
				
				w[iamps][iseries][2] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF2Bandwidth) //double 
				
				w[iamps][iseries][3] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF2Frequency) //double 
				
				w[iamps][iseries][4] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRsValue) //double 
				
				w[iamps][iseries][5] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRsFraction) //double 
				
				w[iamps][iseries][6] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sGLeak) //double
				
				w[iamps][iseries][7] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCFastAmp1) //double
				
				w[iamps][iseries][8] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCFastAmp2) //double
				
				w[iamps][iseries][9] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCFastTau) //double
				
				w[iamps][iseries][10] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCSlow) //double
				
				w[iamps][iseries][11] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sGSeries) //double
				
				w[iamps][iseries][12] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVCStimDacScale) //double
				
				w[iamps][iseries][13] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCStimScale) //double
				
				w[iamps][iseries][14] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVHold) //double
				
				w[iamps][iseries][15] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sLastVHold) //double
				
				w[iamps][iseries][16] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVpOffset) //double
				
				w[iamps][iseries][17] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVLiquidJunction) //double
				
				w[iamps][iseries][18] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCIHold) //double
				
				w[iamps][iseries][19] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCSlowStimVolts) //double
				
				w[iamps][iseries][20] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCTrackVHold) //double
				
				w[iamps][iseries][21] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sTimeoutCSlow) //double
				
				w[iamps][iseries][22] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSearchDelay) //double
				
				w[iamps][iseries][23] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMConductance) //double
				
				w[iamps][iseries][24] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMCapacitance) //double
				
				w[iamps][iseries][25] = ampStructArr.ampNumber[iamps].ampSeries[iseries].sSerialNumber //	char   sSerialNumber[8]  // (*8 = SizeSerialNumber)
				
				w[iamps][iseries][26] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sE9Boards) //	int16 
				
				w[iamps][iseries][27] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCSlowCycles) //	int16 
				
				w[iamps][iseries][28] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sIMonAdc) //	int16 
				
				w[iamps][iseries][29] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVMonAdc) //	int16 
				
				w[iamps][iseries][30] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMuxAdc) //	int16 
				
				w[iamps][iseries][31] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sTestDac) //	int16 
				
				w[iamps][iseries][32] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimDac) //	int16 
				
				w[iamps][iseries][33] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimDacOffset) //	int16 
				
				w[iamps][iseries][34] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMaxDigitalBit) //	int16 
							
				w[iamps][iseries][35] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasCFastHigh) //	char
				
				w[iamps][iseries][36] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCFastHigh) //	char
				
				w[iamps][iseries][37] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasBathSense) //	char
				
				w[iamps][iseries][38] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sBathSense) //	char
				
				w[iamps][iseries][39] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasF2Bypass) //	char
				
				w[iamps][iseries][40] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF2Mode) //	char
				
				w[iamps][iseries][41] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sAmplKind) //	char
				
				w[iamps][iseries][42] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sIsEpc9N) //	char
				
				w[iamps][iseries][43] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sADBoard) //	char
				
				w[iamps][iseries][44] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sBoardVersion) //	char
				
				w[iamps][iseries][45] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sActiveE9Board) //	char
				
				w[iamps][iseries][46] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMode) //	char
				
				w[iamps][iseries][47] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRange) //	char
				
				w[iamps][iseries][48] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF2Response) //	char
				
				w[iamps][iseries][49] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRsOn) //	char
				
				w[iamps][iseries][50] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCSlowRange) //	char
				
				w[iamps][iseries][51] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCRange) //	char
				
				w[iamps][iseries][52] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCGain) //	char
				
				w[iamps][iseries][53] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCSlowToTestDac) //	char
				
				w[iamps][iseries][54] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimPath) //	char
				
				w[iamps][iseries][55] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCTrackTau) //	char
				
				w[iamps][iseries][56] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sWasClipping) //	char
				
				w[iamps][iseries][57] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRepetitiveCSlow) //	char
				
				w[iamps][iseries][58] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sLastCSlowRange) //	char
				
				w[iamps][iseries][59] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sOld1) //	char

				w[iamps][iseries][60] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCanCCFast) //	char
				
				w[iamps][iseries][61] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCanLowCCRange) //	char
				
				w[iamps][iseries][62] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCanHighCCRange) //	char
				
				w[iamps][iseries][63] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCanCCTracking) //	char
				
				w[iamps][iseries][64] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasVmonPath) //	char
				
				w[iamps][iseries][65] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasNewCCMode) //	char
				
				w[iamps][iseries][66] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSelector) //	char
			
				w[iamps][iseries][67] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHoldInverted) //	char
				
				w[iamps][iseries][68] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sAutoCFast) //	char
				
				w[iamps][iseries][69] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sAutoCSlow) //	char
				
				w[iamps][iseries][70] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasVmonX100) //	char
				
				w[iamps][iseries][71] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sTestDacOn) //	char
				
				w[iamps][iseries][72] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sQMuxAdcOn) //	char
				
				w[iamps][iseries][73] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sImon1Bandwidth) // double
				
				w[iamps][iseries][74] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimScale) // double
				
				w[iamps][iseries][75] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sGain) //	char
				
				w[iamps][iseries][76] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sFilter1) //	char
				
				w[iamps][iseries][77] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimFilterOn) //	char
				
				w[iamps][iseries][78] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRsSlow) //	char
				
				w[iamps][iseries][79] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sOld2) //	char
				
				w[iamps][iseries][80] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCCFastOn) //	char
				
				w[iamps][iseries][81] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCFastSpeed) //	char
				
				w[iamps][iseries][82] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF2Source) //	char
				
				w[iamps][iseries][83] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sTestRange) //	char
				
				w[iamps][iseries][84] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sTestDacPath) //	char
				
				w[iamps][iseries][85] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMuxChannel) //	char
				
				w[iamps][iseries][86] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sMuxGain64) //	char
				
				w[iamps][iseries][87] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVmonX100) //	char
				
				w[iamps][iseries][88] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sIsQuadro) //	char
				
				w[iamps][iseries][89] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sF1Mode) //	char
				
				w[iamps][iseries][90] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sOld3) //	char
				
				w[iamps][iseries][91] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sStimFilterHz) // double
				
				w[iamps][iseries][92] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sRsTau) // double
				
				w[iamps][iseries][93] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sDacToAdcDelay) // double
				
				w[iamps][iseries][94] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sInputFilterTau) // double
				
				w[iamps][iseries][95] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sOutputFilterTau) // double
				
				w[iamps][iseries][96] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVmonFactor) // double
				
				w[iamps][iseries][97] = ampStructArr.ampNumber[iamps].ampSeries[iseries].sCalibDate // char[16]
				
				w[iamps][iseries][98] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVmonOffset) // double
				
				w[iamps][iseries][99] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sEEPROMKind)
				
				w[iamps][iseries][100] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVrefX2)
				
				w[iamps][iseries][101] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sHasVrefX2AndF2Vmon)
				
				w[iamps][iseries][102] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSpare1)
				
				w[iamps][iseries][103] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSpare2)
				
				w[iamps][iseries][104] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSpare3)
				
				w[iamps][iseries][105] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSpare4)
				
				w[iamps][iseries][106] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sSpare5)
				
				w[iamps][iseries][107] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sCCStimDacScale)
				
				w[iamps][iseries][108] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVmonFiltBandwidth)
				
				w[iamps][iseries][109] = num2str(ampStructArr.ampNumber[iamps].ampSeries[iseries].sVmonFiltFrequency)

   	endfor
   endfor
    
   
   return wn 
    
End




/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////     PANEL WITH WAVE PARAMETERS     ///////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function WaveParametersPanel() : Panel
	PauseUpdate; Silent 1		// building window...
	string topwin = stringfromlist(0,winlist("*",";","WIN:81"))
	NewPanel /N=Parameter_Panel /W=(93,149,911,705) /K=1
	variable/g gRadioVal = 5

	ShowTools/A
	SetDrawLayer UserBack
		
	
	DrawText 44,44,"Experiments"
	ListBox experiments,pos={40.00,65.00},size={147.00,400.00},proc=panel_exp_proc
	ListBox experiments,listWave=root:explistw,selWave=root:expselw
	ListBox experiments,colorWave=root:mycolors,mode= 2,selRow= 0
	
	
	DrawText 213,42,"Series"
	ListBox series,pos={211.00,63.00},size={150.00,401.00},proc=panel_series_proc
	ListBox series,listWave=root:serieslistw,selWave=root:seriesselw
	ListBox series,colorWave=root:mycolors,mode= 2,selRow= 0
	
	//make it so they can only select one 
	//go to command help in the help browser
	panelchkboxes(kvNumTraces,213,30,500,0,"amp","")
	
//	//Listboxes from before
	DrawText 389,44,"Parameters"
//	ListBox waveElements,pos={384.00,61.00},size={173.00,403.00}
//	ListBox waveElements,listWave=root:ampElementList
	DrawText 569,42,"Values"
//	ListBox waveValues,pos={558.00,61.00},size={215.00,403.00}
	make /T/O/N=(111) wval
//	ListBox waveValues,listWave=wval
	
	
	make /o/n=(111,2)/T lb2dw = ""
	WAVE /T w = ampElementList
	lb2dw[][0] = w
	lb2dw[][1] = wval
	ListBox list0,pos={384,61},size={300.00,395.00},listWave=root:lb2dw,mode=2
	if(strlen(topwin)>0)
		dowindow/F $topwin
	endif
End


////////////////////////////////////////////////
//
//
//			ListBox _ PANEL_EXP_PROC
//
//
/////////////////////////////////////////////////


function panel_exp_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct
	String ctrlName = LB_Struct.ctrlName    // name of this control
	WAVE selw = LB_Struct.selwave
	variable selwsize = dimsize(selw,0)
	
	WAVE/T listw = LB_Struct.listWave
	variable listwsize = dimsize(listw,0)
	
	Variable expnum = LB_Struct.row       // row if click in interior, -1 if click in title
	Variable col  = LB_Struct.col      // column number
	Variable event  = LB_struct.eventCode    // event code
	Variable thiscolor=3
	string thisExp="",labext="_lab",explabwaven=""
	
	if(event == 4) //arrow keys or mouse selection
		thisExp = listw[expnum]
		//selw=0
		selw[expnum][0][1]=thiscolor
		selw[expnum][0][0]=1
		
		explabwaven = thisExp+labext
		
		string serieslistwn = ksserieslistwn
		string seriesselwn = ksseriesselwn
		
		WAVE/T explabw = $explabwaven
		WAVE/T serieslistw = $serieslistwn
		WAVE seriesselw = $seriesselwn

		panelPopulateSeriesList( expnum,  ksexplistwn, ksexpselwn, "_lab",ksserieslistwn,ksseriesselwn )

		variable success=0
		success=readexpcard(thisexp)
		if(!success)
			selw[expnum][0][2] = kvGrey
		else
			selw[expnum][0][2] = kvwhite
		endif
	endif
end

////////////////////////////////////////////////
//
//
//			ListBox _ PANEL_SERIES_PROC
//
//
/////////////////////////////////////////////////

//fix this so we don't touch collector

function panel_series_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct

	if( LB_Struct.eventcode == 2 )
		String ctrlName = LB_Struct.ctrlName    // name of this control
		WAVE selw = LB_Struct.selwave
		variable selwsize = dimsize(selw,0)
		
		WAVE/T listw = LB_Struct.listWave
		variable listwsize = dimsize(listw,0)
		
		Variable row = LB_Struct.row       // row if click in interior, -1 if click in title
		Variable col  = LB_Struct.col      // column number
		Variable event  = LB_struct.eventCode    // event code
		
		string thisitem="",datecode
		variable series = 0,index=inf
		if( row < listwsize ) //arrow keys or mouse selection
					thisitem = listw[ row ]
					datecode = datecodeZ( thisitem )
					series = seriesnumber( thisitem )
					
					variable trace = str2num(panelchkstatus("amp", 1)) // NEED TO GET A REAL TRACE NUMBER!! 911
	 				variable ampNumber = findAmpNumber(datecode, series, trace)
					
					WAVE/T twval = wval
					WAVE/T ampStor = ampStorage
					populateValuesList(twval, ampStor, series, ampNumber) 
					WAVE/T 	lb2dw = lb2dw //[][1] = wval
					lb2dw[][1] = twval[p]
						
		endif
	endif
end

////////////////////////////////////////////////
//
//
//			Checkbox _ panelchkProc
//
//
/////////////////////////////////////////////////



Function panelchkProc(cba) : CheckBoxControl
    STRUCT WMCheckboxAction &cba
    String chkname = cba.ctrlName

	NVAR gRadioVal= root:gRadioVal

	strswitch (chkname)
	case "amp1":
	gRadioVal= 1
	break
	case "amp2":
	gRadioVal= 2
	break
	case "amp3":
	gRadioVal= 3
	break
	case "amp4":
	gRadioVal= 4
	break
	endswitch
	
	CheckBox amp1, value=gRadioVal==1
	CheckBox amp2, value=gRadioVal==2
	CheckBox amp3, value=gRadioVal==3
	CheckBox amp4, value=gRadioVal==4
	
       
End

////////////////////////////////////////////////
//
//
//			PANEL CHECK BOX FUNCTIONS 
//
//
////////////////////////////////////////////////

function panelchkboxes(nboxes,startx,dx,starty,dy,basen,basetitle)
variable nboxes,startx,dx,starty,dy
string basen,basetitle
	//check box row and column stuff
	variable i,chrow=startx,swdx=dx,chcol=starty,swdy=dy
	string chbase=basen, tit=basetitle,chn,chtitle
	
//	NVAR gRadioVal= root:gRadioVal
	for(i=0;i<nboxes;i+=1)
		chn=chbase+num2str(i+1)
		chtitle=tit+num2str(i+1)
		CheckBox $chn pos={chrow+swdx*i,chcol+swdy*i}, title=chtitle, fsize=8, value=0, proc=panelchkProc
	endfor
end

function/S panelchkstatus(chkname,num) // returns list of checked boxes
string chkname
variable num
variable i
string chklist="",thischeckbox=chkname
	for(i=1;i<=num;i+=1)
		thischeckbox=chkname+num2str(i)
		controlInfo $thischeckbox
		if(V_Value==1)
			chklist=num2str(i)
		endif
	endfor
return chklist
end

////////////////////////////////////////////////
//
//
//		 FIND AMP NUMBER FOR PANEL FUNCTION
//
//
////////////////////////////////////////////////

function findAmpNumber(datecode, series, trace)

	String datecode
	variable series
	variable trace
	
	
	variable igroup = 0
	variable isweep = 0
	
	String panelWaveName = datecode+"g"+num2str(igroup+1)+"s"+num2str(series)+"sw"+num2str(isweep+1)+"t"+num2str(trace)
	variable ampN = returnNoteInfoByKey(panelWaveName, "TrAmpIndex")
	if( numtype( ampN) != 0 )
		print "WARNING! findampnumber failed to find amp number! returning 1 as default", panelwavename, ampN 
		ampN = 1
	endif	
	
	return ampN
	

end


////////////////////////////////////////////////
//
//
//			Specific to parameter panel _ POPULATE THE VALUES LIST
//
//
/////////////////////////////////////////////////

function/WAVE populateValuesList(twval, tampStorage, series, ampNumber)  
	
	WAVE/T &twval
	WAVE/T &tampStorage
	variable series
	variable ampNumber
	
	twval = tampStorage[ampNumber-1][series-1][p]

end

////////////////////////////////////////////////
//
//
//			Specific to parameter panel _ POPULATE THE SERIES LIST
//
//
/////////////////////////////////////////////////

function panelPopulateSeriesList( expnum, expwn, expselwn, label_ext, serieswn ,serselwn)
variable expnum //inf if all exp, otherwise only process this experiment
string expwn, expselwn, label_ext,serieswn,serselwn

variable thiscolor = 1

WAVE/T expw = $expwn
variable i, nexp = dimsize(expw, 0), j, nseries=0, k=0,expstart=0

WAVE expselw = $expselwn
string thisexp="", explabellist="", serieslabel=""

WAVE/T seriesw = $serieswn
nseries = dimsize(seriesw, 0)

WAVE serselw = $serselwn

make/O/T/N=2000 labelledseries
labelledseries=""
if(expnum!=inf)
	expstart=expnum
	nexp = expstart+1
	thiscolor = 3
endif
for( i=expstart ; i < nexp ; i+=1 )
	thisexp = expw[i]
//	print "in populate series list: ",thisexp	
	explabellist=thisexp+label_ext
	WAVE/T explabellistw = $explabellist
	nseries = dimsize(explabellistw,0)
	expselw[i][0][1]=0
	for( j=0; j < nseries; j+=1 )
		labelledseries[k]=thisexp+"g1s"+num2str(j+1)
		expselw[i][0][1]=thiscolor
		k+=1
	endfor
endfor
nseries = k //dimsize(labelledseries,0)
redimension/N=(nseries,1) seriesw
redimension/N=(nseries,1,-1) serselw
seriesw = labelledseries
end

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////        PANEL!!!                                ///////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

///////////
/// buildCollectorPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-31
/// NOTES: Height and width are percentage of screen
///////////
function buildCollectorPanel([string panelName, variable showExp, variable showClearButtons, variable showExpCard, variable fillExpCard, variable showTraceDispCtrls, variable showAnalysisButtons, variable panelWidth, variable panelHeight, variable killExisting])
	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif

	// Show the experiment group list box?
	// As of 2024-06-03, most of the buttons associated with this list box do not
	// have coded procedures associated with them, so defaulting to not show
	if(paramIsDefault(showExp))
		showExp = 0
	endif

	if(paramIsDefault(showClearButtons))
		showClearButtons = 0
	endif

	if(paramIsDefault(fillExpCard))
		fillExpCard = 0
	endif
	
	if(paramIsDefault(showExpCard))
		showExpCard = 0
	endif

	// As of 2024-06-03, getting errors when running the analysis card and ANALYZE buttons,
	// so defaulting to remove from the display
	if(paramIsDefault(showAnalysisButtons))
		showAnalysisButtons = 0
	endif

	if(paramIsDefault(killExisting))
		killExisting = 0
	endif
	
	

	// As of 2024-06-03, the underlying function to update based on these controls
	// (updateCollectorDisplay) does not have any action on the display
	// so defaulting to not show the controls
	if(paramIsDefault(showTraceDispCtrls))
		showTraceDispCtrls = 0
	endif
	
	DoWindow/F $panelName  // bring panel to the front if it exists
    if( V_Flag != 0 )       // panel exists
		if(!killExisting)
			print "the_Collector is already open. Please close if you want to remake"
        	return NaN
		endif
		killwindow/Z $panelName
    endif

    DFREF panelDFR = root:
    
	// define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP

    // initialize it
    InitScreenParameters(ScrP)

	if(paramIsDefault(panelWidth))
		panelWidth = 93
	endif

	if(paramIsDefault(panelHeight))
		panelHeight = 80
	endif
	
	

    // Create the panel
    AGG_makePanel(5, 0, 5 + panelWidth, 0 + panelHeight, panelName)
    // modifypanel cbRGB=(50000,50000,50000)

    variable width, height
    [width, height] = getWindowDims(type = "point")

    // print width
    variable tabFSize = width/100
    variable panelFSize = width/88
    if(panelFSize>18)
        panelFSize = 18
    endif

	if(panelFSize<9)
		panelFSize = 9
	endif
    
    // print "tab font size", tabFSize, "panel font size", panelFSize
    //the current publishing standard is 1 pt = 1/72"). In other words points are fixed in size independent of resolution. This is good since, all things properly configured, 12 point text on an monitor will be the same height when printed on paper.

    DefaultGUIFont/W=$panelName panel = {"Arial", panelFSize, 0}, all = {"Arial", panelFSize, 0}, TabControl = {"Arial", tabFSize, 0}
    DefaultGUIFont/W=$panelName graph = {"Arial", 16, 0}, table = {"Arial", panelFSize, 0}
    // DefaultFont

    // Use a structure for positioning to pass easily to subfunctions
    STRUCT AGG_EventsPanelSizeInfo panelSize
    
    // Panel Sizing
		variable usableLeft = 0.01, usableRight = 0.99, usableTop = 0.005, usableBottom = 0.995
		variable usableWidth = usableRight - usableLeft, usableHeight = usableBottom - usableHeight
		variable xPosRel = usableLeft
		variable yPosRel = usableTop
		variable xRightPosRel = usableWidth / 2 + usableLeft

		variable xLeftPos = posRelPanel(xPosRel, "width", panelName = panelName)
		variable yTopPos = posRelPanel(yPosRel, "height", panelName = panelName)
		variable xRightPos = posRelPanel(xRightPosRel, "width", panelName = panelName)
		
		variable colWidthRel = usableWidth / 2 / 4, colWidth = posRelPanel(colWidthRel, "width", panelName = panelName)
		variable colItemWidthRel = colWidthRel * 0.95, colItemWidth = colWidth * 0.95

		variable checkWidthRel = (usableRight - xRightPosRel)/kvNumSweeps, checkWidth = posRelPanel(checkWidthRel, "width", panelName = panelName)

		variable rightButtonWidthRel = 0.11, rightButtonWidth = posRelPanel(rightButtonWidthRel, "width", panelName = panelName)
		variable displayButtonWidthRel = rightButtonWidthRel / 2, displayButtonWidth = rightButtonWidth / 2
		variable displayDxRel = displayButtonWidthRel * 1.1, displayDx = displayButtonWidth * 1.1
		
		variable buttonHeightRel = 0.038, buttonHeight = posRelPanel(buttonHeightRel, "height", panelName = panelName)
		variable listBoxHeightRel = 0.62, listBoxHeight = posRelPanel(listBoxHeightRel, "height", panelName = panelName)

		variable buttonRowHeightRel = buttonHeightRel * 1.1, buttonRowHeight = buttonHeight * 1.1

		variable leftRel = xRightPosRel, topRel = yPosRel + buttonHeightRel, rightRel = usableRight, bottomRel = buttonHeightRel + yPosRel + listBoxHeightRel
		variable left = posRelPanel(leftRel, "width", panelName = panelName), right = posRelPanel(rightRel, "width", panelName = panelName)
		variable top = posRelPanel(topRel, "height", panelName = panelName), bottom = posRelPanel(bottomRel, "height", panelName = panelName)

		variable buttonRowsYPos = bottom + buttonHeight*0.2, lastRowYPos = posRelPanel(usableBottom - buttonHeightRel, "height", panelName = panelName)

	// Variables
	// 2024-06-03, AGG set maxes to 0, so that empty rows aren't created with initial setup
	// Would prefer to redimension when adding experiments
	// variable maxexp=300, maxlabels=1, maxseries=10000, maxexpgrp=1
	variable maxexp=0, maxlabels=0, maxseries=0, maxexpgrp=0
	variable maxSelwPanes=5

	string tabControls = ""

	// color wave ! ! !	
	variable gray=60000
	Make/O/W/U panelDFR:mycolors= {{0,0,0},{65535,0,0},{0,65535,0},{0,0,65535},{0,65535,65535},{ gray,gray,gray },{ 65535,65535,65535 }}
	Wave/SDFR=panelDFR myColors
	MatrixTranspose myColors

	// Experiments column e.g. 20150505a
		variable xPos = xLeftPos, yPos = yTopPos, buttonWidth = colItemWidth, listBoxWidth = colItemWidth
		TitleBox expText title="Experiments", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
		yPos += buttonHeight
		tabControls += "expText;"

		// Make experiment list waves
		make/T/O/N=(maxexp,1) panelDFR:explistw/Wave=expListW
		make/O/N=(maxexp,1,maxselwpanes) panelDFR:expselw/Wave=expSelW
		explistw=""
		expselw=0
		SetDimLabel 2, 1, forecolors, expselw				// redefine plane 1 as foreground mycolors
		SetDimLabel 2, 2, backcolors, expselw				// redefine plane 2 as background mycolors

		ListBox list_exp,pos={xPos,yPos},size={listBoxWidth,listBoxHeight}, mode=2, listwave=explistw, selwave=expselw,colorwave=mycolors, proc = lb_exp_proc
		tabControls += "list_exp;"

		yPos = buttonRowsYPos
		Button expupdatebutt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="Select files",proc=bc_ExpUpdate_proc
		tabControls += "expupdatebutt;"
		yPos += buttonRowHeight

		Button showAllExpButt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="show all exp series",proc=bc_SeriesForExp_proc
		tabControls += "showAllExpButt;"
		yPos += buttonRowHeight

		CheckBox fillExpCardCheck, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Fill experiment card", value = fillExpCard
		tabControls += "fillExpCardCheck;"
		yPos += buttonRowHeight
		
		CheckBox showExpCardCheck, pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Bring card to front", value = showExpCard
		tabControls += "showExpCardCheck;"
		yPos += buttonRowHeight
		
		if(showClearButtons)
			yPos = lastRowYPos
			button expClearButt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc
			tabControls += "expClearButt;"
		endif

	// Label column e.g. "PASSIVE"
		xPos += colWidth
		yPos = yTopPos
		TitleBox labelText title="Label list", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
		yPos += buttonHeight
		tabControls += "labelText;"

		make/T/O/N=(maxlabels,1) labellistw
		make/O/N=(maxlabels,1,maxselwpanes) labelselw
		SetDimLabel 2, 1, forecolors, labelselw				// redefine plane 1 as foreground mycolors
		SetDimLabel 2, 2, backcolors, labelselw				// redefine plane 2 as background mycolors
		labellistw = ""
		labelselw = 0

		ListBox list_label,pos={xPos,yPos},size={listBoxWidth,listBoxHeight}, mode=2, listwave=labellistw, selwave=labelselw, colorwave=mycolors, proc = lb_label_proc	
		tabControls += "list_label;"
		
		if(showClearButtons)
			yPos = lastRowYPos
			button labelClearButt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc
			tabControls += "labelClearButt;"
		endif
	
	// series column e.g. 20150505ag1s15 (selected from all series based on label/cell)	
		xPos += colWidth
		yPos = yTopPos
		TitleBox seriesText title="Series list", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
		yPos += buttonHeight
		tabControls += "seriesText;"

		make/T/O/N=(maxseries,1) serieslistw
		make/O/N=(maxseries,1,maxselwpanes) seriesselw
		SetDimLabel 2, 1, forecolors, seriesselw					// redefine plane 1 as foreground mycolors
		SetDimLabel 2, 2, backcolors, seriesselw				// redefine plane 2 as background mycolors
		serieslistw=""
		seriesselw=0

		ListBox list_series, pos={xPos,yPos},size={listBoxWidth,listBoxHeight}, mode=2, listwave=serieslistw,selwave=seriesselw, colorwave=mycolors, proc = lb_series_proc
		tabControls += "list_series;"

		yPos = buttonRowsYPos
		Button button6,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="import series list", proc= bc_ImportSeriesProc
		tabControls += "button6;"
		yPos += buttonRowHeight
		
		if(showClearButtons)
			yPos = lastRowYPos
			button seriesClearButt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc
			tabControls += "seriesClearButt;"
		endif

	//experimental group column, e.g. ovx am
	if(showExp)
		xPos += colWidth
		yPos = yTopPos
		TitleBox expGroupText title="Experimental group", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
		yPos += buttonHeight
		tabControls += "expGroupText;"

		make/T/O/N=(maxexpgrp,1) expgrplistw
		make/O/N=(maxexpgrp,1,maxselwpanes) expgrpselw
		SetDimLabel 2, 1, forecolors, expgrpselw				// redefine plane 1 as foreground mycolors
		SetDimLabel 2, 2, backcolors, expgrpselw				// redefine plane 2 as background mycolors
		expgrplistw=""
		expgrpselw=0

		ListBox list_expgrp,pos={xPos,yPos},size={listBoxWidth,listBoxHeight}, mode=5, listwave=expgrplistw, selwave=expgrpselw, colorwave=mycolors, proc=lb_EXPGRP_proc
		tabControls += "list_expgrp;"
		
		yPos = buttonRowsYPos
		Button button2,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="select all egroup"
		tabControls += "button2;"
		yPos += buttonRowHeight
		
		Button button7,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="attach series"
		tabControls += "button7;"
		yPos += buttonRowHeight

		Button button8,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="attach exp"
		tabControls += "button8;"
		yPos += buttonRowHeight

		Button expgrpADDbutt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="add group",proc=bc_EXPGRP_ADD_proc
		tabControls += "expgrpADDbutt;"
		yPos += buttonRowHeight

		Button button10,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="delete group"
		tabControls += "button10;"
		yPos += buttonRowHeight

		if(showClearButtons)
			yPos = lastRowYPos
			button expGrpClearButt,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc
			tabControls += "expGrpClearButt;"
		endif
	endif

	buttonWidth = rightButtonWidth
	//sweeps selector
		yPos = buttonRowsYPos
		xPos = xRightPos
		TitleBox sweepsText title="Sweeps", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LC
		yPos += buttonRowHeight
		tabControls += "sweepsText;"

		makechkboxes(kvNumSweeps, xPos, checkWidth, yPos, 0,"checkSW","", targwin = panelName)
		variable iCheck
		for(iCheck=0; iCheck<kvNumSweeps; iCheck++)
			tabControls += "checkSW" + num2str(iCheck+1) + ";"
		endfor

	//traces selector
		yPos += buttonRowHeight
		TitleBox traceText title="Traces", pos={xPos,yPos}, frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LC
		yPos += buttonRowHeight
		tabControls += "traceText;"

		makechkboxes(kvNumTraces, xPos, checkWidth, yPos, 0, "checkTR","", targwin = panelName)
		for(iCheck=0; iCheck<kvNumTraces; iCheck++)
			tabControls += "checkSW" + num2str(iCheck+1) + ";"
		endfor

	// Set sweeps button
		xPos = right - buttonWidth
		yPos -= buttonRowHeight

		Button checkAllSweepsButton, pos={xPos, yPos}, size={buttonWidth, buttonHeight}, title = "Check all sweeps", proc=collectorSwitchCheckProc
		tabControls += "checkAllSweepsButton;"
		yPos += buttonRowHeight
		
		Button uncheckAllSweepsButton, pos={xPos, yPos}, size={buttonWidth, buttonHeight}, title = "Uncheck all sweeps", proc=collectorSwitchCheckProc
		tabControls += "uncheckAllSweepsButton;"
		yPos += buttonRowHeight

		Button checkAllTracesButton, pos={xPos, yPos}, size={buttonWidth, buttonHeight}, title = "Check all traces", proc=collectorSwitchCheckProc
		tabControls += "checkAllTracesButton;"
		yPos += buttonRowHeight

		Button uncheckAllTracesButton, pos={xPos, yPos}, size={buttonWidth, buttonHeight}, title = "Uncheck all traces", proc=collectorSwitchCheckProc
		tabControls += "uncheckAllTracesButton;"
		yPos += buttonRowHeight

		Display/W=(left,top,right,bottom)/HOST=$panelName/N=G0
		string tabDisplays = "G0"

		xPos = xRightPos
		yPos = buttonRowsYPos + buttonRowHeight * 4
		Button bPass, pos={ xPos, yPos}, size={buttonWidth, buttonHeight},title="Passive",proc=bPassiveProc
		tabControls += "bPass;"
		yPos += buttonRowHeight
		
		if(showAnalysisButtons)
			Button bAcard, pos={ xPos, yPos}, size={buttonWidth, buttonHeight},title="analysis card",proc=bAcardProc
			tabControls += "bAcard;"
			yPos += buttonRowHeight

			Button bAnalyze, pos={ xPos, yPos}, size={buttonWidth, buttonHeight},title="ANALYZE!",proc=bAnalyzeProc
			tabControls += "bAnalyze;"
			yPos += buttonRowHeight
		endif

		Button bReplicate, pos={ xPos, yPos}, size={buttonWidth, buttonHeight},title="replicate graph",proc=bReplicateGraph
		tabControls += "bReplicate;"
		yPos += buttonRowHeight
		
		if(showTraceDispCtrls)
			assembleDisplayControls(x0 = xRightPos + buttonWidth * 1.05, y0 = buttonRowsYPos + buttonRowHeight * 4, dx = displayDx, dy = buttonRowHeight, h = buttonHeight, w = displayButtonWidth)	
		endif
		
		SetActiveSubwindow ##
end


macro COLLECTOR() 
	buildCollectorPanel(showExp = 1, showClearButtons = 1, showAnalysisButtons = 1, showTraceDispCtrls = 1, panelWidth = 75, panelHeight = 60, fillExpCard = 1, showExpCard = 1)
// 	PauseUpdate; Silent 1		// building window...
// 	variable top=20, left =10, dy=25, dx=150,lbw=140,lbh=490
// 	variable buttTop=550, buttLeft=left, buttwx=120, buttwy=20

// 	NewPanel /N=the_Collector/K=1/W=(0,0,1280,720)
// 	scaleForScreenSize("the_collector")
// 	ShowTools/A
// 	SetDrawLayer UserBack

// 	variable maxexp=300, maxlabels=1, maxseries=10000, maxexpgrp=1
// 	variable maxSelwPanes=5

// // color wave ! ! !	
// 	variable gray=60000
// 	Make/O/W/U mycolors= {{0,0,0},{65535,0,0},{0,65535,0},{0,0,65535},{0,65535,65535},{ gray,gray,gray },{ 65535,65535,65535 }}
// 	MatrixTranspose myColors
	
// //experiments e.g. 20150505a
// 	DrawText left,top,"Experiments"
// 	make/T/O/N=(maxexp,1) explistw
// 	make/O/N=(maxexp,1,maxselwpanes) expselw
// 	explistw=""
// 	expselw=0
// 	SetDimLabel 2, 1, forecolors, expselw				// redefine plane 1 as foreground mycolors
// 	SetDimLabel 2, 2, backcolors, expselw				// redefine plane 2 as background mycolors

// 	ListBox list_exp,pos={left,top},size={lbw,lbh}, mode=2, listwave=explistw, selwave=expselw,colorwave=mycolors, proc = lb_exp_proc
// 	// Button button4,pos={buttLeft,buttTop},size={buttwx,buttwy},title="select all (exp)"
// 	Button expupdatebutt,pos={buttLeft,buttTop+dy},size={buttwx,buttwy},title="update",proc=bc_ExpUpdate_proc
// 	Button showAllExpButt,pos={buttLeft,buttTop+dy*3},size={buttwx,buttwy},title="show all exp series",proc=bc_SeriesForExp_proc
// 	button expClearButt,pos={buttLeft,buttTop+dy*5},size={buttwx,buttwy},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc
	
// //labels, e.g. "PASSIVE"
// 	DrawText left+dx,top,"Label list"
// 	make/T/O/N=(maxlabels,1) labellistw
// 	make/O/N=(maxlabels,1,maxselwpanes) labelselw
// 	SetDimLabel 2, 1, forecolors, labelselw				// redefine plane 1 as foreground mycolors
// 	SetDimLabel 2, 2, backcolors, labelselw				// redefine plane 2 as background mycolors

// 	labellistw = ""
// 	labelselw = 0
// 	ListBox list_label,pos={left+dx,top},size={lbw,lbh}, mode=2, listwave=labellistw, selwave=labelselw, colorwave=mycolors, proc = lb_label_proc	
// 	// Button button1,pos={buttLeft+dx,buttTop},size={buttwx,buttwy},title="select all (label)"
// 	button labelClearButt,pos={buttLeft+dx,buttTop+dy*5},size={buttwx,buttwy},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc

// //series, e.g. 20150505ag1s15 (selected from all series based on label)	
// 	DrawText left+dx*2,top,"Series list"
// 	make/T/O/N=(maxseries,1) serieslistw
// 	make/O/N=(maxseries,1,maxselwpanes) seriesselw
// 	SetDimLabel 2, 1, forecolors, seriesselw					// redefine plane 1 as foreground mycolors
// 	SetDimLabel 2, 2, backcolors, seriesselw				// redefine plane 2 as background mycolors

// 	serieslistw=""
// 	seriesselw=0
// 	ListBox list_series,pos={left+dx*2,top},size={lbw,lbh}, mode=2, listwave=serieslistw,selwave=seriesselw, colorwave=mycolors, proc = lb_series_proc
// 	// Button button3,pos={buttLeft+dx*2,buttTop},size={buttwx,buttwy},title="select all series"	
// 	Button button6,pos={buttLeft+dx*2,buttTop+dy*1},size={buttwx,buttwy},title="import series", proc= bc_ImportSeriesProc
// 	button seriesClearButt,pos={buttLeft+dx*2,buttTop+dy*5},size={buttwx,buttwy},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc

// //experimental group, e.g. ovx am
// 	DrawText left+dx*3,top,"Experimental group"
// 	make/T/O/N=(maxexpgrp,1) expgrplistw
// 	make/O/N=(maxexpgrp,1,maxselwpanes) expgrpselw
// 	SetDimLabel 2, 1, forecolors, expgrpselw				// redefine plane 1 as foreground mycolors
// 	SetDimLabel 2, 2, backcolors, expgrpselw				// redefine plane 2 as background mycolors

// 	expgrplistw=""
// 	expgrpselw=0
// 	ListBox list_expgrp,pos={left+dx*3,top},size={lbw,lbh}, mode=5, listwave=expgrplistw, selwave=expgrpselw, colorwave=mycolors, proc=lb_EXPGRP_proc
// 	Button button2,pos={buttLeft+dx*3,buttTop},size={buttwx,buttwy},title="select all egroup"
// 	Button button7,pos={buttLeft+dx*3,buttTop+dy*1},size={buttwx,buttwy},title="attach series"
// 	Button button8,pos={buttLeft+dx*3,buttTop+dy*2},size={buttwx,buttwy},title="attach exp"
// 	Button expgrpADDbutt,pos={buttLeft+dx*3,buttTop+dy*3},size={buttwx,buttwy},title="add group",proc=bc_EXPGRP_ADD_proc
// 	Button button10,pos={buttLeft+dx*3,buttTop+dy*4},size={buttwx,buttwy},title="delete group"
// 	button expGrpClearButt,pos={buttLeft+dx*3,buttTop+dy*5},size={buttwx,buttwy},title="clear",fcolor=(65535,0,0), proc=bc_Clear_proc

// //sweeps selector
// 	DrawText 608,butttop,"Sweeps"
// 	makechkboxes(kvNumSweeps,left+dx*4,30,butttop ,0,"checkSW","")
// 	DrawText 608,butttop + 2*buttwy,"Traces"
// 	//traces selector
// 	makechkboxes(kvNumTraces,left+dx*4,30,butttop + 2*buttwy ,0,"checkTR","")

// // Set sweeps button
// 	variable gx=650,gy=500 
// 	variable xPos = left+dx*4 + gx - buttwx
// 	variable yPos = butttop + dy
// 	Button checkAllSweepsButton, pos={xPos, yPos}, size={buttwx, buttwy}, title = "Check all sweeps", proc=collectorSwitchCheckProc
// 	yPos += dy
// 	Button uncheckAllSweepsButton, pos={xPos, yPos}, size={buttwx, buttwy}, title = "Uncheck all sweeps", proc=collectorSwitchCheckProc
// 	yPos += dy
// 	Button checkAllTracesButton, pos={xPos, yPos}, size={buttwx, buttwy}, title = "Check all traces", proc=collectorSwitchCheckProc
// 	yPos += dy
// 	Button uncheckAllTracesButton, pos={xPos, yPos}, size={buttwx, buttwy}, title = "Uncheck all traces", proc=collectorSwitchCheckProc
// 	yPos += dy

// 	Display/W=(left+dx*4,top,left+dx*4+gx,top+gy)/HOST=#  

// 	Button bPass,pos={ 608, butttop + 3*buttwy},size={buttwx,buttwy},title="Passive",proc=bPassiveProc
// 	Button bAcard,pos={ 608, butttop + 4*buttwy},size={buttwx,buttwy},title="analysis card",proc=bAcardProc
// 	Button bAnalyze,pos={ 608, butttop + 5*buttwy},size={buttwx,buttwy},title="ANALYZE!",proc=bAnalyzeProc
// 	Button bReplicate,pos={ 608, butttop + 6*buttwy},size={buttwx,buttwy},title="replicate graph",proc=bReplicateGraph
	
// 	assembleDisplayControls()	
	
// 	RenameWindow #,G0
// 	SetActiveSubwindow ##
EndMacro

////////////////////////////////////////////////
//
//
//			Button _ ANALYSIS CARD ! _ PROC : handles all clear buttons
//
//
/////////////////////////////////////////////////
function bPassiveProc( s ) : ButtonControl
STRUCT WMButtonAction &s

	if( s.eventcode == 2 )
		string win = s.win
		string path = "collector_data"
		string expcode = ""
		// get expcode from panel
		string subwaven = ""
		// get subwaven from selected series
		// serieslistw, seriesselw
		subwaven = returnserieslbsel()
		print "bPassiveProc: ", subwaven
		//expcode = datecodeGREP2( subwaven )
		expcode = datecodegrep2( subwaven )
		string wl = ""
		wl = CollectorPassive( win, path, expcode, subwaven )
	endif

end

function/s returnSeriesLBsel( )
	string serieslistwn = "serieslistw"
	string seriesselwn = "seriesselw"
	WAVE/T slw = $serieslistwn
	WAVE ssw = $seriesselwn
	if( waveexists( slw ) && waveexists( ssw ) )
		//variable i, n=numpnts( slw )
		//for( i=0 ; i<n ; i+=1 )
			//print ssw //[i][0][0]
		//endfor
		//print ssw
		controlinfo list_series
		print V_value, s_value
		string out = slw[ V_Value ]
	else
		print "can't find the list and sel waves", serieslistwn, seriesselwn
	endif
	return out
end

////////////////////////////////////////////////
//
//
//			Button _ ANALYSIS CARD ! _ PROC : handles all clear buttons
//
//
/////////////////////////////////////////////////
function bAcardProc( ctrlname ) : ButtonControl
string ctrlname

 analysiscard()

end
////////////////////////////////////////////////
//
//
//			Button _ ANALYSIS CARD ! _ PROC : handles all clear buttons
//
//
/////////////////////////////////////////////////
function bAnalyzeProc( ctrlname ) : ButtonControl
string ctrlname

SetUpCrunch()

end
////////////////////////////////////////////////
//
//
//			Button _ ANALYSIS CARD ! _ PROC : handles all clear buttons
//
//
/////////////////////////////////////////////////
function bReplicateGraph( ctrlname ) : ButtonControl
string ctrlname

recreateTopGraph()

end
////////////////////////////////////////////////
//
//
//			Button _ CLEAR _ PROC : handles all clear buttons
//
//
/////////////////////////////////////////////////
function bc_clear_proc( ctrlname ) : ButtonControl
string ctrlname

strswitch ( ctrlname )
	case "expClearButt":
		WAVE/T explistw = explistw
		WAVE expselw = expselw
		redimension/N=(1,1) explistw
		redimension/N=(1,1,-1) expselw
		explistw=""
		expselw=0
		break
	case "labelClearButt":
		WAVE/T labellistw = labellistw
		WAVE labelselw = labelselw
		redimension/N=(1,1) labellistw
		redimension/N=(1,1,-1) labelselw
		labellistw=""
		labelselw=0
		break
	case "seriesClearButt":
		Wave/T serieslistw
		wave seriesSelw
		redimension/N=(1,1) serieslistw
		redimension/N=(1,1,-1) seriesselw
		serieslistW = ""
		seriesSelw = 0
		break
	case "expGrpClearButt":
		Wave/T expgrplistw
		Wave expgrpselw
		redimension/N=(1,1) expgrplistw
		redimension/N=(1,1,-1) expgrpselw
		expgrplistw = ""
		expgrpselw = 0
		break
	default:
		print "can't identify ctrlname:",ctrlname
		break
endswitch

end

///////////
/// bc_ImportSeriesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Switched to modern button control structure
///////////
function bc_ImportSeriesProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		importseriesALL(panelName = B_Struct.win)
	endif
	return 0
end


////////////////////////////////////////////////
//
//
//			Button _ ExpUpdate _ PROC : runs "dfile" to get all exps and labels in a folder
//
//
/////////////////////////////////////////////////
///////////
/// bc_expUpdate_proc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Update button function to use structure, get panel name of calling button
///////////
function bc_expUpdate_proc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		variable getDir = 1 // 1 requires user to select file, 0 uses default and fails
		dfile(getDir, panelName = B_Struct.win)
	endif
	return 0
end


////////////////////////////////////////////////
//
//
//			ListBox _ EXPERIMENT _ PROC
//
//
/////////////////////////////////////////////////
function lb_exp_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct
	String ctrlName = LB_Struct.ctrlName    // name of this control
	WAVE selw = LB_Struct.selwave
	variable selwsize = dimsize(selw,0)
	
	WAVE/T listw = LB_Struct.listWave
	variable listwsize = dimsize(listw,0)
	
	Variable expnum = LB_Struct.row       // row if click in interior, -1 if click in title
	Variable col  = LB_Struct.col      // column number
	Variable event  = LB_struct.eventCode    // event code
	Variable thiscolor=3
	string thisExp="",labext="_lab",explabwaven=""
	
	if(event == 4) //arrow keys or mouse selection
		selw[][0][0]=0 // reset all selection to 0
		selw[][0][1]=0
		variable iSel

		// Set the colors for all experiments
		for(iSel=0; iSel<selwsize; iSel++)
			string selLabel = getSelectedItem("list_label", hostName = LB_Struct.win)
			if(strlen(selLabel)==0)
				continue
			endif

			thisexp = listw[iSel]
			if(strlen(thisexp) == 0)
				continue
			endif

			string explabellist=thisexp+labext // AGG - labeled list of series for a given experiment
			WAVE/T explabellistw = $explabellist
			if(!WaveExists(explabellistw))
				continue 
			endif

			variable nseriesExp = dimsize(explabellistw,0)
			variable iSeries
			for( iSeries=0; iSeries < nseriesExp; iSeries+=1 )
				string serieslabel = explabellistw[iSeries]
				if( stringmatch(selLabel,serieslabel) )
					selw[iSel][0][1]=1 // red, matches label
				endif
			endfor
		endfor

		thisExp = listw[expnum]
		//selw=0
		selw[expnum][0][1]=thiscolor
		selw[expnum][0][0]=1
		
		explabwaven = thisExp+labext
		
		string lablistwn = kslabellistwn
		string labselwn = kslabelselwn
		string serieslistwn = ksserieslistwn
		string seriesselwn = ksseriesselwn
		
		WAVE/T explabw = $explabwaven
		WAVE/T lablistw = $lablistwn
		WAVE labselw = $labselwn
		WAVE/T serieslistw = $serieslistwn
		WAVE seriesselw = $seriesselwn
		
		variable nexplab = dimsize(explabw,0),i
		variable nlabels = dimsize(lablistw,0),j
		variable nseries = dimsize(serieslistw,0),k,labelindex
		
		string explab,lab,thislabel

		labelindex =returnselected("list_label")
		if(labelindex < 0) // if nothing is selected, select first row
			ListBox list_label, selrow = 0, win=$LB_Struct.win
			labelindex =returnselected("list_label")
		endif
		thislabel = lablistw[labelindex]
		labselw[][0][1]=0

		populateSeriesList( thislabel, expnum,  ksexplistwn, ksexpselwn, "_lab",ksserieslistwn,ksseriesselwn )

		// print labelindex, lablistw[labelindex]
		
		// highlight labels for this experiment		
		for(i=0;i<nexplab;i+=1)
			explab = explabw[i]
			for(j=0;j<nlabels;j+=1)
				lab = lablistw[j]
				if( stringmatch(explab, lab) )
					labselw[j][0][1]=thiscolor
				endif
			endfor
		endfor
		variable success=0

		variable fillCard = getCBVal("fillExpCardCheck", panelName = LB_Struct.win)
		if(fillCard)
			success=readexpcard(thisexp)
			if(!success)
				selw[expnum][0][2] = kvGrey
			else
				selw[expnum][0][2] = kvwhite
			endif
		endif

		if(getCBVal("showExpCardCheck", panelName = LB_struct.win) && fillCard)
			DoWindow/F experimentCard
		endif

		// Clear series list selection and display
		listbox List_series selrow=-1, win = $LB_Struct.win
		removealltraces(LB_Struct.win + "#G0")
	endif
end

////////////////////////////////////////////////
//
//
//			ListBox _ LABEL _ PROC
//
//
/////////////////////////////////////////////////
function lb_label_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct
	String ctrlName = LB_Struct.ctrlName    // name of this control
	WAVE selw = LB_Struct.selwave
	variable selwsize = dimsize(selw,0)
	
	WAVE/T listw = LB_Struct.listWave
	variable listwsize = dimsize(listw,0)
	
	Variable row = LB_Struct.row       // row if click in interior, -1 if click in title
	Variable col  = LB_Struct.col      // column number
	Variable event  = LB_struct.eventCode    // event code
	
	string thislabel=""
	
	if(event == 4) //arrow keys or mouse selection
		thislabel = listw[row]
		selw[][0][1]=0
		selw[row][0][1]=1
		populateSeriesList( thislabel, inf, ksexplistwn, ksexpselwn, "_lab",ksserieslistwn,ksseriesselwn )

		listbox List_Exp selrow=-1, win = $LB_Struct.win
		listbox List_series selrow=-1, win = $LB_Struct.win

		removealltraces(LB_Struct.win + "#G0")
	endif
end

///////////
/// clearCollectorSeriesList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-05
/// NOTES: 
///////////
function clearCollectorSeriesList()
	wave/T listW = $ksserieslistwn
	wave selW = $ksseriesselwn

	if(!WaveExists(listW) || !WaveExists(selW))
		return NaN
	endif

	redimension/N=0 listW, selW
end


////////////////////////////////////////////////
//
//
//			ListBox _ SERIES _ PROC
//
// stores the selected series in userdata in the order it was selected ! 20150624
// for use with crunch and acard
/////////////////////////////////////////////////
function lb_series_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct
	String ctrlName = LB_Struct.ctrlName    // name of this control
	WAVE selw = LB_Struct.selwave
	variable selwsize = dimsize(selw,0)
	
	WAVE/T listw = LB_Struct.listWave
	variable listwsize = dimsize(listw,0)
	
	Variable row = LB_Struct.row       // row if click in interior, -1 if click in title
	Variable col  = LB_Struct.col      // column number
	Variable event  = LB_struct.eventCode    // event code
	
	string thisitem="",datecode
	variable series = 0,index=inf
	
	switch(event)
		case 4: // cell selection: mouse or arrow keys
			if( row < listwsize ) //arrow keys or mouse selection
				thisitem = listw[ row ]
				datecode = datecodeZ( thisitem )
				series = seriesnumber( thisitem )
				
				importseries( datecode, series, panelName = LB_Struct.win ) // imports and displays !!!
				
				//update selection in Experiments ListBox
				WAVE expselw = $ksExpSelwn //ksExpSelwn
				expselw[][0][0] = 0
				index = textWlistIndex( ksExpListwn, datecode )
				listbox List_Exp selrow=index, win = $LB_Struct.win
				LB_struct.userdata = thisitem + ";"
			endif
			break
		case 5: // cell selection with shift key
			print "case 5!",row, listw[row]
			LB_struct.userdata += listw[ row ] + ";"
			print "case 5!",row, listw[row]
			thisitem = listw[ row ]
			datecode = datecodeZ( thisitem )
			series = seriesnumber( thisitem )			
			importseries( datecode, series, app=1, panelName = LB_struct.win ) // imports and displays !!!
			break
		default: // sometimes multiple event codes, be sure to check if a series list item is selected to set exp
			thisitem = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)
			if(strlen(thisItem) == 0)
				listbox list_exp selrow=listwsize-1, win = $LB_Struct.win
			endif
			datecode = datecodeZ( thisitem )
			index = textWlistIndex( ksExpListwn, datecode )
			listbox list_exp selrow=index, win = $LB_Struct.win
	endswitch
	//print "in lb_series_proc", LB_struct.userdata
end

////////////////////////////////////////////////
//
//
//			ListBox _ EXPGRP _ PROC
//
//
/////////////////////////////////////////////////
function lb_EXPGRP_proc( LB_Struct ) : ListBoxControl
	STRUCT WMListboxAction &LB_Struct
	String ctrlName = LB_Struct.ctrlName    // name of this control
	WAVE selw = LB_Struct.selwave
	variable selwsize = dimsize(selw,0)
	
	WAVE/T listw = LB_Struct.listWave
	variable listwsize = dimsize(listw,0)
	
	Variable row = LB_Struct.row       // row if click in interior, -1 if click in title
	Variable col  = LB_Struct.col      // column number
	Variable event  = LB_struct.eventCode    // event code
	
	string thisitem="",datecode
	variable series = 0,index=inf
	switch( event )
		case 2: // mouse up
			break
		case 3: //double click //4: arrow keys or mouse selection
			print "here"
			if(row<selwsize)
				selw[row][0][0]=0x02
			else
				redimension/N=(listwsize+1) listw
				redimension/N=(selwsize+1,1,-1) selw
				selw[selwsize][0][0]=0x02
			endif
			listbox List_Exp setEditCell={row, 0, 0, inf}
			break
		case 4: // cell selection
			//get selected exp
			//set back color 5 or 6 for grey
			variable thisexp = selected(ksExpSelwn)
			WAVE expselw = $ksExpSelwn
			expselw[thisexp][0][0]=5
			break
		case 7: //done editing!
			selw[row][0][0]=0
			break
		default:
			break
	endswitch
	
end
////////////////////////////////////////////////
//
//
//			Button _ EXPGRP_ADD _ PROC : runs "dfile" to get all exps and labels in a folder
//
//
/////////////////////////////////////////////////
function bc_EXPGRP_ADD_proc( ctrlname ) : ButtonControl
string ctrlname
variable getdir=1 // 1 requires user to select file, 0 uses default and fails
//ksExpgrplistwn="expGrpListw", ksExpgrpselwn="expGrpSelw"
string listwn=ksExpGrpListwn, selwn=ksExpGrpSelwn
WAVE/T listw = $listwn
WAVE selw = $selwn

//ADD


end

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//	filename datecode routines and utilites
////////////////////////////////////////////////////////////
function textWListIndex(twaven,teststr) //returns index of text wave containing teststr; returns inf if not found
string twaven, teststr
WAVE/T tw = $twaven
variable i, n=dimsize(tw,0), out=inf
string thisitem=""
for(i=0;i<n;i+=1)
	thisitem=tw[i]
	if( stringmatch(thisitem, teststr) )
		out=i
		i=inf
	endif	
endfor
return out
end

function/s datecodefromfilename(filen)
string filen
// receives a filename like 20150505a.dat, returns 20150505a
variable dot=strsearch(filen,".",inf,3)-1
string output = filen[0,dot]
return output
end

function/s filenamefromdatecode(datecode)
string datecode
// appends .dat to datecode to form a filename
string output = datecode+".dat"
return output
end

// get selection from selection wave
function selected(selwn)
string selwn
variable output=0, n, i
WAVE selw = $selwn
	n = dimsize( selw, 0 )
	for(i=0;i<n;i+=1)
		if(selw[i][0][0]==1)
			output=1
			i=inf
		endif
	endfor
return output
end 

function returnselected(ctrln)
string ctrln
controlinfo $ctrln
return v_value
end


function makechkboxes(nboxes,startx,dx,starty,dy,basen,basetitle, [targwin])
variable nboxes,startx,dx,starty,dy
string basen,basetitle
string targwin // aim for a specific window/panel
	//check box row and column stuff
	variable i,chrow=startx,swdx=dx,chcol=starty,swdy=dy
	string chbase=basen, tit=basetitle,chn,chtitle
	if( paramisdefault( targwin ) )
		for(i=0;i<nboxes;i+=1)
			chn=chbase+num2str(i+1)
			chtitle=tit+num2str(i+1)
			CheckBox $chn pos={chrow+swdx*i,chcol+swdy*i}, title=chtitle, fsize=8, value=1, proc = updateCollectorDisplayOnlyProc
		endfor
	else
		for(i=0;i<nboxes;i+=1)
			chn=chbase+num2str(i+1)
			chtitle=tit+num2str(i+1)
			CheckBox $chn pos={chrow+swdx*i,chcol+swdy*i}, title=chtitle, fsize=8, value=1, win=$targwin, proc = updateCollectorDisplayOnlyProc
		endfor
	endif
end

///////////
/// getCollectorCheckVal
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: Return NaN if check doesn't exist, 0 if unchecked, 1 if checked
///////////
function getCollectorCheckVal(variable checkNum[, string sweepOrTrace, string panelName])
	if(paramIsDefault(sweepOrTrace))
		sweepOrTrace = "sweep"
	endif

	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif
	
	string chkName
	strswitch (sweepOrTrace)
		case "sweep":
			chkName = "checkSW"
			break
		case "trace":
			chkName = "checkTR"
			break
		default:
			return NaN
			break
	endswitch
	
	chkName += num2str(checkNum) // append check number

	controlInfo/W=$panelName $chkName
	if(V_Flag < 0)
		return NaN // doesn't exist
	endif

	return V_value
end


///////////
/// collectorSwitchCheckProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: 
///////////
function collectorSwitchCheckProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		string bName = B_Struct.ctrlName
		variable setVal
		string sweepOrTrace
		strswitch (bName)
			case "checkAllSweepsButton":
				setVal = 1
				sweepOrTrace = "sweep"
				break
			case "uncheckAllSweepsButton":
				setVal = 0
				sweepOrTrace = "sweep"
				break
			case "checkAllTracesButton":
				setVal = 1
				sweepOrTrace = "trace"
				break
			case "uncheckAllTracesButton":
				setVal = 0
				sweepOrTrace = "trace"
				break
			default:
				return NaN
				break
		endswitch

		setAllCollectorChecks(setVal = setVal, sweepOrTrace = sweepOrTrace, panelName = B_Struct.win)
		updateCollectorDisplayOnly(panelName = B_Struct.win)
	endif
	return 0
end



///////////
/// setAllCollectorChecks
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: 
///////////
function setAllCollectorChecks([variable setVal, string sweepOrTrace, string panelName])
	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif

	if(paramIsDefault(sweepOrTrace))
		sweepOrTrace = "sweep"
	endif

	variable lastCheck
	strswitch (sweepOrTrace)
		case "sweep":
			lastCheck = 20
			break
		case "trace":
			lastCheck = 4
			break
		default:
			return NaN
			break
	endswitch

	if(paramIsDefault(setVal))
		variable lastSweepState = getCollectorCheckVal(lastCheck, sweepOrTrace = sweepOrTrace, panelName = panelName)
		setVal = (lastSweepState == 0) // will swap value from current last check if no setVal provided
	endif

	variable i
	for(i=0; i<lastCheck; i++)
		setCollectorCheck(i+1, setVal, sweepOrTrace = sweepOrTrace, panelName = panelName)
	endfor
end


///////////
/// setCollectorCheck
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-30
/// NOTES: 
///////////
function setCollectorCheck(variable checkNum, variable val[, string sweepOrTrace, string panelName])
	if(paramIsDefault(sweepOrTrace))
		sweepOrTrace = "sweep"
	endif

	if(paramIsDefault(panelName))
		panelName = "the_Collector"
	endif
	
	string chkName
	strswitch (sweepOrTrace)
		case "sweep":
			chkName = "checkSW"
			break
		case "trace":
			chkName = "checkTR"
			break
		default:
			return NaN
			break
	endswitch
	
	chkName += num2str(checkNum) // append check number

	controlInfo/W=$panelName $chkName
	if(V_Flag < 0)
		return NaN // doesn't exist
	endif

	CheckBox $chkName, value = val, win = $panelName
end





//////////////////////////////////
function setchkboxFromWavelist( chkname, wl )
string chkname, wl
variable i, n, val = 1 // default to select all
string chkn = ""

	n = itemsinlist( wl )
	if ( n > 20 ) 
		n = 20
	endif
	for( i=0 ; i < n ; i+=1 )
		chkn = chkname + num2str( i+1 )
		CheckBox $chkn value = val
	endfor

end
//////////////////////////////////




function/S chkstatus(chkname,num[, panelName]) // returns list of checked boxes
string chkname, panelName
variable num
variable i

if(paramIsDefault(panelName))
	panelName = ksCollectorPanelName
endif

string chklist="",thischeckbox=chkname
	for(i=1;i<=num;i+=1)
		thischeckbox=chkname+num2str(i)
		controlInfo/W=$panelName $thischeckbox
		if(V_Value==1)
			chklist+=num2str(i)+";"
		endif
	endfor
//print chklist
return chklist
end

//////////////////////////////////////////////////
// merge two waves, remove duplicates, adds new items to the end... TO DO: alphabetize?
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-06-03
/// NOTES: Issue #125, fixed indexing error when adding a new item
function mergew(sourcewn, destwn)
string sourcewn, destwn

WAVE/T sourcew = $sourcewn

//print sourcew[11]

WAVE/T destw = $destwn

variable is=0,id=0, ns = numpnts( sourcew ), nd ,flag=0,icount=0

string thisitem="",thatitem=""

for( is = 0 ; is < ns ; is+=1) // loop over each item in sourcew
	flag=0
	thisitem = sourcew[ is ]
	//print is, thisitem
	nd = dimsize( destw, 0 )
	for( id = 0; id < nd ; id+=1 ) // compare with each item in destw
		thatitem = destw[id][0]	
		if( stringmatch( thisitem, thatitem ) )
			flag=1
			id=inf
		endif
	endfor
	if( flag == 0 )
		redimension/N=(nd+1,1) destw
		// AGG 2024-06-03 Switched from nd-1 to nd, as nd-1 was overwriting the *old* last item, and leaving the 
		// newly appended last item blank, which could lead to some of the source items not making it into the dest w
		// and extra blank rows in some cases
		destw[nd][0]=thisitem
	else
	endif
endfor

end


function/T returnCheckTraces(wn) // puts in wn of array with check trace info, 0 not check, 1 checked
//variable all // if all=0 
string wn
string base="checkTR", chkn=""
variable i, maxtr=4

make/O/N=(maxtr) $wn
WAVE checks=$wn
checks=0

for(i=1;i<=maxtr;i+=1)
	chkn=base+num2str(i)
	controlinfo /W=$ksCollectorPanelName $chkn
	checks[i]=V_value
//	print "in return check tracen",V_value
endfor
//print checks
end

function returnFirstCheckTrace() // puts in wn of array with check trace info, 0 not check, 1 checked
//variable all // if all=0 
string base="checkTR", chkn=""
variable i, maxtr=4

for(i=1;i<=maxtr;i+=1)
	chkn=base+num2str(i)
	controlinfo /W=$ksCollectorPanelName $chkn
	if(V_value)
		break
	endif
//	print "in return check tracen",V_value
endfor
return i
end


// // test function for getampstatestruct
// function/S testGetAmpStruct()
// 		string datecode = "20220228A" //This will give us access to the filename (year, month, day, then letter) 
// 		variable seriesn = 1
// 		variable ADCchannel = 6
// 		STRUCT AmplifierStateRecord S

// 		string filename = ""
// 		variable refnum
// 		filename = filenamefromdatecode(datecode)

// 		Print "in testgetampstatestruct:", fileName
// 		open /R /P=collector_data refnum as filename

// 		print getAmpStateStruct(refnum, datecode, seriesn, ADCchannel, S)
		
// 		close refnum

// 		print S 
// end 


// test function for getampstatestruct
function updateAmpSerieslist( serieslist, refnum )
	string serieslist 
	variable refnum // assumes file is already open and will be closed by calling function(s)

		variable success = 0 
		string datecode = "" //This will give us access to the filename (year, month, day, then letter) 
		variable seriesn = nan
		variable ADCchannel = nan, AmpIndex = nan 
		STRUCT AmplifierStateRecord S

		string wn = ""
		variable i, n 
		n = itemsinlist( serieslist )
		for( i=0; i< n; i+=1 )
			wn = stringfromlist( i, serieslist )
			datecode = datecodeGREP2( wn )
			seriesn = seriesnumberGREP( wn )
			ADCchannel = returnNoteInfoByKey( wn, "TrADCchannel" )
			AmpIndex = returnNoteInfoByKey( wn, "TrAmpIndex" )
			//print wn, getAmpStateStruct(refnum, datecode, seriesn, ADCchannel, AmpIndex, S)
		endfor 		
		return success
end 

function returnNoteInfoByKey( wn, key ) // returns TrADCchannel from wavenot
	string wn // wavename
	string key // keyword in note string

	wn = removequotes( wn )
	variable nOut = nan 
	if( waveexists( $wn ))
		WAVE w = $wn 
		string wnote = note( w )
		wnote = replacestring( "\r", wnote, "" )
		string str = ""
		str = stringbykey( key, wnote )
		nOut = str2num( str )
	endif

	return nOut 	
end

/////////////////////////////////////////////////////////////////
///////////// FUNCTION TO PULL AMP STATE STUCT ////////////////// 
// returns full wavename associated with amp state
//
// was function/S getAmpStateStruct(refnum, datecode, seriesn, ADCchannel, AmpIndex, Amp)
function/S getAmpStateStruct(refnum, datecode, Amp)
		variable refnum 
		string datecode //This will give us access to the filename (year, month, day, then letter) 
		// variable seriesn	
		// variable ADCchannel
		// variable AmpIndex 
		//STRUCT AmplifierStateRecord &S // pass by reference struct array defined below
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////ADDING IN STUFF TO READ CUSTOM AMPLIFIER STUFF////////////////////////
		STRUCT ampSeriesArr	&Amp  		// ampSeriesArr is defined in HekaFileDefs, 3 amps, 100 series at this time 20220323
// access is Amp.AmpNumber[0-2].AmpSeries[0-99] = ...amplifierStateRecord.AmAmplifierState

//STRUCT AmplifierState S  //THIS IS ALREADY INITIALIZED SO I THINK WE JUST NEED THE STRUCTURE

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
		
		Variable i //,refnum
		string filename = "", wnout = "failed"
		variable BYTEoRDER = 2 // assumes mac corrects below if error
							 
		filename = filenamefromdatecode(datecode)

		//Print "in getampstatestruct:", fileName
		//open /R /P=collector_data refnum as filename

		// code below is modified from returnserieslist 202203116

// NOW READ THE BUNDLE HEADER USING A STRUCTURE
STRUCT	bundleHeader 				myBundleHeader	

/////////////////////////ADDING IN STRUCTURE STUFF FOR AMPLIFIER FILE///////////////////////////
STRUCT  amplifierRootRecord			ampedRoot
STRUCT	amplifierSeriesRecord		ampedSeries
STRUCT	amplifierStateRecord		ampedState


//start reading the tree, set back to beginning of file 
Fsetpos refnum, 0
//reads header, predefined in heka file definitions
FBinRead /B=(BYTEoRDER) refnum, myBundleHeader

//ERROR CHECKING
if((myBundleHeader.oItems<0)||(myBundleHeader.oItems>10))
	byteOrder=3
	Fsetpos refnum, 0
	//tries to fix by changing byte order
	FBinRead /B=(BYTEoRDER) refnum, myBundleHeader
//	print "Something wrong with byte order, trying again", myBundleHeader.oIslittleendian
	if((myBundleHeader.oItems<0)||(myBundleHeader.oItems>10))
		print "Still failing to read header properly.  Contact tech support!  tony.defazio@gmail.com"
		abort
	endif
endif	

if(myBundleHeader.oIslittleendian==1)
	byteOrder=3
else
	byteOrder=2
endif

//modern data format, Signature is DAT2 
if(stringmatch(myBundleHeader.oSignature,"DAT2")==1)
	// DAT2 files have informative headers.
	// However, this program is so well written that we don't need this information.
else  
	// if not DAT2, we can handle DAT1 bundles 
	// by skipping the header read and asking the loop
	// to read the file until nothing remains
	print filename, "Blank or corrupted header.  This is not your fault.  There is nothing you can do about it.",mybundleheader.oitems
	mybundleheader.oitems=10
	print "Corrected header for current analysis only: ",mybundleheader.oitems	
endif

//print mybundleheader


//do while (maybe add parameters) 
//looping over oItems (main branches of the tree)
//puls = data are, (one of the oItems)amp = info for user, sim = wave form 
variable datapos, start, length, pos, nlevels, nchildren
string extension = "", magicnumber = "", thiswavename = ""

i=0
do

// Here is where we check for the corrupted header information:
	start = mybundleheader.oBundleItems[i].oStart
	length = mybundleheader.oBundleItems[i].oLength
	extension = mybundleheader.oBundleItems[i].oExtension

//	print "ReadHEKA loop: item in bundle, start, length, ext: ",i, start,length,extension
	
	if((i==0)&&((start==0)||(length==0)))
		print "Looks like the header in this file is messed up.  We'll try to get data out the hard way."
		variable go=1,myEOF=0
		string myBloodySearchString=""
		myBloodySearchString=padstring(myBloodySearchString,4,0)
		datapos=0
		fstatus refnum
		myEOF = V_logEOF
		do
			fsetpos refnum, datapos
			fbinread /b=(byteOrder) refnum, myBloodySearchString
		//	print myBloodySearchString
			if(stringmatch(mybloodysearchstring,"Tree")||stringmatch(mybloodysearchstring,"eerT"))
				go=0
				print "got it!!"
				start=datapos-4
			endif
			datapos+=4
		while((go==1)&&(datapos<myEOF))
// here are the magic lines of code that will accomplish this:
//  la la la la
// blah blah blah

		if(go==1)
			print "Failed to locate file information.  Sorry."
			abort
		endif
	endif
	
//	if(stringmatch(showfiles,extension))
	
	strswitch (extension)
		case ".dat":  //loads all data into one wave, for now
			break
		case ".pgf":  //read the PGF file, Tree format
			break
		case ".pul":
			break
		case ".amp":
		
			magicnumber=padstring(magicnumber,4,0)
			pos=start
			fsetpos refnum,pos
			fbinread /b=(byteOrder) refnum, magicnumber
			pos+=4
			//read the number of levels
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nLevels
			pos+=4
			make /o/n=(nlevels) AmpedLevelSizes
			fsetpos refnum,pos
			//AmpedLevelSizes are correct root=80, series=16 , state=560
			fbinread /b=(byteOrder)/f=3 refnum, ampedlevelsizes	
			
			pos+=nlevels*4
			
			// read the root of the tree, there is only one root record
			fsetpos refnum,pos
			fbinread /b=(byteOrder) refnum, ampedRoot

			// move up 80, which is the size of the root, so we enter the series
			pos+=ampedlevelsizes[0]
			
			//first read the root children, which are the series
			//possible that series is actually sweeps
			fsetpos refnum,pos
			fbinread/b=(byteOrder)/f=3 refnum, nchildren

			variable Xseries, aseries, Xstates, astates 
			Xseries=nchildren
			pos+=4

			if(xseries>100)
				print "WARNING! some series amp states not stored: ", xseries
				xseries = 100
			endif
			
			if(xseries>100)
				print "WARNING! some series amp states not stored: ", xseries 
				xseries = 100
			endif
			// start at 0 and loop over the series
			// this loops over the series associted with this date code / .dat file
			//   note that the amp state doesnt change for sweeps or traces
			//   so these properties are shared for all waves in a series 
			for(aseries=0;aseries<Xseries;aseries+=1)

				//read cuurent series record
				fsetpos refnum, pos
				fbinread /b=(byteOrder) refnum, ampedSeries
				

				//increment by the series level size
				pos+=ampedlevelsizes[1]
				
					
				//get children, which gives us the states	
				//the states are going to represent the amplifiers
				fsetpos refnum,pos
				fbinread /b=(byteOrder) /f=3 refnum, nchildren
				Xstates=nchildren
				pos+=4		
				
				
				//intializing the values I want to grab from the state records
				make/O/N=(Xstates) sccihold
				make/O/N=(Xstates) svhold
				sccihold=inf
				svhold=inf

				// this loop is probably over the number of amplifiers
				for(astates=0;astates<Xstates;astates+=1)
				
					fsetpos refnum, pos
					fbinread /b=(byteOrder) refnum, ampedState
					
					pos+=ampedlevelsizes[2]	
																
					//read number of children BUT state shouldn't have any
					fsetpos refnum,pos
					fbinread /b=(byteOrder) /f=3 refnum, nchildren
					pos+=4	
										
					thisWaveName = datecodeGREP2(filename) + "g1s" + num2str(aseries+1) + "_amp" + num2str(astates+1)//+sweeplabel+num2str(isweep+1)+tracelabel+num2str(itrace+1)

					//sccihold[astates]=ampedState.AmAmplifierState.sCCIHold
					//svhold[astates]=ampedState.AmAmplifierState.sVhold
					// string pmver = ampedroot.roversionname
					// variable sImonADC = ampedState.AmAmplifierState.sImonADC
					// variable sVmonADC = ampedState.AmAmplifierState.sVmonADC
					// variable sMuxADC = ampedState.AmAmplifierState.sMuxADC
					
					///////////////////////////////////////////////////////////////////////////////////////////////
					///////////////////////////////////////////////////////////////////////////////////////////////
					
					//SEEMS TO WORK, CHECK WITH TONY THAT I AM USING CORRECT VARIABLES TO ACCESS THE SERIES AND AMPS AND SUCH
					
					//////////////////////////Trying to put each state into the custom array///////////////////////
					Amp.ampNumber[astates].ampSeries[aseries] = ampedState.AmAmplifierState 
					
					///////////////////////////////////////////////////////////////////////////////////////////////
					///////////////////////////////////////////////////////////////////////////////////////////////

				endfor
			endfor
		
			break	
		case ".txt": // skipping txt component
			break
		default:
//			print "Unknown extension in bundle file.",extension
	endswitch
	
//	endif
	i+=1
while (i<=mybundleheader.oItems)
		
		//close refnum

// NOW RETURNS THE STUCT WE WANT, MAYBE IT SHOULD JUST RETURN THE STRUCTURE NOW?!?
// can only return sting or variable
wnout = "SUCCESS"
return wnout

end
