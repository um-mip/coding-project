#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later


function makeWaterfall(wave/t twave)
	
	// break twave into segments of 10 waves
	
	variable segSize = 8
	variable pnts = numpnts(twave)
	variable segs = floor(pnts/segSize)
	
	Make/O/FREE/T/N=(segSize) tWaveSeg
	
	variable i 
	
	// cut into segments of 10 waves
	for(i=0; i<segs; i+=1)
		tWaveSeg = twave[p+(i*segSize)]
	endfor
	
	// handle the remainder
	if(mod(pnts,segs)!=0)
		print "mod"
		Make/O/FREE/T/N=(mod(pnts,segSize)) tWaveSeg
		tWaveSeg = twave[p+(segs*segSize)]
	endif
	
	plotTWaveList(tWaveSeg,windowName="windowname",waterfall=1)
end 



function/WAVE makeList(wave/t waveNames, wave conditionalWave, variable condition, String newWaven)
// from a list of waves waveNames, narrow the list down to indices in conditionalWave that are equal to condition
// assumes corresponding indices i.e. waveNames[0] inclusion in newListName is contingent on value in conditionalWave[0]
// there is likely a more efficient way to do this ??

	Extract/INDX/FREE conditionalWave, tempWave, conditionalWave==condition
	if (numpnts(tempWave)<1)
		Extract/INDX/Free conditionalWave, tempwave, numtype(conditionalWave)==2 /// this is not elegant, need better way around conditional NaN not working
	endif

	if(numpnts(tempwave)>0)
		Make/O/N=(numpnts(tempwave))/T $newWaven = waveNames[tempwave[p]]
	else
		Make/O/N=0/T $newWaven
	endif	
	return $newWaven
end


function/WAVE tWaveListAverage(wave/t wavePool, string aveWave) 
	//average waves in a text wave
	
	String tw2sl = textwave2stringlist(nameofWave(wavePool))

	if(strlen(tw2sl)>0) // if there's any waves
		fWaveAverage(tw2sl,"",0,0,aveWave,"")
	else
		make/O/N=0 $aveWave
		print "No waves for", nameofWave(wavePool)
	endif

	return $aveWave

end

function plotTWaveList(wave/t twavelist, [string windowName, variable waterfall])
	//graph all waves listed in the text wave in a single window
	//if windowName is not provided, name the window automatically 
	//if windowName is provided, create the window or overwrite an existing window with that name 
	//waterfall != 0 creates a waterfall graph (8 wave display maximum)
	
	if (ParamisDefault(windowName)==1)
		windowName = "NewGraph"
		Display/K=1 $windowName
	else
		// [variable curLeft, variable currTop, variable curRight, variable curBottom] = getWindowLocation(windowName = windowName)
		// KillWindow/Z $windowName
		// Display/K=1/N=$windowName/W=(curLeft, currTop, curRight, curBottom)
		clearDisplay(windowName)
	endif
		
	variable nwaves = numpnts(twavelist)
	variable i
			
	if (ParamIsDefault(waterfall)==0) // make waterfall plot? 
		
		if (nwaves>8)// this is not supported for more than 8 waves! the displayed plots will be too small / too crowded if ngraphs >8  
			Print("waterfall plot does not support more than 8 waves. Only showing first 8 waves")
			nwaves = 8
		endif
			
		variable buffer = 0.02
		variable range = (1-(nwaves*buffer))/nwaves
		variable top = 1
		variable bottom = top - range
			
		Make/O/T/FREE/N=(8) yaxisNames = "left" + num2str(p)
				
		for(i=0;i<nwaves;i+=1)
			AppendtoGraph/W=$windowName/L=$yaxisNames[i] $twavelist[i]
			ModifyGraph/W=$windowName axisEnab($yaxisNames[i])={bottom,top},freePos($yaxisNames[i])=0, fSize($yaxisNames[i])=12,lblPosMode($yaxisNames[i])=4,lblPos($yaxisNames[i])=55
			top-=range+buffer
			bottom= top-range
		endfor
		
	else 
		for(i=0; i<nwaves; i+=1)
			// print nameofwave($twavelist[i])
			AppendtoGraph/W=$windowName $twavelist[i]
		endfor
	endif 	
	
end

function TextWave_NaNstoEmpty(wave/t textWave)
// clean up the text wave appeareance by replacing "NaN" strings with empty string ""

	variable rows = dimsize(textWave,0)
	variable columns = dimsize(textWave,1)
	
	if (columns ==0) // 1D waves have dimsize(..,1) == 0, therefore loop will not execute
		columns = 1
	endif


	variable i
	variable j
	
	for (j=0;j<columns;j+=1)
	 
			for (i=0;i<rows;i+=1)
				if ((StringMatch(textWave[i][j],"NaN")==1))
					textWave[i][j] = ""
					
				endif
			endfor
	endfor

end