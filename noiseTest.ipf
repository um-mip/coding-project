// demonstrate that noise is irreverisible
// use idealized PSC model
// add noise
// generate fit
// repeat to create histogram
// insert smoothing, renormalization, re-ranging: nothing helps
// noise is lost data / information

// td 20220917
// call noisecomplaint in the command window
// for example
// 		noisecomplaint( 1000, "000" )
// this will run 1000 curve fits with no parameter held
// use "110" to only allow tau to vary
//
function noiseComplaint( ntests, holdstring ) // Big D and the Kids Table
variable ntests //  = 1000 // repeat fit 1000 to generate histograms
string holdstring // "000" all free, "110" tau free
variable i // index
variable wdur = 110 // ms, total duration
variable wxoff = 10 // ms, zero of the x-axis, time before this will be negative
variable wnpts = 1100 // total number of points
variable sleepsec = 0.0001

// fit ranges
variable fit_max = 0.8
variable fit_min = 0.2
variable xs, xe // start and end x values for FIND

string wn = "wn" // name of original idealized PSC
make/O/N=(wnpts) $wn
WAVE w = $wn 
setscale/i x -wxoff, (wdur - wxoff), w // sets the scale 


make/O/N=(4) c = { 0, 1, 10, 0 } // coef for fit func
// y0, A, tau, x0

// get idealized PSC
w = deltafexp( c, x )
duplicate/O w, wfree

display/k=1 wfree, w
ModifyGraph rgb(wfree)=(0,0,0)
setaxis left, -0.25, 1.25

// make storage
// these are also the initial guesses
make/O/N=(4) coefs // y0, A, tau, x0
coefs[0] = 0 // y0 baseline
coefs[1] = 1 // A amplitude
coefs[2] = 10 // tau mseconds
coefs[3] = 0 // x0 mseconds
duplicate/O coefs, backup_coefs 

string hold = holdstring // "110" // y0, A, tau; 000 all free
string wn_max = "w_max" + "_" + hold
string wn_y0 = "w_y0" + "_" + hold
string wn_A = "w_A" + "_" + hold
string wn_tau = "w_tau" + "_" + hold
string wn_x0 = "w_x0" + "_" + hold
string wn_xs = "w_xs" + "_" + hold
string wn_xe = "w_xe" + "_" + hold 

make/O/N=(ntests) $wn_max, $wn_y0, $wn_A, $wn_tau, $wn_x0, $wn_xs, $wn_xe
WAVE w_max = $wn_max 
WAVE w_y0 = $wn_y0 
WAVE w_A = $wn_A 
WAVE w_tau = $wn_tau
WAVE w_x0 = $wn_x0
WAVE w_xs = $wn_xs 
WAVE w_xe = $wn_xe 

for( i=0; i<ntests; i+=1 )

	// add noise
	wfree = w + gnoise(0.1)

	// smooth  "BOX 9"
	smooth/b 9, wfree 

	// rescale/normalize
	wavestats/Z/Q wfree 
	w_max[i] = V_max 
	w_x0[i] = V_maxloc 
	// normalize
	wfree /= w_max[i]
	// move peak to 0

// find levels
	xs = xlevel( wfree, fit_max, -1, xstart = 0 )
	xe = xlevel( wfree, fit_min, -1, xstart = 0 )
	w_xs[i] = xs 
	w_xe[i] = xe 

// 	fit
	// set coefs for initial guess
	coefs = backup_coefs	
	fitEXP_XOFFSET( wfree, xs, xe, coefs, hold ) 
	w_y0[i] = coefs[0]
	w_A[i] = coefs[1]
	w_tau[i] = coefs[2]
	//w_x0[i] = coefs[3]

endfor 
makeHisto(w_max, xlabel="box 9 amplitude (normalized units)", x_vertline=1)
makeHisto(w_xs, xlabel="time at 80% (msec)", x_vertline=0.0022)
makeHisto(w_xe, xlabel="time at 20% (msec)", x_vertline=0.016)
makeHisto(w_y0, xlabel="baseline (Y0, normalized units)", x_vertline=0)
makeHisto(w_A, xlabel="fit amplitude (A, normalized units)", x_vertline=1)
makeHisto(w_tau, xlabel="decay time constant (tau, msec)", x_vertline=0.01)
makeHisto(w_x0, xlabel="x offset (x0, msec)", x_vertline=0)
end


function fitEXP_XOFFSET( w, xs, xe, coefs, hold )
	WAVE w // wave to fit
	variable xs, xe // fit start and end
	WAVE coefs // contains the initial guesses
	string hold // specify which 3 var to hold 
	// coefs[0] = y0
	// coefs[1] = A
	// coefs[2] = tau
	// coefs[3] = x0 // not a guess, x0 !

	make/O/N=(3) wcoefs // curve fit coefficients
	// get the initial guesses, these are set in main code block
	wcoefs[0] = coefs[0]
	wcoefs[1] = coefs[1]
	wcoefs[2] = coefs[2]
	variable x0 = coefs[3] // not used, /K={0} sets x0 here
	CurveFit/Q/G/K={0}/H=hold exp_XOffset, kwCWave=wcoefs, w( xs, xe ) /D 
	// pass the fit parameters back via coefs wave reference
	coefs[0] = wcoefs[0]
	coefs[1] = wcoefs[1]
	coefs[2] = wcoefs[2]
	coefs[3] = coefs[3] 
	//return coefs 
end


function xlevel( w, yvalue, direction, [xstart] )
	wave w 
	variable yvalue // value to find
	variable direction // direction to search
	// +1 forward, -1 backward
	variable xstart // optional start x value, default is 0

	variable xout // output
	variable xs = 0  // xstart and end range
	variable xe = rightx(w) // ends at the last x value

	if( !paramisdefault(xstart) )
		xs = xstart 
	endif // only if xstart is specified
	if(direction < 0)
		xe = xs
		xs = rightx( w )  // this is first x value
	endif

	findlevel /Q/R=(xs,xe) w, yvalue 
	if( V_flag == 0 )
		xout = V_levelX
	else
		xout = NaN 
	endif
	return xout
end 

function makeHisto( w, [nbins, xlabel, x_vertline ] )
	wave w
	variable nbins 
	string xlabel
	variable x_vertline

	variable numbins = 100
	if(!paramisdefault(nbins))
		numbins = nbins 
	endif
	string wn = nameofwave( w )
	string wn_hist = wn + "_hist"
	Make/N=(numbins)/O $wn_hist; DelayUpdate
	Histogram/B=1 w, $wn_hist; DelayUpdate
	Display/K=1 $wn_hist
	if(!paramisdefault(xlabel))
		label bottom xlabel 
	endif 
	if(!paramisdefault(x_vertline))
		getaxis/Q left 
		SetDrawEnv xcoord= bottom, ycoord= left;DelayUpdate
		DrawLine x_vertline, V_min, x_vertline, v_max
	endif 
end 	

