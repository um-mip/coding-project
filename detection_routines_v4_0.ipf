//20160229 restored derviative for use with wave navigator

// 20151007 deleted some old stuff see v3_4 for original

//20120829	creating an event database to better control adding and deleting events
//20120904 ohnoyoudidnt
// 20060829 Hurricane Ernesto, storing output from findlevels to allow adding missed events

#pragma rtGlobals=1		// Use modern global access method.

// standalone detect panel

macro detectPanelmac()
	standaloneDetection()
endmacro


function standaloneDetection()
// check if blast panel exists
	// prompt to close blast panel

string/g detectpanel="DetectPanel0"

SVAR/Z bpname = blastpanel
if( SVAR_Exists( bpname ) )
	print "PLEASE CLOSE BLASTPANEL"
	doWindow/F $bpname
	if(V_Flag == 1)
		abort
	else
		bpname = detectpanel 
	endif
else
	string/g bpname = detectpanel 
	string/g blastpanel = detectpanel
endif



//variable /g g_nav2event = 0
variable MAXABFFILES=256, MAXSWEEPS=999, MAXIMPORTWAVES=999
variable/g gpredsmooth=11,gdsmooth=1,gdthresh=10,gdmin_dur=0.5,gdmax_dur=5,gdpwin=2
variable/g gdetectsign=-1
variable/g gboffset=2,gbdur=5,gpthresh=5,gpsmooth=3
variable/g gathresh=1,gawin=10
variable/g gtdur=200,gtoffset=10
variable /g gavecut=1000
variable/g gxmin1=0,gxmax1=120, gymin1=0,gymax1=1
variable /g gxmin2=0,gxmax2=120, gymin2=0,gymax2=1
variable /g g_autopass=0

STRUCT		analysisParameters 		myParameterStruct
// default settings for detection
myParameterStruct.dPreDerivativeSmoothPoints	=		gpredsmooth
myParameterStruct.dThreshold_pA_ms			=		gdthresh
myParameterStruct.dMaxWidth_ms			=		gdmax_dur
myParameterStruct.dMinWidth_ms				=		gdmin_dur
myParameterStruct.dSmoothPoints				=		gdsmooth
myParameterStruct.peakWindowSearch_ms		=		gdpwin
myParameterStruct.peakSign					=		gdetectsign
myParameterStruct.peakThreshold_pA			=		gpthresh
myParameterStruct.peakSmoothPoints			=		gpsmooth
myParameterStruct.areaThreshold_pA_ms		=		gathresh
myParameterStruct.areaWindow_ms			=		gawin
myParameterStruct.baseOffset_ms				=		2
myParameterStruct.baseDuration_ms			=		5
myParameterStruct.traceDuration_ms			=		50
myParameterStruct.traceOffset_ms				=		10
myParameterStruct.averageCutoff_pA			=		0.1
myParameterStruct.scale1[0]					=		gxmin1
myParameterStruct.scale1[1]					=		gxmax1
myParameterStruct.scale1[2]					=		gymin1
myParameterStruct.scale1[3]					=		gymax1
myParameterStruct.scale2[0]					=		gxmin2
myParameterStruct.scale2[1]					=		gxmax2
myParameterStruct.scale2[2]					=		gymin2
myParameterStruct.scale2[3]					=		gymax2



	PauseUpdate; Silent 1		// building window...
	// 20170615 updates for igor7
	variable p_xpos = 640, p_ypos = 60, p_dx = 710, p_dy = 650
	NewPanel /K=1/W=(p_xpos,p_ypos,p_xpos+p_dx,p_ypos+p_dy)/N=$detectpanel
	SetWindow $detectpanel, hook(myhook)=detectWinHook

	variable col0start=10,col1start=25, col2start=50,col3start=330
	variable rowstart=50, rowinc=20,bw1=100,bw2=200

	make/T/O/N=(MAXIMPORTWAVES,1) ImportListWave
	make/O/B/U/N=(MAXIMPORTWAVES,1,2) ImportSelWave

	importselwave[][][1]=0
	importlistwave= ""

	ListBox importfileList, proc=ImportFilelistboxproc2,mode=4, pos={286,rowstart},size={225,325}, listwave=ImportListwave, selwave=importselwave
	ListBox importfilelist, userdata(listwn)="ImportListWave", userdata(selwn)="ImportSelWave"

	// string testud1 = getuserdata("", "importfilelist", "listwn")
	// string testud2 = getuserdata("", "importfilelist", "selwn")
	// print "userdata test:", testud1, testud2 

	TitleBox importTitle title="Imported waves", pos={286,rowstart-20},frame=0

	Button GUI,pos={366,1},size={bw1,rowinc},proc=InitializeGUIProc,title="GUI"

	// build wave importer interface, Wii
	make/N=(1)/O/T IWPlistw
	make/N=(1)/O IWPselw  
	SetDrawLayer UserBack
	DrawText 307,391,"wave template,  use * for wildcard"
	SetVariable setTemplate,pos={287,391},size={225.00,20.00},proc=setTemplateAction
	SetVariable setTemplate,fSize=14,value= _STR:"2*g1s*1"
	ListBox listWaves,pos={286,413},size={225,150},listWave=root:IWPlistw
	ListBox listWaves,selWave=root:IWPselw,mode= 4, proc=ImportLBproc
	ListBox listWaves, userdata(listwn)="IWPlistw", userdata(selwn)="IWPselw"

	// testud1 = getuserdata("", "listwaves", "listwn")
	// testud2 = getuserdata("", "listwaves", "selwn")
	// print "userdata test IWP:", testud1, testud2 

	Button buttUpdateDetect,pos={308,566},size={173.00,22.00},title="update detection list"
	Button buttUpdateDetect, proc=ButtUpdateDetectProc

	variable buttonrow=605
	variable  clearButtonX=col3start, clearButtonY=600,tcinc=32

	variable crap=5
// set up detection parameter handling
	SetVariable deriv_Presmooth, pos={col2start,rowstart},size={200,17},title="Deriv. Pre-smooth (points)"
	SetVariable deriv_Presmooth,limits={0,1000,1},value= gpredsmooth
	SetVariable deriv_thresh,pos={col2start,rowstart+rowinc},size={200,17},title="Deriv. Threshold (pA/ms)"
// deriv thresh label color
	SetVariable deriv_thresh,limits={-inf,inf,0.1},value= gdthresh, proc=testingsetvar,labelBack=(65535,0,0)
	
	SetVariable deriv_max_dur,pos={col2start,rowstart+rowinc*2},size={200,17},title="Deriv. Max. Width (ms)"
	SetVariable deriv_max_dur,limits={0,1000,0.2},value= gdmax_dur	
	SetVariable deriv_min_dur,pos={col2start,rowstart+rowinc*3},size={200,17},title="Deriv. Min. Width (ms)"
	SetVariable deriv_min_dur,limits={0,1000,0.2},value= gdmin_dur
	SetVariable deriv_smooth,pos={col2start,rowstart+rowinc*4},size={200,17},title="Deriv. Smoothing (points) "
	SetVariable deriv_smooth,limits={0,1000,1},value= gdsmooth
	SetVariable dp_window,pos={col2start,rowstart+rowinc*5},size={200,17},title="Window search (ms)"
	SetVariable dp_window,limits={0,inf,1},value= gdpwin
	
	SetVariable peak_sign,pos={col2start,rowstart+rowinc*6},size={200,17},title="Peak sign"
	SetVariable peak_sign,limits={-1,1,1},value= gdetectsign
	SetVariable peak_thresh,pos={col2start,rowstart+rowinc*7},size={200,17},title="Peak threshold (pA)"
// peak thresh label color	
	SetVariable peak_thresh,limits={0,inf,1},value= gpthresh, proc=testingsetvar,labelBack=(3,52428,1)
	
	SetVariable peak_smth_pts,pos={col2start,rowstart+rowinc*8},size={200,17},title="Peak Smoothing (points)"
	SetVariable peak_smth_pts,limits={0,1000,1},value= gpsmooth
	SetVariable peak_a_thresh,pos={col2start,rowstart+rowinc*9},size={200,17},title="Area threshold (pA*ms)"
	SetVariable peak_a_thresh,limits={0,inf,1},value= gathresh
	SetVariable peak_a_win,pos={col2start,rowstart+rowinc*10},size={200,17},title="Area Window (ms)"
	SetVariable peak_a_win limits={0,inf,1},value= gawin
	SetVariable peak_a_win title="Area Window (ms)"
		
	SetVariable base_offset,pos={col2start,rowstart+rowinc*11},size={175,17},title="Baseline offset (ms)"
// baseline offset label color	
	SetVariable base_offset,limits={0,inf,1},value= gboffset,labelback=(16385,16388,65535)
	
	SetVariable base_dur,pos={col2start,rowstart+rowinc*12},size={175,17},title="Baseline duration (ms)"
	SetVariable base_dur,limits={0,inf,1},value= gbdur //,labelback=(0,0,65535)
	SetVariable trace_dur,pos={col2start,rowstart+rowinc*13},size={175,17},title="Trace duration (ms)"
	SetVariable trace_dur,limits={0,inf,10},value= gtdur
	SetVariable trace_base_dur,pos={col2start,rowstart+rowinc*14},size={175,17},title="Trace offset (ms)"
	SetVariable trace_base_dur,limits={0,inf,10},value= gtoffset
	SetVariable trace_ave_cutoff,pos={col2start,rowstart+rowinc*15},size={175,17},title="Ave cutoff (pA)"
	SetVariable trace_ave_cutoff,limits={0,inf,1},value= gavecut
	
	Button detect,pos={col2start,rowstart+rowinc*16},size={bw2,20},proc=DetectButtonProc,title="detect"
//	Button makeWave,pos={col2start,rowstart+rowinc*17},size={90,20},proc=makeWaveButtonProc,title="make wave"
	CheckBox autoMan,pos={col2start,rowstart+rowinc*18},size={117,20},title="Auto Detection?",value=1
	CheckBox displayPlots,pos={col2start,rowstart+rowinc*19},size={117,20},title="Display Plots?",value=0
	CheckBox saveWaves,pos={col2start,rowstart+rowinc*20},size={117,20},title="Save Waves?",value=0

	Button alignPlots, pos={col2start, rowstart+rowinc*24},size={bw2,20}, proc=AlignPlotsProc,title="align"
//	Button RedrawPlots, pos={col2start, rowstart+rowinc*22},size={90,20}, proc=RedrawPlotsProc,title="redraw"
//	Button BringGUI, pos={col2start, rowstart+rowinc*23},size={90,20}, proc=BringGUIProc,title="bring GUI"
	Button ImportEventSettings, pos={col2start, rowstart+rowinc*25},size={bw2,20}, proc=ImportEventSettingsProc,title="import settings"		
	Button ExportEventSettings, pos={col2start, rowstart+rowinc*26},size={bw2,20}, proc=ExportEventProc,title="export settings"


// set up interface for display of results

	make/o/t/n=(20,1) resultswave 
	make/U/O/N=(20,1,2) resultsSelWave

	make/o/t/n=(10,1) plottypeswave 
	make/U/O/N=(10,1,2) plottypesSelWave

	resultswave[0]="absolute peak"
	resultswave[1]="relative peak"
	resultswave[2]="peak time"
	resultswave[3]="derivative"
	resultswave[4]="derivative time"
	resultswave[5]="interval"
	resultswave[6]="baseline"
	resultswave[7]="baseline time"
	resultswave[8]="fwhm"
	resultswave[9]="10to90decay"
	resultswave[10]="tau"
	resultswave[11]="risetime"
	resultswave[12]="area"
	resultswave[13]="events"
	resultswave[14]="event average"
	resultswave[15]="normalized average"

	plottypeswave[0]="xy"
	plottypeswave[1]="prob dist"
	plottypeswave[2]="histogram"
	plottypeswave[3]="wave intrinsic"
	plottypeswave[4]="time course"
	plottypeswave[5]="CEREBRO"
	plottypeswave[6]="KS"
	plottypeswave[7]="summary table"
	plottypeswave[8]="result(s) table(s)"

	resultsselwave=0
	plottypesselwave=0
	
	ListBox resultsList, proc=resultsListProc,mode=4, pos={col3start+200,rowstart},size={175,325}, listwave=resultswave, selwave=resultsselwave
	TitleBox resultstitle title="analysis for display", pos={col3start+200,rowstart-20},frame=0

	ListBox plottypesList, proc=plottypesListProc,mode=4, pos={col3start+200,rowstart+2*rowinc+320},size={175,150}, listwave=plottypeswave, selwave=plottypesselwave
	TitleBox plottypestitle title="plot types", pos={col3start+200,rowstart+2*rowinc-20+320},frame=0
	
	button plot, pos={567,568}, size={bw1,20}, proc=plotButtonProc, title="Plot!"

//v3 20060413 add free window buttons and other controls to top of blastpanel
variable buttonStart=475,buttondx=22,setdx=15,sizex=20,sizey=20
button analysisWin1butt, pos={buttonstart,1}, size={20,20}, proc=analWin,title="1",fcolor=(48896,59904,65280)
checkbox activeAnalysisWin1, disable=0,pos={buttonstart+buttondx,1},size={10,10},value=0,mode=0,proc=checkprocAAwin,title=""
button analysisWin2butt, pos={buttonstart+buttondx+setdx,1}, size={20,20}, proc=analWin,title="2",fcolor=(65280,65280,48896)
checkbox activeAnalysisWin2, disable=0,pos={buttonstart+2*buttondx+setdx,1},size={10,10},value=0,mode=0,proc=checkprocAAwin,title=""

variable rangestart=550,rangedx=60
SetVariable scale1xmin, pos={rangestart,5},size={60,17},title="xmin",limits={0,10000,1},value= gxmin1
SetVariable scale1xmax, pos={rangestart+rangedx,5},size={60,17},title="xmax",limits={0,10000,1},value= gxmax1
button getrange, pos={rangestart+2*rangedx+1,1}, size={30,20}, proc=getrangeproc,title="Get"


end

Function detectWinHook(s)
	STRUCT WMWinHookStruct &s

	Variable hookResult = 0
	string message=""

	switch(s.eventCode)
		case 0:				// Activate
			// Handle activate
			break

		case 1:				// Deactivate
			// Handle deactivate
			break
		case 11:				// keyboard event
			// Handle keystroke
			//print "detectwinhook: keystroke:", s.keytext, "; code:", s.keycode
			//print "kw", kwSelectedControl
			string S_DataFolder = ""
			controlinfo kwSelectedControl
			//print "detect window hook: sel control, flag: ", S_value, V_flag
			string lbn = S_Value, lwn="", swn=""
			lwn = getuserdata("", lbn, "listwn")
			swn = getuserdata("", lbn, "selwn")
			//print "listw, selw", lwn, swn 
			//print "listbox name", lbn 
			if(s.keycode==32) // 32 is ascii for "space", only key detected!
				
				//WAVE/Z/T lw = $lwn 
				WAVE/Z sw = $swn 
				sw = 1
				listbox $lbn, selwave=sw 
			endif
			hookResult = 1	// We handled keystroke
			break
		// And so on . . .
	endswitch

	return hookResult		// 0 if nothing done, else 1
End

//////////////////////////////////////////////////////////////////////////////////////////////
//
//FUNCTION 		TEST DERIVATIVE
//
//	RETURNS NAME OF WAVE CONTAINING DERIVATIVE 
////////////////////////////////////////////////////////////////////////////////////////////

function/S testDerivative(wavelet)
string wavelet
PauseUpdate; Silent 1		// building window...
struct analysisparameters ps

variable worked = readpanelparams2(ps)
//print "inside test derivative/r"
//print "parameter string ",worked,ps

variable dpresmooth =  ps.dprederivativesmoothpoints
variable dsmooth = ps.dsmoothpoints
variable usetb = ps.usetb

string d_wavelet = "d"+wavelet 		//stores name of derivative wave
string timebase = wavelet+"_tb" 
duplicate /O $(wavelet), deriv
if(exists(wavelet))
	WAVE temp=$wavelet
else
	print "can't find wave: ",wavelet
endif

if(usetb==0)
	smooth /B dpresmooth, deriv
	differentiate deriv
	smooth /B dsmooth, deriv
else
	if(exists(timebase))
		WAVE temp_tb = $timebase
		print "box car smoothing doesn't work with gapped data, using default smooth"
		smooth /e=2 dpresmooth, deriv
		differentiate deriv /X=temp_tb
		smooth /e=2 dsmooth, deriv
//		print "wavestats for deriv:"
		wavestats/Z/Q deriv
		if(V_numINFs>0)
			print "trying to fix ET infinities!!"
			variable i=0
			do
				if((deriv[i]==inf)||(deriv[i]==-inf))
					deriv[i]=0
				endif
				i+=1
			while(i<numpnts(deriv))
		endif
//		doupdate
	else
		print "failed to locate timebase:", wavelet,timebase
		abort
	endif
endif
SetScale d 0,0,"A/sec", deriv

duplicate/O deriv, $(d_wavelet)
return d_wavelet
end

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////
//////			detect from derivative
////// AGG replaced function by this name on 2023-12-22
////// Addresses some potential errors in event feature detection
////// and makes consistent the process by which events are analyzed
////// between detection and adding events
//////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

function derivDetectP2_20231222(pwavelet)
string pwavelet

variable/g globaltakeit
variable /g g_progress=0
variable abort_event=0, localderivpeak=0

string wavelet=""
wavelet = removequotes( pwavelet )
wavelet = pwavelet

struct analysisparameters ps

variable worked = readpanelparams2(ps)

// units from parameter window are pA/msec/mV
// units from data files are A, V, sec--convert params to be convenient for user!!!
// units now converted in readpanelparams2!!!!

// derivative params
variable dpresmooth = ps.dPreDerivativeSmoothPoints
variable dsmooth = ps.dSmoothPoints
variable dthresh = ps.dThreshold_pA_ms	
variable min_dur = ps.dMinWidth_ms		
variable max_dur = ps.dMaxWidth_ms		
variable chunk = ps.peakWindowSearch_ms	

// baseline params
variable base_offset = ps.baseOffset_ms	
variable base_dur = ps.baseDuration_ms	

// peak parameters
variable thissign = ps.peaksign					
variable thresh = ps.peakThreshold_pA		
variable peak_smooth = ps.peakSmoothPoints			
variable area_thresh = ps.areaThreshold_pA_ms
variable area_win = ps.areaWindow_ms		

// output trace parameters
variable trace_dur = ps.traceDuration_ms	
variable trace_offset = ps.traceOffset_ms	
variable avecutoff =  ps.averageCutoff_pA		

//control parameters
variable automan =  ps.automan
variable displayplots =  ps.displayplots
variable savewaves =  ps.savewaves
variable useTB = ps.useTB
//string 	extlist = ps.extlist

variable post_peak=0, this_area,accept,area_time=0

variable time0,dx,t0,data_points,t_end,dt=0,pretime,maxtime
variable peak,peak_time,dpeak,dpeak_time,baseline,base_start,base_end

variable nevents=0,ipeak=0,ievent=0,npeaks=0,iavetrace=0, bad_int=0
variable pstart,pend,t50rise,p50,delta_levels=0

variable d_baseline
variable missedT50=0

// output waves
string peaks="p_"+wavelet,peaks2="pmb_"+wavelet,peaks_tb="ptb_"+wavelet,levels="levels_"+wavelet
string base="b_"+wavelet,base_tb="btb_"+wavelet,pderiv="d_"+wavelet,pderiv_tb="dtb_"+wavelet
string area="a_"+wavelet, interval="i_"+wavelet,avetrace="ave_"+wavelet

string fwhm="fwhm_"+wavelet,decaytime="dtime_"+wavelet

string trace="",prevtrace="",mymessages="",label=""

print wavelet,peaks,levels,peaks_Tb

dx=dimDelta($(wavelet),0)
maxtime=dx*dimSize($(wavelet),0)

duplicate /O $(wavelet), deriv
if( dpresmooth > 0 )
	smooth /B dpresmooth, deriv
endif

differentiate deriv
SetScale d 0,0,"A/sec", deriv
if( dsmooth > 0 )
	smooth /B dsmooth, deriv
endif

dthresh*=thissign
//print "max, min duration: ",max_dur,min_dur
//findlevels /M=(max_dur)/T=(min_dur) deriv, dthresh
make /O w_levels
findlevels /Q/D=w_levels deriv, dthresh
nevents = V_LevelsFound

if (nevents>50000) 
	mymessages="Lots of events!  N="+num2str(nevents)+"  Continue? 1 is yes, 0 is no"
	accept=acceptReject(mymessages)
	if(accept==0)
		print "User abort"
		abort
	endif
endif
variable firstevent=1
ievent=0
time0=w_levels[ievent]

//create waves to hold the data
// AGG - 2022-12-06, made all of these waves double precision
make/D /o/N=(nevents) wpderiv
make/D /o/N=(nevents) wpderiv_tb
make/D /O/N=(nevents) wpeaks
make/D /O/N=(nevents) wpeaks2
// make /O/D/N=(nevents) wpeaks_tb // AGG 2022-10-31: testing double precision ptb
make/D /O/N=(nevents) wpeaks_tb 
make/D/o/N=(nevents)  wbase
make/D/o/N=(nevents)  wbase_tb
make/D/o/N=(nevents)  warea
make/D/o/N=(nevents)  winterval
make/D/o/N=(nevents)  wfwhm
make/D/o/N=(nevents)  wdecaytime
make/D/o/N=(nevents) wrisetime
make/D/O/N=(nevents) w_avelist
//initialize the waves
wpderiv=0
wpderiv_tb=0
wpeaks=0
wpeaks2=0
wpeaks_tb=0
wbase=0
wbase_tb=0
warea=0
winterval=0
wfwhm=0
wdecaytime=0
wrisetime=0
w_avelist=0

ipeak=0
ievent=0
pretime=0
bad_int=0

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//print "running"
//start searching for events using the threshold crossings in the derivative
variable firstpeak=0, t50rise2=nan

// here is the big loop over all the threshold crossings
do

	abort_event=0

	// verify we are on the "rising" phase of derivative threshold crossing (corrected for sign of course).

	wavestats /Q/R=(time0-min_dur,time0+min_dur) deriv

	if(thissign<0)
		localderivpeak = V_minloc
	else
		localderivpeak = V_maxloc
	endif

	//print "time0 outside of ifthen:",time0
	if(localDerivPeak<=time0)
	//	print "DANGER WILL ROBINSON! DANGER!  aborting event", time0,localderivpeak,ievent
	//	abort_event = 1
	else
	
//	if(time0<(maxtime-chunk-peak_smooth)) OLD CODE, MODIFIED 20170627 !!! UNF*CKING BELIEVABLE

// chunk is peak window search!

	if( time0 < ( maxtime - chunk - dx * peak_smooth ) )
	//print "time0 inside of ifthen:",time0

	// get peak of derivative
		duplicate /o/R=(time0,time0+chunk) deriv,dwave_chunk

	//this may be an error in the original code!!! deriv is already smoothed!!!
	//		smooth /B dsmooth, dwave_chunk

		wavestats /Q dwave_chunk
		if (thissign<0) 
			dpeak=V_min
			dpeak_time=V_minloc
		else
//			print "using v_max"
			dpeak=V_max
			dpeak_time=V_maxloc
		endif

		// modification of the original code, now searching from the time of the peak of the derivative,
		// rather than the threshold crossing.  This should tighten burst mode searching.
		time0=dpeak_time

		// locate peak in raw data
		// get peak of event looking forward in time from when derivative crossed threshold.
		
		duplicate /o/R=(time0,time0+chunk) $(wavelet),wave_chunk

		//another potential error in the original code??  wavestats run before smoothing?
		//		wavestats /Q wave_chunk
		//		smooth /B peak_smooth, wave_chunk

		smooth /B peak_smooth, wave_chunk
		wavestats /Q wave_chunk

		if (thissign<0) 
			peak=V_min
			peak_time=V_minloc
		else
//			print "using v_max"
			peak=V_max
			peak_time=V_maxloc
		endif
		
		// baseline
		// original code offset baseline from time of peak
		//		base_start=peak_time-base_dur-base_offset
		//		base_end=peak_time-base_offset
		base_start=time0-base_dur-base_offset
		base_end=time0-base_offset

		//print base_start		
		duplicate /o/R=(base_start,base_end) $(wavelet), wave_base
		wavestats /Q wave_base
		baseline = V_avg

		// show what's going on!!!
		duplicate /O/R=(peak_time-trace_offset, peak_time+trace_dur-trace_offset) $(wavelet), dispthis
		duplicate /O dispthis, deriv_dispthis

		if( peak_smooth > 0 )
			smooth /b peak_smooth, dispthis
		endif
		if( dpresmooth > 0 )
			smooth /B dpresmooth, deriv_dispthis
		endif

		differentiate deriv_dispthis
		SetScale d 0,0,"A/sec", deriv_dispthis
		if( dsmooth > 0 )
			smooth /B dsmooth, deriv_dispthis
		endif 
		
		duplicate /O dispthis, gui_baseline
		gui_Baseline = baseline
		duplicate /O dispthis, gui_thresh
		gui_thresh = thissign*thresh+baseline

		if(firstevent==1)
			make/O/N=1 showpeak
			make/O/N=1 showpeakD
			make/O/N=1 showpeaktime
			make/O/N=1 showpeakDtime

			setactiveanalysiswindowselect(1,1)  //set the first analysis window and clear
			appendtograph dispthis
			setaxis /A bottom
			modifygraph rgb=(0,0,0)
			appendtograph gui_baseline, gui_thresh
			appendtograph showpeak vs showpeaktime
			ModifyGraph mode(showpeak)=3,marker(showpeak)=19
		endif

		showpeak[0] = peak
		showpeaktime[0]= peak_time
		showpeakd[0] = dpeak
		showpeakdtime[0] = dpeak_time

		duplicate /O dispthis, gui_dthresh
		gui_dthresh = dthresh
	
		if(firstevent==1)	
			setactiveanalysiswindowselect(2,1)
			appendtograph deriv_dispthis
			modifygraph rgb=(0,0,0)
			appendtograph gui_dthresh
			appendtograph showpeakd vs showpeakdtime
			ModifyGraph mode(showpeakd)=3,marker(showpeakd)=19
			firstevent=0
		endif

//doupdate

		if(((thissign*(peak-baseline))>thresh)%&((thissign*peak)>(thissign*baseline)))	
			
//			get t50 rise time to align events
				p50 = 0.5*(peak-baseline)+baseline
				findlevel /Q/R=(peak_time,peak_time-2*chunk) $(wavelet), p50
// original code:
//				findlevel /Q/R=(peak_time,peak_time-chunk) $(wavelet), p50

				if(v_flag==0)
					t50rise = V_levelX
					duplicate /o/R=(t50rise-trace_offset,t50rise+trace_dur) $(wavelet),newtrace
					t50rise = peak_time - t50rise
					missedT50=0
				else
					print "Missed level crossing, using peak to align event instead of t50", peak_time, chunk, peak*10^12, p50*10^12, (peak-baseline)*10^12, ipeak,ievent,peak_time
					duplicate /o/R=(peak_time-trace_offset,peak_time+trace_dur) $(wavelet),newtrace
					missedT50=1 /// sets the flag so this will not be added to average //must confirm!
					t50rise=NaN
				endif			
											//dx is set near the top from raw trace
			setScale /P x,(-trace_offset),dx,newtrace
			newtrace -= baseline
			duplicate /o newtrace, smoothnewtrace
			smooth /b peak_smooth, smoothnewtrace
			
			//modified 20130717 now using FWHM for area instead of area_win parameter
			area_time=returnFWHM("smoothnewtrace",thissign)
			this_area = area(newtrace,(0),(area_time))

//			ModifyGraph rgb=(0,0,0)			
//			doupdate
// this is the original code, it fails when a large positive area due to an artifact
//   is detected when looking for negative events
//			if(thissign*(this_area)>area_thresh)
//
			accept = 0
			if((thissign*(this_area)>area_thresh)&&(sign(this_area)==thissign))
		//	print "!!!!! ", thissign, sign(this_area)
				if((this_area>0)&&(area_thresh!=0))  
//					print "A:  area issues!",this_area
				endif
				accept=1
				if(automan == 0) 
					print "MANUAL DETECTION"
					prevtrace=trace
					print ipeak," Area ",area_thresh*1e15,this_area*1e15," peak ",(peak-baseline)*1e12," baseline ",baseline*1e12
					accept = acceptReject("Accept?")			
				endif
			else
				if(area_thresh==0)
					accept=1
					if(automan == 0) 
						print "MANUAL DETECTION"
						prevtrace=trace
						print ipeak," Area ",area_thresh*1e15,this_area*1e15," peak ",(peak-baseline)*1e12," baseline ",baseline*1e12
						accept = acceptReject("Accept?")			
					endif
				else
//					print "More area issues."
				endif					
			endif		
			if (accept==1)				
//				if(saveWaves==1)
//					trace="e_"+wavelet+"_"+num2str(ipeak)
//					duplicate /o oldtrace,$(trace)
//				endif
				if((this_area>0)&&(area_thresh!=0))
//					print "B: area issues!",this_area
				endif				
				wpderiv[ipeak]=dpeak
				wpderiv_tb[ipeak]=dpeak_time
				wpeaks[ipeak]=peak -baseline
				wpeaks2[ipeak]=peak//-baseline
				wpeaks_tb[ipeak]=peak_time
				wbase[ipeak]=baseline
				wbase_tb[ipeak]=base_end
// revised central t50rise calculator
// suppressing this 20211207, uses fixed window
				t50rise2 = returnT50rise("newtrace",thissign, trace_offset)
				if( abs(t50rise-t50rise2) > 0.1*t50rise )
					print "in detection, t50rise discrepancy:", t50rise, t50rise2
				endif
				if((t50rise>0)&&(missedT50<=0))
					wrisetime[ipeak]=t50rise
				else
					wrisetime[ipeak]=nan
				endif
				
// original code used the trace duration to calculate area for output				
//				this_area = area(newtrace,0,trace_dur)

// modified 2013-07-17, now using 1090decay to measure area
				wfwhm[ipeak]=returnFWHM("smoothnewtrace",thissign)
				wdecaytime[ipeak]=return1090decay("smoothnewtrace",thissign)
//				area_time=wdecaytime[ipeak]
				area_time=wfwhm[ipeak]
				if(numtype(area_time)==0)
					this_area = area(newtrace,0,area_time)
//					this_area = area(newtrace,0,area_win)
					if( (thissign*this_area) < 0 )
//						print "The area has an opposite sign compared to the peak: ",thissign
//						print "area ",this_area
						warea[ipeak]=nan
					else
						warea[ipeak]=this_area
					endif
				else
//					print "no area measurement,", area_time,ipeak
					warea[ipeak]=nan
				endif			

				
// 20130717 moved before area to use these values as the length of the tail for area
//				wfwhm[ipeak]=returnFWHM("smoothnewtrace",thissign)
//				wdecaytime[ipeak]=return1090decay("smoothnewtrace",thissign)

				if(ipeak==0)
					winterval[ipeak]=0
					duplicate/O newtrace,oldtrace
					//duplicate /O oldtrace,$(avetrace)
					//$(avetrace)=0.0		
					iavetrace=0
				else
					winterval[ipeak]=dpeak_time-pretime
					if((winterval[ipeak]>trace_dur)&&(missedT50==0))
						//print peak-baseline, avecutoff
//						print "winterval[ipeak], winterval[ipeak-1]",winterval[ipeak], winterval[ipeak-1]
						if((winterval[ipeak-1]>trace_dur)%&(oldtrace[0]!=0)) // bitwise AND
							w_avelist[ipeak-1]=1
							if(iavetrace==0)
								duplicate /O oldtrace,wavetrace
							else
								wavetrace+=oldtrace
							endif
//							doupdate
//original code allowed saving both all events and just averaged events
//							if(saveWaves==1)
//								trace="e_"+wavelet+"_"+num2str(ipeak)
//								duplicate /o oldtrace,$(trace)
//							endif

//							print "accepted intervals: ",winterval[ipeak],winterval[ipeak-1]," event ",(ipeak-1)
							iavetrace+=1
						endif
						if ((thissign*(peak-baseline))<(avecutoff))
							duplicate /O newtrace,oldtrace
							//print "accepting for average"
						else
							oldtrace=0.0
						endif
					endif
					
				endif
				pretime = dpeak_time
				
				if((winterval[ipeak]>chunk)%|(ipeak==0)) // bitwise OR
				//	print peak-baseline,winterval[ipeak],winterval[ipeak-1]
					ipeak+=1
				else
					//print "bad interval, discarding"
					bad_int+=1
				endif
			endif
		endif
		endif //maxtime edge
	endif  // endif statement regarding location of derivative max to avoid double event detection, or falling phase.
	ievent+=1
//	delta_levels=w_levels[ievent]-w_levels[ievent-1]
//	do
//		print "delta_levels is too small: ",delta_levels,ievent
//		ievent+=1
//		delta_levels=w_levels[ievent]-w_levels[ievent-1]
//	while ((delta_levels<chunk)&&(ievent<nevents))
	time0=w_levels[ievent]
//	print time0
while((ievent<=nevents)&&((time0+chunk)<maxtime))

if(iavetrace!=0)
	wavetrace/=iavetrace
else
//	wavetrace=0
	print "Failed to identify any events that meet averaging criteria."
endif

npeaks=ipeak
print "npeaks nevents,averaged traces", npeaks,nevents,iavetrace
print "bad intervals: ", bad_int
deletepoints (npeaks),(nevents-npeaks),wpderiv
deletepoints (npeaks),(nevents-npeaks),wpderiv_tb
deletepoints (npeaks),(nevents-npeaks),wpeaks
deletepoints (npeaks),(nevents-npeaks),warea
deletepoints (npeaks),(nevents-npeaks),wpeaks2
deletepoints (npeaks),(nevents-npeaks),wpeaks_tb
deletepoints (npeaks),(nevents-npeaks),wbase
deletepoints (npeaks),(nevents-npeaks),wbase_tb
deletepoints (npeaks),(nevents-npeaks),winterval
deletepoints (npeaks),(nevents-npeaks),wfwhm
deletepoints (npeaks),(nevents-npeaks),wdecaytime
deletepoints (npeaks),(nevents-npeaks), wrisetime
deletepoints (npeaks),(nevents-npeaks), w_avelist
	
if(displayplots==1)
	if (iavetrace!=0)
		display wavetrace
	endif
	display deriv
	appendtograph wpderiv vs wpderiv_tb
	ModifyGraph mode(wpderiv)=3,marker(wpderiv)=19,rgb(wpderiv)=(0,0,65000)
	display $(wavelet)
	AppendToGraph wpeaks2 vs wpeaks_tb
	ModifyGraph mode(wpeaks2)=3,marker(wpeaks2)=19,rgb(wpeaks2)=(0,0,65000)
endif

// rename waves to reflect source data
if(iavetrace!=0)
	string renamed=wavelet+"_ave"
	duplicate/O wavetrace,$renamed
	wavestats/Q wavetrace

	if(thissign==-1)
		wavetrace/=-V_min		//assumes negative going peak!!!
	else
		wavetrace/=V_max
	endif
	renamed=wavelet+"_nave"
	duplicate/O wavetrace,$renamed
	setscale d,0,1,"",$renamed
endif

string info=waveinfo($wavelet,0),timeunits="",signalunits="",myunits=""
timeunits = stringbykey("XUNITS",info)
signalunits = stringbykey("DUNITS",info)

renamed=wavelet+"_lev"
duplicate/O w_levels,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed

renamed=wavelet+returnext("ave list")
duplicate/O w_avelist,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed

renamed=wavelet+"_der"
duplicate/O wpderiv,$renamed
myunits = signalunits+"/"+timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,thissign)

renamed=wavelet+"_pks"
duplicate/O wpeaks,$renamed
myunits = signalunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,thissign)

renamed=wavelet+"_int"
duplicate/O winterval,$renamed
myunits =timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,1)

renamed=wavelet+"_pk2"
duplicate/O wpeaks2,$renamed
myunits = signalunits
setScale d,0,1,myunits,$renamed

renamed=wavelet+"_dtb"
duplicate/O wpderiv_tb,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed

renamed=wavelet+"_ptb"
duplicate/O wpeaks_tb,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed

renamed=wavelet+"_t50r"
duplicate/O wrisetime,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,1)

renamed=wavelet+"_fwhm"
duplicate/O wfwhm,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,1)

renamed=wavelet+"_1090d"
duplicate/O wdecaytime,$renamed
myunits = timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,1)

renamed=wavelet+"_area"
duplicate/O warea,$renamed
myunits = signalunits+"*"+timeunits
setScale d,0,1,myunits,$renamed
probdistp(renamed,thissign)

wavestats /Z/Q winterval
print "Mean interval = ",V_avg
print "Number of events = ",V_npnts

// 20160229 need -deriv for wave intrinsic wave navigator
renamed=wavelet+"-deriv"
duplicate /O deriv, $renamed

// 20150824 stop saving the derivative!!!
killwaves /Z deriv

//renamed="d"+wavelet
//duplicate /O deriv, $renamed

updatedetectsummary() // creates and updates the summary waves
end



//////////////////////////////////////////////////////////////////////////////
//        Probability Distribution
//////////////////////////////////////////////////////////////////////////////
// modified to eliminate Nans! 20081208
//
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-12-19
/// NOTES: Fixed NaN handling. Now the V_npnts in Igor already excludes the # of NaNs
/// so this function was removing twice the number of NaNs, and eliminating real data
/// from the distribution wave

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-08
/// NOTES: Added optional wave parameter. If provided, the provided wave will
/// be sorted and updated in the background, instead of making a copy of mywavename

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-09
/// NOTES: Add a wave note with the sort direction for plotting

//// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-01-15
/// NOTES: Changed the scaling so that the first event in dist starts at dx,
/// and the last event occurs at 1
function probdistP(string mywavename, variable thissign[, wave distWave])
	string dist=mywavename+"_dist"
	variable nevents,dx,zero, delstart=0, delpoints=0,wstat_npnts=0,npnts=0,numNaNs=0
	variable dbg = 0

	if(thissign==0)
		print "probDistP: sign cannot equal 0. Must be negative or positive"
		return 0
	endif

	variable dupMyWaveN = 0

	if(paramIsDefault(distWave) || !WaveExists(distWave))
		dupMyWaveN = 1
	endif

	if(dupMyWaveN)
		if(!WaveExists($(mywavename)))
			print "probDistP: couldn't find mywavename in current data folder. Stopping func"
			return 0			
		endif
		
		duplicate /o $(mywavename),$(dist)
		Wave distWave = $(dist)
	else
		dist = nameOfWave(distWave)
	endif
	
	
	npnts=numpnts(distWave)
	if( dbg == 1 )
		print "in probdistP: wn", mywavename, "numpnts:", npnts
	endif
	//count the events
	if(npnts<=0)
		// no points to sort
		return 0
	endif

	wavestats/Q distWave
	numNaNs=V_numNans
	nevents=V_npnts // AGG 2023-12-19: V_npnts already excludes NaNs, don't need to subtract the NaNs
	
	if (thissign>0)
		sort distWave,distWave
		delstart=nevents
	else
		sort/R distWave,distWave
		delstart=0
	endif

	deletepoints delstart, numNaNs, distWave // startElement, numElements, waveName
	
	//count the events
	wavestats/Z/Q distWave
	nevents=V_npnts // see above for note on NaNs

	if(V_numNans > 0)
		print "probDistP: Something went wrong!! There are still NaNs in the distribution wave", dist
	endif

	dx=1/nevents
	//	print "nevents in probdist: ",delstart,nevents,dx, delpoints,dist
	
	//normalize x-scaling of dist
	// AGG 2024-01-15: This makes it so that the rightx (point *after the last element in the dimension*) is 1
	// The first event has an x value of 0 still
	// setscale /P x,0,dx, distWave 
	setscale /P x,dx,dx, distWave // change it so that the first event starts at the dx, should end at 1

	string noteStr = "sortDir:" + num2str(thisSign) + ";"
	Note/NOCR distWave, noteStr
end




///////
//		returnFWHM:  returns the full width at half maximum given:
//			wavename,peak
//
//			assumes the baseline has already been corrected
////////
function returnFWHM_AGG_troubleshoot(wavelet,thissign)
string wavelet; variable thissign
variable peak,peaktime,range=0.002

// display/K=1/W=(600, 0, 1200, 300)/N=FWHM $wavelet
// modifyGraph rgb = (0,0,0)

//wavestats /Q $(wavelet)
//updated 20130717
wavestats /Q/R=(0,range) $(wavelet)
if (thissign<0)
	peak=V_min
	peaktime=V_minloc
else
	peak=V_max
	peaktime=V_maxloc
endif

variable start_time,end_time,halfmax,FWHM
variable rise50,fall50

start_time=pnt2x($(wavelet),0)			//gets the end of the wave
end_time=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

// SetAxis bottom, start_time, 0.01 // hard-set to match the trace offset
// SetAxis/A=2 left

halfmax=0.5*peak

findlevel /Q/R=(peaktime, start_time) $(wavelet),halfmax
rise50=V_levelX
findlevel /Q/R=(peaktime,end_time) $(wavelet),halfmax
fall50=V_levelX

FWHM=fall50-rise50
if(fall50==end_time)
	FWHM=nan
endif
if(rise50==start_time)
	FWHM=nan
endif

//print halfmax,rise50,fall50,FWHM


// make/O/N=1 showT50rise_returnFWHM, showT50rise_time_returnFWHM, showT50fall_returnFWHM, showT50fall_time_returnFWHM, showpeak_returnFWHM, showpeaktime_returnFWHM
// showT50rise_returnFWHM[0] = halfmax
// showT50rise_time_returnFWHM[0] = rise50
// showT50fall_returnFWHM[0] = halfmax
// showT50fall_time_returnFWHM[0] = fall50
// showpeak_returnFWHM[0] = peak
// showpeaktime_returnFWHM[0] = peaktime

// appendtograph showpeak_returnFWHM vs showpeaktime_returnFWHM
// ModifyGraph mode(showpeak_returnFWHM)=3,marker(showpeak_returnFWHM)=19
// appendtograph showT50rise_returnFWHM vs showT50rise_time_returnFWHM
// modifygraph mode(showT50rise_returnFWHM)=3, marker(showT50rise_returnFWHM)=19
// modifygraph rgb(showT50rise_returnFWHM)=(26411,1,52428)
// appendtograph showT50fall_returnFWHM vs showT50fall_time_returnFWHM
// modifygraph mode(showT50fall_returnFWHM)=3, marker(showT50fall_returnFWHM)=19
// modifygraph rgb(showT50fall_returnFWHM)=(26411,1,52428)

// DrawAction getgroup= peakSearchLine, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = peakSearchLine
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 0, 65535) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline range, 0, range, 1
// setdrawenv gstop
// DrawAction endinsert

// DrawAction getgroup= startSearchLine, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = startSearchLine
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 0, 65535) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline 0, 0, 0, 1
// setdrawenv gstop
// DrawAction endinsert

// DrawAction getgroup= t50align, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = t50align
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 65535, 0) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline 0, .25, 0, .75
// setdrawenv gstop
// DrawAction endinsert

return FWHM
end

///////
//		return1090decay:  returns the decay time  given:
//			wavename, peak
//
//			assumes the baseline has already been corrected
// 20130717 modified peak search to stick to first peak at 0
////////
function return1090Decay_AGG_troubleshoot(wavelet,thissign)
string wavelet; variable thissign
variable peak,peaktime,range=0.002

// display/K=1/W=(600, 375, 1200, 675)/N=Decay1090 $wavelet
// modifyGraph rgb = (0,0,0)

//wavestats /Q $(wavelet)
//updated 20130717
wavestats /Q/R=(0,range) $(wavelet)
if (thissign<0)
	peak=V_min
	peaktime=V_minloc
else
	peak=V_max
	peaktime=V_maxloc
endif

variable start_time,end_time,max90,max10,decay1090
variable fall90,fall10

start_time=pnt2x($(wavelet),0)			//gets the end of the wave
end_time=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

// SetAxis bottom, start_time, 0.01 // hard-set to match the trace offset
// SetAxis/A=2 left

max90=0.9*peak
max10=0.1*peak

findlevel /Q/R=(peaktime,end_time) $(wavelet),max90
IF(V_flag==0)
	fall90=V_levelX
else
	fall90=nan
	print "10-90 FAILURE fall90: ",peaktime, end_time, peak, max90
endif

findlevel /Q/R=(peaktime,end_time) $(wavelet),max10
IF(V_flag==0)
	fall10=V_levelX
else
	fall10=nan
	print "10-90 FAILURE fall10: ",peaktime, end_time, peak, max10
//	display $wavelet
endif
//print fall90,fall10
decay1090=fall10-fall90
if (decay1090==end_time)
	decay1090=nan
	print wavelet," Failed to get 10-90 decay time! Increase trace duration!"
endif

// make/O/N=1 show10decay_return1090, show10decay_time_return1090, show90decay_return1090, show90decay_time_return1090, showpeak_return1090, showpeaktime_return1090
// show10decay_return1090[0] = max10
// show10decay_time_return1090[0] = fall10
// show90decay_return1090[0] = max90
// show90decay_time_return1090[0] = fall90
// showpeak_return1090[0] = peak
// showpeaktime_return1090[0] = peaktime

// appendtograph showpeak_return1090 vs showpeaktime_return1090
// ModifyGraph mode(showpeak_return1090)=3,marker(showpeak_return1090)=19
// appendtograph show10decay_return1090 vs show10decay_time_return1090
// modifygraph mode(show10decay_return1090)=3, marker(show10decay_return1090)=19
// modifygraph rgb(show10decay_return1090)=(26411,1,52428)
// appendtograph show90decay_return1090 vs show90decay_time_return1090
// modifygraph mode(show90decay_return1090)=3, marker(show90decay_return1090)=19
// modifygraph rgb(show90decay_return1090)=(26411,1,52428)

// DrawAction getgroup= peakSearchLine, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = peakSearchLine
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 0, 65535) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline range, 0, range, 1
// setdrawenv gstop
// DrawAction endinsert

// DrawAction getgroup= startSearchLine, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = startSearchLine
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 0, 65535) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline 0, 0, 0, 1
// setdrawenv gstop
// DrawAction endinsert

// DrawAction getgroup= t50align, delete, begininsert // the delete should be redunant, but just in case
// setdrawenv gstart, gname = t50align
// setdrawenv xcoord = bottom // set the x coordinators relative to named axis (x-axis)
// setdrawenv linefgc = (0, 65535, 0) // foreground color
// setdrawenv linethick = 2, dash = 2
// drawline 0, .25, 0, .75
// setdrawenv gstop
// DrawAction endinsert

return decay1090
end


///////
//		returnFWHM:  returns the full width at half maximum given:
//			wavename,peak
//
//			assumes the baseline has already been corrected
////////
function returnFWHM(wavelet,thissign)
string wavelet; variable thissign
variable peak,peaktime,range=0.002

//wavestats /Q $(wavelet)
//updated 20130717
wavestats /Q/R=(0,range) $(wavelet)
if (thissign<0)
	peak=V_min
	peaktime=V_minloc
else
	peak=V_max
	peaktime=V_maxloc
endif

variable start_time,end_time,halfmax,FWHM
variable rise50,fall50

start_time=pnt2x($(wavelet),0)			//gets the end of the wave
end_time=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

halfmax=0.5*peak

// AGG 2023-12-13: Changed the search direction from (start_time, peaktime) to (peaktime, start_time)
// The previous version would find the first level crossing from the start of the trace,
// instead of backwards from the peak
// Keeping the old version here for now to troubleshoot and ID differences
// findlevel /Q/R=(start_time, peaktime) $(wavelet),halfmax // bad - starts from start of trace, instead of backwards from peak
findlevel /Q/R=(peaktime, start_time) $(wavelet),halfmax
rise50=V_levelX
findlevel /Q/R=(peaktime,end_time) $(wavelet),halfmax
fall50=V_levelX

FWHM=fall50-rise50
if(fall50==end_time)
	FWHM=nan
endif
if(rise50==start_time)
	FWHM=nan
endif

//print halfmax,rise50,fall50,FWHM

return FWHM
end

///////
//		return1090decay:  returns the decay time  given:
//			wavename, peak
//
//			assumes the baseline has already been corrected
// 20130717 modified peak search to stick to first peak at 0
////////
function return1090Decay(wavelet,thissign)
string wavelet; variable thissign
variable peak,peaktime,range=0.002

//wavestats /Q $(wavelet)
//updated 20130717
wavestats /Q/R=(0,range) $(wavelet)
if (thissign<0)
	peak=V_min
	peaktime=V_minloc
else
	peak=V_max
	peaktime=V_maxloc
endif

variable start_time,end_time,max90,max10,decay1090
variable fall90,fall10

start_time=pnt2x($(wavelet),0)			//gets the end of the wave
end_time=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

max90=0.9*peak
max10=0.1*peak

findlevel /Q/R=(peaktime,end_time) $(wavelet),max90
IF(V_flag==0)
	fall90=V_levelX
else
	fall90=nan
	print "10-90 FAILURE fall90: ",peaktime, end_time, peak, max90
endif

findlevel /Q/R=(peaktime,end_time) $(wavelet),max10
IF(V_flag==0)
	fall10=V_levelX
else
	fall10=nan
	print "10-90 FAILURE fall10: ",peaktime, end_time, peak, max10
//	display $wavelet
endif
//print fall90,fall10
decay1090=fall10-fall90
if (decay1090==end_time)
	decay1090=nan
	print wavelet," Failed to get 10-90 decay time! Increase trace duration!"
endif

return decay1090
end


////////////////////////////////////////////////////////////////////////////////
// fit decay between cursors
////////////////////////////////////////////////////////////////////////////////

function fitdecayIPSC(thissign)
variable thissign //=-1
	string wavel=tracenamelist("",";",1), wavelet=removequotes(stringfromlist(0,wavel))
	variable nwaves=itemsinlist(wavel)
	variable iwave, transientOffset=0.000,maxDur=0.4

variable epochstart,epochend
epochstart=pnt2x($(wavelet),0)			//gets the end of the wave
epochend=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

	variable sstart=epochStart+transientOffset, send=sStart+maxDur,maxtime
variable peak


	if(send>epochend)
		send=epochend
		print "resetting send: ",send, epochend
	endif

//print sstart,send

	variable t0,t1,deltariseT,deltafallT
	variable fall20,fall80
	variable V_fiterror=0  // prevent collapse during fitting singularities
	
	variable sludge=nwaves
	
//	string name="fit"+wavelet,rise,fall,timePeak,tauDecay1,taudecay2
	
//	rise=name+"rise"//
//	fall=name+"fall"
//	timepeak=name+"timePeak"
//	tauDecay1=name+"tauDecay1"
//	tauDecay2=name+"tauDecay2"

	make /O/n=(sludge) rise
//	setscale/P x -100,10,rise
	make /O/n=(sludge) fall
//	setscale/P x -100,10,fall
	make /O/n=(sludge) timepeak
//	setscale/P x -100,10,timePeak
	make /O/n=(sludge) tauDecay1
//	setscale/P x -100,10,taudecay1
	make /O/n=(sludge) tauDecay2
//	setscale/P x -100,10,taudecay2	
	make/O/n=4 w_coef
	make/O/T/n=(sludge) wn
	
	iwave=0
	do
		wavelet=removequotes(stringfromlist(iwave,wavel))
		
		//zero out incomplete subtraction
		//wavestats /Q/R=(send-0.01,send) $(wavelet)
		//$(wavelet)-=V_avg

		WAVE w=$wavelet
		if(waveexists(w))

		else
			print "failed miserably"
			abort
		endif

		wavestats /Q/R=(sstart,send) w //$(wavelet)
		if(thissign>0)
			maxtime=V_maxloc
			peak=V_max
		else
			maxtime=V_minloc
			peak=V_min
		endif
		
		//measure 20-80% risetime
		findlevel /Q/R=(sstart,send) w, 0.2*peak
		t0=V_levelX
		findlevel /Q/R=(sstart,send) w, 0.8*peak
		deltariseT=V_levelX-t0
		
		//print "rise start ",t0
		//measure 20-80% decaytime
		findlevel /Q/R=(maxtime,send) w, 0.8*peak
		t0=V_levelX
		fall20=V_levelX
		findlevel /Q/R=(maxtime,send) w, 0.1*peak
		deltafallT=V_levelX-t0
		fall80=V_levelX
		
		//print "debug fit ", fall20,fall80,0.2*V_max,0.8*V_max
		//print "fall end ", t0
		//print "Maxtime,dtup, dtdown:  ",maxtime,deltaRiseT,deltaFallT
		
		if(t0<sstart) 
			t0=send
		endif
		
		k0=0		// demand zero baseline
		cursor a, $(wavelet),maxtime
 if(fall80<0)
 	fall80=maxdur
 endif
cursor b, $(wavelet), fall80
		if(fall80>0) 
			CurveFit /Q/N/H="100" exp w(xcsr(A),xcsr(B)) /D
			taudecay1[iwave]=1/w_coef[2]
			//$(taudecay2)[iwave]=1/w_coef[4]
		else
			w_coef[0]=0
			w_coef[1]=0
			w_coef[2]=0
			taudecay1[iwave]=0
			//$(taudecay2)[iwave]=0
		endif
		//print fall80," : ",iwave,w_coef[0],w_coef[1],1/w_coef[2]
		
		rise[iwave]=deltaRiseT
		fall[iwave]=deltaFallT
		timepeak[iwave]=maxtime-sstart
		wn[iwave]=wavelet
		iwave+=1
	while(iwave<nwaves)
	edit wn, taudecay1
	wavestats/Q/Z taudecay1
	print V_avg
end


////////////////////////////////////

// updated to work with PSPs 20151005

////////////////////////////////////////////////////////////////////////////////
// fit decay between cursors
////////////////////////////////////////////////////////////////////////////////

function fitdec(thissign)
variable thissign //=-1
	string wavel=tracenamelist("",";",1), wavelet=removequotes(stringfromlist(0,wavel))
	variable nwaves=itemsinlist(wavel)
	variable iwave, transientOffset=0.000,maxDur=0.4,minfitdur=0.02

variable epochstart,epochend
epochstart=pnt2x($(wavelet),0)			//gets the end of the wave
epochend=pnt2x($(wavelet), numpnts($(wavelet))-1)			//gets the end of the wave

	variable sstart=epochStart+transientOffset, send=sStart+maxDur,maxtime
variable peak


	if(send>epochend)
		send=epochend
		print "resetting send: ",send, epochend
	endif

	variable t0,t1,deltariseT,deltafallT
	variable fall20,fall80
	variable V_fiterror=0  // prevent collapse during fitting singularities
	
	variable sludge=nwaves
	
	make /O/n=(sludge) peaks
	make /O/n=(sludge) rise
	make /O/n=(sludge) fall
	make /O/n=(sludge) timepeak
	make /O/n=(sludge) tauDecay1
	make /O/n=(sludge) tauDecay2

	make/O/n=4 w_coef
	make/O/T/n=(sludge) wn
	
	iwave=0
	do
		wavelet=removequotes(stringfromlist(iwave,wavel))
		
		WAVE w=$wavelet
		if(waveexists(w))

		else
			print "failed miserably"
			abort
		endif

		wavestats /Q/R=(sstart,send) w
		if(thissign>0)
			maxtime=V_maxloc
			peak=V_max
		else
			maxtime=V_minloc
			peak=V_min
		endif
				
		sstart=maxtime
		
		// 20-80% rise time
		findlevel /Q/R=(sstart,send) w, 0.2*peak
		t0=V_levelX
		findlevel /Q/R=(sstart,send) w, 0.8*peak
		deltariseT=V_levelX-t0
		
		//measure 80-20% decaytime
		findlevel /Q/R=(maxtime,send) w, 0.8*peak
		t0=V_levelX
		fall20=V_levelX
		findlevel /Q/R=(maxtime,send) w, 0.1*peak
		deltafallT=V_levelX-t0
		fall80=V_levelX
		
		if(t0<sstart) 
			t0=send
		endif
		
		k0=0		// demand zero baseline
		cursor a, $(wavelet),maxtime
		if(fall80<minfitdur)
 			fall80=minfitdur //maxdur
 		endif
		cursor b, $(wavelet), fall80
		if(fall80>0) 
			CurveFit /Q/N/H="100" exp w(xcsr(A),xcsr(B)) /D
			taudecay1[iwave]=1000/w_coef[2]
			//$(taudecay2)[iwave]=1/w_coef[4]
		else
			w_coef[0]=0
			w_coef[1]=0
			w_coef[2]=0
			taudecay1[iwave]=0
			//$(taudecay2)[iwave]=0
		endif
		//print fall80," : ",iwave,w_coef[0],w_coef[1],1/w_coef[2]
		
		peaks[iwave]=1e12*peak
		rise[iwave]=1e3*deltaRiseT
		fall[iwave]=1e3*deltaFallT
		timepeak[iwave]=1e3*maxtime-sstart
		wn[iwave]=wavelet
		iwave+=1
	while(iwave<nwaves)
	edit /K=1 wn, taudecay1,peaks
	wavestats/Q/Z taudecay1
	print V_avg
end




////////////////////////////////////////////////////////////////////////////////
// zero baseline: uses graph settings as range, top graph envelope
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// fit decay between cursors
////////////////////////////////////////////////////////////////////////////////

function autobaselineEnvelope(thissign)
variable thissign //=-1
string wavel=tracenamelist("",";",1), wavelet=removequotes(stringfromlist(0,wavel))
variable nwaves=itemsinlist(wavel)
variable iwave, transientOffset=0.000,maxDur=0.4

// get graph range



end // envelope

function autobaseline(wn, thissign, offset)
string wn
variable thissign, offset //=-1

WAVE w = $wn

if(!waveexists(w))
	print "no wave:",wn
	abort
endif

end // autobaseline
