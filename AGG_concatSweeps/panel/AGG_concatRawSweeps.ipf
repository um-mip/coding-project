///////////
/// buildConcatRawSweepsPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: 
///////////
function buildConcatRawSweepsPanel()
    
    DoWindow/F AGG_ConcatRawSweeps     // bring panel to the front if it exists
    if( V_Flag != 0 ) // panel exists
        return 0
    endif
    
    // panel doesn't exist
    string panelName = "AGG_ConcatRawSweeps"
    string udata = ""

    // define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP

    // initialize it
    InitScreenParameters(ScrP)

    // Create the panel
    AGG_makePanel(5, 0, 95, 80, panelName)
    modifypanel cbRGB=(50000,50000,50000)

    variable width, height
    [width, height] = getWindowDims(type = "point")

    // print width
    variable tabFSize = width/100
    variable panelFSize = width/88
    if(panelFSize>18)
        panelFSize = 18
    endif
    print "tab font size", tabFSize, "panel font size", panelFSize
    
    //the current publishing standard is 1 pt = 1/72"). In other words points are fixed in size independent of resolution. This is good since, all things properly configured, 12 point text on an monitor will be the same height when printed on paper.

    DefaultGUIFont/W=$panelName panel = {"Arial", panelFSize, 0}, all = {"Arial", panelFSize, 0}, TabControl = {"Arial", tabFSize, 0}
    DefaultGUIFont/W=$panelName graph = {"Arial", 16, 0}, table = {"Arial", panelFSize, 0}
    // DefaultFont

    // Use a structure for positioning to pass easily to subfunctions
    STRUCT AGG_EventsPanelSizeInfo panelSize
    
    // Variable and dimensions
    variable xPos = 0.02
    variable yPos = 0.06
    variable xRightPos = 0.8

    variable xPosPnts = posRelPanel(xPos, "width")
    variable yPosPnts = posRelPanel(yPos, "height")
    variable xRightPosPnts = posRelPanel(xRightPos, "width")

    variable buttonWidth, buttonHeight, listBoxWidth, listBoxHeight, buttonWidthRel = 0.15, buttonHeightRel = 0.04, listBoxHeightRel = 0.35
    [buttonWidth, buttonHeight] = xyRelPanel(buttonWidthRel, buttonHeightRel)
    [listBoxWidth, listBoxHeight] = xyRelPanel(buttonWidthRel, listBoxHeightRel)

    panelSize.xPos = xPos
    panelSize.yPos = yPos
    panelSize.xRightPos = xRightPos
    panelSize.xPosPnts = xPosPnts
    panelSize.yPosPnts = yPosPnts
    panelSize.xRightPosPnts = xRightPosPnts
    panelSize.buttonWidthRel = buttonWidthRel
    panelSize.buttonHeightRel = buttonHeightRel
    panelSize.buttonWidth = buttonWidth
    panelSize.buttonHeight = buttonHeight
    panelSize.listBoxWidth = listBoxWidth
    panelSize.listBoxHeight = listBoxHeight
    panelSize.listBoxHeightRel = listBoxHeightRel
    panelSize.left = 0.2
    panelSize.top = 0.1
    panelSize.right = 0.8
    panelSize.bottom = 0.95

    createConcatRawDF()
    createConcatRawPanelDF()
    DFREF panelDFR = getConcatRawPanelDF()
    
    string/G panelDFR:origDateCode/N=origDateCode
    origDateCode = ""
    
    string/G panelDFR:updatedDateCode/N=updatedDateCode
    updatedDateCode = ""

    xPos = xPosPnts
    yPos = yPosPnts
    SetVariable origDateCodeVar pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Original date code:", value = origDateCode, proc = updateConcRawSeriesLBProc
    xPos += listBoxWidth*1.1
    SetVariable updatedDateCodeVar pos = {xPos, yPos}, size = {listBoxWidth + listBoxWidth*1.1, buttonHeight}, title = "Updated date code - don't overwrite:", value = updatedDateCode, proc = updateConcRawSeriesLBProc
    xPos -= listBoxWidth*1.1

    Button saveConcPlotButton, pos = {posRelPanel(0.55, "width", panelName = panelName), yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="concatDisplay", proc = openSaveIntProc, title = "COPY or SAVE"
    yPos += buttonHeight

    // Series Select Listbox
    string selSeriesTextWN = "selSeriesText"
    if(! WaveExists(panelDFR:$selSeriesTextWN))
        make/T/O panelDFR:$selSeriesTextWN
    endif
    
    string selSeriesWN = "selSeries"
    if(! WaveExists(panelDFR:$selSeriesWN))
        make/O panelDFR:$selSeriesWN
    endif

    // Sweep Select Listbox - allow multiple
    string selSweepTextWN = "selSweepText"
    if(! WaveExists(panelDFR:$selSweepTextWN))
        make/T/O panelDFR:$selSweepTextWN
    endif

    string selSweepWN = "selSweep"
    if(! WaveExists(panelDFR:$selSweepWN))
        make/O panelDFR:$selSweepWN
    endif

    // Trace Select Listbox - allow multiple
    string selTraceTextWN = "selTraceText"
    if(! WaveExists(panelDFR:$selTraceTextWN))
        make/T/O panelDFR:$selTraceTextWN
    endif

    string selTraceWN = "selTrace"
    if(! WaveExists(panelDFR:$selTraceWN))
        make/O panelDFR:$selTraceWN
    endif

    redimension/N=0 panelDFR:$selSeriesTextWN, panelDFR:$selSeriesWN, panelDFR:$selSweepTextWN, panelDFR:$selSweepWN, panelDFR:$selTraceTextWN, panelDFR:$selTraceWN

    TitleBox seriesNamesText pos = {xPos, yPos}, title = "Select a series:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    ListBox seriesListBox, mode=1, listwave = panelDFR:$selSeriesTextWN, selwave = panelDFR:$selSeriesWN, pos = {xPos, yPos}, size = {listBoxWidth, listBoxHeight}, proc = updateConcRawSweepLBProc
    xPos += listBoxWidth*1.1
    
    yPos -= buttonHeight
    TitleBox sweepNamesText pos = {xPos, yPos}, title = "Select sweeps to concat (shift):", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    ListBox sweepListBox, mode=4, listwave = panelDFR:$selSweepTextWN, selwave = panelDFR:$selSweepWN, pos = {xPos, yPos}, size = {listBoxWidth, listBoxHeight}
    xPos += listBoxWidth*1.1

    yPos -= buttonHeight
    TitleBox traceNamesText pos = {xPos, yPos}, title = "Select trace num:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    ListBox TraceListBox, mode=1, listwave = panelDFR:$selTraceTextWN, selwave = panelDFR:$selTraceWN, pos = {xPos, yPos}, size = {listBoxWidth, listBoxHeight}
    yPos += listBoxHeight + buttonHeight

    xPos -= listBoxWidth*2.2
    variable/G panelDFR:durationToAvg/N=durationToAvg
    if(numtype(durationToAvg)!=0)
        durationToAvg = 5
    endif

    SetVariable durationToAvgVar, pos = {xPos, yPos}, size = {listBoxWidth*2.1, buttonHeight}, value=durationToAvg, title="Duration to average for gaps (ms):"
    yPos += buttonHeight
    Button concatRawButton, pos = {xPos, yPos}, size = {listBoxWidth*3.2, buttonHeight}, title = "Concatenate sel'd sweeps", proc=concatRawSweepsProc

    Display/W=(0.55, 0.1, 0.98, 0.98)/L/B/Host=$panelName
    RenameWindow #, concatDisplay
    SetActiveSubWindow##
end

///////////
/// updateConcRawSeriesLBProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Based on the typed date code, find the associated series and update the listbox
///////////
function updateConcRawSeriesLBProc(Var_Struct) :SetVariableControl
    STRUCT WMSetVariableAction & Var_Struct
    if(Var_Struct.eventcode == 1 || Var_Struct.eventcode == 6 || Var_Struct.eventCode == 8)
        string cellName = Var_Struct.sVal
        updateConcRawSeriesLB(cellName)
    endif
    return 0
end

///////////
/// updateConcRawSeriesLB
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Based on the typed date code, find the associated series and update the listbox
///////////
function updateConcRawSeriesLB(string dateCode)
    clearDisplay("AGG_ConcatRawSweeps#concatDisplay")
    DFREF panelDFR = getConcatRawPanelDF()
    if(dataFolderRefStatus(panelDFR)==0)
        print "updateConcRawSeriesLB: No panelDFR. Quitting"
        return 0
    endif
    
    string selSeriesTextWN = "selSeriesText", selSeriesWN = "selSeries"
    Wave/SDFR=panelDFR/T selSeriesText = $selSeriesTextWN
    Wave/SDFR=panelDFR selSeries = $selSeriesWN

    if(!WaveExists(selSeriesText) || !WaveExists(selSeries))
        return 0
    endif

    redimension/N=0 selSeriesText, selSeries
    
    SVAR/SDFR=panelDFR origDateCode
    if(strlen(origDateCode)==0)
        // no date code
        return 0
    endif

    string wavesMatchingCellRegEx = getStringOfRawWaves()
    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    string listOfSeries = "", firstCellID = ""
    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID, seriesN, sweepN, traceN
        [cellID, seriesN, sweepN, traceN] = getCellIDSeriesSweepTraceFromWaveN(waveN)
        if(strlen(cellID)==0) // no cellID, means search failed
            continue
        endif

        if(stringmatch(cellID, origDateCode))
            if(strlen(firstCellID) == 0)
                firstCellID = cellID
            endif
            if(!stringMatch(firstCellID, cellID))
                continue // different cellID. It shouldn't happen because using stringmatch above, but could with a grep
            endif
            if(!isInList(seriesN, listOfSeries))
                listOfSeries += seriesN + ";"
            endif


        endif
    endfor

    variable iSeries = 0, nSeries = itemsInList(listOfSeries)
    redimension/N=(nSeries) selSeriesText, selSeries

    selSeries = 0
    for(iSeries=0; iSeries<nSeries; iSeries++)
        selSeriesText[iSeries] = StringFromList(iSeries, listOfSeries)
    endfor
end


///////////
/// updateConcRawSweepLBProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Based on the selected series, find the associated sweeps waves and update the listbox
///////////
function updateConcRawSweepLBProc(LB_Struct) : ListBoxControl
    STRUCT WMListboxAction & LB_Struct
    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF panelDFR = getConcatRawPanelDF()
        SVAR/SDFR=panelDFR origDateCode

        string selSeries = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)

        updateConcRawSweepLB(origDateCode, selSeries)

        SVAR/SDFR=panelDFR updatedDateCode
        if(strlen(updatedDateCode)==0)
            return 0
        endif

        string traceN = getSelectedItem("TraceListBox", hostName = LB_Struct.win)
        string newWaveN = buildSeriesWaveName(updatedDateCode, str2num(selSeries), traceNum = str2num(traceN))
        Wave newWave = $newWaveN
        if(!WaveExists(newWave))
            return 0
        endif
        plotSeriesWithSweepGaps(newWave, "AGG_ConcatRawSweeps#concatDisplay")
    endif
    return 0
end

///////////
/// updateConcRawSweepLB
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Based on the typed date code and series, find the associated Sweep and update the listbox
///////////
function updateConcRawSweepLB(string dateCode, string seriesMatch)
    clearDisplay("AGG_ConcatRawSweeps#concatDisplay")
    DFREF panelDFR = getConcatRawPanelDF()
    if(dataFolderRefStatus(panelDFR)==0)
        print "updateConcRawSeriesLB: No panelDFR. Quitting"
        return 0
    endif
    
    string selSweepTextWN = "selSweepText", selSweepWN = "selSweep", selTraceTextWN = "selTraceText", selTraceWN = "selTrace"
    Wave/SDFR=panelDFR/T selSweepText = $selSweepTextWN, selTraceText = $selTraceTextWN
    Wave/SDFR=panelDFR selSweep = $selSweepWN, selTrace = $selTraceWN

    if(!WaveExists(selSweepText) || !WaveExists(selSweep) || !WaveExists(selTraceText) || !WaveExists(selTrace))
        return 0
    endif

    redimension/N=0 selSweepText, selSweep, selTraceText, selTrace
    
    SVAR/SDFR=panelDFR origDateCode
    if(strlen(origDateCode)==0 || strlen(seriesMatch) == 0)
        // no date code and/or seriesMatch
        return 0
    endif

    string wavesMatchingCellRegEx = getStringOfRawWaves()
    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    string listOfSweeps = "", listOfTraces = "", firstCellID = "", firstSeriesN = ""
    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID, seriesN, sweepN, traceN
        [cellID, seriesN, sweepN, traceN] = getCellIDSeriesSweepTraceFromWaveN(waveN)
        if(strlen(cellID)==0) // no cellID, means search failed
            continue
        endif

        if(stringmatch(cellID, origDateCode) && stringMatch(seriesN, seriesMatch))
            if(strlen(firstCellID) == 0)
                firstCellID = cellID
            endif
            if(!stringMatch(firstCellID, cellID))
                continue // different cellID. It shouldn't happen because using stringmatch above, but could with a grep
            endif
            if(strlen(firstSeriesN) == 0)
                firstSeriesN = SeriesN
            endif
            if(!stringMatch(firstSeriesN, SeriesN))
                continue // different seriesN. It shouldn't happen because using stringmatch above, but could with a grep
            endif
            if(!isInList(sweepN, listOfSweeps))
                listOfSweeps += sweepN + ";"
            endif

            if(!isInList(traceN, listOfTraces))
                listOfTraces += traceN + ";"
            endif
        endif
    endfor

    // Update the listbox waves for sweeps
    variable iSweep = 0, nSweeps = itemsInList(listOfSweeps)
    redimension/N=(nSweeps) selSweepText, selSweep
    selSweep = 0
    for(iSweep=0; iSweep<nSweeps; iSweep++)
        selSweepText[iSweep] = StringFromList(iSweep, listOfSweeps)
    endfor
    
    // Update the listbox waves for traces
    variable iTrace = 0, nTraces = itemsInList(listOfTraces)
    redimension/N=(nTraces) selTraceText, selTrace
    selTrace = 0
    for(iTrace=0; iTrace<nTraces; iTrace++)
        selTraceText[iTrace] = StringFromList(iTrace, listOfTraces)
    endfor
end

///////////
/// getStringOfRawWaves
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Use the datecode and expected name to find waves in root by regex
///////////
function/S getStringOfRawWaves()
    DFREF dfr = root:
    string wavesMatchingCellRegEx = findWavesInDFRByRegEx("([[:digit:]]+)([[:alpha:]])g([[:digit:]]+)s([[:digit:]]+)sw([[:digit:]]+)t([[:digit:]]$)", dfr = dfr)
    return wavesMatchingCellRegEx
end


///////////
/// getSelectedWavesForRawConc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: 
///////////
function/Wave getSelectedWavesForRawConc()

    DFREF panelDFR = getConcatRawPanelDF()

    if(dataFolderRefStatus(panelDFR)==0)
        abort
    endif

    string selSweepTextWN = "selSweepText", selSweepWN = "selSweep"
    Wave/SDFR=panelDFR/T selSweepText = $selSweepTextWN
    Wave/SDFR=panelDFR selSweep = $selSweepWN

    if(!WaveExists(selSweepText) || !WaveExists(selSweep))
        abort
    endif

    SVAR/SDFR=panelDFR origDateCode
    if(strlen(origDateCode)==0)
        abort
    endif

    string selSeriesString = getSelectedItem("seriesListBox", hostName = "AGG_ConcatRawSweeps")
    string selTraceString = getSelectedItem("traceListBox", hostName = "AGG_ConcatRawSweeps")

    if(strlen(selSeriesString)==0 || strlen(selTraceString) == 0)
        abort
    endif

    variable nSweeps = numpnts(selSweep), iSweep = 0
    string listOfSweeps = ""
    for(iSweep=0; iSweep<nSweeps; iSweep++)
        if(selSweep[iSweep]==1)//selected
            string thisSweep = selSweepText[iSweep]
            listOfSweeps += thisSweep + ";"
        endif
    endfor

    if(itemsInList(listOfSweeps)==0)
        abort
    endif

    make/Wave/O panelDFR:matchingWaves/Wave=matchingWaves
    redimension/N=0 matchingWaves

    string wavesMatchingCellRegEx = getStringOfRawWaves()
    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    variable iMatching = 0
    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID, seriesN, sweepN, traceN
        [cellID, seriesN, sweepN, traceN] = getCellIDSeriesSweepTraceFromWaveN(waveN)

        if(stringmatch(cellID, origDateCode) && stringmatch(seriesN, selSeriesString) && isInList(sweepN, listOfSweeps) && stringmatch(traceN, selTraceString))
            Wave thisWave = root:$waveN
            if(!WaveExists(thisWave))
                continue
            endif
            redimension/N=(iMatching + 1) matchingWaves
            matchingWaves[iMatching] = thisWave
            iMatching++
        endif
    endfor

    return matchingWaves
end

///////////
/// concatRawSweepsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: concatenate the selected sweeps for the indicated cell/series/trace
///////////
function concatRawSweepsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        concatRawSweeps()
    endif
    return 0
end


///////////
/// testPrintMatching
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: 
///////////
function testPrintMatching()
    Wave/Wave matchingWaves = getSelectedWavesForRawConc()

    for(Wave thisWave : matchingWaves)
        print NameOfWave(thisWave)
    endfor
end

///////////
/// checkUpdatedDateCode
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: Checks that the updated date code is numeric then single alpha, and not matching existing cell names
///////////
function checkUpdatedDateCode(string stringToCheck, string forSeriesNum, string forTraceNum)
    variable isOkay = 0

    if(strlen(stringToCheck)==0)
        print "checkUpdatedDateCode: Provide an updated date code"
        return isOkay
    endif

    string regExp="^([[:digit:]]+)([[:alpha:]])$"
    string datecode, letter
    splitstring /E=(regExp) stringToCheck, datecode, letter

    if(strlen(dateCode)==0 && strlen(letter)==0)
        print "checkUpdatedDateCode: The pattern of the updated date code is incorrect"
        print "You supplied", stringToCheck
        print "Date code must start with number(s) and end with a single letter. Ex. 20240503a"
        return isOkay
    endif

    string newCellID = dateCode + letter

    string wavesMatchingCellRegEx = getStringOfRawWaves()
    variable iWave = 0, nWaves = itemsInList(wavesMatchingCellRegEx)

    variable iMatching = 0
    for(iWave=0; iWave<nWaves; iWave++)
        string waveN = StringFromList(iWave, wavesMatchingCellRegEx)
        string cellID, seriesN, sweepN, traceN
        [cellID, seriesN, sweepN, traceN] = getCellIDSeriesSweepTraceFromWaveN(waveN)

        if(stringmatch(cellID, newCellID) && stringMatch(seriesN, forSeriesNum) && stringMatch(traceN, forTraceNum))
            variable allowOverwrite = 0
            string allowOverwritePrompt
            Prompt allowOverwritePrompt, "You've provided a new date code that matches an existing cell & series & trace combo. Are you sure that you want to overwrite?: ", popup, "no;yes;"
            DoPrompt "Detected Wave", allowOverwritePrompt
            if(StringMatch(allowOverwritePrompt, "yes"))
                allowOverwrite = 1
            endif
            if(!allowOverwrite)
                print "checkUpdatedDateCode: You've provided a new date code that matches an existing cell & series & trace combo. Pick a new one"
                return isOkay
            endif
        endif
    endfor

    isOkay = 1
    return isOkay
end


///////////
/// concatRawSweeps
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-03
/// NOTES: concatenate the selected sweeps for the indicated cell/series/trace
///////////
function concatRawSweeps()
    DFREF panelDFR = getConcatRawPanelDF()
    if(dataFolderRefStatus(panelDFR)==0)
        abort
    endif
    clearDisplay("AGG_ConcatRawSweeps#concatDisplay")
    string selSeriesString = getSelectedItem("seriesListBox", hostName = "AGG_ConcatRawSweeps")
    string selTraceString = getSelectedItem("traceListBox", hostName = "AGG_ConcatRawSweeps")
    
    SVAR/SDFR=panelDFR updatedDateCode 
    variable dateCodeOkay = checkUpdatedDateCode(updatedDateCode, selSeriesString, selTraceString)
    if(!dateCodeOkay)
        abort
    endif

    Wave/Wave matchingWaves = getSelectedWavesForRawConc()
    
    variable iWave = 0, nWaves = numpnts(matchingWaves)
    if(nWaves == 0)
        print "concatRawSweeps: There are no waves matching selection"
        abort
    endif


    if(strlen(selSeriesString)==0 || strlen(selTraceString) == 0)
        abort
    endif

    string newWaveN = updatedDateCode + "g1s" + selSeriesString + "sw1" + "t" + selTraceString
    print "concatRawSweeps: Creating a new wave with the name", newWaveN
    
    Wave newWave = concatWithGaps(matchingWaves, newWaveN)
    if(WaveExists(newWave))
        // AppendToGraph/W=AGG_ConcatRawSweeps#concatDisplay newWave
        plotSeriesWithSweepGaps(newWave, "AGG_ConcatRawSweeps#concatDisplay")
    endif
end

///////////
/// concatWithGaps
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-04
/// NOTES: Concatenate waves together with gaps accounted for
/// Option to leave gap empty, or use an average. If avg, specify duration (ms)
///////////
function/Wave concatWithGaps(Wave/Wave wavesToConcat, string newWaveN[, variable fillGap, variable durationToAvg, DFREF newWaveDFR])
    if(paramIsDefault(fillGap))
        fillGap = 1
    endif

    if(paramIsDefault(durationToAvg))
        durationToAvg = 5
        DFREF panelDFR = getConcatRawPanelDF()
        if(dataFolderRefStatus(panelDFR)!=0)
            NVAR/Z/SDFR=panelDFR gDurationToAvg = durationToAvg
            if(numType(gDurationToAvg) == 0)
                durationToAvg = gDurationToAvg
            endif
        endif
    endif

    if(paramIsDefault(newWaveDFR))
        DFREF newWaveDFR = root:
    endif
    

    variable durationToAvg_sec = durationToAvg / 1000

    print "durationToAvg_sec", durationToAvg_sec

    if(!WaveExists(wavesToConcat))
        print "concatWithGaps: No wavesToConcat"
        abort
    endif

    Wave/SDFR=newWaveDFR newWave = $newWaveN
    KillWaves/Z newWave // get rid of existing wave by this name. It shouldn't exist from check, but to be certain...
    
    variable iWave = 0, nWaves = numpnts(wavesToConcat)
    variable waveStart, waveDur, waveEnd
    variable absStart, prevStart, prevEnd, gapDur, gapStart, gapEnd
    variable prevWaveEndAvg, thisWaveStartAvg, combinedAvg
    string gapStarts = "", gapEnds = "", addedSweeps = ""

    for(iWave=0; iWave<nWaves; iWave++)
        Wave thisWave = wavesToConcat[iWave]
        if(!WaveExists(thisWave))
            continue
        endif

        string cellID, seriesN, sweepN, traceN
        [cellID, seriesN, sweepN, traceN] = getCellIDSeriesSweepTraceFromWaveN(nameOfWave(thisWave))
        addedSweeps += sweepN + "," // there will be extra commas. Can fix later if care, but extra work

        waveStart=NaN; waveDur=NaN; waveEnd=NaN
        [waveStart, waveDur] = calcSeriesTimeAndDuration(thisWave) // time in note (from Patchmaster) converted to Igor
        waveEnd = waveStart + waveDur

        if(iWave == 0)
            absStart = waveStart
            duplicate/O/FREE thisWave, thisWaveToAdd
        else
            gapDur = waveStart - prevEnd
            gapEnds += num2str(waveStart - absStart) + "," // there will be extra commas. Can fix later if care, but extra work

            WaveStats/Q/R=(0, durationToAvg_sec)/Z thisWave
            thisWaveStartAvg = V_avg

            combinedAvg = NaN
            if(fillGap)
                combinedAvg = avgTwoVals(prevWaveEndAvg, thisWaveStartAvg)
            endif

            // Add gap's worth of points to the beginning of this wave, set equal to combinedAvg
            variable dx=deltaX(thisWave)
            variable gapPnts = gapDur/dx // this might not round perfectly, but it should be close enough
            duplicate/O/FREE thisWave, thisWaveToAdd
            insertpoints/V=(combinedAvg) 0, gapPnts, thisWaveToAdd
        endif

        Concatenate/NP {thisWaveToAdd}, newWaveDFR:$newWaveN
        
        if(iWave != nWaves - 1)
            WaveStats/Q/R=(waveDur - durationToAvg_sec, waveDur)/Z thisWave
            prevWaveEndAvg = V_avg

            prevStart = waveStart
            prevEnd = waveEnd

            gapStarts += num2str(waveEnd - absStart) + "," // there will be extra commas. Can fix later if care, but extra work
        endif
    endfor

    Wave/SDFR=newWaveDFR newWave = $newWaveN

    // To get values from note later, will need to use a function like AGG_wnoteSTRbyKey that gets rid
    // of whitespace in the note
    Note newWave, "concatSweeps:" + addedSweeps + ";sweepGapStarts:" + gapStarts + ";sweepGapEnds:" + gapEnds

    return newWave
end


///////////
/// plotSeriesWithSweepGaps
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-04
/// NOTES: 
///////////
function plotSeriesWithSweepGaps(wave waveToPlot, string hostN[, DFREF dfrForPlotWaves])
    if(paramIsDefault(dfrForPlotWaves))
        DFREF dfrForPlotWaves = getConcatRawPanelDF()
    endif

    if(dataFolderRefStatus(dfrForPlotWaves)==0)
        print "Can't find the inteded folder to store the plot waves. Defaulting to root"
        DFREF dfrForPlotWaves = root:
    endif

    string waveN
    DFREF waveDFR
    [waveN, waveDFR] = getWaveNameAndDFR(waveToPlot)

    string gapSTARTSstring = AGG_wnoteSTRbyKey( waveN, "sweepGapStarts", waveDFR )
    string gapENDSstring = AGG_wnoteSTRbyKey( waveN, "sweepGapEnds", waveDFR )

    variable duration = pnt2x(waveToPlot,numpnts(waveToPlot)-1) - pnt2x(waveToPlot, 0)

    variable foundGaps = makeGapPlotWaves_fromString(gapStartsString, gapEndsString, dfrForPlotWaves = dfrForPlotWaves, addStartEnd = 1, totalDur = duration)

    if(foundGaps)
        Wave/SDFR=dfrForPlotWaves gapPlotX, gapPlotY

        if(WaveExists(gapPlotX) && WaveExists(gapPlotY))
            appendGapPlotsToGraph(gapPlotX, gapPlotY, hostN = hostN) // put this first, so can see the average over it
        endif
        
    endif

    duplicate/O waveToPlot, dfrForPlotWaves:concatWave/Wave=concatWave
    AppendToGraph/W=$hostN concatWave
    SetAxis/A/W=$hostN left
    Label/W=$hostN bottom, "\Z15 Elapsed Time (hh:mm:ss.ms)"
    ModifyGraph/W=$hostN dateInfo(bottom)={1,2,0}, rgb(concatWave) = (0, 0, 0)
end


///////////
/// makeGapPlotWaves_fromString
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-04
/// NOTES: 
///////////
function makeGapPlotWaves_fromString(string gapStartsString, string gapEndsString[, DFREF dfrForPlotWaves, variable addStartEnd, variable totalDur])
    if(paramIsDefault(dfrForPlotWaves))
        DFREF dfrForPlotWaves = getConcatRawPanelDF()
    endif

    if(dataFolderRefStatus(dfrForPlotWaves)==0)
        print "Can't find the inteded folder to store the plot waves. Defaulting to root"
        DFREF dfrForPlotWaves = root:
    endif

    // Add these waves to the cell detection folder. Can be referenced wtihin detection
    Wave gapStarts = AGG_waveFromStringList( gapStartsString, "gapStarts", dfrForPlotWaves)
    Wave gapEnds = AGG_waveFromStringList( gapEndsString, "gapEnds", dfrForPlotWaves )

    if(!WaveExists(gapStarts) || !WaveExists(gapEnds))
        print "makeGapPlotWaves_fromString: Couldn't find gap start/end info"
        return 0
    endif

    if(paramIsDefault(addStartEnd) || paramIsDefault(totalDur))
        addStartEnd = 0
    endif

    variable numGaps = numpnts(gapStarts) // assumption here that they have same # of points - TO-DO check this
    
    variable iGap = 0, iGapPlot = 0

    variable maxPlotPoints = numGaps * 4 // two points each for gaps starts/end

    make/N=(maxPlotPoints)/D/O dfrForPlotWaves:gapPlotX/Wave=gapPlotX
    make/N=(maxPlotPoints)/D/O dfrForPlotWaves:gapPlotY/Wave=gapPlotY
    gapPlotX = 0
    gapPlotY = 0 // is it a gap (1) or not(0)
    
    if(numGaps == 0)
        print "no gaps"
        return 0
    endif

    variable thisGapStart, thisGapRelStart
    variable thisGapEnd, thisGapRelEnd

    variable relStartTime, relEndTime

    for(iGap = 0; iGap < numGaps; iGap ++ )
        thisGapStart = gapStarts[iGap]
        thisGapEnd = gapEnds[iGap]

        gapPlotX[iGapPlot] = thisGapStart
        gapPlotY[iGapPlot] = 0
        iGapPlot ++ 

        gapPlotX[iGapPlot] = thisGapStart
        gapPlotY[iGapPlot] = 1
        iGapPlot ++ 

        gapPlotX[iGapPlot] = thisGapEnd
        gapPlotY[iGapPlot] = 1
        iGapPlot ++

        gapPlotX[iGapPlot] = thisGapEnd
        gapPlotY[iGapPlot] = 0
        iGapPlot ++
    endfor

    if(addStartEnd)
        insertPoints 0, 1, gapPlotX, gapPlotY
        gapPlotX[0] = 0
        gapPlotY[0] = 0

        gapPlotX[numpnts(gapPlotX)] = {totalDur}
        gapPlotY[numpnts(gapPlotY)] = {0}

        // if want to see vertical line at start and end
        // insertPoints 0, 2, gapPlotX, gapPlotY
        // gapPlotX[0] = 0
        // gapPlotY[0] = 1

        // gapPlotX[1] = 0
        // gapPlotY[1] = 0

        // gapPlotX[numpnts(gapPlotX)] = {totalDur}
        // gapPlotY[numpnts(gapPlotY)] = {0}
        // gapPlotX[numpnts(gapPlotX)] = {totalDur}
        // gapPlotY[numpnts(gapPlotY)] = {1}
        // gapPlotX[numpnts(gapPlotX)] = {totalDur}
        // gapPlotY[numpnts(gapPlotY)] = {0}
    endif

    return 1
end