// Outer Events folder
    ThreadSafe function createConcatRawDF()
        NewDataFolder/O root:ConcatRaw
    end

    ThreadSafe function/DF getConcatRawDF()
        DFREF dfr = root:ConcatRaw
        return dfr
    end

// Panel folder
    ThreadSafe function createConcatRawPanelDF()
        DFREF ConcatRawDF = getConcatRawDF()
        NewDataFolder/O ConcatRawDF:Panel
    end

    ThreadSafe function/DF getConcatRawPanelDF()
        DFREF ConcatRawDF = getConcatRawDF()
        DFREF dfr = ConcatRawDF:Panel
        return dfr
    end