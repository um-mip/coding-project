#pragma rtGlobals=1		// Use modern global access method.
//#include <Resize Controls>

// 20160229 adjusted zooom feature to use 0.9 decrement, 1.1 increment
// 20131211 now includes scale bars in every subwindow
// 20131216 select all
// 20150825 creates (and destroys) derwave for display

///////////
/// cerebroDelete
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-01
/// NOTES: Scan for events within Cerebro that have been marked for deletion
/// Right now, this relies on the event number being NaN to exclude the possibility of
/// deleting an event accidentally when showing levels
///////////
function cerebroDelete([string panelN])
	// Default will search for global variable 
	// within current DF that is named g_paneln
	// This should likely be "Cerebro"
	if(paramIsDefault(panelN)) 
		SVAR globalPanelN = g_paneln
		panelN = globalPanelN
	endif

	// Get number of rows and columns
	NVAR ngx = g_ngx, ngy = g_ngy

	// Get wave of interest
	SVAR waven = g_waven

	// Store which event numbers are checked
	make/O/N=(ngx*ngy) toBeDeleted
	toBeDeleted = NaN // start off assuming nothing is to be deleted
	
	// indices
	variable iX = 0, iY = 0, iGraph = 0

	// loop over graphs in Cerebro, mark to be deleted
	do
		iX = 0
		do
			variable checkState = getCerebroCheckState(iX, iY, "delete")
			if(checkState == 1) // mark to be deleted
				// Get the event number for the graph
				// This doesn't have to be sequential if only averages are shown
				variable iEvent = getGraphEventNum(iX, iY)
				if(iEvent >= 0) // it could be NaN, if no associated event for graph (or if level plotted)
					toBeDeleted[iGraph] = iEvent
					print "event", iEvent, "from wave", waven, "is tagged to be deleted"
				endif
			endif
			iGraph++
			iX++
		while (iX < ngx)
		iY++
	while (iY < ngy)


	// Need panel parameters for sign for deleteEvent function
	STRUCT analysisParameters ps
	variable junk = readpanelparams2(ps) // AGG the panel needs to be updated with what was used during detection prior to looking at the event in Cerebro
	variable thissign = ps.peaksign	

	// Delete the events
	for(variable iEvent : toBeDeleted)
		if(iEvent >= 0)
			// print iEvent, "iEvent"
			// print toBeDeleted
			deleteevent(iEvent,waven,thissign)
			toBeDeleted -=1 // need to reduce since just deleted one event
		endif
	endfor
	
	// recalculateaverages4( waven, "", "" ) // removed 2024-05-19, deleteEvent uses recalculateaverages5
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	ADD ADD ADD ADD ADD ADD ADD EVENTS IN CEREBRO
//

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cerebroADD()
	// get wave of interest
	SVAR waven = g_waven, paneln=g_paneln
	
	// send active waven and time point to "add event" routine from wave_navigator
	addEvent(xcsr(A), waven)
	// recalculateaverages4( waven, "", "" ) // 20220105 from v2 to v4	// this is also called within addEvent // 2024-05-19 removed, since addEvent calls recalculateAverages5
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	SELECT ALL EVENTS IN CEREBRO
//

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cerebroSelectAll()
	// get number of rows and columns (global variables)
	NVAR ngx=g_ngx, ngy=g_ngy, event=g_event, level=g_level
	// get wave of interest
	SVAR waven = g_waven, paneln=g_paneln
	STRUCT analysisParameters ps
	WAVE/Z eventlist=eventlist
	
	variable junk=0,ix=0,iy=0,ievent=0,i=0
	string target, graphname, checkbname, minipaneln="", ptarget=""
	
	junk = readpanelparams2(ps) // AGG - need to pull this from cell detect info not from the panel
	variable thissign = ps.peaksign	

	ievent=event
	i=0
	iy=0
	do	
		ix=0
		do
			graphname = getSpecCerebroStr(ix, iy, "graph") // AGG 2023-08-01
			// sprintf graphname, "g%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency
			target=paneln+"#"+graphname

			minipaneln = getSpecCerebroStr(iX, iY, "miniPanel") // AGG 2023-08-01
 			// sprintf minipaneln, "P%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency
			ptarget=paneln + "#" + minipaneln
			setactivesubwindow $ptarget
			
			checkbname = getSpecCerebroStr(iX, iY, "delete") // AGG 2023-08-01
			// sprintf checkbname, "d%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency
			// see if the delete check box is checked
			
			//controlinfo /W=$target $checkbname
			controlinfo $checkbname

			if(V_flag==0)
				print "Missing delete check box", graphname, checkbname, ptarget
			else
				if(V_Value==1)
					checkbox $checkbname, value=0 //,win=$target
				else
					checkbox $checkbname, value=1 //,win=$target
				endif
			endif
			i+=1
			ievent+=1
			ix+=1
		while(ix<ngx)
		iy+=1
	while(iy<ngy)
end