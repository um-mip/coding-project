#pragma rtGlobals=1		// Use modern global access method.
//#include <Resize Controls>

///////////
/// populate
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-04
/// NOTES: timeOrEvent tracks if the panel should be updated according to
/// 	- 0, a time, still plot based on events with the ptb wave
/// 	- 1, events, plot with the ptb wave
///		- 2, level crossings
///////////
function populate(variable timeOrEvent)
	SVAR paneln = g_paneln

	// Check if things are still being made
	NVAR enableHook

	if( enableHook == 0 )
		print "in populate!"
		abort
	endif
	
	// Reset the cerebro environment. Clear all graphs and minipanels
	resetCerebro(panelN)

	// Clear previous derivative waves - these prepend "d" to raw wavename
	// If the cerebro panel was made from AGG_events, this function
	// knows to look in a different place for the waves that are included
	// in the dropdown wave menu of cerebro
	killallderwaves(useWavesForCerebro = checkUseWavesForCerebro())

	// Get the raw data wave
	SVAR waven = g_waven
	Wave/Z rawdata = $waven

	// Check that the raw data wave exists
	if(WaveExists(rawdata))
		// clean up waveN from any potential quotes or oddities
		waveN = nameOfWave(rawdata)

		// Make the derivative wave
		// string derwaven = derwave(waven)
        string derwaven = waveN + returnext("derivative waveIntrinsic")
		Wave derWave = $derwaven

		// Check that everything went okay and the derivative wave exists
		if(WaveExists(derWave))
			
			// Determine what information about event timing
			// to use for plotting
			NVAR event = g_event, startTime = g_time
			
			//Wave eventTimes
			make/O eventTimes
			variable useLevels, firstEventNum
			[eventTimes, firstEventNum, useLevels] = getCerebroEventTimesWave(waveN, timeOrEvent, event, startTime)
			
			if(WaveExists(eventTimes))
				
                variable iEvent = firstEventNum, nEvents = numPnts(eventTimes)

				variable aveOnly = getAverageOnlyForCerebro()
				string aveListN = waveN + returnExt("ave list")
				Wave/Z aveListWave = $aveListN

				if(WaveExists(aveListWave))
					variable plotRaw, plotDeriv
					[plotRaw, plotDeriv] = getWhatToPlotForCerebro(panelN)

					
					variable iY = 0, iX = 0
					NVAR ngx = g_ngx, ngy = g_ngy
					do // while iY < ngy
						iX = 0
						do // while iX < ngx
							// Find the next event
							if(aveOnly>0 && useLevels == 0) // if we're only plotting events for average, and not plotting levels
								if(aveListWave[iEvent]<=0) // if the current event isn't included in average, skip it
									do
										iEvent++
									while ((iEvent < nEvents) && (aveListWave[iEvent] <= 0))
								endif
							endif

							if(iEvent < nEvents)
                                if(iY == 0 && iX == 0) // first event plotting
                                    // update cerebro panel first event and start time
                                    event = iEvent
                                    startTime = eventTimes[event]
                                endif
                                variable plotFeatures = getShowFeaturesForCerebro()
								// print "iEvent", iEvent, "iX", iX, "iY", iY
								populateIndivEvent(rawdata, derWave, eventTimes, aveListWave, panelN, iEvent, iX, iY, useLevels, plotRaw, plotDeriv, plotFeatures)
								// doupdate // shows progress
								if(useLevels && mod(iEvent, 2) == 0)
									iEvent += 2 // if using levels, add two to the eventNum, if already divisible by 2
								else
									iEvent += 1 // if not, just add 1
								endif
							endif

							iX++
						while (iX < ngx)
						iY++
					while (iY < ngy)
				else
					print "The average list wave was not found for", waven, "in populate for cerebro. Not plotting"
				endif
			else
				print "The event times wave was not found appropriately for", waven, "in populate for cerebro. Not plotting"
			endif
		else
			print "The derivative wave was not made appropriately for", waven, "in populate for cerebro. Not plotting"
		endif
	else
		print "The selected raw data wave", waven, "doesn't exist as a wave in the current data folder. Populate can't fill cerebro"
	endif
end

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
//                     getCerebroEventTimesWave               \\
///////////
/// getCerebroEventTimesWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-04
/// NOTES: Return the 
/// - eventTimes wave 
/// - firstEventNum - eventNum for the first event that should be plotted
/// 	This only changes when time is used
/// - useLevels - information about whether or not levels are used
///////////
function [Wave eventTimes, variable firstEventNum, variable useLevels] getCerebroEventTimesWave(string waveN, variable timeOrEvent, variable eventNum, variable startTime)
	// Wave extensions
	string time_ext = "_ptb", lev_ext = "_lev"
	string eventwaven = ""

	// in the process, track if levels are to be used
	useLevels = 0 // default to no

	firstEventNum = eventNum

	switch (timeOrEvent)
		case 0: // time based, but using events
			eventwaven = waven + time_ext
			Wave eventtimes = $eventwaven
			firstEventNum = getIndexClosestToValue(eventTimes, startTime)
			break
		case 1: // event based
			eventwaven = waven + time_ext
			Wave eventTimes = $eventwaven
			break
		case 2: // level based
			useLevels = 1
			eventwaven = waven + lev_ext
			Wave eventTimes = $eventwaven
			break
		default: // event based
			eventwaven = waven + time_ext
			Wave eventTimes = $eventwaven
			break
	endswitch

	return [eventTimes, firstEventNum, useLevels]
end


    ///////////
    /// getIndexClosestToValue
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-08-04
    /// NOTES: Get the point number of the event that is closest to the search time
    /// For use with populate for cerebro, but could have other use cases
    /// EDITOR: Amanda Gibson
    /// UPDATED DATE: 2023-08-14
    /// NOTES: Changed name from getEventClosestToTime to getIndexClosestToValue
    /// Going to also use this function to get for the event in the average 
    /// event index list that is closest to the listed first event
    ///////////
    function getIndexClosestToValue(wave searchWave, variable searchValue)
        variable foundIndex = 0 // default to 0
        if(WaveExists(searchWave))
            variable pntsInWave = numpnts(searchWave)
            if(pntsInWave > 0)
                variable firstValue = searchWave[0]
                variable lastValue = searchWave[pntsInWave - 1]
                if(searchValue > firstValue)
                    if(searchValue < lastValue)
                        findlevel/Q searchWave, searchValue

                        if(V_flag == 0)
                            variable xVal = V_LevelX // this will be fractional
                            foundIndex = x2pnt(searchWave, xVal) // this will find the closest point to this time
                        else
                            // This shouldn't happen because already checked these, but just in case
                            print "Couldn't find the search value between the first and last points"
                        endif
                    else
                        foundIndex = pntsInWave - 1
                        if(searchValue > lastValue)
                            print "the search value was after the last point. Setting foundIndex to last point"
                        endif
                    endif
                else
                    if(searchValue < firstValue)
                        print "the search value was before the first point. Setting foundIndex to first point"
                    endif
                endif
            endif
        else
            print "searchWave in getIndexClosestToValue doesn't exist"
        endif
        return foundIndex
    end
//                 END getCerebroEventTimesWave               \\
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\


///////////
/// getWhatToPlotForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-08
/// NOTES: Return plotRaw and plotDeriv, both 0 or 1, based on the value of the
/// raw, derivative, or both radio button in the cerebro panel
///////////
function [variable plotRaw, variable plotDeriv] getWhatToPlotForCerebro(string panelN)
	NVAR rawDerivOrBoth = g_radioval
	
	if(rawDerivOrBoth == 1 || rawDerivOrBoth == 3) // Raw or both
		plotRaw = 1
	else
		plotRaw = 0
	endif

	if(rawDerivOrBoth == 2 || rawDerivOrBoth == 3) // Derivative or both
		plotDeriv = 1
	else
		plotDeriv = 0
	endif
	// print "plotRaw", plotRaw, "plotDeriv", plotDeriv
	
	return [plotRaw, plotDeriv]
end


////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\
////                       populateIndivEvent                       \\\\
///////////
/// populateIndivEvent
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-08
/// NOTES: Populate the graph and miniPanel for each event
///////////
function populateIndivEvent(wave rawData, wave derWave, wave eventTimes, wave aveListWave, string panelN, variable iEvent, variable iX, variable iY, variable useLevels, variable plotRaw, variable plotDeriv, variable plotFeatures)
	plotIndivCerebroEvent(rawData, derWave, eventTimes, panelN, iEvent, iX, iY, plotRaw, plotDeriv, plotFeatures)

	// for now (2023-08-08), leave ave check empty and eventNum NaN if plotting levels
	// The aveListWave in indexed based on PTB iEvent number, not level event number,
	// So it's not accurate as it stands
	// Other functions that I've updated to check for eventNum in the SetVariable box
	// would also break if there's an event number listed when plotting levels
	// However, going to have to think about how prev/next work then
	// maybe when dealing will levels, still count it off of the number of plots that there are

    // 2023-08-14 - Created a function `changeCerebroPage` to figure out what the new
    // number should be depending on ave/level selections, doesn't require reading
    // the plotted event number value.
    // It could still be nice to see the plotted level number, but would need to
    // update other functions, like delete function, to explicitly check if
    // levels are plotted before deleting an event number, as it currently relies
    // on checking the eventNum variable
	if(useLevels == 0) 
		setCerebroAveCheck(aveListWave, panelN, iX, iY, iEvent)
		setGraphEventNum(iX, iY, iEvent)
	endif
end


    ///////////
    /// plotIndivCerebroEvent
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-08-08
    /// NOTES: 
    ///
    /// A note on scaling of y-axis:
    /// 	A clearer approach would be to use the auto-scale function within SetAxis
    /// 	This would look something like this:
    /// 		A=2 - Autoscale only part matching x-axis
    /// 		E=0 - don't force to include 0
    ///			N=2 - Pick nice values, ensure data is inset from the ends
    ///			setaxis/W=$target/A=2/E=0/N=2 left
    ///		Because the PTB waves can still be plotted without the raw data wave
    ///		their scaling can get odd since it may just be one event
    ///		Possible solutions to this are
    ///			- Always plot the raw data to set scales, then hide
    ///			- Get the y values manually, and set the scale
    ///		For now, I've opted for the second option, to avoid extra plotting effort
    /// EDITOR: Amanda Gibson
    /// UPDATED DATE: 2023-12-31
    /// NOTES: Add plot features option to show t50r, fall 50, fall10, and fall90
    ///////////
    function plotIndivCerebroEvent(wave rawData, wave derWave, wave eventTimes, string panelN, variable iEvent, variable iX, variable iY, variable plotRaw, variable plotDeriv, variable plotFeatures)
        string graphname = getSpecCerebroStr(iX, iY, "graph")
        string target = panelN + "#" + graphname

        string waveN = nameOfWave(rawData), derWaveN = nameOfWave(derWave)

        variable thisTime = eventTimes[iEvent]
        [variable xMin, variable xMax] = getCerebroXMinXMax(thisTime)
        [variable rawYMin, variable rawYMax] = getCerebroYMinYMax(rawData, xMin, xMax)
        [variable derivYMin, variable derivYMax] = getCerebroYMinYMax(derWave, xMin, xMax)

        // Add markers to detected events
        string pks = waven + returnExt("absolute peak"), ptb = waven+ returnExt("peak time")
        Wave wpks = $pks
        Wave wptb = $ptb
        if(WaveExists(wpks) && waveexists(wptb))
            appendtograph/W=$target wpks vs wptb
            ModifyGraph/W=$target mode($pks)=3, marker($pks) = 19, msize($pks) = 5, rgb($pks)=(0,0,55000)
        else
            print "Either the peaks wave '_pk2' or ptb wave '_ptb' don't exist for", waveN
        endif


        if(plotRaw)
            appendtograph/W=$target rawData
            ModifyGraph/W=$target rgb($waven) = (0, 0, 0) // black
        endif
        
        if(plotDeriv)
            appendtograph/W=$target/R derWave
            setaxis/W=$target right derivYMin, derivYMax // set y-axis scale
        endif

        if(plotFeatures && plotRaw) // only show if also plotting the raw
            string p50N = waven + returnExt("p50"), t50rTimeN = waveN + returnext("t50riseAtTime"), fall50N = waveN + returnExt("fall50_time")
            string p10N = waveN + returnExt("p10"), fall10N = waveN + returnExt("fall10_time")
            string p90N = waveN + returnExt("p90"), fall90N = waveN + returnExt("fall90_time")

            Wave p50W = $p50N, t50rW = $t50rTimeN, fall50W = $fall50N
            Wave p10W = $p10N, fall10W = $fall10N
            Wave p90W = $p90N, fall90W = $fall90N
            if(WaveExists(p50W) && waveexists(t50rW))
                AppendToGraph/W=$target p50W/TN=t50r vs t50rW
                ModifyGraph/W=$target mode(t50r)=3, marker(t50r)=19, msize(t50r) = 2, rgb(t50r)=(16385,49025,65535)
            endif
            if(WaveExists(p50W) && waveexists(fall50W))
                AppendToGraph/W=$target p50W/TN=fall50 vs fall50W
                ModifyGraph/W=$target mode(fall50)=3, marker(fall50)=19, msize(fall50) = 2, rgb(fall50)=(16385,49025,65535)
            endif
            if(WaveExists(p10W) && waveexists(fall10W))
                AppendToGraph/W=$target p10W/TN=fall10 vs fall10W
                ModifyGraph/W=$target mode(fall10)=3, marker(fall10)=19, msize(fall10) = 2, rgb(fall10)=(51664,44236,58982)
            endif
            if(WaveExists(p90W) && waveexists(fall90W))
                AppendToGraph/W=$target p90W/TN=fall90 vs fall90W
                ModifyGraph/W=$target mode(fall90)=3, marker(fall90)=19, msize(fall90) = 2, rgb(fall90)=(51664,44236,58982)
            endif
        endif

        // Draw indicator line
        plotCerebroIndicator(target, thisTime)

        // Modify axes
        ModifyGraph/W=$target axThick = 0, noLabel = 2 // hide axes
        setaxis/W=$target bottom, xMin, xMax // set x-axis
        setaxis/W=$target left rawYMin, rawYMax // set left (y-axis)

        // Add scale bar
        // Note function assumes that target graph is already set as active subwindow
            GetWindow/Z kwTopWin activeSW
            string currentSW = S_value
            SetActiveSubwindow $target
        
        STRUCT analysisParameters ps
        variable junk = readpanelparams2(ps) // This reads whatever is currently in the detect panel, update before populating for this cell

        variable windur, winoffset, totalDur
        windur = ps.traceduration_ms
        winoffset = ps.traceoffset_ms
        totalDur = winDur + winOffset
        variable scaleDur = totalDur / 5
        string scaleString = num2str(scaleDur * 1000) + "ms"
        fscalebar1(scaleDur,20e-12,scaleString,"20pA")
        // fscalebar1(0.02,20e-12,"20 msec","20pA")
            SetActiveSubwindow $currentSW
    end

        ///////////
        /// getCerebroXMinXMax
        /// AUTHOR: Amanda Gibson
        /// ORIGINAL DATE: 2023-08-08
        /// NOTES: Return the xMin and xMax values for each cerebro panel based off of the
        /// detection parameters and the event time
        /// EDITOR: Amanda Gibson
        /// UPDATED DATE: 2024-05-18
        /// NOTES: Fixed problem with xMax and gave option for whether or not the trace offset time should
        /// be included in the trace duration time (default is no, don't include the offset, so that the trace duration
        /// is the time after `thisTime`)
        /// Before, xMin + (winDur - winOffset) * zoom. Assuming that zoom = 1 for these examples
        /// That meant that we were actually removing two window offsets from the end of the trace that was shows in cerebro. 
        /// If thisTime was 100, trace offset was 10, and trace duration was 30, the old calc would have meant
        /// that the xmin was 90 and xmax was (90 + (30-10)) = 110, instead of 120 (offset included) or 130 (offset not included)
        ///////////
        function [variable xMin, variable xMax] getCerebroXMinXMax(variable thisTime[, variable includeOffsetInTraceDur])
            if(paramIsDefault(includeOffsetInTraceDur))
                includeOffsetInTraceDur = 0
            endif
            
            STRUCT analysisParameters ps
            variable junk = readpanelparams2(ps) // This reads whatever is currently in the detect panel, update before populating for this cell

            variable windur, winoffset

            windur = ps.traceduration_ms
            winoffset = ps.traceoffset_ms

            NVAR zoom = g_zoom // this is in cerebro panel

            // Window offset also scales with zoom
            xMin = thisTime - (winoffset) * zoom
            if(includeOffsetInTraceDur)
                xMax = xMin + (winDur) * zoom
            else
                xMax = thisTime + (winDur) * zoom
            endif

            return [xMin, xMax]
        end

        ///////////
        /// getCerebroYMinYMax
        /// AUTHOR: Amanda Gibson
        /// ORIGINAL DATE: 2023-08-08
        /// NOTES: Get the yMin and the yMax plus some buffer for the dataWave
        /// between the xMin and xMax values
        ///////////
        function[variable yMin, variable yMax] getCerebroYMinYMax(wave dataWave, variable xMin, variable xMax)
            if(WaveExists(dataWave))
                WaveStats/Q/R=(xMin, xMax)/Z dataWave
                variable dataYMin = V_min
                variable dataYMax = V_max

                variable dy = 0.05 * (yMax - yMin) // get 5% of the range
                yMin = dataYmin - dy
                yMax = dataYMax + dy
            endif
            return[yMin, yMax]
        end

        ///////////
        /// plotCerebroIndicator
        /// AUTHOR: Amanda Gibson
        /// ORIGINAL DATE: 2023-08-08
        /// NOTES: Draw a green dashed line at the time of the event within the target graph
        ///////////
        function plotCerebroIndicator(string target, variable thisTime)
            DrawAction/W=$target getgroup= indicator, delete, begininsert // the delete should be redunant, but just in case
            setdrawenv/W=$target gstart, gname = indicator
            setdrawenv/W=$target xcoord = bottom // set the x coordinators relative to named axis (x-axis)
            setdrawenv/W=$target linefgc = (0, 65535, 0) // foreground color
            setdrawenv/W=$target linethick = 2, dash = 2
            drawline/W=$target thisTime, 0, thisTime, 1
            setdrawenv/W=$target gstop
            DrawAction/W=$target endinsert
        end

    ///////////
    /// setCerebroAveCheck
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-08-08
    /// NOTES: This sets the AVE checkbox in the cerebro minipanel based on
    /// whether or not iEvent is included in the average list
    ///////////
    function setCerebroAveCheck(wave aveListWave, string panelN, variable iX, variable iY, variable iEvent)
        string miniPanelN = getSpecCerebroStr(iX, iY, "miniPanel")
        string pTarget = panelN + "#" + miniPanelN

        string aveCheckN = getSpecCerebroStr(iX, iY, "average")

        variable aveListVal = aveListWave[iEvent]

        variable incInAve = 0 // default to not include
        if(aveListVal > 0)
            incInAve = 1 // include if aveList value is greater than 0
        endif

        ControlInfo/W=$pTarget $aveCheckN
        if(V_flag==0)
            print "Missing ave check box", aveCheckN
        else
            CheckBox $aveCheckN, value = incInAve, win = $pTarget
        endif
    end

    ///////////
    /// setGraphEventNum
    /// AUTHOR: Amanda Gibson
    /// ORIGINAL DATE: 2023-08-01
    /// NOTES: set the graph event number
    ///////////
    function setGraphEventNum(variable iX, variable iY, variable eventNum)
        string eventNumStr = getSpecCerebroStr(iX, iY, "event")
        NVAR/Z/SDFR=root: eventNumVar = $eventNumStr
        eventNumVar = eventNum
    end

////                   END populateIndivEvent                       \\\\
////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\

