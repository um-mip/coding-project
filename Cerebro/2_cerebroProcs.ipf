////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	Window Hook Handler

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
Function CerebroHook(s)
    STRUCT WMWinHookStruct &s
    NVAR g_zoom = g_zoom
    NVAR enableHook = enableHook
    SVAR g_waven = g_waven

    if( enableHook == 1 ) // hook is disabled during construction

        Variable hookResult = 0 // 0 if we do not handle event, 1 if we handle it.
        string panelname="Cerebro", minipaneln = "", ptarget
        
        //print "In cerebroHook handler", s.eventcode, g_Waven
        switch(s.eventCode)
        case 2: // kill
            print "killing cerebro and der wave", g_waven

			ControlInfo/W=$(panelName+"#ctrlPanel") detwave

			string wavesInCerebro = S_userData
			variable err
			Wave/Wave wavesToAvg = ListToWaveRefWave(wavesInCerebro, 0); err = GetRTError(1) // handle my own error
			if(err != 0)
				print "One of the detect waves doesn't exist"
				err = GetRTError(1)
			else
				recalculateAverages5(wavesToAvg)
			endif



			killCerebroGraphsAndPanels(panelname) // kill graphs and panels first, so that the der waves can be deleted
            killAllDerWaves(useWavesForCerebro = checkUseWavesForCerebro())
            break
        case 3: // mouse down toggles the DELETE checkbox
            //print s.winName
            string activesubwindow = "",buttonname=""
            activesubwindow = s.winName
            
            // need to extract subwindow from host#subwin
            string cerebroType, cerebroWinStr
            [cerebroType, cerebroWinStr] = getCerebroWinStrFromSubWin(activesubwindow)

            if(stringmatch(cerebroType, "graph"))
                if(strlen(cerebroWinStr)==4) // doublecheck that format makes sense
                    toggleCerebroDeleteCheck(panelname, cerebroWinStr)
                endif
            endif
            break
        case 6: // Window resize
            // handle the graph updates
            break
        case 11: // Keyboard event
            //print s.keycode
            switch (s.keycode)
                case 28:
                    //	Print "Left arrow key pressed."
                    cerebroUpdateButt("ButtPrevious")
                    hookResult = 1
                    break
                case 29:
                    //	Print "Right arrow key pressed."
                    cerebroUpdateButt("ButtNext")
                    hookResult = 1
                    break
                case 30:
                    //	Print "Up arrow key pressed."
                    hookResult = 1
                    // if(g_zoom<(9.9))
                    // 	g_zoom+=0.1
                    // else
                    // 	g_zoom+=1
                    // endif
                    g_zoom*=1.1
                    
                    // print g_zoom
                    break
                case 31:
                    //	Print "Down arrow key pressed."
                    hookResult = 1
                    // if(g_zoom>=0.2)
                    // 	g_zoom-=0.1
                    // else
                    // 	g_zoom-=0.01
                    // endif
                    g_zoom*=0.9
                    // print g_zoom
                    break
                case 97:
                    // AGG 2023-08-03, also don't know what this key is supposed to be
                    //select all
                    cerebroSelectAll()
                    break
                case 100: // AGG 2023-08-03: I don't see this keycode in Igor list anymore, not sure what was intended
                    // case 8 is delete (backspace) and 127 is forwardDelete
                    cerebroUpdateButt("buttDelete")
                    hookresult = 1
                    break
            endswitch
            break
        endswitch

    else
        print "in cerebroHook!", s.eventcode, enableHook
        //abort
    endif

End

	///////////
	/// toggleCerebroDeleteCheck
	/// AUTHOR: Amanda Gibson
	/// ORIGINAL DATE: 2023-08-03
	/// NOTES: Toggle the state of the cerebro delete checkbox
	/// if it's on, turn it off, if it's off turn it on
	/// This is used to support being able to click on the 
	/// graph subwindow to toggle the check, instead of having to
	/// click on the checkbox itself
	///////////
	function toggleCerebroDeleteCheck(string panelN, string cerebroWinStr)
		string buttonN = getCerebroStartStrFromName("delete") + cerebroWinStr
		string miniPanelN = getCerebroStartStrFromName("miniPanel") + cerebroWinStr
		string ptarget = panelN + "#" + miniPanelN

		ControlInfo/W=$ptarget $buttonN

		if(V_flag != 0)
			if(V_value == 0)
				checkbox $buttonN, win = $ptarget, value = 1
			else
				checkbox $buttonN, win = $ptarget, value = 0
			endif
		endif
	end


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	CEREBRO BUTTON UPDATE HANDLER

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cerebroUpdateButt(ctrlName, [checked]) : ButtonControl
	string ctrlName
	variable checked
	// get number of rows and columns (global variables)
	NVAR ngx=g_ngx, ngy=g_ngy, event=g_event, level=g_level // level not used here
	// get wave of interest
	SVAR waven = g_waven, paneln=g_paneln
	variable timeorevent=0
	// print "ctrlName", ctrlName
	strswitch(ctrlName)
	case "ButtPrevious":
		timeorevent = 1
		changeCerebroPage("backwards")
		break
	case "ButtNext":
		timeorevent=1
		changeCerebroPage("forwards")
		break
	case "eventUpdate":
		timeorevent=1
		break
	case "timeUpdate":
		timeorevent=0
		break
	case "buttDelete":
		print "inside delete function"
		cerebroDelete()
		timeorevent=1
		break
	case "buttADD":
		print "inside ADD function"
		cerebroADD() // calls addEvent(xcsr(A), waven)
		timeorevent=1
		break
	default:
		timeOrEvent=1 // default to event
		break
	endswitch
	if(getUseLevelsForCerebro() == 1) // if use levels is checked
		timeOrEvent = 2
	endif
	populate(timeorevent)
end 


///////////
/// changeCerebroPage
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-14
/// NOTES: Get the event number for going back or forward a page in cerebro
/// need to keep in mind if all events are being shown, 
/// average events only are being shown, or levels are shown
/// IMPORTANT: This directly changes the "first event" g_event value
/// that is shown in cerebro based off of the intended action
/// direction = "backwards" or "forwards"
///////////
function changeCerebroPage(string direction)
	variable newFirstEvent = 0
	// Get the raw data wave
	SVAR waven = g_waven
	Wave/Z rawdata = $waven

	if(WaveExists(rawData))
		NVAR ngx=g_ngx, ngy=g_ngy, currentFirstEvent=g_event
		
		variable numGraphs = ngx * ngy

		variable useLevels = getUseLevelsForCerebro()

		string displayType

		if(useLevels == 1)
			displayType = "levels"
		else
			variable useAverages = getAverageOnlyForCerebro()
			if(useAverages == 1)
				displayType = "average list"
			else
				displayType = "all events" // default
			endif
		endif

		
		strswitch (displayType)
			case "levels":
				string levWaveN = waveN + "_lev"
				Wave/Z levWave = $levWaveN
				if(WaveExists(levWave))
					variable numLevels = numpnts(levWave)
					strswitch (direction)
						case "backwards":
							if(mod(currentFirstEvent, 2) == 0)
								newFirstEvent = currentFirstEvent - (numGraphs * 2)
							else
								newFirstEvent = currentFirstEvent - 1 - (numGraphs * 2) // if first event not already any even number, jump back one to be safe
							endif
							if(newFirstEvent < 0)
								newFirstEvent = 0
							endif
							break
						case "forwards":
							if(mod(currentFirstEvent, 2) == 0)
								newFirstEvent = currentFirstEvent + (numGraphs * 2)
							else
								newFirstEvent = currentFirstEvent - 1 + (numGraphs * 2) // if first event not already any even number, jump back one to be safe
							endif
							if(newFirstEvent >= numLevels)
								newFirstEvent = numLevels - 2
							endif
							break
						default:
							print "direction doesn't match forwards or backwards, going back to the beginning"
							newFirstEvent = 0
							break
					endswitch
				else
					print "Can't find level wave for", waven, "within changeCerebroPage. Setting event number to 0"
					newFirstEvent = 0
				endif
				break
			case "average list":
				string aveWaveN = waveN + "_avel"
				wave/Z aveWave = $aveWaveN
				if(WaveExists(aveWave))
					make/O/L aveEventIndx
					Wave aveEventIndx = extractAveListIndices(aveWave, givenIndexWave = aveEventIndx)
					variable closestEventIndx = getIndexClosestToValue(aveEventIndx, currentFirstEvent)
					variable numEventsInAvg = numpnts(aveEventIndx)
					variable newEventIndx
					
					strswitch (direction)
						case "backwards":
							newEventIndx = closestEventIndx - numGraphs
							if(newEventIndx < 0)
								newEventIndx = 0
							endif
							break
						case "forwards":
							newEventIndx = closestEventIndx + numGraphs
							if(newEventIndx >= numEventsInAvg)
								newEventIndx = numEventsInAvg - 1
							endif
							break
						default:
							print "direction doesn't match forwards or backwards, going back to the beginning"
							newEventIndx = 0
							break
					endswitch

					newFirstEvent = aveEventIndx[newEventIndx]
					KillWaves/Z aveEventIndx // clean up
				else
					print "Can't find average wave list for", waven, "within changeCerebroPage. Setting event number to 0"
					newFirstEvent = 0
				endif
				break
			case "all events":
				string ptbWaveN = waveN + "_ptb"
				wave/Z ptbWave = $ptbWaveN
				if(WaveExists(ptbWave))
					variable numEvents = numpnts(ptbWave) // just using average to count the number of events

					strswitch (direction)
						case "backwards":
							newFirstEvent = currentFirstEvent - numGraphs
							if(newFirstEvent < 0)
								newFirstEvent = 0
							endif
							break
						case "forwards":
							newFirstEvent = currentFirstEvent + numGraphs
							if(newFirstEvent >= numEvents)
								newFirstEvent = numEvents - 1
							endif
							break
						default:
							print "direction doesn't match forwards or backwards, going back to the beginning"
							newFirstEvent = 0
							break
					endswitch
				else
					print "Can't find PTB wave for", waven, "within changeCerebroPage. Setting event number to 0"
					newFirstEvent = 0
				endif
				break
		endswitch
		
	else
		print "can't find raw data wave for", waven, "within changeCerebroPage. Setting event number to 0"
		newFirstEvent = 0
	endif

	// Update the panel value
	NVAR event = g_event
	event = newFirstEvent
	return newFirstEvent
end




////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	Pop Wave Proc POPUP PROC
// 	This function changes the events that are plotted when the selected wave in the drop box changes
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function POPWAVEPROC(s) : PopupMenuControl
	STRUCT WMPopupAction &s

	NVAR event=g_event, level=g_level
	SVAR waven = g_waven

	variable ecode = s.eventCode
	variable item = s.popNum

	waven = s.popStr // AGG - increase flexibility, get name from popup box

	if(ecode == 2)
		variable timeOrEvent = getTimeOrEventForCerebro()
		// AGG - Reset to event 0 when you change the wave selection
		event = 0 // if levels, should this be updating level?
		populate(timeorevent)
	endif

end
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	ROW COL SetVar PROC
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function svRowColProc(s) : SetVariableControl
	STRUCT WMSetVariableAction &s
	variable ecode = s.eventCode
	if((ecode>0)&&(ecode<6))
		string fullPanelName = s.win
		string hostN, subWin
		[hostN, subWin] = splitFullWindowName(fullPanelName)

		variable ctrlHeight = str2num(stringbykey("ctrlHeight", s.userdata))

		killCerebroGraphsAndPanels(hostN)
		
		graphmaker(ctrlHeight)

		variable timeOrEvent = getTimeOrEventForCerebro()
		populate(timeorevent)
	endif
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	ZOOM SetVar PROC
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function svZoomProc(s) : SetVariableControl
	STRUCT WMSetVariableAction &s

	NVAR enableHook = enableHook

	variable ecode = s.eventCode
	if((ecode>0))// &&(ecode<6)))
		if( enableHook == 0 )
			print "in svZoomProc!"
			//debugger
			//abort
		else
			variable timeOrEvent = getTimeOrEventForCerebro()
			populate(timeorevent)
		endif
	endif
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	Plot Type CHECK PROC
// 20170615 fixed windowless controls for igor7 cerebro panel; now in subwindow
// This function is the procedure that handles changes to the raw, derivative, both radio select
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function CheckPlotProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	//print s.eventcode
	if( s.eventcode>0 )
		variable chk = s.checked
		string ctrlPanelN = s.win
		string cbn = s.ctrlname
		
		NVAR g_radioval=g_radioval
		
		strswitch(cbn)
			case "checkRAW":
				g_radioval = 1
				break
			case "checkDERIV":
				g_radioval = 2
				break
			case "checkBOTH":
				g_radioval = 3
				break
		endswitch

		checkbox checkRAW, value = g_radioval==1, win=$ctrlPanelN
		checkbox checkDERIV, value = g_radioval==2, win=$ctrlPanelN
		checkbox checkBOTH, value = g_radioval==3, win=$ctrlPanelN
		
		variable timeOrEvent = getTimeOrEventForCerebro()
		populate(timeorevent)
	endif
end


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	AVE CHECK PROC
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function avecheckproc(s) : CheckBoxControl
	STRUCT WMCheckboxAction &s

	if(s.eventcode >0)
		variable chk = s.checked
		string activesubwindow = s.win
		string cbn = s.ctrlname
		
		SVAR waven = g_waven, paneln=g_paneln
		
		Wave rawWave = $(removequotes(waven))
		if(WaveExists(rawWave))
			variable timeorevent=0, ix=0, iy=0, ievent=0
			string avelist=removequotes(waven)+returnext("ave list")
			WAVE/Z w_avelist = $avelist
			Wave backIntWave = $(waven + returnExt("peak time") + "_backInt")
			Wave forIntWave = $(waven + returnExt("peak time") + "_forInt")

			variable useLevels = getUseLevelsForCerebro()
			string cerebroType, cerebroWinStr
			[cerebroType, cerebroWinStr] = getCerebroWinStrFromSubWin(activesubwindow)

			if(useLevels == 0)
				// need to extract subwindow from host#subwin
				string eventNumStr = getCerebroStartStrFromName("event") + cerebroWinStr
				NVAR/Z/SDFR=root: eventNum = $eventNumStr

				if(numType(eventNum)== 0) // if this eventNum exists
					if(chk>0)
						if(WaveExists(backIntWave) && waveExists(forIntWave))
							variable backInt = backIntWave[eventNum]
							variable forInt = forIntWave[eventNum]
							STRUCT analysisParameters ps
							variable worked = readpanelparams2(ps)
							variable traceDur = ps.traceDuration_ms	
							variable passesIntervals = (backInt > traceDur) && (forInt > traceDur)

							if(passesIntervals)
								w_avelist[eventNum] = avgDecisionVal("autoAccept") // if the interval is good, don't mark specially
							else
								w_avelist[eventNum] = avgDecisionVal("manualAccept")
							endif
						else // can't find interval waves
							w_avelist[eventNum] = avgDecisionVal("manualAccept")
						endif
					else // check is unchecked
						w_avelist[eventNum] = avgDecisionVal("manualReject")
					endif
				endif

				variable updateAvgWave = getUpdateAvgCheck()
				if(updateAvgWave)
					make/Wave/N=1/O waveToAvg = {rawWave}
					recalculateAverages5(waveToAvg)
					killwaves/Z waveToAvg
				endif
				
			else
				DoAlert/T="Levels - no average list", 0, "Because use level checkmark is selected, can't mark events for average"
				// Reset to unchecked
				string buttonN = getCerebroStartStrFromName("average") + cerebroWinStr
				string miniPanelN = getCerebroStartStrFromName("miniPanel") + cerebroWinStr
				string ptarget = panelN + "#" + miniPanelN
				ControlInfo/W=$ptarget $buttonN

				if(V_flag != 0)
					checkbox $buttonN, win = $ptarget, value = 0
				endif
			endif
		endif
	endif			
end

///////////
/// name: checkShowFeaturesProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: 
///////////
function checkShowFeaturesProc(s) : CheckBoxControl
	STRUCT WMCheckboxAction &s
	if(s.eventcode>0)
		variable timeOrEvent = getTimeOrEventForCerebro()
		populate(timeorevent)
	endif
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	LEVELCHECK PROC
// 	AGG 2023-08-08, also added ave only check to call this procedure
//	Changed name from checkLevelsProc to checkLevelsAveOnlyProc
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function checkLevelsAveOnlyProc(s) : CheckBoxControl
	STRUCT WMCheckboxAction &s
	if(s.eventcode>0)
		variable timeOrEvent = getTimeOrEventForCerebro()

		// Turn off average only if use levels
		if(timeOrEvent == 2) // Use levels
			variable aveOnly = getAverageOnlyForCerebro()
			if(aveOnly == 1)
				DoAlert/T="Unchecking ave only", 0, "Because use level checkmark is selected, average only is being unchecked"
				Checkbox CheckAveOnly value = 0, win = $s.win
			endif
		endif
		populate(timeorevent)
	endif
end