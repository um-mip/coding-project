///////////
/// checkUseWavesForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Checks for a global variable that marks whether
/// we should be using the `wavesForCerebro` wave for the list of waves in cerebro
/// instead of the `importListWave` list
///////////
function checkUseWavesForCerebro()
	variable useWavesForCerebro = 0
	NVAR/Z/SDFR=root: globalUse = useWavesForCerebro		
	if(NVAR_Exists(globalUse))
		useWavesForCerebro = globalUse
	endif
	return useWavesForCerebro
end

function/s returnSubWindow( winstr )
	string winstr // should be host#subwin

	string separator = "#"
	string subwin =""

	variable sep_loc = 0

	sep_loc = strsearch( winstr, separator, inf, 1 ) + 1 // search backwards for separator string

	if( sep_loc != -1 )
		subwin = winstr[ sep_loc, inf ]
	endif
	return subwin
end


function/S retAnalWave()
	string thewaven=""
	WAVE/T/Z  importlistwave=importlistwave
	WAVE/Z importselwave=importselwave
	thewaven = importlistwave[0]

	return thewaven
end

function/s retanalwaveS([useWavesForCerebro])
	variable useWavesForCerebro

	if(ParamIsDefault(useWavesForCerebro))
		useWavesForCerebro = 0
	endif

	string thewavens=""
	if(useWavesForCerebro == 1) // AGG - Added 2022-01-16 to not be beholden to importlistwave for cerebro 
		DFREF evDetectDFR = getEvDetectDF()
		WAVE/T/Z importlistwave=evDetectDFR:wavesForCerebro // can't have more than 128 for drop down menu
		if(!WaveExists(importListWave))
			WAVE/T/Z importlistwave=importlistwave // backup if doesn't exist yet
		endif
	else
		WAVE/T/Z importlistwave=importlistwave
	endif
	WAVE/Z importselwave=importselwave

	variable item=0, nitems=numpnts(importlistwave)
	do
		thewavens += importlistwave[item]+";"
		item+=1
	while(item<nitems)
	
	return thewavens
end

function/s retanalwaveSel(sel)
	variable sel // if sel = 0 then all waves, if sel = 1 then return selected waves
	string thewavens=""
	WAVE/T/Z  importlistwave=importlistwave
	WAVE/Z importselwave=importselwave
	variable item=0, nitems=numpnts(importlistwave)
	do
		if((sel==0)||(importselwave[item]==1))
			thewavens += importlistwave[item]+";"
		endif
		item+=1
	while(item<nitems)
	return thewavens
end

function/s ireturnext(iname) //negative numbers return full name descriptor
	variable iname
	string prefix="",extension=""

		switch(iname)
		case 1:
			extension = "_pk2"
			break
		case -1:
			extension = "absolute peak"
			break
		case 2:
			extension = "_pks"
			break
		case -2:
			extension = "relative peak"
			break
		case 3:
			extension = "_ptb"
			break
		case -3:
			extension = "peak time base"
			break
		case 4:
			extension = "_int"
			break
		case -4:
			extension = "interval"
			break
		case 5:
			extension="_der"
			break
		case -5:
			extension="derivative"
			break
		case 6:
			extension="_area"
			break
		case -6:
			extension="area"
			break
		//
		case 8:
			prefix=""
			extension="_ave"
			break
		case 9:
			prefix=""
			extension="_nave"
			break		
		case 10:
			prefix=""
			extension="_t50r"
			break		
		case 11:
			prefix=""
			extension="_1090d"
			break
		case 12:
			prefix=""
			extension="_fwhm"
			break
		case 13:
			extension="_avel"
			break
		default:
			extension="_garbage"
			break
		endswitch
	return extension
end

////// return list of selected extensions (resultsSELwave)
function/s returnExtSel()
	WAVE/T/Z r = resultswave // these are the results to summarize (columns)
	WAVE/Z rsel = resultsselwave // these are the results SELECTED
	string slist=""
	variable npnts=numpnts(resultswave),i=0,j=0,nsel=0
	do
		if(rsel[i]>0)
			slist+=returnext(r[i])+";"
		endif

		i+=1
	while(i<npnts)

	return slist
end

function/s returnFullNameFromExt(string ext)
	string fullname

	strswitch(ext)
	case "pk2":
		fullName = "absolute peak"
		break
	case "pks":
		fullName = "relative peak"
		break
	case "ptb":
		fullName = "peak time"
		break
	case "int":
		fullName = "interval"
		break
	case "der":
		fullName = "derivative"
		break
	case "dtb":
		fullName = "derivative time"
		break
	case "-deriv":
		fullName = "derivative waveIntrinsic"
		break
	case "area":
		fullName = "area"
		break
	case "evl":
		fullName = "events"
		break
	case "ave":
		fullName = "event average"
		break
	case "nave":
		fullName = "normalized average"
		break		
	case "t50r":
		fullName = "risetime"
		break		
	case "diffRiseSmth":
		fullName = "difference rise smoothed"
		break		
	case "t50rAbsTime":
		fullName = "t50riseAtTime"
		break		
	case "1090d":
		fullName = "10to90decay"
		break
	case "fwhm":
		fullName = "fwhm"
		break
	case "avel":
		fullName = "ave list"
		break
	case "naave": // base is datecodegn of first of pool
		fullName = "psnsna ave"
		break
	case "naavet": // base is datecodegn of first of pool
		fullName = "psnsna ave trunc"
		break
	case "nanave":
		fullName = "psnsna nave"
		break
	case "navar":
		fullName = "psnsna var" // time is x axis instrinsic
		break
	case "navart":
		fullName = "psnsna var trunc" // still function of time
		break
	case "naBvar":
		fullName = "psnsna binned var" // function of amplitude
		break
	case "lev":
		fullName = "levels"
		break
	case "base":
		fullName = "baseline"
		break
	case "baseStart":
		fullName = "baseline start"
		break
	case "baseEnd":
		fullName = "baseline end"
		break
	case "p50":
		fullName = "p50"
		break
	case "p10":
		fullName = "p10"
		break
	case "p90":
		fullName = "p90"
		break
	case "fall50Time":
		fullName = "fall50_time"
		break
	case "rise50Time":
		fullName = "rise50_time"
		break
	case "fall10Time":
		fullName = "fall10_time"
		break
	case "fall90Time":
		fullName = "fall90_time"
		break
	default:
		
		break
	endswitch
	return fullName
end
function/s returnext(fullname)
	string fullname
	string prefix="",extension=""

	strswitch(fullname)
	case "absolute peak":
		extension = "_pk2"
		break
	case "relative peak":
		extension = "_pks"
		break
	case "peak time":
		extension = "_ptb"
		break
	case "interval":
		extension = "_int"
		break
	case "derivative":
		extension="_der"
		break
	case "derivative time":
		extension="_dtb"
		break
	case "derivative waveIntrinsic":
		extension="-deriv"
		break
	case "area":
		extension="_area"
		break
	case "events":
		prefix = "e_"
		extension = "_evl"
		break
	case "event average":
		extension="_ave"
		break
	case "normalized average":
		extension="_nave"
		break		
	case "risetime":
		extension="_t50r"
		break		
	case "difference rise smoothed":
		extension="_diffRiseSmth"
		break		
	case "t50riseAtTime":
		extension="_t50rAbsTime"
		break		
	case "10to90decay":
		prefix=""
		extension="_1090d"
		break
	case "fwhm":
		prefix=""
		extension="_fwhm"
		break
	case "ave list":
		extension="_avel"
		break
	case "psnsna ave":
		extension="_naave" // base is datecodegn of first of pool
		break
	case "psnsna ave trunc":
		extension="_naavet" // base is datecodegn of first of pool
		break
	case "psnsna nave":
		extension="_nanave"
		break
	case "psnsna var": // time is x axis instrinsic
		extension="_navar"
		break
	case "psnsna var trunc": // still function of time
		extension="_navart"
		break
	case "psnsna binned var": // function of amplitude
		extension="_naBvar"
		break
	case "levels":
		extension="_lev"
		break
	case "baseline":
		extension="_base"
		break
	case "baseline start":
		extension="_baseStart"
		break
	case "baseline end":
		extension="_baseEnd"
		break
	case "p50":
		extension="_p50"
		break
	case "p10":
		extension="_p10"
		break
	case "p90":
		extension="_p90"
		break
	case "fall50_time":
		extension="_fall50Time"
		break
	case "rise50_time":
		extension="_rise50Time"
		break
	case "fall10_time":
		extension="_fall10Time"
		break
	case "fall90_time":
		extension="_fall90Time"
		break
	default:
		extension="_garbage"
		break
	endswitch
	return extension
end

///////////
/// getGraphEventNum
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-01
/// NOTES: Get the graph event number
///////////
function getGraphEventNum(variable iX, variable iY)
	string eventNumStr = getSpecCerebroStr(iX, iY, "event")
	NVAR/Z/SDFR=root: eventNum = $eventNumStr
	return eventNum
end



///////////
/// getCerebroStr
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-01
/// NOTES: Get the digits portion (xxyy) for the subgraphs in cerebro.
/// For example, if the graph is the 2nd column, 3rd row, it would be iX = 1, iY = 2
/// and the digits portion would be 0102
/// Used for graph window gXXYY, mini panel pXXYY, delete dXXYY, and average aXXYY
///////////
function/S getCerebroStr(variable iX, variable iY)
	string cerebroNumStr
	sprintf cerebroNumStr, "%02.f%02.f", ix, iy
	return cerebroNumStr
end

///////////
/// getSpecCerebroStr
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-01
/// NOTES: Uses getCerebroStr and then adds the appropriate start string
///////////
function/S getSpecCerebroStr(variable iX, variable iY, string strType)
	string startStr = getCerebroStartStrFromName(strType)
	
	string specStr = startStr + getCerebroStr(iX, iY)
	return specStr
end

///////////
/// getCerebroStartStrFromName
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Get the cerebro start string from the window type
///////////
function/S getCerebroStartStrFromName(string strType)
	string startStr = ""
	strswitch (strType)
		case "graph":
			startStr = "g"
			break
		case "miniPanel":
			startStr = "p"
			break
		case "delete":
			startStr = "d"
			break
		case "average":
			startStr = "a"
			break
		case "event":
			startStr = "e"
			break
		default:
			
			break
	endswitch
	return startStr
end

///////////
/// getCerebroNameFromStartStr
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Get the cerebro window type from the start string
///////////
function/S getCerebroNameFromStartStr(string startStr)
	string strType = ""
	strswitch (startStr)
		case "g":
			strType = "graph"
			break
		case "p":
			strType = "miniPanel"
			break
		case "d":
			strType = "delete"
			break
		case "a":
			strType = "average"
			break
		case "e":
			strType = "event"
			break
		default:
			
			break
	endswitch
	return strType
end

///////////
/// getCerebroCheckState
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-01
/// NOTES: Get the state of the average or delete checkbox for a graph in cerebro
///////////
function getCerebroCheckState(variable iX, variable iY, string checkType[, string panelN])
	// checkType should be either "average" or "delete"

	// Default will search for global variable 
	// within current DF that is named g_paneln
	// This should likely be "Cerebro"
	if(paramIsDefault(panelN)) 
		SVAR globalPanelN = g_paneln
		panelN = globalPanelN
	endif

	string checkbname, minipaneln, fullMiniPanelN

	minipaneln = getSpecCerebroStr(iX, iY, "miniPanel")
	checkbname = getSpecCerebroStr(iX, iY, checkType)

	fullMiniPanelN = panelN + "#" + minipaneln

	ControlInfo/W=$fullMiniPanelN $checkbname

	variable checkState = NaN
	if(V_flag==0)
		print "Missing", checkType, "check box", getSpecCerebroStr(iX, iY, "graph"), checkbname, fullMiniPanelN
	else
		checkState = V_Value
	endif

	return checkState
end

///////////
/// getTimeOrEventForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: This function will check the state of the "use levels" checkbox
/// in the cerebro control panel and return one of the following values for timeorevent
/// 1 - Use event
/// 2 - Use levels
///
/// This value is passed into populate for deciding how to fill cerebro graphs
/// Note that there is also a 0 option, which is to use time. This function does not
/// consider this, as there is not currently an option to statically indicate that you want
/// time to be the determining factor. This needs to be signalled with the cerebroUpdateButt
/// function, though at the time of writing this info on 2023-08-03, that doesn't get called
/// when time is changed.
///////////
function getTimeOrEventForCerebro()
	SVAR panelN = g_paneln
	string ctrlPanelN = panelN + "#ctrlPanel"
	
	ControlInfo/W=$ctrlPanelN CheckUseLevels
	variable timeOrEvent = 1
	if(V_flag<0)
		print "no checkUseLevels control found, defaulting to event"
	else
		if(V_value == 1)
			timeOrEvent = 2
		endif
	endif
	return timeOrEvent
end

///////////
/// getUseLevelsForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-08
/// NOTES: Get the state of the checkUseLevels checkbox for cerebro
///////////
function getUseLevelsForCerebro()
	SVAR panelN = g_paneln
	string ctrlPanelN = panelN + "#ctrlPanel"
	
	ControlInfo/W=$ctrlPanelN CheckUseLevels
	variable useLevels = 0
	if(V_flag<0)
		print "no checkUseLevels control found, defaulting to event"
	else
		if(V_value == 1)
			useLevels = 1
		endif
	endif
	return useLevels
end


///////////
/// getAverageOnlyForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-04
/// NOTES: This function will check the state of the "average only" checkbox
/// in the cerebro control panel and return one of the following values for timeorevent
/// 0 - all events
/// 1 - average only
///
/// This value is passed into populate for deciding how to fill cerebro graphs
///////////
function getAverageOnlyForCerebro()
	SVAR panelN = g_paneln
	string ctrlPanelN = panelN + "#ctrlPanel"
	
	ControlInfo/W=$ctrlPanelN CheckAveOnly
	variable aveOnly = 0
	if(V_flag<0)
		print "no checkAveOnly control found, defaulting to all"
	else
		if(V_value == 1)
			aveOnly = 1
		endif
	endif
	return aveOnly
end

///////////
/// getShowFeaturesForCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: Get the state of the checkShowFeatures checkbox for cerebro
///////////
function getShowFeaturesForCerebro()
	SVAR panelN = g_paneln
	string ctrlPanelN = panelN + "#ctrlPanel"
	
	ControlInfo/W=$ctrlPanelN checkShowFeatures
	variable showFeatures = 0
	if(V_flag<0)
		print "no checkShowFeatures control found, defaulting to show"
	else
		if(V_value == 1)
			showFeatures = 1
		endif
	endif
	return showFeatures
end

///////////
/// getUpdateAvgCheck
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: Get the state of the checkShowFeatures checkbox for cerebro
///////////
function getUpdateAvgCheck()
	SVAR panelN = g_paneln
	string ctrlPanelN = panelN + "#ctrlPanel"
	
	ControlInfo/W=$ctrlPanelN CheckUpdateAvgWave
	variable CheckUpdateAvgWave = 0
	if(V_flag<0)
		print "no checkCheckUpdateAvgWave control found, defaulting to show"
	else
		if(V_value == 1)
			CheckUpdateAvgWave = 1
		endif
	endif
	return CheckUpdateAvgWave
end


///////////
/// getCerebroWinStrFromSubWin
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Extract the xxyy digit string and the cerebro window type
/// from the full subwindow path
///////////
function [string cerebroType, string cerebroWinStr] getCerebroWinStrFromSubWin(string fullSubWinPath)
	string hostN, subWin
	[hostN, subWin] = splitFullWindowName(fullSubWinPath)
	variable len = strlen(subwin)

	cerebroType = getCerebroNameFromStartStr(subwin[0])
	cerebroWinStr = subwin[len-4, len]
	return [cerebroType, cerebroWinStr]
end


///////////
/// killCerebroGraphsAndPanels
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: This will kill the graph subwindows ("gXXYY") and mini-panel
/// subwindows ("pXXYY") in Cerebro
/// Use this before closing cerebro itself to be able to actually kill
/// the extra derivative waves that were created for plotting. Otherwise
/// the last ones will stick around
///////////
function killCerebroGraphsAndPanels(string hostN)
	string currentSubPanels = ChildWindowList(hostN)
	variable numPanels = itemsInList(currentSubPanels)
	variable iPanel = 0
	for(iPanel = 0; iPanel < numPanels; iPanel++)
		string thisPanel = StringFromList(iPanel, currentSubPanels)
		string cerebroType = getCerebroNameFromStartStr(thisPanel[0]) // check first letter
		// Delete existing graphs
		if(StringMatch(cerebroType, "graph"))
			killwindow/Z $(hostN + "#" + thisPanel)
		else
			// Delete existing minipanels
			if(StringMatch(cerebroType, "miniPanel"))
				killwindow/Z $(hostN + "#"+ thisPanel)
			endif
		endif
	endfor

	clearCerebroGuides(hostN)
end


///////////
/// clearCerebroGuides
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Remove previous guides - helpful for when remaking the graphs
///////////
function clearCerebroGuides(string panelName)
	string allGuides = GuideNameList("Cerebro", "TYPE:User")
	variable numGuides = itemsInList(allGuides)
	variable iGuide
	for(iGuide=0; iGuide<numGuides; iGuide++)
		string thisGuide = StringFromList(iGuide, allGuides)
		if(!stringmatch(thisGuide, "top")) // this is the top of the ctrlPanel, don't delete
			DefineGuide/W=$panelName $thisGuide = {}
		endif
	endfor
end

///////////
/// resetCerebro
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: This removes the traces from the cerebro graphs and resets
/// delete, average, and event number for each panel
///////////
function resetCerebro(string hostN)
	string currentSubPanels = ChildWindowList(hostN)
	variable numPanels = itemsInList(currentSubPanels)
	variable iPanel = 0
	for(iPanel = 0; iPanel < numPanels; iPanel++)
		string thisPanel = StringFromList(iPanel, currentSubPanels)
		string cerebroType, cerebroWinStr
		[cerebroType, cerebroWinStr] = getCerebroWinStrFromSubWin(hostN + "#" + thisPanel)
		// Clear graphs
		if(StringMatch(cerebroType, "graph"))
			clearDisplay(hostN + "#" + thisPanel)
		else
			if(StringMatch(cerebroType, "miniPanel"))
				// Assume there should be a matching number of graphs and checkboxes
				string deleteCheck = getCerebroStartStrFromName("delete") + cerebroWinStr
				string aveCheck = getCerebroStartStrFromName("average") + cerebroWinStr
				string eventNumStr = getCerebroStartStrFromName("event") + cerebroWinStr

				CheckBox $deleteCheck, value = 0, win = $(hostN + "#" + thisPanel)
				CheckBox $aveCheck, value = 0, win = $(hostN + "#" + thisPanel)
				NVAR/Z/SDFR=root: eventNumVar = $eventNumStr
				eventNumVar = NaN
			endif
		endif
	endfor
end

///////////
/// extractAveListIndices
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-14
/// NOTES: Extract the indices of the events that are to be included in the
/// calculation of the average event.
/// Defaults to adding "_indx" to the end of the wave name of aveList and
/// storing it in the same data folder if indexWave is not provided
///////////
function/Wave extractAveListIndices(wave aveList[, wave givenIndexWave, DFREF storageDF])
	Wave indexWave
	if(WaveExists(aveList))
		if(paramIsDefault(givenIndexWave) || !waveexists(givenIndexWave))
			string waveN
			DFREF waveDFR
			[waveN, waveDFR] = getWaveNameAndDFR(aveList)
			if(paramIsDefault(storageDF))
				storageDF = waveDFR
			endif
			make/O/L storageDF:$(waveN + "_indx")
			Wave indexWave = storageDF:$(waveN + "_indx")
		else
			Wave indexWave = givenIndexWave
		endif
		Extract/O/INDX aveList, indexWave, aveList > 0
	else
		print "Cannot find the aveList wave in extractAveListIndices"
	endif
	return indexWave
end


///////////
/// calcBackwardsInterval
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-15
/// NOTES: Returns the backwards interval wave calculated from a timeWave
/// if no intWaveN provided, function takes the basename of the timewave
/// and adds on "_int"
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-12-10
/// NOTES: Didn't update this function, but made a calculateIntervals function that does both
/// forward and backwards interval calculation. This should be the preferred function
/// moving forward
///////////
function/Wave calcBackwardsInterval(wave timeWave[, string intWaveN, DFREF storageDF])
	Wave intWave

	if(WaveExists(timeWave))
		string timeWaveN
		DFREF waveDFR
		[timeWaveN, waveDFR] = getWaveNameAndDFR(timeWave)

		string basename = getWaveBasename(timeWaveN)
		if(paramIsDefault(intWaveN))
			intWaveN = basename + "_int"
		endif
		
		if(paramIsDefault(storageDF))
			storageDF = waveDFR
		endif
		
		variable numEvents = numpnts(timeWave)


		make/O/D/N=(numEvents) storageDF:$intWaveN
		Wave/SDFR=storageDF intWave = $intWaveN
		if(numEvents > 0)
			intWave[0] = timeWave[0]
			variable iEvent
			for(iEvent=1; iEvent<numEvents; iEvent++)
				intWave[iEvent] = timeWave[iEvent] - timeWave[iEvent - 1]
			endfor
		endif
		
	else
		print "Cannot find timewave in calcBackwardsInterval"
	endif
	print intWave
	return intWave
end
