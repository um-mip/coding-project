// 2023-08-03: AGG, as far as I can tell, this isn't actually in use any more
function updateGraphPanels(wx, wy, ctrlHeight)
	variable wx, wy, ctrlheight
	NVAR gap=g_gap, ngx=g_ngx, ngy=g_ngy
	SVAR paneln=g_paneln, waven=g_waven
	
	setactivesubwindow $paneln
	
	variable gx=0, gy=0, gapx=gap, gapy=gap
	
	variable ix=0, iy=0
	variable x0=0,x1=0,y0=1,y1=0
	variable cbheight = 14, cbwidth = 20
	
	string graphname="",buttonname="",panelname=paneln,target="",  minipaneln=""
	string peaks_ext="_pk2", ptb_ext="_ptb",der_prefix="d"
	string peaksn=removequotes(waven)+peaks_ext
	string peaks_tbn=removequotes(waven)+ptb_ext
	string mywaven=removequotes(waven)
	string derwaven=der_prefix+removequotes(waven)
	string gleft,gright,gtop,gbottom
	
	WAVE/Z peaks=$peaksn
	WAVE/Z peaks_tb=$peaks_tbn
	WAVE/Z mywave=$waven
	WAVE/Z derwave = $derwaven
	
	if(!waveexists(derwave))
	// make derwave!!! and remember to clean up afterwards, you fool...
		derwaven = derwave(mywaven)
		WAVE/Z derwave = $derwaven
	endif
		
	pauseupdate
		
	gx = (wx-gapx*(ngx+1))/ngx
	gy = (wy-gapy*(ngy+1))/ngy
	
	SetDrawLayer UserBack
	//	pauseupdate

	//make guides	
	variable fraction=0
	string gnbase="UG",gn="",bottomguide=""

	//	20170207: added below line
	defineguide FTA={FT, cbheight}
	do 
		gn = gnbase+"V"+num2str(ix)
		fraction=(ix+1)/ngx
		defineguide $gn={FL,fraction,FR}
		ix+=1
	while(ix<ngx)	
	bottomguide = gnbase+"H"+num2str(ngy)
	defineguide $bottomguide={FB,-ctrlheight}
	do
		gn = gnbase+"H"+num2str(iy)
		fraction=(iy+1)/ngy
		defineguide $gn={FT,fraction,$bottomguide}
	// 20170207: added below line
		defineguide $(gn+"A")={$gn,cbheight}
		iy+=1
	while(iy<ngy)
end

///////////
/// testLevelCrossing
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-04
/// NOTES: Testing the findLevel function
/// "20220226cg1s14sw1t1_ptb" was a wave with the following peak times
/// 	31.189899
/// 	33.518002
/// 	46.0854
/// 	54.752201
/// 	61.552799
/// 	70.077904
/// 	80.491402
/// 	118.158
/// Any times before the first event (index 0) throws an error. /Q suppresses
/// Times between the first and last event, 
/// the value reported by V_LevelX is reported as a fraction of the point index
/// at which this level is crossed
/// Times after the last event will also throw an error
/// if the find level function is going to be used to determine the event number
/// then there needs to be rounding involved
/// I think that the best solution is to continue to use level crossing,
/// but get the value as the x-value and not the point value, then use x2pnt
/// to get the point value that is closest to the x-value
/// Because the x-axis of PTB shouldn't be scaled, it would probably still work 
/// with the /P flag, but don't make assumptions where assumptions don't need to be made
///////////
// function testLevelCrossing()
// 	Wave testWave = root:'20220226cg1s14sw1t1_ptb'

// 	// These will return fractional x values
// 	print "0 - no levels"
// 	findlevel /P/Q testWave, 0
// 	print "29 - no levels"
// 	findlevel /P/Q testWave, 29
// 	print "31.18 - no levels"
// 	findlevel /P/Q testWave, 31.189899
// 	print "31.19"
// 	findlevel /P testWave, 31.19
// 	print "40"
// 	findlevel /P testWave, 40
// 	print "50"
// 	findlevel /P testWave, 50
	
// 	// Get the x value first
// 	print "by x value, not point value, though here, b/c not scaled, should be the same"
// 	findlevel testWave, 50
// 	variable pnt = x2pnt(testWave, V_LevelX) // this will get the closest point value to the x value
// 	print pnt
// end

// AGG 2023-08-01 - re-wrote to be able to consider the event number
// when displaying only the average list (also works when showing all events)
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	DELETE EVENTS IN CEREBRO
//

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// function cerebroDelete()
// 	// get number of rows and columns (global variables)
// 	NVAR gap=g_gap, ngx=g_ngx, ngy=g_ngy, event=g_event, level=g_level, gtime=g_time
	
// 	// get wave of interest
// 	SVAR waven = g_waven, paneln=g_paneln
// 	STRUCT analysisParameters ps
// 	WAVE/Z eventlist=eventlist
	
// 	variable junk=0,ix=0,iy=0,ievent=0,i=0
// 	string target, graphname, checkbname, minipaneln="", ptarget=""
	
// 	junk = readpanelparams2(ps) // AGG - need to pull this not from the panel for viewing after the fact
// 	variable thissign = ps.peaksign	
	
// 	// loop over graphs in Cerebro
// 	// store which event numbers are checked
// 	make/O/N=(ngx*ngy) tobedeleted
// 	tobedeleted=-1
// 	ievent=event
// 	i=0
// 	iy=0
// 	do	
// 		ix=0
// 		do
// 			sprintf graphname, "g%02.f%02.f",ix,iy
// 			target=paneln+"#"+graphname

// 			sprintf minipaneln, "P%02.f%02.f",ix,iy
// 			ptarget=paneln + "#" + minipaneln
// 			setactivesubwindow $ptarget
			
// 			sprintf checkbname, "d%02.f%02.f",ix,iy
// 			// see if the delete check box is checked
			
// 			//controlinfo /W=$target $checkbname
// 			controlinfo $checkbname

// 			if(V_flag==0)
// 				print "Missing delete check box", graphname, checkbname, ptarget
// 			else
// 				if(V_Value==1)
// 					//print "deleting: ",ievent, waven, thissign, i, eventlist[i]
// 					//deleteevent(eventlist[i],waven,thissign)
// 					tobedeleted[i]=ievent
// 					checkbox $checkbname, value=0//,win=$target
// 				// if(event!=0)
// 				// 	event-=1
// 				// 	ix-=1
// 				// endif
// 				endif
// 			endif
// 			i+=1
// 			ievent+=1
// 			ix+=1
// 		while(ix<ngx)
// 		iy+=1
// 	while(iy<ngy)
	
// 	// repeat loop, now delete events
// 	ievent=event
// 	i=0
// 	iy=0
// 	do	
// 		ix=0
// 		do
// 			if(tobedeleted[i]>=0)
// 				print "deleting: event",ievent, "waven", waven, "sign", thissign, "i", i, "eventlist",eventlist[i], "to be deleted", tobedeleted[i]
// 				deleteevent(tobedeleted[i],waven,thissign)
// 				tobedeleted-=1
// 			endif
// 			i+=1
// 			ievent+=1
// 			ix+=1
// 		while(ix<ngx)
// 		iy+=1
// 	while(iy<ngy)
// 	recalculateaverages4( waven, "", "" )  // 20220105 from v2 to v4
// end

/// Graphmaker from JP_CEREBRO_V2-8z.ipf
/// Updated by Amanda Gibson Aug 2023. In 0_buildCerebro.ipf
// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////////////////

// // 	GRAPHMAKER

// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// function graphmaker(wx, wy,ctrlheight)
// 	variable wx, wy, ctrlheight
// 	NVAR gap=g_gap, ngx=g_ngx, ngy=g_ngy
// 	SVAR paneln=g_paneln, waven=g_waven
	
// 	setactivesubwindow $paneln
	
// 	variable gx=0, gy=0, gapx=gap, gapy=gap
	
// 	variable ix=0, iy=0
// 	variable x0=0,x1=0,y0=1,y1=0
// 	variable cbheight = 14, cbwidth = 20
	
// 	string graphname="",buttonname="",panelname=paneln,target="",  minipaneln=""
// 	string peaks_ext="_pk2", ptb_ext="_ptb",der_prefix="d"
// 	string peaksn=removequotes(waven)+peaks_ext
// 	string peaks_tbn=removequotes(waven)+ptb_ext
// 	string mywaven=removequotes(waven)
// 	string derwaven=der_prefix+removequotes(waven)
// 	string gleft,gright,gtop,gbottom
	
// 	WAVE/Z peaks=$peaksn
// 	WAVE/Z peaks_tb=$peaks_tbn
// 	WAVE/Z mywave=$waven
// 	WAVE/Z derwave = $derwaven
	
// 	if(!waveexists(derwave))
// 		// make derwave!!! and remember to clean up afterwards, you fool...
// 		derwaven = derwave(mywaven)
// 		WAVE/Z derwave = $derwaven
// 	endif
		
// 	pauseupdate
		
// 	gx = (wx-gapx*(ngx+1))/ngx
// 	gy = (wy-gapy*(ngy+1))/ngy
	
// 	SetDrawLayer UserBack
// 	//	pauseupdate

// 	//make guides	
// 	variable fraction=0
// 	string gnbase="UG",gn="",bottomguide=""

// 	//20170207: added below line
// 	defineguide FTA={FT, cbheight}
// 	variable runningGraphNum = 0
// 	do 
// 		gn = gnbase+"V"+num2str(ix)
// 		fraction=(ix+1)/ngx
// 		defineguide $gn={FL,fraction,FR}
// 		ix+=1
// 	while(ix<ngx)	
// 	bottomguide = gnbase+"H"+num2str(ngy)
// 	defineguide $bottomguide={FB,-ctrlheight}
// 	do
// 		gn = gnbase+"H"+num2str(iy)
// 		fraction=(iy+1)/ngy
// 		defineguide $gn={FT,fraction,$bottomguide}
// 		//20170207: added below line
// 		defineguide $(gn+"A")={$gn,cbheight}
// 		iy+=1
// 	while(iy<ngy)
// 	//place graphs
// 	iy=0
// 	do // loop over columns
// 		ix=0
// 		do //loop over rows
// 			x0 = gapx + ix * (gx+gapx)
// 			x1 = x0 + gx
		
// 			y0 = gapy + iy * (gy+gapy)
// 			y1 = y0 + gy
// 			//graphname = "g"+num2str(ix)+num2str(iy)

// 			graphname = getSpecCerebroStr(iX, iY, "graph") // AGG 2023-08-01
// 			// sprintf graphname, "g%02.f%02.f",ix,iy // AGG 2023-08-01 updated for consistency
			
// 			//print graphname		
// 			if(ix==0)
// 				gleft="FL"
// 				gright="UGV"+num2str(ix)
// 			else
// 				gleft="UGV"+num2str(ix-1)
// 				if(ix==(ngx-1))
// 					gright="FR"
// 				else
// 					gright="UGV"+num2str(ix)
// 				endif
// 			endif
// 			if(iy==0)
// 				gtop="FT"
// 				gbottom="UGH"+num2str(iy)
// 			else
// 				gtop="UGH"+num2str(iy-1)
// 				if(iy==(ngy-1))
// 					gbottom=bottomguide
// 				else
// 					gbottom="UGH"+num2str(iy)
// 				endif
// 			endif

// 			//20170207: added the '+ "A"' to below line
// 			Display/FG=($gleft,$(gtop+"A"),$gright,$gbottom)/N=$graphname/HOST=# mywave
// 			appendtograph peaks vs peaks_tb
// 			appendtograph/R derwave
// 			ModifyGraph hideTrace($derwaven)=1

// 			ModifyGraph mode($peaksn)=3,marker($peaksn)=19, mrkThick($peaksn)=8
// 			ModifyGraph rgb($peaksn)=(0,0,55000)	

// 			ModifyGraph margin=15
// 			ModifyGraph rgb($waven)=(0,0,0)
// 			setaxis left 0,0
// 			setaxis bottom 0,0
// 			//hide original axes
// 			modifygraph axThick=0	
// 			Modifygraph noLabel=2
// 			setdrawenv gstart, gname= indicator	
// 			//20170207: commented out below line (gives error if left in)						
// 			setdrawenv xcoord=bottom, linefgc=(0,65535,0), dash=2
// 			drawline 0,0,0,1
// 			setdrawenv gstop			
			
// 			minipaneln = getSpecCerebroStr(ix, iy, "miniPanel") // AGG 20230801, ensure consistency
// 			//20170207: added next two lines
// 			// sprintf minipaneln, "P%02.f%02.f",ix,iy // "p" for minipanel name // TD 20170207 // AGG replaced with getSpecCerebroStr
// 			NewPanel/HOST=$panelname/N=$minipaneln/W=(0,0,cbheight,cbwidth)/FG=($gleft,$gtop,$gright,$(gtop+"A"))
// 			ModifyPanel frameStyle=0
			
// 			//20170207: commented out below line (gives error if left in)
// 			//target=panelname+"#"+graphname
// 			//print target
// 			// TD 20170207
// 			//CheckBox $buttonname, WIN=$target, TITLE="DELETE",pos={0,0},size={60,14},value= 0
// 			//sprintf buttonname, "a%02.f%02.f",ix,iy  // "a" for AVERAGE
// 			//CheckBox $buttonname, WIN=$target, TITLE="AVE",pos={60,0},size={60,14},side=0,value=0,  proc=AveCheckProc
// 			buttonname = getSpecCerebroStr(ix, iy, "delete") // AGG 20230801, ensure consistency
// 			// sprintf buttonname, "d%02.f%02.f",ix,iy // "d" for DELETE // AGG replaced with getSpecCerebroStr
// 			CheckBox $buttonname, TITLE="DELETE",pos={0,0},size={60,14},value= 0

// 			buttonname = getSpecCerebroStr(ix, iy, "average") // AGG 20230801, ensure consistency
// 			// sprintf buttonname, "a%02.f%02.f",ix,iy  // "a" for AVERAGE // AGG replaced with getSpecCerebroStr
// 			CheckBox $buttonname, TITLE="AVE",pos={60,0},size={50,14},side=0,value=0,  proc=AveCheckProc

// 			// AGG 2023-08-01: Added variable to track the event number that's plotted
// 			string variableName = getSpecCerebroStr(iX, iY, "event")
// 			variable/G $variableName/N=eventGlobalVar
// 			eventGlobalVar=runningGraphNum
// 			SetVariable $variablename, TITLE="e",pos={110,0},size={80,14},value=eventGlobalVar, noedit = 1, disable = 2 // don't allow the user to edit
			
// 			SetActiveSubwindow ##
	
// 			ix+=1
// 			runningGraphNum++ // add one to running graph number
// 		while(ix<ngx)
// 		iy+=1
// 	while (iy<ngy)

// end


// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////////////////

// // 	POPULATE CEREBRO
// // timeorevent : 0 use time, 1 use event, 2 use levels

// // 20170615 moved fscalebar call to avoid empty error, no host window at edge of analysis

// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// function populate(timeorevent)
// 	variable timeorevent
// 	// get number of rows and columns (global variables)
// 	NVAR gap=g_gap, ngx=g_ngx, ngy=g_ngy, event=g_event, level=g_level, gtime=g_time, gradioval=g_radioval,zoom=g_zoom, enableHook = enableHook
	
// 	if( enableHook == 0 )
// 		print "in populate!"
// 		abort
// 	endif
	
// 	// get wave of interest
// 	SVAR waven = g_waven, paneln=g_paneln
// 	variable irow=0,nrows=ngx,icol=0,ncols=ngy
// 	string lev_ext="_lev"
// 	string levwaven = removequotes(waven)+lev_ext
// 	string peak_ext="_pk2", time_ext="_ptb", peaks_waven="",peak_timen="",dpeaks_ext="_der",dtime_ext="_dtb",dpeaks_waven="",dpeaks_timen=""
// 	string der_prefix="d",derwaven=der_prefix+removequotes(waven), eventwaven=""
	
// 	variable ievent=event,nevents=0,junk=0,i=0,ix=0,iy=0,zoomfactor=1
// 	variable windur=0,winoffset=0,newmin=0,newmax=0,ymin=0,ymax=0,dy=0,thistime=0,delta=0,mindelta=0.001
// 	string graphname="",target="", minipaneln="", ptarget=""

// 	string checkbname="",avelistn = removequotes(waven)+returnext("ave list")
	
// 	string graphwaves=""
	
// 	WAVE/Z w_avelist = $avelistn
// 	STRUCT analysisParameters ps

// 	junk = readpanelparams2(ps) // AGG - Need to pull this from the cell info folder for detect, not from panel, or update panel when view a cell for confirmation
	
// 	WAVE/Z rawdata = $waven
// 	//	WAVE derwave = $derwaven
	
// 	//	keep it clean!!!
// 	killallderwaves(useWavesForCerebro = checkUseWavesForCerebro())
// 	derwaven = derwave(waven)
// 	WAVE derwave = $derwaven
// 	//	endif	
	
// 	WAVE levwave = $levwaven
	
// 	switch(timeorevent)
// 	case 0: // time based, but using events
// 		eventwaven = waven+time_ext
// 		WAVE eventtimes = $eventwaven
// 		findlevel /Q/P eventtimes, gtime
// 		if(V_flag==1)
// 			event=0
// 		else
// 			event=V_LevelX
// 		endif
// 		break
// 	case 1: // event based
// 		eventwaven = waven+time_ext
// 		WAVE eventtimes = $eventwaven
// 		break
// 	case 2: // level based	
// 		eventwaven = levwaven
// 		WAVE eventtimes = $eventwaven		
		
		
// 		break
// 	endswitch	


// 	nevents = numpnts(eventtimes)
// 	make/O/N=(nevents) eventlist
// 	eventlist=0
			
// 	gtime = eventtimes[event]
// 	windur = ps.traceduration_ms
// 	winoffset = ps.traceoffset_ms
// 	zoomfactor = winoffset/windur
// 	variable iw=0, nt=0
	
// 	// get display parameters
// 	// loop over displays

// 	// AGG - 2023-07-31, the checkaveonly control is in the control panel,
// 	// so the info about its state wasn't found when referencing only paneln, 
// 	// which often is "Cerebro"

// 	// controlinfo /W=$paneln checkaveonly
// 	string controlPanelString = paneln + "#ctrlPanel"
// 	controlinfo /W=$controlPanelString checkaveonly
// 	variable aveonly=V_value
	
// 	pauseupdate
	
// 	iy=0
// 	do
// 		ix=0
// 		do
// 			//check that the correct wave is displayed	
// 			// if not, remove all waves and append the new ones
// 			minipaneln = getSpecCerebroStr(iX, iY, "miniPanel") // AGG 2023-08-01
// 			// sprintf minipaneln, "P%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency

// 			graphname = getSpecCerebroStr(iX, iY, "graph") // AGG 2023-08-01
// 			// sprintf graphname, "g%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency

// 			ptarget=paneln+"#"+minipaneln // 20170207 TD graphname
// 			target=paneln+"#"+ graphname
			
// 			setactivesubwindow $target
// 			graphwaves = tracenamelist("",";",1)
// 			nt=itemsinlist(graphwaves)
// 			if(nt>0)
// 				iw=0		
// 				do
// 					removefromgraph $removequotes(stringfromlist(iw,graphwaves))
// 					iw+=1
// 				while(iw<itemsinlist(graphwaves))
// 			endif

// 			setactivesubwindow $ptarget // 20170207 TD $target
// 			checkbname = getSpecCerebroStr(iX, iY, "average") // AGG 2023-08-01
// 			// sprintf checkbname, "a%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency
			
// 			controlinfo $checkbname
// 			if(V_flag==0)
// 				print "Missing ave check box", graphname, checkbname, ptarget
// 			else
// 				checkbox $checkbname, value=0
// 			endif
			
// 			if(aveonly>0)
// 				if(w_avelist[ievent]<=0)
// 					do
// 					//skip events that are not to be averaged!!
// 						ievent+=1
// 					while((ievent<nevents)&&(w_avelist[ievent]<=0))
// 					//print aveonly, w_avelist[ievent]
// 				endif
// 			endif
				
// 			if(ievent<nevents)
// 				setactivesubwindow $target			
// 				appendtograph rawdata
// 				ModifyGraph rgb($waven)=(0,0,0)

// 				appendtograph /R derwave
// 				string pks=waven+"_pk2", ptb=waven+"_ptb"
// 				WAVE wpks = $pks
// 				WAVE wptb = $ptb
// 				appendtograph wpks vs wptb
// 				ModifyGraph mode($pks)=3,marker($pks)=19, msize($pks)=5 //, mrkThick($pks)=8
// 				ModifyGraph rgb($pks)=(0,0,55000)	
// 				setaxis left 0,0
// 				setaxis bottom 0,0
// 				//hide original axes
// 				modifygraph axThick=0	
// 				Modifygraph noLabel=2
									
// 				eventlist[i]=ievent // AGG - i is never updated, always 0. So the eventList appears to just be a wave that has the number of events in the series that is currently displayed. All rows are 0, except the first row which equals the number of events
// 				thistime = eventtimes[ievent]
// 				// append trace using appropriate range	
// 				newmin = thistime-(winoffset)*zoom //*(1-zoomfactor)
// 				newmax = newmin+(windur-winoffset)*zoom
// 				if((iy==0)&&(ix==0))
// 					//print newmax-newmin
// 				endif
				
// 				wavestats /Q/R=(newmin,newmax) /Z rawdata
// 				ymin=V_min
// 				ymax=V_max
// 				dy = 0.05*(ymax-ymin)

// 				//print target				
// 				setactivesubwindow $target
				
// 				setaxis left (ymin-dy), (ymax+dy)
// 				setaxis bottom (newmin),(newmax)
				
// 				// set axis for derivative				
// 				wavestats /Q/R=(newmin,newmax) /Z derwave
// 				ymin=V_min
// 				ymax=V_max
// 				dy = 0.05*(ymax-ymin)
				
// 				setactivesubwindow $target				
// 				setaxis right (ymin-dy), (ymax+dy)

// 				// update ave check box, this determines if event will be used for averaging
// 				//get name of avelist!!
// 				setactivesubwindow $ptarget

// 				checkbname = getSpecCerebroStr(iX, iY, "average") // AGG 2023-08-01
// 				// sprintf checkbname, "a%02.f%02.f",ix,iy // AGG 2023-08-01, changed to getSpecCerebroStr for consistency
// 				controlinfo $checkbname
// 				if(V_flag==0)
// 					print "Missing ave check box", graphname, checkbname, ptarget
// 				else
// 					if(w_avelist[ievent]>0)
// 						checkbox $checkbname, value=1
// 					else
// 						checkbox $checkbname, value=0
// 					endif
						
// 				endif

// 				// AGG 2023-08-01
// 				// Need to know the event number in case the average list is plotted

// 				setGraphEventNum(iX, iY, iEvent)

// 				// AGG return here

// 				setactivesubwindow $target				
// 				switch(gradioval)
// 					case 1: // show raw data
// 						ModifyGraph hideTrace($waven)=0
// 						ModifyGraph hideTrace($derwaven)=1
// 						break
// 					case 2: // show deriv
// 						ModifyGraph hideTrace($waven)=1
// 						ModifyGraph hideTrace($derwaven)=0
// 						break
// 					case 3: // show deriv
// 						ModifyGraph hideTrace($waven)=0
// 						ModifyGraph hideTrace($derwaven)=0
// 						break
// 				endswitch
				
// 				// draw indicator line
// 				setactivesubwindow $target				

// 				DrawAction getgroup= indicator, delete, begininsert
// 				setdrawenv gstart, gname= indicator							
// 				setdrawenv xcoord=bottom
// 				setdrawenv linefgc= (0,65535,0)
// 				setdrawenv linethick= 2, dash=2
// 				drawline thistime,0,thistime,1
// 				setdrawenv gstop
// 				DrawAction endinsert
// 				fscalebar1(0.02,20e-12,"20 msec","20pA")
// 				//i+=1
// 				if((timeorevent==2)&&(mod(ievent,2)==0))
// 					ievent+=2
// 				else
// 					ievent+=1
// 				endif
// 			else
// 				//sprintf graphname, "g%02.f%02.f",ix,iy
// 				//target=paneln+"#"+graphname
// 				//setactivesubwindow $target
// 				//setaxis left 0,0
// 				//setaxis bottom 0,0
				
// 				// AGG 2023-08-01
// 				// if there aren't any more events - clear event #
// 				setGraphEventNum(iX, iY, NaN)
// 			endif
// 			//fscalebar1(0.02,20e-12,"","")
// 			ix+=1
// 		while((ix<ngx))
// 		iy+=1
// 	while((iy<ngy))
// 	//fscalebar1(0.02,20e-12,"20 msec","20pA")
// 	doupdate
// end

