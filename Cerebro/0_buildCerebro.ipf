////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	PANELMAKER

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function panelmaker([useWavesForCerebro])
	variable useWavesForCerebro

	if(ParamIsDefault(useWavesForCerebro))
		useWavesForCerebro = 0
	endif
	
	PauseUpdate; Silent 1		// building window...

	variable /G enableHook = 0

	// Make cerebro host panel
	string /G g_paneln = "Cerebro"
	
	Struct ScreenSizeParameters ScrP
	InitScreenParameters(ScrP)

	variable wx = ScrP.scrwpnt * .80 // AGG based on screen size
	variable wy = ScrP.scrhpnt * .80

	NewPanel /k=1 /W=( 0, 0, wx, wy)/N=Cerebro // AGG adaptive by screensize
	SetWindow Cerebro, hook(myHook)=CerebroHook
	ShowInfo/W=Cerebro

	
	//adapting guides for controls at bottom of panel
	variable ctrlrowdelta = 20, ctrlrows = 4, ctrlheight = ctrlrows * ctrlrowdelta
	
	SetDrawLayer UserBack
	variable fraction = ( wy - ctrlheight )/ wy
	defineguide /W=Cerebro top={ FT, fraction, FB }
	
	NewPanel/HOST=Cerebro/N=ctrlpanel/W=( 0, 0,wx, ctrlheight )/FG=( FL, top, FR, FB )
	ModifyPanel frameStyle=0
	setactivesubwindow Cerebro#ctrlpanel

	// Create global variables
	variable /G g_gap = 0, g_ngx = 5, g_ngy = 5,  g_event = 0, g_level = 0, g_time = 0, g_radioval = 1, g_Zoom = 1
	
	variable xstart =5, xpos = xstart, yStart = 5, ypos = yStart
	variable cbheight = ctrlrowdelta - 1, colWidth = 100, cbwidth = colWidth - 2

	// Top Left Quadrant
	
	Button buttPrevious,pos={ xpos, ypos },size={cbwidth,cbheight},title="PREVIOUS",proc=CerebroUpdateButt
	ypos += ctrlrowdelta
	Button buttNext,pos={ xpos, ypos },size={cbwidth,cbheight},title="NEXT",proc=CerebroUpdateButt
	
	yPos = yStart
	xpos += colWidth
	SetVariable event0, pos={ xpos, ypos }, size={cbwidth,cbheight},title="First Event", value=g_event
	ypos += ctrlrowdelta
	button eventUpdate, pos={ xpos, ypos }, size={cbwidth,cbheight},title="UPDATE by event",proc=CerebroUpdateButt
	
	yPos = yStart
	xpos += colWidth
	SetVariable time0, pos={ xpos, ypos }, size={cbwidth,cbheight},title="Time", value=g_time
	ypos += ctrlrowdelta
	button timeUpdate, pos={ xpos, ypos }, size={cbwidth,cbheight},title="UPDATE by time",proc=CerebroUpdateButt
	
	// Levels - underneath top left quadrant
	xPos = xstart
	yPos = yStart + ctrlrowdelta*2
	SetVariable level0, pos={ xpos, ypos }, size={cbwidth,cbheight},title="First Level", value=g_level
	xPos += colWidth
	CheckBox CheckUseLevels,pos={ xpos, ypos },size={39,14},title="Use Levels",value= 0,mode=0,proc=checkLevelsAveOnlyProc
	
	// Wave dropdown
	xpos = xStart
	yPos = yStart + ctrlrowdelta*3
	
	// Get list of waves to be included in dropdown menu
	string wavenlist=retanalwaveS(useWavesForCerebro = useWavesForCerebro)
	print "list", wavenlist
	string/G g_waven = stringfromlist(0,wavenlist)

	String listStringSpecial
	listStringSpecial = "\"" + wavenlist + "\""
	popupmenu detwave, pos={ xpos, ypos }, size={colWidth*2 - 2,cbheight},title="Wave",value=#listStringSpecial
	popupmenu detwave, proc=popwaveproc, userdata = wavenlist
	// add the list to the popupmenu user data

	// Raw, deriv, both
	yPos = yStart
	xpos = xStart + colWidth * 3.2 // Just bump it a little. These are narrower
	CheckBox checkRAW, pos={ xpos, ypos }, size={cbwidth,cbheight},title="RAW",value= 1,mode=1,proc=CheckPlotProc
	yPos += ctrlrowdelta
	CheckBox checkDERIV,pos={ xpos, ypos },size={cbwidth,cbheight},title="DERIV",value= 0,mode=1,proc=CheckPlotProc
	yPos += ctrlrowdelta
	CheckBox checkBOTH,pos={ xpos, ypos },size={cbwidth,cbheight},title="BOTH",value= 0,mode=1,proc=CheckPlotProc
	yPos += ctrlrowdelta
	CheckBox CheckAveOnly pos={ xpos, ypos },title="ave only",size={cbwidth,cbheight},value=0, proc=checkLevelsAveOnlyProc

	// Columns, rows, zoom
	yPos = yStart
	xpos = xStart + colWidth * 4
	SetVariable setvarColumns, pos={ xpos, ypos }, size={cbwidth,cbheight},title="COLUMNS", value=g_ngx, proc=svRowColProc
	yPos += ctrlrowdelta
	SetVariable setvarRows,pos={ xpos, ypos },size={cbwidth,cbheight},title="ROWS", value=g_ngy, proc=svRowColProc // AGG - 20220116 - Switched titles of rows/columns to be accurate with later processing
	yPos += ctrlrowdelta
	SetVariable setvarZOOM,pos={xpos, ypos },size={cbwidth,cbheight},title="ZOOM", value=g_ZOOM,proc=svZoomProc
	SetVariable setvarZoom, limits={0,100,0.01}
	yPos += ctrlrowdelta
	CheckBox CheckShowFeatures pos={xpos, ypos}, size = {cbwidth, cbheight},title = "Show features", value=1, proc=checkShowFeaturesProc

	// Delete and add buttons
	xPos += colWidth
	yPos = yStart + ctrlrowdelta
	Button buttDELETE,pos={ xpos, ypos },size={cbwidth,ctrlrowdelta*2},title="DELETE",labelBack=(0,0,0)
	Button buttDELETE,font="Arial",fSize=14,valueColor=(65535,0,0), proc=CerebroUpdateButt
	yPos += ctrlrowdelta * 2
	CheckBox CheckUpdateAvgWave pos={xpos, ypos}, size = {cbwidth, cbheight}, title = "Update avg event wave", value=0
	yPos -= ctrlrowdelta * 2
	xPos += colWidth
	Button buttADD,pos={ xpos, ypos },size={cbwidth,ctrlrowdelta*2},title="ADD",labelBack=(0,0,0)
	Button buttADD,font="Arial",fSize=14,valueColor=(0,0,0), proc=CerebroUpdateButt

	
	// Add userdate to the rows and columns variables to be able to resize appropriatley later
	string sizeInfoForGraphUpdate ="ctrlheight:" + num2str(ctrlHeight)
	SetVariable setvarRowS, userdata = sizeInfoForGraphUpdate
	SetVariable setvarColumns, userdata = sizeInfoForGraphUpdate
	print ctrlheight
	graphmaker(ctrlheight)

	doupdate
	enableHook = 1
	cerebroUpdateButt("ButtPrevious") // 20170616 needs to update on first build in igor7
end


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	GRAPHMAKER

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2023-08-03
/// NOTES: Removed the plotting of traces within graphmaker, as this is handled by populate
/// Separated out the making of guides for the panel into a different function
/// and another function for placing the graph displays and mini-panels

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function graphmaker(ctrlheight)
	variable ctrlheight
	
	SVAR panelname=g_paneln
	setactivesubwindow $panelname
	
	variable cbheight = 14
	//make guides	
	makeCerebroGuides(ctrlHeight, cbheight)
	placeCerebroGraphAndMiniPanel(ctrlHeight, cbheight, panelName)
end

///////////
/// makeCerebroGuides
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: 
/// FT = top of the full cerebro panel
/// FL = left of the full cerebro panel
/// FR = right of the full cerebro panel
/// Vertical guides
/// "UGVx"
/// Horizontal guides
/// "UGHy"
/// Add an "A" at the end of the above string, which corresponds
///		to the top of the graph and bottom of the mini-panel
/// Bottom guide is still "UGHy" with y as number of rows
/// 	and is at the top of the ctrlPanel
///////////
function makeCerebroGuides(variable ctrlHeight, variable cbheight)
	NVAR ngx=g_ngx, ngy=g_ngy
	variable iX = 0, iY = 0

	SetDrawLayer UserBack

	variable fraction = 0
	string gName
	
	// Vertical guides
	do 
		gName = "UGV"+num2str(ix)
		fraction=(ix+1)/ngx
		defineguide $gName={FL,fraction,FR}
		ix+=1
	while(ix<ngx)

	// top guide for bottom of first miniPanel row
	defineguide FTA={FT, cbheight}
	
	// Bottom guide which aligns with the top of the ctrlPanel
	string bottomguide = "UGH"+num2str(ngy-1)
	defineguide $bottomguide={FB,-ctrlheight}

	// Other horizontal guides
	if(ngy > 1)
		do
			gName = "UGH"+num2str(iy)
			fraction=(iy+1)/ngy
			defineguide $gName={FT,fraction,$bottomguide}

			// Add an additional guide for the bottom of the miniPanel row and
			// the top of the corresponding graph row. 
			// Move down cbheight from current guide
			defineguide $(gName+"A")={$gName,cbheight}
			iy+=1
		while(iy<(ngy-1))
	endif
end


///////////
/// placeCerebroGraphAndMiniPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Make the graph display and mini-panels for cerebro
///////////
function placeCerebroGraphAndMiniPanel(variable ctrlHeight, variable cbheight, string panelName)
	NVAR ngx=g_ngx, ngy=g_ngy
	variable ix=0, iy=0
	
	variable cbwidth = 20

	string graphname=""
	string gleft,gright,gtop,gbottom

	variable runningGraphNum = 0

	//place graphs
	iy=0
	do // loop over columns
		ix=0
		do //loop over rows			
			graphname = getSpecCerebroStr(iX, iY, "graph")
			[gleft, gtop, gright, gbottom] = getCerebroGuides(iX, iY, "graph")
			Display/FG=($gleft,$gtop,$gright,$gbottom)/N=$graphname/HOST=# 
			ModifyGraph margin=15			
			
			makeMiniPanel(iX, iY, cbheight, cbwidth, panelName)

			ix+=1
			runningGraphNum++ // add one to running graph number
		while(ix<ngx)
		iy+=1
	while (iy<ngy)
end

///////////
/// getCerebroGuides
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: Using the index for x and y, and the type of graph/panel
/// get the strings for the appropriate guides for each side
/// The A is the bottom of mini panel and top of graph panel
/////////// 
function [string gleft, string gtop, string gright, string gbottom]  getCerebroGuides(variable iX, variable iY, string cerebroType)
	NVAR ngx=g_ngx, ngy=g_ngy
	// string gleft, gtop, gright, gbottom
	if(iX == 0)
		gleft = "FL"
		gright = "UGV" + num2str(iX)
	else
		gleft = "UGV" + num2str(iX - 1)
		if(iX == (ngx - 1))
			gright = "FR"
		else
			gright = "UGV" + num2str(iX)
		endif
	endif

	string initialGuideTop
	string initialGuideBottom
	if(iY == 0)
		initialGuideTop = "FT"
	else
		initialGuideTop = "UGH" + num2str(iY - 1)
	endif
	initialGuideBottom = "UGH" + num2str(iY)

	if(StringMatch(cerebroType, "graph"))
		gtop = initialGuideTop + "A"
		gbottom = initialGuideBottom
	else
		gtop = initialGuideTop
		gbottom = initialGuideTop + "A"
	endif
end

///////////
/// makeMiniPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-08-03
/// NOTES: 
///////////
function makeMiniPanel(variable iX, variable iY, variable cbheight, variable cbwidth, string panelName)
	string gleft,gright,gtop,gbottom

	[gleft, gtop, gright, gbottom] = getCerebroGuides(iX, iY, "miniPanel")
	
	string buttonname="",minipaneln=""
	minipaneln = getSpecCerebroStr(ix, iy, "miniPanel")
	NewPanel/HOST=$panelname/N=$minipaneln/W=(0,0,cbheight,cbwidth)/FG=($gleft,$gtop,$gright,$gbottom)
	ModifyPanel frameStyle=0

	buttonname = getSpecCerebroStr(ix, iy, "delete") // AGG 20230801, ensure consistency
	CheckBox $buttonname, TITLE="DELETE",pos={0,0},size={60,14},value= 0
	
	buttonname = getSpecCerebroStr(ix, iy, "average") // AGG 20230801, ensure consistency
	CheckBox $buttonname, TITLE="AVE",pos={60,0},size={50,14},side=0,value=0,  proc=AveCheckProc

	string variableName = getSpecCerebroStr(iX, iY, "event")
	variable/G $variableName/N=eventGlobalVar
	eventGlobalVar=NaN
	SetVariable $variablename, TITLE="e",pos={110,0},size={80,14},value=eventGlobalVar, noedit = 1, disable = 2 // don't allow the user to edit
	
	SetActiveSubwindow ##
end
