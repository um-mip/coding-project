// modified 20121210 to clarify derivative cutoff value and units
// also storing derivative cutoff in the wave note, and
// renaming data arrays based on wavename.


#pragma rtGlobals=1		// Use modern global access method.

function getparam(boxtitle,prompttext,defaultvalue)
	string boxtitle, prompttext
	variable defaultvalue
	variable input=defaultvalue
	prompt input, prompttext
	DoPrompt boxtitle, input
	return input  
end

// sets up prompt for two numeric entries, returns keyed stringlist
// DO NOT PUT COLONS IN PROMPTTEXT! 
function/s get2params(boxtitle,prompttext,defaultvalue,prompttext2,defaultvalue2)
	string boxtitle, prompttext, prompttext2
	variable defaultvalue,defaultvalue2
	variable input=defaultvalue, input2=defaultvalue2
	prompt input, prompttext
	prompt input2, prompttext2
	
	DoPrompt boxtitle, input, input2
	string output = prompttext + ":" + num2str(input) + ";" + prompttext2 + ":" + num2str(input2) + ";"
	return output  
end

///////////
/// APpropV3
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: Goal is to make action potential detection that is not dependent on top graph,
/// with additional flexibility over what gets saved and displayed where
///////////
function APpropV3(variable dthreshold[, variable smoothing, variable delay, wave/wave wavesToAnalyze, variable ap2qub, variable resample_rate, variable preStepBase, variable showTable, variable storeSpikes, variable dispSpikes])
	// this function establishes analysis parameters and a list of waves for feeding into detectAPsinWaves() 

	// establish defaults
	showTable = ParamIsDefault(showTable) ? 0 : showTable // default to not showing table results
	smoothing = ParamIsDefault(smoothing) ? 1 : smoothing  // default to smoothing the waves
	delay = ParamIsDefault(delay) ? 0 : delay // default to no delay 
	storeSpikes = ParamIsDefault(storeSpikes) ? 2 : storeSpikes // default to storing spikes and phases
	dispSpikes = ParamIsDefault(dispSpikes) ? 2 : dispSpikes // default to displaying spikes and phases
	
	if (ParamisDefault(wavesToAnalyze)) // default to analyzing all waves in top graph unless a list of waves is supplied
		wave/WAVE wavestoAnalyze = getWavesFromGraph()
	endif
	
	variable frontrange = 5e-3 // assumes units are seconds!!!  
	variable ahp_offset = 0.010 // look for afterhyperpolarization minumum within this time (s) of the action potential peak 20200428 change to 0.01 GnRH neurons
	
	// QuB stuff (consider deleting)
	variable storeAP_frontRange, storeAP_offset
	if(paramIsDefault(ap2qub))
		storeAP_frontRange = frontrange
		storeAP_offset = ahp_offset
	else // use the pre and post duration, overriding default range for AP storage
		storeAP_frontRange = ap2qub
		storeAP_offset = ap2qub
	endif
	
	// resample_rate: resample ap output to this rate in Hz
	// this can be handled by the user in a separate function 
	variable doResample
	if(paramIsDefault(resample_rate))
		doResample = 0
		resample_rate = NaN
	else
		doResample = 1
	endif
	
	variable baseStart, baseEnd 
	if(paramIsDefault(preStepBase))
		baseStart = 0
		baseEnd = 0.005
	else
		baseStart = delay - preStepBase
		baseEnd = preStepBase
	endif
	

	variable iWave = 0 
	variable nWaves = numpnts(wavesToAnalyze)

	wave thisWave = wavesToAnalyze[iWave]
	variable waveDuration 
	variable tStart0 = 0
	
	if(WaveExists(thisWave))
		[tStart0, waveDuration] = calcSeriesTimeAndDuration(thisWave)
	endif
	
	do
		Wave thisWave = wavesToAnalyze[iWave]
		if(WaveExists(thisWave))
			variable waveRelTime, waveAbsTime // add other outputs. OR export a 1row multidimensional wave to
			// plop into place of summary wave

			detectAPsInWave(thisWave, smoothing, delay, dThreshold, frontrange, ahp_offset, storeAP_frontRange, storeAP_offset, doResample, resample_rate, baseStart, baseEnd, iWave, tStart0, showTable, storeSpikes, dispSpikes)
		endif
		iWave ++ 
	while(iWave < nWaves)


	
end

///////////
/// detectAPsInWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: Detect for a given Wave
///////////
function detectAPsInWave(wave thisWave, variable smoothing, variable delay, variable minSlope, variable frontrange, variable ahp_offset, variable storeAP_frontRange, variable storeAP_offset, variable doResample, variable resample_rate, variable baseStart, variable baseEnd, variable iWave, variable tStart0, variable showTable, variable storeSpikes, variable dispSpikes)

	if (WaveExists(thisWave) == 0)
		print "In detectAPsInWave: wave does not exist"  
		return NaN
	endif
		
	string waveN
	DFREF waveDFR
	[waveN, waveDFR] = getWaveNameAndDFR(thisWave)

	variable waveDuration, tStart
	[tStart, waveDuration] = calcSeriesTimeAndDuration(thisWave)
		
	string waveNote = note(thisWave)
	variable eStart = leftx(thisWave)
	variable eEnd = rightx(thisWave)

	variable maxLevels = 6000, nProps = 13
		
	string alldataname = waveN + "_alldata_AGG"
	make/D/N=(maxLevels, nProps)/O waveDFR:$alldataname/Wave=allData
	allData = 0

	string absTimeName = waveN + "_abstime_AGG"
	make/D/N=(maxLevels)/O waveDFR:$absTimeName/Wave=event_abstime

	string relTimeName = waveN + "_reltime_AGG"
	make/D/N=(maxLevels)/O waveDFR:$relTimeName/Wave=event_reltime

	// find the baseline for this wave
	wavestats/Q/R=(basestart, baseend) thisWave
	variable baseline = V_avg
	
	// make the derivative wave
	string dWaveName = "d" + waveN
	duplicate/O thisWave, waveDFR:$dWaveName/Wave=derivWave
	differentiate derivWave

	

	// Find action potentials based on when wave crosses 0 - assumed APs cross zero
	Findlevels/R=(eStart, eEnd) thisWave, 0
	
	Duplicate/O W_FindLevels levels
	
	if (mod(numpnts(levels),2) >0)
		print ("Error: uneven number of level crossings detected")
		return NaN
	endif 
	
	
	// throw out quick events
	// check interval between upward crossings > 0.0005
	Differentiate/METH=1/EP=1 levels/D=levelCrossingIntervals // calculate interval between level crossings
	Insertpoints numpnts(levelCrossingIntervals),1, levelCrossingIntervals // last crossing (should be downward) has an interval = 0
	Make/O/D/N=(numpnts(levelCrossingIntervals)/2) downwardsDeleted = levelCrossingIntervals[p*2] // remove the intervals between downard and upward crossings 
	levels = downwardsDeleted[p/2]<0.0005 ? NaN : levels[p] // if interval between upward/downard crossing pairs is < 0.0005 ms, replace both crossings with NaN
	MatrixOP/O levels = ZapNANs(levels) // remove NaNs
	
	// throw out high amplitude events
	Make/O/D/N=(numpnts(levels)/2) upwardCrossings, downwardCrossings // easier to work with 2 waves here
	upwardCrossings = levels[p*2] // upwards are every 2 events starting at 0
	Duplicate/O/FREE levels dupwave
	Deletepoints 0,1,dupwave // downards are ever 2 events starting at 1
	downwardCrossings = dupwave[p*2]
	levels = wavemax(thiswave, upwardcrossings[floor(p/2)], downwardcrossings[floor(p/2)]) > 0.07 ?  NaN : levels[p] // if the wave crosses 0.07 V in between upward and downard crossings, change those crossings to NaN
	MatrixOP/O levels = ZaPNaNs(levels) // throw out NaNs
	 
	

	variable nLevels = numpnts(levels) // # of levels found
	variable nSpikes = nLevels / 2 // Each spike should cross once on way up and once on way down



	duplicate/O levels, levelswave // should delete this at the end
	


	print "For", waveN, nSpikes, "APs were found"

	if(nLevels<1)
		return NaN
	endif

	variable interval, APloc_corrected, APampRel, apfwhm
	variable AHPamp, AHPloc, AHPmin, dAPriseMax, threshold, APamp, countSpike, APloc
	variable iLevel = 0, prevLoc = 0, iSpike = 0, backrange = 10e-3 // assumes units are seconds!!!
	variable dpeak_time

	for(iLevel=0; iLevel<nLevels; iLevel+=2)
		variable start0 = levelswave[iLevel] - frontrange // frontrange passed to this function
		variable end0 = levelswave[iLevel] + backrange
		
		[interval, APloc_corrected, APampRel, apfwhm, AHPamp, AHPloc, AHPmin, dAPriseMax, threshold, APamp, countSpike, APloc, dpeak_time] = processLevelForAPDetect(thisWave, derivWave, iSpike, start0, end0, prevLoc, delay, smoothing, minSlope, ahp_offset, backrange)

		// TODO: May want to protect this
		prevLoc = APloc

		if(countSpike)

			storeAPwavelets(thisWave, iSpike, dpeak_time, storeSpikes, dispSpikes, storeAP_frontRange, storeAP_offset, doResample, resample_rate)
			alldata[ iSpike ][ 0 ] = iwave
			alldata[ iSpike ][ 1 ] = iSpike
			alldata[ iSpike ][ 2 ] = interval
			alldata[ iSpike ][ 3 ] = APloc_corrected
			alldata[ iSpike ][ 4 ] = APampRel
			alldata[ iSpike ][ 5 ] = APfwhm
			alldata[ iSpike ][ 6 ] = AHPamp
			alldata[ iSpike ][ 7 ] = AHPloc
			alldata[ iSpike ][ 8 ] = AHPmin
			alldata[ iSpike ][ 9 ] = dAPrisemax
			alldata[ iSpike ][ 10 ] = threshold
			alldata[ iSpike ][ 11 ] = baseline // going to be the same for all spikes in wave
			alldata[ iSpike ][ 12 ] = APamp // absolute amplitude

			event_reltime[ iSpike ] = tstart + aploc - tstart0
			event_abstime[ iSpike ] = tstart + aploc
			iSpike++
		endif
	endfor

	redimension /N=( ispike ,12) alldata // TODO - gets rid of APamp
	redimension /N=( ispike ) event_abstime
	redimension /N=( ispike ) event_reltime

	if(showTable)
		edit/k=1/W=(600,400,1200,800) event_abstime, event_reltime, alldata

		ModifyTable format(event_abstime)=8
		modifytable title[1]="dateTime"
		modifytable title[2]="exp time"
		modifytable title[3]="sweep"
		modifytable title[4]="spike"
		modifytable title[5]="interval"
		modifytable title[6]="ap latency"
		modifytable title[7]="ap amp"
		modifytable title[8]="ap width"
		modifytable title[9]="ahp amp"
		modifytable title[10]="ahp loc"
		modifytable title[11]="ahp min"
		modifytable title[12]="ap rate rise"
		modifytable title[13]="ap threshold"
		modifytable title[14]="baseline"
	endif


end

///////////
/// storeAPwavelets
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-12-13
/// NOTES: 
///////////
function storeAPwavelets(wave thisWave, variable iSpike, variable dpeak_time, variable storeSpikes, variable dispSpikes, variable storeAP_frontRange, variable storeAP_offset, variable doResample, variable resample_rate)
	// storeSpikes >0 -> store spike
	// storeSpikes >1 -> also store phase plane

	// dispSpikes >0 -> display spikes
	// dispSpikes >1 -> also display phase plane

	string apsuffix = "_AP"

	string waveN
	DFREF waveDFR

	[waveN, waveDFR] = getWaveNameAndDFR(thisWave)

	if(storeSpikes>0)
		// store spike
		string apwaven = waveN + apsuffix + "a"+ num2str( iSpike )
		duplicate/O/R=(dpeak_time-storeAP_frontRange, dpeak_time + storeAP_offset ) thisWave, waveDFR:$apwaven
		setscale/P x,  -storeAP_frontRange, deltax(waveDFR:$apwaven), waveDFR:$apwaven
		
		WAVE apw = waveDFR:$apwaven

		if ( doResample )
			resample/RATE=(resample_rate) apw
		endif

		if(dispSpikes>0)
			string appn = "g" + waveN + "_APs"
			if(iSpike==0) // make display
				display/K=1/N=$appn apw
			else
				appendtograph/W=$appn apw
			endif
		endif

		// store phase plane
		if(storeSpikes>1) 
			string ppwaven = waveN + apsuffix + "p"+ num2str( iSpike )
			duplicate/O apw, $ppwaven
			WAVE dapw = $ppwaven
			differentiate dapw
			if(dispSpikes>1)
				string ppn = "g" + waveN + "_phases"
				if(iSpike==0) // make display
					display/K=1/N=$ppn dapw vs apw

					Label left "dV / dt ( V/sec )"
					Label bottom "membrane potential (V)"
				else
					appendtograph/W=$ppn dapw vs apw
				endif
			endif
		endif // storeSpikes >1											
	endif // storeSpikes > 0
end


///////////
/// processLevelForAPDetect
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function [variable interval, variable APloc_corrected, variable APampRel, variable apfwhm, variable AHPamp, variable AHPloc, variable AHPmin, variable dAPriseMax, variable threshold, variable APamp, variable countSpike, variable aploc, variable dpeak_time] processLevelForAPDetect(wave thisWave, wave derivWave, variable iSpike, variable start0, variable end0, variable prevLoc, variable delay, variable smoothing, variable minSlope, variable ahp_offset, variable backrange)
	variable threshold_time
	variable halfmax0, firsthalf, sechalf
	variable notDuplicate, thresholdTimeExits, foundHalfMax
	countSpike = 0

	if(WaveExists(thisWave)==0 && waveexists(derivWave)==0)
		return [interval, APloc_corrected, APampRel, apfwhm, AHPamp, AHPloc, AHPmin, dAPriseMax, threshold, APamp, countSpike, aploc, dpeak_time]
	endif
	
	[notDuplicate, APloc, APloc_corrected, interval, APamp] = getAPLocMaxAndInterval(thisWave, iSpike, start0, end0, prevLoc, delay)
	
	if(notDuplicate == 0)
		print "duplicate spike", iSpike, nameOfWave(thisWave)
		return [interval, APloc_corrected, APampRel, apfwhm, AHPamp, AHPloc, AHPmin, dAPriseMax, threshold, APamp, countSpike, aploc, dpeak_time]
	endif  // reject duplicate spikes!!!
		
	[dpeak_time, dAPriseMax] = getDerivPeakandTime(derivWave, start0, end0)

	[thresholdTimeExits, threshold_time] = getDerivThresholdTime(derivWave, minSlope, smoothing, dpeak_time, start0)

	if(thresholdTimeExits)
		[threshold, APampRel, halfmax0] = getAPThresholdRelAmpHalfMax(thisWave, threshold_time, APamp)
		print "ap thresh info:", threshold, threshold_time, threshold_time - APloc

		[apfwhm, firsthalf, sechalf, foundHalfMax] = getAPHalfMaxInfo(thisWave, APloc, threshold_time, halfmax0, backrange)

		if(foundHalfMax)
			[AHPamp, AHPloc, AHPmin] = getAPAHPInfo(thisWave, aploc, ahp_offset, threshold)
		endif
	endif

	countSpike = 1 // TODO: Decide where this belongs
		
	return[interval, APloc_corrected, APampRel, apfwhm, AHPamp, AHPloc, AHPmin, dAPriseMax, threshold, APamp, countSpike, aploc, dpeak_time]
end



///////////
/// name getAPLocMaxAndInterval
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function [variable notDuplicate, variable APloc, variable APloc_corrected, variable interval, variable APamp] getAPLocMaxAndInterval(wave thisWave, variable iSpike, variable start0, variable end0, variable prevLoc, variable delay)
	
	if(WaveExists(thisWave) == 0)
		return [notDuplicate, APLoc, APloc_corrected, interval, Apamp]
	endif
	
	Wavestats/Q/R=(start0, end0) thisWave

	interval = 0
	APloc = V_maxloc

	if(APloc != prevLoc)
		notDuplicate = 1
		
		if(iSpike>0)
			interval = V_maxloc - prevLoc
			// AGG: 2022-10-27 got rid of resetting previous location to 0 here, 
			// I don't think you would ever detect duplicate spikes at next point if always set to 0 here
		endif

		APloc_corrected = APloc - delay
		APamp = V_max
	else
		notDuplicate = 0 // it is a duplicate

		APloc = NaN
		APloc_corrected = NaN
		APamp = NaN
		interval = NaN
	endif

return[notDuplicate, APloc, APloc_corrected, interval, APamp]
end

///////////
/// getDerivPeakandTime
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////


function [variable dpeak_time, variable dAPriseMax] getDerivPeakandTime(wave derivWave, variable start0, variable end0)
	if(WaveExists(derivWave))
		wavestats/Q/R=(start0, end0) derivWave
		dpeak_time = V_maxloc
		dAPriseMax = V_max
	endif
	return [dpeak_time, dAPriseMax]
end

///////////
/// getDerivThresholdTime
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function [variable timeExists, variable threshold_time] getDerivThresholdTime(wave derivWave, variable minSlope, variable smoothing, variable dpeak_time, variable start0)
	timeExists = 0
	if(WaveExists(derivWave))
		if((start0>rightx(derivWave))%|(dpeak_time>rightx(derivWave)))
			print "ran out of data--spike properties of the edge of recording", nameOfWave(derivWave)
		else
			findlevel /b=(smoothing)/Q/R=(dpeak_time, 0) derivWave, minSlope
			threshold_time = V_levelX
			//if the threshold is between the peak of the derivative and the previous spike
			// ^AGG 2022-10-27, I think that the level is actually looking back to the beginning of the derivative wave
			// not just to the time of the previous spike
			if(V_Flag==0)
				timeExists = 1
			else
				// not found
			endif
		endif
	endif
	return[timeExists, threshold_time]
end

///////////
/// getAPThresholdRelAmpHalfMax
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function[variable threshold, variable APampRel, variable halfmax0] getAPThresholdRelAmpHalfMax(wave thisWave, variable threshold_time, variable APamp)
	if(WaveExists(thisWave))
		threshold = thisWave(threshold_time) // get the value of the AP at the derivative threshold
		

		APampRel = APamp - threshold
		halfmax0 = threshold + 0.5 * APampRel
		
	endif
	return[threshold, APampRel, halfmax0]
end

///////////
/// getAPHalfMaxInfo
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function[variable apfwhm, variable firsthalf, variable sechalf, variable foundHalfMax] getAPHalfMaxInfo(wave thisWave, variable APloc, variable threshold_time, variable halfmax0, variable backrange)
	apfwhm = nan
	firsthalf = nan
	sechalf = nan
	foundHalfMax = 0

	if(WaveExists(thisWave))
		// Find the level crossing at the half-max point
		findlevel/Q/R=(APloc, threshold_time) thisWave, halfmax0

		if(V_Flag == 0) // half-max upside is found
			firsthalf = V_levelX

			findlevel/Q/R=(APloc, APloc+backrange) thisWave, halfmax0
			if(V_Flag == 0) 
				foundHalfMax = 1

				sechalf = V_levelX
				APfwhm = sechalf - firsthalf
			else // couldn't find halfmax downside
				print "couldn't locate halfmax downside for", nameOfWave(thisWave), "for AP at", aploc

				// print "couldn't locate halfmax downside--halfmax bottom, cursor a", aploc, nameOfWave(thisWave)

				// display thisWave
				// cursor a, thisWave, firsthalf
				// cursor b, thisWave, aploc
				// cursor/A=1 c, thisWave, aploc + backrange
				// setaxis bottom, firsthalf - 0.01, aploc + 0.01

				// apfwhm = nan
				// ahpamp = nan
				// ahploc = nan
				// ahpmin = nan

				// variable catchUser
				// Prompt catchUser, "Continue analysis OR stop analysis and show problem?", popup "continue;show problem"
				// DoPrompt "?" catchUser
				// print "Cursor C shows the extent of the backrange"

				// // if user hits cancel just exit 
				// if (V_Flag != 0)
				// 	return -1
				// else
				// 	if( catchuser != 1 )
				// 		abort
				// 	endif
				// endif
				
			endif
		else
			print "couldn't locate halfmax frontside for", nameOfWave(thisWave), "for AP at", aploc
			// print "couldn't locate halfmax frontside--AP location is shown with cursor b", aploc, nameOfWave(thisWave)
			// display thisWave
			// cursor b, thisWave, aploc
		endif
	endif
	
	return [apfwhm, firsthalf, sechalf, foundHalfMax]
end

///////////
/// getAPAHPinfo
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-27
/// NOTES: 
///////////
function[variable AHPamp, variable AHPloc, variable AHPmin] getAPAHPinfo(wave thisWave, variable aploc, variable ahp_offset, variable threshold)
	if(WaveExists(thisWave))
		wavestats/Q/R=(aploc, aploc+ahp_offset) thisWave
		AHPamp = V_min - threshold
		AHPloc = V_minloc - APloc
		AHPmin = V_min
	endif
	return[AHPamp, AHPloc, AHPmin]
end


// modified to add direct derivative smoothing using box2way 20240610 td
function APpropV2_2(trace, smoothing, dthreshold, [ disp, delay, scale, slist, ap2qub, resample_rate, preStepBase, dsmooth, use2ndDer, notable ] )
	
	variable trace // not trace in the usual sense, closer to "sweep"

	variable smoothing, dthreshold
	variable disp // =0 no display, =1 save chunks and display, =2 save chunks and phase planes and display
	variable delay // time of the step start
	variable scale // rescale from qub mV to patchmaster V

	string slist 	// 20190109 upgrade to analyze all sweeps in a string list, for Rudi
						//	( bypasses trace name list from top graph )
						// should be a string containing a standard string list of full and proper wavenames
	variable ap2qub // pre and post duration, overides frontrange in AP storage 
	variable resample_rate // resample ap output to this rate in Hz
	variable preStepBase // set this value to the duration of the baseline to be measured working backwards from "delay"
	variable dsmooth // if set, 2-way boxcar smoothing is employed immediately after creation of the derivative
	variable use2ndDer // set to 1 to enable
	variable notable // set to 1 to suppress table generation

	if(paramisdefault(disp) )
		disp = 0 
	endif
	
	if(paramisdefault(delay) )
		delay = 0 
	endif
	
	variable rescale = 1
	if(paramisdefault(scale))
		rescale = 1
	else
		rescale = scale
		print "XXX NOT IMPLEMENTED! appropV2_2, rescaling", rescale
	endif

	variable bs, be // base start, base end
	if(paramisdefault( preStepBase ))
		bs = 0
		be = 0.005 // default from the beginning of time
	else
		bs = delay - preStepBase
		be = preStepBase
	endif

	if(paramisdefault(dsmooth))
		dsmooth = 0 
	endif 

	if(paramisdefault(use2ndDer))
		use2ndder = 0 
	endif 

	print "appropV2_2, delay to current step:", delay


	variable minslope = dthreshold // AP threshold at 1V/sec
//	variable smoothing=10
//	print "Using minslope = ",minslope,". For GNRH action potentials, this should be 1!"

	string winl = winlist("*", ";", ""), thiswin = stringfromlist( 0, winl ) + "#G0"

	variable eoi=1

	string wlist
	if( paramisdefault( slist ) )
		wlist = tracenamelist("",";",1)
	else
		// if slist is set, it should contain a standard string list of waves to analyze
		wlist = slist
	endif

	string dwave, ddwave, dddwave, twave, tablelist
	
	string wavelet = stringfromlist( 0, wlist ), wavenote

	string apwaven = "", ppwaven = " ", APsuffix = "_AP", PPsuffix = "_PP" //followed by sweep number and ap number, e.g. _AP0112 sweep 1 ap 12

	variable nwaves = 0
	variable iwave = 0, ispike = 0, prevloc = 0
	variable threshold = 0//=$(wavelet)(xcsr(a))
	variable threshold_time = 0, dpeak_time = 0, ddpeak_time = 0, dddpeak_time = 0
	variable interval, APloc, APloc_corrected, APamp, APampRel, APFWHM
	variable AHPamp, AHPFWHM, AHPloc, AHPmin
	variable halfmax0, firsthalf, sechalf
	variable start0, end0, levels0, maxlevels=6000
	variable dAPrisemax, thresholdloc
	variable minThreshold, minThreshold_time, minThresholdV

	variable tablecount = 0
	variable frontrange = 10e-3, backrange = 20e-3 // assumes units are seconds!!! back was 3msec 20181016
	variable ahp_offset = 0.010 // 20200428 change to 0.01 GnRH neurons //20181024 //0.02 // look for the AHP minium within 20 msec of AP peak loc
	
	variable basestart = bs // 20200820 was 0 since beginning of time, 
	variable baseend = be // 20200820 was 0.005 ,
	variable baseline, estart, eend, baseline_avg = 0, baseline_count = 0
	variable Ih_offset = 0.1, myspikes = 0, thisspike = 0, nspikes = 0
	variable iprop = 0, firstspike = 0, offset = 0
	variable nprops = 17
	variable tstart=0, tstart0=0, align=0
	
	if(trace==-1)
		iwave=0
		nwaves=itemsinlist(wlist)
	else
		iwave=trace
		nwaves=1
	endif
	
	wavelet=removequotes(removequotes(stringfromlist(iwave,wlist)))
	tstart0=PMsecs2Igor(acqtime(wavelet))
	
	WAVE waveletZ = $wavelet
	wavenote=note(waveletZ)
//uses whole trace
	estart = leftx(waveletZ)
	eend = rightx(waveletZ)

	// 20190109 not sure this was ever useful	
	// showinfo
	// cursor a, $wavelet, estart
	// cursor b, $wavelet,eend
	// cursor/K c

	make /D/N=(maxlevels,nprops)/O alldata
//	WAVE twaveZ = $(twave)
	myspikes=0
	alldata=0
	make /D/N=(nwaves,nprops)/O tracedata
	tracedata=0

	make/D/O/n=(maxlevels) event_abstime
	make/D/O/n=(maxlevels) event_reltime

	make/D/O/n=(nwaves) trace_abstime
	make/D/O/n=(nwaves) trace_reltime

	do //loop over each wave in graph

		wavelet=removequotes(wavelet)
		tstart=PMsecs2Igor(acqtime(wavelet))

		WAVE waveletZ = $wavelet
		wavestats /Q/R=( basestart, baseend ) waveletZ
		baseline = V_avg
		baseline_avg += baseline
		baseline_count += 1

		twave = "t" + wavelet
		dwave = "d" + wavelet
		ddwave = "dd" + wavelet
		dddwave = "ddd" + wavelet

//		first derivative
		duplicate/O waveletz, $(dwave)
		differentiate $(dwave)
		WAVE dw = $dwave
		if (dsmooth>1)
			WAVE sdw = box2way(dw,dsmooth)
		else
			WAVE sdw = dw
			print "no smoothing of derivative"
		endif

		// second derivatve
		duplicate/O dw, $(ddwave)
		differentiate $(ddwave) // this differentiates the raw derivative, not the smoothed version
		WAVE ddw = $ddwave
		if (dsmooth>1)
			WAVE sddw = box2way(ddw,dsmooth)
		else
			WAVE sddw = ddw
			print "no smoothing of 2nd derivative"
		endif

		// third derivatve
		duplicate/O ddw, $(dddwave)
		differentiate $(dddwave) // this differentiates the raw derivative, not the smoothed version
		WAVE dddw = $dddwave
		if (dsmooth>1)
			WAVE sdddw = box2way(dddw,dsmooth)
		else
			WAVE sdddw = dddw
			print "no smoothing of 3rd derivative"
		endif

		findlevels /R=(estart,eend) /Q waveletZ, 0 //assumes action potentials cross zero!
		levels0 = V_levelsFound
		nspikes = levels0/2
		duplicate/O w_findlevels, levelswave
		duplicate/o levelswave, yaxis
		
		yaxis=0
//		appendtograph yaxis vs levelswave
		print iwave, wavelet, nspikes
		if(levels0>0)
		
			ispike=0

			prevloc=0
			interval=0
			thisspike=0
			do // loop over the spikes in current wave
				start0=levelswave[ispike]-frontrange
				end0=levelswave[ispike]+backrange
				wavestats /Q/R=(start0,end0) waveletZ
				if(ispike>0) 
					interval=V_maxloc-prevloc
					prevloc=0 // resets previous location for next spike
				endif
		
				if(V_maxloc!=prevloc)  // reject duplicate spikes!!!	
					prevloc = V_maxloc
					APloc = V_maxloc
					// 20181026 correct for delay to pulse start
					APloc_corrected = V_maxloc - delay
					APamp = V_max

					// FIND THE PEAK IN THE DERIVATIVE
					wavestats /Q/R=(start0,end0) $(dwave) // NOT SMOOTHED!!!
					dpeak_time=V_maxloc // from raw derivative (not smoothed)
					dAPriseMax=V_max // from raw derivative

					wavestats /Q/R=(start0,end0) $(ddwave) // NOT SMOOTHED!!!
					ddpeak_time=V_maxloc // from raw derivative (not smoothed)
					//dAPriseMax=V_max // from raw derivative

					wavestats /Q/R=(start0,end0) $(dddwave) // NOT SMOOTHED!!!
					dddpeak_time=V_maxloc // from raw derivative (not smoothed)
					//dAPriseMax=V_max // from raw derivative

					if((start0>rightx($(dwave)))%|(dpeak_time>rightx($(dwave))))
						print "ran out of data--spike properties of the edge of recording", wavelet
					else

//original				findlevel /b=(smoothing)/Q/R=(start0,dpeak_time) $(dwave), minslope
// now working backwards from dpeak to first crossing of minslope, search out to the previous AP
// 20210208 /b=1 does nothing! smoothing = 1 in ap2sweeplist 20210208, changed to 10 for historicity
// findlevel smoothing was 10 back in the day! see below ap_propz
// ALSO NOTE THAT FINDLEVEL /B=10 actually is boxcar 11!! 20210224

						//findlevel /b=(smoothing)/Q/R=(dpeak_time, 0) $(dwave), minslope
						// 20240610 sdw is a wave ref defined at time of derivative 

						// sdw is SMOOTHED with 2way boxcar
						if(use2ndder==0) // default is to use first derivative
							// find the threshold crossing
							findlevel /Q/R=(dpeak_time, delay) sdw, minslope
							threshold_time=V_LevelX
							if(V_flag==1) // failed to find threshold
								display sdw
								SetAxis bottom delay,delay + 0.1
								print "FAILED TO FIND THRESHOLD: ",minslope
								abort
							endif
						else // using 2nd derivative
							findlevel /EDGE=1/Q/R=(dpeak_time, delay) sddw, minslope
							threshold_time=V_LevelX
							print "USING 2ND DERIVATIVE, ADJUST THRESHOLD ACCORDINGLY!"
							if(V_flag==1) // failed to find threshold
								display sddw
								SetAxis bottom delay,delay + 0.1
								print "FAILED TO FIND THRESHOLD: ",minslope
								abort
							endif
						endif 

							// as an estimate of AP threshold, get the minimum of the derivative between the peak and the start of the stimulus
						wavestats/Q/R=(delay+0.0005,dpeak_time) sdw
						minThreshold = V_min 
						minThreshold_time = V_minloc - delay

						if(v_flag==0) //if the threshold is between the peak of the derivative and the previous spike
							
							threshold = waveletz(threshold_time)
							minthresholdV = waveletz(minThreshold_time+delay)

							//print "ap thresh info:", threshold, threshold_time, threshold_time - APloc
							APampRel = APamp - threshold
							halfmax0 = threshold + 0.5 * APampRel 
							// 20210205 was APamp, APamprel redefined 20201028 no comments from that time!!! doubles AP width using APamp (absolute)
							findlevel /Q/R=(APloc,threshold_time) waveletz, halfmax0

							//+++ find halfmax upside
							if(v_flag==0) // if level is found = halfmax updside
								
								firsthalf = V_levelX
								findlevel /Q/R=(APloc,aploc+backrange) waveletz, halfmax0

								//xxx find halfmax downside
								if(v_flag!=0)	// if level is found = halfmax downside

									print "couldn't locate halfmax downside--halfmax bottom, cursor a", aploc, wavelet
									dowindow/f $thiswin
									cursor a, $wavelet, firsthalf
									cursor b, $wavelet, aploc
									cursor/A=1 c, $wavelet, aploc+backrange 
									setaxis bottom, firsthalf - 0.01, aploc + 0.01
									apfwhm = nan
									ahpamp = nan
									ahploc = nan
									ahpmin = nan

									variable catchUser
									Prompt catchUser, "Continue analysis OR stop analysis and show problem?", popup "continue;show problem"
									DoPrompt "?" catchUser
									print "Cursor C shows the extent of the backrange"

									// if user hits cancel just exit 
									if (V_Flag != 0)
										return -1
									else
										if( catchuser != 1 )
											abort
										endif
									endif

								else // xxx backside of halfmax

									// ap fwhm
									sechalf = V_levelX
									APfwhm = sechalf - firsthalf
								
									//AHP stats
									wavestats /Q/R=(aploc,aploc+ahp_offset ) waveletz
									AHPamp=V_min-threshold
									AHPloc=V_minloc-APloc
									AHPmin=V_min

									if(disp>0)
								// store spike
								
										apwaven = wavelet + apsuffix + "s"+ num2istr( sweepnumber(wavelet)-1 )+ "a"+ num2str( myspikes )
										string dwn = apwaven  + "d"
										string ddwn = dwn + "d"
										string dddwn = ddwn + "d"

										print "graphing: ",apwaven, num2str( sweepnumber(wavelet)-1 )

										align = threshold_time // dddpeak_time // threshold_time // firsthalf // dpeak_time // threshold_time

										if ( paramisdefault( ap2qub ) ) //!! not using QuB output!
											// cutout the AP
											duplicate/O/R=(align-frontrange, align + ahp_offset ) waveletz, $apwaven
											setscale/P x,  -frontrange, deltax($apwaven), $apwaven
											// cutout the deriv
											duplicate/O/R=(align-frontrange, align + ahp_offset ) sdw, $dwn
											setscale/P x,  -frontrange, deltax($apwaven), $dwn
											wave dw = $dwn 
											// cut out the second deriv
											duplicate/O/R=(align-frontrange, align + ahp_offset ) sddw, $ddwn
											setscale/P x,  -frontrange, deltax($apwaven), $ddwn
											wave ddw = $ddwn
											// cut out the third deriv
											duplicate/O/R=(align-frontrange, align + ahp_offset ) sdddw, $dddwn
											setscale/P x,  -frontrange, deltax($apwaven), $dddwn
											wave dddw = $dddwn
										else	
											duplicate/O/R=(align-ap2qub, align + ap2qub ) waveletz, $apwaven
											setscale/P x,  -ap2qub, deltax($apwaven), $apwaven
										endif
										
										WAVE apw = $apwaven

										if ( !paramisdefault( resample_rate ) )
											resample/RATE=(resample_rate) apw
										endif

									
										if(myspikes==0) // make display
											string appn = "g" + apwaven + "0"
											display/K=1/N=$appn apw
											modifygraph/W=$appn rgb($nameofwave(apw))=(0,0,0)
										else
											appendtograph/W=$appn apw
											modifygraph/W=$appn rgb($nameofwave(apw))=(0,0,0)
										endif
										if(paramisdefault(ap2qub)) // if this is not for QuB
											appendtograph/W=$appn/R dw 
											modifygraph/W=$appn rgb($nameofwave(dw))=(0,65535,0)
											appendtograph/W=$appn/R=secondder ddw 
											if(use2ndder==1) // draw 2nd deriv cutoff
												SetDrawEnv xcoord= bottom,ycoord= secondder
												DrawLine -0.002,minslope,0.002,minslope
											else // draw first deriv cutoff
												SetDrawEnv xcoord= bottom,ycoord= right 
												DrawLine -0.002,minslope,0.002,minslope
											endif
											SetDrawEnv xcoord= bottom,ycoord= left
											DrawLine -0.002,threshold,0.002,threshold

											modifygraph/W=$appn rgb($nameofwave(ddw))=(0,0,65535)
											appendtograph/W=$appn/R=thirdder dddw 
											modifygraph/W=$appn rgb($nameofwave(dddw))=(65535,0,0)
										endif
										ModifyGraph axisEnab(left)={0,0.49},axisEnab(right)={0.5,0.9},axisEnab(secondder)={0.5,0.9},axisEnab(thirdder)={0.9,1}
										ModifyGraph freePos(secondder)={-5,bottom},freePos(thirdder)={inf,bottom}
										ModifyGraph axRGB(secondder)=(0,0,65535),tlblRGB(secondder)=(0,0,65535),alblRGB(secondder)=(0,0,65535)
										ModifyGraph axRGB(thirdder)=(65535,0,0),tlblRGB(thirdder)=(65535,0,0),alblRGB(thirdder)=(65535,0,0)
										ModifyGraph axRGB(right)=(0,65535,0),tlblRGB(right)=(0,65535,0),alblRGB(right)=(0,65535,0)

										Label left "Vm (mV)"
										Label bottom "time (ms)"
										Label right "derivative (V/s)"
										Label secondder "2nd der (V/s/s)"
										Label thirdder "3rd der (V/s/s/s)"
										SetAxis right -20,20
										SetAxis secondder -50000,50000
										SetAxis thirdder -1e+09,1e+09

										//if(myspikes==0)

										//	Legend/C/N=text0/J/F=0/A=MC "\\s(",nameofwave(apw),") Vm\r\\s(",nameofwave(dw),") first derivative"
										//	AppendText "\\s(",nameofwave(ddw),") second derivative\r\\s(",nameofwave(dddw),") third derivative"
										//endif
								// store phase plane
										if(disp>3) 
											ppwaven = wavelet + apsuffix + "s"+ num2istr( sweepnumber(wavelet) )+ "p"+ num2str( myspikes )
											duplicate/O apw, $ppwaven
											WAVE dapw = $ppwaven
											differentiate dapw
											
											WAVE sdapw = box2way(dapw,dsmooth)
											if(myspikes==0) // make display
												string ppn = "g" + ppwaven + "0"
												display/K=1/N=$ppn sdapw vs apw

												Label left "dV / dt ( V/sec )"
												Label bottom "membrane potential (V)"

											else
												appendtograph/W=$ppn sdapw vs apw
												SetAxis bottom -0.065,-0.040
												SetAxis left 0,10
											endif
										endif // disp >1											
									endif // disp > 0
								
								endif // no backside!

								// store measurements

								alldata[ myspikes ][ 0 ] = iwave
								alldata[ myspikes ][ 1 ] = thisspike
								alldata[ myspikes ][ 2 ] = interval
								alldata[ myspikes ][ 3 ] = APloc_corrected
								alldata[ myspikes ][ 4 ] = APampRel
								alldata[ myspikes ][ 5 ] = APfwhm
								alldata[ myspikes ][ 6 ] = AHPamp
								alldata[ myspikes ][ 7 ] = AHPloc
								alldata[ myspikes ][ 8 ] = AHPmin
								alldata[ myspikes ][ 9 ] = dAPrisemax
								alldata[ myspikes ][ 10 ] = threshold
								alldata[ myspikes ][ 11 ] = baseline
								alldata[ myspikes ][ 12 ] = APamp // absolute amplitude
								alldata[ myspikes ][13] = threshold_time - delay
								alldata[ myspikes ][14] = minThreshold
								alldata[ myspikes ][15] = minThreshold_time
								alldata[ myspikes ][16] = minThresholdV

								event_reltime[myspikes] = tstart + aploc - tstart0
								event_abstime[myspikes] = tstart + aploc
								iprop=2
								do
									tracedata[iwave][iprop] = tracedata[iwave][iprop] + alldata[myspikes][iprop]
									iprop+=1
								while( iprop < nprops )
								thisspike += 1
								myspikes += 1
							else
								print "couldn't locate halfmax frontside--halfmax top, cursor b", aploc, wavelet
								dowindow/f $thiswin
								cursor b, $wavelet, aploc
							endif  //+++ frontside of halfmax
						else
							print "big delay between threshold and peak", aploc, start0, dpeak_time,1000*(dpeak_time-start0), wavelet
						endif
					endif			
				else
					print "rejected duplicate spikes ",prevloc,v_maxloc, wavelet
				endif
				ispike+=2
			while(ispike<Levels0) // loop over spikes in a wave
		endif

		tracedata[iwave][0]=iwave
		tracedata[iwave][1]=nspikes
		tracedata[iwave][11] = baseline // this is the first 5ms of the current trace
		trace_reltime[iwave]=tstart-tstart0
		trace_abstime[iwave]=tstart
		
		iprop=2
		do	
			if(nspikes>0)
				offset=0
				if(iprop==2)
					offset=-1
				endif
				if( iprop != 11) 
					tracedata[iwave][iprop]/=(nspikes+offset)
				endif
				//print myspikes, iprop, tracedata[iwave][iprop]
			endif
			iprop+=1
		while(iprop<nprops)
//		print nspikes, tracedata[iwave][2],tracedata[iwave][3],tracedata[iwave][10],tracedata[iwave][11]
		iwave+=1
		wavelet=stringfromlist(iwave,wlist)
	while(iwave<nwaves)
	
	baseline_avg /= baseline_count // i made this nice calculation but i have no where to put it 20210205: who? what does it mean?
	
	redimension /N=(myspikes,nprops) alldata
	redimension /N=(myspikes) event_abstime
	redimension /N=(myspikes) event_reltime

	wavelet = removequotes(stringfromlist(0,wlist))
	string alldataname=wavelet+"_alldata"+num2str(dsmooth), tracedataname=wavelet+"_tracedata"
	duplicate/O alldata, $alldataname
	duplicate/O tracedata, $tracedataname

	string abstime = wavelet + "_abstime", reltime = wavelet + "_reltime"
	duplicate/O event_abstime, $abstime
	duplicate/O event_reltime, $reltime
	if(paramisdefault(notable))
		edit/k=1/W=(600,400,1200,800) $abstime, $reltime, $alldataname

		ModifyTable format($abstime)=8
		modifytable title[1]="dateTime"
		modifytable title[2]="exp time"
		modifytable title[3]="sweep"
		modifytable title[4]="spike"
		modifytable title[5]="interval"
		modifytable title[6]="ap latency"
		modifytable title[7]="ap amp"
		modifytable title[8]="ap width"
		modifytable title[9]="ahp amp"
		modifytable title[10]="ahp loc"
		modifytable title[11]="ahp min"
		modifytable title[12]="ap rate rise"
		modifytable title[13]="ap threshold"
		modifytable title[14]="baseline"
		modifytable title[15]="AP abs amp"
		modifytable title[16]="threshold time"
		modifytable title[17]="min d thresh"
		modifytable title[18]="min d thresh time"
		modifytable title[19]="min d thresh Vm"

		string tabstime = wavelet + "_tabstime", treltime = wavelet + "_treltime" 
		duplicate/O trace_abstime, $tabstime
		duplicate/O trace_reltime, $treltime

		edit/k=1/W=( 1200, 800, 1800, 1200 ) $tabstime, $treltime, $tracedataname

		ModifyTable format( $tabstime )=8
	endif	
end

function EventPropZ(minslope)
	variable minslope // AP threshold at 1V/sec
	print "Using minslope = ",minslope,". For action potentials, this should be 10!"

	variable eoi=1
	string mywavelist=tracenamelist("",";",1)
	string wavelet=stringfromlist(0,mywavelist),wavenote
	variable nwaves=itemsinlist(mywavelist)
	variable iwave=0,ispike=0,prevloc=0
	variable threshold//=$(wavelet)(xcsr(a))
	variable interval,APloc,APamp,APFWHM
	variable AHPamp,AHPFWHM,AHPloc,AHPmin
	variable halfmax0,firsthalf,sechalf
	variable start0,end0,levels0,maxlevels=30
	variable dAPrisemax,thresholdloc
	string dwave,twave,tablelist
	variable tablecount=0
	variable frontrange=1e-3, backrange=2e-3
	variable basestart=0, baseend=0.005,baseline,estart,eend
	variable Ih_offset=0.1,myspikes=0,thisspike=0
	
	iwave=0
	
	wavelet=removequotes(removequotes(stringfromlist(iwave,mywavelist)))
	WAVE waveletZ = $wavelet
	wavenote=note(waveletZ)

//	estart = leftx(waveletZ)
//	eend = rightx(waveletZ)

estart = xcsr(a)
eend = xcsr(b)
	print estart, eend
//	estart=epoch_start(eoi,wavenote)
//	eend=epoch_end(eoi,wavenote)

	//cursor a, $(wavelet), estart+Ih_offset
	//cursor b, $(wavelet),eend
	//makeivcsr()
	showinfo
	cursor a, $wavelet, estart
	cursor b, $wavelet,eend
	//	if(iwave==(nwaves-1))
			maxlevels=500
	//	endif
		make /N=(maxlevels,12)/O alldata
//		WAVE twaveZ = $(twave)
		myspikes=0
		alldata=nan

	do

		wavelet=removequotes(wavelet)
		WAVE waveletZ = $wavelet
		wavestats /Q/R=(basestart,baseend) waveletZ
		baseline=V_avg
		twave="t"+wavelet
		dwave="d"+wavelet
//		WAVE waveletZ = $wavelet
		duplicate/O waveletz, $(dwave)
		differentiate $(dwave)
//		findlevels /R=(estart,eend)/Q waveletZ,minslope //assumes action potentials cross zero
		findlevels /R=(estart,eend)/Q $dwave,minslope //use der threshold only!!! 20140825
		levels0=V_levelsFound
		duplicate/O w_findlevels, levelswave
		duplicate/o levelswave, yaxis
		
		yaxis=0
//		appendtograph yaxis vs levelswave
		print iwave, wavelet, levels0/2
	if(levels0>0)
		
		ispike=0

		prevloc=0
		interval=0
		thisspike=0
		do
			start0=levelswave[ispike]-frontrange
//			print start0,ispike
			end0=levelswave[ispike]+backrange
			wavestats /Q/R=(start0,end0) waveletZ
			if(ispike>0) 
				interval=V_maxloc-prevloc
			endif
		
		if(V_maxloc!=prevloc)
		// reject duplicate spikes!!!	
		
			prevloc=V_maxloc
			APloc=V_maxloc
			APamp=V_max
			wavestats /Q/R=(start0,end0) $(dwave)
			dAPriseMax=V_max
			if((start0>rightx($(dwave)))%|(V_maxloc>rightx($(dwave))))
				print "ran out of data--spike properties of the edge of recording"
			else
			//	cursor a, $(wavelet), start0
			//	cursor b, $(wavelet),v_maxloc
			//	doupdate
			//	findlevel /B=21/Q/R=(start0,V_maxloc) $(dwave), minslope
				findlevel /b=5/Q/R=(start0,V_maxloc) $(dwave), minslope

				if(v_flag==0) 
					threshold=waveletz(V_levelX)
					APamp-=threshold
					halfmax0=threshold+0.5*APamp
					findlevel /Q/R=(start0,APloc) waveletz, halfmax0
			//		cursor a, $(wavelet), start0
			//		cursor b, $(wavelet),APloc
			//		doupdate
			//		print "set range for halfmax upside"
					//find halfmax upside
					if(v_flag==0)
						firsthalf=V_levelX
						findlevel /Q/R=(APloc,end0) waveletz, halfmax0
			//			cursor a, $(wavelet), APloc
			//			cursor b, $(wavelet),end0
			//			doupdate
			//			print "set range for halfmax downside"
						//find halfmax downside
						if(v_flag==0)
							
							sechalf=V_levelX
							APfwhm=sechalf-firsthalf
							//AHP stats
							wavestats /Q/R=(aploc,aploc+0.02) waveletz
							AHPamp=V_min-threshold
							AHPloc=V_minloc-APloc
							AHPmin=V_min
							alldata[myspikes][0]=iwave
							alldata[myspikes][1]=thisspike
							alldata[myspikes][2]=interval
							alldata[myspikes][3]=aploc
							alldata[myspikes][4]=APamp
							alldata[myspikes][5]=APfwhm
							alldata[myspikes][6]=AHPamp
							alldata[myspikes][7]=AHPloc
							alldata[myspikes][8]=AHPmin
							alldata[myspikes][9]=dAPrisemax
							alldata[myspikes][10]=threshold
							alldata[myspikes][11]=baseline
							thisspike+=1
							myspikes+=1
						else
							print "couldn't locate halfmax downside--halfmax bottom"
						endif // backside of halfmax
					else
						print "couldn't locate halfmax frontside--halfmax top"
					endif  // frontside of halfmax
				else
					print "over the edge too"
				endif
			endif
//			print wavelet,iwave,ispike,interval,aploc,APamp,APfwhm,AHPamp,AHPloc
			
			else
				print "rejected duplicate spikes ",prevloc,v_maxloc
			endif
			
			ispike+=2
		while(ispike<Levels0)
	endif
		iwave+=1
		wavelet=stringfromlist(iwave,mywavelist)
	while(iwave<nwaves)

end
// findlevel smoothing was 10 back in the day! see below
function APpropZ()
	variable minslope=1 // AP threshold at 1V/sec
	variable smoothing=10
	print "Using minslope = ",minslope,". For GNRH action potentials, this should be 1!"

	variable eoi=1
	string wavelist=tracenamelist("",";",1)
	string wavelet=stringfromlist(0,wavelist),wavenote
	variable nwaves=itemsinlist(wavelist)
	variable iwave=0,ispike=0,prevloc=0
	variable threshold=0//=$(wavelet)(xcsr(a))
	variable threshold_time=0,dpeak_time=0
	variable interval,APloc,APamp,APFWHM
	variable AHPamp,AHPFWHM,AHPloc,AHPmin
	variable halfmax0,firsthalf,sechalf
	variable start0,end0,levels0,maxlevels=6000
	variable dAPrisemax,thresholdloc
	string dwave,twave,tablelist
	variable tablecount=0
	variable frontrange=3e-3, backrange=3e-3 // assumes units are seconds!!!
	variable basestart=0, baseend=0.005,baseline,estart,eend
	variable Ih_offset=0.1,myspikes=0,thisspike=0
	
	iwave=0
	
	wavelet=removequotes(removequotes(stringfromlist(iwave,wavelist)))
	WAVE waveletZ = $wavelet
	wavenote=note(waveletZ)

	estart = leftx(waveletZ)
	eend = rightx(waveletZ)
	
//	estart=epoch_start(eoi,wavenote)
//	eend=epoch_end(eoi,wavenote)

	//cursor a, $(wavelet), estart+Ih_offset
	//cursor b, $(wavelet),eend
	//makeivcsr()
	showinfo
	cursor a, $wavelet, estart
	cursor b, $wavelet,eend
	//	if(iwave==(nwaves-1))
	//		maxlevels=500
	//	endif
		make /N=(maxlevels,12)/O alldata
//		WAVE twaveZ = $(twave)
		myspikes=0
		alldata=nan

	do //loop over each wave in graph

		wavelet=removequotes(wavelet)
		WAVE waveletZ = $wavelet
		wavestats /Q/R=(basestart,baseend) waveletZ
		baseline=V_avg
		twave="t"+wavelet
		dwave="d"+wavelet
//		WAVE waveletZ = $wavelet
		duplicate/O waveletz, $(dwave)
		differentiate $(dwave)
		findlevels /R=(estart,eend)/Q waveletZ,0 //assumes action potentials cross zero!
		levels0=V_levelsFound
		duplicate/O w_findlevels, levelswave
		duplicate/o levelswave, yaxis
		
		yaxis=0
//		appendtograph yaxis vs levelswave
		print iwave, wavelet, levels0/2
	if(levels0>0)
		
		ispike=0

		prevloc=0
		interval=0
		thisspike=0
		do
			start0=levelswave[ispike]-frontrange
//			print start0,ispike
			end0=levelswave[ispike]+backrange
			wavestats /Q/R=(start0,end0) waveletZ
			if(ispike>0) 
				interval=V_maxloc-prevloc
			endif
		
		if(V_maxloc!=prevloc)
		// reject duplicate spikes!!!	
		
			prevloc=V_maxloc
			APloc=V_maxloc
			APamp=V_max
			wavestats /Q/R=(start0,end0) $(dwave)
			dpeak_time=V_maxloc
			dAPriseMax=V_max
			if((start0>rightx($(dwave)))%|(dpeak_time>rightx($(dwave))))
				print "ran out of data--spike properties of the edge of recording"
			else
				findlevel /b=(smoothing)/Q/R=(start0,dpeak_time) $(dwave), minslope
				threshold_time=V_LevelX
				if(v_flag==0) //if the threshold is between start0 and the peak of the derivative
					threshold=waveletz(V_levelX)
					APamp-=threshold
					halfmax0=threshold+0.5*APamp
					findlevel /Q/R=(start0,APloc) waveletz, halfmax0
					//+++ find halfmax upside
					if(v_flag==0)
						firsthalf=V_levelX
						findlevel /Q/R=(APloc,end0) waveletz, halfmax0
						//xxx find halfmax downside
						if(v_flag==0)
							
							sechalf=V_levelX
							APfwhm=sechalf-firsthalf
							//AHP stats
							wavestats /Q/R=(aploc,aploc+0.02) waveletz
							AHPamp=V_min-threshold
							AHPloc=V_minloc-APloc
							AHPmin=V_min
							alldata[myspikes][0]=iwave
							alldata[myspikes][1]=thisspike
							alldata[myspikes][2]=interval
							alldata[myspikes][3]=aploc
							alldata[myspikes][4]=APamp
							alldata[myspikes][5]=APfwhm
							alldata[myspikes][6]=AHPamp
							alldata[myspikes][7]=AHPloc
							alldata[myspikes][8]=AHPmin
							alldata[myspikes][9]=dAPrisemax
							alldata[myspikes][10]=threshold
							alldata[myspikes][11]=baseline
							thisspike+=1
							myspikes+=1
						else
							print "couldn't locate halfmax downside--halfmax bottom"
						endif // xxx backside of halfmax
					else
						print "couldn't locate halfmax frontside--halfmax top"
					endif  //+++ frontside of halfmax
				else
					print "big delay between threshold and peak", start0, dpeak_time,1000*(dpeak_time-start0)
				endif
			endif
//			print wavelet,iwave,ispike,interval,aploc,APamp,APfwhm,AHPamp,AHPloc
			
			else
				print "rejected duplicate spikes ",prevloc,v_maxloc
			endif
			
			ispike+=2
		while(ispike<Levels0)
	endif
		iwave+=1
		wavelet=stringfromlist(iwave,wavelist)
	while(iwave<nwaves)

end
function EventPropZ2(thresh,smth,offset)
	variable thresh, smth , offset
//	print "Using minslope = ",minslope,". For action potentials, this should be 10!",thresh

	variable eoi=1
	string wavelist=tracenamelist("",";",1)
	string wavelet=stringfromlist(0,wavelist),wavenote
	variable nwaves=itemsinlist(wavelist)
	variable iwave=0,ispike=0,prevloc=0
	variable threshold//=$(wavelet)(xcsr(a))
	variable interval,APloc,APamp,APFWHM
	variable AHPamp,AHPFWHM,AHPloc,AHPmin
	variable halfmax0,firsthalf,sechalf
	variable start0,end0,levels0,maxlevels=30
	variable dAPrisemax,thresholdloc
	string dwave,twave,tablelist
	variable tablecount=0
	variable frontrange=1e-3, backrange=2e-3
	variable basestart=0, baseend=0.005,baseline,estart,eend
	variable Ih_offset=0.1,myspikes=0,thisspike=0,ievent=0,delta_level=0, ilevel=0,delta=0
	variable thistime=0
	
	iwave=0
	
	wavelet=removequotes(removequotes(stringfromlist(iwave,wavelist)))
	WAVE waveletZ = $wavelet
	wavenote=note(waveletZ)

//	estart = leftx(waveletZ)
//	eend = rightx(waveletZ)

estart = xcsr(a)
eend = xcsr(b)
	print estart, eend
//	estart=epoch_start(eoi,wavenote)
//	eend=epoch_end(eoi,wavenote)

	//cursor a, $(wavelet), estart+Ih_offset
	//cursor b, $(wavelet),eend
	//makeivcsr()
	showinfo
	cursor a, $wavelet, estart
	cursor b, $wavelet,eend
	//	if(iwave==(nwaves-1))
			maxlevels=500
	//	endif
		make /N=(maxlevels,12)/O alldata
//		WAVE twaveZ = $(twave)
		myspikes=0
		alldata=nan

		wavelet=removequotes(wavelet)
		WAVE waveletZ = $wavelet
	
		wavestats /Q/R=(estart,eend) waveletZ
		baseline=V_avg
		twave="t"+wavelet
		dwave="d"+wavelet
//		WAVE waveletZ = $wavelet
		duplicate/O/r=(estart,eend) waveletz, mynewwave
		smooth smth, mynewwave
		differentiate mynewwave
//		display mynewwave
		findlevels /edge=1/R=(estart,eend)/Q mynewwave,thresh //assumes action potentials cross zero
		levels0=V_levelsFound
		duplicate/o w_findlevels, mylevels
		
		ievent=1
		ilevel=1
		do
			delta_level=0
			thistime=mylevels[ilevel]
			do
				delta=mylevels[ilevel]-mylevels[ilevel-1]
				delta_level+=delta
				ilevel+=1
			while((delta_level<offset)||(ilevel<levels0))
			
			print thistime,mylevels[ilevel], delta_level
			ievent+=1
		while(ilevel<levels0)
print levels0
return ievent
end


// // // // CC capacitance from AP series
function CCcap( ntraces, [ maxCurrent, slist ] )
variable ntraces // number of traces to analyze
variable maxCurrent // in pA, cut off current, analyze all current steps less than max
string slist // list of sweeps to override tracename list

variable ideltamax
if( paramisdefault( maxCurrent ) )	
	IdeltaMax = 100
else
	IdeltaMax = maxCurrent
endif

string thisTrace = "", traces , dummytrace=""

if( paramisdefault( slist ) )
	traces = tracenamelist( "", ";", 1 )
else
	traces = slist
endif	

variable i, isweep, nsweeps = itemsinlist( traces )

variable bs = 0, be = 0.01, fs = 0.01, fe = 0.51, fw = 0.0 // bs baseline start, etc fs fit start etc...
variable CCout = nan, V_FitError, V_FitQuitReason 
variable deltaI = 5 //e-12 // 5 pA steps

variable Vbase = inf, Vss =0, deltaV = nan, Rin = nan, Rin2=nan, Cap = nan, Cap2=nan, Tau = nan

string Itracen = ""
variable Ibase = 0, Idelta = 0, Ibump = 0.005

variable Ravg = 0, Tavg = 0, count = 0

make/O/N=3 W_Coef
W_Coef = 0

if( ntraces < 0 ) // secret mode to select a single trace
	isweep = abs( ntraces ) - 1
	ntraces = isweep + 1
else
	isweep = 0
endif

if( ntraces > nsweeps )
	print "check the graph, wrong trace number", ntraces, isweep, "nsweeps:", nsweeps
	ntraces = nsweeps
	//abort
endif

display/k=1
for( i = isweep; i < ntraces; i++ )
	thisTrace = removequotes( stringfromlist( i, traces ) )
	dummytrace = thisTrace + "_dum"
	duplicate/O $thisTrace, $dummytrace
	WAVE dummy = $dummytrace
	
	wavestats/Z/Q/R=(bs, be) dummy // measure the baseline
	Vbase = V_avg
	dummy -= Vbase // zero the baseline
	Vbase *= 1000 // convert to mV

	wavestats/Z/Q/R=( fe - fw, fe ) dummy // measure the baseline
	Vss = 1000 * V_avg // get the steady state voltage

	// duplicate the current trace
	Itracen = thisTrace[ 0, strlen(thisTrace)-2 ] + num2str( 2 ) // assumes I trace is #2

	wavestats/Z/Q/R=(bs, be) $Itracen // measure the baseline
	Ibase = V_avg // zero the baseline
	wavestats/Z/Q/R=(fs + Ibump, fe - Ibump ) $Itracen // measure the baseline
	Idelta = V_avg - Ibase
	//Print i, Itracen, Ibase, "delta:", Idelta

	deltaI = Idelta * 1e12 // extracted from current trace
	if( deltaI == 0 )
		deltaI = -5 * ( 4 - i ) // pA
		print isweep, "warning in CCCap, defaulting to -5 pA current step", deltaI, Idelta, "max", IdeltaMax
	endif

	if( deltaI < IdeltaMax )
		//if( i == isweep ) // first trace set up graph
			appendtograph dummy
			ModifyGraph rgb( $dummytrace )=(0,0,0)
		//endif

		// turn off fitting errors
		V_FitError = 0
		V_fitquitreason = 0
		CurveFit/TBOX=256/Q/N/K={fs} exp_XOffset dummy( fs, fe-fw ) /D 
		//CurveFit/Q/N exp_XOffset, dummy( fs, fe ) // /D	
		deltaV = 1000 * W_Coef[0] // ( abs(W_coef[0]) + abs(W_Coef[1]) ) / 2 // mV
		
		Rin = deltaV / deltaI // megaohms
		Rin2 = Vss / deltaI  // Vss is steady state voltage from fit

		Tau = W_Coef[2] * 1000 // msec
		Cap = Tau / Rin //
		Cap2 = Tau / Rin2 

		print thisTrace, "Vbase", Vbase, "dI", deltaI, "tau:", Tau, "dV:", Vss, "Rin:", Rin, "fit rin", Rin2, "Cap:", Cap, "fit cap", Cap2
		Ravg += Rin2
		Tavg += Tau
		count += 1
	else
		print i, idelta, deltai, ideltamax, "current too small"
		i = inf
	endif

endfor
Ravg /= count
Tavg /= count
return tau/Ravg

end // CC cap


function/S sAHPprocessor( wl, bstart, bend, xstart, xend, binsize )
string wl // list of waves to analyze
	variable bstart //= 1.9 // baseline start
	variable bend //= 2.0 // baseline end
	variable xstart //= 2.5 // time to start looking for minimum
	variable xend //= 5.0 // end time, set to INF for end of wave
	variable binsize //= 0.1 // in seconds, between xstart and xend bins


variable i, nw = itemsinlist( wl )
string wn, bins_wave, base_wn, minAbs_wn, minRel_wn, minRel2_wn, mint_wn, maxRel2_wn, maxT_wn 
wn = stringfromlist( 0, wl )
base_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPbase"
minAbs_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPmin"
minRel_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPminR"
minRel2_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPminR2"
mint_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPminT"
maxRel2_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPmaxR2"
maxT_wn = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + "_sAHPmaxT"

// auto detect min vs max AHP vs ADP? 
make/O/N=(nw) $base_wn
WAVE/Z basew = $base_wn

make/O/N=(nw) $minABS_wn
WAVE/Z minABSw = $minABS_wn

make/O/N=(nw) $minRel_wn
WAVE/Z minRelw = $minRel_wn

make/O/N=(nw) $minRel2_wn
WAVE/Z minRel2w = $minRel2_wn

make/O/N=(nw) $mint_wn
WAVE/Z mintw = $mint_wn

make/O/N=(nw) $maxRel2_wn
WAVE/Z maxRel2w = $maxRel2_wn

make/O/N=(nw) $maxT_wn
WAVE/Z maxTw = $maxT_wn

//edit/k=1/N=min_wn // table holds bins for each sweep
for( i=0; i< nw; i +=1 )
	wn = removequotes( stringfromlist( i, wl ) )
	bins_wave = sAHP( wn, bstart, bend, xstart, xend, binsize )
	WAVE/Z bw = $bins_wave
	//appendtotable/W=min_wn bw
	wavestats/Z/Q/R=(0,inf) bw
	basew[ i ] = bw[ 0 ]
	minABSw[ i ] = V_min 
	minRelw[ i ] = V_min - bw[ 0 ]
	minRel2w[ i ] = V_min - basew[ 0 ]
	mintw[ i ] = V_minloc
	maxRel2w[ i ] = V_max - basew[ 0 ]
	maxTw[ i ] = V_maxloc
	//print wn, V_min, V_minloc
endfor

//edit/k=1 basew, minAbsw, minRelw, minRel2w, mintw
string outstring
outstring = base_wn + ";" + minAbs_wn + ";" + minRel_wn + ";" + minRel2_wn + ";" + mint_wn + ";" + maxRel2_wn + ";" + maxT_wn + ";"

return outstring
end

// analyze slow AHP
function/S sAHP( wn, bstart, bend, xstart, xend, binsize ) // averages the Vm, puts in bins (doesn't care + or -!)
	string wn // name of wave to analyze
	variable bstart // baseline start
	variable bend // baseline end
	variable xstart // time to start looking for minimum
	variable xend // end time, set to INF for end of wave
	variable binsize // in seconds, between xstart and xend bins

	variable nbins = round( ( xend - xstart ) / binsize ) + 1 // storing baseline in first position

	string bins_out = datecodeGREP2( wn ) + num2str( seriesnumber( wn ) ) + num2str( sweepnumber( wn ) ) + "_sAHP"
	make/O/N=( nbins ) $bins_out
	WAVE/Z bout = $bins_out
	setscale/P x, -binsize, binsize, "s", bout

	WAVE/Z w = $wn
	if( !waveexists( w ) )
		print "sAHP: can't find wave to analyze!", wn
		abort
	endif

	variable baseline = nan, Vmin = nan, Vminloc = nan

	// get baseline
	wavestats/Z/Q/R=(bstart, bend) w 
	baseline = V_avg * 1000
	bout[0] = baseline

	variable i, bin_s, bin_e
	for( i = 1; i < nbins; i += 1 )

		bin_s = xstart + i* binsize
		bin_e = bin_s + binsize
		// get AVG !
		wavestats/Z/Q/R=( bin_s, bin_e ) w
		bout[ i ] = 1000 * V_avg //- baseline 
		//print nbins, bin_s, bin_e, V_avg

	endfor

	return bins_out // wave name containing the bins

end // sAHP

function plotFromSeriesLW( slwn, sweepnum )
	string slwn
	variable sweepnum

	WAVE/T/Z slw = $slwn
	if( !waveexists( slw ))
		print "plotfromSeriesLW, no wave", slwn
		abort
	endif

	string wn, temp_wn, datecode 
	variable seriesnum 
	variable iw, nw = numpnts( slw )
	
	display/k=1

	for( iw=0; iw < nw; iw += 1 )
		temp_wn = removequotes ( slw[ iw ] )
		seriesnum = seriesnumber( temp_wn )
		datecode = datecodeGREP2( temp_wn ) 
		wn = datecode + "g1s" + num2str( seriesnum ) + "sw" + num2str( sweepnum ) + "t1"
		//print iw, wn
		appendtograph $wn
	endfor
end


function secondThresher( series_listwn, dsmooth, iseries, isweep )
// plot membrane potential, first derivative and second derivative

  string series_listwn //= "ap0" // serieslistw" // 20200605 "seriesNames"
  variable dsmooth // need this for alldata wave name!! 20240612
  variable iseries, isweep 

  variable derderThresh = 5000
  variable offset = 0.499 // delay to the stimulus pulse, this is used to offset the timing of the event
  variable duration = 0.01 // secs, duration of current pulse
  variable apstart = 0.5, apdur = 0.005 // apdur after peak

	variable selectTrace = 1 // trace number, actual number of trace to be analyzed, usually 1 or 2


	string pathn = "collector_data" // this is the default path, must run collector first!

	if( !waveexists( $series_listwn ) )
		print "no series wave for secondThresher:", series_listwn
		abort
	endif 
	
	string sweeplist //= returnwavesfromseries( seriesn=sn, tracen = selectTrace, pathn = pathn )

	variable nseries = numpnts( $series_listwn )
	
	variable nsweeps //= 15 // number sweeps in AP protocol 

	string sn, wavelet, alldataname, minthreshn,minthreshtimen
	
	WAVE/T/Z series_listw = $series_listwn

	string graphn = "DER",graphn2, dVdVgraphn = "DERdV", dVdVgraphn2
	string dern,derdern, derderdVn, derderdVn2, derderdern
	string Vmtruncn, dertruncn

	variable V0cross, derderThresh_time, derderderthresh_time 

	//nseries = 1 // for now
//	iseries = 0
//	do
	sn = series_listw[ iseries ]
	sweeplist = returnwavesfromseries( sn, tracen = selectTrace, pathn = pathn )
	if(isweep<0)
		isweep = 0
		nsweeps = itemsinlist(sweeplist)
	else
		nsweeps = 0
	endif
	variable firstrun = 1
	do 
		wavelet = removequotes( stringfromlist( isweep, sweeplist )) // first wave from list for name
		print "processing series", sn, wavelet 

		Wave/Z Vm = $wavelet
		SetScale/P d 0,1,"V", Vm
		findlevel/Q Vm, 0
		V0cross = V_levelx

		// get the derivative dV/dt
		dern = wavelet + "d"
		duplicate/O Vm, $dern 
		WAVE der = $dern  
		differentiate der
		Wave sder = box2way( der, dsmooth )
		SetScale/P d 0,1,"V/s", sder

		// get the 2nd derivative versus time d(dV/dt)/dt
		derdern = dern + "d"
		duplicate/O der, $derdern 
		WAVE derder = $derdern
		differentiate derder
		Wave sderder = box2way( derder, dsmooth )
		findlevel/Q/R=(V0cross,offset)/edge=1 sderder, derderThresh // look backwards from peak
		derderThresh_time = V_levelX
		SetScale/P d 0,1,"V/s/s", sderder
		//display/K=1 sderder vs Vm

		// BUZSACKI THIRD DERIVATIVE! Neuroscience Vol. 105, No. 1, pp. 121, 2001
		derderdern = derdern + "d"
		duplicate/O derder, $derderdern 
		WAVE derderder = $derderdern
		differentiate derderder
		Wave sderderder = box2way( derderder, dsmooth )
		findlevel/Q/R=(V0cross,offset)/edge=1 sderder, derderThresh // look backwards from peak
		derderderThresh_time = V_levelX
		SetScale/P d 0,1,"V/s/s/s", sderderder

		// get the derivative versus V d(dV/V)/dV
		derderdVn = dern + "dV"
		derderdVn2 = dern + "dV2"
		Vmtruncn = wavelet + "Vtrunc"
		dertruncn = dern + "trunc"
		duplicate/O/R=(apstart,V0cross+apdur) Vm, $Vmtruncn
		WAVE Vmtrunc = $Vmtruncn
		SetScale/P d 0,1,"V", Vmtrunc
		duplicate/O/R=(apstart,V0cross+apdur) Vm, $dertruncn
		WAVE dertrunc = $dertruncn
		differentiate dertrunc
		WAVE sdertrunc = box2way(dertrunc,dsmooth)
		SetScale/P d 0,1,"V/s", sdertrunc

		duplicate/O/R=(apstart,V0cross+apdur) der, $derderdVn
		duplicate/O/R=(apstart,V0cross+apdur) der, $derderdVn2
		WAVE derderdV = $derderdVn
		differentiate/METH=2 derderdV /X=Vmtrunc
		WAVE derderdV2 = $derderdVn2
		WAVE derderdV2d =  td_diff(Vmtrunc, derderdV)

		WAVE sderderdV = box2way( derderdV, dsmooth )
		SetScale/P d 0,1,"V/s/V", sderderdV
		WAVE sderderdV2d = box2way( derderdV2d, dsmooth )
		SetScale/P d 0,1,"V/s/V", sderderdV2d

		// plot the phase plot and its derivative
		if(firstrun==1)
			dVdVgraphn2 = dVdVgraphn + wavelet + "_" +num2str(iseries)
			display/K=1/N=$dVdVgraphn2
		endif 
		appendtograph/W=$dVdVgraphn2/L sdertrunc vs Vmtrunc 
		ModifyGraph/W=$dVdVgraphn2 rgb($nameofwave(sdertrunc))=(0,0,0)
		appendtograph/W=$dVdVgraphn2/R sderderdV vs Vmtrunc
		ModifyGraph/W=$dVdVgraphn2 rgb($nameofwave(sderderdV))=(0,65535,0)
		//appendtograph/W=$dVdVgraphn2/R sderderdV2d vs Vmtrunc
		
		//appendtograph/W=$dVdVgraphn2/R=temp sderder vs Vm
		//ModifyGraph/W=$dVdVgraphn2 rgb($nameofwave(sderder))=(65535,0,0)

		ModifyGraph/W=$dVdVgraphn2 axRGB(right)=(0,65535,0),tlblRGB(right)=(0,65535,0),alblRGB(right)=(0,65535,0)
		SetAxis/W=$dVdVgraphn2 right -10000,10000
		SetAxis/W=$dVdVgraphn2 left -200,200
		ModifyGraph/W=$dVdVgraphn2 zero(right)=1

		// plot the membrane potential!
		if(firstrun==1)
			graphn2 = graphn + wavelet + "_" +num2str(iseries)
			display/k=1/N=$graphn2	
		endif 
		appendtograph/W=$graphn2/L Vm
		appendtograph/W=$graphn2/R sder
		appendtograph/W=$graphn2/L=secondder sderder
		appendtograph/W=$graphn2/L=thirdder sderderder
		
//		showinfo
//		cursor/W=$graphn2 a, $nameofwave(Vm), derderThresh_time
//		cursor/W=$graphn2 b, $nameofwave(sder), derderThresh_time
//		cursor/W=$graphn2/A=1 c, $nameofwave(sderder), derderThresh_time 

//		SetDrawEnv xcoord= bottom;DelayUpdate
//		DrawLine derderThresh_time,0.0,derderThresh_time,1

		ModifyGraph/W=$graphn2 axisEnab(left)={0,0.49}
		ModifyGraph/W=$graphn2 axisEnab(right)={0.5,1}
		ModifyGraph/W=$graphn2 axisEnab(secondder)={0.5,1}
		ModifyGraph/W=$graphn2 freePos(secondder)={0,bottom}
		ModifyGraph/W=$graphn2 zero(secondder)=1
		ModifyGraph/W=$graphn2 rgb($nameofwave(Vm))=(0,0,0)
		ModifyGraph/W=$graphn2 axRGB(right)=(0,65535,0),tlblRGB(right)=(0,65535,0),alblRGB(right)=(0,65535,0)
		ModifyGraph/W=$graphn2 rgb($nameofwave(sder))=(0,65535,0)
		ModifyGraph/W=$graphn2 axRGB(secondder)=(65535,0,0),tlblRGB(secondder)=(65535,0,0),alblRGB(secondder)=(65535,0,0)
		ModifyGraph/W=$graphn2 rgb($nameofwave(sderder))=(65535,0,0)

		SetAxis/W=$graphn2 bottom offset,offset+duration
		SetAxis/W=$graphn2 right -1,10
		SetAxis/W=$graphn2 secondder -10000,100000
		Label/W=$graphn2 bottom "time (seconds)"
		Label/W=$graphn2 left "Vm (V)"
		Label/W=$graphn2 right "deriv Vm (V)"
		Label/W=$graphn2 secondder "2nd deriv Vm (V)"

		firstrun = 0
		isweep+=1
	while(isweep<nsweeps)		
//		iseries = iseries + 1
//while( iseries < nseries )
end

function/WAVE td_diff(xw,yw)
	WAVE xw,yw 
	variable ip=0,np = numpnts(xw)
	variable dx,dy,slope
	string dwn = "d" + nameofwave(yw) + "_" + "d" + nameofwave(xw)
	duplicate/O yw,$dwn
	WAVE dw = $dwn  
	dw = 0
	for(ip=1;ip<np;ip+=1)
		dw[ip] = ( yw[ip] - yw[ip-1] ) / ( xw[ip] - xw[ip-1]) // backward difference
	endfor
	return dw 
end

function Thresher( series_listwn, [dsmooth] ) // ( threshold, offset, series_listwn )
	string series_listwn //= "ap0" // serieslistw" // 20200605 "seriesNames"
  variable dsmooth // need this for alldata wave name!! 20240612

  string suffix = ""
  if (!paramisdefault(dsmooth))
  	suffix = num2str(dsmooth)
  endif

	variable offset = 0.01 // delay to the stimulus pulse, this is used to offset the timing of the event
	variable HertzConversionFactor = 0.5 // secs, duration of current pulse

	variable selectTrace = 1 // trace number, actual number of trace to be analyzed, usually 1 or 2


	string pathn = "collector_data" // this is the default path, must run collector first!

	if( !waveexists( $series_listwn ) )
		print "no series wave for ap2 batchmode:", series_listwn
		abort
	endif 
	
	string sweeplist //= returnwavesfromseries( seriesn=sn, tracen = selectTrace, pathn = pathn )

	variable iseries, nseries = numpnts( $series_listwn )
	
	variable ipar, npar = 12 // number of parameters in all data
	variable isweep, nsweeps = 15 // number sweeps in AP protocol 

	string sn, wavelet, alldataname, minthreshn,minthreshtimen
	//string firstSpikeSummary //= "fss_"
	//string excitability //= "exc_"
	WAVE/T/Z series_listw = $series_listwn
	iseries = 0
	display/k=1/N=minthresh0 

	variable j=0
	do
		sn = series_listw[ iseries ]
		print "processing series", sn

		sweeplist = returnwavesfromseries( sn, tracen = selectTrace, pathn = pathn )

		wavelet = removequotes( stringfromlist( 0, sweeplist ))
		print wavelet
		alldataname = wavelet + "_alldata" + suffix
		minthreshn = wavelet + "_minThr" + suffix 
		minthreshtimen = wavelet + "_minThrTime" + suffix

		WAVE/Z alldata = $alldataname
		nsweeps = dimsize(alldata,0)

		make/O/N=(nsweeps) $minthreshn,$minthreshtimen
		WAVE/Z minthresh = $minthreshn
		WAVE/Z minthreshtime = $minthreshtimen

		for(j=0;j<nsweeps;j+=1)
			minthresh[j] = alldata[j][14]
			minthreshtime[j] = alldata[j][15] * 1000
		endfor
		appendtograph/W=minthresh0 minthresh vs minthreshtime 

		if( iseries == 0 )
			// make the tables to store the summaries

		endif
		iseries += 1
	while(iseries<nseries)	
	Label/W=minthresh0 bottom "min threshold time (ms)"
	Label/W=minthresh0 left "min threshold V/s"	
	rainbow()
end // Thresher !!



