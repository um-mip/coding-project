#pragma rtGlobals=3		// Use modern global access method and strict wave access.



strconstant svYear = "svYear", svMonth = svMonth, svDay = "svDay", svLetter = "svLetter", svDateCode = "svDateCode" 
// structure to store and read experiemntal paramenters to and from expcard
constant csize=16

structure expCardDef
	int16 year
	int16 month
	int16 day
	char letter[csize]
	char datecode[csize]
	char group[csize]
	char subgroup1[csize]
	char subgroup2[csize]
	char subgroup3[csize]
	char strain[csize]
	char transgenic[csize]
	char hetero[csize] // 
	char bdatecode[csize]
	char sex[csize] // 'm' male, 'f' female
	char surgeryDatecode[csize]
	char surgery[csize*10]
	char treatment[csize*10]
	char treatdatecode[csize]
	char status[csize] //na, P, D, E, M
	char sactime[csize]
	char sacAMorPM[csize] //AM or PM
	float sacWeight
	float gndWeight //gonad weight (ovary/testes)
	float tractWeight //tract weight (uterus/seminal vesicles)
	char comment1[csize*10]
	char comment2[csize*10]
	char comment3[csize*10]
	char comment4[csize*10]
	char comment5[csize*10]
endstructure

structure expCardDef_igor
	NVAR year
	NVAR month
	NVAR day
	SVAR letter
	SVAR datecode
	SVAR group
	SVAR subgroup1
	SVAR subgroup2
	SVAR subgroup3
	SVAR strain
	SVAR transgenic
	SVAR hetero 
	SVAR bdatecode
	SVAR sex // 'm' male, 'f' female
	SVAR surgeryDatecode
	SVAR surgery
	SVAR treatment
	SVAR treatdatecode
	SVAR status //na, P, D, E, M
	SVAR sactime
	SVAR sacAMorPM //AM or PM
	NVAR sacWeight
	NVAR gndWeight //gonad weight (ovary/testes)
	NVAR tractWeight //tract weight (uterus/seminal vesicles)
	SVAR comment1
	SVAR comment2
	SVAR comment3
	SVAR comment4
	SVAR comment5
endstructure


///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 			GET CARD 			/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-06-02
/// NOTES: Switched to using functions to get the control info
/// as these functions will check if the control exists, and use a defined panel name
/// so that card doesn't have to be front to get these values
/// The problem with using V_value/S_value is that if a control doesn't exist, these variables
/// retain their values from the last control, and can lead to storing incorrect values for the
/// intended parameter
///
/// Also note that even though the function says it returns "success", this never changes from 0
function getcard(s) // gets the info from the expcard panel and populates the ExpCard structure
struct expCardDef &s
variable success=0
variable het,homo,male,female, surgGDX,surgNone,treatE,treatT,statusNA,pro,di,est,met,sactam,sactpm
string treatOther

// make sure the expcard panel is the active window
string windows=winlist("ExperimentCard",";","")
variable nitems=itemsinlist(windows)
string expPanelName = "ExperimentCard"
if(nitems>0)
	// dowindow /F $expPanelName
else
	print "there is no card to GET!"
	abort
endif

// exp basic info
s.year = getSetVarVal("svYear", panelName = expPanelName)
s.month= getSetVarVal("svMonth", panelName = expPanelName)
s.day=getSetVarVal("svDay", panelName = expPanelName)
s.letter= getSetVarString("svLetter", panelName = expPanelName)
s.datecode=getSetVarString("svDateCode", panelName = expPanelName)
s.group=getSetVarString("svGroup", panelName = expPanelName)

// sub group categories
s.subgroup1=getSetVarString("svsubgroup1", panelName = expPanelName)
s.subgroup2=getSetVarString("svsubgroup2", panelName = expPanelName)
s.subgroup3=getSetVarString("svsubgroup3", panelName = expPanelName)

// animal info
s.strain=getSelectedItem("lbStrain", hostName = expPanelName)
if(stringmatch(s.strain,"other"))
	string enteredStrain = getSetVarString("svStrain", panelName = expPanelName)
	if(strlen(enteredStrain)>0)
		s.strain = enteredStrain
	endif
endif

s.transgenic=getSelectedItem("lbTransgenic", hostName = expPanelName)
if(stringmatch(s.transgenic, "other"))
	string enteredTransgene = getSetVarString("svTgOther", panelName = expPanelName)
	if(strlen(enteredTransgene)>0)
		s.transgenic = enteredTransgene
	endif
endif

het = getCBVal("cbHet", panelName = expPanelName)
homo = getCBVal("cbHomo", panelName = expPanelName)

if(het)
	s.hetero="heterozygous"//heterozygous
else
	s.hetero="homozygous" //homozygous
endif

// sex
s.bdatecode=getSetVarString("svBirthDateCode", panelName = expPanelName)
male = getCBVal("cbMale", panelName = expPanelName)
female = getCBVal("cbFemale", panelName = expPanelName)
if(male)
	s.sex = "male"
else
	s.sex="female"
endif

// surgery info
s.surgeryDateCode = getSetVarString("svSurgeryDateCode", panelName = expPanelName)
surgnone= getCBVal("cbSurgeryNone", panelName = expPanelName)
surgGDX=getCBVal("cbSurgeryGDX", panelName = expPanelName)
if(surgNone)
	s.surgery="none"
else
	if(surgGDX)
		s.surgery="GDX"
	endif
endif
string surgOther = getSetVarString("svSurgeryOther", panelName = expPanelName)
if(!stringmatch(surgOther,"other") && strlen(surgOther)>0)
	if(strlen(s.surgery)>0)
		s.surgery += " "
	endif
	s.surgery += surgOther
endif

// treatment info
treatE= getCBVal("cbTreatE2", panelName = expPanelName)
treatT=getCBVal("cbTreatT", panelName = expPanelName)
if(treatE)
	s.treatment="E2"
endif

if(treatT)
	if(strlen(s.treatment)>0)
		s.treatment += " "
	endif
	s.treatment +="T"
endif

treatOther = getSetVarString("svTreatmentOther", panelName = expPanelName)
if(!stringmatch(treatOther,"other") && strlen(treatOther)>0)
	if(strlen(s.treatment)>0)
		s.treatment += " "
	endif
	s.treatment += treatOther
endif

s.treatdatecode= getSetVarString("svTreatDateCode", panelName = expPanelName)

// natural cycling status
if(getCBVal("cbStatusNA", panelName = expPanelName)) // is NA is checked
	s.status="n/a"
elseif(getCBVal("cbStatusP", panelName = expPanelName)) // P is checked
	s.status="proestrus"
elseif(getCBVal("cbStatusE", panelName = expPanelName)) // E is checked
	s.status="estrus"
elseif(getCBVal("cbStatusD", panelName = expPanelName)) // D is checked
	s.status="diestrus"
elseif(getCBVal("cbStatusM", panelName = expPanelName)) // M is checked
	s.status="metestrus"
endif

// sac time and params
s.sactime = getSetVarString("svSacTime", panelName = expPanelName)

sactam= getCBVal("cbSacTAM", panelName = expPanelName)
sactpm=getCBVal("cbSacTPM", panelName = expPanelName)

if(sactam)
	s.sacAMorPM="AM"
else
	s.sacAMorPM="PM"
endif

s.sacweight=getSetVarVal("svSacWeight", panelName = expPanelName)
s.gndweight = getSetVarVal("svGweight", panelName = expPanelName)
s.tractweight = getSetVarVal("svTractweight", panelName = expPanelName)

s.comment1 = getSetVarString("svComment1", panelName = expPanelName)
s.comment2 = getSetVarString("svComment2", panelName = expPanelName)
s.comment3 = getSetVarString("svComment3", panelName = expPanelName)
s.comment4 = getSetVarString("svComment4", panelName = expPanelName)
s.comment5 = getSetVarString("svComment5", panelName = expPanelName)

return success
end


///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 			PUT CARD 			/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
function putcard(s) // puts the info from the structure into the card
struct expCardDef &s
variable success=0
variable het,homo,male,female, surgGDX,surgNone,treatE,treatT,statusNA,pro,di,est,met,sactam,sactpm
string treatOther

// make sure the expcard panel is the active window
string expPanelName = "ExperimentCard"
string windows=winlist(expPanelName,";","")
variable nitems=itemsinlist(windows)
if(nitems>0)
	// dowindow /F  $expPanelName
else
	//make expcard panel
	string datecode = s.datecode
	expcard(datecode)
endif

// exp basic info
setvariable svYear, value=_NUM:s.year, win = $expPanelName
setvariable svMonth, value=_NUM:s.month, win = $expPanelName
setvariable svDay, value=_NUM:s.day, win = $expPanelName
setvariable svLetter, value=_STR:s.letter, win = $expPanelName
setvariable svDateCode, value=_STR:s.datecode, win = $expPanelName
setvariable svGroup, value=_STR:s.group, win = $expPanelName

// sub group categories
setvariable svsubgroup1, value=_STR:s.subgroup1, win = $expPanelName
setvariable svsubgroup2, value=_STR:s.subgroup2 , win = $expPanelName
setvariable svsubgroup3, value=_STR:s.subgroup3, win = $expPanelName

// animal info
// cheating! we KNOW the selwave for the strain listbox
// strain
WAVE/T listw = strainList
variable i, n=dimsize(listw,0),flag=0
for(i=0;i<n;i+=1)
	if(stringmatch(s.strain,listw[i]))
		listbox lbStrain selrow=i, win = $expPanelName
		flag=1
	endif
endfor
if(!flag)
	// select other
	// print "IN PUTCARD: setting strain to 'other'"
	FindValue/Text="other" listw
	ListBox lbStrain selRow = V_Value, win = $expPanelName // if "other" isn't found, v_value will be -1, and selRow of -1 unselects all

	setvariable svStrain, value=_STR:s.strain, win = $expPanelName
endif

//transgenic
WAVE/T listw = transgenicList
flag=0
n=dimsize(listw,0)
for(i=0;i<n;i+=1)
	if(stringmatch(s.transgenic,listw[i]))
		listbox lbTransgenic selrow=i, win=$expPanelName
		flag=1
	endif
	if(stringmatch(listw[i], "GnRH-GFP3") && stringmatch(s.transgenic, "GnRH-GFP CBA")) // switched label, since it's a hybrid CBA/Bl6 line
		listbox lbTransgenic selrow=i, win=$expPanelName
		flag=1
	endif
endfor
if(!flag)
	// select other
	// print "IN PUTCARD: setting transgenic to 'other'",s.Transgenic
	FindValue/Text="other" listw
	ListBox lbTransgenic selRow = V_Value, win = $expPanelName // if "other" isn't found, v_value will be -1, and selRow of -1 unselects all

	setvariable svTgOther, value=_STR:s.transgenic, win = $expPanelName
endif

// homo vs. hetero
NVAR g_radioval = g_cbHetRadio
if(stringmatch(s.hetero,"heterozygous"))
	g_radioval = 1
else
	g_radioval = 2
endif

checkbox cbHet, value=g_radioval==1, win = $expPanelName
checkbox cbHomo, value=g_radioval==2, win = $expPanelName

// birthdate
setvariable svBirthDateCode, value=_STR:s.bdatecode, win = $expPanelName

// sex
NVAR g_radioval = g_cbFemaleRadio
if(stringmatch(s.sex,"female"))
	g_radioval = 1
else
	g_radioval = 2
endif
checkbox cbFemale, value=g_radioval==1, win = $expPanelName
checkbox cbMale, value=g_radioval==2, win = $expPanelName

// surgery
setvariable svSurgeryDateCode, value=_STR:s.surgerydatecode, win = $expPanelName
NVAR g_radioval = g_cbGDXRadio
string extraSurgery
if(stringmatch(s.surgery,"none*") || strlen(s.surgery)==0)
	g_radioval = 1
	splitString/E=("^none (.*)$") s.surgery, extraSurgery
else
	if(stringmatch(s.surgery,"GDX*"))	
		g_radioval = 2
		splitString/E=("^GDX (.*)$") s.surgery, extraSurgery
	else
		extraSurgery = s.surgery
	endif			
endif
setvariable svSurgeryOther, value=_STR:extraSurgery, win = $expPanelName
checkbox cbSurgeryNone, value=g_radioval==1, win = $expPanelName
checkbox cbSurgeryGDX, value=g_radioval==2, win = $expPanelName

// treatment
string E2orT, spaceStr, extraTreatment
splitString/E="(E2\s+T|E2|T)?(\s+)?(.*)" s.treatment, E2orT, spaceStr, extraTreatment
checkbox cbTreatE2, value=(stringmatch(E2orT,"E2*")), win = $expPanelName
checkbox cbTreatT, value=(stringmatch(E2orT,"T") || stringmatch(E2orT, "E2 T")), win = $expPanelName
setvariable svTreatmentOther, value=_STR:extraTreatment, win = $expPanelName
setvariable svTreatDateCode, value=_STR:s.treatdatecode, win = $expPanelName

// natural cycle status
NVAR g_radioval = g_cbStatusRadio
string status=s.status
strswitch(status)
	case "n/a":
		g_radioval = 1
		break
	case "proestrus":
		g_radioval = 2
		break
	case "diestrus":
		g_radioval = 3
		break
	case "estrus":
		g_radioval = 4
		break
	case "metestrus":
		g_radioval = 5
		break
endswitch

checkbox cbStatusNA, value = g_radioval==1, win = $expPanelName
checkbox cbStatusP, value = g_radioval==2, win = $expPanelName
checkbox cbStatusD, value = g_radioval==3, win = $expPanelName
checkbox  cbStatusE, value = g_radioval==4, win = $expPanelName
checkbox cbStatusM, value = g_radioval==5, win = $expPanelName

//sac info
setvariable svSactime, value=_STR:s.sactime, win = $expPanelName
NVAR g_radioval = g_cbAMPMRadio
status=s.sacAMorPM
strswitch(status)
	case "AM":
		g_radioval = 1
		break
	case "PM":
		g_radioval = 2
		break
endswitch

checkbox cbSacTAM, value = g_radioval==1, win = $expPanelName
checkbox cbSacTPM, value = g_radioval==2, win = $expPanelName

setvariable svSacWeight, value=_NUM:s.sacweight, win = $expPanelName
setvariable svGweight, value=_NUM:s.gndweight, win = $expPanelName
setvariable svTractweight, value=_NUM:s.tractWeight, win = $expPanelName

setvariable svComment1, value=_STR:s.comment1, win = $expPanelName
setvariable svComment2, value=_STR:s.comment2, win = $expPanelName
setvariable svComment3, value=_STR:s.comment3, win = $expPanelName
setvariable svComment4, value=_STR:s.comment4, win = $expPanelName
setvariable svComment5, value=_STR:s.comment5, win = $expPanelName

end//putcard
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////


///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 			WRITE CARD 		/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
function writeExpCard()
// get params from card
struct expCardDef s
getcard(s)

string datecode = s.datecode, ext=".txt"
string fn = datecode+ext
variable refnum

pathinfo collector_data
string mypath = s_path
fn=mypath+datecode+ext

open refnum as fn
fbinwrite/b=3 refnum, s
close refnum

end

///////////
/// saveExpCardIgor
/// AUTHOR: 
/// ORIGINAL DATE: 2024-06-03
/// NOTES: 
///////////
function saveExpCardIgor([string panelName])
	if(paramIsDefault(panelName))
		panelName = "experimentCard"
	endif
	
	string cellName = getSetVarString("svDateCode", panelName = panelName)
	DFREF cardDFR = getCellExpCardDF(cellName)
	if(dataFolderRefStatus(cardDFR)!=0)
		KillDataFolder/Z cardDFR
	endif

	createCellExpCardDF(cellName)
	DFREF cardDFR = getCellExpCardDF(cellName)

	struct expCardDef_igor igorS
	StructFill/AC=1/SDFR=cardDFR igorS

	Struct expCardDef s
	getcard(s)

	fillIgorExpCardStruct(igorS, s)	
end

///////////
/// fillIgorExpCardStruct
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Fill the igor experimental card structure with the text card structure
///////////
function fillIgorExpCardStruct(igorS, textS)
	Struct expCardDef_igor &igorS
	Struct expCardDef &textS

	igorS.year = textS.year
	igorS.month = textS.month
	igorS.day = textS.day
	igorS.letter = textS.letter
	igorS.datecode = textS.datecode
	igorS.group = textS.group
	igorS.subgroup1 = textS.subgroup1
	igorS.subgroup2 = textS.subgroup2
	igorS.subgroup3 = textS.subgroup3
	igorS.strain = textS.strain
	igorS.transgenic = textS.transgenic
	igorS.hetero = textS.hetero 
	igorS.bdatecode = textS.bdatecode
	igorS.sex = textS.sex // 'm' male, 'f' female
	igorS.surgeryDatecode = textS.surgeryDatecode
	igorS.surgery = textS.surgery
	igorS.treatment = textS.treatment
	igorS.treatdatecode = textS.treatdatecode
	igorS.status = textS.status //na, P, D, E, M
	igorS.sactime = textS.sactime
	igorS.sacAMorPM = textS.sacAMorPM //AM or PM
	igorS.sacWeight = textS.sacWeight
	igorS.gndWeight = textS.gndWeight //gonad weight
	igorS.tractWeight = textS.tractWeight //gonad weight
	igorS.comment1 = textS.comment1
	igorS.comment2 = textS.comment2
	igorS.comment3 = textS.comment3
	igorS.comment4 = textS.comment4
	igorS.comment5 = textS.comment5
end

///////////
/// fillTextCardStruct
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: Fill the text card structure with the igor card structure
///////////
function fillTextCardStruct(igorS, textS)
	Struct expCardDef_igor &igorS
	Struct expCardDef &textS

	textS.year = igorS.year
	textS.month = igorS.month
	textS.day = igorS.day
	textS.letter = igorS.letter
	textS.datecode = igorS.datecode
	textS.group = igorS.group
	textS.subgroup1 = igorS.subgroup1
	textS.subgroup2 = igorS.subgroup2
	textS.subgroup3 = igorS.subgroup3
	textS.strain = igorS.strain
	textS.transgenic = igorS.transgenic
	textS.hetero = igorS.hetero 
	textS.bdatecode = igorS.bdatecode
	textS.sex = igorS.sex // 'm' male, 'f' female
	textS.surgeryDatecode = igorS.surgeryDatecode
	textS.surgery = igorS.surgery
	textS.treatment = igorS.treatment
	textS.treatdatecode = igorS.treatdatecode
	textS.status = igorS.status //na, P, D, E, M
	textS.sactime = igorS.sactime
	textS.sacAMorPM = igorS.sacAMorPM //AM or PM
	textS.sacWeight = igorS.sacWeight
	textS.gndWeight = igorS.gndWeight //gonad weight
	textS.tractWeight = igorS.tractWeight //tract weight
	textS.comment1 = igorS.comment1
	textS.comment2 = igorS.comment2
	textS.comment3 = igorS.comment3
	textS.comment4 = igorS.comment4
	textS.comment5 = igorS.comment5
end




///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 		READ CARD 			/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
function readExpCard2(datecode, sout, suppress)
string datecode  //datecode 20160504a
STRUCT expcarddef &sout
variable suppress // set to 1 to suppress making new expcard if no card exists
string ext=".txt",fn
variable refnum, success=0

struct expCardDef s

fn=datecode+ext
pathinfo collector_data
string mypath = s_path
fn=mypath+datecode+ext

open /Z/R refnum as fn
if(v_flag==0)
	fbinread/b=3 refnum, s
	close refnum
//	print s
	sout = s
	putcard(s)
	success=1
else
	//handle no expcard
	//print "no expcard file:", fn
	if(!suppress)
		expcard(datecode)
	endif
	success=0
endif
return success
end

///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 		READ CARD 			/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
function readExpCard(datecode)
	string datecode  //datecode 20160504a

	struct expCardDef s
	DFREF cardDFR = getCellExpCardDF(datecode)
	variable success = 0
	
	// If there's an experimental card saved to Igor, prefer that over a text file
	if(dataFolderRefStatus(cardDFR)!=0)
		Struct expCardDef_igor igorS
		StructFill/SDFR=cardDFR	igorS

		fillTextCardStruct(igorS, s)
		putcard(s)
		success = 1
	else
		string ext=".txt",fn
		variable refnum

		fn=datecode+ext
		pathinfo collector_data
		string mypath = s_path
		fn=mypath+datecode+ext

		open /Z/R refnum as fn
		if(v_flag==0)
			fbinread/b=3 refnum, s
			close refnum
			// print s
			putcard(s)
			success=1
		else
			//handle no expcard
			//	print "no expcard file:", fn
			expcard(datecode)
			success=0
		endif
	endif

	return success
end

///////////////////////////////////////////////
///////////////////////////////////////////////
///////// 		BUILD EXP CARD 			/////////
///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-06-03
/// NOTES: Added specified window to the controls so that experimentCard doesn't have to be brought to the front each time
/// Also changed the upper limit of the year from 2020 to 2024
Function ExpCard(dc) 
	string dc //pass the date code if appropriate
	variable/G g_cbStatusRadio=0, g_cbHetRadio=0, g_cbFemaleRadio=0,g_cbAMPMRadio=0, g_cbGDXRadio=0
	variable xs=25,dx=200,ys=25, dy=22
	variable col=0, row=0
	PauseUpdate; Silent 1		// building window...

	// make sure the expcard panel is the active window
	string expPanelName = "ExperimentCard"
	string windows=winlist(expPanelName,";","")
	variable nitems=itemsinlist(windows)
	if(nitems>0)
		// dowindow /F  $expPanelName
	else
		//make expcard panel
		NewPanel /N=$expPanelName/k=1/W=(892,54,1800,400)
	endif

	[variable year, variable month, variable day] = getYearMonthDayFromCellID(dc)

	//column 1
	SetVariable svYear,pos={xs,ys},size={100,15},proc=SetVarProcCard,title="year", win = $expPanelName
	SetVariable svYear,limits={2010,2040,1},value= _NUM:year, win = $expPanelName
	SetVariable svMonth,pos={xs,ys+dy},size={100,15},proc=SetVarProcCard,title="month", win = $expPanelName
	SetVariable svMonth,limits={1,12,1},value= _NUM:month, win = $expPanelName
	SetVariable svDay,pos={xs,ys+dy*2},size={100,15},proc=SetVarProcCard,title="day", win = $expPanelName
	SetVariable svDay,limits={1,31,1},value= _NUM:day, win = $expPanelName
	SetVariable svLetter,pos={xs,ys+dy*3},size={100,15},proc=SetVarProcCard,title="code", win = $expPanelName
	SetVariable svLetter,value= _STR:"", win = $expPanelName
	SetVariable svDateCode,pos={xs,ys+dy*4},size={150,15},proc=SetVarProcCard,title="Date code", win = $expPanelName
	SetVariable svDateCode,value= _STR:dc, win = $expPanelName, disable = 2 // no edit

	SetVariable svGroup,pos={xs,ys+dy*5},size={150,15},proc=SetVarProcCard,title="Group", win = $expPanelName
	SetVariable svGroup,value= _STR:"", win = $expPanelName
	SetVariable svsubGroup1,pos={xs,ys+dy*6},size={150,15},proc=SetVarProcCard,title="subgroup", win = $expPanelName
	SetVariable svSubGroup1,value= _STR:"", win = $expPanelName
	SetVariable svSubGroup2,pos={xs,ys+dy*7},size={150,15},proc=SetVarProcCard,title="subgroup2", win = $expPanelName
	SetVariable svSubGroup2,value= _STR:"", win = $expPanelName
	SetVariable svSubGroup3,pos={xs,ys+dy*8},size={150,15},proc=SetVarProcCard,title="subgroup3", win = $expPanelName
	SetVariable svSubGroup3,value= _STR:"", win = $expPanelName
	
	SetVariable svBirthDateCode,pos={xs,ys + dy*9},size={150,15},proc=SetVarProcCard,title="birth date", win = $expPanelName
	SetVariable svBirthDateCode,value= _STR:"", win = $expPanelName

	col=xs
	row=ys+dy*11
	TitleBox saveInfoText title="Card saves to Igor with edits", pos = {col, row}, frame=0, fstyle=1, win = $expPanelName
	Button resetCardButton title="Reset to default",pos={col,row + dy}, size={150,20},fColor=(0,0,65535), proc=resetExpCardProc, win = $expPanelName
	Button bWriteCard title="write card to txt file",pos={col,row + dy*2}, size={150,20},fColor=(65535,0,0), proc=bWriteCardProc, win = $expPanelName

	//column 2
	col=xs+dx
	row=ys
	variable maxstrains=6
	make/O/T/N=(maxstrains,1) strainlist
	strainlist={"C57BL6", "CBB6F1 (hybrid)", "CD1","CBA","SV129", "other"}
	make/O/N=(maxstrains,1,2) strainsel
	
	listbox lbStrain, pos={col,row}, size={150,120}, mode=2, proc=lbExpCardProc, title="Strain", win = $expPanelName
	listbox lbStrain, listwave=strainlist, selwave=strainsel,selrow=(maxstrains-1), win = $expPanelName

	row+=dy*6
	SetVariable svStrain,pos={col,row},size={150,15},proc=SetVarProcCard,title="strain ", win = $expPanelName
	SetVariable svStrain,value= _STR:"", win = $expPanelName
	row+=dy
	variable maxTransgenics=5
	make/O/T/N=(maxTransgenics,1) transgeniclist
	transgeniclist={"GnRH-GFP2","GnRH-GFP3","Kiss-hrGFP","TAC2-GFP","other"}
	make/O/N=(maxTransgenics,1,2) transgenicsel
	
	listbox lbTransgenic, pos={col,row}, size={150,100}, mode=2, proc=lbExpCardProc, title="Transgenic", win = $expPanelName
	listbox lbTransgenic, listwave=transgeniclist, selwave=transgenicsel,selrow=(maxtransgenics-1), win = $expPanelName

	row+=dy*5
	SetVariable svTgOther,pos={col,row},size={150,15},proc=SetVarProcCard,title="transgenic", win = $expPanelName
	SetVariable svTgOther,value= _STR:"", win = $expPanelName
	row+=dy
	CheckBox cbHet,pos={col,row},size={77,14},title="heterozygous",value= 1,mode=1,proc=cbHetRadioProc, win = $expPanelName
	CheckBox cbHomo,pos={col+0.5*dx,row},size={73,14},title="homozygous",value= g_cbHetRadio,mode=1,proc=cbHetRadioProc, win = $expPanelName
	row+=dy
	
	//column 3
	col=xs+dx*2
	row=ys
	CheckBox cbMale,pos={col,row},size={38,14},title="male",value= g_cbFemaleRadio,mode=1,proc=cbFemaleRadioProc, win = $expPanelName
	CheckBox cbFemale,pos={col+dx*0.5,row},size={47,14},title="female",value= 1,mode=1,proc=cbFemaleRadioProc, win = $expPanelName
	row+=dy
	SetVariable svSurgeryDateCode,pos={col,row},size={150,15},proc=SetVarProcCard,title="surgery date", win = $expPanelName
	SetVariable svSurgeryDateCode,value= _STR:"", win = $expPanelName
	row+=dy
	CheckBox cbSurgeryNone,pos={col,row},size={39,14},title="none",value= g_cbGDXRadio,mode=1,proc=cbGDXRadioProc, win = $expPanelName
	CheckBox cbSurgeryGDX,pos={col+dx*0.5,row},size={77,14},title="gonadectomy", win = $expPanelName
	CheckBox cbSurgeryGDX,value= 1,mode=1,proc=cbGDXRadioProc, win = $expPanelName
	row+=dy
	SetVariable svSurgeryOther,pos={col,row},size={150,15},proc=SetVarProcCard,title="surgery", win = $expPanelName
	SetVariable svSurgeryOther,value= _STR:"", win = $expPanelName
	row+=dy
	CheckBox cbTreatE2,pos={col,row},size={29,14}, proc=cbExpCardProc,title="E2",value= 0,mode=0, win = $expPanelName
	CheckBox cbTreatT,pos={col+0.5*dx,row},size={24,14}, proc=cbExpCardProc,title="T",value= 0,mode=0, win = $expPanelName
	row+=dy
	SetVariable svTreatmentOther,pos={col,row},size={150,15},proc=SetVarProcCard,title="treatment", win = $expPanelName
	SetVariable svTreatmentOther,value= _STR:"", win = $expPanelName
	row+=dy
	SetVariable svTreatDateCode,pos={col,row},size={150,15},proc=SetVarProcCard,title="treatment date", win = $expPanelName
	SetVariable svTreatDateCode,value= _STR:"", win = $expPanelName
	row+=dy
	CheckBox cbStatusNA,pos={col,row},size={23,14},title="n/a",value=1, mode=1,proc=cbStatusProc, win = $expPanelName
	CheckBox cbStatusP,pos={col+dx*.2,row},size={23,14},title="P",value= g_cbStatusRadio,mode=1,proc=cbStatusProc, win = $expPanelName
	CheckBox cbStatusD,pos={col+dx*.4,row},size={24,14},title="D",value= g_cbStatusRadio,mode=1,proc=cbStatusProc, win = $expPanelName
	CheckBox cbStatusE,pos={col+dx*.6,row},size={23,14},title="E",value= g_cbStatusRadio,mode=1,proc=cbStatusProc, win = $expPanelName
	CheckBox cbStatusM,pos={col+dx*.8,row},size={25,14},title="M",value=g_cbStatusRadio,mode=1,proc=cbStatusProc, win = $expPanelName

	row+=dy
	SetVariable svComment1 proc=SetVarProcCard, title="comment1",pos={col,row}, size={dx*2,15},value=_STR:"", win = $expPanelName
	row+=dy
	SetVariable svComment2 proc=SetVarProcCard, title="comment2",pos={col,row}, size={dx*2,15},value=_STR:"", win = $expPanelName
	row+=dy
	SetVariable svComment3 proc=SetVarProcCard, title="comment3",pos={col,row}, size={dx*2,15},value=_STR:"", win = $expPanelName
	row+=dy
	SetVariable svComment4 proc=SetVarProcCard, title="comment4",pos={col,row}, size={dx*2,15},value=_STR:"", win = $expPanelName
	row+=dy
	SetVariable svComment5 proc=SetVarProcCard, title="comment5",pos={col,row}, size={dx*2,15},value=_STR:"", win = $expPanelName

	//column 4
	col=xs+dx*3
	row=ys
	SetVariable svSacTime,pos={col,row},size={200,15},proc=SetVarProcCard,title="sac time", win = $expPanelName
	SetVariable svSacTime,limits={31,1,1},value= _STR:"", win = $expPanelName
	row+=dy
	CheckBox cbSacTAM,pos={col,row},size={30,14},title="AM",value= 1,mode=1,proc=cbAMPMRadioProc, win = $expPanelName
	CheckBox cbSacTPM,pos={col+0.5*dx,row},size={30,14},title="PM",value= g_cbAMPMRadio,mode=1,proc=cbAMPMRadioProc, win = $expPanelName
	row+=dy
	SetVariable svSacWeight,pos={col,row},size={200,15},proc=SetVarProcCard ,title="body mass (g)", win = $expPanelName
	SetVariable svSacWeight,limits={0,200,1},value= _NUM:0, win = $expPanelName
	row+=dy
	SetVariable svGweight,pos={col,row},size={200,15},proc=SetVarProcCard ,title="gonad mass (mg)", win = $expPanelName
	SetVariable svGweight,limits={0,300,1},value= _NUM:0, win = $expPanelName
	row+=dy
	SetVariable svTractweight,pos={col,row},size={200,15},proc=SetVarProcCard ,title="repro tract mass (mg)", win = $expPanelName
	SetVariable svTractweight,limits={0,300,1},value= _NUM:0, win = $expPanelName
End

///////////
/// lbExpCardProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: 
///////////
function lbExpCardProc(LB_Struct) : ListBoxControl
	STRUCT WMListboxAction & LB_Struct
	if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
		saveExpCardIgor(panelName = LB_struct.win)
	endif
	return 0
end

///////////
/// cbExpCardProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: 
///////////
function cbExpCardProc(CB_Struct) : CheckBoxControl
	STRUCT WMCheckboxAction & CB_Struct
	if(CB_Struct.eventcode == 2) // Mouse up
		saveExpCardIgor(panelName = CB_struct.win)
	endif
	return 0
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	handler for setVar stuff
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
Function SetVarProcCard(sva) : SetVariableControl
	STRUCT WMSetVariableAction &sva
	
	switch( sva.eventCode )
		case 8: // end edit
			saveExpCardIgor(panelName = sva.win)
			break
		case 1: // mouse up
			saveExpCardIgor(panelName = sva.win)
			break
		case 2: // Enter key
		case 3: // Live update
			Variable dval = sva.dval
			String sval = sva.sval
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	RADIO HANDLER FOR REPRODUCTIVE STATUS
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cbStatusProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	variable chk = s.checked
	string graphn = s.win
	string cbn = s.ctrlname

	NVAR g_radioval = g_cbStatusRadio

	strswitch(cbn)
		case "cbStatusNA":
			g_radioval = 1
			break
		case "cbStatusP":
			g_radioval = 2
			break
		case "cbStatusD":
			g_radioval = 3
			break
		case "cbStatusE":
			g_radioval = 4
			break
		case "cbStatusM":
			g_radioval = 5
			break
	endswitch
	checkbox cbStatusNA, value = g_radioval==1
	checkbox cbStatusP, value = g_radioval==2
	checkbox cbStatusD, value = g_radioval==3
	checkbox  cbStatusE, value = g_radioval==4
	checkbox cbStatusM, value = g_radioval==5

	saveExpCardIgor(panelName = s.win)
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	RADIO HANDLER FOR HET/HOMO
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cbHetRadioProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	variable chk = s.checked
	string graphn = s.win
	string cbn = s.ctrlname

	NVAR g_radioval = g_cbStatusRadio

	strswitch(cbn)
		case "cbHet":
			g_radioval = 1
			break
		case "cbHomo":
			g_radioval = 2
			break

	endswitch
	checkbox cbHet, value = g_radioval==1
	checkbox cbHomo, value = g_radioval==2

	saveExpCardIgor(panelName = s.win)
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	RADIO HANDLER FOR MALE / FEMALE
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cbFemaleRadioProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	variable chk = s.checked
	string graphn = s.win
	string cbn = s.ctrlname

	NVAR g_radioval = g_cbFemaleRadio

	strswitch(cbn)
		case "cbMale":
			g_radioval = 1
			break
		case "cbFemale":
			g_radioval = 2
			break

	endswitch
	checkbox cbMale, value = g_radioval==1
	checkbox cbFemale, value = g_radioval==2

	saveExpCardIgor(panelName = s.win)
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	RADIO HANDLER FOR AM / PM
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cbAMPMRadioProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	variable chk = s.checked
	string graphn = s.win
	string cbn = s.ctrlname

	NVAR g_radioval = g_cbAMPMRadio

	strswitch(cbn)
		case "cbSacTAM":
			g_radioval = 1
			break
		case "cbSacTPM":
			g_radioval = 2
			break

	endswitch
	checkbox cbSacTAM, value = g_radioval==1
	checkbox cbSacTPM, value = g_radioval==2

	saveExpCardIgor(panelName = s.win)
end

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 	RADIO HANDLER FOR SURGERY: GDX / NONE
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
function cbGDXRadioProc(s) : CheckBoxControl
	STRUCT WMCHECKBOXACTION &s
	variable chk = s.checked
	string graphn = s.win
	string cbn = s.ctrlname

	NVAR g_radioval = g_cbGDXRadio

	strswitch(cbn)
		case "cbSurgeryNone":
			g_radioval = 1
			break
		case "cbSurgeryGDX":
			g_radioval = 2
			break

	endswitch
	checkbox cbSurgeryNone, value = g_radioval==1
	checkbox cbSurgeryGDX, value = g_radioval==2

	saveExpCardIgor(panelName = s.win)
end

///////////
/// resetExpCardProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-03
/// NOTES: 
///////////
function resetExpCardProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		string cellID = getSetVarString("svDateCode", panelName = B_Struct.win)
		if(strlen(cellID) == 0)
			return 0
		endif

		DFREF cardDFR = getCellExpCardDF(cellID)
		if(dataFolderRefStatus(cardDFR)==0)
			return 0
		endif

		KillDataFolder/Z cardDFR
		expCard(cellID)
	endif
	return 0
end



function bWriteCardProc(s) : ButtonControl
	STRUCT wmButtonACTION &s

	switch(s.eventcode)
		case 2:
			writeExpCard()
			print "saved exp card to txt file"
			break
	endswitch
end

///////////
/// bSaveCardIgorProc
/// AUTHOR: 
/// ORIGINAL DATE: 2024-06-03
/// NOTES: 
///////////
function bSaveCardIgorProc(B_Struct) :ButtonControl
	STRUCT WMButtonAction & B_Struct
	if(B_Struct.eventcode == 2)
		saveExpCardIgor(panelName = B_Struct.win)
		print "saved exp card to Igor"
	endif
	return 0
end
