// foundations for a user friendly interface for 
// curve fitting single exonentials to decaying data
macro fitwrapper( dwn, y0, A, tau, x0)
string dwn = "norm_raw" // data wave name
variable y0 = 0
variable A = -1
variable tau = 0.0086
variable x0 = 0
extra_exp( dwn, y0, A, tau, x0 )
endmacro 

// extrapolates and plots results of fit from t = 0 to end of wave
function extra_exp( dwn, y0, A, tau, x0 )
string dwn // data wave name
variable tau//=0.0087
variable y0//=0
variable A//=-1
variable x0// = 0

string fwn = ""

make/O/N=3 coefs 
coefs[0]=Y0
coefs[1]=A
coefs[2]=tau 
fwn = dwn + "_fit" 
wave dw = $dwn
//duplicate/O dw, $fwn
//wave fw = $fwn 
//fw = 0
fwn = getexpfit( dw, coefs, x0 )
wave fw = $fwn
display/k=1 dw, fw
ModifyGraph rgb($dwn)=(0,0,65535)
ModifyGraph zero=1

end 


function/s getExpFit( dw, coefs, x0 )
wave dw // for timing and plot
wave coefs // function coefficients: Y0, A, tau
variable x0 // x offset for exponential
variable xs // x start
variable xe // x end
string fwn = "fitwave" 
// testing fit function
duplicate/O dw, $fwn
wave fw = $fwn
fw = td_exp( coefs, x0, x ) // this uses the intrinsic wave scaling of dw
//display/k=1 dw, fw 
return fwn 
end

// use this to plot single exponential fit results from curvefit
// delta function: y = 0 for all values less than x0
function td_exp( coefs, x0, x )
wave coefs // wave containing Y0, A, tau
variable x0, x // x0 constant, x is variable
variable yout 
if(x>=x0)
	yout = coefs[0] + coefs[1] * exp( -( x - x0 ) / coefs[2] )
else
	yout = 0
endif
return yout
end

// foundations for a user friendly interface for 
// curve fitting single exonentials to decaying data

// main course
macro fitterExp()

// get user info:
	// data selection
	// fit selection
		// fit range, specify exactly 
		// OR get 80-20, 90-10
			// get range from data if 80-20 or 90-10 is specified
				// run function to return 80-20 or 90-10
		// hold
		// starting coefs
		// more? fit type?
// execute the fit via fit_decay function
	// execute each variation of fit
		// hold = "000"
		// hold = "001" etc
// display output

end

// wrapper for fit function, returns wave with fit, coefs in wave note
function/WAVE fit_decay( w, fitstart, fitend, [holdstring, coefs_io, confirm] )
	// currently restricted to curvefit exp_xoffset
	wave w 				// data for the fit
	variable fitstart	// when to start the fit
	variable fitend 	// where to end the fit
	string holdstring	// optional, default is no holds, size of string should match size of coefs
	wave coefs_io 			// optional, default set below should match coefs for exp_xoffset
	variable confirm 	// set to 1 to show range on input wave with cursors

	// error checking to confirm start and end are valid for input wave
	// insert awesome code here
	
	// handle optional params	
	string hold
	if( paramisdefault( holdstring ) )
		// error checking
		//		confirm correct number of characters, 3
		hold = "" // zero length
	else
		hold = holdstring
	endif 

	if( paramisdefault( coefs_io ) )
		// currently hard coded to exp_xoffset coefs dimensions
	    Make/D/O/N=3 coefs // \\ // \\ // td: set to starting values!!
	    // current starting values are assuming normalized wave, psc decay
	    // preprocessed data wave, baseline = 0amps, peak at 0seconds, etc
        coefs [0]=0.0 //td Y0  HERE
        coefs [1]=-1 //td A 
        coefs [2]=0.006 //td tau 
    else
    	// error checking
    	//		confirm coefs is double
    	//		confirm coefs is correct size (3 elements)
    	duplicate/O coefs_io, coefs 
    endif

    if( paramisdefault( confirm ) )
    	// do not display confirmation
    else 
    	// set up confirmation display, demonstrate range of fit
    	display/k=1 w 
	    cursor A w, fitstart  
	    cursor B w, fitend 
    endif 

    duplicate/O w, fit 
    if( strlen(hold)==0 ) // 
	    CurveFit exp_XOffset, kwCWave=coefs, w(fitstart,fitend) /D=fit
	else
	    CurveFit/H=hold exp_XOffset, kwCWave=coefs, w(fitstart,fitend) /D=fit
	endif		
    // // store the coefs in the wave note, 
    // probably should add info on the fit, 
    // especially x0 and hold 
    string coefs_string 
    coefs_string = num2str(coefs[0]) + ";" + num2str(coefs[1]) + ";" + num2str(coefs[2]) + ";" // always terminate stringlist
    note fit, coefs_string 
    coefs_io = coefs 
return fit 
end 