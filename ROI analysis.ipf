// 2020 04 13 image analysis ROI tools, focus on subtracting bleaching
macro DoProcROImask( noback )
	variable noback // set to 1 to turn off background 

	if( noback == 1 )
		procROI_mask( noback = noback )
	else
	  procROI_mask()
	endif
endmacro


macro doMaskAUC() // now uses the top window
  string gn //= "dFoverF0C0" 
  getwindow kwTopWin activeSW
  dowindow/f $gn
 	//rainbow()
 	//dowaterfall( 0.1 )
	variable bs = 120, be = 300 // baseline region start and end
	variable cs = 300, ce = 480 // CNO region 1
	variable ds = 960, de = 1080 // 2nd CNO region
  maskAUC( gn, bs, be, cs, ce, ds, de )
end

macro testROI()
	print "testROI"
endmacro

macro ROIfake( fakeF0 )
	// wavenames need "_" before and after ROI number for ID
	// place cursors at baseline region
	// needs to be a time wave 
	variable fakeF0 = 1
	print "fakeROI"
	procROI_fakeF0( fakeF0, word="wave" )
	print "done"
end

function/s ROIfromNumber( roi_number, roi_list, [word, offset] ) // returns wave name associated with roi_number
variable roi_number // desired roi
string roi_list // semicolons
string word // separator, used to pull trace number from wavename
variable offset // use this to offset roinumbers from wave numbers
// set this to "_" for wave_0_
// set this FIX THIS SOMETIME SOON!! 
string roi_name=""
// hard coding number after first _
variable i, n, spos1, spos2, rnum, roiflag =0
string rn = "" // this_roi_name 
n = itemsinlist( roi_list )
print "roifromnumber", n, roi_number, roi_list
if( paramisdefault( word ) )
	// check for the underscore
	rn = stringfromlist( i, roi_list )
	spos1 = strsearch( rn, "_", 0 ) // get the first _
	if( spos1 < 0 ) // no underscore, get roi number from 
		string regExp = "([[:alpha:]])([[:digit:]]+)", prefix, roin 

		for( i=0; i< n; i+=1 )
		  rn = stringfromlist( i, roi_list )
			splitstring /E=( regExp ) prefix, roin 
		  //print spos1, spos2, rn[spos1+1], rn[spos2], rn[spos1+1,spos2-1]
		  rnum = str2num( roin )
		  if( rnum == roi_number )
		  	roi_name = rn
		  	//roiflag = 1 
		  endif
		endfor		
	else	
		for( i=0; i< n; i+=1 )
		  rn = stringfromlist( i, roi_list )
		  spos1 = strsearch( rn, "_", 0 ) // get the first _
		  spos2 = strsearch( rn, "_", spos1+1 ) // get the next _
		  //print spos1, spos2, rn[spos1+1], rn[spos2], rn[spos1+1,spos2-1]
		  rnum = str2num( rn[spos1+1,spos2-1] )
		  if( rnum == roi_number )
		  	roi_name = rn
		  	//roiflag = 1 
		  endif
		endfor
	endif
else

	for( i=0; i< n; i+=1 )
	  rn = stringfromlist( i, roi_list )
	  spos1 = strlen(word) // strsearch( rn, word, inf, 1 ) // get the first _
	  spos2 = strlen( rn ) // strsearch( rn, "_", spos1+1 ) // get the next _
	  print spos1, spos2, rn[spos1], rn[spos2-1], rn[spos1,spos2-1]
	  rnum = str2num( rn[spos1,spos2-1] )
	  if( rnum == roi_number )
	  	roi_name = rn
	  	//roiflag = 1 
	  endif
	endfor

endif 

return roi_name
end

function procROI_fakeF0( fakeF0, [word] )

variable fakeF0
string word

string fakeF0wn

string tnl, wn, dwn, cdwn, own, obwn, timen, fwn
string o_dF_wn
string o_dFoverF0_wn 
//string wlist = winlist("*", ";","WIN:2"), mytable = stringfromlist( 0, wlist ) // win:2 is tables

// get the list of waves
string wavel = tracenamelist("", ";", 1); //wavelist("*", ";", "WIN:" + mytable ) //
//print wavel

// get the xwave
string ywn = stringfromlist( 0, wavel )
WAVE/Z yw = $ywn
string xwn = xwavename( "", ywn )
//print "in fakeF0", xw, yw 

variable i=0, n=itemsinlist( wavel ), maxpnts = -inf, pnts = -inf
timen = xwn // stringfromlist( 0, wavel )
WAVE/Z tw = $timen
if ( !waveexists( tw ) )
  //need to make a time wave
  duplicate/O yw, tw 
  tw = p * deltax( yw )
 endif

if( !paramisdefault(word) )
	fakeF0wn = roifromnumber( fakeF0, wavel, word=word )

else
	fakeF0wn = roifromnumber( fakeF0, wavel )
endif

WAVE/Z fakeF0w = $fakeF0wn
print "fakeF0:", fakeF0wn
//print "roi name for fake f0:", roifromnumber( fakeF0, wavel )

variable bstart, bend // get baseline start and end from cursors A and B
bstart = xcsr(A)
bend = xcsr(B)
print "baseline region:", bstart, bend
// get the interval
variable npnts = numpnts( tw )
variable interval = tw[npnts-1] / ( npnts - 1 )
print "*** ave int:", interval, tw[npnts-1], npnts
print "*** first int:", tw[1]-tw[0]

wavestats/Z/Q/R=(bstart,bend) fakeF0w
variable fakeF0_baseline = V_avg, w_baseline = 0

// loop over all waves in graph 
for( i = 0; i < n; i += 1 )
	wn = stringfromlist( i, wavel )
	WAVE/Z w = $wn

	own = "sub_" + wn // Output Wave Name
	fwn = "fake_" + wn 
	duplicate/O w, $own
	duplicate/O fakeF0w, $fwn 

	WAVE ow = $own // Output Wave
	WAVE fw = $fwn // this fake F0

	// get baselines
	wavestats/Z/Q/R=(bstart,bend) ow
    w_baseline = V_avg
    //translate the fake F0 "background"
    fw += w_baseline - fakeF0_baseline

    ow -= fw // subtract background and F0
    ow /= fw // divide by F0
    ow *= 100 // convert to percent

	if( i == 0 )
		display/N=outputC0/K=1 ow
	else
	 	appendtograph/W=outputC0 ow 
	endif
	
	//display/N=rawWithFitC0/K=1 w, fw 

	// else
	// 	obwn = "bs_" + wn
	// 	duplicate/O ow, $obwn
	// 	WAVE obw = $obwn
	// 	obw -= background // FIRST SUBTRACT BACKGROUND
	// 	appendtograph/W=outputC0 obw

	// 	//	CurveFit/Q/X dblexp, kwCWave=W_coef, w  /X=tw /D=dw /M=mw 
		
	// 	CurveFit/Q dblexp, kwCWave=W_coef, obw /M=mw // FIT !!!
		
	// 	W_coef[0] = 0

	// 	// because the mask backfills the destination wave with the 
	// 	// data trace, we have to recreate the fit wave for subtraction	
	// 	cdwn = "cfit0" + wn
	// 	duplicate/O w, $cdwn
	// 	WAVE cdw = $cdwn
	// 	// this calcuates the fit and fills cdw (Curve fit Destination Wave)
	// 	cdw = td_dblexp( W_coef, x ) // this is what we'll call F0 
	// 	appendtograph/W=rawWithFitC0 obw, cdw 
	// 	modifygraph/W=rawWithFitC0 rgb($cdwn)=(0,0,0)

	// 	o_dF_wn = "dF_cfit0" + wn
	// 	duplicate/O obw, $o_dF_wn
	// 	WAVE o_dF = $o_dF_wn
	// 	o_dF -= cdw // SECOND SUBTRACT F0 
	// 	appendtograph/W=dFC0 o_dF

	// 	o_dFoverF0_wn = "dFoverF0_cfit0" + wn
	// 	duplicate/O o_dF, $o_dFoverF0_wn
	// 	WAVE o_dFoverF0 = $o_dFoverF0_wn
	// 	o_dFoverF0 /= cdw // THIRD DIVIDE BY F0 
	// 	appendtograph/W=dFoverF0C0 o_dFoverF0

	//endif

endfor	
end


function procROI_fitpnts( doMask, doRange )

variable doMask, doRange
string tnl, wn, dwn, cdwn, own, obwn, timen
string o_dF_wn
string o_dFoverF0_wn 
string wlist = winlist("*", ";","WIN:2"), mytable = stringfromlist( 0, wlist ) // win:2 is tables

// get the list of waves

// !! this is unreliable !! wave order in table is not consistent in wavelist

string wavel = wavelist("*", ";", "WIN:" + mytable ) //
print mytable, wavel

variable i=0, n=itemsinlist( wavel ), maxpnts = -inf, pnts = -inf
timen = stringfromlist( 0, wavel )
WAVE tw = $timen

wn = stringfromlist( 0, wavel )
WAVE w = $wn
if( doMask )
	duplicate/O w, mw
	mw = 1
	mw[ pcsr(A), pcsr(B) ] = NaN
endif

variable fitstart = 0, fitend = inf
if( doRange )
	fitstart = pcsr(C)
	fitend = pcsr(D)
endif

// get the interval
variable npnts = numpnts( tw )
variable interval = tw[npnts-1] / ( npnts - 1 )
print "*** int:", interval, tw[npnts-1], npnts

make/O/N=5 W_coef
W_coef = 0

// first wave is time! start at i=1, not i=0
for( i = 1; i < n; i += 1 )
	wn = stringfromlist( i, wavel )
	WAVE w = $wn
	setscale/P x, tw[0], interval, w

	//dwn = "fit_" + wn
	//duplicate/O w, $dwn
	//WAVE dw = $dwn  

	own = "sub_" + wn // Output Wave Name
	duplicate/O w, $own
	WAVE ow = $own // Output Wave

	ow = w //- cdw 

	if( i == 1 )
		display/N=outputC0/K=1 ow
		display/N=rawWithFitC0/K=1 w
		//modifygraph rgb($dwn)=(0,0,0)
		display/N=dFC0/K=1
		display/N=dFoverF0C0 /K=1
		duplicate/O ow, background
	else
		obwn = "bs_" + wn
		duplicate/O ow, $obwn
		WAVE obw = $obwn
		obw -= background // FIRST SUBTRACT BACKGROUND
		appendtograph/W=outputC0 obw

		//	CurveFit/Q/X dblexp, kwCWave=W_coef, w  /X=tw /D=dw /M=mw 
		// CurveFit/X=1 dblexp_XOffset ROI_1__Average_[pcsr(A),pcsr(B)] /X=Time__s_ /D 

		if( doMask )
			CurveFit/Q dblexp, kwCWave=W_coef, obw(fitstart,fitend) /M=mw // FIT !!!
		else
			CurveFit/Q dblexp, kwCWave=W_coef, obw(fitstart,fitend) // FIT !!!
		endif	
		W_coef[0] = 0

		// because the mask backfills the destination wave with the 
		// data trace, we have to recreate the fit wave for subtraction	
		cdwn = "cfit0" + wn
		duplicate/O w, $cdwn
		WAVE cdw = $cdwn
		// this calcuates the fit and fills cdw (Curve fit Destination Wave)
		cdw = td_dblexp( W_coef, x ) // this is what we'll call F0 
		appendtograph/W=rawWithFitC0 obw, cdw 
		modifygraph/W=rawWithFitC0 rgb($cdwn)=(0,0,0)

		o_dF_wn = "dF_cfit0" + wn
		duplicate/O obw, $o_dF_wn
		WAVE o_dF = $o_dF_wn
		o_dF -= cdw // SECOND SUBTRACT F0 
		appendtograph/W=dFC0 o_dF

		o_dFoverF0_wn = "dFoverF0_cfit0" + wn
		duplicate/O o_dF, $o_dFoverF0_wn
		WAVE o_dFoverF0 = $o_dFoverF0_wn
		o_dFoverF0 /= cdw // THIRD DIVIDE BY F0 
		appendtograph/W=dFoverF0C0 o_dFoverF0

	endif

endfor	
end

function procROI_mask( [noback] )
variable noback // set to 1 to disable background subtraction
variable doMask = 1
string tnl, wn, dwn, cdwn, own, obwn, timen
string o_dF_wn
string o_dFoverF0_wn 
string wlist = winlist("*", ";","WIN:2"), mytable = stringfromlist( 0, wlist ) // win:2 is tables

// get the list of waves
string wavel = wavelist("*", ";", "WIN:" + mytable ) //
variable i=0, n=itemsinlist( wavel ), maxpnts = -inf, pnts = -inf
timen = stringfromlist( 0, wavel )
WAVE tw = $timen

wn = stringfromlist( 0, wavel )
WAVE w = $wn
if( doMask )
	duplicate/O w, mw
	mw = 1
	mw[ pcsr(A), pcsr(B) ] = NaN
endif

// get the interval
variable npnts = numpnts( tw )
variable interval = tw[npnts-1] / ( npnts - 1 )
print "*** int:", interval, tw[npnts-1], npnts

make/O/N=5 W_coef
W_coef = 0

// first wave is time! start at i=1, not i=0
for( i = 1; i < n; i += 1 )
	wn = stringfromlist( i, wavel )
	WAVE w = $wn
	setscale/P x, tw[0], interval, w

	//dwn = "fit_" + wn
	//duplicate/O w, $dwn
	//WAVE dw = $dwn  

	own = "sub_" + wn // Output Wave Name
	duplicate/O w, $own
	WAVE ow = $own // Output Wave

	ow = w //- cdw 

	if( i == 1 )
		display/N=outputC0/K=1 ow
		display/N=rawWithFitC0/K=1 w
		//modifygraph rgb($dwn)=(0,0,0)
		display/N=dFC0/K=1
		display/N=dFoverF0C0 /K=1
		duplicate/O ow, background
	else
		obwn = "bs_" + wn
		duplicate/O ow, $obwn
		WAVE obw = $obwn
		
		if( paramisdefault( noback ) )
			obw -= background // FIRST SUBTRACT BACKGROUND
		else
			print "*** BACKGROUND SUBTRACTION DISABLED! ***"
		endif

		appendtograph/W=outputC0 obw

		//	CurveFit/Q/X dblexp, kwCWave=W_coef, w  /X=tw /D=dw /M=mw 
		
		CurveFit/Q dblexp, kwCWave=W_coef, obw /M=mw // FIT !!!
		
		//W_coef[0] = 0 // 20210917 this was used prior, not sure why!

		// because the mask backfills the destination wave with the 
		// data trace, we have to recreate the fit wave for subtraction	
		cdwn = "cfit0" + wn
		duplicate/O w, $cdwn
		WAVE cdw = $cdwn
		// this calcuates the fit and fills cdw (Curve fit Destination Wave)
		cdw = td_dblexp( W_coef, x ) // this is what we'll call F0 
		appendtograph/W=rawWithFitC0 obw, cdw 
		modifygraph/W=rawWithFitC0 rgb($cdwn)=(0,0,0)

		o_dF_wn = "dF_cfit0" + wn
		duplicate/O obw, $o_dF_wn
		WAVE o_dF = $o_dF_wn
		o_dF -= cdw // SECOND SUBTRACT F0 
		appendtograph/W=dFC0 o_dF

		o_dFoverF0_wn = "dFoverF0_cfit0" + wn
		duplicate/O o_dF, $o_dFoverF0_wn
		WAVE o_dFoverF0 = $o_dFoverF0_wn
		o_dFoverF0 /= cdw // THIRD DIVIDE BY F0 
		appendtograph/W=dFoverF0C0 o_dFoverF0

	endif

endfor	
end

// process mask
function maskAUC( gn, bs, be, cs, ce, ds, de )
	string gn // graph name 
	variable bs, be // baseline region start and end
	variable cs, ce // CNO region 1
	variable ds, de // 2nd CNO region

 	dowindow/F $gn

  string wn="", tl = tracenamelist( gn, ";", 1 )
  variable nt = itemsinlist( tl ), it = 0
  make/T/O/N=(nt) nw 
  nw = ""
  make/N=(nt)/O bw 
  bw = nan 
  make/N=(nt)/O cw 
  cw = nan 
  make/N=(nt)/O dw 
  dw = nan 

  for( it = 0; it < nt; it += 1 ) // loop over traces
  	
  	wn = stringfromlist( it, tl )
    WAVE w = $wn
    //wavestats/Q/Z/R=(bs, be) w
    nw[ it ] = wn
    bw[ it ] = area( w, bs, be )
    cw[ it ] = area( w, cs, ce )
    dw[ it ] = area( w, ds, de )
  endfor
  edit/K=1 nw, bw, cw, dw
end// maskAUC

function td_dblexp( c, ex )
WAVE c
variable ex
variable why 
why = c[0] + c[1] * exp( -c[2] * ex ) + c[3] * exp( -c[4] * ex )
return why
end

function/S returnTimeFromCXD( keyWave, valueWave )
	// copy and paste metadata from FIJI to table
	// should be two columns, key and value
	string keyWave
	string valueWave

  WAVE/Z/T kw = $keywave
  WAVE/Z vw = $valuewave

	// keys are in field order 1, 10..19, 100..199, etc 2, 20-29, 200-299, etc
// get the number of keys

variable i, nkeys = numpnts( kw ), it=0, nt = 0, itmax=0
string key = ""
variable spos = 0

make/O/N=(nkeys)/D tw  // wave to hold the times
tw = nan

// pre RTC keys, these work for CXD files created by HCImage
string regExp = "([[:alpha:]]+) ([[:digit:]]+) ([[:alpha:]]+)_([[:alpha:]]+)_([[:alpha:]]+)"
string fs="", fn="", under1, under2, under3, RestOfKey=""

// RTC keys "TD GFP Value #0001" times follow odd number starting with 1
//string regExp = "([[:alpha:] ]+)#([[:digit:]]+)"
//string keypart1, keypart2, keypart3//fs="", fn="", under1, under2, under3, RestOfKey=""
string keyID = "Field"
//for( i=0; i<nkeys; i+=1 )
// get the field number
do // looop over keys
  do // loop until field
  	i+=1  // first key never has a field
    key = kw[ i ]
    spos = strsearch( key, keyid, 0 )
  while( (spos < 0) && (i<nkeys) )

  // we are here because the key contains field
  // now get the field number (frame number)
  splitstring/E=(regExp) key, fs, fn, under1, under2, under3
  if( stringmatch( under3, "Start") )
  	//print i, "key:", key, "fs:", fs, "fn:", fn, "under 1,2,3:", under1, under2, under3
    it = str2num( fn ) - 1
    //print i, key, "it:", it, "part1", keypart1, "part2", keypart2, "value:", vw[ i ]
    tw[ it ] = vw[ i ]
    if(it>itmax) 
    	itmax = it
    endif
  endif
  i+=1
while( i < nkeys )
print "hello world!", nkeys, itmax 
//trim tw
deletepoints itmax+1, nkeys-itmax, tw
string tw_int = "tw_i"
print intervalsfromtime("tw")  // auto name is + "_i"
Make/N=100/O tw_i_Hist;DelayUpdate
Histogram/B=1 tw_i,tw_i_Hist;DelayUpdate
Display tw_i_Hist

end
macro tCXD()
	string wn1="cxd1", wn2="cxd2"
	print returntimefromcxd(wn1, wn2)
end
macro tVSI(wn1, wn2)
	string wn1="vsikey", wn2="vsivalue"
	variable offset = 0 // was 500, but that doesn't work with 20211130 headers
	print returntimefromVSI(wn1, wn2, offset)
end
function/S returnTimeFromVSI( keyWave, valueWave, offset )
	// copy and paste metadata from FIJI to table
	// should be two columns, key and value
	string keyWave
	string valueWave
  variable offset // in milliseconds to help when first time is not first
	
	variable stepsize = 7 // the number of entries until the next time value in the list

  WAVE/Z/T kw = $keywave
  // check the type of the value wave
  variable vw_type = wavetype( $valuewave )
  if(  vw_type == 2 )
    WAVE/T vwt = $valuewave // this means the value wave was converted to text on import to igor
  else 
    WAVE vw = $valuewave
  endif

	// SOMETIMES keys are in field order 1, 10..19, 100..199, etc 2, 20-29, 200-299, etc
// get the TOTAL number of keys

variable i, nkeys = numpnts( kw ), it=0, nt = 0, itmax=0
string key = ""
variable spos = 0

make/O/N=(nkeys)/D tw  // wave to hold the times
tw = nan

// RTC keys "TD GFP Value #0001" times follow odd number starting with 1
// 20211130 now the keys are "Value #00001", with zeros filling in for all digits

string obsMeth = "", RegExp="" // first value is the CellSens observation method
// but sometimes it is not, it is simply Value #0001
// if( vw_type == 2 )
// 	obsmeth = vwt[ 0 ] // this is no longer true as of 20211130
// 	regExp = obsMeth + " ([[:alpha:] ]+) #([[:digit:]]+)"
// else
//	regExp = "([[:alpha:] ]+) #([[:digit:]]+)"
	regExp = "Value #([[:digit:]]+)"
//endif

string keypart1, keypart2, keypart3//fs="", fn="", under1, under2, under3, RestOfKey=""
string keyID = "Value"
//for( i=0; i<nkeys; i+=1 )
// get the field number
i=0
it=0
variable temp = nan, tempmax = 0

do // loop until field
	i+=1  // first key never has a field
  key = kw[ i ]
  spos = strsearch( key, keyid, 0 )
while( (spos != 0) && (i<nkeys) )
print "loop until", i, key//  print key

do // looop over keys
  // we are here because the key contains field
  // now get the field number (frame number)
  key = kw[ i ]
  splitstring/E=(regExp) key, keypart1//, keypart2 // fs, fn, under1, under2, under3
  //if( stringmatch( keypart1, keyid ) )

  	//print i, "key:", key, "fs:", fs, "fn:", fn, "under 1,2,3:", under1, under2, under3
  	if( vw_type == 2 )
      temp = str2num( vwt[ i ] )
    else 
    	temp = vw[ i ]
    endif
    if ( temp > offset ) // tw[ it - 1 ] )
    	if ( temp > tempmax )
    		tempmax = temp 
    		tw[ it ] = temp
    		it += 1
    		i += stepsize // this is the number of entries in Key Value list 
    	else
    		print i, it, temp, tempmax
    		debugger

    		i = inf 
    		it = inf 

    	endif
    endif
    //print i, key, "it:", it, "part1", keypart1, "part2", keypart2, "value:", vw[ i ]
    if(it>itmax) 
    	itmax = it
    endif
  //endif
//  i+=1
while( i < nkeys )
print "hello world!", nkeys, itmax 
// trim tw
deletepoints itmax, nkeys-itmax, tw

tw /= 1000 // convert ms to s

string tw_int = "tw_i"
print intervalsfromtime("tw")  // auto name is + "_i"
Make/N=100/O tw_i_Hist;DelayUpdate
Histogram/B=1 tw_i,tw_i_Hist;DelayUpdate
Display tw_i_Hist

end


function sweeptimes( seriesn )
	variable seriesn 


	string ms = "*" + num2str(seriesn) + "*" + "t1"
	string wl = wavelist( ms, ";", "" )
	print wl

end


function/S returnTimeFromVSIOLD( keyWave, valueWave, offset )
	// copy and paste metadata from FIJI to table
	// should be two columns, key and value
	string keyWave
	string valueWave
  variable offset // in milliseconds to help when first time is not first

  WAVE/Z/T kw = $keywave
  // check the type of the value wave
  variable vw_type = wavetype( $valuewave )
  if(  vw_type == 2 )
    WAVE/T vwt = $valuewave
  else 
    WAVE vw = $valuewave
  endif

	// keys are in field order 1, 10..19, 100..199, etc 2, 20-29, 200-299, etc
// get the number of keys

variable i, nkeys = numpnts( kw ), it=0, nt = 0, itmax=0
string key = ""
variable spos = 0

make/O/N=(nkeys)/D tw  // wave to hold the times
tw = nan

// pre RTC keys
//string regExp = "([[:alpha:]]+) ([[:digit:]]+) ([[:alpha:]]+)_([[:alpha:]]+)_([[:alpha:]]+)"
//string fs="", fn="", under1, under2, under3, RestOfKey=""

// RTC keys "TD GFP Value #0001" times follow odd number starting with 1
string obsMeth = "", RegExp="" // first value is the CellSens observation method
// but sometimes it is not, it is simply Value #0001
if( vw_type == 2 )
	obsmeth = vwt[ 0 ]
	regExp = obsMeth + " ([[:alpha:] ]+) #([[:digit:]]+)"
else
	regExp = "([[:alpha:] ]+) #([[:digit:]]+)"
endif

string keypart1, keypart2, keypart3//fs="", fn="", under1, under2, under3, RestOfKey=""
string keyID = "Value"
//for( i=0; i<nkeys; i+=1 )
// get the field number
i=0
it=0
variable temp = nan

do // looop over keys
	do // loop until field
		i+=1  // first key never has a field
	  key = kw[ i ]
	  spos = strsearch( key, keyid, 0 )
	while( (spos < 0) && (i<nkeys) )
	print "loop until", i, key//  print key
  // we are here because the key contains field
  // now get the field number (frame number)
  key = kw[ i ]
  splitstring/E=(regExp) key, keypart1, keypart2 // fs, fn, under1, under2, under3
  if( stringmatch( keypart1, keyid ) )
  	//print i, "key:", key, "fs:", fs, "fn:", fn, "under 1,2,3:", under1, under2, under3
  	if( vw_type == 2 )
      temp = str2num( vwt[ i ] )
    else 
    	temp = vw[ i ]
    endif
    if ( temp > offset ) // tw[ it - 1 ] )
    		tw[ it ] = temp
    		it += 1
    endif
    //print i, key, "it:", it, "part1", keypart1, "part2", keypart2, "value:", vw[ i ]
    if(it>itmax) 
    	itmax = it
    endif
  endif
//  i+=1
while( i < nkeys )
print "hello world!", nkeys, itmax 
// trim tw
deletepoints itmax, nkeys-itmax, tw
string tw_int = "tw_i"
print intervalsfromtime("tw")  // auto name is + "_i"
Make/N=100/O tw_i_Hist;DelayUpdate
Histogram/B=1 tw_i,tw_i_Hist;DelayUpdate
Display tw_i_Hist

end





