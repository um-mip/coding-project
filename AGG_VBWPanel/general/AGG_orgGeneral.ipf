// Kill waves matching string in a data folder
function killWavesInFolder(stringToMatch, dfr)
    string stringToMatch
    DFREF dfr

    String objName
    Variable index = 0

    string matchingWaves = ""
    #if (IgorVersion() >= 9.00)
        matchingWaves = WaveList(stringToMatch, ";", "", dfr)
    #else
        do
            objName = GetIndexedObjNameDFR(dfr, 1, index)
            if (strlen(objName) == 0)
                break
            endif
            if(stringmatch(objName, stringToMatch))
                matchingWaves += objName + ";"
            endif
            index += 1
        while(1)
    #endif

    deleteWavesFromList(matchingWaves, dfr=dfr)
end

// Kill all specific analysis folders matching string
// Main function is to kill the binned subsets of an analysis name
function killSpecAnalysisFolder(stringToMatch)
    string stringToMatch
    
    DFREF dfr = getVBWanalysisDF()

    String objName
    Variable index = 0

    string matchingFolders = ""
    do
        objName = GetIndexedObjNameDFR(dfr, 4, index)
        // print "there is a folder named", objName
        if (strlen(objName) == 0)
            break
        endif
        if(stringmatch(objName, stringToMatch))
            matchingFolders += objName + ";"
            // print "Found match of ", objName
        endif
        index += 1
    while(1)

    variable numMatches =  itemsInList(matchingFolders), iMatch = 0
    for(iMatch = 0; iMatch < numMatches; iMatch ++ )
        string thisMatch = StringFromList(iMatch, matchingFolders)
        DFREF folderToKill = getVBWSpecAnalysisDF(thisMatch)
        KillDataFolder/Z folderToKill
        // print "Deleted: ", thisMatch
    endfor
end

///////////
/// getCellGroup
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: Get the group for a cell, defaults to VBW waves
///////////
function/S getCellGroup(string cellName[, wave/T cellNames, wave/T groupNames])
    if(paramIsDefault(cellNames))
        DFREF panelDFR = getVBWPanelDF()
        if(dataFolderRefStatus(panelDFR)==0)
            return ""
        endif
        Wave/SDFR=panelDFR/T cellNames = cellName
    endif

    if(paramIsDefault(groupNames))
        DFREF panelDFR = getVBWPanelDF()
        Wave/SDFR=panelDFR/T groupNames = groupName
    endif

    if(numpnts(cellNames)!=numpnts(groupNames))
        print "getCellGroup: Unequal number of points in cell and group waves"
        return ""
    endif
    
    FindValue/TEXT=cellName cellNames
    if(V_value == -1)
        print "getCellGroup: Can't find cell in cellNames wave"
        return ""
    endif

    return groupNames[V_value]
end


ThreadSafe function isStringInWave(strToCheck, waveToCheck)
    string strToCheck
    WAVE/T waveToCheck

    variable isInWave = 0

    if(waveExists(waveToCheck))
        Extract /INDX waveToCheck, indxOfMatching, StringMatch(waveToCheck, strToCheck)
        if(numpnts(indxOfMatching)>0)
            isInWave = 1 //There's a match
            // print strToCheck, "is in the wave"
        endif
        KillWaves/Z indxOfMatching
    else
        print "isStringInWave: Wave does not exist"
    endif

    return isInWave
end

ThreadSafe function isValueInWave(variable valToCheck, Wave waveToCheck)
    variable isInWave = 0

    if(waveExists(waveToCheck))
        FindValue/V=(valToCheck) waveToCheck
        if(V_value != -1)
            isInWave = 1
        endif
    else
        print "isValueInWave: Wave does not exist"
    endif

    return isInWave
end

ThreadSafe function/S getCellNameFromSCTWN (sctWaveName)
    string sctWaveName

    string ending = "_sct"
    string cellName = RemoveEnding(sctWaveName, ending)

    return cellName
end

ThreadSafe function AGG_wnoteDuration( wn, dfr )
    string wn // wavename
    DFREF dfr // data folder reference of the wave from which to get note

    string tnote="", intstr="", durstr="", rawn
    variable samp_int=0, npnts=0, duration = 0
    
    // check if there is a duration entry in the wavenote
    WAVE w = dfr:$wn
    tnote = note( w )
    //print tnote
    durstr = stringbykey( "DURATION",  tnote)
    if( strlen( durstr ) > 0 )
        duration = str2num( durstr )
    else
        print "AGG_wnoteDuration: WARNING: COULD NOT GET DURATION FROM NOTE"
    endif
    return duration
end

ThreadSafe function AGG_wnoteVarByKey( wn, key, dfr )
    string wn, key // wavename
    DFREF dfr // data folder reference of the wave from which to get note


	string tnote="", intstr="", durstr="", rawn
	variable samp_int=0, npnts=0, duration = 0
	
	// check if there is a duration entry in the wavenote
	WAVE w = dfr:$wn
	tnote = note( w )
	//print tnote
	durstr = stringbykey( key,  tnote)
	if( strlen( durstr ) > 0 )
		duration = str2num( durstr )
	else
		print "ERROR: COULD NOT GET", key, "FROM NOTE", wn, "in AGG_wnoteVarByKey"
	endif
    return duration
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-04
/// NOTES: Fix line breaks in the wavenote for the original wave note. Can't find keys otherwise
ThreadSafe function/S AGG_wnoteSTRByKey( wn, key, dfr[, suppressWarning])
    string wn, key
    DFREF dfr // data folder reference of the wave from which to get note
    variable suppressWarning

    if(paramIsDefault(suppressWarning))
        suppressWarning = 0
    endif
    

    string tnote="", infostr=""
    
    WAVE w = dfr:$wn

    if(!WaveExists(W))
        print "AGG_wnoteSTRbyKey, wave doesn't exist"
        print "wn", wn, "dfr", getDFREFstring(dfr)
        return infostr
    endif
    tnote = note( w )

    tnote = ReplaceString("\r", tnote, "") // mac new line
    tnote = ReplaceString("\n", tnote, "") // Windows/unix

    //print tnote
    infostr = stringbykey( key,  tnote)
    if( strlen( infostr ) == 0 && !suppressWarning)
            print "ERROR: COULD NOT GET", key, "FROM NOTE ", wn, " AGG_wnoteSTRByKey"
    endif
    return infostr
end

THREADSAFE function/WAVE AGG_waveFromStringList( strlist, requestedName, dfr ) // comma is default delimiter
    string strlist, requestedName
    DFREF dfr // data folder reference of where to create the wave
    variable i, n = itemsinlist( strlist, "," )

    if( n > 0 ) 
        make/N=(n)/O dfr:$requestedname
        WAVE w = dfr:$requestedname
        for( i=0; i<n; i+=1 )
            w[i] = str2num( stringfromlist( i, strlist, "," ) )
        endfor
    else
        // make/N=1/O dfr:$requestedname={NaN} // must return a wave reference!?
        // WAVE w = dfr:$requestedname
        Wave w = $""
    endif	

    return w
end

///////////
/// makeFreeWaveFromStringList
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-05
/// NOTES: Make a free numeric wave from a string list with the delimiter as a comma
/// Helpful for dealing with gap lists for smart conc universe 
///////////
function/WAVE makeFreeWaveFromStringList(strlist) // comma is default delimiter
    string strlist
    variable i, n = itemsinlist( strlist, "," )

    if( n > 0 ) 
        make/N=(n)/FREE/D strWave
        for( i=0; i<n; i+=1 )
            strWave[i] = str2num( stringfromlist( i, strlist, "," ) )
        endfor
    else
        make/N=(0)/FREE/D strWave
    endif	

    return strWave
end

///////////
/// convertNumWaveToStr
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-05
/// NOTES: convert a numeric wave back into a string list 
///////////
function/S convertNumWaveToStr(Wave waveToConvert[, string delimiter])
    if(paramIsDefault(delimiter))
        delimiter = ","
    endif
    
    if(!WaveExists(waveToConvert))
        return ""
    endif

    string varsAsList = ""
    for(variable thisVal : waveToConvert)
        varsAsList += num2str(thisVal) + ","
    endfor

    return varsAsList
end
