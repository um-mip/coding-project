///////////
/// fillCellBurstOutTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-25
/// NOTES: Display the resulting burst analysis for the selected cell and first selected region
///////////
function fillCellBurstOutTable([string tableName, string panelName])
    if(paramIsDefault(tableName))
        tableName = "AGG_bursts#cellBurstOutTable"
    endif
    
    if(paramIsDefault(panelName))
        string subWin
        [panelName, subWin] = splitFullWindowName(tableName)
        if(strlen(panelName)==0)
            panelName = "AGG_bursts"
        endif
    endif
    
    RemoveWavesFromTable_AGG(tableName, "*")
    
    DFREF panelDFR = getVBWPanelDF()

    string cellName = getSelectedItem("cellListBox_burst", hostName = panelName)
    if(strlen(cellName) == 0 )
        return NaN
    endif

    string selRegion = getFirstSelectedCellBurstRegion()
    
    if(strlen(selRegion) == 0)
        return NaN
    endif
    
    DFREF cellAnalysisDFR = getVBWCellAnalysisDF(selRegion, cellName)
    if(dataFolderRefStatus(cellAnalysisDFR)==0)
        return NaN
    endif

    DFREF indivCellOutDFR = getVBWIndivCellOutDF()
    if(dataFolderRefStatus(indivCellOutDFR)==0)
        createVBWIndivCellOutDF()
        DFREF indivCellOutDFR = getVBWIndivCellOutDF()
    endif

    Struct burstOutputWaves outputWaves
    fillBurstOutWavesStrings(outputWaves)

    string analysisTypes = outputWaves.analysis_types
    variable iType = 0, nTypes = itemsInList(analysisTypes)

    for(iType=0; iType<nTypes; iType++)
        string waveN = StringFromList(iType, analysisTypes)
        Wave/SDFR=cellAnalysisDFR cellWave = $waveN
        if(!WaveExists(cellWave))
            continue
        endif

        duplicate/O cellWave, indivCellOutDFR:$waveN
        Wave/SDFR=indivCellOutDFR outWave = $waveN

        AppendToTable/W=$tableName  outWave

        string threeDigitNames = "mbd;ssf;bf;tf;inter;intra;"
        string twoDigitNames = "bww;spb;"
        string integers = "bn;ssn;bFlags;ssFlags;"

        if(ItemsInList(ListMatch(integers, waveN)))
            ModifyTable/W=$tableName format(outWave) = 1
        endif
        
        if(ItemsInList(ListMatch(twoDigitNames, waveN)))
            ModifyTable/W=$tableName format(outWave) = 3, digits(outWave) = 2
        endif
        
        if(ItemsInList(ListMatch(threeDigitNames, waveN)))
            ModifyTable/W=$tableName format(outWave) = 3, digits(outWave) = 3
        endif
    endfor
    ModifyTable/W=$tableName autosize={0, 0, -1, 0, 10}
end