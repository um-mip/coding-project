///////////
/// displayBurstPlot
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-25
/// NOTES: 
///////////
function displayBurstPlot([string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_Bursts"
    endif
    string displayHost = panelName + "#burstCellPlot"
    clearDisplay(displayHost)
    DFREF indivCellOutDFR = getVBWIndivCellOutDF()
    Wave/SDFR=indivCellOutDFR eventHisto, eventHistoByFreq

    if(WaveExists(eventHisto))
        redimension/N=0 eventHisto
    endif

    if(waveExists(eventHistoByFreq))
        redimension/N=0 eventHistoByFreq
    endif

    string selRegion = getFirstSelectedCellBurstRegion()
    string cellName = getSelectedItem("cellListBox_burst")
    if(strlen(cellName)==0 || strlen(selRegion)==0)
        return 0
    endif
    DFREF cellDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = selRegion, outType = "cellHistoGraph", param = "eventHisto")

    if(dataFolderRefStatus(cellDFR)==0)
        return 0
    endif
    

    if(!stringmatch(selRegion, "Full duration"))
        NVAR/Z/SDFR=cellDFR startTime, endTime
        plotPTBHisto(cellName, cellSmartConcDF = cellDFR, histoHost = displayHost, cellSumDFR = indivCellOutDFR, startTime = startTime, endTime = endTime)
    else
        plotPTBHisto(cellName, cellSmartConcDF = cellDFR, histoHost = displayHost, cellSumDFR = indivCellOutDFR)
    endif
end