///////////
/// regionListBoxCellBurstProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-25
/// NOTES: 
///////////
function regionListBoxCellBurstProc(LB_Struct) : ListBoxControl
    STRUCT WMListboxAction & LB_Struct
    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        fillCellBurstOutTable()
        displayBurstPlot(panelName = LB_Struct.win)
    endif
    return 0
end

///////////
/// getFirstSelectedCellBurstRegion
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-25
/// NOTES: Returns the first selected region in the list box on the detect & analyze page of burst panel
///////////
function/S getFirstSelectedCellBurstRegion()
    DFREF panelDFR = getVBWPanelDF()

    Wave/T/SDFR=panelDFR cellRegionNames
    Wave/B/SDFR=panelDFR regionSelWave = regionSelWave_CellBurst

    if(!WaveExists(cellRegionNames) || !WaveExists(regionSelWave))
        return ""
    endif

    FindValue/V=1 regionSelWave
    variable firstMatchingRow = V_Value

    if(V_value == -1)
        return ""
    endif

    string selRegion = cellRegionNames[V_value]
    return selRegion
end

