///////////
/// regionLBPlotProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function regionLBPlotProc(LB_Struct) : ListBoxControl
    STRUCT WMListboxAction & LB_Struct
    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        string target = "AGG_Bursts#regionsPlot"
        clearDisplay(target)
        plotMultiColRegions(target = target)
    endif
    return 0
end