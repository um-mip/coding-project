///////////
/// clearOutputRegionsWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function clearOutputRegionsWave()
    DFREF outDFR = getVBWallCellsOutDF()
    if(dataFolderRefStatus(outDFR)==0)
        return NaN
    endif
    make/N=(0, 0)/D/O outDFR:regionsMultiCol_rows
    make/N=(0, 0)/D/O outDFR:regionsMultiCol_cols
end


///////////
/// makeOutputRegionsWave
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: Default to make the cells be the cols, and the regions be the rows
/// If you want it the other way, then specify it
///////////
function/Wave makeOutputRegionsWave(wave/T regionsWave, string param[, string cellsAs])
    if(paramIsDefault(cellsAs))
        cellsAs = "cols"
    endif
    
    if(!WaveExists(regionsWave) || numpnts(regionsWave)==0)
        return NaN
    endif

    DFREF panelDFR = getVBWPanelDF()
    Wave/SDFR=panelDFR/T origCellNames = cellName, origGroupNames = groupName
    DFREF outDFR = getVBWallCellsOutDF()

    duplicate/O origCellNames, outDFR:cellNames
    duplicate/O origGroupNames, outDFR:groupNames
    Wave/SDFR=outDFR/T cellNames, groupNames
    Sort/A=2 {groupNames, cellNames}, groupNames, cellNames

    if(!WaveExists(cellNames))
        return NaN
    endif

    variable iRegion = 0, nRegions = numpnts(regionsWave), iCell = 0, nCells = numpnts(cellNames)

    strswitch (cellsAs)
        case "rows":
            make/N=(nCells, nRegions)/D/O outDFR:regionsMultiCol_rows/Wave=regionsMultiCol
            break
        case "cols":
            make/N=(nRegions, nCells)/D/O outDFR:regionsMultiCol_cols/Wave=regionsMultiCol
            break
        default:
            make/N=(nRegions, nCells)/D/O outDFR:regionsMultiCol_cols/Wave=regionsMultiCol
            break
    endswitch

    regionsMultiCol = NaN

    if(strlen(param)==0)
        return NaN
    endif

    NVAR/Z/SDFR=panelDFR  selectedBW
    variable bw_index = getIndexOfBW(selectedBW)
    
    // debugger
    string regionName
    for(iRegion=0; iRegion<nRegions; iRegion++)
        regionName = regionsWave[iRegion]
        strswitch (cellsAs)
            case "rows":
                SetDimLabel 1, iRegion, $regionName, regionsMultiCol
                break
            case "cols":
                SetDimLabel 0, iRegion, $regionName, regionsMultiCol
                break
            default:
                SetDimLabel 0, iRegion, $regionName, regionsMultiCol
                break
        endswitch
    endfor

    for(iCell=0; iCell<nCells; iCell++)
        string cellName = cellNames[iCell]
        strswitch (cellsAs)
            case "rows":
                SetDimLabel 0, iCell, $cellName, regionsMultiCol
                break
            case "cols":
                SetDimLabel 1, iCell, $cellName, regionsMultiCol
                break
            default:
                SetDimLabel 1, iCell, $cellName, regionsMultiCol
                break
        endswitch

        for(iRegion=0; iRegion<nRegions; iRegion++)
            regionName = regionsWave[iRegion]
            DFREF cellRegionDFR = getVBWCellAnalysisDF(regionName, cellName)
            if(dataFolderRefStatus(cellRegionDFR)==0)
                continue
            endif

            Wave/SDFR=cellRegionDFR propWave = $param
            if(!WaveExists(propWave))
                continue
            endif

            variable thisVal = propWave[bw_index]

            strswitch (cellsAs)
                case "rows":
                    regionsMultiCol[iCell][iRegion] = thisVal
                    break
                case "cols":
                    regionsMultiCol[iRegion][iCell] = thisVal
                    break
                default:
                    regionsMultiCol[iRegion][iCell] = thisVal
                    break
            endswitch
        endfor
    endfor

    // edit/k=1/N=burstPropMulti regionsMultiCol.l, regionsMultiCol.ld
    return regionsMultiCol
end

///////////
/// plotMultiColRegions
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function plotMultiColRegions([string target])
    clearOutputRegionsWave()
    if(paramIsDefault(target))
        target = "AGG_Bursts#regionsPlot"
    endif

    DFREF panelDFR = getVBWPanelDF()

    // Get the selected regions 
    DFREF outDFR = getVBWallCellsOutDF()
    DFREF cellsOutDFR = getAllCellsOutputDF()
    
    Wave/SDFR=cellsOutDFR/T allRegionNames
    Wave/SDFR=outDFR allRegionNames_sel
    variable iTotalRegion = 0, nTotalRegions = numpnts(allRegionNames)

    string regionsList = ""
    for(iTotalRegion=0; iTotalRegion<nTotalRegions; iTotalRegion++)
        string totalRegionName = allRegionNames[iTotalRegion]

        variable isSelected = allRegionNames_sel[iTotalRegion]
        if(isSelected)
            if(stringmatch(totalRegionName, "Full duration"))
                string tabLabel = getSelectedTab(tabCtrlName = "BurstTabs", panelName = "AGG_bursts")
                if(stringmatch(tabLabel, "Region Plot") || stringmatch(tabLabel, "region table"))
                    print "plotMultiColRegions: Full duration not included. Please make other selections if you haven't already"
                endif
                continue
            endif
            regionsList += totalRegionName + ";"
        endif
    endfor
    wave/T regionsWave = ListToTextWave(regionsList, ";")

    if(!WaveExists(regionsWave) || numpnts(regionsWave)==0)
        return NaN
    endif

    // What parameter?
    string selBParam = getSelectedItem("bParamsListBox", hostName = "AGG_Bursts")
    
    // Prep multi-col wave
    Wave regionsMultiCol = makeOutputRegionsWave(regionsWave, selBParam, cellsAs = "cols") 
    Wave/SDFR=panelDFR/T uniqueGroups
    Wave/SDFR=outDFR/T groupNames, cellNames // sorted by group versions of waves

    variable nCells = DimSize(regionsMultiCol, 1), iCell = 0

    if(!winExists(target))
        Display/K=1/N=$target
    else
        clearDisplay(target)
    endif

    variable iGroup, nGroups = numpnts(uniqueGroups)
    Wave rgb_table = makeColorTableWave(nGroups)

    // Add each cell
    variable plottingSomething = 0
    for(iCell=0; iCell<nCells; iCell++)
        string cellN = GetDimLabel(regionsMultiCol, 1, iCell)
        plottingSomething = 1
        
        // Plots each cell as its own trace, using the labels of the rows as the x-values
        AppendToGraph/W=$target regionsMultiCol[][iCell]/TN=$cellN vs _labels_

        // Connect them with lines and put a small dot for each value
        ModifyGraph/W=$target mode($cellN) = 4, marker($cellN) = 19, mSize($cellN)=1

        // Try to find the group to decide how to color the trace
        string groupN = getCellGroup(cellN)
        if(strlen(groupN)==0)
            continue
        endif

        FindValue/Text=groupN uniqueGroups
        if(V_value == -1)
            continue
        endif

        ModifyGraph/W=$target rgb($cellN) = (rgb_table[V_value][0], rgb_table[V_value][1], rgb_table[V_value][2])
    endfor

    // Add mean and SEM for each group
    variable nRegions = DimSize(regionsMultiCol, 0)
    string legendString = ""
    for(iGroup=0; iGroup<nGroups; iGroup++)
        make/N=(nRegions, 0)/D/O outDFR:$("regionsMultiCol_g" + num2str(iGroup))/Wave=groupMultiCol
        CopyDimLabels/Rows=0 regionsMultiCol, groupMultiCol
        
        // Get the cells that are in this group
        Wave/T groupCells = getTextWaveSubsetFromGroup(groupNames, cellNames, groupNum = iGroup)
        for(iCell=0; iCell<nCells; iCell++)
            cellN = GetDimLabel(regionsMultiCol, 1, iCell)
            variable isInGroup = isStringInWave(cellN, groupCells)
            if(isInGroup)
                // if cell is in group, add it to the group's multicolumn wave
                redimension/N=(nregions, DimSize(groupMultiCol, 1)+1) groupMultiCol
                groupMultiCol[][DimSize(groupMultiCol, 1)-1] = regionsMultiCol[p][iCell]
                SetDimLabel 1, DimSize(groupMultiCol, 1)-1, $cellN, groupMultiCol
            endif
        endfor

        if(DimSize(groupMultiCol, 1) == 0)
            continue
        endif

        // For the mean/sem functions to work correctly, need to have the cells in the rows and regions in the columns
        MatrixTranspose groupMultiCol
        Wave meanWave = getGroupMean(groupMultiCol, meanName = "g" +num2str(iGroup) + "Mean")
        Wave errorWave = getGroupSEM(groupMultiCol, semName = "g" + num2str(iGroup) + "SEM")

        // Put back the region labels
        CopyDimLabels/Cols=1 groupMultiCol, meanWave
        CopyDimLabels/Cols=1 groupMultiCol, errorWave

        // Flip it back to the cells in the columns and regions in the rows
        MatrixTranspose meanWave
        MatrixTranspose errorWave

        // Add the mean and sem
        string avgStr = uniqueGroups[iGroup] + "_avg"
        AppendToGraph/W=$target meanWave[][%avg]/TN=$avgStr vs _labels_
        ModifyGraph/W=$target mode($avgStr) = 4, marker($avgStr) = 19, mSize($avgStr)=3, rgb($avgStr) = (rgb_table[iGroup][0], rgb_table[iGroup][1], rgb_table[iGroup][2]), lsize($avgStr)=3

        ErrorBars/W=$target $avgStr Y, wave=(errorWave[*][%sem], errorWave[*][%sem])

        if(strlen(legendString)>0)
            legendString += "\n"
        endif
        
        legendString += "\s(" + avgStr + ")" + uniqueGroups[iGroup]
    endfor

    if(plottingSomething)
        STRUCT burstOutputWaves outputWaves
        fillBurstOutWavesStrings(outputWaves)
        string niceName = StringByKey(selBParam, outputWaves.niceNamesFromWave)
        if(strlen(niceName))
            Label/W=$target left, niceName
        endif
        if(strlen(legendString))
            Legend/W=$target /N=text0 /C/J/B=1/G=(0,0,0) legendString
        endif
        SetAxis/W=$target/Z/E=3/A left
    endif
end