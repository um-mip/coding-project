///////////
/// updateBurstInfoProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: 
///////////
function updateBurstInfoProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        updateCellsOutTable(doReconcat = 0)
        fillBurstInfoTable("AGG_Bursts#burstInfoTable")
    endif
    return 0
end

///////////
/// fillBurstInfoTable
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: Ultimately need to add a variable to the panel for display time units
///////////
function fillBurstInfoTable(string tableName[, variable timeUnits])
    DFREF panelDFR = getVBWPanelDF()
    if(paramIsDefault(timeUnits))
        NVAR/Z/SDFR=panelDFR regionTimeUnits
        if(NVAR_Exists(regionTimeUnits) && numtype(regionTimeUnits)==0)
            timeUnits = regionTimeUnits
        else
            timeUnits = 2 // minutes
        endif
    endif

    RemoveWavesFromTable_AGG(tableName, "*")

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR=cellsOutDFR/T cellName, groupName, groupName2, groupName3

    duplicate/O/T cellName, panelDFR:cellName
    duplicate/O/T groupName, panelDFR:groupName
    duplicate/O/T groupName2, panelDFR:groupName2
    duplicate/O/T groupName3, panelDFR:groupName3
    Wave/SDFR=panelDFR/T cellName, groupName, groupName2, groupName3

    AppendToTable/W=$tableName cellName, groupName, groupName2, groupName3
    ModifyTable/W=$tableName width(cellName) = 90
    
    variable iCell = 0, nCells = numpnts(cellName)

    make/N=(nCells)/D/O panelDFR:totalDur/Wave=totalDur
    make/N=(nCells)/D/O panelDFR:gapDur/Wave=gapDur
    make/N=(nCells)/D/O panelDFR:recDur/Wave=recDur
    make/N=(nCells)/D/O panelDFR:percGap/Wave=percGap

    totalDur = NaN; gapDur = NaN; recDur = NaN; percGap = NaN
    AppendToTable/W=$tableName totalDur, gapDur, recDur, percGap

    Wave/T regionNames = getAllRegionNames()
    variable iRegion = 0, nRegions = numpnts(regionNames)

    for(string regionName : regionNames)
        make/N=(nCells)/D/O panelDFR:$(regionName+"_start")/Wave=startTimes
        make/N=(nCells)/D/O panelDFR:$(regionName+"_end")/Wave=endTimes

        startTimes = NaN; endTimes = NaN;

        AppendToTable/W=$tableName startTimes, endTimes
    endfor

    for(iCell=0; iCell<nCells; iCell++)
        string thisCell = cellName[iCell]

        DFREF cellDFR = getCellSummaryDF(thisCell)
        if(dataFolderRefStatus(cellDFR)==0)
            continue
        endif
        
        NVAR/Z/SDFR=cellDFR cellTotalDur = totalDur, cellGapDur = totalGapDur, cellRecDur = totalRecDur
        
        if(NVAR_Exists(cellTotalDur) && NVAR_Exists(cellTotalDur) && NVAR_Exists(cellRecDur))
            switch (timeUnits)
                case 1: //sec
                    totalDur[iCell] = cellTotalDur
                    gapDur[iCell] = cellGapDur
                    recDur[iCell] = cellRecDur
                    percGap[iCell] = cellGapDur / cellTotalDur * 100
                    break
                case 2: //min
                    totalDur[iCell] = cellTotalDur / 60
                    gapDur[iCell] = cellGapDur / 60
                    recDur[iCell] = cellRecDur / 60
                    percGap[iCell] = cellGapDur / cellTotalDur * 100
                    break
                default:
                    totalDur[iCell] = cellTotalDur
                    gapDur[iCell] = cellGapDur
                    recDur[iCell] = cellRecDur
                    percGap[iCell] = cellGapDur / cellTotalDur * 100
                    break
            endswitch
        endif

        for(regionName : regionNames)
            Wave/SDFR=panelDFR startTimes = $(regionName + "_start")
            Wave/SDFR=panelDFR endTimes = $(regionName + "_end")

            [variable startTime, variable endTime] = getCellRegionStartEndTime(thisCell, regionName)

            if(!numtype(startTime)==0 || !numtype(endTime)==0)
                continue
            endif

            switch (timeUnits)
                case 1: //sec
                    startTimes[iCell] = startTime
                    endTimes[iCell] = endTime
                    break
                case 2: //min
                    startTimes[iCell] = startTime / 60
                    endTimes[iCell] = endTime / 60
                    break
                default:
                    startTimes[iCell] = startTime
                    endTimes[iCell] = endTime
                    break
            endswitch
            
        endfor
    endfor
end

Function updateRegionUnitsProc_v2(cb) : CheckBoxControl
    STRUCT WMCheckboxAction& cb
    
    // don't change things when the control is killed
    //Otherwise, it switches the value to 1 when macro closed
    if(cb.eventcode != -1) 
        changeRegionUnits_burstPanel(cb.win, cb.ctrlName)
        
    endif


    return 0
End

///////////
/// changeRegionUnits_burstPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: 
///////////
function changeRegionUnits_burstPanel(string panelName, string ctrlName)
    DFREF panelDFR = getVBWPanelDF()
    NVAR regionTimeUnits= panelDFR:regionTimeUnits
    variable previousUnits = regionTimeUnits

    Wave/Wave startWaves = getWavesInTable(panelName + "#burstInfoTable", "*_start")
    Wave/Wave endWaves = getWavesInTable(panelName + "#burstInfoTable", "*_end")
    Wave/Wave durWaves = getWavesInTable(panelName + "#burstInfoTable", "*Dur")

    concatenate/FREE/Wave/NP {startWaves, endWaves, durWaves}, regWaves
    variable iRegWave = 0, nRegWaves = numpnts(regWaves)
    for(iRegWave=0; iRegWave<nRegWaves; iRegWave++)
        wave thisRegWave = regWaves[iRegWave]
        if(!WaveExists(thisRegWave))
            continue
        endif

        string waveN = nameOfWave(thisRegWave)

        strswitch (ctrlName)
            case "regionsAsSec_cb":
                regionTimeUnits = 1
                if(previousUnits == 1)
                    continue // don't need to change anything
                endif

                thisRegWave *= 60
                break
            case "regionsAsMin_cb":
                regionTimeUnits = 2
                if(previousUnits == 2)
                    continue // don't need to change anything
                endif

                thisRegWave /= 60
                break
            default:
                
                break
        endswitch
    endfor
    CheckBox regionsAsSec_cb, value= regionTimeUnits==1
    CheckBox regionsAsMin_cb, value= regionTimeUnits==2
end