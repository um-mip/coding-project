

///////////
/// plotBurstParam
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: Switching to use generic plot group data instead of violin plot
///////////
function plotBurstParam(string bParam[, string hostN])
    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif

    string target = hostN + "#bOutDisplay"
    
    DFREF allCellsOutByBW_dfr = getVBWallCellsOutDF()
    wave dataWave = allCellsOutByBW_dfr:$bParam

    DFREF panelDFR = getVBWPanelDF()
    Wave/T groupNames = panelDFR:groupName

    if(!WaveExists(dataWave) || !WaveExists(groupNames))
        return NaN
    endif

    string labelList = ""
    labelList += "bn:Burst Number;"
    labelList += "mbd:Mean Burst Duration (s);"
    labelList += "spb:Spikes Per Burst;"
    labelList += "bf:Burst Frequency (Hz);"
    labelList += "ssn:# Single Spikes;"
    labelList += "ssf:Single Spike Frequency (Hz);"
    labelList += "tf:Total Frequency (Hz);"
    labelList += "inter:Interevent Interval (s);"
    labelList += "intra:Intraburst Interval (s);"
    labelList += "bFlags:# bursts near gaps;"
    labelList += "ssFlags:# single spikes near gaps;"
    
    string leftAxisLabel = stringbykey(bParam, labelList)

    plotGroupData(groupNames, dataWave, displayHost = target, yLabel = leftAxisLabel)
end