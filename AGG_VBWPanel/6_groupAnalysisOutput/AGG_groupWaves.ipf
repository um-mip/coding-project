function getUniqueTrtGroupsWave()
    DFREF panelDFR = getVBWPanelDF()

    WAVE/T trtGroup = panelDFR:groupName
    if(numpnts(trtGroup)>0)
        if(numpnts(trtGroup) == 1)
            duplicate/O/t trtGroup, panelDFR:uniqueGroups
        else
            FindDuplicates /RT=panelDFR:uniqueGroups trtGroup // Gets rid of duplicates
            WAVE/T uniqueGroups = panelDFR:uniqueGroups // Get local reference

            Extract/T /O uniqueGroups, uniqueGroups, strlen(uniqueGroups) > 0 // Get rid of blank group
            // print "the treatment groups are", uniqueGroups
        endif
    endif
end

// Creates a cellName_group# and cellIndex_group# wave for each group
function makeTrtGroupWaves()
    DFREF panelDFR = getVBWPanelDF()

    killWavesInFolder("*_group*", panelDFR)

    getUniqueTrtGroupsWave()

    Wave/T uniqueGroups = panelDFR:uniqueGroups

    variable numGroups = numpnts(uniqueGroups)
    variable iGroup
    string appendix, trtGroupName

    WAVE/T cellNames = panelDFR:cellName, groupNames = panelDFR:groupName
    variable numCells = numpnts(cellNames), iCell
    string thisCellName, thisCellGroup

    for(iGroup = 0; iGroup < numGroups; iGroup++)
        trtGroupName = uniqueGroups[iGroup]

        appendix = "_group" + num2str(iGroup)
        
        string groupedCellsWN = "cellName" + appendix
        string indexKeyWN = "cellIndex" + appendix

        Extract/O/INDX groupNames, panelDFR:$indexKeyWN, stringmatch(groupNames, trtGroupName)
        WAVE indexKey = panelDFR:$indexKeyWN
        
        variable numCellsInGroup = numpnts(indexKey)
        make/O/T/N=(numCellsInGroup) panelDFR:$groupedCellsWN/WAVE=groupedCells
        variable matchingIndex = 0
        for(iCell = 0; iCell < numCellsInGroup; iCell ++)
            matchingIndex = indexKey[iCell]
            groupedCells[iCell] = cellNames[matchingIndex]
        endfor

        // Get rid of extra points
        redimension/N=(numCellsInGroup) groupedCells, indexKey
    endfor
end