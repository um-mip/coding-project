////////////////////////////////////////////////////////////////////////
///////////////////// UPDATE GROUP OUTPUT TABLE \\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////////////////////////////////
function updateGroupOutputProc (LB_struct) : ListBoxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        updateGroupOutput(hostN = LB_Struct.win)
        plotMultiColRegions(target = "AGG_Bursts#regionsPlot")
    endif

    return 0
end

function updateGroupOutput_bw_Proc(SV_struct) : SetVariableControl
    STRUCT WMSetVariableAction &SV_struct

    if(SV_struct.eventcode == 1 || SV_struct.eventcode == 2 || SV_struct.eventcode == 6)
        updateGroupOutput(hostN = SV_Struct.win)
        plotMultiColRegions(target = "AGG_Bursts#regionsPlot")
    endif
    return 0
end

function updateGroupOutput([string hostN])
    DFREF panelDFR = getVBWPanelDF()

    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif
    
    string regionLB, bParamsLB
    strswitch (hostN)
        case "AGG_Bursts":
            regionLB = "regionListBox_maxBW"
            bParamsLB = "bParamsListBox"
            break
        case "AGG_VBW":
            regionLB = "regListBox_tab3"
            bParamsLB = "bParamsListBox_tab5"
            break
        default:
            regionLB = "regionListBox_maxBW"
            bParamsLB = "bParamsListBox"
            break
    endswitch

    updateOutputBurstParamsWaves("", nan)

    string bOutdisplay = hostN + "#bOutDisplay"
    clearDisplay(bOutDisplay)

    string selRegion = getSelectedItem(regionLB, hostName = hostN)
    string selBParam = getSelectedItem(bParamsLB, hostName = hostN)

    DFREF regionDFR = getVBWSpecAnalysisDF(selRegion)
    if(dataFolderRefStatus(regionDFR)==0)
        print "updateGroupOutput_bw_Proc: Region DFR doesn't exist"
        return NaN
    endif

    NVAR/Z/SDFR=panelDFR selectedBW, vbwmin_used, vbwmax_used
    if(selectedBW > vbwmax_used || selectedBW < vbwmin_used)
        print "updateGroupOutput_bw_Proc: selected BW is out of range"
        return NaN
    endif

    if(strlen(selRegion)==0)
        return NaN
    endif
    
    updateOutputBurstParamsWaves(selRegion, selectedBW)

    if(strlen(selBParam)==0)
        return NaN
    endif

    if(!winExists(bOutDisplay))
        return NaN
    endif

    plotBurstParam(selBParam)
end