function getIndexOfBW(bw)
    variable bw

    DFREF panelDFR = getVBWPanelDF()

    WAVE burstWindows = panelDFR:burstWindows

    findValue / V = (bw) burstWindows
    variable bw_index = V_value

    return bw_index
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-28
/// NOTES: make this actually save the values and then pull them programatically
/// instead of returning with a keyed string
function storeBurstParamsAtBW (variable bw, DFREF cellAnalysisFolder, DFREF burstValsFolder)
    STRUCT burstOutputWaves outputWaves
    fillBurstOutWavesStrings(outputWaves)

    string analysisTypes = outputWaves.analysis_types
    variable iAnalysis = 0, nAnalyses = itemsInList(analysisTypes)

    variable bw_index = getIndexOfBW(bw)

    for(iAnalysis=0; iAnalysis<nAnalyses; iAnalysis++)
        string analysisType = StringFromList(iAnalysis, analysisTypes)
        if(stringmatch(analysisType, "bww"))
            continue
        endif

        NVAR/Z/SDFR=burstValsFolder propVar = $analysisType
        propVar = NaN
        
        if(dataFolderRefStatus(cellAnalysisFolder)==0)
            continue
        endif

        Wave/SDFR=cellAnalysisFolder propWave = $analysisType
        if(!WaveExists(propWave))
            continue
        endif

        propVar = propWave[bw_index]
    endfor

    return 1
end

function updateOutputBurstParamsWaves(selectedAnalysis, bw)
    string selectedAnalysis
    variable bw

    DFREF analysisDFR = getVBWSpecAnalysisDF(selectedAnalysis)

    DFREF panelDFR = getVBWPanelDF()

    WAVE/T cellNamesWave = panelDFR:cellName
    if(waveExists(cellNamesWave))
    
        variable numCells = numpnts(cellNamesWave)

        variable iCell = 0
        string thisCellName, cellFolder

        DFREF groupTableOutDFR = getVBWallCellsOutDF()
        NewDataFolder/O groupTableOutDFR:vars
        DFREF cellVarsDFR = groupTableOutDFR:vars


        WAVE /T /SDFR = groupTableOutDFR cellName
        WAVE /SDFR = groupTableOutDFR bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags
        cellName = cellNamesWave

        redimension/N=(numCells) cellName
        redimension/N=(numCells) bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags
        cellName = cellNamesWave

        bn = nan
        mbd = nan
        spb = nan
        bf = nan
        ssn = nan
        ssf = nan
        tf = nan
        inter = nan
        intra = nan
        bFlags = nan
        ssFlags = nan

        for(iCell = 0; iCell < numCells; iCell ++ )
            thisCellName = cellNamesWave[iCell]

            DFREF cellAnalysisDFR = getVBWCellAnalysisDF(selectedAnalysis, thisCellName)

            if(DataFolderRefStatus(cellAnalysisDFR) != 0)
                // This makes the appropriate NVARs
                STRUCT burstVals burstVals
                StructFill/AC=3/SDFR=cellVarsDFR burstVals

                storeBurstParamsAtBW(bw, cellAnalysisDFR, cellVarsDFR)

                STRUCT burstOutputWaves outputWaves
                fillBurstOutWavesStrings(outputWaves)

                string analysisTypes = outputWaves.analysis_types
                variable iAnalysis = 0, nAnalyses = itemsInList(analysisTypes)

                for(iAnalysis=0; iAnalysis<nAnalyses; iAnalysis++)
                    string analysisType = StringFromList(iAnalysis, analysisTypes)
                    if(stringmatch(analysisType, "bww"))
                        continue
                    endif

                    NVAR/Z/SDFR=cellVarsDFR propVar = $analysisType
                    
                    if(!NVar_Exists(propVar))
                        continue
                    endif

                    Wave/SDFR=groupTableOutDFR propWave = $analysisType
                    if(!WaveExists(propWave))
                        continue
                    endif
                    propWave[iCell] = propVar
                endfor

            endif

        endfor
        KillDataFolder/Z cellVarsDFR
    else
        print "updateOutputBurstParamsWaves - cell names wave from panel doesn't exist"
    endif

end