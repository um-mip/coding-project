///////////
/// createBurstInfoTab
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: 
///////////
function createBurstInfoTab(panelName, tabNum, panelSize)
    string panelName
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabDisplays = ""

    edit /N=burstInfoTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.1, 0.98, 0.98)
    ModifyTable autosize={0, 0, -1, 0, 10}
    tabDisplays += "burstInfoTable;"

    fillBurstInfoTable(panelName + "#burstInfoTable")

    ModifyTable showParts=126

    [variable xPos, variable yPos] = xyRelPanel(0.2, 0.06, panelName = panelName)
    Button updateBurstInfoButton, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button updateBurstInfoButton, title = "Update table", proc = updateBurstInfoProc
    tabControls += "updateBurstInfoButton;"
    

    xPos = panelSize.xPosPnts
    yPos = panelSize.yPosPnts
    string infoText = "This panel pulls cells from the AGG_Events panel.\n\nBe sure that the cell output table in AGG_Events is up to date.\n\n"
    infoText += "Enter the appropriate region times for each cell within the cell concat tab of AGG_Events.\n\n"
    
    NewNoteBook/F=1/OPTS=(2^3)/V=0/W=(0.02, 0.1, 0.19, 0.4)/N=burstInfoNote/Host=$panelName
    NoteBook AGG_Bursts#burstInfoNote, text=infoText, autosave=0
    tabDisplays += "burstInfoNote;"

    // Region Times as Seconds or Minutes
    DFREF panelDFR = getVBWPanelDF()
    variable/G panelDFR:regionTimeUnits/N=regionTimeUnits

    if(!regionTimeUnits)
        regionTimeUnits = 2 //if no value, default to 2 (min)
    endif

    xPos = panelSize.xPosPnts
    yPos = posRelPanel(0.5, "height", panelName = panelName)

    TitleBox regionUnits_text pos = {xPos, yPos}, title = "Select units of region\nstart/end times:", frame=0, fstyle=3
    tabControls += "regionUnits_text;"
    yPos += panelSize.buttonHeight*2
    CheckBox regionsAsSec_cb pos = {xPos, yPos}, proc = updateRegionUnitsProc_v2, title = "Seconds", value = (regionTimeUnits == 1), mode =1
    tabControls += "regionsAsSec_cb;"
    CheckBox regionsAsMin_cb pos = {xPos + panelSize.buttonWidth/2, yPos}, proc = updateRegionUnitsProc_v2, title = "Minutes", value = (regionTimeUnits == 2), mode = 1
    tabControls += "regionsAsMin_cb;"



    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end