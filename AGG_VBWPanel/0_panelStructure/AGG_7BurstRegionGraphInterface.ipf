///////////
/// createBurstRegionGraphInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function createBurstRegionGraphInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""

    tabControls += "selectBWEntry;"
    tabControls += "selectBWSlider;"
    tabControls += "bParamsListBox;"
    string tabNumStr = num2str(tabNum)

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    DFREF allCellsOutByBW_dfr = getVBWallCellsOutDF()

    Display /W=(0.2, 0.1, 0.98, 0.98)/N=regionsPlot/Hide=(1) /L/B /HOST=$panelName
    tabDisplays += "regionsPlot;"
    Button saveRegionsPlot, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="RegionsPlot", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveRegionsPlot;"   

    TitleBox regionSelectPlot_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select region(s):", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelectPlot_text;"
    yPos += buttonHeight

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR=cellsOutDFR/T allRegionNames
    if(!WaveExists(allRegionNames))
        make/T/O/N=0 cellsOutDFR:allRegionNames/Wave=allRegionNames
    endif

    Wave regionSelWave = storeAllRegionsSelectWave(allRegionNames, whichSelWave = "regionsOutput")

    ListBox regionListBox_plot, mode=4, listwave = allRegionNames, selwave=regionSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight * 0.5}, proc = regionLBPlotProc
    tabControls += "regionListBox_plot;"
    yPos += panelSize.listBoxHeight * 0.5

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end