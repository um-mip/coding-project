///////////
/// createBurstGraphOutInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function createBurstGraphOutInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    
    tabControls += "regionSelectMaxBW_text;"
    tabControls += "regionListBox_maxBW;"
    tabControls += "selectBWEntry;"
    tabControls += "selectBWSlider;"

    string tabNumStr = num2str(tabNum)

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    DFREF allCellsOutByBW_dfr = getVBWallCellsOutDF()
    // WAVE/D multColBOut = allCellsOutByBW_dfr:multColBOut

    // if(!WaveExists(multColBOut))
    //     make/O/D/N=(0, 0) allCellsOutByBW_dfr:multColBOut/WAVE=multColBout
    // endif

    Display /W=(0.2, 0.1, 0.98, 0.98)/N=bOutdisplay/Hide=(1) /L/B /HOST=$panelName
    tabDisplays += "bOutDisplay;"
    Button saveBOutDisplay, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="BOutDisplay", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveBOutDisplay;"   

    make/O/T/N=(11) panelDFR:bParams = {"bn","mbd","spb","bf","ssn","ssf","tf","inter","intra","bFlags","ssFlags"}
    Wave/SDFR=panelDFR bParams

    yPos += listBoxHeight * 0.5 + buttonHeight * 5
    ListBox bParamsListBox listwave = bParams, mode = 2, proc = updateGroupOutputProc, pos={xPos, yPos}, size = {listBoxWidth, listBoxHeight}
    tabControls += "bParamsListBox;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end