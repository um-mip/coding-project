///////////
/// createBurstDetectAnalysisTab
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: 
///////////
function createBurstDetectAnalysisTab(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    
    string tabNumStr = num2str(tabNum)

    DFREF eventsDFR = getEventsPanelDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    variable buttonWidth = panelSize.buttonWidth
    variable buttonHeight = panelSize.buttonHeight
    variable listBoxWidth = panelSize.listBoxWidth
    variable listBoxHeight = panelSize.listBoxHeight

    Wave/SDFR=eventsDFR cellName
    make/N=(numpnts(cellName))/B/O eventsDFR:cellSelWave_burst/Wave=cellSelWave_burst
    cellSelWave_burst = 0
    cellSelWave_burst[0] = 1

    variable yPos = panelSize.yPosPnts
    variable xPos = panelSize.xPosPnts
    TitleBox cellNamesText_cell pos = {xPos, yPos}, title = "Select a cell:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    tabControls += "cellNamesText_cell;"
    
    // Cell Select Listbox
    yPos += buttonHeight
    ListBox cellListBox_burst, mode=1, listwave = cellName, selwave=cellSelWave_burst, pos={xPos, yPos}, size={listBoxWidth, listBoxHeight}, proc = selectedCellListProc_burst
    tabControls += "cellListBox_burst;"

    yPos += listBoxHeight*1.1

    variable/G panelDFR:vbwmin/N=vbwmin, panelDFR:vbwmax/N=vbwmax, panelDFR:vbwint/N=vbwint, panelDFR:vbw_num_ints/N=vbw_num_ints
    if(!vbwmin)
        vbwmin = 0.01
    endif
    if(!vbwmax)
        vbwmax = 1
    endif
    if(!vbwint)
        vbwint = 0.01
    endif
    if(!vbw_num_ints)
        vbw_num_ints = 100
    endif

    makeBurstWindowsWave()

    // Burst detection parameters
    SetVariable vbwmin_setVar pos={xPos, yPos}, size={buttonWidth,buttonHeight}, proc=AGG_vbwParamsProc, value=vbwmin, title="Burst window start (s):", limits={0, inf, 0.01}
    tabControls += "vbwmin_setVar;"
    yPos += buttonHeight
    
    SetVariable vbwmax_setVar pos={xPos, yPos}, size={buttonWidth,buttonHeight}, proc=AGG_vbwParamsProc, value=vbwmax, title="Burst window max (s):", limits={0, inf, 0.01}
    tabControls += "vbwmax_setVar;"
    yPos += buttonHeight
    
    SetVariable vbwint_setVar pos={xPos, yPos}, size={buttonWidth,buttonHeight}, proc=AGG_vbwParamsProc, value=vbwint, title="Increment (s):", limits={0, inf, 0.01}
    tabControls += "vbwint_setVar;"
    yPos += buttonHeight

    SetVariable vbw_num_ints_setVar pos={xPos, yPos}, size={buttonWidth,buttonHeight}, proc=AGG_vbwnumintsSetVarProc, value=vbw_num_ints, title="Num. intervals:", limits={0, inf, 1}
    tabControls += "vbw_num_ints_setVar;"
    yPos += buttonHeight

    Button detectCellBurstsButton pos = {xPos, yPos}, size={buttonWidth, buttonHeight}, proc=detectCellBurstsProc, title="Detect bursts for cell"
    tabControls += "detectCellBurstsButton;"
    yPos += buttonHeight

    // xPos += buttonWidth * 1.1
    // yPos = panelSize.yPosPnts

    // Cell region listbox
    TitleBox regionSelect_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select cell region(s):", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelect_text;"
    yPos += buttonHeight

    make/t/O/N=0 panelDFR:cellRegionNames/Wave=cellRegionNames
    Make/B/O/N=0 panelDFR:regionSelWave_CellBurst/Wave=regionSelWave
    regionSelWave = 0

    ListBox regionListBox_cell, mode=4, listwave = cellRegionNames, selwave=regionSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight * 0.5}, proc = regionListBoxCellBurstProc
    tabControls += "regionListBox_cell;"
    yPos += panelSize.listBoxHeight * 0.5

    Button analyzeCellBurstsButton pos={xPos, yPos}, size={buttonWidth, buttonHeight}, proc=analyzeCellBurstsProc, title = "Analyze sel'd regions for cell"
    tabControls += "analyzeCellBurstsButton;"
    yPos += buttonHeight

    xPos = posRelPanel(0.82, "width", panelName = panelName)
    yPos = panelSize.yPosPnts + buttonHeight

    edit /N=cellBurstOutTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.1, 0.8, 0.98)
    tabDisplays += "cellBurstOutTable;"
    ModifyTable showParts=126
    fillCellBurstOutTable()

    [variable upperXPos, variable upperYPos] = xyRelPanel(0.2, 0.06, panelName = panelName)
    Button saveCellBurstOutTable, pos = {upperXPos, upperYPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveCellBurstOutTable, title = "Save table", proc = saveTableProc, userdata="tableName:cellBurstOutTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveCellBurstOutTable;"
    

    // All Cells Detection/Analysis
    Button detectAllCellsBurstsButton pos = {xPos, yPos}, size={buttonWidth, buttonHeight}, proc=detectAllCellsBurstsProc, title="Detect bursts for all cells"
    Button detectAllCellsBurstsButton fColor=(65535,49151,62258)
    tabControls += "detectAllCellsBurstsButton;"
    yPos += buttonHeight

    TitleBox regionSelectAll_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select region(s):", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelectAll_text;"
    yPos += buttonHeight

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR=cellsOutDFR/T allRegionNames
    if(!WaveExists(allRegionNames))
        make/T/O/N=0 cellsOutDFR:allRegionNames/Wave=allRegionNames
    endif

    Wave regionSelWave = storeAllRegionsSelectWave(allRegionNames, whichSelWave = "burstAnalysis")

    ListBox regionListBox_allCellsAnalysis, mode=4, listwave = allRegionNames, selwave=regionSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight * 0.5}
    tabControls += "regionListBox_allCellsAnalysis;"
    yPos += panelSize.listBoxHeight * 0.5

    Button analyzeAllCellsBurstsButton pos={xPos, yPos}, size={buttonWidth, buttonHeight}, proc=analyzeAllCellsBurstsProc, title = "Analyze sel'd regions for all cells"
    Button analyzeAllCellsBurstsButton fColor=(65535,49151,62258)
    tabControls += "analyzeAllCellsBurstsButton;"
    yPos += buttonHeight*2

    // Wave translations

    Struct burstOutputWaves outputWaves
    fillBurstOutWavesStrings(outputWaves)

    string analysisTypes = outputWaves.analysis_types
    variable iType = 0, nTypes = itemsInList(analysisTypes)
    for(iType=0; iType<nTypes; iType++)
        string thisType = StringFromList(iType, analysisTypes)
        string niceName = StringByKey(thisType, outputWaves.niceNamesFromWave)

        string label = thisType + ": " + niceName
        TitleBox $("defName" + num2str(iType)) pos = {xPos, yPos}, frame = 0, fstyle = 0, title = label
        tabControls += "defName" + num2str(iType)+";"
        yPos += buttonHeight
    endfor

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end