///////////
/// createBurstRegionTableInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function createBurstRegionTableInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""

    tabControls += "selectBWEntry;"
    tabControls += "selectBWSlider;"
    tabControls += "bParamsListBox;"
    tabControls += "regionSelectPlot_text;"
    tabControls += "regionListBox_plot;"
    string tabNumStr = num2str(tabNum)

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    DFREF allCellsOutByBW_dfr = getVBWallCellsOutDF()

    Wave/SDFR=allCellsOutByBW_dfr/D regionsMultiCol = regionsMultiCol_cols
    if(!WaveExists(regionsMultiCol))
        make/N=(0,0)/D/O allCellsOutByBW_dfr:regionsMultiCol_cols/Wave=regionsMultiCol
    endif
    edit /W=(0.2, 0.1, 0.98, 0.98)/N=regionsTable/Hide=(1) /HOST=$panelName regionsMultiCol.ld
    tabDisplays += "regionsTable;"
    ModifyTable/W=$(panelName + "#regionsTable") showParts = 126

    [variable upperXPos, variable upperYPos] = xyRelPanel(0.2, 0.06, panelName = panelName)
    Button saveRegionsTable, pos = {upperXPos, upperYPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveRegionsTable, title = "Save table", proc = saveTableProc, userdata="tableName:RegionsTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveRegionsTable;"
    

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end