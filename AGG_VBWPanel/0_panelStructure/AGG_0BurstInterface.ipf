#pragma rtGlobals = 3 // Use modern global access method and strict wave access
#include <Resize Controls Panel>

Structure AGG_Burst_tabs
    NVAR infoTab, burstDetectTab, cellTab, maxBWTab, tableOutputTab, graphOutputTab, regionGraphTab, regionTableTab
    SVAR tabLabels, tabVarNames//    NVAR optoTab
EndStructure

function fillBurstTabNums (tabStruct)
    STRUCT AGG_Burst_tabs &tabStruct

    tabStruct.infoTab = 0
    tabStruct.burstDetectTab = 1
    tabStruct.cellTab = 2
    tabStruct.maxBWTab = 3
    tabStruct.tableOutputTab = 4
    tabStruct.graphOutputTab = 5
    tabStruct.regionGraphTab = 6
    tabStruct.regionTableTab = 7

    tabStruct.tabLabels = "infoTab:Region Info;burstDetectTab:Detect & Analyze;cellTab:Cell Plot;maxBWTab:Max BW;"
    tabStruct.tabLabels += "tableOutputTab:All Tbl Output;graphOutputTab:Graph Output;regionGraphTab:Region Plot;regionTableTab:Region table;"
    tabStruct.tabVarNames = "infoTab;burstDetectTab;cellTab;maxBWTab;tableOutputTab;graphOutputTab;regionGraphTab;regionTableTab;"
end

function AGG_build_burst_panel()
    AGG_build_MultCellEvents_panel() // make multicell events panel first
    DoWindow/F AGG_Bursts   // bring panel to the front if it exists
    if( V_Flag != 0 )       // panel exists
        return NaN
    endif

    createVBWDF()

    DFREF eventsPanelDF = getEventsPanelDF()

    createVBWPanelDF()
    DFREF panelDFR = getVBWPanelDF()

    // 2024-05-17 this is where I stopped. Need to get VBW panel DFR. Decide what to keep of that folder structure or not
    
    string eventsPanelName = "AGG_Events"
    string panelName = "AGG_Bursts"
    string udata = ""

    // define the screen parameter structure
    STRUCT ScreenSizeParameters ScrP

    // initialize it
    InitScreenParameters(ScrP)

    // Create the panel
    AGG_makePanel(5, 0, 95, 80, panelName)
    modifypanel cbRGB=(50000,50000,50000)

    variable width, height
    [width, height] = getWindowDims(type = "point")

    // print width
    variable tabFSize = width/100
    variable panelFSize = width/88
    if(panelFSize>18)
        panelFSize = 18
    endif
    print "tab font size", tabFSize, "panel font size", panelFSize
    
    //the current publishing standard is 1 pt = 1/72"). In other words points are fixed in size independent of resolution. This is good since, all things properly configured, 12 point text on an monitor will be the same height when printed on paper.

    DefaultGUIFont/W=$panelName panel = {"Arial", panelFSize, 0}, all = {"Arial", panelFSize, 0}, TabControl = {"Arial", tabFSize, 0}
    DefaultGUIFont/W=$panelName graph = {"Arial", 16, 0}, table = {"Arial", panelFSize, 0}
    // DefaultFont

    // Use a structure for positioning to pass easily to subfunctions
    STRUCT AGG_EventsPanelSizeInfo panelSize
    
    // Variable and dimensions
    variable xPos = 0.02
    variable yPos = 0.06
    variable xRightPos = 0.8

    variable xPosPnts = posRelPanel(xPos, "width")
    variable yPosPnts = posRelPanel(yPos, "height")
    variable xRightPosPnts = posRelPanel(xRightPos, "width")

    variable buttonWidth, buttonHeight, listBoxWidth, listBoxHeight, buttonWidthRel = 0.15, buttonHeightRel = 0.04, listBoxHeightRel = 0.35
    [buttonWidth, buttonHeight] = xyRelPanel(buttonWidthRel, buttonHeightRel)
    [listBoxWidth, listBoxHeight] = xyRelPanel(buttonWidthRel, listBoxHeightRel)

    panelSize.xPos = xPos
    panelSize.yPos = yPos
    panelSize.xRightPos = xRightPos
    panelSize.xPosPnts = xPosPnts
    panelSize.yPosPnts = yPosPnts
    panelSize.xRightPosPnts = xRightPosPnts
    panelSize.buttonWidthRel = buttonWidthRel
    panelSize.buttonHeightRel = buttonHeightRel
    panelSize.buttonWidth = buttonWidth
    panelSize.buttonHeight = buttonHeight
    panelSize.listBoxWidth = listBoxWidth
    panelSize.listBoxHeight = listBoxHeight
    panelSize.listBoxHeightRel = listBoxHeightRel
    panelSize.left = 0.2
    panelSize.top = 0.1
    panelSize.right = 0.8
    panelSize.bottom = 0.95

    Struct AGG_Burst_tabs tabStruct
    StructFill/SDFR=panelDFR/AC=1 tabStruct
    fillBurstTabNums(tabStruct) // Set values

    string tabs = tabStruct.tabVarNames
    string tablabels = tabStruct.tabLabels
    TabControl BurstTabs pos={0, 0}, size={posRelPanel(1, "width"),buttonHeight}, proc=changeDisplayForTab, userdata(panelDFR)=GetDataFolder(1, panelDFR), userdata(tabs)=tabs, userdata(tabLabels)=tabLabels
    makeTabControls("BurstTabs", tabs, tabLabels, panelDFR)

    createBurstInfoTab(panelName, tabStruct.infoTab, panelSize)
    createBurstDetectAnalysisTab(panelName, panelDFR, tabStruct.burstDetectTab, panelSize)
    createBurstCellPlotInterface(panelName, panelDFR, tabStruct.cellTab, panelSize)
    createMaxBWInterface(panelName, panelDFR, tabStruct.maxBWTab, panelSize)
    createBurstTableOutInterface(panelName, panelDFR, tabStruct.tableOutputTab, panelSize)
    createBurstGraphOutInterface(panelName, panelDFR, tabStruct.graphOutputTab, panelSize)
    createBurstRegionGraphInterface(panelName, panelDFR, tabStruct.regionGraphTab, panelSize)
    createBurstRegionTableInterface(panelName, panelDFR, tabStruct.regionTableTab, panelSize)

    // updateCellNameListBox()
    // updatePanelForSelectedSeries(panelName = panelName)

    enableControlsByTabData("", 0)
    enableDisplaysByTabData("", 0, panelName)

    SetActiveSubWindow $panelName
    setDataFolder root:
end