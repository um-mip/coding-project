///////////
/// createMaxBWInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function createMaxBWInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    
    string tabNumStr = num2str(tabNum)

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    createVBWMaxBWPlotDF()

    TitleBox regionSelectMaxBW_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select region:", frame=0, fstyle=1, anchor = LB
    tabControls += "regionSelectMaxBW_text;"
    yPos += buttonHeight

    DFREF cellsOutDFR = getAllCellsOutputDF()
    Wave/SDFR=cellsOutDFR/T allRegionNames
    if(!WaveExists(allRegionNames))
        make/T/O/N=0 cellsOutDFR:allRegionNames/Wave=allRegionNames
    endif

    Wave regionSelWave = storeAllRegionsSelectWave(allRegionNames, whichSelWave = "maxBW")

    ListBox regionListBox_maxBW, mode=1, listwave = allRegionNames, selwave=regionSelWave, pos={xPos, yPos}, size={panelSize.listBoxWidth, panelSize.listBoxHeight * 0.5}, proc=plotMaxBWForRegionProc
    tabControls += "regionListBox_maxBW;"
    yPos += panelSize.listBoxHeight * 0.5

    variable/G panelDFR:maxBWdispState/N=maxBWdispState = 1
    CheckBox maxDispTypeChk1, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Show avg burst freq by win", value = 1, mode = 1, proc = maxBWDisplayChkProc
    tabControls += "maxDispTypeChk1;"
    yPos += buttonHeight
    
    CheckBox maxDispTypeChk2, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Show avg + cells burst freq by win", value = 0, mode = 1, proc = maxBWDisplayChkProc
    tabControls += "maxDispTypeChk2;"
    yPos += buttonHeight
    
    
    CheckBox maxDispTypeChk3, pos = {xPos, yPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, title = "Show by cell max BW", value = 0, mode = 1, proc = maxBWDisplayChkProc
    tabControls += "maxDispTypeChk3;"
    yPos += buttonHeight

    string maxBWtarget = "maxBWDisplay"
    Display/W=(0.2, 0.1, 0.8, 0.98)/N=$maxBWtarget /L/B /HOST=$panelName /HIDE=(1)
    tabDisplays += maxBWtarget + ";"
    
    Button saveMaxBWDisplay, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="MaxBWDisplay", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveMaxBWDisplay;"   

    xPos = posRelPanel(0.82, "width", panelName = panelName)

    getUniqueTrtGroupsWave()
    WAVE/T uniqueGroups = panelDFR:uniqueGroups

    yPos = panelSize.yPosPnts
    
    TitleBox groupDispMaxBW_text pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, title = "Select group:", frame=0, fstyle=1, anchor = LB
    tabControls += "groupDispMaxBW_text;"
    yPos += buttonHeight
    
    ListBox groupsListBox listWave = uniqueGroups, pos={xPos, yPos}, size = {listBoxWidth, listBoxHeight}, proc = getMaxBWForGroupProc, mode = 2
    tabControls += "groupsListBox;"

    variable/G panelDFR:maxByCell/N=maxByCell, panelDFR:maxByBF/N=maxByBF
    string/G panelDFR:selGroupName/N=selGroupName, panelDFR:traceN/N=traceN

    yPos += listBoxHeight + buttonHeight
    TitleBox groupNameText, pos = {xPos, yPos}, title = "Group Name:", size = {buttonWidth, buttonHeight}, frame=0, fstyle=1, anchor = LB
    tabControls += "groupNameText;"

    yPos += buttonHeight
    SetVariable groupName, pos = {xPos, yPos}, title = " ", value = selGroupName, disable = 2, size = {buttonWidth, buttonHeight}
    tabControls += "groupName;"
    tabControlsNoEdit += "groupName;"

    yPos += buttonHeight
    TitleBox maxBW_byCellText, pos = {xPos, yPos}, title = "Avg of cells' max BW:", size = {buttonWidth, buttonHeight}, frame=0, fstyle=1, anchor = LB
    tabControls += "maxBW_byCellText;"

    yPos += buttonHeight
    SetVariable maxBW_byCell, pos = {xPos, yPos}, title = " ", value = maxByCell, disable = 2, size = {buttonWidth, buttonHeight}
    tabControls += "maxBW_byCell;"
    tabControlsNoEdit += "maxBW_byCell;"
    
    yPos += buttonHeight
    TitleBox maxBW_byBFText, pos = {xPos, yPos}, title = "BW at peak burst freq:", size = {buttonWidth, buttonHeight}, frame=0, fstyle=1, anchor = LB
    tabControls += "maxBW_byBFText;"

    yPos += buttonHeight
    SetVariable maxBW_byBF, pos = {xPos, yPos}, title = " ", value = maxByBF, disable = 2, size = {buttonWidth, buttonHeight}
    tabControls += "maxBW_byBF;"
    tabControlsNoEdit += "maxBW_byBF;"
    
    yPos += buttonHeight
    TitleBox hoverTNText, pos = {xPos, yPos}, title = "Hovered over cell trace:", size = {buttonWidth, buttonHeight}, frame=0, fstyle=1, anchor = LB
    tabControls += "hoverTNText;"

    yPos += buttonHeight
    SetVariable hoverTN, pos = {xPos, yPos}, title = " ", value = traceN, disable = 2, size = {buttonWidth, buttonHeight}
    tabControls += "hoverTN;"
    tabControlsNoEdit += "hoverTN;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
    addTabUserData_forNoEdit(tabControlsNoEdit, tabNum)
end
