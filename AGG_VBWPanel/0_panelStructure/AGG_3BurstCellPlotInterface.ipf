///////////
/// createBurstCellPlotInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: 
///////////
function createBurstCellPlotInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    
    string tabNumStr = num2str(tabNum)

    createVBWIndivCellOutDF()
    DFREF eventsDFR = getEventsPanelDF()
    DFREF cellSumDFR = getEventsCellSumOutDF()

    tabControls += "cellNamesText_cell;"
    tabControls += "cellListBox_burst;"
    tabControls += "regionSelect_text;"
    tabControls += "regionListBox_cell;"

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    NVAR/Z/SDFR=cellSumDFR binDur, showHisto, colorHisto, showChopped, showGaps
    variable/G cellSumDFR:showCityPlot/N=showCityPlot 

    xPos = posRelPanel(0.82, "width", panelName = panelName)
    TitleBox histoInfoText_cell pos = {xPos, yPos}, title = "Specify histogram information:", frame=0, fstyle=1, size = {buttonWidth, buttonHeight}, anchor = LB
    yPos += buttonHeight
    tabControls += "histoInfoText_cell;"

    CheckBox showGapsCheck pos = {xPos, yPos}, title = "show gaps", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showGaps
    yPos += buttonHeight
    tabControls += "showGapsCheck;"

    CheckBox showChoppedCheck pos = {xPos, yPos}, title = "show chopped events", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showChopped
    yPos += buttonHeight
    tabControls += "showChoppedCheck;"

    CheckBox showHistoCheck pos = {xPos, yPos}, title = "show histogram", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showHisto
    yPos += buttonHeight
    tabControls += "showHistoCheck;"
    
    CheckBox colorHistoCheck pos = {xPos, yPos}, title = "color by region", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = colorHisto
    yPos += buttonHeight
    tabControls += "colorHistoCheck;"
   
    CheckBox showCityPlotCheck pos = {xPos, yPos}, title = "show burst city plot", size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc_check, variable = showCityPlot
    yPos += buttonHeight
    tabControls += "showCityPlotCheck;"
    
    SetVariable binDurVar value = binDur, title = "histo bin width (s)", pos = {xPos, yPos}, size = {buttonWidth, buttonHeight}, proc = updateConcHistoProc
    yPos += buttonHeight
    tabControls += "binDurVar;"

    Wave/SDFR=cellSumDFR sliderTicks, sliderTickLabels
    
    Slider binDur_slider, pos = {xPos, yPos}, limits = {0, 180, 5}, vert=0, size = {buttonWidth, buttonHeight}, variable = binDur, ticks = 2, live = 0, userTicks = {sliderTicks, sliderTickLabels}
    tabControls += "binDur_slider;"
    yPos += buttonHeight*2.2
    
    DFREF indivCellOutDFR = getVBWIndivCellOutDF()
    make/D/O/N=0 indivCellOutDFR:eventHisto/Wave=eventHisto
    make/D/O/N=0 indivCellOutDFR:eventHistoByFreq/Wave=eventHistoByFreq
    Display /W=(0.2, 0.1, 0.8, 0.98) /L/B /HOST=$panelName /HIDE=(1)
    RenameWindow #, burstCellPlot
    tabDisplays += "burstCellPlot;"

    Button saveBurstCellPlot, pos = {panelSize.xPosPnts, posRelPanel(0.9, "height", panelName = panelName)}, size = {panelSize.buttonWidth, panelSize.buttonHeight}, userdata="burstCellPlot", proc = openSaveIntProc, title = "COPY or SAVE"
    tabControls += "saveBurstCellPlot;"   


    [variable width, variable height] = getWindowDims(windowName = panelName)
    variable xRel = xPos / width
    variable yRel = yPos / height

    edit /N=cellHistoTable /HIDE=(1) /K=1 /HOST=$panelName /W=(xRel, yRel, 0.98, 0.95) eventHisto.x, eventHisto, eventHistoByFreq.d
    ModifyTable showParts=126
    ModifyTable title(Point)="bin#"
    ModifyTable title(eventHisto.x)="binStart"
    ModifyTable title(eventHistoByFreq.d)="freq(Hz)"
    ModifyTable title(eventHisto.d)="#events"
    ModifyTable format(eventHistoByFreq.d)=3, digits(eventHistoByFreq.d)=2
    ModifyTable autosize={0, 0, 0, 0, 10}
    tabDisplays += "cellHistoTable;"

    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end