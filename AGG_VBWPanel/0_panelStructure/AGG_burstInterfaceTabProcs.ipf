///////////
/// updateTabDisplay_burstsPanel
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2023-12-31
/// NOTES: Use this to update only the relevant tab of the panel
/// to save processing time
///////////
function updateTabDisplay_burstsPanel([string panelName])
    if(paramIsDefault(panelName))
        panelName = "AGG_bursts"
    endif
    
    string tabLabel = getSelectedTab(tabCtrlName = "BurstTabs", panelName = panelName)
    string cellName
    strswitch (tabLabel)
        case "Region Info":
            updateCellsOutTable(doReconcat = 0)
            fillBurstInfoTable(panelName + "#burstInfoTable")
            break
        case "Detect & Analyze":
            cellName = getSelectedItem("cellListBox_burst", hostName = panelName)
            updateCellRegionsLB(cellName, whichLB = "cellListBox_burst")
            fillCellBurstOutTable()
            break
        case "Cell Plot":
            cellName = getSelectedItem("cellListBox_burst", hostName = panelName)
            updateCellRegionsLB(cellName, whichLB = "cellListBox_burst")
            displayBurstPlot(panelName = panelName)
            break
        case "Max BW":
            string selRegion = getSelectedItem("regionListBox_maxBW", hostName = panelName)
            getMaxBW(selRegion)
            plotAvgBurstFreqByGroup(selRegion, hostN = panelName)
            printGroupMaxBWVals(hostN = panelName)
            break
        case "All Tbl Output":
            updateGroupOutput(hostN = panelName)
            break
        case "Graph Output":
            updateGroupOutput(hostN = panelName)
            break
        case "Region Plot":
            plotMultiColRegions()
            break
        case "Region Table":
            plotMultiColRegions()
            break
        default:
            
            break
    endswitch
end