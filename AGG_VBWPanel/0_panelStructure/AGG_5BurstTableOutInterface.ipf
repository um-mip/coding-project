///////////
/// createBurstTableOutInterface
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function createBurstTableOutInterface(panelName, panelDFR, tabNum, panelSize)
    string panelName
    DFREF panelDFR
    variable tabNum
    STRUCT AGG_EventsPanelSizeInfo &panelSize

    string tabControls = ""
    string tabControlsNoEdit = ""
    string tabDisplays = ""
    
    tabControls += "regionSelectMaxBW_text;"
    tabControls += "regionListBox_maxBW;"
    string tabNumStr = num2str(tabNum)

    variable xPos = panelSize.xPosPnts, yPos = panelSize.yPosPnts
    variable buttonHeight = panelSize.buttonHeight, buttonWidth = panelSize.buttonWidth
    variable listBoxHeight = panelSize.listBoxHeight, listBoxWidth = panelSize.listBoxWidth

    variable/G panelDFR:selectedBW/N=selectedBW
    if(!selectedBW)
        selectedBW = 0.3
    endif

    variable/G panelDFR:vbwmin_used/N=vbwmin_used, panelDFR:vbwmax_used/N=vbwmax_used, panelDFR:vbwint_used/N=vbwint_used, panelDFR:vbw_num_ints_used/N=vbw_num_ints_used
    NVAR/Z/SDFR=panelDFR vbwmin, vbwmax, vbwint, vbw_num_ints
    if(!vbwmin_used)
        vbwmin_used = vbwmin
    endif
    if(!vbwmax_used)
        vbwmax_used = vbwmax
    endif
    if(!vbwint_used)
        vbwint_used = vbwint
    endif
    if(!vbw_num_ints_used)
        vbw_num_ints_used = vbw_num_ints
    endif

    yPos += listBoxHeight * 0.5 + buttonHeight * 2

    SetVariable selectBWEntry, pos = {xPos, yPos}, limits = {vbwmin_used, vbwmax_used, vbwint_used}, value = selectedBW, title = "Select BW:", size={buttonWidth,buttonHeight}, proc = updateGroupOutput_bw_Proc
    tabControls += "selectBWEntry;"
    yPos += buttonHeight

    Slider selectBWSlider, pos = {xPos, yPos}, limits = {vbwmin_used, vbwmax_used, vbwint_used}, vert=0, size = {buttonWidth, buttonHeight}, variable = selectedBW, ticks = 6, live = 0
    tabControls += "selectBWSlider;"

    createVBWallCellsOutDF()
    DFREF allCellsOutByBW_dfr = getVBWallCellsOutDF()

    make/O/T allCellsOutByBW_dfr:cellName/Wave=cellName
    make/O/D allCellsOutByBW_dfr:bn/Wave=bn, allCellsOutByBW_dfr:mbd/Wave=mbd, allCellsOutByBW_dfr:spb/Wave=spb, allCellsOutByBW_dfr:bf/Wave=bf, allCellsOutByBW_dfr:ssn/Wave=ssn, allCellsOutByBW_dfr:ssf/Wave=ssf, allCellsOutByBW_dfr:tf/Wave=tf, allCellsOutByBW_dfr:inter/Wave=inter, allCellsOutByBW_dfr:intra/Wave=intra, allCellsOutByBW_dfr:bFlags/Wave=bFlags, allCellsOutByBW_dfr:ssFlags/Wave=ssFlags
    
    edit /N=outputBParamsTable /HIDE=(1) /K=1 /HOST=$panelName /W=(0.2, 0.1, 0.98, 0.98) cellName, bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags
    tabDisplays += "outputBParamsTable;"
    ModifyTable showParts=126

    [variable upperXPos, variable upperYPos] = xyRelPanel(0.2, 0.06, panelName = panelName)
    Button saveoutputBParamsTable, pos = {upperXPos, upperYPos}, size = {panelSize.buttonWidth, panelSize.buttonHeight}
    Button saveoutputBParamsTable, title = "Save table", proc = saveTableProc, userdata="tableName:outputBParamsTable;" // add "fileName:fileNameHere;" if want something else as default
    tabControls += "saveoutputBParamsTable;"
    
    ModifyTable width(point) = 35
    ModifyTable width(bn) = 50, format(bn) = 1 // integer
    ModifyTable width(mbd) = 50, digits(mbd) = 3, format(mbd) = 3 //decimal
    ModifyTable width(spb) = 50, digits(spb) = 2, format(spb) = 3
    ModifyTable width(bf) = 50, digits(bf) = 3, format(bf) = 3
    ModifyTable width(ssn) = 60, format(ssn) = 1
    ModifyTable digits(ssf) = 3, format(ssf) = 3
    ModifyTable digits(tf) = 3, format(tf) = 3
    ModifyTable width(inter) = 60, digits(inter) = 3, format(inter) = 3
    ModifyTable width(intra) = 60, digits(intra) = 3, format(intra) = 3
    ModifyTable width(bFlags) = 60, format(bFlags) = 1
    ModifyTable width(ssFlags) = 60, format(ssFlags) = 1


    addTabUserData(tabControls, tabNum)
    addDisplayUserData(tabDisplays, tabNum, panelName)
end
