// Update the regions available for the cell and show its output
// when user selection changes
function selectedCellListProc_burst(LB_Struct) : ListboxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        string cellName = getSelectedItem("cellListBox_burst", hostName = LB_Struct.win)
        updateCellRegionsLB(cellName, whichLB = LB_Struct.ctrlName)
        fillCellBurstOutTable()
        displayBurstPlot(panelName = LB_Struct.win)
    endif
    return 0
end


///////////
/// detectCellBurstsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-23
/// NOTES: Run burst detection for the single cell selected in cellListBox_burst
///////////
function detectCellBurstsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        string cellName = getSelectedItem("cellListBox_burst", hostName = B_Struct.win)
        detectCellBursts(cellName)
        
        updateVBWParams(hostN = B_Struct.win)
    endif
    return 0
end

///////////
/// detectCellBursts
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: 
///////////
function detectCellBursts(string cellName)
    if(strlen(cellName)==0)
        return 0
    endif

    Wave burstWindows = makeBurstWindowsWave()
    if(!WaveExists(burstWindows) || numpnts(burstWindows) == 0)
        return 0
    endif

    DFREF burstDetectDFR = getVBWdetectionDF()
    if(dataFolderRefStatus(burstDetectDFR)==0)
        createVBWdetectionDF()
    endif
    DFREF burstDetectDFR = getVBWdetectionDF()

    Wave sctWave = getCellSCTWave(cellName)

    if(!WaveExists(sctWave))
        return 0
    endif

    AGG_vbwDetection(sctWave, cellName, burstWindows)
end


///////////
/// detectAllCellsBurstsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: Run burst detection for all cells
///////////
function detectAllCellsBurstsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        detectAllCellsBursts()
        updateVBWParams(hostN = B_Struct.win)
    endif
    return 0
end

///////////
/// detectAllCellsBursts
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: 
///////////
function detectAllCellsBursts()
    DFREF eventsDFR = getEventsPanelDF()
    if(dataFolderRefStatus(eventsDFR)==0)
        return NaN
    endif

    Wave/SDFR=eventsDFR/T cellNames = cellName
    if(!WaveExists(cellNames))
        return NaN
    endif

    for(string cellName : cellNames)
        detectCellBursts(cellName)
    endfor
end