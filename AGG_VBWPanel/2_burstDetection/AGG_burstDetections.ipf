////////////////////////////////////////
// AGG_vbwDetection
//
// This will run the burst detection for a single wave with event times at different burst windows
// It will create output waves for the intervals, burst/single spike times, etc for this wave
// The output waves can then be later processed for region detection to give the output values at each burst window for each region
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Pass in a wave reference to the SCT wave instead of the string of the wave name
/// cellName is used to determine output and folder locations
/// burstWindows is the already created wave with the burst windows to be analyzed
function AGG_vbwDetection(wave sctDataset, string cellName, wave burstWindows)
	string bursts=""
	variable bw=0

	variable ibw=0, nbw=0

    if(!WaveExists(sctDataset))
        print "AGG_vbwDetection: the sctDataset wave doesn't exist yet", cellName
        return NaN
    endif

    DFREF detectionDFR = getVBWdetectionDF()
    if( DataFolderRefStatus(detectionDFR) == 0) // if overall detection folder doesn't exist
        print "AGG_vbwDetection: the detection datafolder doesn't exist yet"
        return NaN
    endif

    if(!WaveExists(burstWindows))
        print "AGG_vbwDetection: the burst window wave doesn't exist yet"
        return NaN
    endif

    variable nanalysis = numpnts(burstWindows)

    // Will create a subfolder for this cell, overwriting whatever already exists
    killVBWCellDetectionDF(cellName)
    createVBWCellDetectionDF(cellName)

    DFREF cellDetectDFR = getVBWCellDetectionDF(cellName)
    if(dataFolderRefStatus(cellDetectDFR)==0)
        print "AGG_VBWDetection: Can't find cell detect dfr folder"
        return NaN
    endif

    string/G cellDetectDFR:cellName = cellName // store cell name in detection folder
    duplicate/O burstWindows, cellDetectDFR:burstWindows // store a copy of burst windows wave used to do the detection for the cell

    // General processing of the SCT wave with gap/duration information
    string sctWN
    DFREF sctDFR
    [sctWN, sctDFR] = getWaveNameAndDFR(sctDataset)

    // Duration information
    variable/G cellDetectDFR:duration /N=duration // global variable plus name of local NVAR
    variable/G cellDetectDFR:recduration /N=recduration
    variable/G cellDetectDFR:gapduration /N=gapduration

    duration = NaN; recduration = NaN; gapDuration = NaN

    variable useStringsForGaps = 1

    // Check for duration information first within the cell summary folder
    // If not there, then look in wavenote
    DFREF cellSumDFR = getCellSummaryDF(cellName)
    if(dataFolderRefStatus(cellSumDFR)!=0)
        NVAR/Z/SDFR=cellSumDFR cellTotalDur = totalDur, cellRecDur = totalRecDur, cellGapDur = totalGapDur
        if(NVar_Exists(cellTotalDur))
            duration = cellTotalDur
        else
            duration = AGG_wnoteduration( sctWN, sctDFR )
        endif

        if(NVAR_Exists(cellRecDur))
            recduration = cellRecDur
        else
            recduration = AGG_wnoteVarByKey( sctWN, "RECDURATION", sctDFR )   
        endif

        if(cellGapDur)
            gapDuration = cellGapDur
        else
            gapduration = AGG_wnoteVarByKey( sctWN, "GAPDURATION", sctDFR )  
        endif

        Wave/SDFR=cellSumDFR gapSTARTSwPlusEnd = $(cellName + "_gapStartsPlusEnd"), gapENDSwPlusStart = $(cellName + "_gapEndsPlusStart")
        if(WaveExists(gapSTARTSwPlusEnd) && WaveExists(gapENDSwPlusStart))
            useStringsForGaps = 0
        endif
    else
        duration = AGG_wnoteduration( sctWN, sctDFR )
        recduration = AGG_wnoteVarByKey( sctWN, "RECDURATION", sctDFR )
        gapduration = AGG_wnoteVarByKey( sctWN, "GAPDURATION", sctDFR )
    endif

    if(useStringsForGaps)
        // processing Gaps - info comes from wavenotes in JP_smart_conc_vAGG.ipf
        string gapSTARTSstring, gapENDSstring, gapSTARTSplusEndString, gapENDSplusStartString
        gapSTARTSstring = AGG_wnoteSTRbyKey( sctWN, "GAPSTARTS", sctDFR )
        gapENDSstring = AGG_wnoteSTRbyKey( sctWN, "GAPENDS", sctDFR )
        gapSTARTSplusEndString = AGG_wnoteSTRbyKey( sctWN, "gapStartsPlusEnd", sctDFR )
        gapENDSplusStartString = AGG_wnoteSTRbyKey( sctWN, "gapEndsPlusStart", sctDFR )

        // Add these waves to the cell summary folder. Can be referenced wtihin detection
        AGG_waveFromStringList( gapSTARTSstring, "gSw", sctDFR)
        AGG_waveFromStringList( gapENDSstring, "gEw", sctDFR )
                    
        WAVE gapSTARTSwPlusEnd = AGG_waveFromStringList( gapSTARTSplusEndString, "gSwPlusE", sctDFR )
        WAVE gapENDSwPlusStart = AGG_waveFromStringList( gapENDSplusStartString, "gEwPlusS", sctDFR )
    endif


    // dataset is the local copy of the sct wave with spike times
    // this function calculates both the forwards and backwards interval for a time based wave
    // moving forward for burst detection, we'll be using the backwards interval
    // Store these waves in the cell detect folder
    // nintervals is the number of intervals (which equals the number of points)
    // These are passed explicitly for the burst detection at a given burst window
    [Wave/D backInt, wave/D forInt] = calculateIntervals(sctDataset, estUnknowns = 1, waveDur = duration, storageDF = cellDetectDFR)

    if(!WaveExists(backInt))
        print "AGG_VBWDetection: Can't find the backwards interval wave"
        return NaN
    endif

    variable nIntervals = numpnts(backInt)

    // for each spike, get the time to the previous gap and the time to the next gap
    // record if there is a gap that crosses the interval with an interval gap flag
    // These waves are stored in the cell detect folder, and are passed explicitly for the burst detection at a given burst window
    Wave backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag
    [backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag] = AGG_getIntervalsInRelationToGaps(nIntervals, sctDataset, gapSTARTSwPlusEnd, gapENDSwPlusStart, cellName)

    ibw = 0

    // track the smallest burst window at which an event appears as part of a burst
    make/N=(numpnts(sctDataset))/D/O cellDetectDFR:eventMinBurstWin/Wave=eventMinBurstWin
    eventMinBurstWin = NaN

    print "Running burst detection for cell", cellName, "which has", nIntervals, "spikes"
    for( ibw = 0; ibw < nanalysis; ibw += 1 )

        bw = burstWindows[ ibw ]

        createVBWCellDetectionBWDF(cellName, bw)

        // Complete burst detection for each bw - the necessary output waves are saved in the data folder
        // bursts is a keyed string with wave names for graphs if going to implement
        // AGG 2024-05-27, output will now be a blank string, not using the keyed string for plotting anymore
        // See appendCityPlot function for new city plot function
        bursts = AGG_burstDetection( bw, cellName, sctDataset, backInt, nIntervals, backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag, eventMinBurstWin )
    endfor
    print "\\//\\//\\"
end


// AGG - ASSESSING RELATIONSHIP OF SPIKES TO BEGINNING/END OF RECORDING AND TO GAPS =====================================================
    // creates backIntervalsToGaps wave
        // the interval (sec) between a spike and the previous end of a gap, 
        // or to the beginning of the recording if the spike is before the first gap
    // creates forwardIntervalsToGaps wave
        // the interval (sec) between a spike and the start of the next gap, 
        // or to the end of the recording if there are no more gaps after the spike
    // creates intervalGapFlag wave
        // is the amount of gap time that occurs during the backwards interval for a spike.
        // if it is zero for an interval, that means that there was no gap between the spike and the previous spike

function [Wave backIntervalsToGaps, Wave forwardIntervalsToGaps, Wave intervalGapFlag] AGG_getIntervalsInRelationToGaps(variable nIntervals, wave dataset, wave gapSTARTSwPlusEnd, wave gapENDSwPlusStart, string cellName)
    // print "in getIntervalsInRelationToGaps"

    DFREF currentFolder = GetDataFolderDFR()

    DFREF thisCellDFR = getVBWCellDetectionDF(cellName)
    setDataFolder thisCellDFR

    variable ipoint = 0 // handle first event

    make/O/N=( nintervals ) backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag
    
    backIntervalsToGaps = 0
    forwardIntervalsToGaps = 0
    intervalGapFlag = 0
    variable igap = 0, prevTime = 0

    if(!waveExists(gapSTARTSwPlusEnd) || !waveExists(gapENDSwPlusStart))
        print "in getIntervalsInRelationToGaps - either gapStartW or gapEndW does not exist"
        setDataFolder currentFolder
        return [backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag]
    endif

    variable numGaps = numpnts(gapStartswPlusEnd)
    // print "there are", numGaps, "gaps"

    if(nIntervals <= 0)
        setDataFolder currentFolder
        return [backIntervalsToGaps, forwardIntervalsToGaps, intervalGapFlag]
    endif
    
    // AGG - If there is at least one interval, get the information about the last event and gaps
    variable endTime = gapSTARTSwPlusEnd[numGaps - 1]
    variable lastEventTime = dataset[ nIntervals - 1 ]

    // AGG 2021-05-21; situation where the timing of one of the datapoints is actually after the "end" listed in the gap, likely due to rounding
    // This led to getting stuck in the while loop because it kept infinitely adding one to the iGap counter
    if(lastEventTime >= endTime)
        print "WARNING - Cell", cellName, ": the last event time is after the rounded 'end' of recording - adding some extra buffer time"
        gapSTARTSwPlusEnd[numGaps - 1] = lastEventTime + 0.001 // change end time to be just after last event time
    endif

    // AGG - Handling of start/end and gaps
    for(ipoint = 0; ipoint < nintervals; ipoint +=1)
        if( dataset[ ipoint ] < gapSTARTSwPlusEnd[ igap ] ) //if the current spike time is less than the gapSTARTS time
            // AGG - Time from this spike until the next gap start
            forwardIntervalsToGaps[ ipoint ] = gapSTARTSwPlusEnd[ igap ] - dataset[ ipoint ]
            
            // AGG -Time from the last gap end (or start of recording) until this spike
            // AGG - added 0 to the start of the wave list for gapENDSwPlusStart, so this still works with the same igap index counter
            backIntervalsToGaps[ ipoint ] = dataset[ ipoint ] - gapENDSwPlusStart[ igap ]
        else 
            //the current spike time is greater than gapSTARTS time
            
            do
                if( ipoint == 0 ) // first point, which doesn't have preceeding time point
                    // AGG - Add the gap duration to the gap flag for this point
                    // AGG - Interval is tainted with gap(s)
                    intervalGapFlag[ ipoint ] += gapENDSwPlusStart[ igap + 1 ] - gapSTARTSwPlusEnd[ igap ]
                else
                    prevTime = dataset[ ipoint - 1 ] //time of the previous event
                    
                    if( prevTime < gapSTARTSwPlusEnd[ igap ] ) // if the previous event is before this gap start
                        // AGG - Add the gap duration to the gap flag for this point
                        // AGG - This indicates the an interval is tainted with gaps
                        intervalGapFlag[ ipoint ] += gapENDSwPlusStart[ igap + 1 ] - gapSTARTSwPlusEnd[ igap ]
                    endif
                endif
                igap += 1 // add 1 to igap counter
                
            // AGG - as long as the time of the current event is greater than or equal to the time of the gap
            // AGG - keep running this while loop

            // print "time of spike", dataset[ipoint], "for ipoint", ipoint
            // print "gapStartswPlusEnd", gapStartswPlusEnd[iGap], "for iGap", iGap
            
            while( dataset[ ipoint ] >= gapSTARTSwPlusEnd[ igap ] ) 
            
            // AGG - the time of the spike should now be before an igap time

            // AGG - Get the time from this spike until the next gap start
            forwardIntervalsToGaps[ ipoint ] = gapSTARTSwPlusEnd[ igap ] - dataset[ ipoint ]

            // AGG - Get the time from the last gap end (or start of recording) until this spike
            // AGG - added 0 to the start of the wave list for gapENDSwPlusStart, so this still works with the same igap index counter
            backIntervalsToGaps[ ipoint ] = dataset[ ipoint ] - gapENDSwPlusStart[ igap ]
        endif
    endfor

    setDataFolder currentFolder
end


////////////////////////////////////////////
// burst detection for a single wave and burst window
// Does not do the final analysis after detecting single spikes/bursts
////////////////////////////////////////////
// AGG based on backwardsBanalysis from Feb 2021
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-26
/// NOTES: Added a wave to track the minimum bw that is a burst for an event
/// eventMinBurstWin gets updated by this function
////////////////////////////////////////////
function/S AGG_burstDetection( 
    variable burstwindow, 
    string cellName,
    wave dataset,
    wave intervals,
    variable nIntervals,
    wave backIntervalsToGaps,
    wave forwardIntervalsToGaps,
    wave intervalGapFlag,
    Wave eventMinBurstWin
    )

	//print "AGG_burstDetection!"

    DFREF currentFolder = GetDataFolderDFR()
    
    DFREF thisCellDFR = getVBWCellDetectionDF(cellName)

    DFREF thisDetectDFR = getVBWCellDetectionBWDF(cellName, burstWindow)
    SetDataFolder thisDetectDFR
    // print GetDataFolder(1)

	variable npoints = nIntervals // these should be equivalent

	// These are global variables and can be referenced to help with analysis afterwards
	variable/G numIntraIntervals = 0, numInterIntervals = 0, nSS = 0, nBursts = 0

	// Only Local Variables
	variable ipoint = 0, iInter = 0, iintra = 0, iburstSpikes = 0

	variable iburststart=0, iburstend=0, iburst = 0 
	variable count=0

    if( !waveexists( dataset ) || !waveExists(intervals) || !waveExists(backIntervalsToGaps) || !waveExists(forwardIntervalsToGaps) || !waveExists(intervalGapFlag))
        print "AGG_burstDetection for cell", cellName, ", bw", burstWindow
        print "At least one of the waves doesn't exist - if there's a 0 by the wave name, it's missing"
        print "Dataset exists: ", waveexists( dataset )
        print "intervals exists: ", waveExists(intervals)
        print "backIntervalsToGaps exists: ", waveExists(backIntervalsToGaps)
        print "forwardIntervalsToGaps exists: ", waveExists(forwardIntervalsToGaps)
        print "intervalGapFlag exists: ", waveExists(intervalGapFlag)
        return ""
    endif

    // make wave to store info
    duplicate/O/D dataset, spikeInfo
    spikeinfo = 0 // = 0 if single spike; = 1 if in burst 

    // AGG - 20210205: 
    // interEvent - any interval greater than burst window. Could be between bursts or single spikes
    make/O/D/N=( nintervals ) inter_Intervals, interTimes
    // intraInt_burstStart will be used to assign intra_Intervals based on the timing of the burst that the intervals belong to
    make/O/D/N=( nintervals ) intra_Intervals, intraInt_burstStart

    make/O/D/N=( npoints ) sspoints, ssTimes
    make/O/D/N=( npoints ) burstpoints, burstSpikeTimes, burstDurations, burstSpikes, burstStartTimes, burstStartPoints
    
    // AGG - 20210205 - these will be used to flag if a single spike or a burst is close to the start/end of a recording or gaps
    // If the "flags" value is 0, then there are no concerns about the event. If the "flags" value is 1, then there are concerns
    make/O/D/N=( npoints ) ssFlags, burstFlags 

    // initialize all waves
    inter_intervals = 0
    interTimes = 0
    intra_Intervals = 0
    intraInt_burstStart = 0
    sspoints = 0
    sstimes = 0
    burstpoints = 0
    burstSpikeTimes = 0
    burstdurations = 0
    burstStartTimes = 0
    burstStartPoints = 0
    burstspikes = 1  // all bursts start with one spike
    ssFlags = 0
    burstFlags = 0

    variable burstflag = 0, singleflag = 0

    if(nIntervals > 0)
        variable lastpoint = nIntervals - 1
    endif
    
    // AGG - //\\//\\//\\//\\//\\//\\//\\ ONLY ONE INTERVAL //\\//\\//\\//\\//\\//\\//\\//\\
    ipoint = 0
    if( nintervals == 1)
        // print "AGG - one interval"
        nss = handleOnlyOneIntervalForBurstDetect(\
            dataset,\
            backIntervalsToGaps,\
            forwardIntervalsToGaps,\
            burstWindow,\
            ssPoints,\
            ssTimes,\
            ssFlags)
    endif

    // AGG - //\\//\\//\\//\\//\\//\\//\\ MORE THAN ONE INTERVAL //\\//\\//\\//\\//\\//\\//\\//\\
    for( ipoint = 1; ipoint < nintervals; ipoint +=1 ) // AGG - loop over all the intervals, starting with the second interval
        // interval is the time from the previous spike: backwards difference!!
        if( intervals[ ipoint ] < burstwindow ) // BURST! ----------------------------------------------------------
            // update eventMinBurstWin
            updateMinBurstWindow(\
                iPoint,\
                burstWindow,\
                eventMinBurstWin)

            // AGG - Only do the following for the first spike in the burst
            if( burstflag == 0)
                // update burstFlags, burstStartPoints, and burstStartTimes for this new burst
                // \ indicates continues on next line
                initializeBurstInformation(\
                    iPoint,\
                    burstWindow,\
                    iBurst,\
                    backIntervalsToGaps,\
                    dataset,\
                    burstFlags,\
                    burstStartPoints,\
                    burstStartTimes)
                
                burstflag = 1 // AGG - Update burstflag
            endif	
                        
            // Update information for spikeinfo, burstPoints, burstSpikeTimes, burstSpikes, and burstDurations
            iBurstSpikes = updateBurstSpikeInformation(\
                ipoint,\
                iBurstSpikes,\
                iBurst,\
                dataset,\
                intervals,\
                spikeInfo,\
                burstPoints,\
                burstSpikeTimes,\
                burstSpikes,\
                burstDurations)
            
            // Update information for intra_intervals, intraInt_burstStart, and burstFlags
            iIntra = updateIntraIntervals(\
                iPoint,\
                iIntra,\
                iBurst,\
                intervals,\
                burstStartTimes,\
                intervalGapFlag,\
                intra_intervals,\
                intraInt_burstStart,\
                burstFlags)
            
            if( ipoint == lastpoint ) // AGG - check if last spike
                updateMinBurstWindow(\
                    iPoint,\
                    burstWindow,\
                    eventMinBurstWin)
                // Update information for spikeInfo, burstPoints, burstSpikeTimes, burstFlags
                string lastSpikeBurstOutput = handleLastSpikeInRecording_inBurst(\
                    iPoint,\
                    iBurstSpikes,\
                    iBurst,\
                    burstWindow,\
                    dataset,\
                    forwardIntervalsToGaps,\
                    spikeInfo,\
                    burstPoints,\
                    burstSpikeTimes,\
                    burstFlags)
                
                iBurst = str2num( stringbykey( "iBurst", lastSpikeBurstOutput ))
                iBurstSpikes = str2num( stringbykey( "iBurstSpikes", lastSpikeBurstOutput ))
            endif
        else
            // potential single spike
            if( burstflag != 1 ) // no burstflag -> prior event not part of burst
                // Update information for ssFlags, ssPoints, and ssTimes
                nss = updateSingleSpikeInformation(\
                    iPoint,\
                    nss,\
                    burstWindow,\
                    backIntervalsToGaps,\
                    forwardIntervalsToGaps,\
                    dataset,\
                    ssFlags,\
                    ssPoints,\
                    ssTimes)
            else
                // AGG 2024-05-27
                // Resist the temptation: don't update the min burst window wave for the last spike in a burst
                // The way that the plot is drawn, this needs to be NaN to keep each burst separate
                // 
                // If the eventMinBurst function is used for other purposes,
                // there could be instances in which you'd want to know that that event is part of a burst at that window.
                // In that case, could consider some sort of "flag" additional wave to mark the ones that are the end of a burst
                // but this would then have to be "cleared" if they're not the end at longer burst windows
                // 
                // I'd recommend using the other waves like spikeInfo (marks if part of burst) for determining if an event is burst of a burst
                // at a given burst window, and let the eventMinBurstWin be used for the plotting purposes
                // updateMinBurstWindow(\
                //     iPoint,\
                //     burstWindow,\
                //     eventMinBurstWin)

                // Updates spikeInfo, burstPoints, burstSpikeTimes, burstFlags
                string priorSpikeInBurstOutput = endThePreviousBurst(\
                    iPoint,\
                    iBurstSpikes,\
                    iBurst,\
                    burstWindow,\
                    forwardIntervalsToGaps,\
                    dataset,\
                    spikeInfo,\
                    burstPoints,\
                    burstSpikeTimes,\
                    burstFlags)
                
                iBurst = str2num( stringbykey( "iBurst", priorSpikeInBurstOutput ))
                iBurstSpikes = str2num( stringbykey( "iBurstSpikes", priorSpikeInBurstOutput ))

                burstflag = 0 // AGG - reset burst flag
            endif
            
            // AGG - FOR ALL INTERVALS GREATER THAN OR EQUAL TO BURST WINDOW 
            
            // Update inter_intervals, interTimes
            iInter = updateInterIntervals(\
                iPoint,\
                iInter,\
                intervals,\
                dataset,\
                intervalGapFlag,\
                inter_intervals,\
                interTimes)	
            
            // AGG - Check if this is the last point
            if( ipoint == lastpoint )
                nss = handleLastSpikeInRecording_singleSpike(\
                    iPoint,\
                    nss,\
                    burstWindow,\
                    backIntervalsToGaps,\
                    forwardIntervalsToGaps,\
                    dataset,\
                    ssFlags,\
                    ssPoints,\
                    ssTimes)
            endif
            
        endif
    endfor
    
    numinterintervals = iinter
    numintraintervals = iintra
    
    nbursts = iburst

    redimension/N=( numinterintervals ) inter_intervals, interTimes
    redimension/N=( numintraintervals ) intra_intervals, intraInt_burstStart
    redimension/N=( iburstSpikes ) burstpoints, burstSpikeTimes
    redimension/N=( nbursts ) burstdurations, burstspikes, burstStartTimes, burstStartPoints, burstFlags
    redimension/N=( nss ) ssTimes, ssPoints, ssFlags

    string output = ""

    // AGG 2024-05-27, output previously contained referenced to x and y burst plots, not using anymore

    SetDataFolder currentFolder

	return output
end



 // AGG - Notes on handling single spike during recording ----------------------------
        // in all cases, we'll allow info about this spike to be added to sspoints and ssTimes
        // if there is a later decision to *NOT* include the questionable single spikes (too close to start/end/gaps) in the sspoints/ssTimes list, 
        // then these assignments should be moved to within the relevant portions of the if statements below.
        // The other option would be to filter out sspoints and ssTimes that correspond to a spike where 
        // the ssFlags = 1 at the end of the analysis
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added ssPoints, ssTimes, and ssFlags as parameters to the function
/// instead of assuming that will be in current datafolder and updating in background
/// returns nss - number of single spikes
function handleOnlyOneIntervalForBurstDetect(
    wave dataset // contains the time information for the spikes
    , wave backIntervalsToGaps
    , wave forwardIntervalsToGaps
    , variable burstWindow
    , wave ssPoints
    , wave ssTimes
    , wave ssFlags
)
    // print "in handleOnlyOneIntervalForBurstDetect"

    variable nss = 0, ipoint = 0

    sspoints[ nss ] = ipoint //add index to sspoints
    ssTimes[ nss ] = dataset[ ipoint ] //add time to sstimes
    
    // AGG - check relationship to start/end/gaps in recording	 
    if( backIntervalsToGaps[ ipoint ] < burstwindow ) 
        // AGG - if less than burst window from start of recording or gap
        // AGG - tag as single spike with uncertainty
        ssFlags[ nss ] = 1
    else
        // AGG - check if it is less than burst window from the end of recording or gap
        if( forwardIntervalsToGaps[ ipoint ] < burstwindow ) 
            // the last spike could be a single spike, but it could also be the start of a burst
            ssFlags[ nss ] = 1
        else
            // the spike is definitely single spike
        endif
    endif
    
    nss += 1 // AGG - add one to the single spike count

    return nss
end

///////////
/// updateMinBurstWindow
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-26
/// NOTES: Updates the min win for the previous index, unless specified that updating for the last point
///////////
function updateMinBurstWindow(variable iPoint, variable burstWindow, wave eventMinBurstWin[, variable isLastPoint])
    if(paramIsDefault(isLastPoint))
        isLastPoint = 0
    endif

    variable indx

    if(isLastPoint)
        indx = iPoint
    else
        indx = iPoint - 1
    endif

    variable currentMin = eventMinBurstWin[indx]
    if(numtype(currentMin)!=0 || currentMin > burstWindow )
        eventMinBurstWin[indx] = burstWindow
    endif
end


// Determine if the burst is less than a burst window from the previous gap
    // if so, update *burstFlags*
// Add the starting point and the starting time for the burst
    // Update *burstStartPoints* and *burstStartTimes*
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added burstFlags, burstStartPoints, and burstStartTimes
/// as parameters to the function, instead of assuming in current datafolder
/// and updating in background
function initializeBurstInformation(
    variable iPoint
    , variable burstWindow
    , variable iburst
    , wave backIntervalsToGaps
    , wave dataset
    , wave burstFlags
    , wave burstStartPoints
    , wave burstStartTimes
)
    // print "in initializeBurstInformation"

    // AGG - if the backwards interval to gap for the previous spike is less than burst window
    if( backIntervalsToGaps[ ipoint - 1 ] < burstwindow ) 
        // AGG - need to flag this as a burst with uncertainty
        burstFlags[ iburst ] = 1
    else
        // AGG - the prior spike was far enough from start of recording/gap 
        // AGG - we didn't miss events from the start of this burst
    endif
    
    // AGG - store the index of the first spike in the burst
    burstStartPoints[ iburst ] = ipoint - 1
    // AGG - store the time of the first spike in the burst
    burstStartTimes[ iburst ] = dataset[ ipoint - 1 ]
end

// Return the updated ibp index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Made burstplotx and burstploty parameters for the function,
/// instead of hidden background updates
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-27
/// NOTES: Removed this function from the burst detection workflow
/// Instead, use the eventMinBurstWin wave to create the updated version of a city plot
/// This takes less data. Don't need two waves with twice the number of points as events for every burst window
function updateCityPlot(ibp, iPoint, burstWindow, dataset, burstplotx, burstploty)
    variable ibp, iPoint, burstWindow
    wave dataset, burstplotx, burstploty

    if(!WaveExists(dataset) || !WaveExists(burstplotx) || !WaveExists(burstploty))
        print "updateCityPlot: Warning: couldn't find either dataset, burstplotx, and/or burstploty"
        print "returning current ibp without adding any points"
        return ibp
    endif

    // print "in updateCityPlot"
    // city plot stuff
    burstplotx[ ibp ] = dataset[ ipoint - 1 ]
    burstploty[ ibp ] = burstwindow
    burstplotx[ ibp+1 ] = dataset[ ipoint ]
    burstploty[ ibp+1 ] = burstwindow
    ibp += 2

    return ibp
end

// Update information for spikeinfo, burstPoints, burstSpikeTimes, burstSpikes, and burstDurations
// Returns the updated iBurstSpikes index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added spikeInfo, burstPoints, burstSpikeTimes, burstSpikes, and burstDurations
/// as parameters in the function instead of updating in background and assuming will be in current folder
function updateBurstSpikeInformation(
    Variable ipoint 
    , variable iBurstSpikes
    , variable iBurst
    , wave dataset
    , wave intervals
    , Wave spikeInfo
    , Wave burstPoints
    , Wave burstSpikeTimes
    , Wave burstSpikes
    , Wave burstDurations
    )

    // print "in updateBurstSpikeInformation"

    spikeinfo[ ipoint - 1 ] = 1 // AGG - mark previous spike as a burst spike
                
    burstpoints[ iburstSpikes ] = ipoint - 1 // AGG - store previous spike index in burstpoints
    burstSpikeTimes[ iburstSpikes ] = dataset[ ipoint - 1 ] // AGG - store previous spike time in burstSpikeTimes

    burstspikes[ iburst ] += 1 // Add 1 to the number of spikes in the burst
    burstdurations[ iburst ] += intervals[ ipoint ] // AGG - Add this interval to the burst duration for this burst
    
    iBurstSpikes += 1

    return iBurstSpikes
end

// Update information for intra_intervals, intraInt_burstStart, and burstFlags
// Returns the updated iIntra index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added intra_intervals, intraInt_burstStart, and burstFlags
/// as parameters within the function instead of background updating and assuming will be in current folder
function updateIntraIntervals(
    variable iPoint
    , variable iIntra
    , variable iBurst
    , wave intervals
    , wave burstStartTimes
    , wave intervalGapFlag
    , wave intra_intervals
    , wave intraInt_burstStart
    , wave burstFlags
    )

    // print "in updateIntraIntervals"

    // AGG - Check if there was a gap during this interval
    if( intervalGapFlag[ ipoint ] == 0 )
        // AGG - no gap
        intra_intervals[ iIntra ] = intervals[ ipoint ] // AGG - add this interval to intra_intervals
        // AGG 2021-04-29 - tag the intra interval time based on start time of burst
        intraInt_burstStart[ iIntra ] = burstStartTimes[ iburst ] 
        
        iIntra += 1 // AGG - add one to the counter for iintra
    else
        // AGG  - Need to mark this as a burst with uncertainty
        burstFlags[ iburst ] = 1
    endif

    return iIntra	
end

/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: added spikeInfo, burstPoints, burstSpikeTimes, burstFlags as parameters
/// to the functions, instead of just updating in background and assuming waves
/// are in the current datafolder
function/S handleLastSpikeInRecording_inBurst(
    variable iPoint
    , variable iBurstSpikes
    , variable iBurst
    , variable burstWindow
    , wave dataset
    , wave forwardIntervalsToGaps
    , wave spikeInfo
    , wave burstPoints
    , wave burstSpikeTimes
    , wave burstFlags
)
    // print "in handleLastSpikeInRecording_inBurst"

    // AGG - Won't be able to update this with the next interval, so we have to end burst here
    spikeinfo[ ipoint ] = 1 // AGG - Mark this point as part of a burst
    
    burstpoints[ iburstSpikes ] = ipoint // AGG - Store this last spike in burstpoints
    burstSpikeTimes[ iburstSpikes ] = dataset[ ipoint ] // AGG - Store this last spike time in burstSpikeTimes
    
    // AGG - Check if the time from this event to the next gap/end of recording is less than burstWindow
    if( forwardIntervalsToGaps[ ipoint ] < 	burstWindow)
        // AGG - need to flag this as a burst with uncertainty
        burstFlags[ iburst ] = 1
    endif
    
    iburst += 1 // AGG - add one to the iburst count so that its value equals total number of bursts
    iburstSpikes += 1 // AGG - add one to the number of spikes that have occurs in bursts

    string output = ""
    output += "iBurst:"
    output += num2istr(iBurst) + ";"
    output += "iBurstSpikes:"
    output += num2istr(iBurstSpikes) + ";"

    return output
end

// Update information for ssFlags, ssPoints, and ssTimes
// Returns the updated nSS index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Add ssFlags, ssPoints, and ssTimes to the parameters for the function
/// instead of assuming in same datafolder and updating in background
function updateSingleSpikeInformation(
    variable iPoint
    , variable nss
    , variable burstWindow
    , wave backIntervalsToGaps
    , wave forwardIntervalsToGaps
    , wave dataset
    , wave ssFlags
    , wave ssPoints
    , wave ssTimes
)
    // print "in updateSingleSpikeInformation"

    // AGG - check if the backwards interval to gap for the previous spike is less than burst window
    if( backIntervalsToGaps[ ipoint - 1 ] < burstwindow ) 
        // AGG - need to flag this as a single spike with uncertainty
        ssFlags[ nss ] = 1
    else
        // AGG - the prior spike was far enough from start of recording/gap
        
        // AGG - check if the forward interval to gap for the previous spike is less than burst window
        if( forwardIntervalsToGaps[ ipoint - 1 ] < burstwindow )
            ssFlags[ nss ] = 1
        else
            // AGG - we can be confident we didn't miss spikes and that prior spike was single spike
        endif
    endif
    
    // AGG - all of the potential single spikes still get added to the ssPoints and ssTimes, regardless of ssFlags
    sspoints[ nss ] = ipoint - 1 // AGG - use nss as the index (before adding one for this point) to add previous point to sspoint
    ssTimes[ nss ] = dataset[ ipoint - 1 ] // AGG - add previous point to ssTimes
    nss += 1 // AGG - add a single spike to the counter for the previous spike

    return nss
end

// Updates spikeInfo, burstPoints, burstSpikeTimes, burstFlags
// Returned keyed string output with new iBurst and iBurstSpike index values
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added spikeInfo, burstPoints, burstSpikeTimes, and burstFlags as parameters for the function
/// instead of assuming in current datafolder and updating in the background
function/S endThePreviousBurst(
    variable iPoint
    , variable iBurstSpikes
    , variable iBurst
    , variable burstWindow
    , wave forwardIntervalsToGaps
    , wave dataset
    , wave spikeInfo
    , wave burstPoints
    , wave burstSpikeTimes
    , wave burstFlags
)
    // print "in endThePreviousBurst"

    // AGG - update the spike info for the previous spike (which ends the burst)
    spikeinfo[ ipoint - 1 ] = 1 // AGG - mark the prior point as part of a burst
    
    burstpoints[ iburstSpikes ] = ipoint - 1 // AGG - store the prior spike in burstpoints
    burstSpikeTimes[ iburstSpikes ] = dataset[ ipoint - 1 ] // AGG - store the prior spike time in burstSpikeTimes
    
    // AGG - Check if the previous spike was too close to the start of a gap/end of recording
    // AGG - Check for if the start of burst occurs too close to end of a gap/beginning of recording happens above during burst analysis
    if( forwardIntervalsToGaps[ ipoint - 1 ] < burstWindow )
        burstFlags[ iburst ] = 1
    endif
    
    iburstSpikes += 1 // AGG - add one to counter for number of spikes in bursts
    iburst += 1 // AGG - add one to counter for number of bursts

    string output = ""
    output += "iBurst:"
    output += num2istr(iBurst) + ";"
    output += "iBurstSpikes:"
    output += num2istr(iBurstSpikes) + ";"

    return output
end 

// Update inter_intervals, interTimes
// Return iInter index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: added inter_Intervals, interTimes as parameters,
/// instead of updating in background and assuming in same datafolder
function updateInterIntervals(
    variable iPoint
    , variable iInter
    , wave intervals
    , wave dataset
    , wave intervalGapFlag
    , wave inter_intervals
    , wave interTimes
)
    // print "in updateInterIntervals"

    // AGG - Check if there was a gap during this interval
    if( intervalGapFlag[ ipoint ] == 0 )
        // AGG - no gap
        inter_intervals[ iinter ] = intervals[ ipoint ] // AGG - add this interval to inter_intervals
        // AGG - 2021-04-29 - Mark timing to split inter_intervals for regions
        interTimes[ iinter ] = dataset[ ipoint ]
        
        iinter += 1 // AGG - add one to the counter for iinter
    else
        // print "AGG - A gap occurred during this interval, so we can't add to the inter list"
    endif

    return iInter
end

// Update ssFlags, ssTimes, ssPoints
// Return nss index
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-17
/// NOTES: Added ssFlags, ssPoints, and ssTimes to the parameters for the function
function handleLastSpikeInRecording_singleSpike(
    variable iPoint
    , variable nss
    , variable burstWindow
    , wave backIntervalsToGaps
    , wave forwardIntervalsToGaps
    , wave dataset
    , wave ssFlags
    , wave ssPoints
    , wave ssTimes
)
    // print "in handleLastSpikeInRecording_singleSpike"

    // AGG - check if the backwards interval to gap/start recording for this spike is less than burst window
    if( backIntervalsToGaps[ ipoint ] < burstwindow ) 
        // AGG - need to flag this as a single spike with uncertainty
        ssFlags[ nss ] = 1
    else
        // AGG - this spike was far enough from start of recording/gap
        
        // AGG - check if the forward interval to gap for this spike is less than burst window
        if( forwardIntervalsToGaps[ ipoint ] < burstwindow )
            ssFlags[ nss ] = 1
        else
            // AGG - we can be confident we didn't miss spikes and this last spike is single spike
        endif
    endif
    
    sspoints[ nss ] = ipoint // AGG - update the sspoints with this last point
    ssTimes[ nss ] = dataset[ ipoint ] // AGG - update the ssTimes with the time of this last point
    
    nss += 1

    return nss
end