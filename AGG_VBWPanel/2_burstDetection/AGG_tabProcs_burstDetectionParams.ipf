////////////////////////////////////////////////////////////////////////
////////////////////// VBW DETECTION PARAMETERS \\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////////////////////////////////

// Adapted from JPSmartConc for data folder references and modern structures

function AGG_vbwParamsProc (SV_struct) : SetVariableControl
    STRUCT WMSetVariableAction &SV_Struct

    switch (SV_struct.eventcode)
        case 6: // value changed
            AGG_update_num_intervals()
        break
    endswitch
    return 0
end

function AGG_vbwnumintsSetVarProc (SV_struct) : SetVariableControl
    STRUCT WMSetVariableAction &SV_Struct

    switch (SV_struct.eventcode)
        case 6: // value changed
            DFREF panelDFR = getVBWPanelDF()
            NVAR/Z/SDFR=panelDFR vbwmin, vbwmax, vbwint, vbw_num_ints
            // Changes the increment based on the min/max and desired # intervals
            vbwint = (vbwmax - vbwmin) / (vbw_num_ints - 1)
        break
    endswitch
    return 0
end

// TO-DO: update for if the same start/end; was getting nan error loops
function AGG_update_num_intervals()
    DFREF panelDFR = getVBWPanelDF()

	NVAR/Z/SDFR=panelDFR vbwmin, vbwmax, vbwint, vbw_num_ints

	variable num_ints = ((vbwmax - vbwmin) / vbwint) + 1

	if ((mod(round(num_ints * 1000000), 1000000)) == 0)
		// round number of intervals, so accept their change
		vbw_num_ints = num_ints
	else
		// work out suggested interval
		num_ints = round(num_ints)
		variable suggested_int = (vbwmax - vbwmin) / (num_ints - 1)

		// prompt with suggestion to make round number of intervals
		string warning = "Parameters will result in a non-round number of intervals! Suggested increment:"
		vbwint = getparam("Warning", warning, suggested_int)
		vbw_num_ints = ((vbwmax - vbwmin) / vbwint) + 1
	endif
end

function/WAVE makeBurstWindowsWave()
    DFREF panelDFR = getVBWPanelDF()
    NVAR/Z/SDFR=panelDFR vbwmin, vbwmax, vbwint, vbw_num_ints

    variable numBW = 0, ibw = 0

    // AGG - 2021-05-09 Getting index error for burst windows with index out of range.
    // I'm not positive that adding the round for vbw_num_ints was the answer
    // But it does seemed to have stopped with this change.
    make/O/D/N=( round(vbw_num_ints) ) panelDFR:burstWindows/WAVE=burstWindows

    for( ibw = 0; ibw < vbw_num_ints; ibw++ )
        burstWindows[ ibw ] = vbwmin + vbwint * ibw
    endfor

    return burstWindows
end

///////////
/// updateVBWParams
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-28
/// NOTES: 
///////////
function updateVBWParams([string hostN])
    DFREF panelDFR = getVBWPanelDF()

    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif

    string selectSliderName, selectEntryName
    strswitch (hostN)
        case "AGG_Bursts":
            selectSliderName = "selectBWSlider"
            selectEntryName = "selectBWEntry"
            break
        case "AGG_VBW":
            selectSliderName = "selectBWSlider_both4_5"
            selectEntryName = "selectBWEntry_both4_5"
            break
        default:
            selectSliderName = "selectBWSlider"
            selectEntryName = "selectBWEntry"
            break
    endswitch
    
    variable/G panelDFR:vbwmin_used/N=vbwmin_used, panelDFR:vbwmax_used/N=vbwmax_used, panelDFR:vbwint_used/N=vbwint_used, panelDFR:vbw_num_ints_used/N=vbw_num_ints_used
    NVAR/Z/SDFR=panelDFR vbwmin, vbwmax, vbwint, vbw_num_ints, selectedBW

    vbwmin_used = vbwmin
    vbwmax_used = vbwmax
    vbwint_used = vbwint
    vbw_num_ints_used = vbw_num_ints_used

    selectedBW = vbwmin

    Slider $selectSliderName limits = {vbwmin_used, vbwmax_used, vbwint_used}, win=$hostN // Update the slider when detection changes
    SetVariable $selectEntryName limits = {vbwmin_used, vbwmax_used, vbwint_used}, win=$hostN  // Update the set variable when detection changes
end