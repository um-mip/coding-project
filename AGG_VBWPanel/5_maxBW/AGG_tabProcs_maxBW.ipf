////////////////////////////////////////////////////////////////////////
////////////////////// GET MAX BW FOR A REGION \\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////////////////////////////////////////////////////

function plotMaxBWForRegionProc(LB_struct) : ListBoxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        DFREF panelDFR = getVBWPanelDF()
        string selRegion = getSelectedItem(LB_Struct.ctrlName, hostName = LB_Struct.win)
        clearDisplay(LB_struct.win + "#maxBWDisplay")
        DFREF analysisDFR = getVBWanalysisDF()
        if(dataFolderRefStatus(analysisDFR)==0)
            return 0
        endif
        DFREF specAnalysisDFR = getVBWSpecAnalysisDF(selRegion)
        if(dataFolderRefStatus(specAnalysisDFR)==0)
            return 0
        endif
        getMaxBW(selRegion)
        printGroupMaxBWVals(hostN = LB_Struct.win)
        plotMaxBW(selRegion, hostN = LB_Struct.win)
        updateGroupOutput(hostN = LB_Struct.win)
    endif

    return 0
end

///////////
/// plotMaxBW
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-04
/// NOTES: 
///////////
function plotMaxBW(string selectedRegion[, string hostN])
    if(paramIsDefault(hostN))
        hostN = "AGG_bursts"
    endif
    
    NVAR/Z/SDFR=getVBWPanelDF() maxBWdispState
    SetWindow $hostN, hook(traceN) = $""
    SetActiveSubWindow $hostN

    switch (maxBWdispState)
        case 1:
            plotAvgBurstFreqByGroup(selectedRegion, hostN = hostN)
            break
        case 2:
            plotAvgBurstFreqByGroup(selectedRegion, hostN = hostN)
            plotAllCellsBFtraces(selectedRegion, hostN = hostN)
            break
        case 3:
            DFREF byCellMaxDFR = getVBWMaxbyCellMaxDF(selectedRegion)
            Wave/SDFR=byCellMaxDFR/T groupWave
            Wave/SDFR=byCellMaxDFR cellMaxBW

            if(!WaveExists(groupWave) || !WaveExists(cellMaxBW))
                return NaN
            endif
            
            DFREF maxBWplotDFR = getVBWMaxBWPlotDF()
            plotGroupData(groupWave, cellMaxBW, storageDF = maxBWplotDFR, displayHost = hostN + "#maxBWDisplay", yLabel = "cell max BW (s)")
            break
        default:
            plotAvgBurstFreqByGroup(selectedRegion, hostN = hostN)
            
            break
    endswitch
    
end


function getMaxBWForGroupProc(LB_struct) : ListBoxControl
    STRUCT WMListboxAction &LB_Struct

    if(LB_Struct.eventcode == 2 || LB_Struct.eventcode == 4)
        string selRegion = getSelectedItem("regionListBox_maxBW", hostName = LB_Struct.win)
        clearDisplay(LB_struct.win + "#maxBWDisplay")
        getMaxBW(selRegion)
        printGroupMaxBWVals(hostN = LB_Struct.win)   
        plotAvgBurstFreqByGroup(selRegion, hostN = LB_Struct.win)
    endif

    return 0
end

///////////
/// maxBWDisplayChkProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-04
/// NOTES: 
///////////
function maxBWDisplayChkProc(CB_Struct) : CheckBoxControl
    STRUCT WMCheckboxAction & CB_Struct
    if(CB_Struct.eventcode == 2) // Mouse up
        NVAR/Z/SDFR=getVBWPanelDF() maxBWdispState

        strswitch (CB_Struct.ctrlName)
            case "maxDispTypeChk1":
                maxBWdispState = 1
                break
            case "maxDispTypeChk2":
                maxBWdispState = 2
                break
            case "maxDispTypeChk3":
                maxBWdispState = 3
                break
            default:
                
                break
        endswitch

        Checkbox maxDispTypeChk1, value = maxBWdispState ==1
        Checkbox maxDispTypeChk2, value = maxBWdispState ==2
        Checkbox maxDispTypeChk3, value = maxBWdispState ==3

        string selRegion = getSelectedItem("regionListBox_maxBW", hostName = CB_Struct.win)
        plotMaxBW(selRegion, hostN = CB_Struct.win)
    endif
    return 0
end
