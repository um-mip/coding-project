function plotAvgBurstFreqByGroup(string selectedRegion[, string hostN])
    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif
    string target = hostN + "#maxBWdisplay"

    if(!winExists(target))
        print "plotAvgBurstFreqByGroup: Target doesn't exist", target
        return NaN
    endif

    clearDisplay(target)

    DFREF regionDFR = getVBWSpecAnalysisDF(selectedRegion)
    if(dataFolderRefStatus(regionDFR)==0)
        print "plotAvgBurstFreqByGroup: region data folder doesn't exist"
        return NaN
    endif

    DFREF maxBWDFR = getVBWAnalysisMaxBWDF(selectedRegion)
    DFREF byCellMaxDFR = getVBWMaxbyCellMaxDF(selectedRegion)
    DFREF byAvgBFDFR = getVBWMaxBWbyAvgBFDF(selectedRegion)
    
    DFREF panelDFR = getVBWPanelDF()

    WAVE/T analysisNames = panelDFR:analysisNames

    string colors
    variable red, green, blue // for changing graph colors
   
    getUniqueTrtGroupsWave()
    WAVE/T uniqueGroups = panelDFR:uniqueGroups
    if(!WaveExists(uniqueGroups))
        return NaN
    endif
    
    WAVE burstWindows = panelDFR:burstWindows

    variable numGroups = numpnts(uniqueGroups)
    variable iGroup = 0

    variable plottingSomething = 0

    string legendString = ""

    Wave rgb_table = makeColorTableWave(numGroups) // default is to skip black

    for(iGroup = 0; iGroup < numGroups; iGroup ++)
        string avgBF_wn = "avgBF_group" + num2str(iGroup)
        string maxBWbyCell_name = "avgMaxBW_group" + num2str(iGroup)
        string maxBWbyBF_name = "maxBW_forAvgBF_group" + num2str(iGroup)

        NVAR byCellMax = byCellMaxDFR:$maxBWbyCell_name
        NVAR byBFMax = byAvgBFDFR:$maxBWbyBF_name

        DFREF maxBWplotDFR = getVBWMaxBWPlotDF()

        if(!waveExists(byAvgBFDFR:$avgBF_wn))
            continue
        endif
        plottingSomething = 1
        duplicate/O byAvgBFDFR:$avgBF_wn, maxBWplotDFR:$avgBF_wn/WAVE=toPlot
        string thisGroup = uniqueGroups[iGroup]
        AppendToGraph/W = $target toPlot/TN=$thisGroup vs panelDFR:burstWindows

        if(iGroup != 0)
            legendString += "\n"
        endif
        
        legendString += "\s(" + "'" + thisGroup + "'" + ")" + thisGroup


        ModifyGraph/W = $target rgb($thisGroup)=(rgb_table[iGroup][0], rgb_table[iGroup][1], rgb_table[iGroup][2]), lsize($thisGroup)=3
    endfor

    if(plottingSomething)
        string leftAxisText = "avg. burst freq (Hz)"
        string bottomAxisText = "burst window (s)"
        Label /W=$target left leftAxisText
        Label /W=$target bottom bottomAxisText
        Legend/W=$target /N=text0 /C/J/B=1 legendString
    endif
end

function printGroupMaxBWVals([string hostN])
    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif

    string regionLB, groupLB

    strswitch (hostN)
        case "AGG_Bursts":
            regionLB = "regionListBox_maxBW"
            groupLB = "groupsListBox"
            break
        case "AGG_VBW":
            regionLB = "regListBox_tab3"
            groupLB = "groupsListBox_tab3"
            break
        default:
            regionLB = "regionListBox_maxBW"
            groupLB = "groupsListBox"
            break
    endswitch
    
    DFREF panelDFR = getVBWPanelDF()

    // Clear everything before we check selections
    SVAR printGroupName = panelDFR:selGroupName
    NVAR printByCellMax = panelDFR:maxByCell, printByBFMax = panelDFR:maxByBF

    printGroupName = ""; printByCellMax = NaN; printByBFMax = NaN

    string selRegion = getSelectedItem(regionLB, hostName = hostN)
    string selGroup = getSelectedItem(groupLB, hostName = hostN)

    if(strlen(selRegion) == 0 || strlen(selGroup) == 0)
        return NaN
    endif

    DFREF regionDFR = getVBWSpecAnalysisDF(selRegion)

    if(DataFolderRefStatus(regionDFR) == 0)
        Print "printGroupMaxBWVals: Invalid data folder reference"
        return NaN
    endif

    DFREF byCellMaxDFR = getVBWMaxbyCellMaxDF(selRegion)
    DFREF byAvgBFDFR = getVBWMaxBWbyAvgBFDF(selRegion)

    ControlInfo /W = $hostN $groupLB
    variable groupSelRow = V_value
    printGroupName = selGroup

    string maxBWbyCell_name = "avgMaxBW_group" + num2str(groupSelRow)
    string maxBWbyBF_name = "maxBW_forAvgBF_group" + num2str(groupSelRow)
    NVAR byCellMax = byCellMaxDFR:$maxBWbyCell_name
    NVAR byBFMax = byAvgBFDFR:$maxBWbyBF_name
    if(NVAR_exists(byCellMax))
        printByCellMax = byCellMax
    endif
    if(NVAR_exists(byBFMax))
        printByBFMax = byBFMax
    endif
end

function plotAllCellsBFtraces(string selectedRegion[, string hostN])
    if(paramIsDefault(hostN))
        hostN = "AGG_Bursts"
    endif
    string target = hostN + "#maxBWdisplay"

    if(!winExists(target))
        print "plotAvgBurstFreqByGroup: Target doesn't exist", target
        return NaN
    endif

    // clearDisplay(target) // Don't clear, append

    DFREF regionDFR = getVBWSpecAnalysisDF(selectedRegion)
    if(dataFolderRefStatus(regionDFR)==0)
        print "plotAvgBurstFreqByGroup: region data folder doesn't exist"
        return NaN
    endif

    DFREF byAvgBFDFR = getVBWMaxBWbyAvgBFDF(selectedRegion)
    
    DFREF panelDFR = getVBWPanelDF()

    string colors
    variable red, green, blue // for changing graph colors
   
    getUniqueTrtGroupsWave()
    WAVE/T uniqueGroups = panelDFR:uniqueGroups
    if(!WaveExists(uniqueGroups))
        return NaN
    endif
    
    WAVE burstWindows = panelDFR:burstWindows

    variable numGroups = numpnts(uniqueGroups)
    variable iGroup = 0

    variable plottingSomething = 0

    string legendString = ""

    Wave rgb_table = makeColorTableWave(numGroups) // default is to skip black

    Wave/SDFR=byAvgBFDFR/T groupWave, cellWave
    Wave/SDFR=byAvgBFDFR/Wave cellBFwaveRefs

    variable iWave = 0, nWaves = numpnts(cellBFwaveRefs), nGroupPnts = numpnts(groupWave)
    
    if(nWaves != nGroupPnts)
        return NaN
    endif

    for(iWave=0; iWave<nWaves; iWave++)
        string thisGroup = groupWave[iWave]
        string thisCell = CellWave[iWave]
        wave cellBF = cellBFwaveRefs[iWave]

        DFREF maxBWplotDFR = getVBWMaxBWPlotDF()

        FindValue/Text=(thisGroup) uniqueGroups
        if(V_value < 0)
            continue
        endif

        iGroup = V_value

        string tn = thisCell
        duplicate/O cellBF, maxBWplotDFR:$(tn)/Wave=toPlot
        AppendToGraph/W=$target toPlot vs panelDFR:burstWindows

        ModifyGraph/W=$target rgb($tn)=(rgb_table[iGroup][0], rgb_table[iGroup][1], rgb_table[iGroup][2]), lsize($tn) = 1
    endfor

    SetWindow $hostN, hook(traceN) = showTraceNWinHook
    SetActiveSubWindow $target
end

Function showTraceNWinHook(s)
	STRUCT WMWinHookStruct &s
	
	// 20171128 modified so the setvar doesn't popup everywhere
	string graphName = "maxBWDisplay"
    [string hostN, string subN] = splitFullWindowName(s.winName)
	if( stringmatch( subN, graphName ) ) // 20171128
        // print s.winName

        string res=tracefrompixel(s.mouseloc.h,s.mouseloc.v,"DELTAX:3;DELTAY:3")
        SVAR/SDFR=getVBWPanelDF() gTraceN = traceN

        gTraceN = ""
        if(strlen(res)>0)
            // print res
            string traceN=stringbykey("TRACE",res)
            // Wave traceW = TraceNameToWaveRef(s.winName, traceN) // if need the actual wave
            
            gTraceN = traceN
        endif
    endif
end

