///////////
/// analyzeBurstsForCellRegion
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: For a specific cell/region combination, analyze the bursts for all burst windows
/// useRecDur: 0 = NO (use total duration), 1 = YES (use recording duration - removes gap times)
/// if don't provide a burstWindows wave, will find the burst window folders within the cell detection folder
///////////
function analyzeBurstsForCellRegion(string cellName, string regionName[, variable useRecDur, wave burstWindows])
    if(paramIsDefault(useRecDur))
        useRecDur = 0 // default to using total duration
    endif

    [variable startTime, variable endTime] = getCellRegionStartEndTime(cellName, regionName)

    if(numtype(startTime)!=0 || numtype(endTime)!=0)
        print "analyzeBurstsForCellRegion: Couldn't find start and end time for cell", cellName, "region", regionName
        return NaN 
    endif
    
    DFREF analysisDFR = getVBWanalysisDF()
    if( DataFolderRefStatus(analysisDFR) == 0) // if doesn't exist
        createVBWanalysisDF()
        DFREF analysisDFR = getVBWanalysisDF()
    endif

    DFREF analysisRegDFR = getVBWSpecAnalysisDF(regionName)
    if(dataFolderRefStatus(analysisRegDFR)==0)
        createVBWSpecAnalysisDF(regionName)
        DFREF analysisRegDFR = getVBWSpecAnalysisDF(regionName)
    endif

    DFREF cellBurstDetectDFR = getVBWCellDetectionDF(cellName)
    if(dataFolderRefStatus(cellBurstDetectDFR)==0)
        print "analyzeBurstsForCellRegion: Couldn't find burst detection folder for cell", cellName, ". Trying to run detection now"
        wave sctDataset = getCellSCTWave(cellName)
        if(!WaveExists(sctDataset))
            print "analyzeBurstsForCellRegion: Couldn't find SCT wave for cell", cellName, ". Stopping analysis"
            return NaN
        endif

        if(paramIsDefault(burstWindows) || !WaveExists(burstWindows))
            Wave burstWindows = makeBurstWindowsWave()
        endif
        
        AGG_vbwDetection(sctDataset, cellName, burstWindows)
        DFREF cellBurstDetectDFR = getVBWCellDetectionDF(cellName)
        if(dataFolderRefStatus(cellBurstDetectDFR)==0)
            print "analyzeBurstsForCellRegion: Still couldn't find burst detection folder for cell", cellName, ". Stopping analysis"
            return NaN
        endif
    endif

    DFREF cellAnalysisDFR = getVBWCellAnalysisDF(regionName, cellName)
    if(dataFolderRefStatus(cellAnalysisDFR)!=0)
        KillDataFolder cellAnalysisDFR // left off Z, so it will error if can't delete. Add /Z before deploying
    endif

    createVBWCellAnalysisDF(regionName, cellName)
    DFREF cellAnalysisDFR = getVBWCellAnalysisDF(regionName, cellName)

    variable/G cellAnalysisDFR:useRecDur/N=gUseRecDur
    gUseRecDur = useRecDur

    DFREF cellRegionDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = regionName)
    NVAR/Z/SDFR=cellRegionDFR recDur = totalRecDur

    if(paramIsDefault(burstWindows) || !WaveExists(burstWindows))
        Wave/SDFR=cellBurstDetectDFR burstWindows // check in the cell burst detection folder first
        if(!WaveExists(burstWindows))
            Wave burstWindows = makeBurstWindowsWave()
        endif
    endif

    variable numBWs = numpnts(burstWindows)

    SetDataFolder cellAnalysisDFR
    string/G cellAnalysisDFR:cellName/N=gCellName
    gCellName = cellName

    // Output burst waves
    duplicate/O burstWindows, bww
    
    // instead of saving each of the values as variables within a folder
    // we're going to save them within these output waves, where the index corresponds to the burst window index
    make/O/D/N=( numBWs ) bn, mbd, spb, bf, ssn, ssf, tf, inter, intra, bFlags, ssFlags

    bn = 0
    mbd = nan
    spb = nan
    bf = 0
    ssn = 0
    ssf = 0
    tf = 0
    inter = nan
    intra = nan
    bFlags = nan
    ssFlags = nan

    variable/G maxBW = nan, duration = endTime - startTime

    variable ibw = 0

    for(ibw=0; ibw<numBWs; ibw++)
        // Get folder reference for each burst window detection folder
        variable bw = burstWindows[ibw]
        DFREF bwFolder = getVBWCellDetectionBWDF(cellName, bw)
        
        // Run the analysis at this burst window
        [variable numBursts, variable burstDuration, variable nSpikesPerBurst, variable burstFrequency, variable numSingleSpikes, variable singleSpikeFrequency, variable totalFrequency, variable averageInterEventInterval, variable averageIntraEventInterval, variable numburstFlags, variable numssFlags] = AGG_burstAnalysisForBW(\
            bwFolder, startTime, endTime, useRecDur = useRecDur, recDur = recDur)
        
        bn[ibw] = numBursts
        mbd[ibw] = burstDuration
        spb[ibw] = nSpikesPerBurst
        bf[ibw] = burstFrequency
        ssn[ibw] = numSingleSpikes
        ssf[ibw] = singleSpikeFrequency
        tf[ibw] = totalFrequency
        inter[ibw] = averageInterEventInterval
        intra[ibw] = averageIntraEventInterval
        bFlags[ibw] = numburstFlags
        ssFlags[ibw] = numssFlags
    endfor

    variable maxBN = WaveMax(bn)
    if(maxBN > 0)
        findValue/V=(maxBN) bn
        variable maxBN_index = V_value
        maxBW = burstWindows[maxBN_index]
    else
        maxBN = NaN
    endif

    SetDataFolder root:
end

/////////////////////////////////////////////
// burst analysis for a specific burst window that has already been detected
// 
// bwFolder should be a reference to the data folder that contains the detected waves for this cell/bw combo
// 
// will select the bursts/single spikes/intervals that are between the provided analysisStart/analysisEnd
///////////////////////////////////////////////
/// EDITOR: Amanda Gibson
/// UPDATED DATE: 2024-05-24
/// NOTES: Changed from a keyed string ouput to output all of the parameters as variables
/// Also switched to use extractWaveWithinRegion to get the region information as free waves,
/// this reduced the need to create and then kill a temporary folder for the region waves
/// and makes the code easier to read, and takes advantage of extract instead of for loops through every wave
function [
    variable regNumBursts,
    variable burstDuration,
    variable nSpikesPerBurst,
    variable burstFrequency,
    variable regNumSingleSpikes,
    variable singleSpikeFrequency,
    variable totalFrequency,
    variable averageInterEventInterval,
    variable averageIntraEventInterval,
    variable numburstFlags,
    variable numssFlags
] AGG_burstAnalysisForBW(DFREF bwFolder, variable analysisStart, variable analysisEnd[, variable useRecDur, variable recDur])
	// DFREF bwFolder
	// variable analysisStart
	// variable analysisEnd
    // variable useRecDur // 0 = NO (use total duration), 1 = YES (use recording duration - takes gaps into account)
    // variable recDur

    if( paramisdefault( useRecDur ))
        useRecDur = 0 // default to using total duration
    endif

    if( useRecDur && paramisdefault( recDur ))
        recDur = analysisEnd - analysisStart
        print "WARNING: AGG_burstAnalysisForBW - no recording duration provided"
    endif

    if(DataFolderRefStatus(bwFolder) == 0)
        print "AGG_burstAnalysisForBW - ERROR: BW FOLDER DOESN'T EXIST"
        return [regNumBursts, burstDuration, nSpikesPerBurst, burstFrequency, regNumSingleSpikes, singleSpikeFrequency, totalFrequency, averageInterEventInterval, averageIntraEventInterval, numburstFlags, numssFlags]
    endif        

    variable msecConversion = 1
    variable duration
    if(useRecDur)
        duration = recDur
    else
        duration = analysisEnd - analysisStart
    endif
    // print "duration used in bw", duration

    variable reg_npoints = 0

    // About each burst
    Wave/SDFR=bwFolder burstStartTimes, burstSpikes, burstDurations, burstFlags//, burstStartPoints

    // About each spike in a burst
    Wave/SDFR=bwFolder burstSpikeTimes//, burstPoints

    // About intraBurst intervals
    NVAR/Z/SDFR=bwFolder numIntraIntervals
    Wave/SDFR=bwFolder intraInt_burstStart, intra_Intervals

    // About interEvent intervals
    NVAR/Z/SDFR=bwFolder numInterIntervals
    Wave/SDFR=bwFolder interTimes, inter_Intervals

    // Single Spike Variables and waves
    NVAR/Z/SDFR=bwFolder nSS
    Wave/SDFR=bwFolder ssTimes, ssFlags//, ssPoints

    // Region burst info - only select bursts whose start time is within the region
    Wave reg_burstSpikes = extractWaveWithinRegion(burstStartTimes, analysisStart, analysisEnd, infoWave = burstSpikes)
    Wave reg_burstDurations = extractWaveWithinRegion(burstStartTimes, analysisStart, analysisEnd, infoWave = burstDurations)
    Wave reg_burstFlags = extractWaveWithinRegion(burstStartTimes, analysisStart, analysisEnd, infoWave = burstFlags)
    regNumBursts = numpnts(reg_burstSpikes)

    // Region burst spikes info
    Wave reg_burstSpikeTimes = extractWaveWithinRegion(burstSpikeTimes, analysisStart, analysisEnd)
    variable regNumBurstSpikes = numpnts(reg_burstSpikeTimes)

    // Single Spikes within Region
    Wave reg_ssFlags = extractWaveWithinRegion(ssTimes, analysisStart, analysisEnd, infoWave = ssFlags)
    regNumSingleSpikes = numpnts(reg_ssFlags)

    // Intervals within Region
    Wave reg_intraIntervals = extractWaveWithinRegion(intraInt_burstStart, analysisStart, analysisEnd, infoWave = intra_Intervals)
    Wave reg_interIntervals = extractWaveWithinRegion(interTimes, analysisStart, analysisEnd, infoWave = inter_intervals)

    //\\//\\//\\//\\//\\//\\ SUMMARY CALCULATIONS //\\//\\//\\//\\//\\

    burstDuration = 0; nSpikesPerBurst = 0; burstFrequency = 0; numburstFlags = 0
    singleSpikeFrequency = 0; totalFrequency = 0; numssFlags = 0
    averageInterEventInterval = 0; averageIntraEventInterval = 0

    if( regNumBursts > 0 )
        wavestats/Z/Q reg_burstDurations
        burstDuration = V_avg
        wavestats/Z/Q reg_burstSpikes
        nSpikesPerBurst = V_avg

        burstFrequency = regNumBursts / duration * msecConversion
        numburstFlags = sum(reg_burstFlags)
    else // No bursts
        burstDuration = nan
        nSpikesPerBurst = nan
        burstFrequency = 0
        numburstFlags = nan
    endif

    singleSpikeFrequency = regNumSingleSpikes / duration * msecConversion
    reg_npoints = regNumSingleSpikes + regNumBurstSpikes
    numssFlags = sum(reg_ssFlags)
    totalFrequency = reg_npoints / duration * msecConversion
    averageInterEventInterval = mean(reg_interIntervals)
    averageIntraEventInterval = mean(reg_intraIntervals)
    
    return [regNumBursts, burstDuration, nSpikesPerBurst, burstFrequency, regNumSingleSpikes, singleSpikeFrequency, totalFrequency, averageInterEventInterval, averageIntraEventInterval, numburstFlags, numssFlags]
end

function isWithinRange(valToCheck, startTime, endTime)
    variable valToCheck, startTime, endTime

    variable isWithinRange = 0

    // Can equal startTime, has to be less than endTime
    if(valToCheck >= startTime && valToCheck < endTime)
        isWithinRange = 1
    endif

    return isWithinRange
end