///////////
/// analyzeCellBurstsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: 
///////////
function analyzeCellBurstsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        string cellName = getSelectedItem("cellListBox_burst", hostName = B_Struct.win)
        
        Wave/T/SDFR=getVBWPanelDF() cellRegionNames
        Wave/SDFR=getVBWPanelDF() selRegions = regionSelWave_CellBurst

        variable iRegion = 0, nRegions = numpnts(cellRegionNames)
        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = cellRegionNames[iRegion]
            variable isSelected = selRegions[iRegion]
            if(isSelected)
                analyzeBurstsForCellRegion(cellName, regionName)
            endif
        endfor
        fillCellBurstOutTable()
    endif
    return 0
end

///////////
/// analyzeAllCellsBurstsProc
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-05-24
/// NOTES: 
///////////
function analyzeAllCellsBurstsProc(B_Struct) :ButtonControl
    STRUCT WMButtonAction & B_Struct
    if(B_Struct.eventcode == 2)
        DFREF eventsDFR = getEventsPanelDF()
        if(dataFolderRefStatus(eventsDFR)==0)
            return NaN
        endif

        Wave/SDFR=eventsDFR/T cellNames = cellName
        if(!WaveExists(cellNames))
            return NaN
        endif

        DFREF cellsOutDFR = getAllCellsOutputDF()
        Wave/SDFR=cellsOutDFR/T allRegionNames
        Wave/SDFR=getVBWPanelDF() allRegionNames_sel
        variable iRegion = 0, nRegions = numpnts(allRegionNames)

        for(iRegion=0; iRegion<nRegions; iRegion++)
            string regionName = allRegionNames[iRegion]
            variable isSelected = allRegionNames_sel[iRegion]
            if(isSelected)
                for(string cellName : cellNames)
                    DFREF cellRegionDFR = returnCellRegionsDFR_bySubType(cellName, "all", regionName = regionName)
                    if(dataFolderRefStatus(cellRegionDFR)==0) // no region cell combo
                        continue
                    endif

                    analyzeBurstsForCellRegion(cellName, regionName)
                endfor
            endif
        endfor
        fillCellBurstOutTable()
    endif
    return 0
end