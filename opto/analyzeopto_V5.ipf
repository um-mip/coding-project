﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3				// Use modern global access method and strict wave access
#pragma DefaultTab={3,20,4}		// Set default tab width in Igor Pro 9 and later






function analyzeOpto(wave iwave, wave stimwave, wave/wave peaksInfo[, string hostPanel])
	if(paramIsDefault(hostPanel)) // for output graphs
		hostPanel = ""
	endif
	
	// iwave = current or voltage wave (electrode)
	// stimwave = output to LED
	// peaksInfo = data from eventdetection {_pks, _pk2, _ptb, _dtb} // AGG - wrong order
	// peaksInfo = data from eventdetection {_ptb, _pks, _pk2, _dtb} 


	// get stimulus info, make peristimulus waves, and classify peaks using the default values
	
	variable/G opto_windowStart = 0
	variable/G opto_windowEnd = 0.02
	
	wave/WAVE stimTrainInfo = detectStim(stimWave)
	if(numpnts(stimTrainInfo[0])>0) // if there's at least 1 stim
		wave/t periStimWaves = makeperistimulusWaves(iwave, stimTrainInfo[0],0.003)
													// 	wave stimStarts,  wave STimEnds,    wave wpeaks_tb, wave wpeaks, wave wpeaks2,   wave dtb, variable detectSTart, variable detectEnd
		wave/wave classWave3D_results = classifyPeaks3D(stimTrainInfo[0], stimTrainInfo[1], peaksInfo[0], peaksInfo[1], peaksInfo[2],peaksInfo[3], opto_windowStart, opto_windowEnd, hostPanel = hostPanel)

		// Make text wave of peristimulus wave names for displaying in listbox. Two columns so that we can store more info in 2nd column
		Duplicate/O periStimWaves listboxtextwave
		Redimension/N=(-1,2) listboxtextwave 
		
		//Make graph windows (empty for now)

		String sw = "selectedWave"
		String ew = "evoked"
		String ne = "not_evoked"
		
		if(strlen(hostPanel)==0)
			KillWindow/Z $sw
			KillWindow/Z $ew
			KillWindow/Z $ne
			
			variable winBuffer=4 // buffer so windows do not overlap
			variable winheight=25 // window height (percent of screen)
			variable winWidth = 30 // window width (percent of screen)
			variable startX = 32 // starting x position (percent of screen)
			variable startY = 10 // starting y position (percent of screen)

			Make/O/FREE coords = {startX, startY, startX + winWidth, startY + winheight} //Adjust graph window size/positioning here
			Make/O/FREE next = {0,winheight+winbuffer,0,winheight+winbuffer}
			
			AGG_makeDisplay(coords[0],coords[1],coords[2],coords[3], sw)
			coords = coords + next
			AGG_makeDisplay(coords[0],coords[1],coords[2],coords[3], ew)
			coords = coords + next
			AGG_makeDisplay(coords[0],coords[1],coords[2],coords[3], ne)
		else
			clearDisplay(hostPanel + "#" + sw)
			clearDisplay(hostPanel + "#" + ew)
			clearDisplay(hostPanel + "#" + ne)
		endif

		// global variables
		variable/g selWaveNum = 0
		variable/g classRun = 0 // don't try to display waves that do not exist yet (if user hasn't run classification yet)
		
		// junk for building the ListBox from classText 
		variable numrows = numpnts(periStimwaves)
		variable numcols = 2
		
		// userdata for passing wave names to panel controls. Keyed string 
		
		String ud="IWAVE:"+nameofwave(iwave)+";PEAKSINFO:"+nameofwave(peaksInfo)+";STIMWAVE:"+nameofwave(stimwave) 
		ud+= ";STIMINFO:" +nameofwave(stimTrainInfo)+";EPEAKSSTATS:"+nameofwave(classWave3D_results[1])+";N_EPEAKSSTATS:"+nameofwave(classWave3D_results[2])
		ud+= ";CLASSWAVE:"+nameofwave(classWave3D_results[0])+";PWAVES:" + nameofwave(periStimWaves) + ";HOSTPANEL:" + hostPanel + ";"

		// panel and listbox stuff
		if(WinType("detectionPanel")!=0)
			// print "panelExists"
			KillWindow/Z detectionPanel
		endif
		AGG_makePanel(5, 5, 30, 90, "detectionPanel")
		variable xPos = posRelPanel(0.02, "width")
		variable yPos = posRelPanel(0.01, "height")
		variable buttonWidth, buttonHeight
		[buttonWidth, buttonHeight] = xyRelPanel(0.96, 0.047)

		Button button0,pos={xPos,yPos},size={buttonWidth,buttonHeight},title="Detect evoked events", proc = ButtonProc_reClass, userData=(ud)
		yPos += buttonHeight * 1.2
		// controls for setting the evoked detection window 
		SetVariable setvar1, title = "window start (s)", size={buttonWidth * 0.45,buttonHeight},pos={xPos,yPos}, value = opto_windowStart
		SetVariable setvar2, title = "window end (s)", size = {buttonWidth * 0.45,buttonHeight},pos={xPos + buttonWidth * 0.5,yPos}, value = opto_windowEnd
		yPos += buttonHeight * 1.2

		Button button1,pos={xPos,yPos},size={buttonWidth, buttonHeight},title="Show/Hide evoked detection window", proc = ButtonProc_eShow 
		yPos += buttonHeight * 1.2
		
		Button button2, pos={xPos,yPos},size={buttonWidth, buttonHeight},title="Show source waves", proc = ButtonProc_showSource, userData = (ud)
		yPos += buttonHeight * 1.2
		
		Make /O/N=(numrows,numcols) selWave1
		Make /O/T/N=(4) colTitles = {"periStim wave","evoked?"}	 
		ListBox list0 size={buttonWidth,posRelPanel(0.75, "height")},pos={xPos, yPos},listWave=listboxtextwave,mode=1,selRow=0 ,selwave=selwave1, titleWave = colTitles,proc=ListBoxProc_1, userData = (ud)
	else
		print "no red-light stimulation for this series"
	endif
end

Function ListBoxProc_1(lba) : ListBoxControl
	
	STRUCT WMListboxAction &lba
	WAVE/T/Z classText = lba.listWave
	wave classWave3D = $StringByKey("CLASSWAVE",lba.userdata)
	NVAR selWaveNum
	NVAR classRun
	SVAR classwavename
	String waven

	print "userdata", lba.userdata
	string hostPanel = StringByKey("HOSTPANEL", lba.userdata)
	// hostPanel = ""
	string target = "selectedWave"
	if(strlen(hostPanel)>0)
		target = hostPanel + "#" + target
	endif
	
	switch( lba.eventCode )
		case 4: // cell selection
			selWaveNum = lba.row
			waven = classText[selWaveNum][0]
			wave w = $waven
			if(WinType(target)==1)
				print "found selected wave", target
			else
				print "couldn't find selected wave"
				target = "selectedWave"
				Display $target
			endif

			RemoveFromGraph/W=$target/ALL
			AppendtoGraph/W=$target $waven
			if (classRun==1)
				wave classWave3D = $classwavename
				showpeaksnew(selWaveNum, classWave3D, hostPanel = hostPanel)
			endif
			break
	endswitch

	return 0
End

Function ButtonProc_showSource(ba) : ButtonControl
	STRUCT wmbuttonaction&ba
	wave iwave = $StringByKey("IWAVE",ba.userdata)
		
	switch( ba.eventCode )
		case 2: // mouse up
		
			
			wave epeakstats = $StringByKey("EPEAKSSTATS",ba.userdata)
			wave not_epeaks = $StringByKey("N_EPEAKSSTATS",ba.userdata)
			Splitwave/OREF=refwave epeakstats
			Splitwave/OREF=refwave2 not_epeaks 
			wave wpeaks2 = refwave[1]
			wave wpeaks_tb = refwave[2]
			
			wave nwpeaks2 = refwave2[1]
			wave nwpeaks_tb = refwave2[2]
			
			Display iwave
			AppendtoGraph wpeaks2 vs wpeaks_tb
			ModifyGraph mode($nameofwave(wpeaks2))=3,marker($nameofwave(wpeaks2))=19,rgb($nameofwave(wpeaks2))=(0,65535,0)
			
			AppendtoGraph nwpeaks2 vs nwpeaks_tb
			ModifyGraph mode($nameofwave(nwpeaks2))=3,marker($nameofwave(nwpeaks2))=19,rgb($nameofwave(nwpeaks2))=(0,0,0)
			
			wave stimbars =$StringFromList(0,wavelist("*_bars",";",""))
			Duplicate/O stimbars $(nameofwave(stimbars)+"_neg")
			wave stimbars_n = $(nameofwave(stimbars)+"_neg")
		
			DoUpdate
			GetAxis/Q left
			DoUpdate
			
			
			stimbars_n = (numtype(stimbars_n[p])==0) ? V_min : stimbars_n[p]
			stimbars = (numtype(stimbars[p])==0) ? V_max : stimbars[p]
			
			AppendtoGraph stimbars, stimbars_n
			ModifyGraph mode($nameofwave(stimbars))=7,hbFill($nameofwave(stimbars))=5,toMode($nameofwave(stimbars))=1, lsize($nameofwave(stimbars_n))=0, lsize($nameofwave(stimbars))=0
			
			Reordertraces iwave,{$nameofwave(stimbars),$nameofwave(stimbars_n)}
			SetScale d 0,1,"A", stimbars, stimbars_n
			
									
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Function ButtonProc_eShow(ba) : ButtonControl
	STRUCT wmbuttonaction&ba
	NVAR opto_windowStart
	NVAR opto_windowEnd
	switch( ba.eventCode )
		case 2: // mouse up
		
			String tname =  StringFromList(0,tracenamelist("",";",1))
		
			// if it's displayed, remove
			if (stringmatch(tracenamelist("",";",1),"*detectWin*") == 1)
				RemoveFromGraph detectWinTop, detectWinBot
			else
		
				DoUpdate
				GetAxis/Q left
				Make/O/D/N=(2) detectWinTop = {V_max,V_max}
				Make/O/D/N=(2) detectWinBot = {V_min,V_min}
				SetScale/I x opto_windowStart,opto_windowEnd,"s", detectWinBot,detectWinTop
				SetScale/P d, 0, 1, "A", detectWinBot,detectWinTop// figure out a better way to do this by getting it from the trace
				AppendtoGraph detectWinTop, detectWinBot
				ModifyGraph mode(detectWinTop)=7,hbFill(detectWinTop)=5,toMode(detectWinTop)=1, lsize(detectWinBot)=0, lsize(detectWinTop)=0
				ReorderTraces $tname, {detectWinTop, detectWinBot}
				
			endif			
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Function ButtonProc_reClass(ba) : ButtonControl
	
	STRUCT wmbuttonaction&ba
	variable/G opto_windowStart
	variable/G opto_windowEnd
	variable/G classRun
	
	
	wave/wave stimTrainInfo = $StringByKey("STIMINFO",ba.userdata)
	wave stimStarts = stimTrainInfo[0]
	wave stimEnds = stimTrainInfo[1]
	 
	wave/wave peakInfo = $StringByKey("PEAKSINFO",ba.userdata)
	wave wpeaks_tb = peakInfo[0]
	wave wpeaks = peakInfo[1]
	wave wpeaks2 = peakInfo[2]
	
	wave/t periStimWaves = $StringByKey("PWAVES",ba.userdata)
	
	wave/t w =$("listboxtextwave")

	string hostPanel = StringByKey("HOSTPANEL", ba.userdata)
	string evokedGraph = "evoked", notEvokedGraph = "not_evoked"
	if(strlen(hostPanel)>0)
		evokedGraph = hostPanel + "#" + evokedGraph
		notEvokedGraph = hostPanel + "#" + notEvokedGraph
	endif
	
	switch( ba.eventCode )
		case 2: // mouse up
		
			wave/wave classWave3D_results = classifyPeaks3D(stimTrainInfo[0], stimTrainInfo[1], peakInfo[0], peakInfo[1], peakInfo[2],peakinfo[3], opto_windowStart, opto_windowEnd, hostPanel = hostPanel)
			String/G classWavename = nameofwave(classWave3D_results[0])
			wave classWave3D = $classWavename
			
			wave/wave sortResult = sort3DWave(classWave3D, periStimWaves)
			wave w2 = sortResult[0]
			w[][1] = num2str(w2[p])
			variable numEvoked = sum(w2) // store this somewhere
			print removeEnding(classwavename, "_classWave"), "numEvoked:", numEvoked
			classRun = 1
			
			wave eAvg = tWaveListAverage(sortResult[1], "evokedwaveAVG")
			wave n_eAvg = tWaveListAverage(sortResult[2], "notevokedWaveAVG")
			
			RemoveFromGraph/W=$evokedGraph/ALL			
			plotTwaveList(sortResult[1],windowname=evokedGraph)
			AppendtoGraph/W=$evokedGraph eAvg
			if(strlen(hostPanel)==0)
				DoWindow/F $evokedGraph // I don't know how this does as a subpanel
			endif
			rainbow(targetName = evokedGraph)
			ModifyGraph/W=$evokedGraph lsize($(nameOfWave(eAvg)))=2,rgb($(nameOfWave(eAvg)))=(34952,34952,34952)
			
			RemoveFromGraph/W=$notEvokedGraph/ALL
			plotTwaveList(sortResult[2],windowname=notEvokedGraph)
			AppendtoGraph/W=$notEvokedGraph n_eAvg
			if(strlen(hostPanel)==0)
				DoWindow/F $notEvokedGraph
			endif
			rainbow(targetName = notEvokedGraph)
			ModifyGraph/W=$notEvokedGraph lsize($(nameOfWave(n_eAvg)))=2,rgb($(nameOfWave(n_eAvg)))=(34952,34952,34952)
			
			
								
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End



function/WAVE sort3DWave(wave classWave3D, wave/t pStimWaveNames)

	String wn = nameofwave(classWave3D)
	// String expr = "([[:alpha:]]+)_classWave3D"
	String expr = "(.*)_classWave3D" // AGG - want more than just alpha characters, everything before _classWave3D
	String basename
	SplitString/E=(expr) wn, basename
	
	variable numTrials = dimsize(classWave3D,2)
	
	String cw = basename + "_classWave"
	string ct = basename + "_classText"
	
	String evokedWaveNames = basename + "_evokedWaves"
	String not_evokedWaveNames = basename + "_not_evokedWaveNames"
	
	Make/I/O/N=(numTrials) $cw
	wave classWave = $cw
	Make/T/O/N=(numTrials) $ct
	wave/t classText = $ct

		
	variable i
	for(i=0;i<dimsize(classwave3D,2);i+=1)
		wave w = cleanStimLayer(i,classWave3D)
		Splitwave/SDIM=1/FREE/OREF=refwave w
		wave v = refwave[4]
		Extract/INDX/FREE v, tempwave, v==0
		if (numpnts(tempwave)>0)
			classWave[i] = 1		
		else
			classWave[i] = 0			
		endif
		
	endfor
	
	Makelist(pStimWaveNames,classWave,1,evokedWaveNames)
	Makelist(pStimWaveNames,classWave,0,not_evokedWaveNames)
	
	Make/O/WAVE sortResult = {classWave,$evokedWaveNames, $not_evokedWaveNames}
	
	return sortResult
		
end




function/wave textWaveFromClass3D(wave classWave3D, wave/t pStimWaves)

	// from the 3D class wave, generate a 2D text wave for displaying in the listbox
	// each layer is now a row in the text wave
	// show name of periStimulus wave, evoked peak amplitude and latency, and number of non-evoked peaks

	// get the base wavename from classWave3D
	String wn = nameofwave(classWave3D)
	// String expr = "([[:alpha:]]+)_classWave3D"
	String expr = "(.*)_classWave3D" // AGG - want more than just alpha characters, everything before _classWave3D
	String basename
	SplitString/E=(expr) wn, basename
	basename = basename + "_classText"
	Make/O/T/FREE/N=(dimsize(classWave3D,2),5) ct
	
	
	variable i
	
	for (i=0;i<dimsize(classWave3D,2);i+=1)
		wave w = cleanStimLayer(i,classWave3D)
		Duplicate/O/RMD=[0,0][0,dimsize(w,1)] w v
		SortColumns/A keyWaves = v, sortWaves = w
		ct[i][] = num2str(w[0][q])
	 endfor
	
	TextWave_NaNstoEmpty(ct)
	Concatenate/NP/O {pStimWaves,ct}, $basename
	wave/t classText = $basename
	return classText
	
end

function showPeaksNew(variable stimNum, wave classWave3D[, string hostPanel])
	if(paramIsDefault(hostPanel))
		hostPanel = ""
	else
		if(strlen(hostPanel)>0)
			hostPanel += "#"
		endif
	endif

	string selGraph = hostPanel + "selectedWave"
	
	
	wave peaksInfo = cleanStimLayer(stimNum, classWave3D)
		
	// are there peaks in this stimulus layer?
	if (peaksInfo[0][0] != NaN)
	
		// split this stimulus layer into evoked vs not evoked peaks 

		Duplicate/O/RMD=[0,dimsize(peaksInfo,0)][5,5] peaksInfo evokedCol // split out to just the evoked categorization column because using Extract on 2d wave doesn't work?
		
		Extract/FREE/INDX evokedCol, freewave1, evokedCol[p] == 0
		Extract/FREE/INDX evokedCol, freewave2, evokedCol[p] != 0

		// create separate 1D waves for the evoked peaks, if there are any and add the peaks to the graph 
		if (numpnts(freewave1)>0)
			print(numpnts(freewave1))
			Make/O/N =(numpnts(freewave1),5) evokedPeaks = peaksInfo[freewave1[p]][q]
			SplitWave/OREF=d_ePeaks/SDIM=1 evokedPeaks
			string wnam = nameofwave(d_ePeaks[1])
			AppendtoGraph/W=$selGraph d_ePeaks[1] vs d_ePeaks[3]
			
			ModifyGraph/W=$selGraph mode($wnam)=3,marker($wnam)=19,rgb($wnam)=(0,65535,0)
		endif
		
		// create separate 1D waves for the not-evoked peaks and add to graph 
		if (numpnts(freewave2)>0)
			print numpnts(freewave2)
			Make/O/N = (numpnts(freewave2),5) notEvoked = peaksInfo[freewave2[p]][q]
			SplitWave/OREF=d_n_ePeaks/SDIM=1 notEvoked
			String wnam1 = nameofwave(d_n_ePeaks[1])

			AppendtoGraph/W=$selGraph d_n_epeaks[1] vs d_n_epeaks[3]
			ModifyGraph/W=$selGraph mode($wnam1)=3,marker($wnam1)=19,rgb($ wnam1)=(0,0,0)
		endif
		
	endif
	
end

function/WAVE makePeriStimulusWaves(wave iwave, wave stimStarts, variable preStim)

	String waven = NameofWave(iwave)
	String psWaven
	String psWaven_all = waven + "_psWaveNames" 
	
	variable samplingInterval = pnt2x(iwave,2) - pnt2x(iwave,1)
	variable stimulusInterval = stimStarts[1]-stimStarts[0]
	
	variable startX
	variable endX
	variable postStim =stimulusInterval // x duration to show after the stimulus (s)
	
	Make/O/T/N=(numpnts(stimStarts)) $psWaven_all
	wave/t pStimWaves = $psWaven_all
	
	variable i
	
	for (i=0;i<numpnts(stimStarts);i+=1)
		psWaven = waven+"_pStim"+num2str(i)
		pStimWaves[i] = psWaven
		startX = stimStarts[i] - preStim
		endX = stimStarts[i] + postStim
		Duplicate/O/R=(startX,endX) iwave $psWaven
		SetScale/P x -1* preStim,samplingInterval,"s", $psWaven
	endfor
	
	return pStimwaves
end


function/WAVE cleanStimLayer(variable stimNum, wave classWave3D)

	// from the 3D class wave, clean up the stimNum layer so that it no longer contains the trailing NaN rows
	Duplicate/O/RMD=[0,dimsize(classWave3D,0)][0,dimsize(classWave3D,1)][stimNum] classWave3D stimLayer
	Duplicate/O stimLayer, stimLayerClean
	Redimension/N=(-1,0,0) stimLayer
	Wavetransform zapnans stimLayer
	DeletePoints/M=0 numpnts(stimLayer),dimsize(stimLayerClean,0)-1, stimLayerClean	
	return stimLayerClean
end
	

function/WAVE classifyPeaks3D(wave stimStarts, wave STimEnds, wave wpeaks_tb, wave wpeaks, wave wpeaks2,wave dtb, variable detectSTart, variable detectEnd[, string hostPanel])
	if(paramIsDefault(hostPanel))
		hostPanel = ""
	else
		if(strlen(hostPanel)>0)
			hostPanel += "#"
		endif
	endif

	// layers = stimulus trial
	// rows = detected peaks within the trial
	// cols: 
	//0 = wpeaks
	//1 = wpeaks2
	//2 = wpeaks_tb
	//3 = wpeaks_tb relative to the stimulus star titme
	//4 = the order of the peak within the detection window, zero based. -1 means the peak is outside the detection window
	
	// get the base wavename from stimStarts
	String wn = nameofwave(stimStarts)
	// String expr = "([[:alpha:]]+)_stimStarts"
	String expr = "(.*)_stimStarts" // AGG - want more than just alpha characters, everything before _stimStarts

	String basename
	SplitString/E=(expr) wn, basename
	basename = basename + "_classWave3D"
	Make/O/D/N=(numpnts(wpeaks_tb),5,numpnts(stimStarts)) $basename = NaN
	wave classWave3D = $basename
	
	Make/O/D/N=(0,5) $(basename+"_epeakstats")
	wave pstats = $(basename+"_epeakstats")
	
	Make/O/D/N=(0,5) $(basename+"_not_epeakstats")
	wave npstats = $(basename+"_not_epeakstats")
	
	Make/O/D/N=(numpnts(wpeaks_tb)) histopeaktimes
	
	// calc interstimulus interval
	variable intstim = stimStarts[1]-stimStarts[0] // AGG - what if this varies?
	
	variable i
	variable j
	variable row
	variable winCount
	variable peakcount = 0

	
	// iterate thru stimulus start times
	for(i=0;i<numpnts(stimStarts);i+=1)
		row=0
		winCount = 0
		
		// iterate thru peaks TODO: make more efficient by not-rechecking peaks that have already been assigned
		// AGG - should also be a way to say that if this peak time is after the stimStart, don't check all the next ones
		for(j=0;j<numpnts(wpeaks_tb);j+=1)
			
			// does the peak fall within the stimulus start (inclusive) and the start of the next stim (non-inclusive)?
			if(dtb[j]>=stimStarts[i] && dtb[j]<stimStarts[i] + intstim) // AGG - could you check if this is the last stim, if so, go to end of the trace, if not, use stimStarts[i+1]
				classWave3D[row][0][i] = wpeaks[j]
				classWave3D[row][1][i] = wpeaks2[j]
				classWave3D[row][2][i]= wpeaks_tb[j]
				classWave3D[row][3][i] = wpeaks_tb[j]-stimStarts[i]
				histopeaktimes[peakCount] = wpeaks_tb[j]-stimStarts[i]
				peakcount+=1
				// does the peak fall within the detection window?
				if(dtb[j]>=stimStarts[i]+detectStart && dtb[j]<=stimStarts[i]+detectEnd)
					classWave3D[row][4][i] = winCount
					winCount +=1
					if (winCount==1)
						InsertPoints dimsize(pstats,0),1, pstats
						pstats[dimsize(pstats,0)-1][] = classWave3D[row][q][i]
					endif
					
				else
					classWave3D[row][4][i] = -1 // peaks order is assigned -1 if peak is outside detection window
					InsertPoints dimsize(npstats,0),1, npstats
					npstats[dimsize(npstats,0)-1][] = classWave3D[row][q][i]
					
				endif
				row+=1
			endif			
		endfor
	endfor
	
	histopeaktimes/=0.001 // ms to s for graphing
	
	// make peristimulus histogram
	Make/N=100/O peristimHist
	
	string periStimGraphN = hostPanel + "peristimHistGraph"

	// Clear existing peristim histograms
	if(WinType(periStimGraphN) == 1)
		clearDisplay(periStimGraphN) // AGG - added "Graph" to end, as it adds a digit when the window name matches wave name. Harder to find -> many graphs
	else
		periStimGraphN = "peristimHistGraph"
		AGG_makeDisplay(65, 10, 95, 35, periStimGraphN)
	endif

	if(numpnts(histopeaktimes)>0)
		Histogram/B={0,1,100} histopeaktimes,peristimHist
	endif
	
	AppendToGraph/W=$periStimGraphN peristimHist
	ModifyGraph/W=$periStimGraphN mode=5,useBarStrokeRGB=1
	Label/W=$periStimGraphN bottom "peak time relative to stimulus onset (ms)"
	Label/W=$periStimGraphN left "# of events"
	
	Make/O/WAVE results = {classWave3D, pstats,npstats} 

	return results
	
end

		
function/wave detectStim(wave stimulusWave)
	String waven = nameofwave(stimulusWave)	
	
	if (stimulusWave[0] < 4.5 || stimulusWave[0] >5.5)
		print "wave", waven, "The LED did not start in off state. This may be an error"
	endif

	variable edgeThreshold = 4.3 

	String stimS = waven + "_stimStarts"
	String stimE= waven + "_stimEnds"
		
	Make/O/D $stimS // AGG 2022-10-31: Made this and stimEnds double precision waves
	wave stimStarts = $stimS
	
	Make/O/D $stimE
	wave stimEnds = $stimE
	
	FindLevels/Q/EDGE=2/D=stimStarts/M=0.005 stimulusWave edgeThreshold // assumes stimuli are at least 5 ms apart (200 Hz)
	FindLevels/Q/EDGE=1/D=stimEnds/M=0.005 stimulusWave edgeThreshold 
	
	if (numpnts(stimStarts)==0 || numpnts(stimEnds)==0 || numpnts(stimStarts)-numpnts(stimEnds) !=0)
		print("No edge crossings detected or unequal number of edge crossings)")
	endif
	
	
	String stimBars = waven + "_bars"
	
	Duplicate/O stimulusWave $stimBars
	wave bars = $stimbars
	bars = NaN

	
	Make/O/D/FREE/N=(numpnts(stimStarts)-1) ints // AGG 2022-10-31: Made ints, durs, amps double precision
	Make/O/D/FREE/N=(numpnts(stimStarts)) durs
	Make/O/D/FREE/N=(numpnts(stimStarts)) amps
	
	variable i
	variable samplingInterval = IndexToScale(stimulusWave,1,0)
	variable platStart
	variable platEnd
	
	for (i=0;i<numpnts(stimStarts);i+=1)
	
		platStart = x2pnt(stimulusWave,stimStarts[i]+samplingInterval)
		platEnd = x2pnt(stimulusWave,stimEnds[i]-samplingInterval)
		amps[i] = stimulusWave[platStart]
		bars[platStart,platEnd]=1
		// bars[platStart,platEnd]=truncateDP(stimulusWave[platStart], 1) // AGG - instead of 1, set to amplitude
		
		durs[i] = stimEnds[i]-stimStarts[i]
		
		if (durs[i]<0.0004)
			print("Stimulation at " + num2str(stimStarts[i]) + " s was less than 0.4 ms. This may indicate an error")
			durs[i] = NaN
			amps[i] = NaN
			bars[x2pnt(stimulusWave,stimStarts[i])+1,x2pnt(stimulusWave,stimEnds[i])-1]=0	
		endif
		
		if(i<numpnts(stimStarts)-1)
			ints[i] = stimStarts[i+1]-stimStarts[i]
		endif
		
	endfor
	
	// put stimulus data in the wave note of bars 
	
	WaveTransform zapNaNs amps
	WaveTransform zapNaNs durs
	WAvetransform zapNans ints

	// print "amps", amps, "durs", durs, "ints", ints
	
	String result = "FREQUENCY: " + num2str(1/mean(ints)) + "Hz;AMPLITUDE: " + num2str(mean(amps)) + "V;PULSE_DURATION: " + num2str(mean(durs))+ "s;NUM_STIMS:" + num2str(numpnts(stimStarts)) + ";"
	Note/K bars, result

	Make/O/N=3/WAVE stimTrainInfo = {stimStarts,stimEnds,bars}

	// display stimTrainInfo[2]
	return stimTrainInfo

end

function [wave/wave stimTrainInfo, variable frequency, variable amplitude, variable pulseDur, variable numStims, string pgfLabel] detectStimInfo(wave stimulusWave)
	String waven = nameofwave(stimulusWave)	
	
	if (stimulusWave[0] < 4.5 || stimulusWave[0] >5.5)
		print "wave", waven, "The LED did not start in off state. This may be an error"
	endif

	variable edgeThreshold = 4.3 

	String stimS = waven + "_stimStarts"
	String stimE= waven + "_stimEnds"
		
	Make/O/D $stimS // AGG: 2022-10-31: Made stimStarts, stimEnds, ints, durs, amps double precision waves
	wave stimStarts = $stimS
	
	Make/O/D $stimE
	wave stimEnds = $stimE
	
	FindLevels/Q/EDGE=2/D=stimStarts/M=0.005 stimulusWave edgeThreshold // assumes stimuli are at least 5 ms apart (200 Hz)
	FindLevels/Q/EDGE=1/D=stimEnds/M=0.005 stimulusWave edgeThreshold 
	
	variable stimIssues = 0
	if (numpnts(stimStarts)==0 || numpnts(stimEnds)==0 || numpnts(stimStarts)-numpnts(stimEnds) !=0)
		// print waveN, "stimStarts", stimStarts, "ends", stimEnds
		print "wave", waven, "No edge crossings detected or unequal number of edge crossings"
		stimIssues = 1
	endif

	String stimBars = waven + "_bars"
	
	Duplicate/O stimulusWave $stimBars
	wave bars = $stimbars
	bars = NaN

	
	if(numpnts(stimStarts)>1)
		Make/O/D/FREE/N=(numpnts(stimStarts)-1) ints
	else
		Make/O/D/FREE/N=(0) ints
	endif
	Make/O/D/FREE/N=(numpnts(stimStarts)) durs
	Make/O/D/FREE/N=(numpnts(stimStarts)) amps
	
	variable i
	variable samplingInterval = IndexToScale(stimulusWave,1,0)
	variable platStart
	variable platEnd
	
	if(!stimIssues)
		for (i=0;i<numpnts(stimStarts);i+=1)
		
			platStart = x2pnt(stimulusWave,stimStarts[i]+samplingInterval) // may want to consider 2 points before/after for these
			platEnd = x2pnt(stimulusWave,stimEnds[i]-samplingInterval)
			amps[i] = mean(stimulusWave, stimStarts[i] + samplingInterval, stimEnds[i] - samplingInterval) // AGG changed from just first point, as was getting decrease down for some of the blue waves
			// bars[platStart,platEnd]=1
			bars[platStart,platEnd]=truncateDP(stimulusWave[platStart], 1) // AGG - instead of 1, set to amplitude
			
			durs[i] = stimEnds[i]-stimStarts[i]
			
			if (durs[i]<0.0004)
				print("Stimulation at " + num2str(stimStarts[i]) + " s was less than 0.4 ms. This may indicate an error")
				durs[i] = NaN
				amps[i] = NaN
				bars[x2pnt(stimulusWave,stimStarts[i])+1,x2pnt(stimulusWave,stimEnds[i])-1]=0	
			endif
			
			if(i<numpnts(stimStarts)-1)
				ints[i] = stimStarts[i+1]-stimStarts[i]
			endif
			
		endfor
		
		// put stimulus data in the wave note of bars 
		
		WaveTransform zapNaNs amps
		WaveTransform zapNaNs durs
		WAvetransform zapNans ints
		// print "amps", amps, "durs", durs, "ints", ints
	endif
	
	numStims = 0

	if(!stimIssues)
		frequency = 1 / mean(ints)
		if(!frequency)
			frequency = NaN
		endif
		amplitude = mean(amps)
		if(!(amplitude == 0) && !amplitude)
			amplitude = NaN
		endif
		pulseDur = mean(durs)
		if(!pulseDur)
			pulseDur = NaN
		endif
		numStims = numpnts(stimStarts)
		if(!numStims)
			numStims = 0
		endif
	endif

	pgfLabel = getWaveLabel(stimulusWave)
	
	String result = "FREQUENCY: " + num2str(frequency) + "Hz;AMPLITUDE: " + num2str(truncateDP(amplitude, 1)) + "V;PULSE_DURATION: " + num2str(truncateDP(pulseDur, 4))+ "s;NUM_STIMS:" + num2str(numStims) + ";"
	Note/K bars, result

	Make/O/N=3/WAVE stimTrainInfo = {stimStarts,stimEnds,bars}

	// display stimTrainInfo[2]
	return [stimTrainInfo, frequency, amplitude, pulseDur, numStims, pgfLabel]

end

function [variable frequency, variable amplitude, variable pulseDur, variable numStims, string pgfLabel] getStimInfo(string cellName, variable seriesNum, variable stimTraceNum)
	string waveN = cellName + "g1s" + num2str(seriesNum) + "sw1t" + num2str(stimTraceNum)
	// print "waveN", waveN
	if(waveexists($waveN))
		[Wave/Wave stimTrainInfo, frequency, amplitude, pulseDur, numStims, pgfLabel] = detectStimInfo($waveN)
		// print "wave", waveN, "freq", frequency, "amp", amplitude, "dur", pulseDur, "numStims", numStims, "label", pgfLabel, "stimTraceNum", stimTraceNum
	else
		print "wave", waveN, "does not exist"
	endif
End

function analyzeOpto_forSeries(string cellName, variable seriesNum, variable stimTraceNum[, string hostPanel])
	
	
	string dataWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t1"
	string stimWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t" + num2str(stimTraceNum)

	wave iWave = $dataWaveN
	if(WaveExists(iWave))
		wave stimWave = $stimWaveN
		if(WaveExists(stimWave))
			wave pksWave = $(dataWaveN + "_pks")
			wave pk2Wave = $(dataWaveN + "_pk2")
			wave ptbWave = $(dataWaveN + "_ptb")
			wave dtbWave = $(dataWaveN + "_dtb")
			if(WaveExists(pksWave) & WaveExists(pk2Wave) & WaveExists(ptbWave) & WaveExists(dtbWave))
				make/Wave/N=4/O $(dataWaveN + "_peaksInfo")/Wave=peaksInfo
				peaksInfo[0] = ptbWave
				peaksInfo[1] = pksWave
				peaksInfo[2] = pk2Wave
				peaksInfo[3] = dtbWave
				analyzeOpto(iWave, stimWave, peaksInfo, hostPanel = hostPanel)
			else
				print "one of the detection waves does not exist for", dataWaveN
			endif
		else
			print "stimWave does not exist for", dataWaveN
		endif
	else 
		print "wave does not exist for", dataWaveN
	endif
end

