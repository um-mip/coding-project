
///////////
/// relateEventsToStims
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Get two waves, one with the index of the stimulation that preceeded each event
/// and other with the latency following the previous stimulation
///////////
function [wave/I prevStimIndex, wave/D latencyFromStim] relateEventsToStims(wave stimStarts, wave ptbWave)
// function relateEventsToStims(wave stimStarts, wave ptbWave)
    
    string ptbWaveN
    DFREF ptbWaveDFR
    [ptbWaveN, ptbWaveDFR] = getWaveNameAndDFR(ptbWave)

    string basename = getWaveBasename(ptbWaveN)

    variable numStims = numpnts(stimStarts), iStim = NaN, lastStim = numStims - 1 //could be -1 if no stims
    variable numEvents = numpnts(ptbWave), iEvent = 0

    make/I/O/N=(numEvents) ptbWaveDFR:$(basename+"_prevStimIdx")/Wave=prevStimIndex
    make/D/O/N=(numEvents) ptbWaveDFR:$(basename+"_latFromStim")/Wave=latencyFromStim
    
    prevStimIndex = NaN
    latencyFromStim = NaN

    if(numStims>0)
        variable eventTime, stimStart = 0, nextStimStart
        nextStimStart = stimStarts[0]
        
        for(iEvent=0; iEvent<numEvents; iEvent++)
            eventTime = ptbWave[iEvent]

            if(iStim != lastStim && eventTime >= nextStimStart)
                do
                    if(numtype(iStim) == 2) // NaN
                        iStim = 0
                    else
                        iStim ++
                    endif
                    
                    stimStart = stimStarts[iStim]

                    if(iStim < lastStim)
                        nextStimStart = stimStarts[iStim + 1]
                    endif
                while (iStim < lastStim && eventTime >= nextStimStart)
            endif
            
            prevStimIndex[iEvent] = iStim
            if(numType(iStim) == 0)
                latencyFromStim[iEvent] = eventTime - stimStart
            else
                latencyFromStim[iEvent] = NaN // if current stim is NaN, then latency is also NaN
            endif
        endfor

    endif

    // edit/K=1 stimStarts, ptbWave, prevStimIndex, latencyFromStim

    return [prevStimIndex, latencyFromStim]
end


///////////
/// getEvokedEventsIndex
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Returns the index wave for events that happen with a latency within specified duration of a stimulus
/// maxLatency is inclusive
///////////
function/Wave getEvokedEventsIndex(wave latencyFromStim, variable maxLatency)
    string latencyFromStimN
    DFREF latencyFromStimDFR
    [latencyFromStimN, latencyFromStimDFR] = getWaveNameAndDFR(latencyFromStim)

    string basename = getWaveBasename(latencyFromStimN)

    Extract/O/INDX latencyFromStim, latencyFromStimDFR:$(basename + "_evkIdx"), latencyFromStim[p] <= maxLatency
    Wave evokedIndex=latencyFromStimDFR:$(basename + "_evkIdx") 

    // print evokedIndex
    return evokedIndex 
end

///////////
/// getEvokedEventsStimIndices
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: Returns the subset of prevStimIndex that corresponds to the evoked events, as marked in evokedIndex
///////////
function/Wave getEvokedEventsStimIndices(wave prevStimIndex, wave evokedIndex)
    if(WaveExists(evokedIndex) & waveExists(prevStimIndex))
        string prevStimIndexN
        DFREF prevStimIndexDFR
        [prevStimIndexN, prevStimIndexDFR] = getWaveNameAndDFR(prevStimIndex)

        string basename = getWaveBasename(prevStimIndexN)

        string waven = basename + "_evkEventsStims"
        make/I/O/N=(numpnts(evokedIndex)) prevStimIndexDFR:$waven/Wave=subWave

        subWave = prevStimIndex[evokedIndex[p]]
        // print subWave
        return subWave
    endif
end

///////////
/// summarizeEvokedEvents
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-10-30
/// NOTES: 
///////////
// function [variable numEvokedEvents, variable numStimsWithEvoked] summarizeEvokedEvents(wave stimStartsOrig, wave ptbWaveOrig, variable maxLatency)
function [variable numEvokedEvents, variable numStimsWithEvoked] summarizeEvokedEvents(wave stimStarts, wave ptbWave, variable maxLatency)
    if(numpnts(stimStarts)>0)
        // duplicate/O/D stimStartsOrig, stimStarts
        // stimStarts = truncateDP(stimStarts, 5) // round to 5 digits
        // duplicate/O/D ptbWaveOrig, ptbWave
        // ptbWave = truncateDP(ptbWave, 5) // round to 5 digits
        wave/D prevStimIndex, latencyFromStim
        [prevStimIndex, latencyFromStim] = relateEventsToStims(stimStarts, ptbWave)

        wave evokedIndex = getEvokedEventsIndex(latencyFromStim, maxLatency)
        wave evokedEventsStimIndices = getEvokedEventsStimIndices(prevStimIndex, evokedIndex)

        Wave uniqueStims, evokedCountsForStim
        string basename = getWaveBasename(nameOfWave(ptbWave))
        string valsName = basename + "_stimsWEvkd"
        string countName = basename + "_stimsEvkdCount"
        [uniqueStims, evokedCountsForStim] = getUniqueValuesAndCounts(evokedEventsStimIndices, valsName = valsName, countName = countName)
        // print uniqueStims, evokedCountsForStim

        numEvokedEvents = sum(evokedCountsForStim)
        numStimsWithEvoked = numpnts(uniqueStims)
        // variable numStims = numpnts(stimStarts)

        // edit/K=1 stimStarts, ptbWave, prevStimIndex, latencyFromStim, uniqueStims, evokedCountsForStim
    endif

    return[numEvokedEvents, numStimsWithEvoked]
end



///////////
/// testFunc
/// AUTHOR: 
/// ORIGINAL DATE: 2022-10-30
/// NOTES: 
///////////
function testFunc()
    Wave stimStarts = $("20220712ag1s32sw1t4_stimStarts")
    Wave ptbWave = $("20220712ag1s32sw1t1_PTB")
    variable numEvokedEvents, numStimsWithEvoked

    [numEvokedEvents, numStimsWithEvoked] = summarizeEvokedEvents(stimStarts, ptbWave, 0.0035)
end

///////////
/// testPrecision
/// AUTHOR: 
/// ORIGINAL DATE: 2022-10-31
/// NOTES: 
///////////
function testPrecisionF()
    Wave rawWave = $("20220712ag1s32sw1t1")
    make/o/D/N=5 testPrecisionDouble
    testPrecisionDouble[0] = pnt2x(rawWave, 21059) // 1.0595
    testPrecisionDouble[1] = pnt2x(rawWave, 41049) // 2.0545
    testPrecisionDouble[2] = pnt2x(rawWave, 61050) // 3.0525
    testPrecisionDouble[3] = pnt2x(rawWave, 61224) // 3.0612
    testPrecisionDouble[4] = pnt2x(rawWave, 81049) // 4.0525
    
    make/o/N=5 testPrecision
    testPrecision[0] = pnt2x(rawWave, 21059) // 1.0595
    testPrecision[1] = pnt2x(rawWave, 41049) // 2.0545
    testPrecision[2] = pnt2x(rawWave, 61050) // 3.0525
    testPrecision[3] = pnt2x(rawWave, 61224) // 3.0612
    testPrecision[4] = pnt2x(rawWave, 81049) // 4.0525

    edit/K=1 testPrecisionDouble, testPrecision
    ModifyTable digits = 7
end
