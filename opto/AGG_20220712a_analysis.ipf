function getAllStimInfo_20220712a()
	string cellName = "20220712a"
	variable i = 0, numSeries = 121
	make/O/N=(numSeries) redStimFreq, blueStimFreq, redStimAmp, blueStimAmp, redPulseDur, bluePulseDur, redStims, blueStims, seriesNums
	make/O/N=(numSeries)/t pgfLabels
	variable frequency, amplitude, pulseDur, numStims
	string pgfLabel
	for(i=0; i<numSeries; i++)
		variable seriesNum = i + 1
		seriesNums[i] = seriesNum
		// get red stimualtions
		frequency = NaN
		amplitude = NaN
		pulseDur = NaN
		numStims = 0
		[frequency, amplitude, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, 4)
		redStimFreq[i] = frequency
		redStimAmp[i] = amplitude
		redPulseDur[i] = pulseDur
		redStims[i] = numStims

		pgfLabels[i] = pgfLabel



		// // get blue stimulations
		frequency = NaN
		amplitude = NaN
		pulseDur = NaN
		numStims = 0
		[frequency, amplitude, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, 3)
		blueStimFreq[i] = frequency
		blueStimAmp[i] = amplitude
		bluePulseDur[i] = pulseDur
		blueStims[i] = numStims
	endfor
	edit/K=1 seriesNums, pgfLabels, redStimFreq, redStimAmp, redPulseDur, redStims, blueStimFreq, blueStimAmp, bluePulseDur, blueStims
end

///////////
/// displayOpto
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2022-07-18
/// NOTES: 
///////////
function displayOpto(string cellName, variable seriesNum, variable stimTraceNum[, variable useBars])
	string dataWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t1"
	string stimWaveN = cellName + "g1s" + num2str(seriesNum) + "sw1t" + num2str(stimTraceNum)

    if(paramIsDefault(useBars))
        useBars = 1
    endif
    
    wave iWave = $dataWaveN
	if(WaveExists(iWave))
		wave stimWave = $stimWaveN
		if(WaveExists(stimWave))
			wave barWave = $(stimWaveN + "_bars")
            string displayN = "optoDisplay_" + dataWaveN
            KillWindow/Z $displayN
            variable frequency, amplitude, pulseDur, numStims
            string pgfLabel
            [frequency, amplitude, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, stimTraceNum)
            if(WaveExists(barWave))
                AGG_makeDisplay(10, 10, 40, 40, displayN)
                string axisname = "current"
                appendtograph/L=$axisname iwave
                ModifyGraph freePos($axisname)=0
                ModifyGraph lblPosMode($axisname)=1
                ModifyGraph axisEnab( $axisname )={0.2,1.0}

                axisname = "voltage"

                // With full stim trace
                if(!useBars)
                    appendtograph/R=$axisname stimWave
                    ModifyGraph rgb( $(stimWaveN) )=(0,0,0)
                    // ModifyGraph lsize($(stimWaveN))=5
                else

                    // With bars for stim
                    appendtograph/R=$axisname barWave
                    ModifyGraph rgb( $(stimWaveN + "_bars") )=(0,0,0)
                    ModifyGraph lsize($(stimWaveN + "_bars"))=10
                endif

                ModifyGraph axisEnab( $axisname )={0,0.2}
                ModifyGraph freePos( $axisname )=0
                ModifyGraph lblPosMode( $axisname )=1

                string infoString = note(barWave)
                TextBox/C/N=amp/F=0/A=LT/E=2 infoString
            endif
		else
			print "stimWave does not exist for", dataWaveN
		endif
	else 
		print "wave does not exist for", dataWaveN
	endif
end

function getAllStimInfo_20220907f()
	string cellName = "20220907f"
	variable i = 6, numSeries = 50
	DFREF currentDFR = GetDataFolderDFR()
	DFREF cellDFR = getEventsCellInfoDF(cellName)
	NewDataFolder cellDFR:optoInfo
	DFREF cellOptoDFR = cellDFR:optoInfo
	make/O/N=(numSeries) cellOptoDFR:redStimFreq, cellOptoDFR:blueStimFreq, cellOptoDFR:redStimAmp, cellOptoDFR:blueStimAmp, cellOptoDFR:redPulseDur, cellOptoDFR:bluePulseDur, cellOptoDFR:redStims, cellOptoDFR:blueStims, cellOptoDFR:seriesNums
	make/O/N=(numSeries)/t cellOptoDFR:pgfLabels
	Wave/SDFR=cellOptoDFR redStimFreq, blueStimFreq, redStimAmp, blueStimAmp, redPulseDur, bluePulseDur, redStims, blueStims, seriesNums
	Wave/SDFR=cellOptoDFR/t pgfLabels
	variable frequency, amplitude, pulseDur, numStims
	string pgfLabel
	for(i=0; i<numSeries; i++)
		variable seriesNum = i + 1
		seriesNums[i] = seriesNum
		// get red stimualtions
		frequency = NaN
		amplitude = NaN
		pulseDur = NaN
		numStims = 0
		// Swapped AD/DA cords - red is 3
		[frequency, amplitude, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, 3)
		redStimFreq[i] = frequency
		redStimAmp[i] = amplitude
		redPulseDur[i] = pulseDur
		redStims[i] = numStims

		if(strlen(pgfLabel)>0)
			pgfLabels[i] = pgfLabel
		endif



		// // get blue stimulations
		frequency = NaN
		amplitude = NaN
		pulseDur = NaN
		numStims = 0
		// Swapped AD/DA cords - red is 4
		[frequency, amplitude, pulseDur, numStims, pgfLabel] = getStimInfo(cellName, seriesNum, 4)
		blueStimFreq[i] = frequency
		blueStimAmp[i] = amplitude
		bluePulseDur[i] = pulseDur
		blueStims[i] = numStims
	endfor
	edit/K=1 seriesNums, pgfLabels, redStimFreq, redStimAmp, redPulseDur, redStims, blueStimFreq, blueStimAmp, bluePulseDur, blueStims
end