///////////
/// combinePXPsForAGG_Events
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-18
/// NOTES: 
///////////
function combinePXPsForAGG_Events([string folderOrSingleFile])
    DFREF eventsDFR = getEventsPanelDF()
    make/FREE/T/N=0 freeCellNames, freeGroupNames, freeGroupNames2, freeGroupNames3
    if(dataFolderRefStatus(eventsDFR)!=0)
        Wave/SDFR=eventsDFR/T cellName, groupName, groupName2, groupName3

        if(WaveExists(cellName))
            duplicate/FREE/T cellName, freeCellNames
        endif
        if(WaveExists(groupName))
            duplicate/FREE/T groupName, freeGroupNames
        endif
        if(WaveExists(groupName2))
            duplicate/FREE/T groupName2, freeGroupNames2
        endif
        if(WaveExists(groupName3))
            duplicate/FREE/T groupName3, freeGroupNames3
        endif
    endif

    if(paramIsDefault(folderOrSingleFile))
        folderOrSingleFile = "folder"
    endif

    strswitch (folderOrSingleFile)
        case "folder":
            NewPath /O pxpFolderPath
            // If don't use override, it will prompt if want to load again
            if(V_flag != 0)
                return 0
            endif

            String extension = ".pxp" // get Igor PXP files 
            String pxpFilesList = IndexedFile(pxpFolderPath, -1, extension)
            Variable numItems = ItemsInList(pxpFilesList)

            // Sort using combined alpha and numeric sort
            pxpFilesList = SortList(pxpFilesList, ";", 16)

            string fileName
            variable index = 0
            do
                fileName = StringFromList(index, pxpFilesList)
                if(strlen(fileName) == 0)
                    break
                endif

                // L=7 -> load waves, numeric, and string variables
                // R -> recursively load sub-data folders
                // P=pxpFolderPath, where fileName is stored
                // O=2 -> overwrite objects if there is a naming conflict

                LoadData /L=7/R/P=pxpFolderPath/O=2 fileName // load waves

                updateFreeCellsAndGroups(freeCellNames, freeGroupNames, freeGroupNames2, freeGroupNames3)

                index++
            while(1)
            
            break
        case "singleFile":
            LoadData /L=7/R/O=2
            updateFreeCellsAndGroups(freeCellNames, freeGroupNames, freeGroupNames2, freeGroupNames3)
            break
        default:
            
            break
    endswitch
    


    DFREF eventsDFR = getEventsPanelDF()
    if(dataFolderRefStatus(eventsDFR)!=0 && numpnts(freeCellNames) > 0)
        duplicate/O freeCellNames, eventsDFR:cellName
        duplicate/O freeGroupNames, eventsDFR:groupName
        duplicate/O freeGroupNames2, eventsDFR:groupName2
        duplicate/O freeGroupNames3, eventsDFR:groupName3
        clearUnnamedCellRows()
    endif
end

///////////
/// updateFreeCellsAndGroups
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-18
/// NOTES: 
///////////
function updateFreeCellsAndGroups(wave/T freeCellNames, wave/T freeGroupNames, wave/T freeGroupNames2, wave/T freeGroupNames3)
    DFREF eventsDFR = getEventsPanelDF()
    if(dataFolderRefStatus(eventsDFR)==0)
        return NaN
    endif
    Wave/SDFR=eventsDFR/T cellName, groupName, groupName2, groupName3

    if(!WaveExists(cellName))
        return NaN
    endif
    
    variable nCells = numpnts(cellName), iCell = 0
    for(iCell=0; iCell<nCells; iCell++)
        string thisCellName = cellName[iCell]
        if(isStringInWave(thisCellName, freeCellNames))
            continue
        endif

        freeCellNames[numpnts(freeCellNames)] = {thisCellName}

        redimension/N=(numpnts(freeCellNames)) freeGroupNames, freeGroupNames2, freeGroupNames3
        
        if(WaveExists(groupName))
            freeGroupNames[numpnts(freeGroupNames) - 1] = groupName[iCell]
        endif
        
        if(WaveExists(groupName2))
            freeGroupNames2[numpnts(freeGroupNames2) - 1] = groupName2[iCell]
        endif
        
        if(WaveExists(groupName3))
            freeGroupNames3[numpnts(freeGroupNames3) - 1] = groupName3[iCell]
        endif
    endfor
end



///////////
/// findSharedWaveVarNames
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-18
/// NOTES: When loading data, can't have matching var/wave names within the same folder
///////////
function findSharedWaveVarNames(DFREF searchDFR)
    string allWaves = WaveList("*", ";", "", searchDFR)
    variable nWaves = itemsInList(allWaves), iWave = 0

    for(iWave=0; iWave<nWaves; iWave++)
        string thisWaveN = StringFromList(iWave, allWaves)

        NVAR/Z/SDFR=searchDFR matchingNVar = $thisWaveN // /Z added TD 20240816
        if(NVAR_exists(matchingNVar))
            print GetDataFolder(1, searchDFR), thisWaveN
            // Rename matchingNVar, $(thisWaveN + "var") // this renames the wave, not the variable... 
        endif
    endfor

    Variable numChildDataFolders = CountObjectsDFR(searchDFR, 4)
    Variable i
    for(i=0; i<numChildDataFolders; i+=1)
        String childDFName = GetIndexedObjNameDFR(searchDFR, 4, i)
        DFREF childDFR = searchDFR:$childDFName
        findSharedWaveVarNames(childDFR)
    endfor
End

///////////
/// renameIntervalFolders
/// AUTHOR: Amanda Gibson
/// ORIGINAL DATE: 2024-06-18
/// NOTES: Interval folders were previously named "interval" but there were also "interval" variables
/// and this caused problems when trying to load data from other experiments
/// This function should be run once to update the structure of existing pxp files
///////////
function renameIntervalFolders(DFREF searchDFR)
    string childDFRlist = getChildDFRList(searchDFR)
    variable nDFRs = itemsInList(childDFRlist), iDFR = 0

    for(iDFR=0; iDFR<nDFRs; iDFR++)
        string thisDFRName = StringFromList(iDFR, childDFRlist)

        if(stringmatch(thisDFRName, "interval"))
            RenameDataFolder searchDFR:$thisDFRName, intervalDF
        endif
    endfor

    variable i
    for(i=0; i<nDFRs; i+=1)
        String childDFName = GetIndexedObjNameDFR(searchDFR, 4, i)
        DFREF childDFR = searchDFR:$childDFName
        renameIntervalFolders(childDFR)
    endfor
End