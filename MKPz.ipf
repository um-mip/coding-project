//////////////////////////////////////////////
////
////
////  MASTER KINETICS PANEL - now with guides and without sim
////
////
/////////////////////////////////////////////
//function/s MasterKinPanelz( [tabnumber, tabname, tabprefix, initialize, labels] )
//variable tabnumber
//string tabname, tabprefix // if not default, switches to tab mode; does not create panel (unless one doesn't exist), puts tabname in front of all names
//variable initialize // set to 1 to set up the panel and tabs for use,  do not use otherwise, slow!
//string labels
//
//String quote = "\"", labellist=""
//if(paramisdefault(labels))
//	// handle no labels?
//else
//	labellist = quote + labels + quote // appropriates label stringlist for popupmenu usage
//endif
//
//// don't recreate everything if just adding a tab!
//variable buildpanel=1, tn = 0
//if(!paramisdefault(tabnumber))
//	if(tabnumber>0)
//		buildpanel=0
//	endif
//	tn = tabnumber
//else
//	tn = 0
//endif
//
//string keyedcontrollist = ""
//string svkey = "svkey", svlist=""
//string graphkey = "graphkey", graphlist="" // this contains a list of graphs, comma separated
//string listboxkey = "listboxkey", listboxlist=""
//string buttonkey = "buttonkey", buttonlist=""
//string popupkey = "popupkey", popuplist=""
//string rangekey = "rangekey", rangelist="" // this is a list of ranges for the graphs in graphkey, to set the correct one, find the graphkey item number
//string staticelementskey = "StaticElementsKey", staticelementslist=""
//
//// user data keys: stores names of the output waves for each section
//string/g mkp_TC = "TC"
//string/g mkp_SSA = "SSA"
//string/g mkp_SSI = "SSI"
//string/g mkp_SI = "SI"
//string/g mkp_RI = "RI"
//string/g mkp_Perm = "Perm"
//string/g mkp_wnroot=""
//
//variable/g mkp_r1start = 0.130, mkp_r1dur = 0.02, mkp_r1end = 0.18
//variable/g mkp_r2start = 0.330, mkp_r2dur = 0.02, mkp_r2end = 0.38
//variable/g mkp_r3start = 0.0, mkp_r3dur = 0.01, mkp_r3end = 0.05
//variable/g mkp_r4start = 0.0, mkp_r4dur = 0.01, mkp_r4end = 0.05
//variable/g mkp_fitoff=0.003, mkp_fitdur=0.01,mkp_disp=0,mkp_ocvm_group=1
//variable/g mkp_g_rev=0
//
//variable/g mkp_MAXTARGETS = 6// maximum number of targets to fed via userdata
//
//// THESE ARE GLOBAL VARIABLES DEFINED HERE ONLY!
//string/g mkp_pann = "KineticsMaster"
//string/g mkp_sSIRIduration = "SIRIduration"
//
//string subwin 
////\\//\\/\\/\\/\/\/\/\/\/\/\/
//
//string prefix ="" // no prefix for items independent of current tab
//
//// path storage receptacle names  
//SVAR mkp_sRealDataPath =  mkp_sRealDataPath 
//
// // sv names
//string/g mkp_svR1 = prefix + "svR1"
//string/g mkp_svR2 = prefix + "svR2"
//string/g mkp_svR3 = prefix + "svR3"
//string/g mkp_svR4 = prefix + "svR4"
//// graph names
//string/g mkp_Realactg = prefix + "Real", mkp_Realactsubg = prefix + "subtracted", mkp_RealActProbg = prefix + "prob"
//string/g mkp_Realinactg = prefix + "RealInact", mkp_Realinactsubg = prefix + "subtractedInact", mkp_RealinactProbg = prefix + "prob"
//string /g mkp_permeabilityg = prefix + "perm"
//string/g mkp_RealSIg = prefix + "RealSI", mkp_RealSIsubg = prefix + "subtractedSI", mkp_RealSIprobg = prefix + "SIprob"
//string/g mkp_RealRIg = prefix + "RealRI", mkp_RealRIsubg = prefix + "subtractedRI", mkp_RealRIprobg = prefix + "RIprob"
//
////  BUTTON NAMES
//string/g mkp_buRealData = prefix + "mkp_realData"
//
//string mkp_buTables = prefix + "buTables"
//string/g mkp_bu2qub = prefix + "mkp_bu2qub"
//string mkp_buFromQuB = prefix + "buFromQuB"
//string mkp_buEmbiggen = prefix + "buEmbiggen"
//string mkp_buLagAnalysis = prefix + "buLagAnalysis"
//
//// permanent popups 
//string/g mkp_puRealExpList = prefix + "PUrealExplist"
//string/g mkp_puSimExpList = prefix + "puSimExpList"
//
//string mkp_puLabelAct = prefix + "puLabelAct"
//string mkp_puLabelActSub = prefix + "puLabelActSub"
//string mkp_puLabelinact = prefix + "puLabelInact"
//
//string mkp_puLabelSI =  prefix +  "puLabelSI"
//string mkp_puLabelRI = prefix + "puLabelRI"
//
//string/g mkp_puRIsubSweep = prefix + "puRIsubSweep"
//string/g mkp_puSIsubSweep = prefix + "puInactSubSweep"
//
////\\//\\/\\/\\/\/\/\/\/\/\/\/
//
//if(paramisdefault(tabprefix)) // set the prefix for tab unique items
// 	prefix = ""
// else
// 	prefix = tabprefix
// endif
//
//// real popup names
//string/g mkp_puRealACTn = prefix + "PUactivation"
//string/g mkp_puRealINACTn = prefix + "PUinactivation"
//string/g mkp_puRealACTsubn = prefix + "PUactivationSub"
//
//string mkp_puINACTsubn = prefix + "PUinactivationSub"
//
//string/g mkp_puRealSIn = prefix + "puSI"
//string/g mkp_puRealSIsubn = prefix + "puSISub"
//string/g mkp_puRealRIn = prefix + "puRI"
//string/g mkp_puRealRIsubn = prefix + "puRISub"
//
//	variable grey = 50000
/////\/\/\/\/\\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\
//
//	PauseUpdate; Silent 1		// building window...
//
//// graph layout params
//
//	variable panelDX = 1000, panelDY = 800, panelX = 50, panelY = 50
//	variable ncol = 3, nrow= 4
//
//	variable xcolsep = 10, yrowsep =15, tabsep = 35
//	
//	variable graphXwidth = panelDX/ncol - 2*xcolsep, graphYheight = panelDY/nrow - 4*yrowsep, graphXspace =10, grpahYspace = 10
//
//	if(buildpanel==1)
//
//	// GUIDES!
//	    string wName = mkp_pann 
//	    STRUCT guideSetup sg 
//	    sg.wName = wName
//	    sg.nrows = 4
//	    sg.ncols = 3 
//	    sg.ctrlH = 20
//	    sg.graphH = 100 
//	    sg.graphW = 100 
//	    sg.topCornerX = 15
//	    sg.topCornerY = 15 
//	    sg.initWidth = panelDX
//	    sg.initHeight = panelDY
//
//	    variable left = sg.topCornerX
//	    variable top = sg.topCornerY
//	    variable right = left + sg.initWidth
//	    variable bottom = top + sg.initHeight
//
//	    //newPanel/K=1/N=$wName /W=(left,top,right,bottom)
//	//    NewPanel /K=1/W=( panelX, panelY, panelX+panelDX, panelY + panelDY) /N=$mkp_pann
//
//
//	/// HEAVY LIFTING HERE \/\/\/\/
//	    string PanelNames 
//	    PanelNames = MKPzGuides(sg)
//	   	subwin = mkp_pann + "#" + "TabPan"
//  	///
//
//
//	else
//		doWindow/F $mkp_pann // bring mkp panel to front!!
//	endif
//
//   	subwin = mkp_pann + "#" + "TabPan"
//	tabcontrol foo, win=$subwin, tabLabel(tn)=tabname
//
//	variable svX_Size = 70, svY_size = 15, svpos = 4 // setvar properties
//	variable lbW=130, lbH = 100 // list box properties
//	variable butW = 85, butH = 12 // button properties
//
///// set up rows and columns
//	variable xcol1 = xcolsep, xcol2 = xcol1 + 2*(xcolsep+graphXwidth), xcol3= xcol2 + xcolsep + graphxwidth
//	variable dxcol=145, yrow1 = tabsep, dyrow = 15 
//
//	variable posX =xcol1, posY = yrow1 
//
//// Real Data : data folder button
//
//// // once selected, sets exp list string
//	string exp_lists = ""
//
//// Real Data : exp dropdown
//// // once sleelcted sets expcode, e.g. 20160525a.dat
//	string expcode = ""
//
//// // once selected, fills label list string
//	string label_lists = ""
//	
//// Real Data : ACT params
//	posx = xcol1
//	posy = yrow1 - buth
//	// data folder sel button
//	string udata = "target1:" + mkp_puRealExpList + ";" 
//	udata += "path:" + mkp_sRealDataPath + ";"
//	
//	// exp list popup
//	string puLabelAct = mkp_puLabelAct
//	string puLabelInact = mkp_pulabelinact
//	string pulabelActSub = mkp_pulabelactsub
//	string pulabelsi = mkp_pulabelsi
//	string puLabelRI = mkp_puLabelRI
//	
//	//posx += dxcol
//
//	string puRealExpList = mkp_puRealExpList
//	subwin = mkp_pann + "#" + "expLBpan"
//	if(buildpanel==1)
//		udata = "target1:" + pulabelAct +";" + "target2:" + pulabelInact +";"  + "target3:" + pulabelActSub +";" // use userdata to point to proper label popup
//		udata += "target4:" + pulabelSI + ";" + "target5:" + pulabelRI + ";"
//		udata += "path:" + mkp_sRealDataPath + ";" 
//		udata +=  "pulist:" + tabname + "," + ";" // tabname happens to be the name of the .dat file, sorry this is confusing
//	
//		PopupMenu $puRealExpList win=$subwin, pos={0,0}, title="exp",proc=mkp_puExpList,mode=2  // this is the target for the selection
//		PopupMenu $puRealExpList win=$subwin, userdata=udata, value=#tabname // getlabels(1, first = "NONE" )
//	//	popupList += puRealExpList + ","
//	else
//		// accumulate items in popup
//		controlinfo $puRealExpList
//		string pudata = S_UserData
//		string pulist = stringbykey( "pulist", pudata )
//		pulist += tabname + "," 									// tabname holds the name of the .dat file !!!
//		pudata = replacestringbykey( "pulist", pudata, pulist )
//		PopupMenu $puRealExpList win=$subwin, userdata = pudata
//		pulist = quote + replacestring(",", pulist, ";" ) + quote
//		PopupMenu $puRealexplist win=$subwin, value = #pulist
//	endif
//	staticelementslist += puRealExplist + ","
//
//// ACT params		:: POPUP ROW 1 ACTIVATION
//	posx = xcol3 - svpos*svX_Size
//	posy += dyrow
//	
//	string svR1start = mkp_svR1 + "start"
//	string svR1dur = mkp_svR1 + "dur" 
//	string svR1end = mkp_svR1 + "end"
//	variable svdxFlex = 0.7
//	if(buildpanel==1)
//		SetVariable $svR1start,pos={posx,posy},size={svX_Size, svY_size},title="R1start",value=mkp_r1start, limits={0,inf,0.001}
//		//svlist += svR1start + "," 
//		posx += svdxFlex*dxcol
//		SetVariable $svR1dur,pos={posx,posy},size={svX_Size, svY_size},title="R1dur", value=mkp_r1dur, limits={0,inf,0.001}
//		//svlist += svR1dur + "," 
//		posx += svdxFlex*dxcol
//		SetVariable $svR1end,pos={posx,posy},size={svX_Size, svY_size},title="R1end", value=mkp_r1end, limits={0,inf,0.001}
//		//svlist += svR1end + ","
//	endif	
//		
//	// Real DATA : ACT labels
//		posx = xcol1
//		udata = "exp:" + mkp_puRealExpList + ";"  // use userdata to indicate host data file and target graph
//		udata += "target1:" + mkp_puRealActn + ";" // name of target for series list
//		udata += "path:" + mkp_sRealDataPath + ";"
//	if(buildpanel == 1 )
//		PopupMenu $puLabelAct pos={posx,posy}, title="Act",proc=mkp_puLABELPROC,mode=2, userdata=udata  // this is the target for the selection
//		PopupMenu $puLabelAct value=#labellist //"NONE" 
//	//	staticelementslist += pulabelact + ","
//	endif
//	popupmenu $pulabelAct userdata($tabname)=udata // there are tab specific data here
//
//// // once selected, sets actlabel string
//	string actlabel = "NONE"
//
//// Real DATA : ACT Real series
//	posx += dxcol
//	udata = "exp:" + mkp_puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:"+mkp_pann+"#"+ mkp_Realactg + ";"// this is the target windwow
//	udata += "range:" + mkp_svR1 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $mkp_puRealActn, title="Series", mode=2, pos={posX, posY}, size={lbW, lbH}, proc=mkp_plotproc
//	PopupMenu $mkp_puRealActn, userdata=udata, value = "NONE" 
//	popuplist += mkp_puRealActn + ","
//
//// Real Data : ACT sub label
//	pulabelactsub = mkp_pulabelactsub
//	posx = xcol1 + graphXwidth + xcolsep 
//	
//	udata = "exp:" + mkp_puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:" + mkp_puRealActSubn + ";" // name of target for series list
//	udata += "path:" + mkp_sRealDataPath + ";"
//if(buildpanel==1)
//	PopupMenu $puLabelActSub pos={posx,posy}, title="Inact",proc=mkp_puLABELPROC,mode=2, userdata=udata // this is the target for the selection
//	PopupMenu $puLabelActSub value=#labellist // "NONE" 
////	popuplist += pulabelactsub + ","
//endif
//popupmenu $puLabelActSub userdata($tabname)=udata
//
//// Real Data : ACT sub series
//	posx += dxcol
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "raw:" + mkp_puRealActn + ";" // source for name of raw act series
//	udata += "target1:"+mkp_pann+"#"+ mkp_Realactg + ";"// target for raw sub data (super impose on raw data 
//	udata += "target2:" + mkp_pann + "#" + mkp_RealActSubg + ";" // this is the target window for the subtracted trace
//	udata += "target3:" + mkp_pann + "#" + mkp_RealActProbg + ";" // target for conductance/probability plot
//	udata += "target4:" + mkp_pann + "#" + mkp_permeabilityg + ";"
//	udata += "range:" + mkp_svR1 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $mkp_puRealActSubn, title= " Series", mode=2,pos={posX, posY},size={lbW, lbH}, proc=mkp_puActSubproc
//	PopupMenu $mkp_puRealActSubn, userdata=udata, value="NONE" 
//	popuplist += mkp_puRealActSubn + ","
//
//// // // // :: POPUP ROW 2 ACTIVATION ::	
//// Real Data : Inact params	
//
//	string svR2start = mkp_svR2 + "start"
//	string svR2dur = mkp_svR2 + "dur" 
//	string svR2end = mkp_svR2 + "end"
//
//	posx = xcol3 - svpos*svX_Size
//	posy +=  graphYheight + 3*yrowsep + 20
//
//if(buildpanel==1)
//	SetVariable $svR2start,pos={posx,posy},size={svX_Size, svY_size},title="R2start",value=mkp_r2start, limits={0,inf,0.001}
//	//svlist += svR2start + "," 
//
//	posx += svdxflex * dxcol
//	SetVariable $svR2dur,pos={posx,posy},size={svX_Size, svY_size},title="R2dur", value=mkp_r2dur, limits={0,inf,0.001}
//	//svlist += svR2dur + "," 
//
//	posx += svdxflex * dxcol
//	SetVariable $svR2end,pos={posx,posy},size={svX_Size, svY_size},title="R2end", value=mkp_r2end, limits={0,inf,0.001}
//	//svlist += svR2end + ","
//endif
//
//// Real Data : Inact labels
//	pulabelinact = mkp_pulabelinact
//	posx = xcol1
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:" + mkp_puRealInActn + ";" // name of target for series list
//	udata += "path:" + mkp_sRealDataPath + ";"
//
//if(buildpanel==1)
//	PopupMenu $puLabelInAct pos={posx,posy}, title="SSinact",proc=mkp_puLABELPROC,mode=2, userdata=udata  // this is the target for the selection
//	PopupMenu $puLabelInAct value=#labellist //"NONE" // getlabels(1, first = "NONE" )
////	popuplist += pulabelinact + ","
//endif
//popupmenu $puLabelInAct userdata($tabname)=udata
//		
//// Real Data : Inact series
//	posx += dxcol
//
//	string puInactSubSweep = mkp_puINACTsubn // mkp_puSISubsweep
//	
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:"+mkp_pann+"#"+ mkp_Realinactg + ";"// this is the target windwow
//	udata += "target2:" + puInactSubSweep + ";"
//	udata += "range:" + mkp_svR2 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $mkp_puRealInActn, title= " Series", mode=2,pos={posX, posY},size={lbW, lbH}, proc=mkp_plotproc // this should populate the popup with sweep numbers
//	PopupMenu $mkp_puRealInActn, userdata=udata, value = "NONE" 
//	popuplist += mkp_puRealInactn + ","
//	
//// Real Data : Inact sub sweep
//	
//	posx = xcol1 + graphXwidth + xcolsep 
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "raw:" + mkp_puRealInActn + ";" // source for name of raw act series
//	udata += "target1:"+mkp_pann+"#"+ mkp_RealInactg + ";"// target for raw sub data (super impose on raw data 
//	udata += "target2:" + mkp_pann + "#" + mkp_RealInActSubg + ";" // this is the target window for the subtracted trace
//	udata += "target3:" + mkp_pann + "#" + mkp_RealInActProbg + ";" // target for conductance/probability plot
//	udata += "range:" + mkp_svR2 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $puInActSubSweep pos={posx,posy}, title="sub sweep",proc=mkp_puInactSubPROC,mode=2, userdata=udata // this is the target for the selection
//	PopupMenu $puInActSubSweep value= "NONE" 
//	popuplist += puInactsubsweep + ","
//	
//// // // // :: POPUP ROW 3 steady state inactivation ::
//// SI // Real Data : SI PARAMS
//	posx = xcol3 - svpos*svX_Size
//	posy +=  graphYheight + 3* yrowsep 
//
//	string svR3start = mkp_svR3 + "start"
//	string svR3dur = mkp_svR3 + "dur" 
//	string svR3end = mkp_svR3 + "end"
//
//	if(buildpanel==1)
//		SetVariable $svR3start,pos={posx,posy},size={svX_Size, svY_size},title="R3start",value=mkp_R3start, limits={0,inf,0.001}
//		//svlist += svR3start + "," 
//		posx += svdxflex * dxcol
//		SetVariable $svR3dur,pos={posx,posy},size={svX_Size, svY_size},title="R3dur", value=mkp_R3dur, limits={0,inf,0.001}
//		//svlist += svR3dur + "," 
//		posx += svdxflex * dxcol
//		SetVariable $svR3end,pos={posx,posy},size={svX_Size, svY_size},title="R3end", value=mkp_R3end, limits={0,inf,0.001}
//		//svlist += svR3end + ","
//	endif
//	
//	// SI // Real Data : SI labels
//		puLabelSI = mkp_puLabelSI
//		posx = xcol1
//		udata = "exp:" + puRealExpList + ";"  		// use userdata to indicate host data file and target graph
//		udata += "target1:" + mkp_puRealSIn + ";" 	// name of target for series list
//		udata += "path:" + mkp_sRealDataPath + ";"	// store the name of the path
//	if(buildpanel==1)
//		PopupMenu $puLabelSI pos={posx,posy}, title="Inact",proc=mkp_puLABELPROC,mode=2, userdata=udata  // this is the target for the selection
//		PopupMenu $puLabelSI value=#labellist //"NONE" 
//	//	popuplist += puLabelSI + ","
//	endif
//	popupmenu $puLabelSI userdata($tabname)=udata
//	
//// Real Data : SI series
//	string puSISubSweep = mkp_puSISubSweep
//
//	posx += dxcol
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:"+mkp_pann+"#"+ mkp_RealSIg + ";"// this is the target windwow
//	udata += "target2:" + puSISubSweep + ";"
//	udata += "range:" + mkp_svR3 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $mkp_puRealSIn, title= " Series", mode=2,pos={posX, posY},size={lbW, lbH}, proc=mkp_plotproc // this should populate the popup with sweep numbers
//	PopupMenu $mkp_puRealSIn, userdata=udata, value = "NONE" 
//	popuplist += mkp_purealsin + ","
//	
//// Real Data : SI sub sweep
//	posx = xcol1 + graphXwidth + xcolsep 
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "raw:" + mkp_puRealSIn + ";" // source for name of raw act series
//	udata += "target1:"+mkp_pann+"#"+ mkp_RealSIg + ";"// target for raw sub data (super impose on raw data 
//	udata += "target2:" + mkp_pann + "#" + mkp_RealSIsubg + ";" // this is the target window for the subtracted trace
//	udata += "target3:" + mkp_pann + "#" + mkp_RealSIprobg + ";" // target for conductance/probability plot
//	udata += "range:" + mkp_svR3 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	udata += "source:" + mkp_puRealSIn + ";" // this is where to get the filename and series number ! 20161201
//	PopupMenu $puSIsubSweep pos={posx,posy}, title="sub sweep",proc=mkp_puSIRISubPROC,mode=2, userdata($tabname)=udata // this is the target for the selection
//	PopupMenu $puSIsubSweep value= "NONE"
//	//popuplist += puSIsubSweep + ","
//
//// // // // :: POPUP ROW 4 recovery ::
//// RI // Real Data : RI PARAMS
//	posx = xcol3 - svpos*svX_Size
//	posy +=  graphYheight + 3* yrowsep 
//	
//	string svR4start = mkp_svR4 + "start"
//	string svR4dur = mkp_svR4 + "dur" 
//	string svR4end = mkp_svR4 + "end"
//
//	if(buildpanel==1)
//		SetVariable $svR4start,pos={posx,posy},size={svX_Size, svY_size},title="R4start",value=mkp_R4start, limits={0,inf,0.001}
//		//svlist += svR4start + "," 
//		posx += svdxflex * dxcol
//		SetVariable $svR4dur,pos={posx,posy},size={svX_Size, svY_size},title="R4dur", value=mkp_R4dur, limits={0,inf,0.001}
//		//svlist += svR4dur + "," 
//		posx += svdxflex * dxcol
//		SetVariable $svR4end,pos={posx,posy},size={svX_Size, svY_size},title="R4end", value=mkp_R4end, limits={0,inf,0.001}
//		//svlist += svR4end + ","
//	endif
//	
//	// RI // Real Data : RI labels
//		pulabelRI = mkp_puLabelRI
//		
//		posx = xcol1
//		udata = "exp:" + puRealExpList + ";"  		// use userdata to indicate host data file and target graph
//		udata += "target1:" + mkp_puRealRIn + ";" 	// name of target for series list
//		udata += "path:" + mkp_sRealDataPath + ";"	// store the name of the path
//	
//	if(buildpanel==1)
//		PopupMenu $puLabelRI pos={posx,posy}, title="Recovery",proc=mkp_puLABELPROC,mode=2, userdata=udata  // this is the target for the selection
//		PopupMenu $puLabelRI value=#labellist //"NONE" 
//	//	popuplist += pulabelri + ","
//	endif
//	popupmenu $puLabelRI userdata($tabname)=udata
//	
//// Real Data : RI series
//	string puRISubSweep = mkp_puRIsubSweep
//	
//	posx += dxcol
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "target1:"+mkp_pann+"#"+ mkp_RealRIg + ";"// this is the target windwow
//	udata += "target2:" + puRISubSweep + ";"
//	udata += "range:" + mkp_svR4 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	PopupMenu $mkp_puRealRIn, title= " Series", mode=2,pos={posX, posY},size={lbW, lbH}, proc=mkp_plotproc // this should populate the popup with sweep numbers
//	PopupMenu $mkp_puRealRIn, userdata=udata, value = "NONE" 
//	popuplist += mkp_puRealRIn + ","
//	
//// Real Data : RI sub sweep
//	posx = xcol1 + graphXwidth + xcolsep 
//	udata = "exp:" + puRealExpList + ";"  // use userdata to indicate host data file and target graph
//	udata += "raw:" + mkp_puRealRIn + ";" // source for name of raw act series
//	udata += "target1:"+mkp_pann+"#"+ mkp_RealRIg + ";"// target for raw sub data (super impose on raw data 
//	udata += "target2:" + mkp_pann + "#" + mkp_RealRIsubg + ";" // this is the target window for the subtracted trace
//	udata += "target3:" + mkp_pann + "#" + mkp_RealRIprobg + ";" // target for conductance/probability plot
//	udata += "range:" + mkp_svR4 + ";"
//	udata += "path:" + mkp_sRealDataPath + ";"
//	udata += "source:" + mkp_puRealRIn + ";" // this is where to get the filename and series number ! 20161201
//	PopupMenu $puRIsubSweep pos={posx,posy}, title="sub sweep",proc=mkp_puSIRISubPROC,mode=2, userdata($tabname)=udata // this is the target for the selection
//	PopupMenu $puRIsubSweep value= "NONE"
//	//popuplist += puRIsubSweep + ","
//	
//	if (BuildPanel == 1 ) // these controls are unique, independent of tab
//	
//	// button row at the bottom
//		posX = xcol1
//		posY = panelDY - dyrow
//		string buTables = mkp_buTables
//		Button $buTables,pos={posX, posY},size={butW,butH},title="datatables", proc=buMakeTablesProc
//		//buttonlist += buTables + ","
//	
//		posX += dxcol
//		Button $mkp_bu2qub, pos={posX, posY},size={butW,butH},title="2 QuB", proc=bu2QuBProc
//		//buttonlist += mkp_bu2qub + ","
//		
//		posX += dxcol
//		string buFromQuB = mkp_buFromQuB
//		Button $buFromQuB, pos={posX, posY},size={butW,butH},title="from QuB", proc=buFromQuBProc	
//		//buttonlist += buFromQuB + ","
//	
//		posX += dxcol
//		string buEmbiggen = mkp_buEmbiggen
//		Button $buEmbiggen, pos={posX, posY},size={butW,butH},title="Embiggen", proc=buEmbiggenProc	
//		//buttonlist += buEmbiggen + ","
//	
//		posX += dxcol
//		string buLagAnalysis = mkp_buLagAnalysis
//		Button $buLagAnalysis, pos={posX, posY},size={butW,butH},title="Lag Analysis", proc=buLagAnalysisProc	
//		//buttonlist += buLagAnalysis + ","
//		
//	//MAKE THE DISPLAYS
//		variable gX = xcol1 + 10, gY = yrow1 + butH, gXwidth=graphXwidth, gyH=graphYheight, xspacing =xcolsep, yspacing = 0.9*yrowsep
//	
//	// activation row: real act, act sub, prob
//		//act
//		gX = xcol1
//		gY = yrow1 + 3*yrowsep
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealActg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealActg + ","	
//		rangelist += mkp_svR1 + ","
//		SetActiveSubwindow ##
//
//		//act sub
//		gX = xcol1 + gXwidth + xspacing
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealActSubg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealActSubg + ","
//		rangelist += mkp_svR1 + ","
//		SetActiveSubwindow ##
//
//		//prob
//		gX = xcol2
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealActProbg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealActProbg + ","
//		rangelist += "" + ","
//		SetActiveSubwindow ##
//
//	// inactivation row
//		//inact raw
//		gX = xcol1
//		gy += gyH + 3*yrowsep
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealInactg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealInActg + ","
//		rangelist += mkp_svR2 + ","
//		SetActiveSubwindow ##
//
//		// inact sub
//		gX = xcol1 + gXwidth + xspacing
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealInactSubg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealInActSubg + ","
//		rangelist += mkp_svR2 + ","
//		SetActiveSubwindow ##
//
//		// PERMEABILITY prob
//		gX = xcol2
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_Permeabilityg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_permeabilityg + ","
//		rangelist += "" + ","
//		SetActiveSubwindow ##
//
//	//SI row
//		// Real SI raw
//		gX = xcol1
//		gy += gyH + 3*yrowsep
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealSIg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealSIg + ","
//		rangelist += mkp_svR3 + ","
//		SetActiveSubwindow ##
//	
//		// Real SI sub
//		gX = xcol1 + gXwidth + xspacing
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealSISubg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealSISubg + ","
//		rangelist += mkp_svR3 + ","
//		SetActiveSubwindow ##
//	
//		// SI prob
//		gX = xcol2
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealSIProbg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealSIprobg + ","
//		rangelist += mkp_svR3 + ","
//		SetActiveSubwindow ##
//	
//	//RI row
//		// Real RI raw
//		gX = xcol1
//		gy += gyH + 3*yrowsep
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealRIg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealRIg + ","
//		rangelist += mkp_svR4 + ","
//		SetActiveSubwindow ##
//	
//		// Real RI sub
//		gX = xcol1 + gXwidth + xspacing
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealRISubg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" +  mkp_RealRIsubg + ","
//		rangelist += mkp_svR4 + ","
//		SetActiveSubwindow ##
//	
//		// RI prob
//		gX = xcol2
//		Display/W=(gX,gY,gX+gXwidth,gy+gYH)/N=$mkp_RealRIProbg/HOST=# 
//		Modifygraph wbRGB=(grey, grey, grey), gbRGB=(grey, grey, grey)
//		graphlist += mkp_pann + "#" + mkp_RealRIprobg + ","
//		rangelist += mkp_svR4 + ","
//		SetActiveSubwindow ##
//	
//	endif //only build the graphs if this is the first panel
//	
//
////assemble the tab control keyed strings
//	keyedcontrollist = ""
//	keyedcontrollist += svkey + ":" + svlist +";"
//	keyedcontrollist += graphkey + ":" + graphlist + ";"
//	keyedcontrollist += buttonkey + ":" + buttonlist + ";"
//	keyedcontrollist += popupkey + ":" + popuplist + ";"
//	keyedcontrollist += rangekey + ":" + rangelist + ";"
//	keyedcontrollist += staticelementskey + ":" + staticelementslist + ";"
//	
//	tabcontrol foo,  userdata($tabname) = keyedcontrollist
//
//	string tabUdata = ""
//	string tlist = "", firstTab =""
//	if( buildpanel == 1 ) // that means it's the first time
//		tlist = tabname + ","
//		firstTab = tabName
//		tabUdata = "currentTab:" + firstTab + ";" + "tablist:" + tabname + "," + ";"
//	else
//		controlinfo foo
//		tabUdata = S_UserData
//		tlist = stringbykey( "tablist", tabUdata )
//		tlist += tabname + ","
//		firstTab = stringfromlist( 0, tlist, "," ) 			
//		tabUdata = replaceStringByKey( "tablist", tabUdata, tlist )
//	endif
//
//	tabcontrol foo, value = 0, userdata = tabUdata // sets the current tab to first and stores udata
//	
//	if( !paramisdefault( initialize ) )
//		if( initialize == 1 )
//			// now fake a call to the tabcontrol procedure to initialize the tabs 
//			STRUCT WMTabControlAction s // put the minimum necessary info into the structure to fake the call
//			s.ctrlname = "foo"
//			s.win = mkp_pann
//			s.eventcode = 2
//			s.tab = 0 // sets up first tab as active
//			s.userdata = tabUdata
//			mkpFoo(s)
//		endif
//	endif
//end