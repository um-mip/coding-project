//this code includes all of the possibile ways to fit the tau 
//edits on July 1, 2022 JJ


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        //GET WAVES FROM THE TOP GRAPH//
//this function pulls the wave that you want to fit Tau to from the top graph, allows you to pull multiple waves
function/Wave gettheWavesFromtheTopGraph([string graphName, variable graphTypes])
    
    if(paramIsDefault(graphName))
        graphName = "" // default to top graph
    endif

    if(paramIsDefault(graphTypes))
        graphTypes = 1 // default to normal waves
    endif
   
    string traces = tracenamelist(graphName, ";", graphTypes)
    variable iTrace = 0, numTraces = itemsInList(traces), iWaves = 0
    make/Wave/N=0/O wavesFromGraph // this is a wave of wave references, each wave is now given a reference

    for(iTrace=0; iTrace<numTraces; iTrace++)
        string trace = StringFromList(iTrace, traces)
        Wave traceWave = traceNameToWaveRef(graphName, trace)
        if(WaveExists(traceWave))
            redimension/N=(iWaves+1) wavesFromGraph
            wavesFromGraph[iWaves] = traceWave
            iwaves++
        endif
    endfor //this for loop allows you to pull multiple waves if they exsit and names them so that they dont get overwritten

    return wavesFromGraph
end
//by giving each of the waves a wave reference, you can run the tau fit code on each of the waves without overwriting the previous wave//




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                //DISPLAY TOP GRAPH WAVES//
//This function displays the wave that you pulled from the top graph and lets you confirm that its the correct wave
function DisplayTopGraph()
    Wave/Wave test = gettheWavesFromtheTopGraph()
    edit test // will show gibberish, but you can use the waves within it
    Wave fromGraph = test[1] // get the first wave
    display/k=1 fromGraph
end




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                    //MASTER FUNCTION//
//This is the main function, calling into place all of the other functions
//allows you to run the entire code on the waves you wish to analyze (from the top graph)
function AddTauToGraph ()
    Wave/Wave test = gettheWavesFromtheTopGraph() //pull all of the waves that exist on the top graph first
    Variable numofwaves = numpnts (test) //IGOR will count the total number of waves that exist
    print "number of waves:", numofwaves
    
    
    variable iwave = 0
    for (iWave=0; iWave<numofwaves; iwave++)
        wave thiswave = test[iWave]
        if (waveExists(thiswave)) ///for loop that will loop and run each tau fit function on each wave if more than one exsists///
        
            print "name of wave:", nameofwave (thisWave)

                            ////the different tau fit parameters that the wave will be run in////
//        TauFit_NormSmooth_AllConstraintsHeld_8020 (thiswave) //constraints held, 8020
        //TauFit_NormSmooth_AllConstraintsHeld_80toEnd (thisWave) //constraints held, 80 to end

//        TauFit_NormSmooth_AllConstraintsHeld_9010 (thiswave) //constraints held, 9010
        //TauFit_NormSmooth_AllConstraintsHeld_90toEnd (thisWave) //constraints held, 90 to end


//        TauFit_NormSmooth_Y0_allowed_to_vary_9010 (thisWave) //Y0 allowed to vary fit 9010
        //Fit_90toEnd_Y0_allowed_to_vary (thisWave) //Y0 allowed to vary fit 90 to end

//        TauFit_NormSmooth_Y0_allowed_to_vary_8020 (thisWave) //Y0 allowed to vary fit 8020
        //Fit_80toEnd_Y0_allowedtovary (thisWave) //Y0 allowed to vary fit 80 to end 

        TauFit_ConstraintsAllowedToVary_8020 (thisWave)
//        TauFit_ConstraintsAllowedToVary_9020 (thisWave)

            //TauFit_NormSmooth_AllConstraintsHeld (thisWave) 
                /////tau fit to normalized smoothed wave --> all constraints held constant except for tau/////

            //TauFit_NormSmooth_Y0_allowed_to_vary (thisWave)
                /////tau fit to norm smoothed wave --> Y0 is allowed to vary/////others held constant

            //TauFit_NormSmooth_X0_allowed_to_vary (thisWave)
                /////tau fit to norm smoothed wave --> X0 is allowed to vary////others held constant 

            //TauFit_NormSmooth_A_allowed_to_vary (thisWave)
                /////tau fit to norm smoothed wave --> A is allowed to vary////others held constant
        endif

    endfor
end






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                //Get minPeak and minPeakTime of the raw data//
Function [variable minPeak, variable minPeakTime] getMinValues (Wave thisWave)
    ////// minPeak = the event min (or max) peak (the highest point)
    //////minPeakTime = the time at which that min (max) peak occured

     WaveStats/Q thisWave //WaveStats is an intrinsic function in Igor//

        minPeak= V_min ///giving the variable minpeak a value///

         minPeakTime= V_minloc ///giving the minpeaktime variable the value (time) of when the minpeak value occured

    return [minPeak, minPeakTime]
end
    







///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Function: this function smooths the normalized raw event and normalizes that event. Tau is fit to the normalized smoothed wave. In this iteration of the code
///all constraints are held and tau is fit to 80 to 20 percent
function TauFit_NormSmooth_AllConstraintsHeld_8020 (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime ///get the min peak values from the norm raw event///
            
    variable min80 
    variable min20 
    variable decay8020 
    variable fall80 
    variable fall20 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime= 1.0 
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave_8020_constraintsheld"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed so that the original wave is not overwritten///
       
        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(smoothedwave,0)
    endtime=pnt2x(smoothedwave, numpnts(smoothedwave)-1) ///pulls all of the points in the wave and converts them to x-values///

    [variable minpeaksmoothedwave, variable minPeakTimesmoothedwave] = getMinValues (smoothedwave)
        print "the min peak of the smoothed wave is:", minpeaksmoothedwave, "the min peak time of the smoothed wave is:", minpeaksmoothedwave

                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

     [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
        print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

  
    min80=(0.8*minpeakNormSmooth) ///.8 of the peak is 80%///
    min20=(0.2*minpeakNormSmooth) ///.2 of the peak is 20%///

    findlevel/R = (minpeaknormsmooth, endtime) normsmoothedWave, min80  ///finds the level crossing of .8 starting at the min peak to end
        if(V_flag==0) 
        fall80=V_levelX
            else
        endif

    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min20 ///finds the level crossing of .2 starting at the min peak to end
        if(V_flag==0)
        fall20=V_levelX 
        endif

    print "80% value is", min80, "and it occurs at", fall80
    print "20% value is", min20, "and it occurs at", fall20

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_all_constraints_held_80_20"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    fit_norm_smoothedWave = 0
    V_FitError = 0 
     
    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedWave (fall80, fall20) /D=fit_norm_smoothed_wave_all_constraints_held_80_20
    ///0 means its not being held, and 1 means its being held///

    print "Tau is:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau8020= The_wave_for_coefs [0]
   
    display/k=1/N= Fit_8020_AllConstraintsHeld normSmoothedWave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    //this section of the code will append the tau fit to the top graph
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorPretty(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( w_coef[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay8020= Tau8020*1000 ///converting tau from seconds to ms
        print "the decay8020 when the constraints are held is:", decay8020

        return decay8020
end
    






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function: All constraints are held and tau is fit from 80% to the end of the wave
function TauFit_NormSmooth_AllConstraintsHeld_80toEnd (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime ///get the min peak values from the norm raw event///
            
    variable min80 
    variable min20 
    variable decay80toEnd
    variable fall80 
    variable fall20 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime= 1.0 
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave_80toEnd_constraintsheld"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed so that the original wave is not overwritten///
       
        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(smoothedwave,0)
    endtime=pnt2x(smoothedwave, numpnts(smoothedwave)-1) ///pulls all of the points in the wave and converts them to x-values///

    [variable minpeaksmoothedwave, variable minPeakTimesmoothedwave] = getMinValues (smoothedwave)
        print "the min peak of the smoothed wave is:", minpeaksmoothedwave, "the min peak time of the smoothed wave is:", minpeaktimesmoothedwave

                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

     [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
        print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

  
    min80=(0.8*minpeakNormSmooth) ///.8 of the peak is 80%///
    min20=(0.2*minpeakNormSmooth) ///.2 of the peak is 20%///

    findlevel/Q/R = (minpeaknormsmooth, endtime) normsmoothedWave, min80  ///finds the level crossing of .8 starting at the min peak to end
        if(V_flag==0) 
        fall80=V_levelX
            else
        endif

    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min20 ///finds the level crossing of .2 starting at the min peak to end
        if(V_flag==0)
        fall20=V_levelX 
        endif

    print "80% value is", min80, "and it occurs at", fall80
    print "20% value is", min20, "and it occurs at", fall20

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_all_constraints_held_80toEnd"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    fit_norm_smoothedWave = 0
    V_FitError = 0 
     
    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedWave (fall80, endtime) /D=fit_norm_smoothed_wave_all_constraints_held_80toEnd
    ///0 means its not being held, and 1 means its being held///
    ///NOTE: this is fitting from .80 to the end!

    print "Tau is:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau80toEnd= The_wave_for_coefs [0]
   
    display/k=1/N=Fit_80toEnd_AllConstraintsHeld normSmoothedWave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    //this section of the code will append the tau fit to the top graph
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorPretty(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( w_coef[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay80toEnd= Tau80toEnd*1000 ///converting tau from seconds to ms
        print "the decay80toEnd when the constraints are held is:", decay80toEnd

        return decay80toEnd
end












////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function: this function fits tau to .9 and .1 and all of the constraints are held 
function TauFit_NormSmooth_AllConstraintsHeld_9010 (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime

    variable min90
    variable min10
    variable decay9010
    variable fall90
    variable fall10
 
    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
        ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 

        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
    endif
    
    ///pull all of the data points from the smoothed wave -- covert to x-values///
    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)
    
                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeaksmoothwave, variable MinPeaktimesmoothwave] = getMinValues (smoothedwave)
            print "the min peak of the smoothed wave is:",  minpeaksmoothwave, "the min peak time of the smoothed wave is:",MinPeaktimesmoothwave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min90=(0.9*minpeakNormSmooth)
    min10=(0.1*minpeaknormsmooth)

    findlevel/R = (minpeakNormSmooth, endtime) normsmoothedWave, min90   

        if(V_flag==0) 
        fall90=V_levelX
            else
        endif

    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10
        if(V_flag==0)
        fall10=V_levelX 
            else
        endif

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

 
    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_all_constraints_held_9010"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
    fit_norm_smoothedWave = 0
    V_FitError = 0 
        ///V_FitError is used from a procedure to attempt to recover from errors during the fit
        ///so what this is doing is preventing an abort in the case of certain errors that arise from 
        ///unpredictable mathematical circumstances. You can do this by setting the V_fitError to 0 before 
        ///performing a fit. The type of error can be set depending on what you want (page III-235 in manual)

    print "90% value is ", min90, "and it occurs at", fall90
    print "10% value is ", min10, "and it occurs at", fall10

    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    
    funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, fall10) /D=fit_norm_smoothed_wave_all_constraints_held_9010 
                        ////all constraints are held constant except for tau in this iteration////


    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau9010 = The_wave_for_coefs [0]

   
    display/k=1/N= fit_9010_AllConstraintsHeld normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    //this section of the code will append the tau fit to the top graph
    //appendtograph fit_norm_smoothed_wave
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorOrange(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( w_coef[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay9010= Tau9010*1000
        print "the decay9010 when the constraints are held is:", decay9010

        return decay9010

end
    










////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function: this function fits tau to .9 and .1 and all of the constraints are held 
function TauFit_NormSmooth_AllConstraintsHeld_90toEnd (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime

    variable min90
    variable min10
    variable decay90toEnd
    variable fall90
    variable fall10
 
    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
        ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 

        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
    endif
    
    ///pull all of the data points from the smoothed wave -- covert to x-values///
    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)
    
                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeaksmoothwave, variable MinPeakTimesmoothwave] = getMinValues (smoothedwave)
            print "the min peak of the smoothed wave is:", minpeaksmoothwave, "the min peak time of the smoothed wave is:", MinPeakTimesmoothwave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min90=(0.9*minpeakNormSmooth)
    min10=(0.1*minpeaknormsmooth)

    findlevel/Q/R = (minpeakNormSmooth, endtime) normsmoothedWave, min90   

        if(V_flag==0) 
        fall90=V_levelX
            else
        endif

    findlevel/R= (minPeakTimenormsmooth, endtime) normsmoothedWave, min10 

        if(V_flag==0)
        fall10=V_levelX 
        endif

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

 
    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_all_constraints_held_90toEnd"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
    fit_norm_smoothedWave = 0
    V_FitError = 0 
        ///V_FitError is used from a procedure to attempt to recover from errors during the fit
        ///so what this is doing is preventing an abort in the case of certain errors that arise from 
        ///unpredictable mathematical circumstances. You can do this by setting the V_fitError to 0 before 
        ///performing a fit. The type of error can be set depending on what you want (page III-235 in manual)

    print "90% value is ", min90, "and it occurs at", fall90
    print "10% value is ", min10, "and it occurs at", fall10

    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    //funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall80, fall20) /D=fit_norm_smoothed_wave_all_constraints_held
    funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, endtime) /D=fit_norm_smoothed_wave_all_constraints_held_90toEnd
                        ////all constraints are held constant except for tau in this iteration////


    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau90toEnd = The_wave_for_coefs [0]

   
    display/k=1/N= fit_90toEnd_AllConstraintsHeld normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    //this section of the code will append the tau fit to the top graph
    //appendtograph fit_norm_smoothed_wave
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorOrange(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( w_coef[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay90toEnd= Tau90toEnd*1000
        print "the decay90toEnd when the constraints are held is:", decay90toEnd

        return decay90toEnd

end
    















//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                     //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
                                 //Holding X0 and A constant --> tau and Y0 not held constant//  
function TauFit_NormSmooth_Y0_allowed_to_vary_9010 (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
    //first I am creating all of the variables that I will be using, for me it helps me to put them
    //all in one place (at the beginning) so that I can more easily keep track of things
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
            
    variable min90
    variable min10
    variable decay9010
    variable fall90
    variable fall10

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
        ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 
        ///if there are more than one
          

        Wave SmoothedWave=$SmoothedWaveName

         if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

        if (doSmoothing ==1)
            print "trying to smooth:"
            Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///  
         endif

    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)
    
  
                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeakSmoothWave, variable minPeakTimeSmoothWave] = getMinValues (SmoothedWave)
            print "the min peak of smoothed wave is:", minPeakSmoothwave, "the min peak time of smooth wave is:", minPeakTimeSmoothWave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min90=(0.9*minpeakNormSmooth)
    min10=(0.1*minpeakNormSmooth)
   
    findlevel/Q/R = (minpeakNormSmooth, endtime) normsmoothedWave, min90   

        if(V_flag==0) 
        fall90=V_levelX
        endif
           
    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10 
        if(V_flag==0)
        fall10=V_levelX 
            else
        endif

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_Y0_allowed_to_vary_9010"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
    fit_norm_smoothedWave = 0
    V_FitError = 0 

    print "90% value is ", min90, "and it occurs at", fall90
    print "10% value is ", min10, "and it occurs at", fall10
  
    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0011" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, fall10) /D=fit_norm_smoothed_wave_Y0_allowed_to_vary_9010 
                        ////all constraints are held constant except for tau in this iteration////


    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau9010 = The_wave_for_coefs [0]

   
    display/k=1/N=Y0_allowed_to_vary_9010 normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave

    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorGreen(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		
            
        if( numtype( w_coef[4] ) == 1 )
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay9010= Tau9010*1000
        print "the decay9010 when Y0 is allowed to vary is:", decay9010

        return decay9010
end
    
















//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                     //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
                                 //Holding X0 and A constant --> tau and Y0 not held constant//  
function Fit_90toEnd_Y0_allowed_to_vary (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
    //first I am creating all of the variables that I will be using, for me it helps me to put them
    //all in one place (at the beginning) so that I can more easily keep track of things
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
            
    variable min90
    variable min10
    variable decay90toEnd
    variable fall90
    variable fall10

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
        ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 
        ///if there are more than one
          

        Wave SmoothedWave=$SmoothedWaveName

         if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

        if (doSmoothing ==1)
            print "trying to smooth:"
            Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///  
         endif

    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)
    
  
                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeakSmoothWave, variable minPeakTimeSmoothWave] = getMinValues (SmoothedWave)
            print "the min peak of smoothed wave is:", minPeakSmoothwave, "the min peak time of smooth wave is:", minPeakTimeSmoothWave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min90=(0.9*minpeakNormSmooth)
    min10=(0.1*minpeaknormsmooth)

    //findlevel/Q/R = (startTime, endTime) normsmoothedWave,min80   
    findlevel/R = (minpeakNormSmooth, endtime) normsmoothedWave, min90   

        if(V_flag==0) 
        fall90=V_levelX
        //fall80=V_levelX
        endif
           
    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10 
        if(V_flag==0)
        //fall20=V_levelX 
        fall10=V_levelX 
        endif

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_Y0_allowed_to_vary_90toEnd"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
    fit_norm_smoothedWave = 0
    V_FitError = 0 

    print "90% value is ", min90, "and it occurs at", fall90
    print "10% value is ", min10, "and it occurs at", fall10
  
    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0011" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, endtime) /D=fit_norm_smoothed_wave_Y0_allowed_to_vary_90toEnd 
                        ////all constraints are held constant except for tau in this iteration////


    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau90toEnd = The_wave_for_coefs [0]

   
    display/k=1/N=Y0_AllowedToVary_90toEnd normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave

    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorGreen(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		
            
        if( numtype( w_coef[4] ) == 1 )
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay90toEnd= Tau90toEnd*1000
        print "the decay90toEnd when Y0 is allowed to vary is:", decay90toEnd

        return decay90toEnd
end
    













//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                     //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
///Function: Y0 is allowed to vary and fit to 8020 
function TauFit_NormSmooth_Y0_allowed_to_vary_8020 (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    //first I am creating all of the variables that I will be using, for me it helps me to put them
    //all in one place (at the beginning) so that I can more easily keep track of things
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
         
    variable min80 
    variable min20 
    variable decay8020 

    variable fall80 
    variable fall20 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///

        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)



                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeakSmoothWave, Variable minPeakTimeSmoothWave] = getMinValues (SmoothedWave)
            print "the mine peak of the smoothed wave is:", minpeaksmoothwave, "the min peak time of the smooth wave is:", minpeaksmoothwave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min80=(0.8*minpeakNormSmooth)
    min20=(0.2*minpeaknormsmooth)

    findlevel/R = (minpeaknormsmooth, endtime) normSmoothedWave, min80  

        
        if(V_flag==0)
        fall80=V_levelX
        endif
       
    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedwave, min20

       if(V_flag==0)
        fall20=V_levelX
        endif
       
    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0


    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_Y0_allowed_to_vary_8020"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    fit_norm_smoothedWave = 0
    V_FitError = 0 

    print "80% value is", min80, "and it occurs at", fall80
    print "20% value is", min20, "and it occurs at", fall20


    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0011" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall80, fall20)  /D=fit_norm_smoothed_wave_Y0_allowed_to_vary_8020
    //funcfit/Q/N/H="0011" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, fall10) /D=fit_norm_smoothed_wave_Y0_allowed_to_vary 
                        ////all constraints are held constant except for tau in this iteration////


    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau8020= The_wave_for_coefs [0]

   
    display/k=1/N=Y0_allowed_to_vary_8020 normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorLavender(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		
            //the V_fitreason is an output that provides additional information about why a nonlinear
            //end so that the fit stops iterating

        if( numtype( w_coef[4] ) == 1 )
            ///w_coef is a wave that is created by the CurveFit/FuncFit operation to contain the fit parameters
            ///the number indicates what the parameter is

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay8020= Tau8020*1000
        print "the decay8020 when Y0 is allowed to vary is:", decay8020
        return decay8020

end
    













/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                     //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
///Function: Y0 is allowed to vary and fit to 80 to the end 
function Fit_80toEnd_Y0_allowedtovary (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    //first I am creating all of the variables that I will be using, for me it helps me to put them
    //all in one place (at the beginning) so that I can more easily keep track of things
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
         
    variable min80 
    variable min20 
    variable decay80toEnd

    variable fall80 
    variable fall20 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///

        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(normSmoothedWave,0)
    endtime=pnt2x(normSmoothedWave, numpnts(normSmoothedWave)-1)

    
                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

            [variable minpeakSmoothWave, Variable minPeakTimeSmoothWave] = getMinValues (SmoothedWave)
            print "the mine peak of the smoothed wave is:", minpeaksmoothwave, "the min peak time of the smooth wave is:", minpeaksmoothwave

            [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
            print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

    min80=(0.8*minpeakNormSmooth)
    min20=(0.2*minpeaknormsmooth)

    findlevel/R = (minpeaknormsmooth, endtime) normSmoothedWave, min80  

        if(V_flag==0)
        fall80=V_levelX
        endif
       
    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedwave, min20

       if(V_flag==0)
        fall20=V_levelX
        endif
       
    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0


    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_Y0_allowed_to_vary_80toEnd"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    fit_norm_smoothedWave = 0
    V_FitError = 0 

    print "80% value is", min80, "and it occurs at", fall80
    print "20% value is", min20, "and it occurs at", fall20


    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N/H="0011" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall80, endtime)  /D=fit_norm_smoothed_wave_Y0_allowed_to_vary_80toEnd

    print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau80toEnd= The_wave_for_coefs [0]

   
    display/k=1/N=Y0_allowed_to_vary_80toEnd normsmoothedwave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorLavender(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		
            //the V_fitreason is an output that provides additional information about why a nonlinear
            //end so that the fit stops iterating

        if( numtype( w_coef[4] ) == 1 )
            ///w_coef is a wave that is created by the CurveFit/FuncFit operation to contain the fit parameters
            ///the number indicates what the parameter is

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay80toEnd= Tau80toEnd*1000
        print "the decay80toEnd when Y0 is allowed to vary:", decay80toEnd
        return decay80toEnd

end
    







/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            //the exponential tau fit equation with the constraints defined//
function TauFit_with_contraints_outlined(coefs, t)
    Wave coefs; Variable t
    variable current
    variable tau, Y0, X0, A
    tau= coefs[0]
    Y0= coefs[1]
    X0= coefs [2]
    A= coefs [3]


    if ( t < 1e-8)
	    current = Y0
        else    

	    current = Y0+A*exp(-(t-X0)/tau)

    endif

    return current
end




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    //NORMALIZE THE SMOOTHED WAVE//
 //this function is going to normalize the smoothed wave that was duplicated above
    //uses the smoothed wave and runs the WaveStats on that smoothed wave
function/wave NormalizeSmoothedWave (Wave SmoothedWave, variable RawMinPeakTime)

        variable SmoothedWaveValueatRawMinPeakTime= SmoothedWave (RawMinPeakTime)
        print "the smoothed peak at the raw min peak time is:", SmoothedWaveValueatRawMinPeakTime

        string TheSmoothedWave= NameofWave(SmoothedWave)
        string NormSmoothedWave= theSmoothedWave + "norm"
        duplicate/O SmoothedWave, $NormSmoothedWave
    
    variable SmoothedWaveMinPeak 
    WaveStats/Q SmoothedWave //run the wave stats on the smoothed wave
    SmoothedWaveMinPeak= V_min
    print "the smoothed wave min peak:" ,SmoothedWaveMinPeak
    
       Wave TheNormalizedSmoothedWave = $NormSmoothedWave

		TheNormalizedSmoothedWave = -SmoothedWave / SmoothedWaveMinPeak

    print "normalized the smoothed wave:" 

    return TheNormalizedSmoothedWave
end 




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
                             //these functions are used to make the graphs and alter graph color//
function sueApprovedYAxis()
    Label left "normalized current"
end

function makeColorBlack(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(0,0,0)
end

function makeColorGrey(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(56797,56797,56797)
end

function makeColorSky(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(32768,40777,65535)
end

function makeColorGreen(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(0,65535,0)
end

function makeColorLavender(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(51664,44236,58982)
end

function makeColorOrange(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(65535,49157,16385)
end

function makeColorPretty(string traceName[, string displayName])
    if(paramIsDefault(displayName))
        displayName = "" // top window
    endif
    
    ModifyGraph/W=$displayName rgb($traceName)=(65535,32768,58981)
end //this is pink :)





// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
//                                  //Holding Y0 and X0 constant --> tau and A not held constant//  
// function TauFit_NormSmooth_A_allowed_to_vary (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
//     //first I am creating all of the variables that I will be using, for me it helps me to put them
//     //all in one place (at the beginning) so that I can more easily keep track of things
//     variable minpeak
//     variable minpeaktime
//             [minpeak, minPeakTime] = getMinValues (thisWave)
//             print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
//             ///get minPeak and minPeakTime values of the raw data///

//     //variable min80 
//     //variable min20 
//     variable min90
//     variable min10

//     //variable decay8020 
//     variable decay9010
    
//     //variable fall80 
//     //variable fall20 
//     variable fall90
//     variable fall10

//     variable start_time
//         if(paramIsDefault(starttime))
//             starttime= 0
//         endif

//     variable end_time
//         if(paramIsDefault(endtime))
//             //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
//         endif

//         string thisWaveName=nameofWave(thisWave)
//         string smoothedWaveName="Norm_SmoothedWave"
//         duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
//         ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 
//         ///if there are more than one
          

//         Wave SmoothedWave=$SmoothedWaveName

//         if(paramIsDefault(doSmoothing))
//             doSmoothing = 1
//         endif

//     if (doSmoothing ==1)
//         print "trying to smooth:"
//         Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
//     endif

//     ///pull all of the data points from the smoothed wave -- covert to x-values///
//     starttime=pnt2x(thisWave,0)
//     endtime=pnt2x(thisWave, numpnts(ThisWave)-1)
    
//     [minpeak, minPeakTime] = getMinValues (ThisWave)

//                                 ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
//     Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

//             [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
//             print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
           
//             print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time is:", minPeakTimeNormSmooth

//     //min80=(0.8*minpeakNormSmooth)
//     //min20=(0.2*minpeaknormsmooth)

//     min90=(0.9*minpeakNormSmooth)
//     min10=(0.1*minpeaknormsmooth)
//             ///NOTE: the 9010 or 8020 are being calculated from the peak of the normalized smoothed data///

//     //findlevel/Q/R = (startTime, endTime) normsmoothedWave,min80   
//     findlevel/Q/R = (startTime, endtime) normsmoothedWave, min90   

//         if(V_flag==0) 
//         fall90=V_levelX
//         //fall80=V_levelX
//             else
//         endif
//             //this is creating an if-then statement
//             //if Igor find that the WaveForm crosses the given point you told it took for then the if 
//             //statement is true (that what the V_flag==0 means); its saying the peak result % peak found 
//             //V_levelX is similar to V_flag
//             //whereas V_flag indicates the success or the failure of the search; V_LevelX contains the
//             //X coordinate of that level crossing
//             //I am giving that X coordinate a variable name, in this case, fall80, fall20, fall90, or fall10


//     findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10 
//     //findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedwave, min20

//         if(V_flag==0)
//         //fall20=V_levelX 
//         fall10=V_levelX 
//         endif

//     make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
//         variable V_FitError = 0, V_fitquitreason = 0

 
//     string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
//     string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_A_allowed_to_vary"
//      duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
//         wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
//     fit_norm_smoothedWave = 0
//     V_FitError = 0 
//         ///V_FitError is used from a procedure to attempt to recover from errors during the fit
//         ///so what this is doing is preventing an abort in the case of certain errors that arise from 
//         ///unpredictable mathematical circumstances. You can do this by setting the V_fitError to 0 before 
//         ///performing a fit. The type of error can be set depending on what you want (page III-235 in manual)

//     print "90% value is ", min90, "and it occurs at", fall90
//     print "10% value is ", min10, "and it occurs at", fall10
//     //print "80% value is", min80, "and it occurs at", fall80
//     //print "20% value is", min20, "and it occurs at", fall20


//     Make/O/N=4 The_wave_for_coefs
//     The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
//     The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
//     The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
//     The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

//     //funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall80, fall20) /D=fit_norm_smoothed_wave_A_allowed_to_vary
//     funcfit/Q/N/H="0110" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, fall10) /D=fit_norm_smoothed_wave_A_allowed_to_vary 
//                         ////all constraints are held constant except for tau in this iteration////


//     print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
//     //variable tau8020= The_wave_for_coefs [0]
//     variable tau9010 = The_wave_for_coefs [0]

   
//     display/N=A_allowed_to_vary normsmoothedwave, thisWave
//     appendtograph $TheFitofThe_Norm_SmoothedWave
//     //this section of the code will append the tau fit to the top graph
//     //appendtograph fit_norm_smoothed_wave
//     //you dont need both the display or append, either or does the job
    
//     ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
//     ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
//     ///rbg converts a wave into a gray scale

//     makeColorOrange(TheFitofThe_Norm_SmoothedWave)
//     makeColorGrey(thisWaveName)
//     makeColorSky(NormSmoothedWaveName)
    
    
//     sueApprovedYAxis()

//         if( V_fitquitreason == 0 )
//              else
//              print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
//         endif		
//             //the V_fitreason is an output that provides additional information about why a nonlinear
//             //end so that the fit stops iterating

//         if( numtype( w_coef[2] ) == 1 )
//             ///w_coef is a wave that is created by the CurveFit/FuncFit operation to contain the fit parameters
//             ///the number indicates what the parameter is

//              print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
//         endif

//         //decay8020= Tau8020*1000
//         //print "the decay8020 is:", decay8020

//         decay9010= Tau9010*1000
//         print "the decay9010 is:", decay9010

//         //return decay8020
//         return decay9010

// end






// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      //TAU FIT FUNCTION TO FIT TO THE NORMALIZED SMOOTHED WAVE// 
//                                  //Holding Y0 and A constant --> tau and X0 not held constant//  
// function TauFit_NormSmooth_X0_allowed_to_vary (Wave thiswave [,variable starttime, variable endtime, variable doSmoothing])
//     //first I am creating all of the variables that I will be using, for me it helps me to put them
//     //all in one place (at the beginning) so that I can more easily keep track of things
//     variable minpeak
//     variable minpeaktime
//             [minpeak, minPeakTime] = getMinValues (thisWave)
//             print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
//             ///get minPeak and minPeakTime values of the raw data///

//     //variable min80 
//     //variable min20 
//     variable min90
//     variable min10

//     //variable decay8020 
//     variable decay9010
    
//     //variable fall80 
//     //variable fall20 
//     variable fall90
//     variable fall10

//     variable start_time
//         if(paramIsDefault(starttime))
//             starttime= 0
//         endif

//     variable end_time
//         if(paramIsDefault(endtime))
//             //endtime=40 ////the end time is not set as a default, so the tau will be fit from the start to the end of the entire wave
//         endif

//         string thisWaveName=nameofWave(thisWave)
//         string smoothedWaveName="Norm_SmoothedWave"
//         duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed///
//         ///by adding this to the code, you prevent Igor from overwriting any of the waves on the top graph 
//         ///if there are more than one
          

//         Wave SmoothedWave=$SmoothedWaveName

//         if(paramIsDefault(doSmoothing))
//             doSmoothing = 1
//         endif

//     if (doSmoothing ==1)
//         print "trying to smooth:"
//         Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
//     endif

//     ///pull all of the data points from the smoothed wave -- covert to x-values///
//     starttime=pnt2x(thisWave,0)
//     endtime=pnt2x(thisWave, numpnts(ThisWave)-1)
    
//     [minpeak, minPeakTime] = getMinValues (ThisWave)

//                                 ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
//     Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

//             [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
//             print "the min peak is:", minPeak, "the min peak time is:", minPeakTime
           
//             print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time is:", minPeakTimeNormSmooth

//     //min80=(0.8*minpeakNormSmooth)
//     //min20=(0.2*minpeaknormsmooth)

//     min90=(0.9*minpeakNormSmooth)
//     min10=(0.1*minpeaknormsmooth)
//             ///NOTE: the 9010 or 8020 are being calculated from the peak of the normalized smoothed data///

//     //findlevel/Q/R = (startTime, endTime) normsmoothedWave,min80   
//     findlevel/Q/R = (startTime, endtime) normsmoothedWave, min90   

//         if(V_flag==0) 
//         fall90=V_levelX
//         //fall80=V_levelX
//             else
//         endif
//             //this is creating an if-then statement
//             //if Igor find that the WaveForm crosses the given point you told it took for then the if 
//             //statement is true (that what the V_flag==0 means); its saying the peak result % peak found 
//             //V_levelX is similar to V_flag
//             //whereas V_flag indicates the success or the failure of the search; V_LevelX contains the
//             //X coordinate of that level crossing
//             //I am giving that X coordinate a variable name, in this case, fall80, fall20, fall90, or fall10


//     findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10 
//     //findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedwave, min20

//         if(V_flag==0)
//         //fall20=V_levelX 
//         fall10=V_levelX 
//         endif

//     make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
//         variable V_FitError = 0, V_fitquitreason = 0

 
//     string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
//     string TheFitofThe_Norm_SmoothedWave= "fit_norm_smoothed_wave_X0_allowed_to_vary"
//      duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
//         wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

   
//     fit_norm_smoothedWave = 0
//     V_FitError = 0 
//         ///V_FitError is used from a procedure to attempt to recover from errors during the fit
//         ///so what this is doing is preventing an abort in the case of certain errors that arise from 
//         ///unpredictable mathematical circumstances. You can do this by setting the V_fitError to 0 before 
//         ///performing a fit. The type of error can be set depending on what you want (page III-235 in manual)

//     print "90% value is ", min90, "and it occurs at", fall90
//     print "10% value is ", min10, "and it occurs at", fall10
//     //print "80% value is", min80, "and it occurs at", fall80
//     //print "20% value is", min20, "and it occurs at", fall20


//     Make/O/N=4 The_wave_for_coefs
//     The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
//     The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
//     The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
//     The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

//     //funcfit/Q/N/H="0111" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall80, fall20) /D=fit_norm_smoothed_wave_X0_allowed_to_vary
//     funcfit/Q/N/H="0101" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedwave (fall90, fall10) /D=fit_norm_smoothed_wave_X0_allowed_to_vary 
//                         ////all constraints are held constant except for tau in this iteration////


//     print "Tau after fit:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
//     //variable tau8020= The_wave_for_coefs [0]
//     variable tau9010 = The_wave_for_coefs [0]

   
//     display/N=X0_allowed_to_vary normsmoothedwave, thisWave
//     appendtograph $TheFitofThe_Norm_SmoothedWave
//     //this section of the code will append the tau fit to the top graph
//     //appendtograph fit_norm_smoothed_wave
//     //you dont need both the display or append, either or does the job
    
//     ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
//     ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
//     ///rbg converts a wave into a gray scale

//     makeColorGreen(TheFitofThe_Norm_SmoothedWave)
//     makeColorGrey(thisWaveName)
//     makeColorSky(NormSmoothedWaveName)
    
    
//     sueApprovedYAxis()

//         if( V_fitquitreason == 0 )
//              else
//              print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
//         endif		
//             //the V_fitreason is an output that provides additional information about why a nonlinear
//             //end so that the fit stops iterating

//         if( numtype( w_coef[4] ) == 1 )
//             ///w_coef is a wave that is created by the CurveFit/FuncFit operation to contain the fit parameters
//             ///the number indicates what the parameter is

//              print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
//         endif

//         //decay8020= Tau8020*1000
//         //print "the decay8020 is:", decay8020

//         decay9010= Tau9010*1000
//         print "the decay9010 is:", decay9010

//         //return decay8020
//         return decay9010

// end








//Function: All constraints are held and tau is fit from 80% to the end of the wave
function TauFit_ConstraintsAllowedToVary_8020 (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime ///get the min peak values from the norm raw event///
            
    variable min80 
    variable min20 
    variable decay80to20
    variable fall80 
    variable fall20 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime= 1.0 
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave_80to20_constraintsVary"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed so that the original wave is not overwritten///
       
        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(smoothedwave,0)
    endtime=pnt2x(smoothedwave, numpnts(smoothedwave)-1) ///pulls all of the points in the wave and converts them to x-values///

    [variable minpeaksmoothedwave, variable minPeakTimesmoothedwave] = getMinValues (smoothedwave)
        print "the min peak of the smoothed wave is:", minpeaksmoothedwave, "the min peak time of the smoothed wave is:", minpeaktimesmoothedwave

                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

     [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
        print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

  
    min80=(0.8*minpeakNormSmooth) ///.8 of the peak is 80%///
    min20=(0.2*minpeakNormSmooth) ///.2 of the peak is 20%///

    findlevel/Q/R = (minpeaknormsmooth, endtime) normsmoothedWave, min80  ///finds the level crossing of .8 starting at the min peak to end
        if(V_flag==0) 
        fall80=V_levelX
            else
        endif

    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min20 ///finds the level crossing of .2 starting at the min peak to end
        if(V_flag==0)
        fall20=V_levelX 
        endif

    print "80% value is", min80, "and it occurs at", fall80
    print "20% value is", min20, "and it occurs at", fall20

    string disptrace = stringfromlist(0, TraceNameList("",";",1))
    cursor A $disptrace, fall80 
    cursor B $disptrace, fall20 


    //make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    //string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    //string TheFitofThe_Norm_SmoothedWave= "fit_all_constraints_vary_8020"
    // duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
    //    wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    //fit_norm_smoothedWave = 0
    V_FitError = 0 
    duplicate/O thisWave, fit
    fit = 0
    variable useCurveFit = 0

    if(useCurveFit)
        Make/D/O/N=3 coefs // \\ // \\ // td: set to starting values!!
        coefs [0]=0.0 //td Y0  HERE
        coefs [1]=-1 //td A 
        coefs [2]=0.006 //td tau 
        CurveFit exp_XOffset, kwCWave=coefs, thiswave[pcsr(A), pcsr(B)] /D=fit
    else 
        Make/D/O/N=4 coefs // \\ // \\ // td: set to starting values!!
        coefs [0]=0.006  ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
        coefs [1]=0.0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
        coefs [2]=0.0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
        coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1
        funcfit/Q/N/H="0000" TauFit_with_contraints_outlined, coefs, normsmoothedWave(fall80, fall20) /D=fit  //=fit_all_constraints_vary_8020
    ///0 means its not being held, and 1 means its being held///
    endif  ///NOTE: this is fitting from .80 to the end!

    print "td Tau is:", coefs [0], "Y0 is:", coefs [1], "X0 is:", coefs [2], "A is:", coefs [3]
    
    variable tau80to20= coefs [0]
   
    display/k=1/N=Fit_80to20_AllConstraintsVary normSmoothedWave, thisWave
    appendtograph fit
//    appendtograph $TheFitofThe_Norm_SmoothedWave

    //this section of the code will append the tau fit to the top graph
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize(fit)=2,rgb(fit)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    //makeColorPretty(TheFitofThe_Norm_SmoothedWave)
    //makeColorGrey(thisWaveName)
    //makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( coefs[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif

        decay80to20= Tau80to20*1000 ///converting tau from seconds to ms
        print "the decay8020 when all of the constraints vary:", decay80to20

        return decay80to20
end







//Function: All constraints are held and tau is fit from 80% to the end of the wave
function TauFit_ConstraintsAllowedToVary_9020 (Wave thiswave [, variable starttime, variable endtime, variable doSmoothing])
    variable minpeak
    variable minpeaktime
            [minpeak, minPeakTime] = getMinValues (thisWave)
            print "the min peak is:", minPeak, "the min peak time is:", minPeakTime ///get the min peak values from the norm raw event///
            
    variable min90
    variable min10
    variable decay9010
    variable fall90
    variable fall10 

    variable start_time
        if(paramIsDefault(starttime))
            starttime= 0
        endif

    variable end_time
        if(paramIsDefault(endtime))
            //endtime= 1.0 
        endif

        string thisWaveName=nameofWave(thisWave)
        string smoothedWaveName="Norm_SmoothedWave_80to20_constraintsVary"
        duplicate/O thisWave, $SmoothedWaveName ///duplicate the wave that will be smoothed so that the original wave is not overwritten///
       
        Wave SmoothedWave=$SmoothedWaveName

        if(paramIsDefault(doSmoothing))
            doSmoothing = 1
        endif

    if (doSmoothing ==1)
        print "trying to smooth:"
        Smooth/B 9, SmoothedWave ///SMOOTH THE WAVE!///
       
    endif

    starttime=pnt2x(smoothedwave,0)
    endtime=pnt2x(smoothedwave, numpnts(smoothedwave)-1) ///pulls all of the points in the wave and converts them to x-values///

    [variable minpeaksmoothedwave, variable minPeakTimesmoothedwave] = getMinValues (smoothedwave)
        print "the min peak of the smoothed wave is:", minpeaksmoothedwave, "the min peak time of the smoothed wave is:", minpeaktimesmoothedwave

                                ////NORMALIZE THE SMOOTHED WAVE USING THE SMOOTH WAVE MIN!!!////
    Wave NormSmoothedWave= NormalizeSmoothedWave (SmoothedWave, minPeak)

     [variable minpeakNormSmooth, variable minPeakTimeNormSmooth] = getMinValues (NormSmoothedWave)
        print "the min peak of norm smoothed wave is:", minpeakNormSmooth, "the min peak time or norm smoothed wave is:", minPeakTimeNormSmooth

  
    min90=(0.9*minpeakNormSmooth) ///.8 of the peak is 80%///
    min10=(0.1*minpeakNormSmooth) ///.2 of the peak is 20%///

    findlevel/Q/R = (minpeaknormsmooth, endtime) normsmoothedWave, min90  ///finds the level crossing of .8 starting at the min peak to end
        if(V_flag==0) 
        fall90=V_levelX
            else
        endif

    findlevel/R= (minPeakTimeNormSmooth, endtime) normsmoothedWave, min10 ///finds the level crossing of .2 starting at the min peak to end
        if(V_flag==0)
        fall10=V_levelX 
        endif

    print "90% value is", min90, "and it occurs at", fall90
    print "10% value is", min10, "and it occurs at", fall10

    make/O/N=4 w_coef ///when making a new wave of whatever dimension, you need to specify that the wave is of "n" dimension////
        variable V_FitError = 0, V_fitquitreason = 0

    string normSmoothedWaveName= nameofwave (thenormalizedsmoothedwave)
    string TheFitofThe_Norm_SmoothedWave= "fit_all_constraints_vary_9010"
     duplicate/O normsmoothedWave, $TheFitofThe_Norm_SmoothedWave
        wave fit_norm_smoothedWave= $TheFitofThe_Norm_smoothedWave

    fit_norm_smoothedWave = 0
    V_FitError = 0 
     
    Make/O/N=4 The_wave_for_coefs
    The_wave_for_coefs [0]=0 ///coefs 0 is tau and WILL NOT BE HELD CONSTANT HERE
    The_wave_for_coefs [1]=0 ///coefs 1 is Y0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [2]=0 ///coefs 2 is X0 and WILL BE HELD CONSTANT AT 0
    The_wave_for_coefs [3]=-1 ///coefs 3 is A and WILL BE HELD CONSTANT AT -1

    funcfit/Q/N TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedWave (fall90, fall10) /D=fit_all_constraints_vary_9010
//    funcfit/Q/N/H="0000" TauFit_with_contraints_outlined, The_wave_for_coefs, normsmoothedWave (fall90, fall10) /D=fit_all_constraints_vary_9010
    ///0 means its not being held, and 1 means its being held///
    ///NOTE: this is fitting from .80 to the end!

    print "Tau is:", The_wave_for_coefs [0], "Y0 is:", The_wave_for_coefs [1], "X0 is:", The_wave_for_coefs [2], "A is:", The_wave_for_coefs [3]
    
    variable tau90to10= The_wave_for_coefs [0]
   
    display/k=1/N=Fit_90to10_AllConstraintsVary normSmoothedWave, thisWave
    appendtograph $TheFitofThe_Norm_SmoothedWave
    //this section of the code will append the tau fit to the top graph
    //you dont need both the display or append, either or does the job
    
    ModifyGraph lsize($TheFitofThe_norm_smoothedWave)=2,rgb($TheFitofThe_norm_smoothedWave)=(0,0,0)
    ///l size sets the thickness used to draw on the graph-- all parameters are default to 1 unless otherwise told
    ///rbg converts a wave into a gray scale

    makeColorPretty(TheFitofThe_Norm_SmoothedWave)
    makeColorGrey(thisWaveName)
    makeColorSky(NormSmoothedWaveName)
    sueApprovedYAxis()

        if( V_fitquitreason == 0 )
             else
             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif		

        if( numtype( w_coef[4] ) == 1 )

             print "failed exp fit, inside measurepeaks", v_fitquitreason, v_fiterror
        endif


        decay9010= Tau90to10*1000 ///converting tau from seconds to ms
        print "the decay9010 when all constraints vary:", decay9010

        return decay9010
end





