#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include <Axis Utilities>
//template for analyzing waves in top graph
// what does it do?
////////////////////////////////////////////////////////////////////////////////
//									FUNCTION recreateTopGraph()
// 20180105 added color spec
////////////////////////////////////////////////////////////////////////////////

function recreateTopGraph()
string mystr
variable myvar
string wavel=tracenamelist("",";",1)
string waven=removequotes(stringfromlist(0,wavel))
variable iwave=0,nwaves=itemsinlist(wavel)
//get display settings
string axisL = AxisList("")
variable iaxis=0, naxes= itemsinlist(axisL)

make/N=(naxes,2)/O axisRange
axisRange=0
make/T/N=(naxes)/O axisRec
axisRec=""

string thisAxis="",win="",gname = findTopGraph(),yaxis="",tinfo="",yaxisflags=""
string LorR="", color = ""
variable r, g, b

for(iaxis=0;iaxis<naxes;iaxis+=1)
	thisAxis = stringfromlist(iaxis,axisL)
	getAxis/Q $thisAxis
	axisRange[iaxis][0]=V_min
	axisRange[iaxis][1]=V_max
	axisRec[iaxis] = getRecString( axisinfo( "", thisAxis ) )
endfor

display/N=dup /k=1
win = S_name // final name of new graph
do
	waven = removequotes(stringfromlist(iwave,wavel))
	tinfo=traceinfo(gname,waven,0)

	color = stringbykey( "rgb(x)", tinfo, "=" )
	sscanf color, "(%d,%d,%d)", r, g, b
	
	yaxis = stringbykey("YAXIS",tinfo)
	yaxisflags=stringbykey("AXISFLAGS",tinfo)
	LorR = yaxisflags[0,1]
	
	WAVE w = $waven
	strswitch( LorR )
		case "/R":
			appendtograph/R=$yaxis/W=$win w
			break
		case "/L":
			appendtograph/L=$yaxis/W=$win w
			break
		default:
			appendtograph/W=$win w
			
			break
	endswitch
	modifygraph rgb($waven)=(r,g,b)
	
	iwave+=1
while(iwave<nwaves)

//apply range settings

for(iaxis=0;iaxis<naxes;iaxis+=1)
	thisAxis = stringfromlist(iaxis,axisL)
	if(strlen(thisAxis)>0)
		setAxis/Z $thisAxis, axisRange[iaxis][0],axisRange[iaxis][1]
		applyRecStr(thisAxis, axisRec[iaxis])
	else
		print "failed to replicate axis: ",thisAxis
	endif
endfor

return nwaves
end

//////////////////////
//
// get topgraph address
//
function/S findTopGraph()
string tgpath=""
// get top window
string winl = winlist("*",";","WIN: 65")
string topwin = stringfromlist(0,winl)

// get graph subwindow 
string childL = childwindowlist(topwin)
string child = stringfromlist(0,childL)

// cunningly combine the two
tgpath = topwin+"#"+child
// print tracenamelist(tgpath,";",1)
return tgpath
end

//////////////////////////
//
// get recreation string
function/S getRecString(axisStr)
string axisStr
string recStr=""

// axis string contains a bunch of junk until RECREATION:
//  need to save everything after RECREATION and return it

// find location of RECREATION:
variable loc=0
loc = strsearch( axisStr, "RECREATION:", INF,1)

recstr = axisstr[loc+strlen("RECREATION:"), inf]


return recStr
end

// function/S getSetAxisCMDString(axisStr)
// string axisStr
// string recStr=""

// // axis string contains a bunch of junk until RECREATION:
// //  need to save everything after RECREATION and return it

// // find location of RECREATION:
// variable loc=0
// loc = strsearch( axisStr, "RECREATION:", INF,1)

// recstr = axisstr[loc+strlen("RECREATION:"), inf]


// return recStr
// end

/////////////////////////////
//
// apply rec string
//
// applies commands in axis recreation string
function applyRecStr(axisName, axisRecStr)
string axisName, axisRecStr
variable ncom = itemsinlist(axisRecStr), icom=0
string com = "", temp="",temp2=""

pauseUpdate

for(icom=0;icom<ncom;icom+=1)
	temp=stringfromlist(icom, axisRecStr)
	temp2=replaceString("(x)",temp,"("+axisName+")")
	com = "ModifyGraph "+temp2
//	print com
	execute com
endfor

resumeUpdate

end


function recreateGraph(string displayName)
	if(WinType(displayName)==1)
		string mystr
		variable myvar
		// Get traces
		string wavel=tracenamelist(displayName,";",25)
		// string wavel=tracenamelist(displayName,";",1)
		print "trace list", wavel
		string waven=removequotes(stringfromlist(0,wavel))
		variable iwave=0,nwaves=itemsinlist(wavel)

		//get display settings
		string axisL = AxisList(displayName)
		variable iaxis=0, naxes= itemsinlist(axisL)

		make/T/N=(naxes)/O axisRec, setAxisCmds, axisLabels
		axisRec=""
		setAxisCmds=""
		axisLabels=""

		String graphName, panelName
		SplitString /E=("(.*#)(.*$)") displayName, panelName, graphName

		string thisAxis="",win="",yaxis="",tinfo="",yaxisflags=""
		string LorR="", color = ""
		variable r, g, b

		for(iaxis=0;iaxis<naxes;iaxis+=1)
			thisAxis = stringfromlist(iaxis,axisL)

			string axisInfoString = axisInfo(displayName, thisAxis)

			// print "full info string", axisInfoString
			// print "SETAXISCMD", stringbykey("SETAXISCMD", axisInfoString)
			// print "SETAXISFLAGS", stringbykey("SETAXISFLAGS", axisInfoString)
			// print "RECREATION", getRecString(axisInfoString)
			// print "axis label", thisAxis, AxisLabelText(displayName, thisAxis)

			axisRec[iaxis] = getRecString( axisinfo( displayName, thisAxis ) )
			setAxisCmds[iaxis] = stringbykey("SETAXISCMD", axisInfoString)
			axisLabels[iAxis] = AxisLabelText(displayName, thisAxis)
		endfor

		display/N=$graphName /k=1
		win = S_name // final name of new graph
		variable aTraceIsBoxPlot = 0
		for(iWave = 0; iWave<nwaves; iWave++)
			string tracen = stringfromlist(iWave, wavel)
			Wave w = traceNameToWaveRef(displayName, tracen)
			tinfo=traceinfo(displayName,tracen,0)
			variable traceType = str2num(stringbykey("TYPE", tinfo))
			if(traceType == 2)
				aTraceIsBoxPlot = 1

				variable numCols = dimsize(w, 1)
				if(numCols>1)
					DFREF wDFR = getWavesDataFolderDFR(w)
					make/T/O/N=(numCols) wDFR:colLabels/Wave=colLabels
					variable iCol = 0
					for(iCol = 0; iCol < numCols; iCol++)
						colLabels[iCol] = GetDimLabel(w, 1, iCol)
					endfor
				endif
			endif
		endfor
		iWave = 0
		do
			tracen = stringfromlist(iWave, wavel)
			print tracen, "tracen"
			waven = removequotes(tracen)
			tinfo=traceinfo(displayName,tracen,0)
			string traceRec = getRecString(tinfo)
			// print tracen, "traceInfo", tinfo
			// print "traceRec", traceRec

			string xwaven = stringbykey("XWAVE", tinfo)
			variable xWaveExists = 0
			if(strlen(xwaven)>0)
				string xwavedf = stringbykey("XWAVEDF", tinfo)
				if(strlen(xwavedf)>0)
					DFREF xWaveDFR = $xwavedf
					if(DataFolderRefStatus(xWaveDFR) != 0)
						Wave/SDFR = xWaveDFR xWave = $xwaven
						if(WaveExists(xWave))
							xWaveExists = 1
						endif
					endif
				endif
			endif
			
			yaxis = stringbykey("YAXIS",tinfo)
			yaxisflags=stringbykey("AXISFLAGS",tinfo)
			LorR = yaxisflags[0,1]

			traceType = str2num(stringbykey("TYPE", tinfo))
			print "trace type", traceType
			
			// WAVE w = $waven
			Wave w = traceNameToWaveRef(displayName, tracen)

			strswitch( LorR )
				case "/R":
					if(traceType == 2)
						AppendBoxPlot/R=$yaxis/W=win w vs colLabels
					else
						if(aTraceIsBoxPlot)
							appendtograph/R=$yaxis/W=$win w vs colLabels
						else
							if(xWaveExists)
								appendtograph/R=$yaxis/W=$win w vs xWave
							else
								appendtograph/R=$yaxis/W=$win w
							endif
						endif
					endif
					break
				case "/L":
					if(traceType == 2)
						AppendBoxPlot/L=$yaxis/W=$win w vs colLabels
					else
						if(aTraceIsBoxPlot)
							appendtograph/L=$yaxis/W=$win w vs colLabels
						else
							if(xWaveExists)
								appendtograph/L=$yaxis/W=$win w vs xWave
							else
								appendtograph/L=$yaxis/W=$win w
							endif
						endif
					endif
					break
				default:
					if(traceType == 2)
						AppendBoxPlot/W=$win w vs colLabels
					else
						if(aTraceIsBoxPlot)
							appendtograph/W=$win w[0][] vs colLabels
						else
							if(xWaveExists)
								appendtograph/W=$win w vs xWave
							else
								appendtograph/W=$win w
							endif
						endif
					endif
					break
			endswitch
			applyRecStr(tracen, traceRec)

			string errorBarString = stringbykey("ERRORBARS", tinfo)
			if(strlen(errorBarString)>0)
				execute errorBarString
			endif

			if(stringmatch(tracen, "datawave") && traceType==2) 
				ModifyBoxPlot/W=$win trace=dataWave, markers={8,8,8},markerSizes={2,2,2} 
				ModifyGraph/W=$win lsize(dataWave)=0 
        		ModifyGraph/W=$win toMode(dataWave)=3 
			endif 
			
			iwave+=1
		while(iwave<nwaves)

		//apply range settings

		for(iaxis=0;iaxis<naxes;iaxis+=1)
			thisAxis = stringfromlist(iaxis,axisL)
			if(strlen(thisAxis)>0)
				string axisCMD = setAxisCmds[iAxis]
				if(strlen(axisCMD)>0)
					execute axisCMD
				endif
				// print "recreation info", axisRec[iAxis]
				applyRecStr(thisAxis, axisRec[iaxis])
				string axisLabel = axisLabels[iAxis]
				if(strlen(axisLabel)>0)
					axisLabel=replaceString("\\\\", axisLabel, "\\")
					// print axisLabel
					Label/W=$win $thisAxis, axisLabel
				endif
			else
				print "failed to replicate axis: ",thisAxis
			endif
		endfor

		return nwaves
	endif // if it's a graph
end


// how to copy Graph subwindow embedded in panel
// calling: DuplGraphInPanelSubwndw("PanelName#GraphName")
// You should end up with new graph, copy,... , kill at the end
// https://www.wavemetrics.com/forum/igor-pro-wish-list/request-duplicateg…

// This is going to be the most robust way to duplicate a graph,
// and way less complicated than the recreateGraph
// Works for boxplots
// Both this function and recreateGraph have to be cautious
// if recreating a graph from events panel where the 
// output display wave changes when a selection is changed
// As the data will change without the axes changing

// Don't have to use this copy - savePlot() function works
// with graphs that are within panels

Function DuplGraphInPanelSubwndw(String gname)
    String rec = WinRecreation(gname, 0) 
	// print "from window recreation", rec
	// Execute rec
    Variable lines=ItemsInList(rec, "\r")
    Variable i
	String newrec = ""
	newrec = ReFactorWinRec(rec)
	// print "rec", newrec
    Execute newrec
end

Function/S ReFactorWinRec(WRstr)
    string WRstr
   
    string gstr
    string fstr, istr, rstr, estr
   
	// print WRstr
   	gstr = "(.*)(Display */W=\(.*?\))(.*$)" // there might be a space between display and /W
    SplitString/E=gstr WRstr, istr, rstr, estr  
	// print "initial", istr, "\rmiddle", rstr, "\rend", estr
    fstr = istr + "Display/K=1" + estr
	// print "after searching for display", fstr
   
    // remove the /HOST component
	gstr = ".*/HOST=#.*"
	variable hostInStr = GrepString(fstr, gstr)
	if(hostInStr)
		gstr = "(.*)(/HOST=#)(.*$)"
		SplitString/E=gstr fstr, istr, rstr, estr  
		// print "initial", istr, "\rmiddle", rstr, "\rend", estr

		fstr = istr + estr
	endif
    return (fstr)
end