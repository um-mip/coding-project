/////////////
//// modified from amanda's "makeCerebroGuides"
////
//structure guideSetUp
//    string wName 
//    variable nrows 
//    variable ncols 
//    variable ctrlH 
//    variable graphH
//    variable graphW 
//    variable topCornerX
//    variable topCornerY
//    variable initWidth
//    variable initHeight
//endstructure
//
//function/S MKPzGuides(STRUCT guideSetup &s)
//// returns keyword list of subwin and controls
//string elementList = ""
//// element: ctrlname, ctrlwin; element2: ctrlname2, ctrlwin2; 
//string element="", elementName="", elementWin=""
//string microPanelNames = ""
//
//    string wName = s.wName 
//
//    variable left = s.topCornerX
//    variable top = s.topCornerY
//    variable right = left + s.initWidth
//    variable bottom = top + s.initHeight
//
//
//    newPanel/K=1/N=$wName /W=(left,top,right,bottom)
//
//    string str = makeGuides(s)
//    
//    element = "tab"
//    elementWin = "tabw"
//    elementName = "tab"
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(FL,FT,FR,UGHtab)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    variable grey = 50000
//    modifypanel cbRGB=(grey, grey, grey)
//    SetDrawLayer UserBack
//    tabcontrol foo, win=$subwin, pos={0,0},proc=mkpFOO, size={500,sg.ctrlH}
// 
//
// // guide names hardcoded for now
//
//    // row 1 col 1 experiment list box
//    microPanelNames = "explistw"
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV0,UGHtab,UGV1,UGHtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    modifypanel cbRGB=(grey, grey, grey)
//    SetDrawLayer UserBack
//    //tabcontrol foo, size={panelDX, panelDY}
//     
//
//    // row 2 col 1 : act control panels
//    microPanelNames = "actcontrolsw1" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV0,UGHtop,UGV1,UGH1graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0 
//    // row 2 col 2 : act control panels
//    microPanelNames = "actcontrolsw2" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV1,UGHtop,UGV2,UGH1graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 2 col 3 : act control panels
//    microPanelNames = "actcontrolsw3" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV2,UGHtop,UGV3,UGH1graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    
//    // row 3 col 1 : act control panels
//    microPanelNames = "actgraph1" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV0,UGH1graphtop,UGV1,UGH1graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 3 col 1 : act control panels
//    microPanelNames = "actgraph2" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV1,UGH1graphtop,UGV2,UGH1graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0        
//    // row 3 col 1 : act control panels
//    microPanelNames = "actgraph3" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV2,UGH1graphtop,UGV3,UGH1graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//
//    // row 4 col 1 : inact control panels
//    microPanelNames = "inactConPan1" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV0,UGH1graphbottom,UGV1,UGH2graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0 
//    // row 4 col 2 : inact control panels
//    microPanelNames = "inactConPan2" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV1,UGH1graphbottom,UGV2,UGH2graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 4 col 3 : inact control panels
//    microPanelNames = "inactConPan3" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV2,UGH1graphbottom,UGV3,UGH2graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    
//    // row 5 col 1 : inact graph panels
//    microPanelNames = "inactgraph1" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV0,UGH2graphtop,UGV1,UGH2graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 5 col 1 : act control panels
//    microPanelNames = "inactgraph2" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV1,UGH2graphtop,UGV2,UGH2graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0        
//    // row 5 col 1 : act control panels
//    microPanelNames = "inactgraph3" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV2,UGH2graphtop,UGV3,UGH2graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//
//    // row 6 col 1 : SI control panels
//    microPanelNames = "SIConPan1" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV0,UGH2graphbottom,UGV1,UGH3graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0 
//    // row 6 col 2 : inact control panels
//    microPanelNames = "SIConPan2" // label and series
//    panelNames += microPanelNames + ";"    
//    NewPanel/N=$microPanelNames/FG=(UGV1,UGH2graphbottom,UGV2,UGH3graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 6 col 3 : inact control panels
//    microPanelNames = "SIConPan3" // label and series
//    panelNames += microPanelNames + ";"    
//    NewPanel/N=$microPanelNames/FG=(UGV2,UGH2graphbottom,UGV3,UGH3graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    
//    // row 7 col 1 : inact graph panels
//    microPanelNames = "SIgraph1" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV0,UGH3graphtop,UGV1,UGH3graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 7 col 1 : act control panels
//    microPanelNames = "SIgraph2" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV1,UGH3graphtop,UGV2,UGH3graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0        
//    // row 7 col 1 : act control panels
//    microPanelNames = "SIgraph3" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV2,UGH3graphtop,UGV3,UGH3graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//
//    // row 8 col 1 : RI control panels
//    microPanelNames = "RIConPan1" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV0,UGH3graphbottom,UGV1,UGH4graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0 
//    // row 4 col 2 : inact control panels
//    microPanelNames = "RIConPan2" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV1,UGH3graphbottom,UGV2,UGH4graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 4 col 3 : inact control panels
//    microPanelNames = "RIConPan3" // label and series
//    panelNames += microPanelNames + ";"
//    NewPanel/N=$microPanelNames/FG=(UGV2,UGH3graphbottom,UGV3,UGH4graphtop)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    
//    // row 5 col 1 : inact graph panels
//    microPanelNames = "RIgraph1" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV0,UGH4graphtop,UGV1,UGH4graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//    // row 5 col 1 : act control panels
//    microPanelNames = "RIgraph2" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV1,UGH4graphtop,UGV2,UGH4graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0        
//    // row 5 col 1 : act control panels
//    microPanelNames = "RIgraph3" // label and series
//    panelNames += microPanelNames + ";"
//    display/N=$microPanelNames/FG=(UGV2,UGH4graphtop,UGV3,UGH4graphbottom)/HOST=$wName // /W=(125,125,375,375)
//    ModifyPanel frameStyle=0, frameInset=0
//
//    return panelNames     //print str
//end
//
//function/s makeGuides( STRUCT guideSetup &s )
//
//    variable iX = 0, iY = 0
//
//    SetDrawLayer UserBack
//
//    variable frac = 0
//    string gName, guideList
//    
//    // Vertical guides based on graph width
//    guideList = "columns:"
//    do 
//        gName = "UGV"+num2str(ix)
//        frac = ix / s.ncols
//        defineguide/W=$s.wName $gName={ FL, frac, FR }
//        guideList += gName + ","
//        ix+=1
//    while( ix <= s.ncols )
//    guidelist += ";"
//
//    // Bottom guide which aligns with the top of the ctrlPanel
//    string tabCtrlguide = "UGH"+"tab"
//    defineguide $tabCtrlguide={ FT, s.ctrlH }
//    //string ctrlGuide = tabCtrlguide
//    // Bottom guide which aligns with the top of the ctrlPanel
//    string topCtrlguide = "UGH"+"top"
//    defineguide $topCtrlguide={ $tabCtrlguide, s.ctrlH }
//    string ctrlGuide = topCtrlguide
//
//    // Other horizontal guides
//        guideList += "rows:"
//iy=1
//        do
//            gName = "UGH"+num2str(iy)
//            frac = iy / s.nrows
//            defineguide $(gName+"graphTop")={ $ctrlguide, s.ctrlH }
//            ctrlGuide =  gName+"graphTop"
//            guidelist += ctrlGuide + ","
//            defineguide $(gName+"graphBottom")={ $topCtrlGuide, frac, FB }
//            ctrlGuide =  gName+"graphBottom"
//            guidelist += ctrlGuide + ","
//            iy+=1
//        while(iy<= s.nrows)
//        return guidelist
//end
//
//
//// structure to store screen parameters
//
//// depends on "screensizer.ipf"
//
//function testtestScreen()
//variable scrwpxl, scrhpxl, scrwpnt, scrhpnt, scrres
//[Struct ScreenSizeParameters ScrP ] = testScreen()
// scrwpxl = Scrp.scrwpxl
// scrhpxl = Scrp.scrhpxl
// scrhpnt = Scrp.scrhpnt
// scrwpnt = Scrp.scrwpnt
// scrres = Scrp.scrres
//print  scrwpxl // width in pixels
//print scrhpxl // height in pixels
//print scrwpnt // width in points
//print scrhpnt // width in points
//print scrRes // screen resolution
//
//end
//function [STRUCT ScreenSizeParameters ScrP] testScreen()
//
//    InitScreenParameters(ScrP)
//
//    return [ ScrP]
//end